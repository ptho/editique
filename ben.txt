#!groovy
// Declarative //
pipeline {
 agent any

 environment { 
 EMAIL_RECIPIENTS = 'email1@domain.com, email2@domain.com'
 }

 stages {
 stage('Build') {
 steps {
bat "git branch"
 echo 'Building..'
 }
 }
 stage('Test') {
 steps {
 echo 'Testing..'
 }
 }
 stage('Deploy') {
 steps {
 echo 'Deploying....'
 }
 }
 }
 
}
