#!/usr/bin/perl
use utf8;

### Application de découpage et d'insertion en GED des avis d'échéance générés par Actimail.
### Demandé dans le cadre de la Sésame 1101103

use strict;
use warnings;

use oEdtk::EDMS qw(EDMS_process);
use oEdtk::Main qw(oe_ID_LDOC);
use oEdtk::Config qw(config_read);
use Date::Calc qw(Today);
use Sys::Hostname;
use File::Basename;
use Cwd;

if ( @ARGV < 2 ) {
    warn "Usage: $0 <pdf_file> <csv_file> [num_lot_reprise]\n\n";
    warn
"\tTraite les pdf et le csv des avis d'échéances générés par Actimail afin de les intégrer en GED.\n";
    warn
"\tPrend en argument le nom sans l'extension du pdf et du csv (qui doivent donc avoir le même nom) suivi du dossier de sortie des pdf et du fichier d'index.\n";
    warn
"\tPour permettre les reprises, peut prendre un 3e argument signifiant le numéro du lot à partir duquel reprendre\n";
    exit 1;
}

my $tailleMaxLot = 50000;

# my $tailleMaxLot = 15;

my $nomdebase_fichier_pdf = basename( $ARGV[0] );
my $nomdebase_fichier_csv = basename( $ARGV[1] );
my $minLot                = $ARGV[2] - 1 || 0;
my $idFile                = $minLot + 1;
my $numPage               = 1;
my $rep_sortie            = getcwd();
my @ligneCSV              = ();
my @ligneIdx1             = ();
my $lastEndNumPage        = 0;
my $numDoc                = 1;
my $idldoc                = oe_ID_LDOC();
my $appname               = "AVECH_ACTIMAIL";

my $numFirstPage = ($tailleMaxLot) * ( $minLot + 1 );

open( CSV_FILE, "<$ARGV[1]" )
  or die "ERROR: Cannot open csv file \"$ARGV[1]\" : $!\n";

mkdir( "lot" . "$idFile" );
my $nomdefichier_idx1 = "$ARGV[1].idx" . $idFile;
open( IDX1_FILE, ">$nomdefichier_idx1" )
  or die "ERROR: Cannot create idx1 file \"$nomdefichier_idx1\" : $!\n";

# Lit ligne par ligne
while (<CSV_FILE>) {
    @ligneCSV = split( ';', $_ );

    if ( $lastEndNumPage < $numFirstPage ) {
        $lastEndNumPage += $ligneCSV[0];

     #		warn "lastEndNumPage = $lastEndNumPage \t numFirstPage = $numFirstPage";
        next;
    }

    $ligneIdx1[0] = $appname;        # Nom de l'application
    $ligneIdx1[1] = $idldoc;         # Id éditique
    $ligneIdx1[3] = $numDoc++;       # Numéro du document dans le pdf
    $ligneIdx1[6] = $ligneCSV[5];    # Numéro contrat individuel
    $ligneIdx1[7] = "$ligneCSV[7] $ligneCSV[6]";           # Nom destinataire
    $ligneIdx1[8] = $ligneCSV[2];                          # Numéro de section
    $ligneIdx1[9] = sprintf( "%04d%02d%02d", Today() );    # Numéro de section
    $ligneIdx1[10] = substr( config_read()->{'EDTK_TYPE_ENV'}, 0, 1 )
      ;    # Type d'environnement sur une lettre
    $ligneIdx1[12] = "$ligneCSV[7] $ligneCSV[6]";    # Ligne adresse 1
    $ligneIdx1[13] = $ligneCSV[3];  # Numéro de contrat collectif en clef GED 1
    $ligneIdx1[15] = $ligneCSV[4];  # Groupe assuré en clef GED 2
    $ligneIdx1[17] =
      $ligneCSV[8];    # Date de naissance du bénéficiaire en clef GED 3
    $ligneIdx1[19] =
      substr( $ligneCSV[9], 0, 10 );    # Période considérée du en clef GED 4
    $ligneIdx1[21] = ( $ligneCSV[1] eq '02' ? 'MUTACITE' : 'MNT' );
    $ligneIdx1[22] = "$nomdebase_fichier_pdf" . "$idFile" . ".pdf";
    $ligneIdx1[24] = "SANTE";
    $ligneIdx1[26] = hostname();

    for ( my $i = $ligneCSV[0] ; $i-- ; ) {    # Pour le nombre de pages
        $ligneIdx1[2] = $numPage++;            # Numéro de page
        print( IDX1_FILE
"$ligneIdx1[0];$ligneIdx1[1];$ligneIdx1[2];$ligneIdx1[3];;;$ligneIdx1[6];$ligneIdx1[7];$ligneIdx1[8];$ligneIdx1[9];$ligneIdx1[10];;$ligneIdx1[12];$ligneIdx1[13];;$ligneIdx1[15];;$ligneIdx1[17];;$ligneIdx1[19];;$ligneIdx1[21];$ligneIdx1[22];;$ligneIdx1[24];;$ligneIdx1[26];;;\n"
        );                                     # Édite la ligne dans l'idx1
    }

    #	warn "numpage = $numPage\n";
    if ( $numPage > $tailleMaxLot ) {
        close(IDX1_FILE);
        my $commandPdftk =
            "pdftk "
          . $ARGV[0] . " cat "
          . ( $lastEndNumPage + 1 ) . "-"
          . ( $numPage - 1 + $lastEndNumPage )
          . " output "
          . $ARGV[0]
          . "$idFile" . ".pdf";
        warn "Process lot $idFile  \n";

        #		warn "pdftk command = $commandPdftk\n";
        system("$commandPdftk");
        EDMS_process( $appname, $idldoc, $ARGV[0] . "$idFile" . ".pdf",
            $nomdefichier_idx1, $rep_sortie . "/lot" . "$idFile" );
        $lastEndNumPage = $numPage - 1 + $lastEndNumPage;
        $numPage        = 1;
        $numDoc         = 1;
        $idldoc++;
        $idFile++;
        mkdir( "lot" . "$idFile" );
        $nomdefichier_idx1 = "$ARGV[1].idx" . $idFile;
        open( IDX1_FILE, ">$nomdefichier_idx1" )
          or die "ERROR: Cannot create idx1 file \"$nomdefichier_idx1\" : $!\n";
        warn "Passage à la suite $idFile \n";
    }
}

close(IDX1_FILE);
close(csv_file);

my $commandPdftk =
    "pdftk "
  . $ARGV[0] . " cat "
  . ( $lastEndNumPage + 1 ) . "-"
  . ( $numPage - 1 + $lastEndNumPage )
  . " output "
  . $ARGV[0]
  . "$idFile" . ".pdf";
warn "Process last lot $idFile  \n";

#	warn "pdftk command = $commandPdftk\n";
system("$commandPdftk");
EDMS_process( $appname, $idldoc, $ARGV[0] . "$idFile" . ".pdf",
    $nomdefichier_idx1, $rep_sortie . "/lot" . "$idFile" );

# EDMS_process($appname, $idldoc, $ARGV[0], $nomdefichier_idx1, $rep_sortie."/lot"."$idFile");

