#!/usr/bin/perl
use utf8;

### - Soufiane BEN YOUNES 21/09/2016
### - Projet H9 - Renouvellement
### - Découpage et insertion en GED des documents de renouvellement envoyés par Actimail et Cortex.
### - Découpage des documents et insertion en BD des metadonnées pour la mise à disposition
### sur l'espace adhérent des documents de renouvellement envoyés par Actimail et Cortex.
### - Dans cette version on utilise plus le script EDMS_PROCESS pour le préparation des fichiers PDF et Index pour Docubase:
### lors des tests on a constaté des ralentissement lors de traitement des fichiers PDF envoyé par actimail et ces ralentissemnt sont provoqués par EDMS_PROCESS lors de controle de conformite de fichier aux normes d'adobe published PDF
### Vu avec Alain, on utilise pas EDMS_PROCESS et on decoupe directemnt les PDF selon le nombre des pages indiqué dans le fichier CVS et genere nous meme le fichier index pour docubase

use strict;
use warnings;
use Data::Dumper;
use oEdtk::Tracking;
use oEdtk::Main;
use oEUser::Lib;
use oEdtk::EDMS qw(EDMS_process);
use oEdtk::Main qw(oe_ID_LDOC);
use oEdtk::Config qw(config_read);
use Date::Calc qw(Today);
use Sys::Hostname;
use File::Basename;
use Cwd;

# use DBUtils qw(dbSelectHash dbSelect dbQuery);
use DBD::mysql;
use DBI;
use File::Copy qw/ move mv /;

if ( @ARGV < 2 ) {
    warn "Usage: $0 <pdf_file> <csv_file> [num_lot_reprise]\n\n";
    warn
"\tTraite les pdf et le csv envoyés par Actimail afin de les intégrer en GED, dans le file systeme et la BD MySQL pour le Web Adhérent.\n";
    warn "\tPrend en argument le nom du pdf et du csv.\n";
    warn
"\tPour permettre les reprises, peut prendre un 3e argument signifiant le numéro du lot à partir duquel reprendre\n";
    exit 1;
}

# Connection à la BD MySQL
my $dbh;
DBconnectMySQL();

#Tracking
my $TRK;

# Récupération de tous les types de documents
my %type_doc = DBselectTypeDoc($dbh);

# warn "INFO : dump TYPE DOC ". Dumper(%type_doc) ."\n";

my $nomdebase_fichier_pdf = basename( $ARGV[0] );
my $nomdebase_fichier_csv = basename( $ARGV[1] );

# En cas de reprise on peut definir la page de reprise
my $numFirstPage;
if   ( defined $ARGV[2] ) { $numFirstPage = $ARGV[2] - 1; }
else                      { $numFirstPage = 0; }

# warn "numFirstPage : $numFirstPage\n";

my $idFile         = $numFirstPage + 1;
my $rep_encours    = getcwd();
my @ligneCSV       = ();
my @ligneIdx1      = ();
my $lastEndNumPage = 1;
my $idldoc         = oe_ID_LDOC();
my $appname        = "";
my $date_archivage = sprintf( "%04d-%02d-%02d", Today() );
my $RepPDF_DMZ     = config_read()->{'EDTK_WEBADH_REP_PDF'};

# my $RepPDF_DMZ = '/pdfi01/pdf/';
my $esp_adh;
my $nomdefichier_idx1;
my $Nom_Sous_Retp;
my $Exist_Fich_idx = 0;
my $rep_sortie;

my $numLigneCSV  = 0;
my $NbDocParLot  = 0;
my $NbPageParLot = 0;

### TRACKING
$TRK = oEdtk::Tracking->new(
    '',
    user   => 'DocsRenouv',
    entity => oe_corporation_set(),
    keys   => [ 'TYPE_DOC', 'NOM_FICHIER', 'NUM_LOT', 'NB_DOC', 'NB_PAGE' ]
);

open( CSV_FILE, "<$ARGV[1]" )
  or die "ERROR: Cannot open csv file \"$ARGV[1]\" : $!\n";

# Lecture ligne par ligne du fichier CSV
while (<CSV_FILE>) {
    @ligneCSV = split( ';', $_ );

    if ( $ligneCSV[0] eq 'Identifiant document' || $ligneCSV[0] eq '' ) {
        next;
    }

    if ( $lastEndNumPage < $numFirstPage ) {
        $lastEndNumPage += $ligneCSV[1];

      # warn "lastEndNumPage = $lastEndNumPage \t numFirstPage = $numFirstPage";
        next;
    }

    $appname = $ligneCSV[0];
    my $Newappname = $appname;
    $Newappname =~ s/[-\.]/_/g;

    if ( $Exist_Fich_idx eq 0 ) {

        #### CREATION DU FICHIER D'INDEX POUR DOCUBASE
        $nomdefichier_idx1 = $Newappname . "_" . $idldoc . ".idx";
        open( IDX1_FILE, ">$nomdefichier_idx1" )
          or die "ERROR: Cannot create idx1 file \"$nomdefichier_idx1\" : $!\n";
        warn "Création du fichier index pour Docubase : $nomdefichier_idx1\n";

        #### CREATION DU DOSSIER DE SORTIE POUR LES PDF
        $Nom_Sous_Retp = $appname . "_" . $idldoc;
        mkdir($Nom_Sous_Retp);

        $rep_sortie = $rep_encours . "/" . $Nom_Sous_Retp;

        # warn "REPERTOIRE SORTIE : $rep_sortie\n";

        $Exist_Fich_idx = 1;
    }

    my $NbPage = $NbPageParLot + 1;
    my $numDocIdx = sprintf( "%07u", $NbPage );

    #### PREPARATION DU FICHIER INDEX POUR DOCUBASE
    $ligneIdx1[0] = $ligneCSV[0]
      ;    # Identifiant document (type de document) - Nom de l'application
    $ligneIdx1[1] = $ligneCSV[2];    # Entité juridique
    $ligneIdx1[2] = $ligneCSV[3];    # Domaine métier
    $ligneIdx1[3] = $idldoc . "_"
      . $numDocIdx;    # Identifiant unique de document - Id éditique
    $ligneIdx1[4]  = $ligneCSV[13];    # Date d'édition
    $ligneIdx1[5]  = $ligneCSV[5];     # Numéro de contrat individuel
    $ligneIdx1[6]  = $ligneCSV[6];     # Numéro contrat collectif
    $ligneIdx1[7]  = $ligneCSV[7];     # Nom destinataire
    $ligneIdx1[8]  = $ligneCSV[8];     # Ville destinateur
    $ligneIdx1[9]  = $ligneCSV[9];     # Numéro de section
    $ligneIdx1[10] = $ligneCSV[11];    # Date de naissance
    $ligneIdx1[11] = $ligneCSV[10];    # N° groupe assuré
    $ligneIdx1[12] = $ligneCSV[4];     # N° adhérent (ID_MNT)
    $ligneIdx1[13] = $ligneCSV[9];     # Numéro de section
    $ligneIdx1[14] =
        $Newappname . "_"
      . $idldoc . "_"
      . $numDocIdx
      . ".pdf";                        #Le nom du fichier PDF

    #### ECRITURE DANS LE FICHIER INDEX LIGNE PAR LIGNE
    print( IDX1_FILE
"$ligneIdx1[0];$ligneIdx1[1];$ligneIdx1[2];$ligneIdx1[3];$ligneIdx1[4];$ligneIdx1[5];$ligneIdx1[6];$ligneIdx1[7];$ligneIdx1[8];$ligneIdx1[9];$ligneIdx1[10];$ligneIdx1[11];$ligneIdx1[12];$ligneIdx1[13];$ligneIdx1[14]\n"
    );

    my $NbPagePDF = $ligneCSV[1];

    my $numPageParDoc = 0;
    for ( my $i = $ligneCSV[1] ; $i-- ; ) {    # Pour le nombre de pages
        $numPageParDoc++;

        # tester la structure du fichier CSV
        if ( $numPageParDoc > 15 ) {
            warn "\t ERROR : Merci de vérifier la structure du fichier CSV -"
              . $ARGV[1] . "-\n";
            exit 1;
        }
    }

    #### Insertion de l'index dans la BD
    my $file_path =
        $RepPDF_DMZ
      . $Nom_Sous_Retp . "/"
      . $ligneIdx1[14];    # URL de fichier PDF sur le DMZ
        # warn "INFO : l'URL du fichier PDF dans la BD = $file_path \n";

    my $id_TypeDOC = $type_doc{$appname}[0]
      ; # recupération de l'Id de type_doc à partir de la table de paramétrage dans BD DMZ
        # warn "id type document : $id_TypeDOC\n";

    $esp_adh = $type_doc{$appname}[1]
      ; # verifier si le fichier doit apparaitre sur le web adhérent ou non à partir de la table de paramétrage dans BD DMZ
        # warn "esp_adh : $esp_adh\n";

    # Insertion dans la BD DMZ
    if ( $esp_adh eq 1 ) {
        warn "BEFORE INSERT : " . localtime . "\n";
        DBInsertDOC( $dbh, $id_TypeDOC, $ligneCSV[4], $ligneCSV[13],
            $date_archivage, $ligneIdx1[3], $file_path, $ligneCSV[5] );

# warn "DBInsertDOC = $id_TypeDOC,".$ligneCSV[4].",".$ligneCSV[13].",$date_archivage,$idldoc _ $numDocIdx,$file_path,".$ligneCSV[5]."\n";
        warn "AFTER INSERT : " . localtime . "\n";
    }

    ### CREATION DU FICHIER PDF POUR DOUBASE ET LE WEB ADHERENT
    my $lastNumPage = $lastEndNumPage + $NbPagePDF - 1;
    my $commandPdftk =
        "pdftk "
      . $ARGV[0]
      . " cat $lastEndNumPage-$lastNumPage output "
      . $ligneIdx1[14];
    warn "BEFORE PDFTK : " . localtime . "\n";
    warn "pdftk command = $commandPdftk\n";
    system("$commandPdftk");
    warn "AFTER PDFTK : " . localtime . "\n";

    # Déplacement du fichier PDF dans le dossier de sortie
    mv( $ligneIdx1[14], $rep_sortie )
      or die "Déplacement impossible du fichier PDF : $!";

    ### LES VARIABLES
    $NbPageParLot =
      $NbPageParLot +
      $NbPagePDF;    # Incrémentation de nombre des pages dans le PDF source
                     # warn "NbPageParLot = $NbPageParLot\n";

    $numLigneCSV++;

    # warn "Je suis à la ligne $numLigneCSV dans l'index\n";

    $NbDocParLot++;
    $lastEndNumPage = $lastEndNumPage + $NbPagePDF;
}

if ( $Exist_Fich_idx eq 1 ) {
    close(IDX1_FILE);

    # Déplacement du fichier INDEX dans le dossier de sortie
    mv( $nomdefichier_idx1, $rep_sortie )
      or die "Déplacement impossible du fichier INDEX : $!";

    ## TRACKING
# warn "track command = $appname,$nomdebase_fichier_pdf/$nomdebase_fichier_csv,Lot $idFile,$NbDocParLot,$NbPageParLot\n";
    $TRK->track( 'Doc', 1, "$appname",
        "$nomdebase_fichier_pdf/$nomdebase_fichier_csv",
        "Lot $idFile", $NbDocParLot, $NbPageParLot );

    ### DEPLACEMENT DES FICHIER PDF DANS LE FILE SYSTEME DMZ POUR LE WEB ADHERENT
    if ( $esp_adh eq 1 ) {
        transfertSCP($rep_sortie);

        # warn "transfertSCP = $sourceRept\n";
    }
}
close(CSV_FILE);
$dbh->disconnect();

sub DBconnectMySQL {

    # Connection à la base de données mysql
    my $bd =
      config_read()->{'EDTK_WEBADH_BD_BASE'};    # Le nom de la base de données
    my $serveur = config_read()->{'EDTK_WEBADH_HOST'}
      ;    # Le serveur du BD : Il est possible de mettre une adresse IP
    my $identifiant = config_read()->{'EDTK_WEBADH_BD_LOGIN'}
      ;    # identifiant de la base de données
    my $motdepasse = config_read()->{'EDTK_WEBADH_BD_PASSWORD'}
      ;    # Le mot de passe de la base de données
    my $port = config_read()->{'EDTK_WEBADH_BD_PORT'}
      ;    # Si vous ne savez pas, ne rien mettre 3306 - 8443

    warn "Tentative de connexion à la base de données $bd\n";
    $dbh = DBI->connect(
        "DBI:mysql:database=$bd;host=$serveur;port=$port",
        $identifiant,
        $motdepasse,
        {
            RaiseError => 1,
        }
      )
      or die
"Connection impossible à la base de données $bd !\n $! \n $@\n$DBI::errstr\n";
    warn "Connexion établie à la base de données $bd\n";
}

sub DBselectTypeDoc {

# requete de selection de la liste de types documents pour récuperer l'id de type-doc et la mise à disposition sur le web adhérent ou non
    my $dbh = shift;
    my %liste_doc;

    my $requeteSelect = <<"SQL";
    SELECT * FROM webadh_type_doc
SQL

    my $prep = $dbh->prepare($requeteSelect) or die $dbh->errstr;
    $prep->execute() or die "Echec requête : $requeteSelect\n";

    # warn "Il y a ", $prep->rows, " lignes dans la table <webadh_type_doc>\n";

    while ( my $les_types_doc = $prep->fetchrow_hashref ) {
        push @{ $liste_doc{ $les_types_doc->{type_doc} } },
          $les_types_doc->{id};
        push @{ $liste_doc{ $les_types_doc->{type_doc} } },
          $les_types_doc->{esp_adh};
    }
    $prep->finish();

    return %liste_doc;
}

sub DBInsertDOC {

    # requete d'insertion de l'index dans la BD DMZ pour le web adhérent
    my ( $dbh, $idType, $numABA, $dtEdition, $dtArch, $idUnique, $file,
        $numCTR ) = @_;

    my $requeteINSERT = <<"SQL";
    INSERT INTO webadh_documents ( id_type, num_aba, date_extract, date_archivage, id_doc, file_path, num_contrat )
	VALUES ( ?, ?, ?, ?, ?, ?, ? )
SQL

    my $prep = $dbh->prepare($requeteINSERT);
    $prep->execute( $idType, $numABA, $dtEdition, $dtArch, $idUnique, $file,
        $numCTR )
      or die "Echec requête : $requeteINSERT  : $DBI::errstr \n";
    $prep->finish();
}

sub transfertSCP {

    # transfert des fichiers PDF vers le file Systeme DMZ pour le web adhérent

    my $source = shift;

    # print "$source\n";
    # print "$RepPDF_DMZ\n";

    my $LocalServeur = config_read()->{'EDTK_TYPE_ENV'};
    print "$LocalServeur\n";

   # si on travail en local ou sur LMNDV00, le transfert des fichier est annulé
    if ( $LocalServeur ne 'Test' && $LocalServeur ne 'Développement' ) {

        # Connection à la base de données mysql
        my $serveur = config_read()->{'EDTK_WEBADH_HOST'}
          ;    # Le nom du serveur: Il est possible de mettre une adresse IP
        my $identifiant =
          config_read()->{'EDTK_WEBADH_LOGIN'};    # identifiant du serveur

        my $scp = "scp -rp $source $identifiant\@$serveur:$RepPDF_DMZ";
        system("$scp");
    }
}
