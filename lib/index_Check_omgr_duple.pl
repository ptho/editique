#!/usr/bin/perl
use utf8;

use strict;
use warnings;

use oEdtk::Main;
use oEdtk::Config qw(config_read);
use oEdtk::DBAdmin qw(db_connect);
use oEdtk::Outmngr qw(omgr_stats);

if ( @ARGV > 0 and $ARGV[0] =~ /-h/i ) {
    die "Usage: $0 [refiddoc] \n"
      . " This looks for potential DUPLE in index output management for last 10 days.\n\n";
}

use Date::Calc qw(Today Gmtime Week_of_Year Add_Delta_Days);
my ( $YWW, $YWW1, $YWW2, $FROM_DATE );
{
    my $time = time;
    my ( $year, $month, $day, $hour, $min, $sec, $doy, $dow, $dst ) =
      Gmtime($time);

    #	$YWW2 = $ARGV[0] || sprintf ("%1d%02d", $year % 10, $week );
    #	$YWW1 = $YWW2-1; # ce qui est faut pour #01 => à finaliser
    #	$YWW2 .='%';
    #	$YWW1 .='%';

    ( $year, $month, $day ) = Add_Delta_Days( $year, $month, $day, -10 );
    my ( $week, ) = Week_of_Year( $year, $month, $day );
    $YWW =
      sprintf( "%1d%02d0000000000000", $year % 10, $week );  # 201 2113638028244

    $FROM_DATE = sprintf( "%4d%02d%02d000000", $year, $month, $day );
}    #                           20120103113640

my $refiddoc = $ARGV[0] || '%';

my $cfg = config_read('EDTK_STATS');
my $dbh =
  db_connect( $cfg, 'EDTK_DBI_STATS', { AutoCommit => 1, RaiseError => 1 } );

################################################################################

my $select =
  "SELECT A.ED_APP, B.ED_IDLDOC, B.ED_IDEMET, B.ED_STATUS, "    # , B.ED_CLEGED1
  . " (SELECT COUNT (DISTINCT C.ED_SEQDOC) "
  . "   FROM "
  . $cfg->{'EDTK_STATS_OUTMNGR'} . " C "
  . "   WHERE C.ED_IDLDOC = B.ED_IDLDOC "
  . " )AS NB_DOCS ";

my $sql =
    " FROM "
  . $cfg->{'EDTK_STATS_TRACKING'} . " A, "
  . $cfg->{'EDTK_STATS_OUTMNGR'} . " B "
  . " WHERE A.ED_SNGL_ID = B.ED_IDLDOC "
  . " AND A.ED_JOB_EVT != 'W' " # pour ne pas récupérer les events warning (td Inex_Block_Refs)
  . " AND A.ED_SNGL_ID > ? " . " AND A.ED_APP LIKE ? ";
my $groupby = " GROUP BY A.ED_APP, B.ED_IDLDOC, B.ED_IDEMET, B.ED_STATUS ";
my $orderby =
  " ORDER BY  A.ED_APP, B.ED_IDEMET ASC, NB_DOCS DESC, B.ED_IDLDOC DESC ";

$sql = $select . $sql . $groupby . $orderby;

#warn "\nINFO : $sql\n\n";

my $sth = $dbh->prepare($sql);
$sth->execute( $YWW, $refiddoc );

my $format     = '%11s %16s %8s %8s %7s %s';
my $check_keyC = "";
my $check_keyP = "";
my $count      = 0;
my $rows       = $sth->fetchall_arrayref();

#	my @checked_rows;
foreach my $row (@$rows) {
    for ( my $i = 0 ; $i <= $#$row ; $i++ ) {
        $$row[$i] = $$row[$i]
          || ""
          ; # IL PEUT ARRIVER QUE CERTAINS CHAMPS SOIENT PAS ENCORE RENSEIGNE (STATUS, CLEGED1...)
    }

    # $$row[0] : ED_APP / ED_REFIDDOC
    # $$row[2] : ED_IDEMET
    # $$row[5] : COUNT NB_DOCS
    $check_keyC = sprintf( "%-020s%06s%07s", $$row[0], $$row[2], $$row[4] );
    warn "INFO :  \$check_keyC = $check_keyC\n";

    if ( $$row[4] eq "ANO" or $$row[4] eq "DUPLE" or $$row[4] eq "STOP" ) {
        push( $row, "HALTED" );
        $count++;
    }
    elsif ( $check_keyC eq $check_keyP ) {
        push( $row, "WARNING !" );
        $count++;
    }
    $check_keyP = $check_keyC;

    # push (@tlist, printf ("%-16s %9s %9s %9s %8s %8s %7s %-10s %s\n", @$row));
}

################################################################################

#if ($#$rows<0) {
if ( $count < 1 ) {
    warn
"INFO : pas de donnees suspectes identifiees pour la periode $FROM_DATE.\n";
    exit;

}
warn sprintf "\tfrom EDTK_STATS_OUTMNGR :\n\n$format \n", "APPLICATION",
  "IDLDOC", "IDEMET", "STATUS", "NB_DOCS", "";

foreach my $row (@$rows) {
    for ( my $i = 0 ; $i <= $#$row ; $i++ ) {
        $$row[$i] = $$row[$i]
          || ""
          ; # CERTAINES VALEURS PEUVENT NE PAS ÊTRE RENSEIGNÉES DANS CERTAINS CAS
    }
    printf "$format \n", @$row, ""
      if ( $$row[-1] eq "WARNING !" or $$row[-1] eq "HALTED" )
      ;     # 1391152325098839
}
