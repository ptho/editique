#!/usr/bin/perl
use oEUser::Lib;

if ( @ARGV < 0 or @ARGV > 2 or $ARGV[0] =~ /-h/i ) {
    die
"Usage: $0 Section [SANTE|PREV]\n\n\tCe script permet de tester l'adresse retour parametree pour la section\n\tdans adr.ret.ini\n";
}

my $section = $ARGV[0] || 'D001';
my $branche = $ARGV[1] || 'SANTE';

print
  "Adresse retournee pour la section '$section' de la branche '$branche' :\n";
printf( "\n%s\n%s\n%s\n%s\n%s\n%s\n%s",
    user_get_adr_ret( $branche, $section ) );
print "\n";
1;
