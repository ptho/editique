#!/usr/bin/perl

### - Projet H9 - Renouvellement
### - Découpage et insertion en GED des documents de renouvellement envoyés par Actimail.
### - Découpage des documents et insertion en BD des metadonnées pour la mise à disposition
###   sur l'espace adhérent des documents de renouvellement envoyés par Actimail.
### - Soufiane BEN YOUNES 25/07/2016
### - Suppression du retour chariot lors de la lecture du fichier index - G. Ballin
### - Lancement de jobs en simultané - 21/10/2016 : SBY + GB

use strict;
use warnings;
use Data::Dumper;
use oEdtk::Tracking;
use oEdtk::Main;
use oEUser::Lib;
use oEdtk::EDMS qw(EDMS_process);
use oEdtk::Main qw(oe_ID_LDOC);
use oEdtk::Config qw(config_read);
use Date::Calc qw(Today);
use Sys::Hostname;
use File::Basename;
use Cwd;
use DBD::mysql;
use DBI;

#### Lancement de jobs en simultané
use threads;
use threads::shared;

if ( @ARGV < 2 ) {
    print STDERR "Usage: $0 <pdf_file> <csv_file> [num_lot_reprise]\n\n";
    print STDERR
"\tTraite les pdf et le csv envoyés par Actimail ou Cortex afin de les intégrer en GED et aussi dans le file systeme et la BD MySQL DMZ pour l'espace Adhérent.\n";
    print STDERR "\tPrend en argument le nom du pdf et du csv.\n";
    print STDERR
"\tPour permettre les reprises, on peut prendre un 3e argument signifiant le numéro du lot à partir duquel reprendre\n";
    exit 1;
}

# my $tailleMaxLot= 20000;
my $tailleMaxLot = 2000;

my $nomdebase_fichier_pdf = basename( $ARGV[0] );
my $nomdebase_fichier_csv = basename( $ARGV[1] );
my $nomFichier            = substr $nomdebase_fichier_pdf, 0, -4;

my $minLot;
if   ( defined $ARGV[2] ) { $minLot = $ARGV[2] - 1; }
else                      { $minLot = 0; }

my $idFile         = $minLot + 1;
my $lastEndNumPage = 1;
my $Exist_Fich_idx = 0;
my $NbDocParLot    = 0;
my $NbPageParLot   = 0;
my $NbLots         = 0;

my $appname = "";
my $esp_adh;
my $nomdefichier_idx1;

my $rep_origine    = getcwd();
my $idldoc         = oe_ID_LDOC();
my $date_archivage = sprintf( "%04d-%02d-%02d", Today() );
my $RepPDF_DMZ     = config_read()->{'EDTK_WEBADH_REP_PDF'};

my @ligneCSV  = ();
my @ligneIdx1 = ();

# Vecteur de threads
my @processes;
my $limit_process = 10;

# une table de hachage pour les lots de threader
my %lot = ();

# quelques variables partagées
my $cpt : shared;
$cpt = 0;
my $NbJOBs = 0;

# Récupération de tous les types de documents
my %type_doc = DBselectTypeDoc();

# print STDERR "INFO : dump TYPE DOC ". Dumper(%type_doc) ."\n";

my $numFirstPage = ($tailleMaxLot) * ($minLot) + 1;

if ( open( CSV_FILE, "<$ARGV[1]" ) ) {
    print STDERR "INFO : Traitement en cours du fichier CSV \"$ARGV[1]\"\n";
}
else {
    print STDERR
      "ERROR : Impossible d'ouvrir le fichier CSV \"$ARGV[1]\" : $!\n";
    exit 1;
}

### Lecture du fichier CSV ligne par ligne
my $NumLigneCSV = 0;
while (<CSV_FILE>) {
    @ligneCSV = split( ';', $_ );

    $NumLigneCSV++;

    # if (
        # $NumLigneCSV eq 1
        # && (   $ligneCSV[0] eq 'Identifiant document'
            # || $ligneCSV[0] eq ''
            # || $ligneCSV[0] eq '"id_doc"'
            # || $ligneCSV[0] eq 'id_doc' )
      # )
    # {
        # next;
    # }

    if ( $ligneCSV[1] =~ m/^\d+$/ ) {    # si c'est numérique
        if ( $lastEndNumPage < $numFirstPage ) {
            $lastEndNumPage += $ligneCSV[1];
            next;
        }
    }
    else {
        print STDERR
"\t ERROR : Merci de vérifier la structure de la ligne n° $NumLigneCSV du fichier CSV -"
          . $ARGV[1] . "-\n";
        exit 1;
    }

    if ( $Exist_Fich_idx eq 0 ) {

        #### CREATION FICHIER D'INDEX
        print STDERR "############### LOT $idFile ###############\n";

        $nomdefichier_idx1 = "$ARGV[1].idx" . $idFile;

        if ( open( IDX1_FILE, ">$nomdefichier_idx1" ) ) {
            print STDERR
"\t INFO LOT-$idFile: Création du fichier index : $nomdefichier_idx1\n";
            $NbLots++;
        }
        else {
            print STDERR
"\t ERROR LOT-$idFile: Impossible de créer le fichier index idx1 \"$nomdefichier_idx1\" : $!\n";
            exit 1;
        }

        $Exist_Fich_idx = 1;
    }

    $NbDocParLot++;

    #### PREPARATION DU FICHIER INDEX
    $appname = $ligneCSV[0];
    $ligneIdx1[0] = $ligneCSV[0]
      ;    # Identifiant document (type de document) - Nom de l'application
    $ligneIdx1[1] = $idldoc;    # Identifiant unique de document - Id éditique
    $ligneIdx1[3] = $NbDocParLot;     # Numéro du document dans le pdf
    $ligneIdx1[4] = $ligneCSV[14];    # Code postal destinataire
    $ligneIdx1[5] = ( $ligneCSV[8] =~ s/^"|"$//g );    # Ville destinateur
    print $ligneIdx1[5], "\n";
    $ligneIdx1[6] = $ligneCSV[6];     # Numéro contrat collectif
    $ligneIdx1[7] = $ligneCSV[7];     # Nom destinataire
    $ligneIdx1[8] = $ligneCSV[9];     # Numéro de section
    $ligneIdx1[9] = $ligneCSV[13];    # Date d’édition
    $ligneIdx1[10] = substr( config_read()->{'EDTK_TYPE_ENV'}, 0, 1 )
      ;                               # Type d'environnement sur une lettre
    $ligneIdx1[13] = $ligneCSV[5];    # Numéro de contrat individuel
    $ligneIdx1[15] = $ligneCSV[11];   # Date de naissance
    $ligneIdx1[17] = $ligneCSV[10];   # N° groupe assuré
    $ligneIdx1[19] = $ligneCSV[4];    # N° adhérent (ID_MNT)
    $ligneIdx1[21] = $ligneCSV[2];    # Entité juridique
    $ligneIdx1[22] =
      "$nomdebase_fichier_pdf" . "$idFile" . ".pdf";    #Le nom de fichier PDF
    $ligneIdx1[25] = $ligneCSV[3];                      # Domaine métier
    $ligneIdx1[27] = hostname();

    # my $numPageParDoc = 0;
    if ( $ligneCSV[1] =~ m/^\d+$/ ) {                   # si c'est numérique
        for ( my $i = $ligneCSV[1] ; $i-- ; ) {    # Pour le nombre de pages
            $NbPageParLot++;
            $ligneIdx1[2] = $NbPageParLot;         # Numéro de page
                                                   # $numPageParDoc++;
            print( IDX1_FILE
"$ligneIdx1[0];$ligneIdx1[1];$ligneIdx1[2];$ligneIdx1[3];$ligneIdx1[4];\"$ligneIdx1[5]\";$ligneIdx1[6];\"$ligneIdx1[7]\";$ligneIdx1[8];$ligneIdx1[9];$ligneIdx1[10];;\"\";\"$ligneIdx1[13]\";\"\";\"$ligneIdx1[15]\";\"\";\"$ligneIdx1[17]\";\"\";\"$ligneIdx1[19]\";\"\";$ligneIdx1[21];$ligneIdx1[22];\"\";;$ligneIdx1[25];;$ligneIdx1[27];;;\n"
            );                                     # Édite la ligne dans l'idx1
        }
    }
    else {
        print STDERR
"\t ERROR : Merci de vérifier la structure de la ligne $NumLigneCSV du fichier CSV -"
          . $ARGV[1] . "-\n";
        exit 1;
    }

    # $esp_adh = $type_doc{$appname}[1];

    if ( $NbPageParLot >= $tailleMaxLot && $Exist_Fich_idx eq 1 ) {

        close(IDX1_FILE);

        # Création des lots
        DecoupPDF();

        #### INITIALISATION DES VARIABLES
        $lastEndNumPage = $NbPageParLot + $lastEndNumPage;
        $NbPageParLot   = 0;
        $NbDocParLot    = 0;
        $idldoc++;
        $idFile++;
        $Exist_Fich_idx = 0;
    }
}

if ( $Exist_Fich_idx eq 1 ) {

    close(IDX1_FILE);

    # Création des lots
    DecoupPDF();
}

close(CSV_FILE);

# print STDERR "INFO : dump LES LOTS ". Dumper(%lot) ."\n";

$idldoc++;
my $Nom_Sous_Retp = $nomFichier . "_" . $date_archivage . "_" . $idldoc;
my $rep_sortie    = $rep_origine . "/" . $Nom_Sous_Retp;

mkdir($rep_sortie)
  or die "ERROR: Cannot create directory \"$rep_sortie\": $!\n";

### ON CHANGE LE REPERTOIRE DE TRAVAIL POUR EVITER LES PROBLEMES AVEC EDMSS_PROCESS
chdir($rep_sortie)
  or die "ERROR: Cannot change current directory to \"$rep_sortie\": $!\n";

my @running = ();
my $i       = 1;
my $k       = 1;    #compteur pour parcourir les lots

while ( $k <= $NbLots ) {

    @running = threads->list(threads::running);

# print STDERR "LOOP $i\n";
# print STDERR "  - BEGIN LOOP >> NB running threads = ".(scalar @running)."\n";

    if ( scalar @running < $limit_process ) {

        # incrémente le compteur
        $NbJOBs++;

        print STDERR "INFO LOT-$lot{$k}[0]: Creation du job $NbJOBs\n";

        my $thread = threads->create(
            "ThreadEDMS",
            (
                $NbJOBs,     $lot{$k}[0], $lot{$k}[1], $lot{$k}[2],
                $lot{$k}[3], $lot{$k}[4], $lot{$k}[5], $lot{$k}[6],
                $lot{$k}[7], \$cpt
            )
        );

        push( @processes, $thread );

        my $tid = $thread->tid;
        print STDERR "INFO LOT-$lot{$k}[0]: starting thread $tid\n";
        $k++;
    }

    @running = threads->list(threads::running);

# print STDERR "  - AFTER STARTING >> NB running Threads = ".(scalar @running)."\n";

    foreach my $thr (@processes) {
        if ( $thr->is_running() ) {
            my $tid = $thr->tid;

            # print STDERR "  - Thread $tid running\n";
        }
        elsif ( $thr->is_joinable() ) {
            my $tid = $thr->tid;
            $thr->join;
            print STDERR "  - Results for thread $tid:\n";
            print STDERR "  - Thread $tid has been joined\n";
        }
    }

    @running = threads->list(threads::running);

    # print STDERR "  - END LOOP >> NB Threads = ".(scalar @running)."\n";
    $i++;
}

print STDERR "\n JOINING pending threads\n";

while ( scalar @running != 0 ) {
    foreach my $thr (@processes) {

        # $thr->join if ($thr->is_joinable());
        if ( $thr->is_joinable() ) {
            my $tid = $thr->tid;
            $thr->join;
            print STDERR "  - Results for thread $tid:\n";
            print STDERR "  - Thread $tid has been joined\n";
        }
    }
    @running = threads->list(threads::running);
}

print STDERR "NB started threads = " . ( scalar @processes ) . "\n";

# foreach my $key (sort keys %lot) {

# # incrémente le compteur
# $NbJOBs++;

# print "INFO LOT-$lot{$key}[0]: Creation du job $NbJOBs\n";

# push (@processes, threads->create("ThreadEDMS", (
# $NbJOBs,
# $lot{$key}[0],
# $lot{$key}[1],
# $lot{$key}[2],
# $lot{$key}[3],
# $lot{$key}[4],
# $lot{$key}[5],
# $lot{$key}[6],
# $lot{$key}[7],
# \$cpt
# ))
# );

# }

# foreach (@processes) {
# # on attends la fin du job pour sortir
# $_->join();
# }

### ON RETOURNE VERS LE REPERTOIRE DE TRAVAIL INITIAL POUR CONTINUE LE RESTE DE TRAITEMENT
chdir($rep_origine)
  or die "ERROR: Cannot change current directory to \"$rep_origine\": $!\n";

print STDERR "INFO : $cpt jobs finis, sur les $NbJOBs prévus\n";

if ( $cpt eq $NbJOBs ) {

    # if ( $esp_adh eq 1 ) {
        ## TRANSFERT DES FICHIERS PDF VERS LE DMZ
        # transfertSCP($rep_sortie);

        # print STDERR "transfertSCP = $rep_sortie\n";
    # }

    foreach my $key ( sort keys %lot ) {
        # if ( $lot{$key}[4] eq 1 ) {
            ### INSERTION DES META-DONNEES DANS LA BASE MYSQL DE DMZ
            # my $Newappname = $lot{$key}[5];
            # $Newappname =~ s/[-\.]/_/g;

            # my $FichIndexDB =
              # $rep_sortie . "/" . $Newappname . "_" . $lot{$key}[1] . ".idx";
            # InsertDBdata( $FichIndexDB, $Nom_Sous_Retp, $lot{$key}[0] );
        # }

        ## TRACKING
        my $TRK = oEdtk::Tracking->new(
            '',
            user   => 'DocsRenouv',
            entity => oe_corporation_set(),
            keys   => [ 'TYPE_DOC', 'NOM_FICH', 'NUM_LOT', 'NB_DOC', 'NB_PAGE' ]
        );

        $TRK->track(
            'Doc', 1, "$lot{$key}[5]",
            "$nomdebase_fichier_pdf/$nomdebase_fichier_csv",
            "Lot $lot{$key}[0]",
            $lot{$key}[6], $lot{$key}[7]
        );
        print STDERR
"\t INFO LOT-$lot{$key}[0]: Track command => $lot{$key}[5],$nomdebase_fichier_pdf/$nomdebase_fichier_csv,Lot $lot{$key}[0],$lot{$key}[6],$lot{$key}[7]\n";
    }

}
else {

    print STDERR
"\t ERROR : Echec du traitement EDMSS_PROCESS. $cpt jobs finis, sur les $NbJOBs prévus\n";
    exit 1;
}

##########################################################################################
################################### LES FONCTIONS ########################################
##########################################################################################
sub DecoupPDF {
    ### DECOUPAGE DU FICHIER PDF EN PLUSIEURS LOTS
    my $lastNumPage = $lastEndNumPage + $NbPageParLot - 1;

# my $commandPdftk = "pdftk ".$ARGV[0]." cat $lastEndNumPage-$lastNumPage output ".$ARGV[0]."$idFile".".pdf";
    my $commandPdftk =
        "pdfjam "
      . $ARGV[0]
      . " $lastEndNumPage-$lastNumPage --outfile "
      . $ARGV[0]
      . "$idFile" . ".pdf";
    print STDERR "\t INFO LOT-$idFile: Pdfjam command => $commandPdftk\n";
    if ( !system("$commandPdftk") ) {
        print STDERR
"\t INFO LOT-$idFile: Découpage du fichier PDF - $ARGV[0] - avec succès\n";
    }
    else {
        print STDERR
"\t ERROR LOT-$idFile: Echec de découpage du fichier PDF - $ARGV[0] -\n";
        exit 1;
    }

    ### ALLIMENTATION DE LA TABLE DE HASHAGE DES LOTS
    my $nomdefichier_pdf1 = $ARGV[0] . "$idFile.pdf";

    push @{ $lot{$NbLots} }, $idFile;
    push @{ $lot{$NbLots} }, $idldoc;
    push @{ $lot{$NbLots} }, $nomdefichier_idx1;
    push @{ $lot{$NbLots} }, $nomdefichier_pdf1;
    push @{ $lot{$NbLots} }, $esp_adh;
    push @{ $lot{$NbLots} }, $appname;
    push @{ $lot{$NbLots} }, $NbDocParLot;
    push @{ $lot{$NbLots} }, $NbPageParLot;
}

sub ThreadEDMS($$$$$$$$$$) {
    ### C'est une fonction pour threader le traitement de découpage et concatenantion des fichiers
    ### au lieu d'attendre la fin d'EDMSS_process pour passer au lot suivant, on lance plusieurs EDMS_process pour paralleliser le traitement

    my $index               = shift;
    my $THidFile            = shift;
    my $THidldoc            = shift;
    my $THnomdefichier_idx1 = shift;
    my $THnomdefichier_pdf1 = shift;
    my $THesp_adh           = shift;
    my $THappname           = shift;
    my $THNbDocParLot       = shift;
    my $THNbPageParLot      = shift;
    my $cpt_ref             = shift;

    print STDERR
      "##############  Process lot $THidFile #################### \n";

    EDMS_process(
        $THappname, $THidldoc,
        $rep_origine . "/" . $THnomdefichier_pdf1,
        $rep_origine . "/" . $THnomdefichier_idx1, $rep_sortie
    );

    print "INFO LOT-$THidFile: Le job $index vient de se terminer\n";

    # incrémente le nombre de jobs finis
    $$cpt_ref++;

    return;
}

sub DBconnectMySQL {

    # Connection à la base de données mysql
    my $bd =
      config_read()->{'EDTK_WEBADH_BD_BASE'};    # Le nom de la base de données
    my $serveur = config_read()->{'EDTK_WEBADH_HOST'}
      ;    # Le serveur du BD : Il est possible de mettre une adresse IP
    my $identifiant = config_read()->{'EDTK_WEBADH_BD_LOGIN'}
      ;    # identifiant de la base de données
    my $motdepasse = config_read()->{'EDTK_WEBADH_BD_PASSWORD'}
      ;    # Le mot de passe de la base de données
    my $port = config_read()->{'EDTK_WEBADH_BD_PORT'}
      ;    # Si vous ne savez pas, ne rien mettre 3306 - 8443

    print STDERR
      "\t INFO : Tentative de connexion à la base de données $bd\n";
    my $dbh = DBI->connect(
        "DBI:mysql:database=$bd;host=$serveur;port=$port",
        $identifiant,
        $motdepasse,
        {
            mysql_auto_reconnect => 1,
            RaiseError           => 1,
            AutoCommit           => 0
        }
      )
      or die
"ERROR : Connection impossible à la base de données $bd !\n $! \n $@\n$DBI::errstr\n";

    print STDERR "\t INFO : Connexion établie à la base de données $bd\n";

    return $dbh;
}

sub DBselectTypeDoc {

    # requete de selection de la liste de types documents pour récuperer l'id
    my %liste_doc;

    # Connection à la BD MySQL
    my $dbh = DBconnectMySQL();

    my $requeteSelect = <<"SQL";
    SELECT * FROM webadh_type_doc
SQL

    my $prep = $dbh->prepare($requeteSelect) or die $dbh->errstr;
    $prep->execute() or die "ERROR : Echec requête : $requeteSelect\n";

# print STDERR "Il y a ", $prep->rows, " lignes dans la table <webadh_type_doc>\n";

    while ( my $les_types_doc = $prep->fetchrow_hashref ) {
        push @{ $liste_doc{ $les_types_doc->{type_doc} } },
          $les_types_doc->{id};
        push @{ $liste_doc{ $les_types_doc->{type_doc} } },
          $les_types_doc->{esp_adh};
    }
    $prep->finish();

    # Fermeture de la connexion MySQL
    $dbh->disconnect();

    return %liste_doc;
}

sub InsertDBdata {
    ## RECUPERATION DU FICHIER INDEX GENERER PAR EDMSS_PROCESS  ET INSERTION DES DONNEES DANS LA BASE MYSQL DE DMZ
    my ( $Fichier, $Repertoire, $NumLot ) = @_;
    my @ligne      = ();
    my $id_TypeDOC = $type_doc{$appname}[0];
    my $serveur    = config_read()->{'EDTK_WEBADH_HOST'};
    my $bd =
      config_read()->{'EDTK_WEBADH_BD_BASE'};    # Le nom de la base de données

    if ( open( IDXDB_FILE, "<$Fichier" ) ) {
        print STDERR
"\t INFO LOT-$NumLot: Insertion en cours dans la BD MySQL -$bd- du serveur -$serveur-\n";
    }
    else {
        print STDERR
"\t ERROR LOT-$NumLot: Impossible d'ouvrir le fichier index IDX \"$Fichier\" : $!\n";
        exit 1;
    }

    # Connection à la BD MySQL
    my $dbh = DBconnectMySQL();

    # Lecture ligne par ligne
    while (<IDXDB_FILE>) {
        @ligne = split( "\n", $_ );
        @ligne = split( ';',  $ligne[0] );
        if ( defined( $ligne[1] ) ) {
            my $file_path = $RepPDF_DMZ . $Repertoire . "/" . $ligne[14];

            DBInsertDOC(
                $dbh,       $id_TypeDOC,     $ligne[12],
                $ligne[4],  $date_archivage, $ligne[3],
                $file_path, $ligne[5],       $NumLot
            );

# print STDERR "DBInsertDOC = $id_TypeDOC,$ligne[12],$ligne[4],$date_archivage,$ligne[3],$file_path,$ligne[5]\n";
        }
    }

    $dbh->commit
      or die
      "ERROR  LOT-$NumLot: Commit Impossible sur la BD $bd => $DBI::errstr \n";

    # Fermeture de la connexion MySQL
    $dbh->disconnect();

    close(IDXDB_FILE);

    print STDERR "\t INFO LOT-$NumLot: Fin d'insertion dans la BD -$bd-\n";
}

sub DBInsertDOC {

    # requete d'insertion dans la BD
    my (
        $dbh,      $idType, $numABA, $dtEdition, $dtArch,
        $idUnique, $file,   $numCTR, $NumLot
    ) = @_;

    my $requeteINSERT = <<"SQL";
    INSERT INTO webadh_documents ( id_type, num_aba, date_extract, date_archivage, id_doc, file_path, num_contrat )
	VALUES ( ?, ?, ?, ?, ?, ?, ? )
SQL

    my $prep = $dbh->prepare($requeteINSERT);
    $prep->execute( $idType, $numABA, $dtEdition, $dtArch, $idUnique, $file,
        $numCTR )
      or die
      "ERROR LOT-$NumLot: Echec requête : $requeteINSERT  : $DBI::errstr \n";
    $prep->finish();
}

sub transfertSCP {

    # transfert des fichiers PDF vers le file Systeme

    my $source = shift;

    # print "$source\n";
    # print "$RepPDF_DMZ\n";

    my $ThisServeur = config_read()->{'EDTK_TYPE_ENV'};

    # print "$ThisServeur\n";

    if ( $ThisServeur ne 'Test' && $ThisServeur ne 'Développement' ) {

        # Connection à la base de données mysql
        my $serveur = config_read()->{'EDTK_WEBADH_HOST'}
          ;    # Le nom du serveur: Il est possible de mettre une adresse IP
        my $identifiant =
          config_read()->{'EDTK_WEBADH_LOGIN'};    # identifiant du serveur

        print STDERR
"\t INFO : Envoi en cours des fichiers vers le file syteme DMZ -$serveur-\n";

        my $scp = "scp -rpC $source $identifiant\@$serveur:$RepPDF_DMZ";
        if ( !system("$scp") ) {
            print STDERR
"\t INFO : Fin d'Envoi des fichiers vers le file syteme DMZ -$serveur- avec succès\n";
        }
        else {
            print STDERR
"\t ERROR : Echec d'envoi des fichiers vers le file syteme DMZ -$serveur-\n";
            exit 1;
        }
    }
}
