#! /usr/bin/perl
use warnings;
use strict;
use feature qw( switch );
use oEdtk::Config qw( config_read );
use oEdtk::Tracking;
use Cwd qw ( abs_path );
use Readonly;
use List::Util qw( sum );
use Data::Dumper qw( Dumper );
use Sys::Hostname qw ( hostname );
use oEdtk::Main qw( oe_app_usage oe_corporation_set );
use oEUser::Lib qw( user_get_aneto_user );
use oEdtk::libXls
  qw( prod_Xls_Col_Init prod_Xls_Init prod_Xls_Insert_Val prod_Xls_Edit_Ligne );
use oEdtk::Infinite qw( is_flux is_entete is_ligne id_ligne record_values );
use oEdtk::Restitution qw( restitution_value );
use charnames ":full",
  ":alias" => {
    e_ACUTE => "LATIN SMALL LETTER E WITH ACUTE",
    E_ACUTE => "LATIN CAPITAL LETTER E WITH ACUTE",
  };
use oEdtk::Utils;

Readonly my $DEBUG => 0 && hostname() eq 'debian';
Readonly my @TRACKING_FIELDS => (qw( ID_DEST NUMADH ID_EMET ETAPES ));

# What we need to extract from the flow
Readonly my $EXPECTED_VALUES => {
    flush => {
        EL => 1,
    },
    ligne => {
        EL => {
            contrat_individuel    => [ 'X', 873,  15 ],
            code_ordonnancier     => [ 'X', 1156, 5 ],
            numero_telephone      => [ 'X', 1329, 50 ],
            frequence             => [ 'X', 956,  2 ],
            entite_rattachement   => [ 'X', 1270, 9 ],
            date_creation_dossier => [ 'X', 1022, 8 ],
            EL_adresse_souscripteur =>
              \&oEdtk::Infinite::EL_adresse_souscripteur,
            EL_restitution => \&oEdtk::Infinite::EL_restitution,
        },
        EM => {
            date_exigibilite => [ 'X', 169, 8, \&oEdtk::Infinite::sub_list ],
            solde => [ 'S9(12)V99', 263, 12, 2, \&oEdtk::Infinite::sub_list ],
        },
        I7 => {
            code_etape => [ 'X', 169, 5, \&oEdtk::Infinite::sub_list ],
        },
    },
};

Readonly my @STEPS => (
    [ 1, 'REL',   'Relance CH' ],
    [ 2, 'SMS',   'SMS' ],
    [ 3, 'MED',   'Mise en demeure' ],
    [ 4, 'BLCDR', 'Blocage droits' ],
    [ 5, 'APPEL', 'Relance tel' ],
    [ 6, 'RESIL', "R\N{e_ACUTE}siliation ct" ],
);
Readonly my $STEP_NUMBER => { map { $$_[1] => $$_[0] } @STEPS };
Readonly my $STEP_NAME   => { map { $$_[0] => $$_[1] } @STEPS };

# 'Style': A/N Alpha/Number L/C/R Left/Center/Right; then size; Ac Alpha center wrap
# 'Path': of keys to follow in hash; code ref should be final
Readonly my @RESTITUTION_RULES => (

# 0: Title                                 # 1: Style  # 2: Path  # 3: Flag_quiet_if_absent (don't warn)
    [
        "ADH\N{E_ACUTE}RENT", 'AL30',
        [ "EL_restitution", "civilite_nom_prenom" ]
    ],
    [ "NUMADH", 'AL10', ["contrat_individuel"] ],
    [ "ADRESSE", 'AL40', [ "EL_adresse_souscripteur", \&shift_adresse ] ]
    ,    # TODO complement d'adresse
    [ "NUMTEL",                       'AL15', ["numero_telephone"] ],
    [ "SOLDE D\N{E_ACUTE}BITEUR",     'NR10', [ "solde", \&total_soldes ] ],
    [ "DATE D'EXIGIBILIT\N{E_ACUTE}", 'AC20', [ "date_creation_dossier", ] ],
    [
        "DATE DE CR\N{E_ACUTE}ATION DU DOSSIER",
        'AC20',
        [ "date_exigibilite", \&premiere_date ]
    ],
    [
        "\N{E_ACUTE}TAPE\n${$STEPS[0]}[2]", 'AC10',
        [ 'code_etape', \&coche_etape_1 ]
    ],
    [
        "\N{E_ACUTE}TAPE\n${$STEPS[1]}[2]", 'AC10',
        [ 'code_etape', \&coche_etape_2 ]
    ],
    [
        "\N{E_ACUTE}TAPE\n${$STEPS[2]}[2]", 'AC10',
        [ 'code_etape', \&coche_etape_3 ]
    ],
    [
        "\N{E_ACUTE}TAPE\n${$STEPS[3]}[2]", 'AC10',
        [ 'code_etape', \&coche_etape_4 ]
    ],
    [
        "\N{E_ACUTE}TAPE\n${$STEPS[4]}[2]", 'AC10',
        [ 'code_etape', \&coche_etape_5 ]
    ],
    [
        "\N{E_ACUTE}TAPE\n${$STEPS[5]}[2]", 'AC10',
        [ 'code_etape', \&coche_etape_6 ]
    ],
    [ "TYPE DE CONTRAT", 'AL20', [ "code_ordonnancier", \&type_de_contrat ] ],
    [ "FR\N{E_ACUTE}QUENCE", 'AL12', ["frequence"] ],
);

Readonly my $CODE_ORDONNANCIER_TO_TYPE_CONTRAT => {
    'ACS'   => "Sant\N{e_ACUTE} ACS",
    'MNTPR' => "MNT Pr\N{e_ACUTE}voyance",
    'MNTRC' => "MNT Sant\N{e_ACUTE}",
    'MUTPR' => "MUT Pr\N{e_ACUTE}voyance",
    'MUTRC' => "MUT Sant\N{e_ACUTE}",
};

my ($TRK);

sub alert {
    my ( $flowFile, $lineNumber, $msg ) = @_;
    die "flowFile='$flowFile' lineNumber='$lineNumber' $msg\n";
}

sub shift_adresse {
    my ($l) = @_;
    my @res = @{$l};
    shift @res;
    return join( "\n", @res );    # TODO refacto with Restitution.pm?
}

sub total_soldes {
    my ($l) = @_;
    return ( ref $l ) ? sum( @{$l} ) : $l;
}

sub premiere_date {
    my ($l) = @_;
    return ( ref $l ) ? $$l[0] : $l;
}

# not a very clean/efficient algorithm...
sub coche_etape {
    my ( $step_number, $l ) = @_;
    $l = [$l] unless ref $l;
    my $step_name = $STEP_NAME->{$step_number};
    foreach my $step ( @{$l} ) {
        return $step if $step eq $step_name;
    }
    return '';
}
sub coche_etape_1 { my ($l) = @_; return coche_etape( 1, $l ); }
sub coche_etape_2 { my ($l) = @_; return coche_etape( 2, $l ); }
sub coche_etape_3 { my ($l) = @_; return coche_etape( 3, $l ); }
sub coche_etape_4 { my ($l) = @_; return coche_etape( 4, $l ); }
sub coche_etape_5 { my ($l) = @_; return coche_etape( 5, $l ); }
sub coche_etape_6 { my ($l) = @_; return coche_etape( 6, $l ); }

sub type_de_contrat {
    my ($code_ordonnancier) = @_;
    return $CODE_ORDONNANCIER_TO_TYPE_CONTRAT->{$code_ordonnancier}
      if exists $CODE_ORDONNANCIER_TO_TYPE_CONTRAT->{$code_ordonnancier};
    warn "Unkown type de contrat for code ordonnancier '$code_ordonnancier'";
    return $code_ordonnancier;
}

sub flatten_hash {
    my ($h) = @_;
    my @res = ();
    foreach my $key ( sort keys %{$h} ) {
        my $val = $h->{$key};
        my $ref = ref $val;
        given ($ref) {
            when ( $ref eq "" ) { push @res, "$key: $val"; }
            when ( $ref eq "ARRAY" ) {
                push @res, map { "$key: $_" } @{$val};
            }
            when ( $ref eq "HASH" ) {
                push @res, map { "$key: $_" } flatten_hash($val);
            }
        }
    }
    return @res;
}

sub init_XLS {
    prod_Xls_Col_Init( map { $$_[1] } @RESTITUTION_RULES ); # must be done first
    prod_Xls_Init( 'M.N.T.', "Restitution pr\N{e_ACUTE}contentieux" );
    prod_Xls_Insert_Val( $$_[0] )
      foreach @RESTITUTION_RULES;                           # add column titles
    return prod_Xls_Edit_Ligne( 'T2', 'HEAD' );    #  line style / table header
}

sub edit_ligne_XLS {
    my ($h) = @_;
    prod_Xls_Insert_Val( restitution_value( $h, $$_[2], $$_[3] ) )
      foreach @RESTITUTION_RULES;
    return prod_Xls_Edit_Ligne();
}

sub track_begin {
    my ( $trackingJob, $user ) = @_;
    return oEdtk::Tracking->new(
        $trackingJob,
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => \@TRACKING_FIELDS
    );
}

sub track {
    my ($l) = @_;
    warn "TRACKING: " . join( "\x20", @{$l} ) if $DEBUG;
    return $TRK->track( 'D', 1, @{$l} );    # "D" as in "Doc"
}

sub track_end {
    return $TRK->track( 'W', 1, '', '', '', '', 'Fin de traitement' )
      ;                                     # "W" as in "Warning"
}

sub init {
    my ( $flowFile, $trackingJob ) = @_;
    return alert( $flowFile, -1, 'file does not exist' )  unless -e $flowFile;
    return alert( $flowFile, -1, 'file is not readable' ) unless -r $flowFile;
    return alert( $flowFile, -1, 'file does not exist' )  unless -e $flowFile;
    return alert( $flowFile, -1, 'file is not readable' ) unless -r $flowFile;
    unless ( defined $trackingJob ) {
        $trackingJob = abs_path($flowFile);
        warn "Using trackingJob='$trackingJob'";
    }
    my $user = user_get_aneto_user($flowFile);
    $TRK = track_begin( $trackingJob, $user );
    return init_XLS();
}

sub treat_line {
    my ( $h, $e, $lineRef, $flowFile, $lineNumber ) = @_;
    my $id = id_ligne($lineRef);
    die "Unexpected line id '$id'"
      unless exists $EXPECTED_VALUES->{'ligne'}->{$id};
    $h = flush_hash( $h, $e, $flowFile, $lineNumber )
      if exists $EXPECTED_VALUES->{'flush'}->{$id};
    my @errors =
      record_values( $h, $EXPECTED_VALUES->{'ligne'}->{$id}, $lineRef );
    die Dumper( \@errors ) if scalar @errors;
    return $h;
}

sub flush_hash {
    my ( $h, $e, $flowFile, $lineNumber ) = @_;
    if ( scalar keys %{$h} ) {
        edit_ligne_XLS($h);
        track(
            [
                $h->{'EL_restitution'}->{'civilite_nom_prenom'},    # ID_DEST
                $h->{'contrat_individuel'},                         # NUMADH
                $h->{'entite_rattachement'},                        # ID_EMET
                (
                    ( ref $h->{'code_action'} )
                    ? join( '/', @{ $h->{'code_action'} } )
                    : $h->{'code_action'}
                ),                                                  # ETAPE
            ]
        );
    }
    return {};                                                      # reset h
}

sub treat_input {
    my ( $h, $e, $lineRef, $flowFile, $lineNumber ) = @_;

    # Should be useless if the input received was not garbage
    oEdtk::Utils::kill_non_latin_1_9_chars_in_place( \$lineRef );
    given ($lineRef) {
        when ( is_flux($lineRef) )   { }
        when ( is_entete($lineRef) ) { }
        when ( is_ligne($lineRef) ) {
            $h = treat_line( $h, $e, $lineRef, $flowFile, $lineNumber );
        }
        default {
            alert( $flowFile, $lineNumber,
                "cannot guess line type for '$$lineRef'" );
        }
    }
    return ( $h, $e );
}

sub treat_flow {
    my ($flowFile) = @_;
    my ( $h, $e ) = ( {}, {} )
      ; # hashes for flow/flux / entetes (shared between several flux); list of tags for debug
    open my $io, '<', $flowFile or die "$flowFile: $!";
    ( $h, $e ) = treat_input( $h, $e, \$_, $flowFile, $. ) while <$io>;
    close($io) || 1;
    flush_hash( $h, $e, $flowFile, $. );    # do not forget the last record!
    track_end();
    return;
}

sub main {
    my ($flowFile) = @_;
    oe_app_usage() unless defined $flowFile;
    init($flowFile);
    treat_flow($flowFile);
    return 0;
}

exit main(@ARGV);
