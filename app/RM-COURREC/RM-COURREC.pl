use utf8;
use strict;
use warnings;

use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Data::Dumper;
use feature "switch";

my $TRK;

sub edit_text {
    my ( $data, $doc ) = @_;
    my $variables = $data;

    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
      localtime(time);
    $mon  += 1;
    $year += 1900;
    my $day;
    for ($wday) {
        when (0) { $day = 'dimanche'; last }
        when (1) { $day = 'lundi';    last }
        when (2) { $day = 'mardi';    last }
        when (3) { $day = 'mercredi'; last }
        when (4) { $day = 'jeudi';    last }
        when (5) { $day = 'vendredi'; last }
        when (6) { $day = 'samedi';   last }
    }
    my $mounth;
    for ($mon) {
        when (1)  { $mounth = 'janvier';   last }
        when (2)  { $mounth = 'février';  last }
        when (3)  { $mounth = 'mars';      last }
        when (4)  { $mounth = 'avril';     last }
        when (5)  { $mounth = 'mai';       last }
        when (6)  { $mounth = 'juin';      last }
        when (7)  { $mounth = 'juillet';   last }
        when (8)  { $mounth = 'août';     last }
        when (9)  { $mounth = 'septembre'; last }
        when (10) { $mounth = 'octobre';   last }
        when (11) { $mounth = 'novembre';  last }
        when (12) { $mounth = 'décembre'; last }
    }
    my $dateTitre = "Le $day $mday $mounth $year";
    $$doc->append( 'datedujour', $dateTitre );

    my $agence;
    my $nom;
    my $idreclamation;
    my $prenom;
    my @tADRDEST = ();
    $$doc->append( 'identifiantCourrier', $variables->{'identifiantCourrier'} );
    $$doc->append( 'civilite',            $variables->{'civilite'} );
    $$doc->append( 'nomReclamant',        $variables->{'nom_reclamant'} );
    $nom = $variables->{'nom_reclamant'};
    $prenom = $variables->{'prenom_reclamant'} || ' ';
    $$doc->append( 'prenomReclamant', $prenom );

#	push(@tADRDEST, '$variables->{"civilite"} $nom $variables->{"prenom_reclamant"}');
    my $adresseLine1;
    if ( $variables->{"civilite"} ne 'Madame, Monsieur' ) {
        $adresseLine1 = $variables->{"civilite"};
    }
    push( @tADRDEST, $adresseLine1 . " " . $nom . " " . $prenom );
    push( @tADRDEST, $variables->{"adresse1"} );
    push( @tADRDEST, $variables->{"adresse2"} );
    push( @tADRDEST, $variables->{'adresse3'} );
    push( @tADRDEST, $variables->{"adresse4"} );
    push( @tADRDEST, $variables->{"adresse5"} );
    @tADRDEST = user_cleanup_addr(@tADRDEST);
    $$doc->append_table( 'xADRLN', @tADRDEST );
    $$doc->append('editAdresse');
    $$doc->append( 'dateReclamation', $variables->{'date_reclamation'} );
    $$doc->append( 'idReclamation',   $variables->{'id_reclamation'} );
    $idreclamation = $variables->{'id_reclamation'};
    $variables->{'signataire'} =~ s/\x{e2}\x{82}\x{ac}/ _/;
    $$doc->append( 'nomExecuteur',  $variables->{'signataire'} );
    $$doc->append( 'paragrapheI',   $variables->{'paragraphe_1'} );
    $$doc->append( 'paragrapheII',  $variables->{'paragraphe_2'} );
    $$doc->append( 'paragrapheIII', $variables->{'paragraphe_3'} );
    $$doc->append( 'paragrapheIV',  $variables->{'paragraphe_4'} );
    $$doc->append( 'paragrapheV',   $variables->{'paragraphe_5'} );
    $$doc->append( 'numCont',       $variables->{'numeroContrat'} | " " );
    $$doc->append( 'numAdh',        $variables->{'numeroAdherent'} | " " );

    my $identifiantCourrier = $variables->{'identifiantCourrier'};

# Suivant le type de courrier. Tombe en erreur si l'identifiant ne correspond à aucun type de courrier connu.
  SWITCH: {
        $identifiantCourrier eq 'R-C1'
          && do { $$doc->append("RCI"); last SWITCH; }; # Courrier Identification Accomplie - AR Courrier
        $identifiantCourrier eq 'R-C2'
          && do { $$doc->append("RCII"); last SWITCH; }; # Courrier Identification Accomplie - AR Courrier avec complément d'information
        $identifiantCourrier eq 'R-C3'
          && do { $$doc->append("RCIII"); last SWITCH; }; # Courrier Identification Accomplie - AR Courrier avec réponse finale
        $identifiantCourrier eq 'R-C4'
          && do { $$doc->append("RCIV"); last SWITCH; }; # Courrier Identification Accomplie - AR Courrier avec passage en médiation
        $identifiantCourrier eq 'R-C5'
          && do { $$doc->append("RCV"); last SWITCH; }; # Courrier Identification Accomplie - AR Courrier avec intervallaire de délai
        $identifiantCourrier eq 'R-C6'
          && do { $$doc->append("RCVI"); last SWITCH; }; # Courrier Identification Accomplie - AR Courrier en cours de traitement
    }
    $TRK->track( 'Doc', 1, $agence, $nom, $idreclamation );
}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        keys   => [ 'AGENCE', 'NOMDEST', 'IDRECLAMATION' ]
    );

    oe_new_job('--index');

    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'RECL' );
    $doc->append( 'xSOURCE', 'coheris' );
    $doc->append( 'xOWNER',  'coheris' );
    my $data = xml_open( \*IN );
    print Dumper($data);
    edit_text( $data, \$doc );
    oe_print_to_output_file("$doc");
    oe_compo_link();
    return 0;
}

exit main(@ARGV);
