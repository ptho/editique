#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;

my ( $TRK, $DOC, $GED, $VALS );
my $TYPECOUR;
my $ACTEGEST;
my @DADDR;
my $AGENCE;
my $CIV;
my $CIVILITE;
my $NOMDEST;
my $NUMCTR;
my $NUMABA;
my $xNOMDEST;
my $GESTIONNAIRE;
my $xIDDEST;
my $EMETTEUR;
my $CODCENTRE;
my $LIBCENTRE;
my $OPT_GED;
my $DtEDITION;
my $DtCOUR;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys =>
          [ 'NUMCTR', 'NUMABA', 'EMETTEUR', 'GESTIONNAIRE', 'DATECOURRIER' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # $E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # $E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    # $E7->debug();
    $E7->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();

    my $LISTE_BENEF_RAD = oEdtk::TexDoc->new();
    my $DELAIS_STAGE_AJ = oEdtk::TexDoc->new();

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'Z1' => $Z1
    );

    $DOC->append( 'xTYPDOC', 'CIRE' );    ### Type de document

    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {

        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n";

            $DOC->append( 'DEBUT', '' );
            $NOMDEST =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            #####GESTION DE LA CIVILITE ENTRE TIERS ET ADHERENT#########
            my $civDest = $VALS->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );

            # $CIVILITE = $VALS->{'LICCODC'};
            $CIVILITE = $VALS->{'PENOCOD'};
            if ( $CIVILITE eq 'M' ) {
                $DOC->append( 'CIVILITE', 'Cher adhérent' );
            }
            elsif ( $CIVILITE eq 'MME' || $CIVILITE eq 'MLLE' ) {
                $DOC->append( 'CIVILITE', 'Chère adhérente' );
            }
            else {
                $DOC->append( 'CIVILITE', 'chère adhérente, cher adhérent' );
            }

            $EMETTEUR = $VALS->{'ODERCOD'};
            if ( $EMETTEUR eq '' ) {
                $CIV      = 0;
                $EMETTEUR = "ER";
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }
            else {
                $CIV = 1;
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }

            $DOC->append( 'CIV', $CIV );

            $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
            $DOC->append( 'xNOMDEST', $xNOMDEST );

            ################CENTRE DE GESTION###############################
            $CODCENTRE = $VALS->{'ODCGCOD'};
            if ( $CODCENTRE eq 'MNT-RC' ) {
                $LIBCENTRE = 'Complémentaire Santé ';
                $DOC->append( 'LETTRE', {'SANTE'} );
            }
            else {
                $LIBCENTRE = 'Prévoyance ';
                $DOC->append( 'LETTRE', {'PREV'} );
            }

            $DOC->append( 'CODCENTRE', $CODCENTRE );
            $DOC->append( 'LIBCENTRE', $LIBCENTRE );

            ################ADRESSE EMETTEUR#########################
            $AGENCE = $VALS->{'PENO027'};
            $DOC->append( 'AGENCE', $VALS->{'PENO027'} );

            my $adresse =
                $VALS->{'PEAD030'} . ' '
              . $VALS->{'LICN034'} . ' '
              . $VALS->{'LIBV035'};
            $DOC->append( 'ADRESSE', $adresse );

            $DOC->append( 'BOITPOS', $VALS->{'BOIP037'} );

            $DOC->append( 'CODPOS', $VALS->{'CODC039'} );

            my $localite = ( $VALS->{'LIBL038'} );
            $DOC->append( 'LOCALITE', $localite );

            ######INDEXATION GED###################
            $GED->append( 'xIDEMET',  $EMETTEUR );        #-	Emetteur
            $GED->append( 'xNOMDEST', $xNOMDEST );        #-	Nom du destinataire
            $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
              ;    #-	Ville du destinataire

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E2' ) {

            # warn "INFO : dump E2 ". Dumper($VALS) ."\n";

            $DtEDITION =
              convertDate( $VALS->{'DATEDTx'} );   #Date du Traitement d'Edition
            $DOC->append( 'DATEEDT', $DtEDITION );

            #########NUMERO CONTRAT INDIVIDUEL############
            $NUMCTR = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMCTR', $NUMCTR );

            ###########CODE COURRIER###################
            $TYPECOUR = $VALS->{'CDCOURx'};

            if ( $TYPECOUR eq 'RESIN' ) {
                $DOC->append( 'TYPECOUR', $TYPECOUR );
            }
            else {
                die "\nERROR:CODE COURRIER INCONNU\n\n";
            }

            ######INDEXATION GED###################
            $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
              ;    #-	Numéro de contrat collectif
            $GED->append( 'xCLEGEDi', $VALS->{'ACH1NUM'} )
              ;    #-	Numéro de contrat individuel

            $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
            $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
            $GED->append( 'xCLEGEDiv', $DtEDITION )
              ;    #Date du Traitement d'Edition

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E7' ) {

            ############BLOC ADRESSE DESTINATAIRE##################
            my @DADDR = ();
            push( @DADDR, $NOMDEST );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
            );
            push( @DADDR, $VALS->{'BOIPOSL'} );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $VALS->{'LICCODP'} );
            $VALS->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $DOC->append_table( 'DADDR', @DADDR );

            # warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";
        }

        if ( $id eq 'OPTION' ) {

            # warn "INFO : dump OPTION ". Dumper($VALS) ."\n";

            $DtCOUR = $VALS->[2];    #DATE COURRIER SAISIE PAR LE GESTIONNAIRE
            my $valeur = $DtCOUR;
            $valeur =~ s/^\s+//;     #supprime les espaces en début de chaine
            $valeur =~ s/\s+$//;     #en fin de chaine
            $valeur =~ s/\s+/ /;     #remplace des séries d'espaces par un seul

            if ( $valeur ne '' ) {
                $DOC->append( 'DATECOUR', $DtCOUR );
            }
            else {
                die "\nERROR:Impossible de generer un courrier "
                  . $TYPECOUR
                  . " avec la date courrier vide\n\n";
            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n"; die();
        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }

    $DOC->append('COURRIER');
    $DOC->append('FINDOC');

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";
    # warn "INFO : dump GED ". Dumper($GED) ."\n";
    # die();

    $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $EMETTEUR, $GESTIONNAIRE,
        $DtCOUR );

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    $DOC->reset();
    $GED->reset();
    oe_compo_link($OPT_GED);
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

exit main(@ARGV);
