#!/usr/bin/perl
use utf8;
use strict;
use warnings;
use feature "switch";
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::HV;
use oEUser::Descriptor::X1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Delta_Days
  Add_Delta_Days
  Delta_YMD);
use Readonly;
my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
  localtime(time);
my $dateJour = sprintf( "%04d%02d%02d", 1900 + $year, $mon + 1, $mday );

#print "nous sommes le ",$dateUs;
my %JoursSem = (
    1 => "lundi",
    2 => "mardi",
    3 => "mercredi",
    4 => "jeudi",
    5 => "vendredi",
    6 => "samedi",
    7 => "dimanche",
);

#print " \n wday=$JoursSem{$wday}   \n";

my %Mois = (
    1  => "janvier",
    2  => "février",
    3  => "mars",
    4  => "avril",
    5  => "mai",
    6  => "juin",
    7  => "juillet",
    8  => "août",
    9  => "septembre",
    10 => "octobre",
    11 => "novembre",
    12 => "décembre",
);

#print " \n Mois=$Mois{$mon+1}   \n";

my $DateEdit =
    $JoursSem{$wday} . ' '
  . $mday . ' '
  . $Mois{ $mon + 1 } . ' '
  . ( 1900 + $year );
my ( $TRK, $GED, $DOC, $VALS, $P );
my ( $NUMCTR, $NUMABA, $nomUser, );
my $OPT_GED;
my %LBenef;
my $Gratuit = 0;

# structure de la table LBenef
# LBenef= {
#           NP01 => NGUYEN Antoine,
#           M01  => 25.20
#           NP20 => DUPONT Lalie
#           M20  => 20.10
#          }
#
#
#
#
my %hData;
my $nomdest;
my $civadh;
my @OrderBenef;
my $Indice          = 0;
my $MontGlobPeriode = 0;
my $EVetSE          = 0;
my $annee           = 1900 + $year;
my $COTMENS;
my $DATEEFFET = undef;
my $NOMDESTINATAIRE;
my $NOMEMETTEUR1;
my $NOMEMETTEUR2;
my $CODEGTIE;
my $CODEPRT;
my %H_INFO_GTIE = ();
my @TABGTIE     = ();
Readonly my $CODEPRTGS1  => "PL-MNT-GS1";
Readonly my $CODEPRTGS2  => "PL-MNT-GS2";
Readonly my $LIBCODE_PDT => "LIB_CODE_PDT_GTIE";
Readonly my $LIBLONG_PDT => "LIB_LONG_PDT_GTIE";
##################################################################################################################################
# Date : 03/04/2013  V1
# Projet :  sesame 1008419
# Nom Application : CRATT-EACEX
# En sortie : ATTESTATION D'ADHESION A UN CONTRAT LABELLISE sous format PDF
# Flux entrée : EACEX
#		ENTETE : information générale
#		E1 : Adresse destinataire donc adhérent
#		E2 : Contrat adhérent
#		H1 : Information des bénéficiaires
#		H4 : Information de garantie/ bénéficiaire
#
#Description de fonctionnement : C'est un traitement de type courrier bureautique
#		Dans cette version,Il concerne à créer uniquement les courrier de code LAB-01, LAB-02, LAB-03( pour la santé et la prévoyance).LAB-04, LAB-05 sont pour le développement du lot suivant
#		Ces courriers consiste aussi à afficher le numero adhérent, les périodes , la cotisation totale, la liste et la cotisation mensuelle de chaque bénéficiaire.
#		la cotisation totale : somme de tous les montants de période qui se trouve sur l'enregistrement H4
#		la cotisation mensulelle : somme de tous les montants de l'exercise/12, ils se trouve également sur l'enregistrement H4
#   Pour la période, il faut prendre les dates d'adhésion et de radiation de la première garantie de base
#                   Date de début période = MAX(01/01/<année considérée>, date d'adhésion de la garantie de base
#                   Date de fin de la période = MIN(31/12/<année considérée>, date de radiation de la garantie de base - 1 jour
##################################################################################################################################
# Projet : sésame 1048458
# Description : Pour l'attestations LAB-02 et LAB-03 : zone "cotisation totale sur la période" de l'attestation, modifier la position à 706 au lieu de 720
#
#####################################################################################################################################
# Projet ABA
# Ajout de la record Z1 pour l'ID-MNT (NUM ABA)
#
#####################################################################################################################################

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = "";

    # SI ON SOUHAITE METTRE LE DOCUMENT EN GED,
    # IL SUFFIT DE PASSER L'OPRION --edms À oe_compo_link COMME CI-DESSOUS
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'CONTRAT', 'AGENCE', 'NOMDEST', 'MONTANT', '' ]
    );
    oe_new_job('--index');
    &init_Doc();
    my $E1 = oEUser::Descriptor::E1->get();

    #		$E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    #		$E2->debug();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    # 	$E7->debug();
    $E7->bind_all_c7();
    my $H1 = oEUser::Descriptor::H1->get();

    #		$H1->debug();
    $H1->bind_all_c7();
    my $H4 = oEUser::Descriptor::H4->get();

    #	$H4->debug();
    $H4->bind_all_c7();
    my $X1 = oEUser::Descriptor::X1->get();

    #		$X1->debug();
    $X1->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();
    $P   = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'H1' => $H1,
        'H4' => $H4,
        'X1' => $X1,
        'Z1' => $Z1
    );
    $P->set_motif_to_denormalized_split('\x{01}');
    $DOC->append( 'xTYPDOC', 'ALAB' );    ### Type de document
    my $firstDoc = 0;
    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'ENTETE' ) {

        }
        elsif ( $id eq 'E1' ) {
            if ( !$firstDoc ) {
                $firstDoc = 1;
            }
            else {
                emitDoc();
                &init_Doc();
            }
            &traitement_E1();    #traitement du record E1#
        }
        elsif ( $id eq 'E2' ) {
#            warn "INFO : dump E2 ". Dumper($VALS) ."\n";
            &traitement_E2();    #traitement du record E2#
        }
        elsif ( $id eq 'E7' ) {
            &traitement_E7();    #traitement du record E7#
        }
        elsif ( $id eq 'H1' ) {
            &traitement_H1();    #traitement du record H1#
        }
        elsif ( $id eq 'H4' ) {    #record H4 #
#            warn "INFO : dump H4 ". Dumper($VALS) ."\n";
            &traitement_H4();
        }
        elsif ( $id eq 'Z1' ) {
            &traitement_Z1();
        }
    }
#    warn "INFO DOC: dump ". Dumper($DOC) ." \n";
    
    given ($hData{'CODECOURRIER'}){
      when (/LAB-02/){
         
         given ($CODEPRT){
            when (m/ACS/){
               $TRK->track( 'R', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
        $MontGlobPeriode, "CONTRAT ACS NON SUPPORTE PAS D'ATTESTATION DE LABELISATION" );
               print STDERR "CONTRAT ACS NON SUPPORTE PAS D'ATTESTATION DE LABELISATION \n";
               exit 1;
            }
         }
         if (($CODEPRT ne $CODEPRTGS1) && ($CODEPRT ne $CODEPRTGS2)){
            given ($EVetSE){
                     when (/0/){
                        $TRK->track( 'R', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
                  $MontGlobPeriode, "PRODUIT $CODEPRT NON SUPPORTE AVEC CODE COURRIER $hData{'CODECOURRIER'} PAS DE EV/ES ou IJ" );
                        print STDERR "PRODUIT $CODEPRT NON SUPPORTE AVEC CODE COURRIER $hData{'CODECOURRIER'} PAS DE EV/ES ou IJ \n";
                        exit 1;
                     }
            }
         }
         given ($DATEEFFET){
            when (undef){
               $TRK->track( 'R', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
        $MontGlobPeriode, "PRODUIT: $CODEPRT NON SUPPORTE PAS DE DATE D'EFFET" );
               print STDERR "PRODUIT: $CODEPRT NON SUPPORTE PAS DE DATE D'EFFET \n";
               exit 1;
            }
         }
         sortiedefault(); 
      }
      when (/LAB-01/){
         if (!defined $hData{'DATEFIN'} and $hData{'CODECOURRIER'} eq 'LAB-01'){
            $TRK->track( 'R', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
        $MontGlobPeriode, "CODE COURRIER $hData{'CODECOURRIER'} NON SUPPORTE PAS D'IJ: PRODUIT:$CODEPRT" );
            print STDERR "CODE COURRIER $hData{'CODECOURRIER'} NON SUPPORTE PAS D'IJ: PRODUIT:$CODEPRT \n";
            exit 1;
         }
         elsif (($CODEPRT =~ m/ACS/)){
            $TRK->track( 'R', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
        $MontGlobPeriode, "CONTRAT ACS NON SUPPORTE PAS D'ATTESTATION DE LABELISATION" );
            print STDERR "CONTRAT ACS NON SUPPORTE PAS D'ATTESTATION DE LABELISATION \n";
            exit 1;
         }else{
            emitDoc();
            oe_compo_link($OPT_GED);
            return 0;
         }
      }
      when (/LAB-03/){
            default{
               emitDoc();
               oe_compo_link($OPT_GED);
               return 0;             
            } 
      }
      when (/LAB-04/){
            default{
               emitDoc();
               oe_compo_link($OPT_GED);
               return 0;             
            } 
      }
      when (/LAB-05/){
            default{
               emitDoc();
               oe_compo_link($OPT_GED);
               return 0;             
            } 
      }
      default{
         $TRK->track( 'R', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
   $MontGlobPeriode, "CODE COURRIER $hData{'CODECOURRIER'} NON SUPPORTE" );
         print STDERR "CODE COURRIER $hData{'CODECOURRIER'} NON SUPPORTE \n";
         exit 1;
      }
    }
}

sub init_Doc() {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    $nomUser = "";
    undef %LBenef;
    undef @OrderBenef;
    undef %hData;
    $Indice          = 0;
    $MontGlobPeriode = 0;
    $EVetSE          = 0;
    $Gratuit         = 0;
    return (0);
}

#####################################

sub emitDoc {
    my ( $y, $m, $d );
    $DOC->append( 'NUMCONTRAT', $hData{'NUMCONTRAT'} );
    if ( exists $hData{'NUMABA'} ) {
        $DOC->append( 'NUMABA', $hData{'NUMABA'} );
    }
    $DOC->append(
        'DATEDEB',
        sprintf( "%02d/%02d/%04d",
            substr( $hData{'DATEDEB'}, 6, 2 ),
            substr( $hData{'DATEDEB'}, 4, 2 ),
            substr( $hData{'DATEDEB'}, 0, 4 ) )
    );

#  si la date n'est pas le dernier jour de l'année en cours alors il faut enlever 1 jour pour retrouver le dernier jour du mois précédent.
#    print STDERR "INFO DATEFIN = $hData{'DATEFIN'} \n";
    if ( $hData{'DATEFIN'} ne $annee . '1231' ) {

        #  	warn "info DATEFIN = $hData{'DATEFIN'} \n";
        ( $y, $m, $d ) = Add_Delta_Days(
            substr( $hData{'DATEFIN'}, 0, 4 ),
            substr( $hData{'DATEFIN'}, 4, 2 ),
            substr( $hData{'DATEFIN'}, 6, 2 ), -1
        );
        $DOC->append( 'DATEFIN', sprintf( "%02d/%02d/%04d", $d, $m, $y ) );
    }
    else {
        $DOC->append(
            'DATEFIN',
            sprintf( "%02d/%02d/%04d",
                substr( $hData{'DATEFIN'}, 6, 2 ),
                substr( $hData{'DATEFIN'}, 4, 2 ),
                substr( $hData{'DATEFIN'}, 0, 4 ) )
        );
    }

    #warn "total periode $nbmois mois = $MontGlobPeriode \n";
    #warn "Gratuit  =  $Gratuit mois   \n ";
    if ( $Gratuit < 1 ) {
        $DOC->append( 'GRATUIT', '' );
    }
    elsif ( $Gratuit == 1 ) {
        $DOC->append( 'GRATUIT', ' (dont 1 mois gratuit)' );
    }
    elsif ( $Gratuit > 1 ) {
        $DOC->append( 'GRATUIT', " (dont $Gratuit mois gratuits)" );
    }

    if ( $hData{'CODECOURRIER'} eq 'LAB-01' ) {
        $DOC->append( 'COTTOTAL',    $MontGlobPeriode );
        $DOC->append( 'EditPeriode', $hData{'EditPeriode'} );
        $DOC->append('Tableau');

        # ecrire le tableau
        for ( $Indice = 0 ; $Indice <= $#OrderBenef ; $Indice++ )
        {    # boucle de calcul de montant
            my $var = 'M'
              . substr( $OrderBenef[$Indice], 2,
                length( $OrderBenef[$Indice] ) - 2 );
            $DOC->append( 'NOMPRE', $LBenef{ $OrderBenef[$Indice] } );

#  Je fais afficher uniquement les bénéficiaires dont le montant de la cotisation mensuelle existe, c'est à dire ces bénéficiaires ne sont pas encore radiés. .
            if ( exists $LBenef{$var} ) {
                my $ValMensuelle = $LBenef{$var} / 12;
                $DOC->append( 'COT', $ValMensuelle );
                $DOC->append('TABCOT');
            }
            else {
                # warn "Ce rang de l assure n existe pas : $var  \n";
            }
        }
        $DOC->append('FERMER');
    }
    else {
        for ( $Indice = 0 ; $Indice <= $#OrderBenef ; $Indice++ )
        {    # boucle de calcul de montant
            my $var = 'M'
              . substr( $OrderBenef[$Indice], 2,
                length( $OrderBenef[$Indice] ) - 2 );
            $DOC->append( 'NOMPRE', $LBenef{ $OrderBenef[$Indice] } );
#  Je fais afficher uniquement les bénéficiaires dont le montant de la cotisation mensuelle existe, c'est à dire ces bénéficiaires ne sont pas encore radiés. .
            if ( exists $LBenef{$var} ) {
                my $ValMensuelle = $LBenef{$var} / 12;
                $COTMENS = $ValMensuelle;
                $DOC->append( 'COTMENS', $COTMENS );
            }
            else {
#                 warn "Ce rang de l assure n existe pas : $var  \n";
            }
        }
    }
    if ( $hData{'CODECOURRIER'} ne 'LAB-01' ) {
        $DOC->append( 'COTTOTAL',    $MontGlobPeriode );
        $DOC->append( 'EditPeriode', $hData{'EditPeriode'} );
    }
    $DOC->append( 'CIVIL',        $civadh );
    $DOC->append( 'NOMPRETEXT',   $nomdest );
    $DOC->append( 'CODECOURRIER', $hData{'CODECOURRIER'} );

# Prise en charge de la NOPI dont les codes produits sont PL-MNT-GS1 et PL-MNT-GS2
    $CODEGTIE = $TABGTIE[0];
    $DOC->append( 'CODEPDT',    $H_INFO_GTIE{$LIBCODE_PDT}{ $TABGTIE[0] } );
    $DOC->append( 'LIBLONGPDT', $H_INFO_GTIE{$LIBLONG_PDT}{ $TABGTIE[0] } );

   if (   ( $hData{'CODECOURRIER'} eq 'LAB-01' )
     or ( $hData{'CODECOURRIER'} eq 'LAB-02' )
     or ( $hData{'CODECOURRIER'} eq 'LAB-03' )
     or ( $hData{'CODECOURRIER'} eq 'LAB-04' )
     or ( $hData{'CODECOURRIER'} eq 'LAB-05' ) )
   {
     $DOC->append('ATTESTATION');
   }
   if (($MontGlobPeriode eq 0 || (!defined $MontGlobPeriode))
   or ($COTMENS eq 0 || !defined $COTMENS) and $hData{'CODECOURRIER'} ne 'LAB-01'){
      $TRK->track( 'R', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
$MontGlobPeriode, "COTISATION A ZERO POUR L'ADHERENT $hData{'NUMCONTRAT'}" );
      print STDERR "COTISATION A ZERO POUR L'ADHERENT $hData{'NUMCONTRAT'} \n";
      exit 1;
   }
   $TRK->track( 'Doc', 1, $hData{'NUMCONTRAT'}, $hData{'AGENCE'}, $nomdest,
        $MontGlobPeriode,, );
   $GED->append( 'xSOURCE', 'BUR' );
   oe_print_to_output_file($GED);
   oe_print_to_output_file($DOC);
   $DOC->reset();
   $GED->reset();
   init_Doc();
   $DOC = oEdtk::TexDoc->new();
   return (0);
}

############################################################################
# récuperer l'adresse destinaire et la date d'édition
###################################################################

sub traitement_E1() {

    # 	NORME 38 : ON UTILISE PLUS LA RECORD E1 POUR LES ADRESSES
    #	warn "INFO : dump E1 ". Dumper($VALS) ."\n";
    # 	afficher la date du traitement;
    $NOMDESTINATAIRE =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

    $nomdest = $VALS->{'PENOLIB'} . ' ' . $VALS->{'PENOPRN'};
    $civadh  = $VALS->{'LICCODC'};
    $GED->append( 'xNOMDEST', $nomdest );
    $NOMEMETTEUR1    = "$VALS->{'PENO025'} $VALS->{'PENO027'}";
    $NOMEMETTEUR2    = "$VALS->{'PNTR028'} $VALS->{'CPLA029'}";
    $hData{'AGENCE'} = $VALS->{'ODERCOD'};
    return (0);
}

sub traitement_E7() {

    # 	NORME 38 : ON UTILISE LA RECORD E7 POUR LES ADRESSES
    #	warn "INFO : dump E7 ". Dumper($VALS) ."\n";

    ############BLOC ADRESSE DESTINATAIRE##################

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $VALS->{'PELICCO'} );
    $VALS->{'PELICCO'} =~ s/FRANCE//i;

    my @DADDR = ();
    push( @DADDR, $NOMDESTINATAIRE );
    push( @DADDR, $VALS->{'PNTREMx'} );
    push( @DADDR, $VALS->{'CPLADRx'} );
    push( @DADDR, "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );
    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
    }
    else {
        push( @DADDR, $VALS->{'BOIPOSL'} );
    }
    push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );
    push( @DADDR, $VALS->{'PELICCO'} );
    @DADDR = user_cleanup_addr(@DADDR);

    $DOC->append_table( 'DADDR',  @DADDR );
    $DOC->append_table( 'xADRLN', @DADDR );

    $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} );
    $GED->append( 'xCPDEST',  oe_iso_country() . $VALS->{'CODCDXC'} );

    ############BLOC ADRESSE EMETTEUR##################

    my @EADDR = ();
    push( @EADDR, $NOMEMETTEUR1 );
    push( @EADDR, $NOMEMETTEUR2 );
    push( @EADDR, "$VALS->{'PEAD016'} $VALS->{'PEAD017'} $VALS->{'LIBV019'}" );
    push( @EADDR, "$VALS->{'CODC023'} $VALS->{'NOMC040'}" );

    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @EADDR, "$VALS->{'BOIP021'} $VALS->{'LIBL038'}" );
    }
    else {
        push( @EADDR, $VALS->{'BOIP021'} );
    }
    @EADDR = user_cleanup_addr(@EADDR);
    $hData{EADDR} = \@EADDR;

    $DOC->append_table( 'EADDR', @EADDR );
    $hData{DATEEDI}    = $DateEdit;
    $hData{EditENTETE} = '';

    $DOC->append( 'DATEEDI', $DateEdit );
    $DOC->append('EditENTETE');

    return (0);
}

sub traitement_E2() {

    #warn "INFO : dump E2 ". Dumper($VALS) ."\n";
    $NUMCTR = $VALS->{'ACH1NUM'};
    $GED->append( 'xIDDEST', $NUMCTR );
    $hData{'NUMCONTRAT'}   = $NUMCTR;
    $hData{'CODECOURRIER'} = $VALS->{'CDCOURx'};
    $DATEEFFET             = convertDate( $VALS->{'ACH1DDE'} );
#    print STDERR "INFO : DATE D'EFFET = $DATEEFFET \n";
####################AJOUT DU NOM-PRENOM DU GESTIONNAIRE DANS L'IDEMET POUR SIMPLIFICATION DE LA GESTION DANS DOCUBASE.############
    my $GEST = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
    $GED->append( 'xIDEMET', $GEST );    # GESTIONNAIRE
    return (0);
}

#################################################################################################################################
# Récupérer le nom et prenom dans un haschage LBenef{numero de rang}=NOM Prenom
# et dans OrderBenef pour garder l'ordre de lecture du flux d'où l'intéret est de retrouver l'assuré principal en premiere ligne
#################################################################################################################################
sub traitement_H1() {
    my $NomPreAss;

    #	warn "INFO : dump H1 ". Dumper($VALS) ."\n";
    $NomPreAss = 'NP'
      . $VALS->{'ACH2ACA'}
      ;    # Une combinaison de cle NP+Numero de rang = Nom prénom de l'assuré
    $LBenef{$NomPreAss} = $VALS->{'PENOLIB'} . ' ' . $VALS->{'PENOPRN'};
    $OrderBenef[$Indice] = $NomPreAss;
    $Indice++;
    return (0);
}

##########################################################################
# Lecture de H4 :
#     Récupérer la date de début
#     Récupérer la date de Fin
#     Faire la somme des montant de période
#     Faire la somme des montants de l'excercice dans haschage LBenef
#########################################################################

sub traitement_H4() {
    my $DateConsid;

    #my $annee;
    my $DateDebut;
    my $DATERAD = $VALS->{'ACH4010'};
    my $MontantAss;
    my $NPAss;
    my $DdDebut;
    my $DdFin;
    my $Dd;
    $CODEGTIE = $VALS->{'ACH4COD'};
    $CODEPRT  = $VALS->{'ACH4006'};

#	La règle pour la début période se base sur la première garantie ; soit ES et EV pour les courriers SANTE, soit les IJ pour les garanties PREVOYANCE, les garanties de PREV et de SANTE ne sont pas mélangées dans un même contrat.
# 	date de début période = MAX(01/01/année considérée, date d'adhésion)
#   warn"  garanties 1 =$VALS->{'PRGALIB'} \n";

    if (
        (
               ( $VALS->{'PRGALIB'} =~ /^(EV)/ )
            or ( $VALS->{'PRGALIB'} eq 'ES' )
            or ( substr( $VALS->{'PRGALIB'}, 0, 2 ) eq 'IJ' )
        )
        and ( $EVetSE == 0 )
      )
    {
        $EVetSE = 1;

        # calcul du début de période
        $DateConsid = $annee . '0101';
        $hData{'DATEDEB'} =
          Select2Date( $DateConsid, $VALS->{'ACH4DDE'}, 'MAX' );

  # s'il n'y a pas de date de radition alors je fais le calcul des mois gratuits
        if ( $DATERAD eq '' ) {
            my $AnneeCot = substr( $VALS->{'ACH4012'}, 0, 4 );
            $AnneeCot = $AnneeCot * 1;    # conversion en chiffre
              # 	si l'année de cotisation est antérieure à l'année en cours alors il n'y a pas de mois gratuits
            if ( $AnneeCot < $annee ) {
                $Gratuit = 0;
            }
            else {
                #	warn"année ahd = $VALS->{'ACH4DDE'}    \n";
                my $AnneeAdh = substr( $VALS->{'ACH4DDE'}, 0, 4 );
                $AnneeAdh = $AnneeAdh * 1;

                my $MoisConsid;

#  si l'année adhésion est antérieure à l'année en cours alors il faut ramener au début de l'année en cours, c'est le mois considéré est janvier sinon le mois de la date d'adhérent
                if ( $AnneeAdh < $annee ) {
                    $MoisConsid = 1;
                }
                else {
                    $MoisConsid = substr( $VALS->{'ACH4DDE'}, 4, 2 );
                    $MoisConsid = $MoisConsid * 1;
                }

                #   warn"année cotisation = $VALS->{'ACH4012'}      \n";
                my $MoisCot = substr( $VALS->{'ACH4012'}, 4, 2 ) * 1;

#                   warn" gratuit = $MoisCot - $MoisConsid \n";
                $Gratuit = $MoisCot - $MoisConsid;
            }
        }
    }

############################################################################################################################
#La règle pour la fin période se base sur toutes garanties de bases ; soit ES et EV pour les courriers SANTE, soit les IJ pour les garanties PREVOYANCE,
# les garanties de PREV et de SANTE ne sont pas mélangées dans un même contrat.
# s'il n'y a pas de radiation alors c'est la date 31/12/année considérée
#  sinon date de fin période = MIN (31/12/année considérée, MAX(date de radiation toutes garanties de bases confondues)- 1 jour)
############################################################################################################################
#    warn "garantie principale =$VALS->{'PRGALIB'}    \n";

    elsif (
           ( $VALS->{'PRGALIB'} =~ /^(EV)/ )
        or ( $VALS->{'PRGALIB'} eq 'ES' )
        or ( substr( $VALS->{'PRGALIB'}, 0, 2 ) eq 'IJ' )

        # JLE 28/07/2017 mise a jour regle de gestion sur le libelle court
        or ( $VALS->{'PRGALIB'} =~ m/MNT Santé [1-9]{1}/i )
      )
    {
        my $DateConsidfin = $annee . '1231';
        my $DateConsiddeb = $annee . '0101';
        $hData{'DATEDEB'} =
          Select2Date( $DateConsiddeb, $VALS->{'ACH4DDE'}, 'MAX' );

# si la date de de radiation est vide alors la date de fin période est 31/12/année considérée
        if ( $DATERAD ne '' ) {
            $hData{'DATEFIN'} =
              Select2Date( $DATERAD, $hData{'DATEFIN'}, 'MAX' );

            #    warn" Recherche Date Radiation Max = $hData{'DATEFIN'}    \n";

            $hData{'DATEFIN'} =
              Select2Date( $DateConsidfin, $hData{'DATEFIN'}, 'MIN' );

           #    warn" Recherche Date Fin Min = $hData{'DATEFIN'}          \n\n";
        }
        else {
            $hData{'DATEFIN'} = $DateConsidfin;
        }
    }

    $NPAss      = 'NP' . $VALS->{'ACH4ACG'};    # Numero de rang de l'assuré
    $MontantAss = 'M'
      . $VALS->{'ACH4ACG'}
      ; # Une combinaison de cle M+Numero de rang = Montant de la cotisation mensuelle par bénéficiaire.

    # Faire la somme des Montants sur la période
    if (   ( $hData{'CODECOURRIER'} eq 'LAB-02' )
        or ( $hData{'CODECOURRIER'} eq 'LAB-03' )
        or ( $hData{'CODECOURRIER'} eq 'LAB-04' )
        or ( $hData{'CODECOURRIER'} eq 'LAB-05' ) )
    {
        $DOC->append( 'DOMAINE', 'PREV' );

        #		warn "LAB PREV  = $VALS->{'ACCOMAO'}    \n";
        my $MontPeriode = $VALS->{'ACCOMAO'};
        my $nbmois;
        my $moisf;
        my $moisd;
        if ( $hData{'DATEFIN'} eq '' || $hData{'DATEFIN'} == undef ) {
            $DateConsid = $annee . '1231';
            $hData{'DATEFIN'} =
              Select2Date( $DATERAD, $hData{'DATEFIN'}, 'MAX' );

            #warn" Recherche Date Radiation Max = $hData{'DATEFIN'}    \n";
            $hData{'DATEFIN'} =
              Select2Date( $DateConsid, $hData{'DATEFIN'}, 'MIN' );

#            warn" Recherche Date Fin Min = $hData{'DATEFIN'}          \n\n";
        }

        if ( $hData{'DATEDEB'} eq '' || undef ) {

            # calcul du début de période
            $DateConsid = $annee . '0101';

            #		warn "    Date debut   \n";
            $hData{'DATEDEB'} =
              Select2Date( $DateConsid, $VALS->{'ACH4DDE'}, 'MAX' );

  # s'il n'y a pas de date de radition alors je fais le calcul des mois gratuits
            if ( $DATERAD eq '' ) {
                my $AnneeCot = substr( $VALS->{'ACH4012'}, 0, 4 );
                $AnneeCot = $AnneeCot * 1;    # conversion en chiffre
                  # si l'année de cotisation est antérieure à l'année en cours alors il n'y a pas de mois gratuits
                if ( $AnneeCot < $annee ) {
                    $Gratuit = 0;
                }
                else {
                    #	warn"année ahd = $VALS->{'ACH4DDE'}    \n";
                    my $AnneeAdh = substr( $VALS->{'ACH4DDE'}, 0, 4 );
                    $AnneeAdh = $AnneeAdh * 1;
                    my $MoisConsid;
                    if ( $AnneeAdh < $annee )
                    { #  si l'année adhésion est antérieure à l'année en cours alors il faut ramener au début de l'année en cours, c'est le mois considéré est janvier sinon le mois de la date d'adhérent
                        $MoisConsid = 1;
                    }
                    else {
                        $MoisConsid = substr( $VALS->{'ACH4DDE'}, 4, 2 );
                        $MoisConsid = $MoisConsid * 1;
                    }

                    #	warn"année cotisation = $VALS->{'ACH4012'}      \n";
                    my $MoisCot = substr( $VALS->{'ACH4012'}, 4, 2 ) * 1;

                    #	warn" gratuit = $MoisCot - $MoisConsid \n";
                    $Gratuit = $MoisCot - $MoisConsid;
                }
            }
        }

        if ( ( substr( $hData{'DATEFIN'}, 4, 2 ) != 12 ) ) {
            if ( ( substr( $hData{'DATEFIN'}, 6, 2 ) ) != 31 ) {
                $moisf = ( substr( $hData{'DATEFIN'}, 4, 2 ) - 1 );
            }
            else {
                $moisf = ( substr( $hData{'DATEFIN'}, 4, 2 ) );
            }

            $moisd = ( substr( $hData{'DATEDEB'}, 4, 2 ) );
            $nbmois = ( $moisf - $moisd ) + 1;

            # warn "INFO date départ = $VALS->{'ACH4DDE'} / $moisd \n";
            # warn "INFO date fin = $hData{'DATEFIN'} / $moisf \n";
            # warn "INFO nombre de mois retenu = $nbmois \n";

        }
        else {
            $moisf = ( substr( $hData{'DATEFIN'}, 4, 2 ) );

            # warn "INFO date fin = $hData{'DATEFIN'} / $moisf \n";
            if ( ( substr( $VALS->{'ACH4DDE'}, 0, 4 ) ) >=
                ( substr( $hData{'DATEDEB'}, 0, 4 ) ) )
            {
                $moisd = ( substr( $VALS->{'ACH4DDE'}, 4, 2 ) );
            }
            else {
                $moisd = ( substr( $hData{'DATEDEB'}, 4, 2 ) );
            }

            # warn "INFO date départ = $VALS->{'ACH4DDE'} / $moisd \n";
            $nbmois = ( $moisf - $moisd ) + 1;

            # warn "INFO nombre de mois retenu = $nbmois \n";
        }

        if ( $nbmois != 12 ) {
            $MontPeriode = ( ( $MontPeriode / 12 ) * $nbmois );
        }
        $MontGlobPeriode += $MontPeriode;
    }
#      warn" Recherche Date Fin = $hData{'DATEFIN'}    \n";
    if ( $hData{'CODECOURRIER'} eq 'LAB-01' ) {
        $DOC->append( 'DOMAINE', 'SANTE' );

#        	warn "LAB01 = $VALS->{'ACCOMPH'}    \n";
        $MontGlobPeriode += $VALS->{'ACCOMPH'};
    }
    if ( substr( $VALS->{'PRGALIB'}, 0, 2 ) eq 'IJ' ) {
        $DOC->append( 'DATEEFFET', $DATEEFFET );
        $DOC->append( 'ANNEE',     $annee );
        
    }
    $H_INFO_GTIE{$LIBCODE_PDT}{$CODEGTIE} = $CODEPRT;
    $H_INFO_GTIE{$LIBLONG_PDT}{$CODEGTIE} = $VALS->{'PRPR008'};
    push( @TABGTIE, $CODEGTIE );
    if (   substr( $VALS->{'PRGALIB'}, 0, 2 ) eq 'IJ'
        || $CODEPRT =~ m/$CODEPRTGS1/
        || $CODEPRT =~ m/$CODEPRTGS2/ )
    {
        $DOC->append( 'DATEEFFET', $DATEEFFET );
        $DOC->append( 'ANNEE',     $annee );
    }
#  si (date radiation vide OU (la date de début< date du jour ET date radiation > date du jour)), alors Faire la somme des montants total de l'exercice

    $DateDebut = $VALS->{'ACH4DDE'};

    # la date de début de période ne peux pas être vide

    $DdDebut = Delta_Days(
        substr( $DateDebut, 0, 4 ),
        substr( $DateDebut, 4, 2 ),
        substr( $DateDebut, 6, 2 ),
        substr( $dateJour,  0, 4 ),
        substr( $dateJour,  4, 2 ),
        substr( $dateJour,  6, 2 )
    );

#    warn " DateJour $dateJour-DateDebut $DateDebut =  $DdDebut \n";
# date de radiation = vide alors le bénéficiaire n'ai pas radié et dans ce cas je donne une valeur fictive $DdFin =-1
# date radiation n'est pas vide alors je vérifie si celle -ci est après la date du jour

    if ( $DATERAD ne '' ) {
        $DdFin = Delta_Days(
            substr( $dateJour, 0, 4 ),
            substr( $dateJour, 4, 2 ),
            substr( $dateJour, 6, 2 ),
            substr( $DATERAD,  0, 4 ),
            substr( $DATERAD,  4, 2 ),
            substr( $DATERAD,  6, 2 )
        );

        #warn " DateRadie $DATERAD-DateJour $dateJour =  $DdFin \n";
    }
    else { $DdFin = -1; }

# si dans le champ date radiation est vide alors la personne n'est pas encore radié donc je fais la somme
    if ( ( $DdDebut >= 0 ) and ( ( $DdFin >= 0 ) or ( $DATERAD eq '' ) ) ) {

        #      warn"$DateDebut  - $DATERAD  \n";
        if ( exists $LBenef{$NPAss} ) {    # s'il existe le numero de rang
            if ( exists $LBenef{$MontantAss} ) {

                #		warn" montant suite$MontantAss= $LBenef{$MontantAss}   \n";
                $LBenef{$MontantAss} += $VALS->{'ACCOMAO'};
            }
            else {
                #		warn" montant debut$MontantAss= $LBenef{$MontantAss}   \n";
                $LBenef{$MontantAss} = $VALS->{'ACCOMAO'};
            }
        }
        else {
            die
"ERROR : Ce code rang de l'assuré inconnu : $VALS->{'ACH4COD'} (H4) ";
        }
    }
    else {    #si le champ date radiation non vide, donc il est radié
    }
    return (0);
}

sub traitement_Z1() {
    ############BLOC ID-MNT##################
    # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
    $NUMABA = $VALS->{'PERF003'};

    $hData{'NUMABA'} = $NUMABA;
    ######INDEXATION GED###################
    $GED->append( 'xCLEGEDiii', $NUMABA );  #- N° adhérent (ID_MNT) == N° ABA
    return (0);
}
##############################################################################################################################
#  sous programme qui compare les deux dates aux formats AAAAMMJJ et renvoie la date max ou min de format AAAAMMJJ
# si une date est vide en réception il retournera la date non vide , si les deux dates sont vides , il retournera la chaine "VIDE"
###############################################################################################################################
sub Select2Date {

    my $Date1    = shift;
    my $Date2    = shift;
    my $MAXouMIN = shift;
    my $DateRet;
    my $Dd;

    #   warn" DateRecu1, DateRecu2 = $Date1,$Date2,$MAXouMIN";
    if ( ( $Date1 eq '' ) and ( $Date2 eq '' ) ) {
        return $Date1;
    }
    elsif ( ( $Date1 eq '' ) and ( $Date2 ne '' ) ) {
        return $Date2;
    }
    elsif ( ( $Date1 ne '' ) and ( $Date2 eq '' ) ) {
        return $Date1;
    }
    else {
        $Dd = Delta_Days(
            substr( $Date1, 0, 4 ),
            substr( $Date1, 4, 2 ),
            substr( $Date1, 6, 2 ),
            substr( $Date2, 0, 4 ),
            substr( $Date2, 4, 2 ),
            substr( $Date2, 6, 2 )
        );
        if ( $MAXouMIN eq 'MAX' ) {
            if ( $Dd > 0 ) {
                $DateRet = $Date2;
            }
            else {
                $DateRet = $Date1;
            }
#            warn "INFO DATE RETENU = $DateRet \n";
        }
        elsif ( $MAXouMIN eq 'MIN' ) {
            if ( $Dd > 0 ) {
                $DateRet = $Date1;
            }
            else {
                $DateRet = $Date2;
            }
        }
        return $DateRet;
    }
    return (0);
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub sortiedefault {
      emitDoc();
      oe_compo_link($OPT_GED);
      return 0;
}

exit main(@ARGV);
