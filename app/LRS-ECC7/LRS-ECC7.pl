#!/usr/bin/perl
use utf8;
use warnings;
use strict;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls 0.16
  ; # utilisation d'une librairie de fonctions standards dédiée à la production Excel
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Descriptor::EB;
use oEUser::Descriptor::EC;
use oEdtk::TexDoc;

#################################################################################
# v0.5 21/11/2006 11:56:54 du squelette d'extraction de donnees pour C7
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données suur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

# CORPS PRINCIPAL DE L'APPLICATION :

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)

my $TRK;
my $doc;
my $totalMontant;
my $dateTitre;
my $lastAgence;

my $kNumColl;
my $kNumGroupe;
my $kMatricule;
my $cumulGrpDebit;
my $cumulGrpCredit;
my $cumulGrpSolde;
my $cumulColDebit;
my $cumulColCredit;
my $cumulColSolde;
my $cumulSecDebit;
my $cumulSecCredit;
my $cumulSecSolde;

my %hContratAssure;        # DETAIL DES LIGNES EB POUR TRI
my %hCumulDebit;           # CUMUL DES MONTANTS PAR MEMBRES
my %hCumulCredit;          # CUMUL DES MONTANTS PAR MEMBRES
my %hCumulSolde;           # CUMUL DES MONTANTS PAR MEMBRES
my %hDesignationGroupe;    # DESIGNATION DES GROUPES PAR CLEF

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => ['']
    );

    oe_new_job();

    # INITIALISATION PROPRE AU DOCUMENT
    my $EB = oEUser::Descriptor::EB->get();
    $EB->bind_all_c7();
    my $EC = oEUser::Descriptor::EC->get();
    $EC->bind_all_c7();
    initSec();
    my $p = oEdtk::RecordParser->new( \*IN, 'EB' => $EB, 'EC' => $EC );
    my $lastagence = '';
    my $compteur_EB = 0;
    $dateTitre = "";

    while ( my ( $id, $vals ) = $p->next() ) {

        if ( $id eq 'EB' ) {
            $compteur_EB++;
            my $agence = $vals->{'ECCI055'};
            $dateTitre = convertDate( $vals->{'ARRETEx'} );
            if ( $agence ne $lastAgence ) {
                if ( $lastAgence ne '' ) {
                    editDetail();
                }
            }
            $lastAgence = $agence;

            # CETTE DATE EST NÉCESSAIRE POUR OUVRIR LE FICHIER EXCEL
            initXls() if ( $compteur_EB == 1 );
            $kNumColl   = $vals->{'ECCINUM'};
            $kNumGroupe = $vals->{'ECCI003'};
            $kMatricule = $vals->{'ECCI042'};

            # ALIMENTATION D UN TABLEAU DE H POUR TRI EN FONCTION DE LA CLEF
            my $date_res = convertDate( $vals->{'ECDODDE'} );
            $hContratAssure{"$kNumColl=$kNumGroupe=$kMatricule"} =
"$vals->{'ECCICOD'};$vals->{'ECCINUM'};$vals->{'ECCI003'};$date_res;$vals->{'ECDONOM'} $vals->{'ECDOPRN'};$vals->{'ECCI042'};$vals->{'ECDOMOD'}";
            $hDesignationGroupe{"$kNumColl $kNumGroupe"} = $vals->{'ACH7LIB'};
        }
        if ( $id eq 'EC' ) {
            $hCumulDebit{"$kNumColl=$kNumGroupe=$kMatricule"} +=
              $vals->{'ECD1MTT'};
            $hCumulCredit{"$kNumColl=$kNumGroupe=$kMatricule"} +=
              $vals->{'ECD1005'};
            $hCumulSolde{"$kNumColl=$kNumGroupe=$kMatricule"} +=
              $vals->{'SOLDExx'};
        }
    }

    # Edition
    editDetail();
    $TRK->track( 'Doc', 1, '' );
    oe_compo_link();
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub editDetail {
    my $numColl     = 0;
    my $numGrp      = 0;
    my $lastNumGrp  = -1;
    my $lastNumColl = -1;
    my $temp        = "";

    # ATTENTION :
    #	il faut passer la dernière date d'édition lu dans un EB avant de
    #	lancer l'édition de l'agence (titre en tête de page)
    $doc->append( 'ARRETEx', $dateTitre );

    # ATTENTION :
    #	on traite l'agence précédente (lastSect) pour effectuer les
    #	tris par Contrat Assuré
    # 'Agence' tga is reserved for edtkdoc.cls
    $doc->append( 'AgenceLRSECCVii', $lastAgence );
    $doc->append('NewSect');

    foreach my $cle ( sort keys %hContratAssure ) {
        $cle =~ /(^.*)=(.*)=(.*)/g;
        $numGrp = "$1 $2";

        if ( $numGrp ne $lastNumGrp && $lastNumGrp ne -1 ) {
            editCumulGroupe($lastNumGrp);
        }

        $cumulGrpDebit  += $hCumulDebit{$cle};
        $cumulGrpCredit += $hCumulCredit{$cle};
        $cumulGrpSolde  += $hCumulSolde{$cle};

        $cumulSecDebit  += $hCumulDebit{$cle};
        $cumulSecCredit += $hCumulCredit{$cle};
        $cumulSecSolde  += $hCumulSolde{$cle};

        my @table_temp = split( /;/, $hContratAssure{$cle} );
        $doc->append( 'ECCICOD', $table_temp[0] );
        $doc->append( 'ECCINUM', $table_temp[1] );
        $doc->append( 'ECCIOOC', $table_temp[2] );
        $doc->append( 'ECDODDE', $table_temp[3] );
        $doc->append( 'PENOCOD', $table_temp[4] );
        $doc->append( 'ECCIODB', $table_temp[5] );
        $doc->append( 'ECDOMOD', $table_temp[6] );
        $doc->append( 'SOLDOOF', $hCumulSolde{$cle} );
        $doc->append( 'ECDAOOC', $hCumulDebit{$cle} );
        $doc->append( 'ECDAOOE', $hCumulCredit{$cle} );

        $doc->append('newEB');

        prod_Xls_Insert_Val($lastAgence);
        prod_Xls_Insert_Val( split( /;/, $hContratAssure{$cle} ) );
        prod_Xls_Insert_Val( $hCumulDebit{$cle} );
        prod_Xls_Insert_Val( $hCumulCredit{$cle} );
        prod_Xls_Insert_Val( $hCumulSolde{$cle} );
        prod_Xls_Edit_Ligne();
    }

    editCumulGroupe("$numGrp");
    finAgence();
    1;
}

sub finAgence {

    #print OUT " <#debSec=<SET>$cumulSecDebit>";
    #print OUT " <#credSec=<SET>$cumulSecCredit>";
    #	print OUT " <#soldSec=<SET>$cumulSecSolde><CUMLSEC><SK>\n";

    $doc->append( 'soldSec', $cumulSecSolde );
    $doc->append( 'debSec',  $cumulSecDebit );
    $doc->append( 'credSec', $cumulSecCredit );
    $doc->append('CUMLSEC');

    $cumulSecDebit  = 0;
    $cumulSecCredit = 0;
    $cumulSecSolde  = 0;

    initSec();
    1;
}

sub initSec {
    oe_print_to_output_file($doc);
    $doc = oEdtk::TexDoc->new();

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    $kNumColl       = "";
    $kNumGroupe     = "";
    $kMatricule     = "";
    $cumulGrpDebit  = 0;
    $cumulGrpCredit = 0;
    $cumulGrpSolde  = 0;
    $cumulColDebit  = 0;
    $cumulColCredit = 0;
    $cumulColSolde  = 0;
    $cumulSecDebit  = 0;
    $cumulSecCredit = 0;
    $cumulSecSolde  = 0;
    undef %hContratAssure;        # DETAIL DES LIGNES EB POUR TRI
    undef %hCumulDebit;           # CUMUL DES MONTANTS PAR MEMBRES
    undef %hCumulCredit;          # CUMUL DES MONTANTS PAR MEMBRES
    undef %hCumulSolde;           # CUMUL DES MONTANTS PAR MEMBRES
    undef %hDesignationGroupe;    # DESIGNATION DES GROUPES PAR CLEF
    1;
}

sub initXls {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large , Ac7 = Alpha Center Wrap... , NR7 = Numérique Right...  )
    prod_Xls_Col_Init(
        'AC8.3', 'AC9.2', 'AC10', 'AC12', 'AC12', 'AL42',
        'AC10',  'AC10',  'NR9',  'NR9',  'NR10'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init( "M.N.T", "LISTE des RADIÉS NON SOLDÉS au $dateTitre" )
      ;    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("Entité de rattachement");
    prod_Xls_Insert_Val("Code part payeur");
    prod_Xls_Insert_Val("N° de contrat collectif");
    prod_Xls_Insert_Val("Groupe assuré");
    prod_Xls_Insert_Val("Date de résiliation");
    prod_Xls_Insert_Val("Nom et Prénom");
    prod_Xls_Insert_Val("N° de l'adhésion");
    prod_Xls_Insert_Val("Mode de paiement");
    prod_Xls_Insert_Val("Montant débit");
    prod_Xls_Insert_Val("Montant crédit");
    prod_Xls_Insert_Val("Montant");

    # ETC.

    #EDITION DE LA TETE DE COLONNE
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );

    1;
}

sub editCumulGroupe {
    my $numGrp = shift;

    #	$doc->append('ECCIOOD', $hDesignationGroupe{$numGrp});
    $doc->append( 'ECCIOOD', ${ hDesignationGroupe { $numGrp } } );

    $doc->append( 'debGrp',  $cumulGrpDebit );
    $doc->append( 'credGrp', $cumulGrpCredit );
    $doc->append( 'soldGrp', $cumulGrpSolde );
    $doc->append('CUMLGRP');

    my $contratCollectif = $numGrp;
    $contratCollectif =~ s/^([\w-]+)\s+.*/$1/g;
    prod_Xls_Insert_Val( $lastAgence, "", $contratCollectif, "", "", );
    prod_Xls_Insert_Val("MONTANT TOTAL groupe ${hDesignationGroupe{$numGrp}}");
    prod_Xls_Insert_Val( "", "", $cumulGrpDebit, $cumulGrpCredit,
        $cumulGrpSolde );
    prod_Xls_Edit_Ligne();

    $cumulGrpDebit  = 0;
    $cumulGrpCredit = 0;
    $cumulGrpSolde  = 0;
    1;
}

exit main(@ARGV);
