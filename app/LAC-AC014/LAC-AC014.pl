#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::F3;
use oEUser::Descriptor::T1;
use oEdtk::TexDoc;
use oEdtk::libXls;
use strict;
use warnings;
use Data::Dumper;

#################################################################################
# v0.5 21/11/2006 11:56:54 du squelette d'extraction de donnees pour C7
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données suur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

# CORPS PRINCIPAL DE L'APPLICATION :

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)
my $TRK;
my $DOC;
my $LIBMOD   = "";
my $ADH      = "";
my $NUMADH   = "";
my $CG       = "";
my $ADRESADH = "";
my $DEBPER   = "";
my $FINPER   = "";
my $MONTANT  = 0;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => ['']
    );

    oe_new_job();

    # INITIALISATION PROPRE AU DOCUMENT
    my $E1 = oEUser::Descriptor::E2->get();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();
    $E7->bind_all_c7();
    my $F3 = oEUser::Descriptor::F3->get();
    $F3->bind_all_c7();
    my $T1 = oEUser::Descriptor::T1->get();
    $T1->bind_all_c7();
    $DOC = oEdtk::TexDoc->new();
    $DOC->append( 'xTYPDOC', 'LDAC' );    ### Type de document
    my $tableau = oEdtk::TexDoc->new();
    my $p       = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'E3' => undef,
        'F3' => $F3,
        'H1' => undef,
        'H0' => undef,
        'N1' => undef,
        'T1' => $T1,
        'L2' => undef
    );
    initXls();
    my $cheque = 1;

    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'E1' ) {

            #    warn "INFO : dump E1 ". Dumper($vals) ."\n";
            $CG = $vals->{'LICMODR'};
        }
        if ( $id eq 'E2' ) {

            $LIBMOD = $vals->{'LIBMODR'};
            $ADH =
                $vals->{'PENOCOD'} . " "
              . $vals->{'PENOLIB'} . " "
              . $vals->{'PENOPRN'};
            $NUMADH = $vals->{'ACH1NUM'};
            $tableau->append( 'ACHUNUM', $NUMADH );    # Num de reference
            $tableau->append( 'PENOCOD', $vals->{'PENOCOD'} );  # Titre abrègé
            $tableau->append( 'PENOLIB', $vals->{'PENOLIB'} );  # Nom
            $tableau->append( 'PENOPRN', $vals->{'PENOPRN'} );  # Prenom
            $tableau->append( 'ACPAMOD', $vals->{'LIBMODR'} );  #Mode reglement

        }
        if ( $id eq 'E7' ) {
            my @addr     = ();
            my $addrLine = $vals->{'PNTREMx'} . ' ' . $vals->{'CPLADRx'};
            oe_clean_addr_line($addrLine);
            push( @addr, $addrLine );                           # Adresse L1
            $addrLine =
                $vals->{'PEADNUM'} . ' '
              . $vals->{'PEADBTQ'} . ' '
              . $vals->{'LICNATV'} . ' '
              . $vals->{'LIBVOIx'};
            oe_clean_addr_line($addrLine);
            push( @addr, $addrLine );                           # Adresse L2
            $addrLine = $vals->{'BOIPOSL'};
            oe_clean_addr_line($addrLine);
            push( @addr, $addrLine );                           # Adresse L3

            if ( $vals->{'LIBLOCL'} eq $vals->{'NOMCDXL'} ) {
                $vals->{'LIBLOCL'} = '';
            }
            $addrLine =
                $vals->{'CODCDXC'} . ' '
              . $vals->{'LIBLOCL'} . ' '
              . $vals->{'NOMCDXL'};
            oe_clean_addr_line($addrLine);
            push( @addr, $addrLine );                           # Adresse L4
            $tableau->append_table( 'addr', user_cleanup_addr(@addr) );

            my $adresse = $vals->{'PNTREMx'} . ' ' . $vals->{'CPLADRx'};
            my $adresse1 =
                $vals->{'PEADNUM'} . ' '
              . $vals->{'PEADBTQ'} . ' '
              . $vals->{'LICNATV'} . ' '
              . $vals->{'LIBVOIx'};
            my $adresse2 =
                $vals->{'CODCDXC'} . ' '
              . $vals->{'LIBLOCL'} . ' '
              . $vals->{'NOMCDXL'};
            $ADRESADH = "$adresse \n $adresse1 \n $adresse2";

        }
        if ( $id eq 'F3' ) {
            $tableau->append( 'DDEPRDx', convertDate( $vals->{'DDEPRDx'} ) )
              ;    # Date du
            $tableau->append( 'DFEPRDx', convertDate( $vals->{'DFEPRDx'} ) )
              ;    # Date au
            $DEBPER = $vals->{'DDEPRDx'};
            $FINPER = $vals->{'DFEPRDx'};
        }
        if ( $id eq 'T1' ) {
            $MONTANT = $vals->{'MONTANT'};
            $tableau->append( 'MONTANT', $vals->{'MONTANT'} );    # Montant
            if ( $cheque == 1 ) {
                $tableau->append('ENDLN');
            }
            edit_ligne_XLS();
            $cheque = 1;
        }
    }

    # Edition
    finPage($tableau);

    #warn "INFO tableau: dump ". Dumper($tableau) ." \n";
    $TRK->track( 'Doc', 1, '' );

    # oe_compo_link();
    return 0;
}

sub finPage {
    my $tableau = shift;
    $DOC->append( 'tableau', $tableau );
    $DOC->append('ENDPG');
    prod_Xls_Edit_Ligne('T2');
    oe_print_to_output_file("$DOC");
    $DOC = oEdtk::TexDoc->new();
    return 0;
}

sub edit_ligne_XLS {
    my $vals;
    prod_Xls_Insert_Val($NUMADH);
    prod_Xls_Insert_Val($ADH);
    prod_Xls_Insert_Val($ADRESADH);
    prod_Xls_Insert_Val($DEBPER);
    prod_Xls_Insert_Val($FINPER);
    prod_Xls_Insert_Val($LIBMOD);
    prod_Xls_Insert_Val($CG);
    prod_Xls_Insert_Val($MONTANT);

    prod_Xls_Edit_Ligne();

    #warn "INFO MONTANT: $MONTANT \n";
    return 0;
}

sub initXls {

    #	my $fichier=$ARGV[1];

    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ; NR7 = Numérique Right...  )
    prod_Xls_Col_Init(
        'AC15', 'AL30', 'AL50', 'AC15', 'AC15', 'AC30',
        'AC30', 'AC30', 'NR20'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init( "", "LISTE DES APPELS DE COTISATIONS INDIVIDUELS" )
      ;    #  ,"ENTETE DROIT");

# INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
# PRÉPARATION DES TITRES DE COLONNES
# AGENCE; CONTRAT; COL; GROUPE; CIV; NOM ; DATNAI; PRéNOM ; NOV; BTQ; NTV; LBV; AD2;  LOC; CPS; BDS
    prod_Xls_Insert_Val("REFERENCE");
    prod_Xls_Insert_Val("ADHERENT");
    prod_Xls_Insert_Val("ADRESSE ADHERENT");
    prod_Xls_Insert_Val("DEBUT DE PERIODE");
    prod_Xls_Insert_Val("FIN DE PERIODE");
    prod_Xls_Insert_Val("MODE DE REGLEMENT.");
    prod_Xls_Insert_Val("CG");
    prod_Xls_Insert_Val("MONTANT");

    prod_Xls_Row_Height(32);

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

exit main(@ARGV);
