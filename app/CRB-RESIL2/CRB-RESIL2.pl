#! /usr/bin/perl
use warnings;
use strict;
use feature qw( switch );
use oEdtk::Main qw( oe_app_usage oe_corporation_set oe_new_job oe_compo_link );
use oEdtk::LaTeX qw( tag );
use oEdtk::Utils
  qw( parse_jjmmaaaa parse_price parse_yyyymmdd track track_end alert flatten_index_variables max_value scalar_dump  trim_list );
use oEdtk::Config qw( config_read );
use oEdtk::Infinite
  qw( is_flux is_entete is_ligne is_option id_ligne record_values extract_options );
use Data::Dumper qw( Dumper );
use Sys::Hostname qw( hostname );
use Date::Calc qw( Days_in_Month );
use oEdtk::Constants qw( check_typdoc );
use Readonly;

Readonly my $TAG_LETTER      => 'Lettre';
Readonly my $TAG_ATTESTATION => 'Attestation';
Readonly my @TRACKING_FIELDS => (qw( CODECOUR ));

# What we need to extract from the flow
# http://doc_infinite/mdg/V7.0.0/DET_EACEX.pdf
Readonly my $EXPECTED_VALUES => {
    ligne => {
        E1 => {
            civ                 => [ 'X', 171,  10 ],
            civilite            => [ 'X', 181,  15 ],
            titre               => [ 'X', 196,  10 ],
            entite_rattachement => [ 'X', 564,  9 ],
            code_courrier       => [ 'X', 2669, 5 ],
            nom_famille         => [ 'X', 221,  50 ],
            prenom              => [ 'X', 271,  50 ],
            agence_nom          => [ 'X', 598,  50 ],
            agence_ville        => [ 'X', 823,  32 ],
        },
        E2 => {
            date_traitement    => [ 'X', 893, 8 ],
            contrat_individuel => [ 'X', 798, 15 ],
        },
        E7 => {
            adresse         => \&oEdtk::Infinite::E7_adresse,
            agence_adresse  => \&oEdtk::Infinite::E7_adresse_agence,
            index_variables => \&oEdtk::Infinite::E7_index_variables,
        },
        H1 => {
            date_adhesion  => [ 'X', 608, 8 ],
            date_radiation => [ 'X', 616, 8 ],
        },
        H4 => {},
        Z1 => {
            numaba => [ 'X', 183, 30 ],
        },
    },
};

Readonly my %OPT_DATE =>
  ( option => { sub => \&oEdtk::Utils::parse_jjmmaaaa, tag => 'DateReception' }
  );
Readonly my %OPT_PRICE =>
  ( option => { sub => \&oEdtk::Utils::parse_price, tag => 'MontantGarantie' }
  );
Readonly my %DOC_ATTESTATION => ( flush => \&produce_attestation );
Readonly my %DOC_LETTER      => ( flush => \&produce_letter );
Readonly my %NEEDED_TR => ( needed => [qw( DateTraitement DateReception )] );
Readonly my %NEEDED_TF => ( needed => [qw( DateTraitement DateFinContrat )] );
Readonly my %NEEDED_TFM =>
  ( needed => [qw( DateTraitement DateFinContrat MontantGarantie )] );
Readonly my %NEEDED_TFD =>
  ( needed => [qw( DateTraitement DateFinContrat DateDebutContrat )] );

# Cf. [[Typologie des documents]] (redmine wiki Éditique)
Readonly my %CRCI => ( typdoc => 'CRCI' );
Readonly my %CREA => ( typdoc => 'CREA' );

Readonly my $SPECIFICS => {

    RESD1 => { %DOC_ATTESTATION, %NEEDED_TFD, %CRCI },
    RESD2 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },
    RESD3 => { %OPT_PRICE,       %DOC_LETTER, %NEEDED_TFM, %CRCI },
    RESD4 => { %OPT_PRICE,       %DOC_LETTER, %NEEDED_TFM, %CRCI },
    RESD5 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },
    RESD6 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },

    RESF1 => { %DOC_ATTESTATION, %NEEDED_TFD, %CRCI },
    RESF2 => { %OPT_PRICE,       %DOC_LETTER, %NEEDED_TFM, %CRCI },
    RESF3 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },
    RESF4 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },
    RESF5 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },
    RESF6 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },

    RESR1 => { %DOC_ATTESTATION, %NEEDED_TFD, %CRCI },
    RESR2 => { %DOC_LETTER,      %NEEDED_TF,  %CRCI },
    RESR3 => { %DOC_LETTER,      %NEEDED_TF,  %CRCI },
    RESR4 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },
    RESR5 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },
    RESR6 => { %OPT_DATE,        %DOC_LETTER, %NEEDED_TR, %CREA },

};

sub need_option {
    my ( $h, $flowFile, $lineNumber ) = @_;
    return alert( $flowFile, $lineNumber, 'No code courrier', $h )
      unless exists $h->{'code_courrier'};
    my $cc = $h->{'code_courrier'};
    return ( exists $SPECIFICS->{$cc}->{'option'} )
      ? $SPECIFICS->{$cc}->{'option'}
      : undef;
}

sub treat_line {
    my ( $trk, $h, $lineRef, $flowFile, $lineNumber ) = @_;
    my $id = id_ligne($lineRef);
    return alert( $flowFile, $lineNumber, "Unexpected line id '$id'" )
      unless exists $EXPECTED_VALUES->{'ligne'}->{$id};
    my @errors =
      record_values( $h, $EXPECTED_VALUES->{'ligne'}->{$id}, $lineRef );
    return alert( $flowFile, $lineNumber, join( "\n", @errors ) )
      if scalar @errors;
    return;
}

sub treat_input {
    my ( $trk, $h, $lineRef, $flowFile, $lineNumber ) = @_;

    # Should be useless if the input received was not garbage
    oEdtk::Utils::kill_non_latin_1_9_chars_in_place( \$lineRef );

    given ($lineRef) {
        when ( is_flux($lineRef) )   { }
        when ( is_entete($lineRef) ) { }
        when ( is_ligne($lineRef) ) {
            treat_line( $trk, $h, $lineRef, $flowFile, $lineNumber );
        }
        when ( is_option($lineRef) ) {
            alert( $flowFile, $lineNumber, "multiple options found" )
              if exists $h->{"option"};
            $h->{'option'} = extract_options($lineRef);
        }
        default {
            alert( $flowFile, $lineNumber,
                "cannot guess line type for '$$lineRef'",
                scalar_dump($$lineRef) );
        }
    }
    return;
}

sub check_common_parameters {
    my ( $h, $flowFile, $lineNumber ) = @_;
    foreach my $key (
        qw( code_courrier civilite index_variables numaba entite_rattachement
        contrat_individuel adresse agence_adresse
        )
      )
    {
        return alert( $flowFile, $lineNumber, "key '$key' missing", $h )
          if !exists $h->{$key};
        return alert( $flowFile, $lineNumber, "key '$key' undef", $h )
          if !defined $h->{$key};
    }
    return 1;
}

sub common_parameters {
    my ( $h, $flowFile, $lineNumber ) = @_;
    my $doc = oEdtk::TexDoc->new();
    $doc->append('xNOM_AGENCE', $h->{'agence_nom'});
    $doc->append('xADRESSE_AGENCE', $h->{'agence_adresse'}[1].' - '.$h->{'agence_adresse'}[2]);
    oEdtk::Main::oe_print_to_output_file($doc);

    return (
        CodeCourrier   => $h->{'code_courrier'},
        Civilite       => $h->{'civilite'},
        NumeroContrat  => $h->{'contrat_individuel'},
        NumeroAdherent => $h->{'numaba'},
        AgenceAdresse  => {
            colored_address => {
                address => $h->{'agence_adresse'},
            }
        },
    );
}

sub add_field {
    my ( $res, $h, $field ) = @_;
    my $val;
    given ($field) {
        when ( $field eq 'DateTraitement' ) {
            $val = parse_yyyymmdd( $h->{'date_traitement'} );
        }
        when ( $field eq 'DateFinContrat' ) {
            $val = parse_yyyymmdd( $h->{'date_radiation'} );
        }
        when ( $field eq 'DateDebutContrat' ) {
            $val = parse_yyyymmdd( $h->{'date_adhesion'} );
        }
        default { die "Unknown field '$field'. h is: " . Dumper($h); }
    }
    $res->{$field} = $val;
    return;
}

sub specific_parameters {
    my ( $h, $flowFile, $lineNumber ) = @_;
    my $cc     = $h->{'code_courrier'};
    my $needed = $SPECIFICS->{$cc}->{'needed'};
    my $res    = optional_parameters( $h, $flowFile, $lineNumber );
    foreach my $field ( @{$needed} ) {
        add_field( $res, $h, $field )
          unless exists $res->{$field};    # already fed by optional_parameters
    }
    return %{$res};
}

sub track_values {
    my ( $h, $type ) = @_;
    return [ ( map { $h->{$_} } qw( code_courrier ) ), $type ];
}

sub track_and_index {
    my ( $trk, $h, $type ) = @_;
    my $track_values = track_values( $h, $type );
    track( $trk, $track_values );
    $h->{'index_variables'}->{'Clef'}       = $track_values;
    $h->{'index_variables'}->{'IdDest'}     = $h->{'numaba'};
    $h->{'index_variables'}->{'IdEmetteur'} = $h->{'entite_rattachement'};
    return;
}

sub produce_attestation {
    my ( $trk, $h, $flowFile, $lineNumber ) = @_;
    track_and_index( $trk, $h, $TAG_ATTESTATION );
    my $index = flatten_index_variables($h);
    my $tag   = tag(
        $TAG_ATTESTATION,
        {
            common_parameters( $h, $flowFile, $lineNumber ),
            specific_parameters( $h, $flowFile, $lineNumber ),
            %{$index},
            NomFamille   => $h->{'nom_famille'},
            Prenom       => $h->{'prenom'},
            AgenceNom    => $h->{'agence_nom'},
            LieuCourrier => $h->{'agence_ville'},
        }
    );
    warn $tag;
    return ( ref $tag )
      ? alert( $flowFile, $lineNumber, @{$tag} )
      : $tag;
}

sub optional_parameters {
    my ( $h, $flowFile, $lineNumber ) = @_;
    my $option = need_option( $h, $flowFile, $lineNumber );
    return alert( $flowFile, $lineNumber,
        'No option line found, but one is needed', $h )
      if ( defined $option ) && !exists $h->{'option'};
    if ( defined $option ) {
        return alert( $flowFile, $lineNumber, 'Option list has wrong size', $h )
          unless scalar @{ $h->{'option'} } == 1;
        my $res = &{ $option->{'sub'} }
          ( ${ $h->{'option'} }[0], $flowFile, $lineNumber );
        return alert( $flowFile, $lineNumber, 'Cannot parse date in option',
            $h )
          unless defined $res;
        return { $option->{'tag'} => $res },;
    }
    return {};
}

sub produce_letter {
    my ( $trk, $h, $flowFile, $lineNumber ) = @_;
    track_and_index( $trk, $h, $TAG_LETTER );
    my $index = flatten_index_variables($h);
    my $tag   = tag(
        $TAG_LETTER,
        {
            common_parameters( $h, $flowFile, $lineNumber ),
            specific_parameters( $h, $flowFile, $lineNumber ),
            %{$index},
            AdresseDestinataire => $h->{'adresse'},
        }
    );
    return ( ref $tag )
      ? alert( $flowFile, $lineNumber, @{$tag} )
      : $tag;
}

sub skip_document {
    my ($h) = @_;
    if ( $h->{'entite_rattachement'} =~ m{^[JM]}sox ) {
        warn "Skipping (rule: produce no document for MUTACITE people)";
        return 1;
    }
    else {
        return 0;
    }
}

sub get_typdoc {
    my ($cc) = @_;
    die "Unknown code courrier '$cc'" unless exists $SPECIFICS->{$cc};
    return $SPECIFICS->{$cc}->{'typdoc'};
}

sub complete_hash {
    my ($h) = @_;
    my $nomPrenom = trim_list( map { $h->{$_} } qw( nom_famille prenom ) );
    my $addrLine1 =
      trim_list( map { $h->{$_} } qw( civ titre nom_famille prenom ) );
    $h->{'index_variables'}->{'DestNomPrenom'} = $nomPrenom;
    $h->{'index_variables'}->{'TypeDocument'} =
      check_typdoc( get_typdoc( $h->{'code_courrier'} ) );
    unshift @{ $h->{'adresse'} },        $addrLine1;
    unshift @{ $h->{'agence_adresse'} }, $h->{'agence_nom'};
    return;
}

sub flush_hash {
    my ( $trk, $h, $flowFile, $lineNumber ) = @_;
    return unless check_common_parameters( $h, $flowFile, $lineNumber );
    return if skip_document($h);
    my $cc = $h->{'code_courrier'};
    return alert( $flowFile, $lineNumber, "Unknown code courrier '$cc'" )
      unless exists $SPECIFICS->{$cc};
    warn "CODE COURRIER is $cc";
    complete_hash($h);
    my $sub = $SPECIFICS->{$cc}->{'flush'};

    oEdtk::Main::oe_print_to_output_file(
        &{$sub}( $trk, $h, $flowFile, $lineNumber ), "\n" );
    return;
}

sub treat_flow {
    my ( $trk, $flowFile ) = @_;
    my $h = {};
    open my $io, '<', $flowFile or die "$flowFile: $!";
    treat_input( $trk, $h, \$_, $flowFile, $. ) while <$io>;
    close($io) || die Dumper($io) . ": $!";
    flush_hash( $trk, $h, $flowFile, $. );    # do not forget the last record!
    return;
}

sub main {
    my ($flowFile) = @_;
    oe_app_usage() unless defined $flowFile;
    my $trk = oEdtk::Utils::init( $flowFile, \@TRACKING_FIELDS );
    oe_new_job('--index');
    treat_flow( $trk, $flowFile );
    track_end($trk);
    oe_compo_link();
    return 0;
}

exit main(@ARGV);
