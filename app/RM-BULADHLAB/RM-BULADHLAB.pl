use utf8;
use strict;
use warnings;

#******************************************************************************
#------------------------------ LIBRAIRIES ------------------------------------
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Data::Dumper;
use Readonly;

#******************************************************************************
#-------------------------- VARIABLES TEMPORAIRES -----------------------------
my ($TRK);
my $NBR_BENEF     = 0;
my $PRESENCE_CONJ = 1;
my $PRESENCE_ENF  = 1;
my %H_BENEF       = ();

#******************************************************************************
#-------------------- INITIALISATION VARIABLES CONSTANTES ---------------------
Readonly my $NBR_BENEF_MAX => 4;
Readonly my $TYPCONJ       => 'Conjoint';
Readonly my $TYPENF        => 'Enfant';
Readonly my $TYPAUTR       => 'Autre';

Readonly my $BNOM     => 'benefnom';
Readonly my $BPRENOM  => 'benefprenom';
Readonly my $BDTNAISS => 'benefdtnaiss';
Readonly my $BSS      => 'benefss';
Readonly my $BSEXE    => 'benefsexe';
Readonly my $BTYPE    => 'beneftype';

Readonly my @TAB_LIB => ( $BNOM, $BPRENOM, $BDTNAISS, $BSS, $BSEXE, $BTYPE );
Readonly my @TAB_TYP => ( $TYPCONJ, $TYPENF, $TYPAUTR );

#******************************************************************************
#------------- INITIALISATION TABLE FORMATAGE SITUATION FAMILIALE -------------
Readonly my %H_FAM => (
    "Célibataire" => 1,
    "Marié(e)"    => 2,
    "Vie Maritale" => 3,
    "Divorcé(e)"  => 4,
    "Veuf(ve)"     => 5
);

#******************************************************************************
#-------------------- INITIALISATION TABLE FORMATAGE MOIS ---------------------
Readonly my %H_FORMAT_MOIS => (
    "00" => 0,
    "01" => 1,
    "02" => 2,
    "03" => 3,
    "04" => 4,
    "05" => 5,
    "06" => 6,
    "07" => 7,
    "08" => 8,
    "09" => 9,
    "10" => 10,
    "11" => 11,
    "12" => 12
);

#******************************************************************************
#----------------- INITIALISATION TABLE FORMATAGE CHIFFRE ---------------------
Readonly my %H_FORMAT_LET => (
    1 => "Un",
    2 => "Deux",
    3 => "Trois",
    4 => "Quatre",
);

#******************************************************************************
#-------------------- INITIALISATION TABLE FORMATAGE SEXE ---------------------
Readonly my %H_FORMAT_SEXE => (
    "F" => 1,
    "M" => 2,
    "H" => 2
);

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------- FONCTION PRINCIPALE -----------------------------------
sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        entity => oe_corporation_set(),
        keys   => [ 'MATRICUL', 'NOMDEST', 'CPAGENCE' ]
    );

    oe_new_job('--index');

    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'BADH' );
    $doc->append( 'xSOURCE', 'coheris' );
    $doc->append( 'xOWNER',  'coheris' );
    my $data = xml_open( \*IN );
    print Dumper($data);
    edit_index( $data, \$doc );
    edit_bulletin( $data, \$doc );

    oe_print_to_output_file("$doc");

    oe_compo_link();
    $TRK->track( 'W', 1, '', '', '', 'Fin de traitement' );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------FONCTION EDITION BULLETIN -------------------------------
sub edit_bulletin {
    my ( $data, $doc ) = @_;

    #--------------------------------------------------------------------------
    #------------------------- VERIFICATION DATE D'EFFET ----------------------
    verif_DateEffet($data);

    #--------------------------------------------------------------------------
    #----------------------------- TRACKING VALUES ----------------------------
    trackingValues($data);

    #--------------------------------------------------------------------------
    #------------------ EDITION INFOS MNT PERSONNELLES ET GARANTIES -----------
    edit_InfoMNT( $data, \$$doc );
    edit_InfoPerso( $data, \$$doc );
    edit_InfoGarantie( $data, \$$doc );

    #--------------------------------------------------------------------------
    #------------------------ EDITION DES BENEFICIAIRES------------------------
    ## -- Lecture des beneficiaires
    lecture_beneficiaire($data);
    ## -- Edition des beneficiaires
    edit_beneficiaire( \$$doc );

    #--------------------------------------------------------------------------
    #------------------------------ FIN DOCUMENT ------------------------------
    $$doc->append('finbulletin');

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------- FONCTION EDITION DES INFOS DE LA MNT -----------------------
sub edit_InfoMNT {
    my ( $data, $doc ) = @_;
    my ( $codePromo, $nbrTmp, @TabPromo );

    #---------------------------------------------
    #------------------------- Controle Code Promo
    $codePromo = $data->{'code_promo'} || "";
    @TabPromo = split( "-", $codePromo, -1 );
    $nbrTmp = @TabPromo;
    die "Error: Le format de la balise <<code_promo>> n'est pas correcte.\n"
      unless ( exists( $H_FORMAT_MOIS{ $TabPromo[2] } ) && $nbrTmp > 2 );

    #---------------------------------------------
    #------------------------- Edition data
    $$doc->append( 'codeOperation', $TabPromo[1] );
    $$doc->append( 'nbrMoisOffert', $H_FORMAT_MOIS{ $TabPromo[2] } );
    $$doc->append( 'numCollectif',
        substr( $data->{'num_contrat_collectif'} || "", 0, 15 ) );
    $$doc->append( 'numGroupeAssure', $data->{'groupe_assures'} || "" );
    $$doc->append( 'idMNT', substr( $data->{'id_mnt'} || "", 0, 8 ) );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------- FONCTION EDITION DES INFOS PERSONNELLES ----------------------
sub edit_InfoPerso {
    my ( $data, $doc ) = @_;
    my $numFam    = -1;
    my $adresse   = $data->{'prct_adr'} || $data->{'pers01_adr'} || "";
    my $ville     = $data->{'prct_ville'} || $data->{'pers01_ville'} || "";
    my $employeur = $data->{'employeur_nom'} || $data->{'employeur'} || "";
    my $situatFam =
      $data->{'pers_01_situation'} || $data->{'pers01_situation_famille'} || "";
    my $civilite   = $data->{'prct_civ'} || $data->{'pers01_civ'} || "";
    my $codePostal = $data->{'prct_cp'}  || $data->{'pers01_cp'}  || "";
    my $teDomicile =
      $data->{'tel_domicile'} || $data->{'pers01_tel_domicile'} || "";
    my $teMobile = $data->{'tel_mobile'} || $data->{'pers01_tel_mobile'} || "";
    my $dtEffet  = $data->{'date_effet'} || $data->{'dt_effet'}          || "";
    my $mail     = $data->{'email'}      || $data->{'pers01_email'}      || "";

    my @TabEmail = format_eMail( $data, $mail );
    if ( exists( $H_FAM{$situatFam} ) ) { $numFam = $H_FAM{$situatFam}; }
    $$doc->append( 'persNom', substr( $data->{'pers01_nom'} || "", 0, 26 ) );
    $$doc->append( 'persNomJf',
        substr( $data->{'pers_01_nom_jf'} || "", 0, 26 ) );
    $$doc->append( 'persPrenom',
        substr( $data->{'pers01_prenom'} || "", 0, 26 ) );

    $$doc->append( 'persVille',    substr( $ville,     0, 56 ) );
    $$doc->append( 'employeurNom', substr( $employeur, 0, 39 ) );
    $$doc->append( 'persAdr',      substr( $adresse,   0, 78 ) );
    $$doc->append( 'persFam',      $numFam );
    $$doc->append( 'dateEffet',    $dtEffet );
    $$doc->append( 'persCiv',      $civilite );
    $$doc->append( 'persCp',       $codePostal );
    $$doc->append( 'persTelDom',   $teDomicile );
    $$doc->append( 'persTelMobile', $teMobile );
    $$doc->append( 'employeurFil',
        substr( $data->{'employeur_filiere'} || "", 0, 22 ) );
    $$doc->append( 'persProfession',
        substr( $data->{'pers01_profession'} || "", 0, 24 ) );
    $$doc->append( 'persMatricule',
        substr( $data->{'pers01_matricule'} || "", 0, 20 ) );
    $$doc->append( 'persDteNais',   $data->{'pers01_dt_naiss'}        || "" );
    $$doc->append( 'persRegime',    $data->{'pers01_regime'}          || "" );
    $$doc->append( 'persSecu',      $data->{'pers01_ss'}              || "" );
    $$doc->append( 'dateEmbauche',  $data->{'date_embauche'}          || "" );
    $$doc->append( 'persSatut',     $data->{'pers01_statut'}          || "" );
    $$doc->append( 'persCategorie', $data->{'pers01_categorie'}       || "" );
    $$doc->append( 'modalitePaie',  $data->{'modalite_paiement'}      || "" );
    $$doc->append( 'numSiret',      $data->{'siret'}                  || "" );
    $$doc->append( 'optinMNT',      $data->{'optin-email-mnt'}        || "" );
    $$doc->append( 'optinPart',     $data->{'optin-email-partenaire'} || "" );
    $$doc->append( 'persEmailLocal', $TabEmail[0] );
    $$doc->append( 'persEmailDNS',   $TabEmail[1] );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------- FONCTION EDITION DES INFOS GARANTIES --------------------------
sub edit_InfoGarantie {
    my ( $data, $doc ) = @_;
    my $choixOffre =
      $data->{'offre_selectionnee'} || $data->{'garanties'} || "";
    my $cotisMens =
      $data->{'devis_cotis_mens'} || $data->{'cotis_option'} || "";
    if ( $choixOffre eq "" ) {
        die "Erreur: Le choix de la garantie est obligatoire.\n";
    }
    $$doc->append( 'offreSelect', $choixOffre );
    $$doc->append( 'cotisMens',   $cotisMens );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------- FONCTION LECTURE DES BENEFICIAIRES -----------------------
sub lecture_beneficiaire {
    my ($data) = @_;
    if ( $data->{'beneficiaires'} ne "" ) {
        if ( defined( $data->{'beneficiaires'}->{'beneficiaire'} ) ) {
            if ( ref( $data->{'beneficiaires'}->{'beneficiaire'} ) eq "ARRAY" )
            {    ### Il y a plusieurs bénéficiaires à stocker
                foreach
                  my $key ( @{ $data->{'beneficiaires'}->{'beneficiaire'} } )
                {
                    $NBR_BENEF++;
                    stock_beneficiaire($key);
                }
            }
            else {    ### Il n'y a qu'un bénéficiaire
                $NBR_BENEF++;
                stock_beneficiaire(
                    $data->{'beneficiaires'}->{'beneficiaire'} );
            }
        }
    }
    else { warn "pas de bénéficiaire"; }
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------- FONCTION EDITION DES BENEFICIAIRES ----------------------------
sub edit_beneficiaire {
    my ($doc) = @_;
    my ( $ittype, $itnum, $itlib, $cptBenef, $nbrBenefAff );
    my ( @tabNum, @tabData );
    $cptBenef = 0;
    for ( $ittype = 0 ; $ittype < @TAB_TYP ; $ittype++ ) {
        @tabNum = sort keys( %{ $H_BENEF{ $TAB_TYP[$ittype] } } );
        for ( $itnum = 0 ; $itnum < @tabNum ; $itnum++ ) {
            $cptBenef++;
            if ( $cptBenef <= $NBR_BENEF_MAX ) {
                @tabData = $H_BENEF{ $TAB_TYP[$ittype] }{ $tabNum[$itnum] };
                for ( $itlib = 0 ; $itlib < @TAB_LIB ; $itlib++ ) {
                    $$doc->append(
                        $TAB_LIB[$itlib] . $H_FORMAT_LET{$cptBenef},
                        $H_BENEF{ $TAB_TYP[$ittype] }{ $tabNum[$itnum] }
                          { $TAB_LIB[$itlib] }
                    );
                }
            }
        }
    }

    ## -- Message avertissement
    if ( $NBR_BENEF > $NBR_BENEF_MAX ) {
        if ( $PRESENCE_CONJ == 0 || $PRESENCE_ENF == 0 ) {
            $nbrBenefAff = $NBR_BENEF_MAX;
        }
        else {
            $nbrBenefAff = $NBR_BENEF_MAX - 1;
        }
        warn
"Attention: Seuls les $nbrBenefAff premiers beneficiaires seront affiches.\n";
    }
    else {
        $nbrBenefAff = $NBR_BENEF;
    }
    $$doc->append( 'nbrBenef',    $nbrBenefAff );
    $$doc->append( 'benefEnfant', $PRESENCE_ENF );
    $$doc->append( 'benefConj',   $PRESENCE_CONJ );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------- FONCTION STOCKAGE DES BENEFICIAIRES ----------------------
sub stock_beneficiaire {
    my ($data) = @_;
    my ( $nom, $prenom, $typebenef, $numsexe, $sexe, $dtenaiss, $secu, $type );

    $type   = $data->{'pers_role'} || $data->{'pers_type_benef'} || "";
    $nom    = ucfirst( lc( $data->{'pers_nom'} ) );
    $nom    = substr( $nom, 0, 16 );
    $prenom = ucfirst( lc( $data->{'pers_prenom'} ) );
    $prenom =~ s/(\w+)(\s+)(\w+)/ucfirst($1).$2.ucfirst($3)/e;
    $prenom = substr( $prenom, 0, 16 );
    $dtenaiss = $data->{'pers_dt_naiss'} || "";
    $secu     = $data->{'pers_ss'}       || "";
    $sexe     = $data->{'pers_sexe'}     || "";
    $numsexe  = -1;
    if ( substr( $type, 0, 6 ) eq $TYPENF ) {
        $type = substr( $type, 0, 6 );
    }
    if ( exists( $H_FORMAT_SEXE{$sexe} ) ) {
        $numsexe = $H_FORMAT_SEXE{$sexe};
    }

    if ( $type eq $TYPCONJ ) {
        $typebenef     = $TYPCONJ;
        $PRESENCE_CONJ = 0;
    }
    elsif ( $type eq $TYPENF ) {
        $typebenef    = $TYPENF;
        $PRESENCE_ENF = 0;
    }
    else {
        $typebenef = $TYPAUTR;
    }

    $H_BENEF{$typebenef}{$NBR_BENEF}{$BNOM}     = $nom;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BPRENOM}  = $prenom;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BDTNAISS} = $dtenaiss;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BSS}      = $secu;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BSEXE}    = $numsexe;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BTYPE}    = $typebenef;

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------- FONCTION FORMATAGE ADRESSE MAIL -------------------------
sub format_eMail {
    my ( $data, $adresMail ) = @_;
    my @tab = ( "", "" );
    if ( $adresMail ne "" ) {
        @tab = split( '@', $adresMail, -1 );
        my $nbrTmp = @tab;
        die
          "Error: Le format de l'adresse mail($adresMail) n'est pas correcte.\n"
          unless ( $nbrTmp == 2 );
    }
    return @tab;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------------- FONCTION VERIFICATION DATE EFFET ----------------------
sub verif_DateEffet {
    my ($data) = @_;
    my $dtEffet = $data->{'date_effet'} || $data->{'dt_effet'} || "";
    my @splitDate = split( '/', $dtEffet, -1 );
    my $nbr = @splitDate;
    die "Error: La date d'effet est de format(JJ/MM/AAAA) et est obligatoire.\n"
      unless ( $nbr == 3 );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------------- FONCTION TRACKING ---------------------------------
sub trackingValues {
    my ($data) = @_;
    $TRK->track(
        'Doc', 1,
        $data->{'pers01_matricule'},
        $data->{'pers01_nom'} . " " . $data->{'pers01_prenom'},
        $data->{'nom_sectionRattache'}
    );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------------- REMPLISSAGE FICHIER INDEX -------------------------
sub edit_index {
    my ( $data, $doc ) = @_;
    $$doc->append( 'xIDDEST', $data->{'num_contrat_collectif'} || "" );
    $$doc->append( 'xNOMDEST',
        $data->{'pers01_nom'} . " " . $data->{'pers01_prenom'} );
    $$doc->append( 'xVILDEST', $data->{'pers01_ville'}        || "" );
    $$doc->append( 'xCPDEST',  $data->{'pers01_cp'}           || "" );
    $$doc->append( 'xIDEMET',  $data->{'nom_sectionRattache'} || "" );
    $$doc->append_table( 'xADRLN', $data->{'pers01_adr'} || "" );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------- EXIT MAIN ---------------------------------
exit main(@ARGV);
