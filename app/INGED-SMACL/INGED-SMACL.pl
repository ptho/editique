use utf8;
use strict;
use warnings;

use oEdtk::Main 0.50;
use oEdtk::Config qw(config_read);
use oEUser::Lib;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Sys::Hostname;
use Data::Dumper;
use File::Basename;
use MIME::Base64;
use Time::Piece;

my ($TRK);

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );

    my $cfg = config_read('COMSET');

#	my $fluxbasename=substr(basename($argv[0]), 0, rindex(basename($ARGV[0]), '.'));
    my $fluxbasename = "INGED_SMACL";
    my $idGed        = oe_ID_LDOC;

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        entity => oe_corporation_set(),
        keys   => [ 'NOMPDF', 'CODEPERS' ]
    );

    #	oe_new_job('--index');

    my $data = xml_open( \*IN );
    print Dumper($data);

    my $pdfdecoded  = decode_base64( $data->{'document'}->{'flux'} );
    my $pdffilename = $fluxbasename . "_" . $idGed . "_0000001.pdf";
    open my $pdffile, '>', "$pdffilename";
    binmode $pdffile;
    print $pdffile $pdfdecoded;
    close $pdffile;

    $pdffilename = "INGED-SMACL.pdf";
    open $pdffile, '>', "$pdffilename";
    binmode $pdffile;
    print $pdffile $pdfdecoded;
    close $pdffile;

    print localtime->strftime('%Y%m%d') . "\n";

    ### À supprimer à terme => Plus besoin dans la nouvelle API Éditique, elle lit les idx1
    ### Create idx
    my $idxName = $fluxbasename . "_" . $idGed . ".idx";
    open my $namefile, '>', "$idxName";
    ### Format IDX : Nom de l'État;EJ;JE_SAIS_PAS_Nom_de_luser?;ID_DOC;DATE_ISO;
    print $namefile $fluxbasename
      . ";MNT;coheris;"
      . $idGed
      . "_0000001;"
      . localtime->strftime('%Y%m%d') . ";"
      . $data->{'document'}->{'nom'} . ";;"
      . $data->{'devis'}->{'code_personne'}
      . ";;;;;;coheris;$pdffilename\n";
    close $namefile;

    ### Create idx1
    my $idx1Name = "INGED-SMACL" . "." . $idGed . ".idx1";
    open my $IDXFILE, ">$idx1Name";
### Format IDX :
    # Nom de l'état;
    # ID_DOC;
    # NUM_PAGE_DANS_DOC(toujours 1 ici);
    # NUM_DOC_DANS_PDF(toujours 1 ici);
    # CP_DEST;
    # VILLE_DEST;
    # ID_DEST;
    # NOM_DEST;
    # NUM_AGENCE;
    # DATE_EDITION;
    # TYPE_ENV;
    # Porte_Adresse;
    # ADRESSE_DEST_L1;
    # CLEF_GED_1;
    # ADRESSE_DEST_L2;
    # CLEF_GED_2;
    # ADRESSE_DEST_L3;
    # CLEF_GED_3;
    # ADRESSE_DEST_L4;
    # CLEF_GED_4;
    # ADRESSE_DEST_L5;
    # ENTITE_JURIDIQUE;
    # DOCLIB (vide car pas de découpage lotissement);
    # REF_PREIMP (vide, pas de preimp ici);
    # ADRESSE_DEST_L6;
    # DOMAINE_MÉTIER;
    # DEMANDEUR DE L'ÉDITION;
    # NOM_DU_SERVEUR_D'ÉDITION;
    # VERSION_IDX(NORMALEMENT oEdtk04);
    # CATHÉGORIE_DU_DOC;
    # CODE_RUPTURE
    print $IDXFILE "INGED-SMACL;$idGed;1;1;;;"
      . $data->{'devis'}->{'num_societaire'} . ";;;"
      . localtime->strftime('%Y%m%d') . ";"
      . substr( config_read()->{'EDTK_TYPE_ENV'}, 0, 1 ) . ";;;"
      . $data->{'document'}->{'nom'} . ";;"
      . $data->{'devis'}->{'code_personne'}
      . ";;;;;;SMACL;;;;;SMACL;"
      . hostname()
      . ";oEdtk04;;;DEVI";

#	$fluxbasename.";MNT;coheris;".$idGed."_0000001;".localtime->strftime('%Y%m%d').";".$data->{'document'}->{'nom'}.";;".$data->{'devis'}->{'code_personne'}.";;;;;;coheris;$pdffilename\n";
    close $IDXFILE;

    $TRK->track(
        'Doc', 1,
        $data->{'document'}->{'nom'},
        $data->{'devis'}->{'code_personne'}
    );

    #	oe_compo_link();
    $TRK->track( 'W', 1, '', '', '', '', 'Fin de traitement' );
    return 0;
}

exit main(@ARGV);
