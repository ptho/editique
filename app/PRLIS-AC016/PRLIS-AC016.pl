#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;    # utilisation d'une librairie de fonctions standards
use oEdtk::libXls 0.46
  ; # utilisation d'une librairie de fonctions standards dédiée à la production Excel
use strict;
use File::Basename;
use oEdtk::Tracking;

my $TRK;

#############################################################################
# cinématique suivante :
#o	C1 : Entête 1, standard collectif, pour chaque collectivité
#o	C2 : Entête 2, standard collectif, pour chaque C1
#o	E3 : Entête 3, pour chaque C1
#o	F5 : Entête de l'édition des groupes d'assurés, de 1 à n pour chaque C1
#o	L3 : informations concernant le Contrat Individuel, de 1 à n pour chaque F5
#o	L4 - informations sur les éléments de prime, de 0 à n par L3
#o	L5 - détail des frais du contrat collectif, de 0 à n par L3
#o	L6 - informations sur les cotisations particulières, de 0 à n par L3
#o	L7 - informations sur les arrondis du contrat individuel, de 0 à n par L3
#o	T3 - informations sur la part du groupe d'assurés, de 1 par F5
#o	T4 - total des cotisations par garantie technique, de 1 à n par F5
#o	T5 - total des frais sur la part, de 1 par F5
#o	T6 - total des cotisations particulières, de 1 par F5
#o	T7 - total des arrondis, de 1 par F5

#################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
#################################################################################

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)
my $compteurL4Adh     = 0;
my $Garanties         = "";
my $Reference_ACH1NUM = "";
my $CompteurC1        = 0;    # INITIALISATION DÉCOMPTE COLLECTIVITÉS
my $compteurL4Adh     = 0;
my $cptAdhL3          = 0;    # Initialisaton du compteur L3
my $CumulAP           = 0;
my $CumulAD           = 0;
my $CumulCB           = 0;
my $CumulIJ           = 0;
my $CumulL6           = 0;
my $CumulLib          = "";
my $CumulLibPGMb      = "";
my $CumulLibPGMc      = "";
my $CumulLibPGMi      = "";
my $CumulPB           = 0;
my $CumulPC           = 0;
my $cumulTotAgtLot    = 0;    # CUMUL GENERAL DES AGENTS POUR LE LOT
my $cumulTotGLot      = 0;    # CUMUL GÉNÉRAL POUR CONTROL DU LOT
my $EcheanceAnterieur = 0;
my $EcheanceAntEvol   = 0;
my $eL3_RAPPEL        = 0;
my $eL3_TPERCU        = 0;
my $RestAnt           = 0;
my $TrpcAnt           = 0;
my $ent_pour_XLS      = "";
my $F5_DEBPERx        = "";
my $F5_LIBLON         = "";
my $flagF5            = 0;    # Initialisation flag groupe d'assurés
my $Garanties         = "";
my $ID_Accc026        = "";
my $ID_Codgrax        = "";
my $ID_Collectivite   = "";
my $matricule         = "";
my $NomPrenom         = "";
my $NomPrenomIdx      = "";
my $Reference_ACH1NUM = "";
my $TotalGlobal       = 0;
my $TotalGlobalCB     = 0;
my $TotalGlobalIJ     = 0;
my $TotalGlobalL6     = 0;
my $TotalGlobalPB     = 0;
my $TotalGlobalPC     = 0;
my $TotalL4           = 0;
my $TotalPercus       = 0;
my $TotalRappels      = 0;
my %cumulParGarantie;
my @ordreDeGarantie;
my $NumRO     = "";
my $CleRO     = "";
my $NumSS     = "";
my $Taux      = "";
my $Base      = "";
my $CumulTaux = "";
my $CumulBase = "";
my $lastnumco = -1;
my $CPTDIFF   = 0;
my $CPTNUMCO  = 0;

# pour reprises contrôles avec sorties Excel
my $CTRL_DAT = "";
my $ODER022  = "";
my $CODPARx  = "";
my $PEN004a  = "";

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    my $ctrl_idx2 = basename($0);
    $ctrl_idx2 =~ s/\.pl$/.idx2/i;
    my $user = user_get_aneto_user( $argv[0] );

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDDEST', 'AGENCE' ]
    );

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );
    oe_open_fo_OUT($ctrl_idx2);

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    &initApp();

    while ( my $ligne = <IN> ) {
        chomp($ligne);

        if ( oe_process_ref_rec( 0, 6, $ligne, 0, 3034 ) ) {

            # FIN traitement enregistrement(s) : ENTETE

        }
        elsif ( oe_process_ref_rec( 158, 2, $ligne, 168, 3034 ) ) {

  # FIN traitement enregistrement(s) : L5 L6 C1 T3 L7 L4 T6 T4 T7 F5 L3 T5 C2 E3

        }
        else {
            # TYPE D'ENREGISTREMENT INCONNU
            #warn "INFO : IGNORE REC. line $.\n";
        }

    }
    &finCollectivite();
    $TRK->track( 'Doc', 1, '', 'SIEGE' );

#print OUT "\n<SK>CONTROLES cumulTotGLot=$cumulTotGLot - cumulTotAgtLot=$cumulTotAgtLot\n";
    if ( $cumulTotGLot == 0 && $cumulTotAgtLot == 0 ) { exit 99; }
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################
sub initApp() {
    oe_rec_motif( 'ENTETE',
'A06 A10 A10 A2 A30 A30 A30 A30 A5 A2 A2 A8 A8 A5 A50 A30 A8 A30 A150 A150 A150 A10 A8 A15 A15 A15 A15 A15 A010 A015 A010 A015 A050 A020 A032 A032 A009 A002 A015 A005 A015 A032 A001 A032 A032 A010 A032 A003 A015 A004 A005 A015 A050 A005 A015 A050 A005 A015 A050 A099 A010 A015 A010 A015 A050 A020 A032 A032 A009 A002 A015 A005 A015 A032 A001 A032 A032 A010 A032 A003 A015 A004 A005 A015 A050 A005 A015 A050 A005 A015 A050 A099 A010 A015 A010 A015 A050 A020 A032 A032 A009 A002 A015 A005 A015 A032 A001 A032 A032 A010 A032 A003 A015 A004 A005 A015 A050 A005 A015 A050 A005 A015 A050 A099 A010 A015 A010 A015 A050 A020 A032 A032 A009 A002 A015 A005 A015 A032 A001 A032 A032 A010 A032 A003 A015 A004 A005 A015 A050 A005 A015 A050 A005 A015 A050 A099 A*'
    );
    oe_rec_process( 'ENTETE', \&prepENT );

    oe_rec_pre_process( 'C1', \&initDoc );
    oe_rec_motif( 'C1',
'A02 A06 A10 A15 A50 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A02 A09 A10 A15 A50 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A05 A15 A50 A02 A09 A10 A15 A50 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A05 A15 A50 A02 A09 A10 A15 A10 A15 A50 A20 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A05 A15 A50 A08 A08 A08 A08 A*'
    );
    oe_rec_process( 'C1', \&prepC1 );

    oe_rec_motif( 'C2',
'A02 A08 A06 A14 A10 A15 A50 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A08 A15 A50 A15 A15 A15 A50 A08 A09 A08 A08 A4 A03 A15 A50 A50 A10 A15 A50 A15 A08 A10 A15 A10 A15 A50 A20 A02 A09 A10 A15 A50 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A05 A15 A50 A02 A09 A50 A04 A15 A50 A04 A15 A50 A8 A8 A8 A8 A3 A08 A08 A08 A08 A08 A*'
    );
    oe_rec_process( 'C2', \&prepC2 );

    oe_rec_motif( 'E3',
'A10 A2 A250 A2 A10 A15 A10 A15 A50 A20 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A2 A9 A10 A15 A50 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A5 A15 A50 A5 A15 A50 A5 A15 A50 A2 A9 A10 A15 A50 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A5 A15 A50 A5 A15 A50 A5 A15 A50 A2 A9 A10 A15 A10 A15 A50 A20 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A5 A15 A50 A5 A15 A50 A5 A15 A50 A8 A8 A8 A8 A2 A8 A10 A15 A10 A15 A50 A20 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A8 A15 A50 A15 A6 A10 A15 A10 A15 A50 A20 A14 A15 A15 A15 A50 A8 A9 A8 A8 A4 A4 A1 A1 A6 A1 A1 A15 A1 A10 A15 A50 A14 A10 A15 A50 A2 A15 A50 A2 A15 A50 A2 A15 A50 A2 A15 A50 A8 A8 A2 A15 A50 A15 A8 A10 A15 A10 A15 A50 A20 A5 A15 A50 A5 A15 A50 A5 A15 A50 A2 A9 A10 A15 A50 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A5 A15 A50 A5 A15 A50 A5 A15 A50 A2 A9 A50 A4 A15 A50 A4 A15 A50 A8 A8 A8 A8 A3 A8 A8 A8 A8 A8 A10 A15 A10 A15 A50 A20 A50 A10 A10 A20 A2 A4 A3 A15 A50 A3 A15 A50 A8 A15 A50 A15 A15 A15 A50 A8 A9 A8 A8 A4 A3 A15 A50 A50 A6 A6 A6 A3 A6 A10 A15 A50 A30 A8 A8 A8 A8 A1 A2 A30 A10 A15 A10 A15 A50 A20 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A5 A15 A50 A5 A15 A50 A5 A15 A50 A50 A15 A250 A50 A10 A15 A10 A15 A50 A20 A32 A32 A9 A2 A15 A5 A15 A32 A1 A32 A32 A10 A32 A3 A15 A4 A5 A15 A50 A5 A15 A50 A5 A15 A50 A*'
    );

    oe_rec_motif( 'F5',
'A06 A10 A15 A50 A14 A08 A08 A30 A1 A06 A10 A15 A50 A5 A03 A15 A50 A02 A15 A50 A02 A15 A50 A02 A15 A50 A02 A15 A50 A08 A08 A02 A15 A50 A06 A10 A15 A50 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A50 A10 A10 A20 A02 A04 A03 A15 A03 A15 A08 A08 A5 A6 A2 A*'
    );
    oe_rec_process( 'F5', \&prepF5 );

    oe_rec_motif( 'L3',
'A5 A15 A15 A8 A15 A14 A14 A8 A10 A15 A10 A15 A50 A20 A32 A32 A09 A02 A15 A05 A15 A32 A01 A32 A32 A10 A32 A03 A15 A04 A01 A14 A14 A01 A14 A01 A14 A14 A14 A*'
    );
    oe_rec_process( 'L3', \&prepL3 );

    oe_rec_motif( 'LJ', 'A05 A15 A02 A10 A10 A08 A08 A06 A15 A50 A14 A14 A*' );
    oe_rec_process( 'LJ', \&prepLJ );

    oe_rec_motif( 'L4',
'A5 A15 A14 A14 A08 A08 A08 A10 A15 A10 A15 A50 A20 A06 A15 A50 A02 A10 A12 A15 A50 A04 A15 A50 A05 A15 A50 A08 A08 A08 A02 A13 A02 A08 A08 A02 A03 A04 A01 A08 A08 A01 A01 A08 A08 A10 A15 A50 A10 A15 A50 A05 A08 A08 A03 A04 A15 A50 A04 A15 A50 A05 A15 A10 A10 A*'
    );
    oe_rec_process( 'L4', \&prepL4 );

    oe_rec_motif( 'L5',
'A5 A15 A14 A14 A08 A02 A15 A50 A08 A08 A05 A08 A08 A03 A04 A15 A50 A04 A15 A50 A05 A*'
    );
    oe_rec_process( 'L5', \&prepL5 );

    oe_rec_motif( 'L6',
'A5 A15 A14 A14 A08 A09 A15 A50 A06 A15 A50 A01 A15 A50 A05 A15 A50 A05 A08 A08 A03 A04 A15 A50 A04 A15 A50 A05 A*'
    );
    oe_rec_process( 'L6', \&prepL6 );

    oe_rec_motif( 'L7',
'A5 A15 A14 A14 A08 A01 A15 A50 A05 A08 A08 A03 A04 A15 A50 A04 A15 A50 A05 A*'
    );
    oe_rec_process( 'L7', \&prepL7 );

    oe_rec_motif( 'T3',
'A5 A15 A14 A14 A01 A14 A01 A14 A01 A14 A01 A02 A01 A14 A14 A01 A14 A14 A*'
    );
    oe_rec_process( 'T3', \&prepT3 );

    oe_rec_motif( 'T4',
        'A5 A15 A14 A14 A08 A10 A15 A50 A01 A14 A01 A14 A01 A14 A01 A02 A*' );
    oe_rec_process( 'T4', \&prepT4 );

    oe_rec_motif( 'T5',
        'A5 A15 A14 A14 A08 A02 A15 A50 A01 A14 A01 A14 A01 A14 A01 A02 A*' );

    oe_rec_motif( 'T6',
'A5 A15 A14 A14 A08 A09 A15 A50 A06 A15 A50 A01 A15 A50 A05 A15 A50 A01 A14 A01 A14 A01 A14 A01 A02 A*'
    );
    oe_rec_process( 'T6', \&prepT6 );

    oe_rec_motif( 'T7',
        'A5 A15 A14 A14 A08 A01 A15 A50 A01 A14 A01 A14 A01 A14 A01 A02 A*' );

    # INITIALISATION PROPRE AU DOCUMENT
    &initXls();
    &initDoc();
    1;
}

sub initXls {

    #	my $fichier=$ARGV[1];

    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ; NR7 = Numérique Right...  )
    prod_Xls_Col_Init(
        'AC18', 'AC15', 'AC15', 'AL25', 'AC19', 'AL19', 'NR08', 'NR08',
        'NR08', 'NR10', 'AC05', 'NR08', 'NR08'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init( "", "ETAT DES PRELEVEMENTS SUR SALAIRE" )
      ;    #  ,"ENTETE DROIT");

# INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
# PRÉPARATION DES TITRES DE COLONNES
# AGENCE; CONTRAT; COL; GROUPE; CIV; NOM ; DATNAI; PRéNOM ; NOV; BTQ; NTV; LBV; AD2;  LOC; CPS; BDS
    prod_Xls_Insert_Val("Identification");
    prod_Xls_Insert_Val("Matricule");
    prod_Xls_Insert_Val("N° SS");
    prod_Xls_Insert_Val("Nom et Prénom");
    prod_Xls_Insert_Val("Références");
    prod_Xls_Insert_Val("Garantie");
    prod_Xls_Insert_Val("Mont. garantie");
    prod_Xls_Insert_Val("Rappel");
    prod_Xls_Insert_Val("Trop payé");
    prod_Xls_Insert_Val("Total");
    prod_Xls_Insert_Val("Ecart");
    prod_Xls_Insert_Val("Taux");
    prod_Xls_Insert_Val("Base");
    prod_Xls_Row_Height(32);

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );

    1;
}

sub initDoc() {
    if ( $CompteurC1 > 0 ) {
        &finCollectivite();
    }
    $compteurL4Adh = 0;
    $TotalGlobal   = 0;
    $TotalGlobalCB = 0;
    $TotalGlobalPB = 0;
    $TotalGlobalPC = 0;
    $TotalGlobalIJ = 0;
    $TotalGlobalL6 = 0;
    $TotalRappels  = 0;
    $TotalPercus   = 0;
    $cptAdhL3      = 0;

    &initAdh();

    $CompteurC1++;
    $flagF5 = 0;    # Initialisation flag groupe d'assurés
    1;
}

sub initAdh {
    $matricule         = "";
    $eL3_RAPPEL        = 0;
    $eL3_TPERCU        = 0;
    $RestAnt           = 0;
    $TrpcAnt           = 0;
    $compteurL4Adh     = 0;
    $CumulAP           = 0;
    $CumulAD           = 0;
    $CumulL6           = 0;
    $CumulCB           = 0;
    $CumulPB           = 0;
    $CumulPC           = 0;
    $CumulIJ           = 0;
    $CumulLib          = "";
    $CumulLibPGMi      = "";
    $CumulLibPGMc      = "";
    $CumulLibPGMb      = "";
    $TotalL4           = 0;
    $NomPrenom         = "";
    $Reference_ACH1NUM = "";
    $Garanties         = "";
    $Taux              = "";
    $Base              = "";
    $CumulTaux         = "";
    $CumulBase         = "";
    undef %cumulParGarantie;
    undef @ordreDeGarantie;
    1;
}

sub deb_Collectivite_Xls () {
    prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
    prod_Xls_Insert_Val($F5_LIBLON);
    prod_Xls_Insert_Val($ID_Collectivite);
    prod_Xls_Insert_Val( "", "", "", "", "" );
    prod_Xls_Insert_Val("prél. du $ent_pour_XLS");
    prod_Xls_Insert_Val( "éch. du $F5_DEBPERx", "", "", "" );
    prod_Xls_Edit_Ligne("T2");

    1;
}

sub finCollectivite() {
    my $LibMont = "<#LibMont=";
    my $ValMont = "<#ValMont=";

    # CLOTURE DU DERNIER ADHÉRENT
    &trtTotAdh();

    # VENTILATION DES SOUS TOTAUX PAR FAMILLE
    if ( $TotalGlobalCB != 0 ) {
        $LibMont = $LibMont . "Montant total des garanties :<Fl>";
        $ValMont = $ValMont . sprintf "<Set>%0.2f<Edit><Fl>", $TotalGlobalCB;
    }
    if ( $TotalGlobalPB != 0 ) {
        $LibMont = $LibMont . "Montant prévoyance de base :<Fl>";
        $ValMont = $ValMont . sprintf "<Set>%0.2f<Edit><Fl>", $TotalGlobalPB;
    }
    if ( $TotalGlobalPC != 0 ) {
        $LibMont = $LibMont . "Montant prévoyance complémentaire :<Fl>";
        $ValMont = $ValMont . sprintf "<Set>%0.2f<Edit><Fl>", $TotalGlobalPC;
    }
    if ( $TotalGlobalIJ != 0 ) {
        $LibMont = $LibMont . "Montant indemnité journalière :<Fl>";
        $ValMont = $ValMont . sprintf "<Set>%0.2f<Edit><Fl>", $TotalGlobalIJ;
    }
    if ( $TotalGlobalL6 != 0 ) {
        $LibMont = $LibMont . "Montant cotisation exceptionnelle :<Fl>";
        $ValMont = $ValMont . sprintf "<Set>%0.2f<Edit><Fl>", $TotalGlobalL6;
    }
    if ( $TotalRappels != 0 ) {
        $LibMont = $LibMont . "Montant Total Rappel :<Fl>";
        $ValMont = $ValMont . sprintf "<Set>%0.2f<Edit><Fl>", $TotalRappels;
    }
    if ( $TotalPercus != 0 ) {
        $LibMont = $LibMont . "Montant Total trop perçu :<Fl>";
        $ValMont = $ValMont . sprintf "<Set>%0.2f<Edit><Fl>", $TotalPercus;
    }
    warn "INFORMATION DIFF: $CPTDIFF";

    # CONTROLE DES CUMULS CALCULÉS ET DES TOTAUX TRANSMIS
    my $sommeCumul =
      $TotalGlobalCB +
      $TotalGlobalPB +
      $TotalGlobalPC +
      $TotalGlobalIJ +
      $TotalGlobalL6 +
      $TotalRappels +
      $TotalPercus;

 #my $debug =sprintf "<Debug>CTRL (%0.2f/%0.2f)<Null>",$sommeCumul,$TotalGlobal;
 #print OUT $debug if ($sommeCumul != $TotalGlobal);

    # EDITION SI ON A BIEN TRAITÉ AU MOINS UN ADHÉRENT POUR LA COLLECTIVITÉ
    if ( $cptAdhL3 != 0 ) {    # GÉRER ICI LES COLLECTIVITÉS À ZÉRO ?
         #print OUT sprintf "\n %s> %s> <#TotGen=<SET>%0.2f><#TotAgt=%d><fCollec>\n",$LibMont,$ValMont,$TotalGlobal,$cptAdhL3;
        &fin_Collectivite_Xls;
    }

    $cumulTotGLot   += $TotalGlobal;    # CUMUL GÉNÉRAL POUR CONTROL DU LOT
    $cumulTotAgtLot += $cptAdhL3;       # CUMUL GENERAL DES AGENTS POUR LE LOT
    return 1;
}

sub fin_Collectivite_Xls () {
    prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
    prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
    prod_Xls_Insert_Val( $ID_Collectivite, "", "", "", "", "", "", "", "", "",
        "" );
    prod_Xls_Edit_Ligne("T2");

    prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
    prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
    prod_Xls_Insert_Val($ID_Collectivite);
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val("NOMBRE D'AGENTS : $cptAdhL3");
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val($TotalRappels);
    prod_Xls_Insert_Val($TotalPercus);
    prod_Xls_Insert_Val($TotalGlobal);
    prod_Xls_Edit_Ligne;

    # VENTILATION DES SOUS TOTAUX PAR FAMILLE
    if ( $TotalGlobalCB != 0 ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
        prod_Xls_Insert_Val($ID_Collectivite);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("Montant total des garanties");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($TotalGlobalCB);
        prod_Xls_Edit_Ligne;
    }
    if ( $TotalGlobalPB != 0 ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
        prod_Xls_Insert_Val($ID_Collectivite);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("Montant prévoyance de base :");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($TotalGlobalPB);
        prod_Xls_Edit_Ligne;
    }
    if ( $TotalGlobalPC != 0 ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
        prod_Xls_Insert_Val($ID_Collectivite);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("Montant prévoyance complémentaire");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($TotalGlobalPC);
        prod_Xls_Edit_Ligne;
    }
    if ( $TotalGlobalIJ != 0 ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
        prod_Xls_Insert_Val($ID_Collectivite);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("Montant indemnité journalière");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($TotalGlobalIJ);
        prod_Xls_Edit_Ligne;
    }
    if ( $TotalGlobalL6 != 0 ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
        prod_Xls_Insert_Val($ID_Collectivite);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("Montant cotisation exceptionnelle");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($TotalGlobalL6);
        prod_Xls_Edit_Ligne;
    }
    if ( $TotalRappels != 0 ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
        prod_Xls_Insert_Val($ID_Collectivite);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("Montant Total Rappel");
        prod_Xls_Insert_Val("");

        #		prod_Xls_Insert_Val("");
        #		prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($TotalRappels);
        prod_Xls_Edit_Ligne;
    }
    if ( $TotalPercus != 0 ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val("FIN DE COLLECTIVITÉ");
        prod_Xls_Insert_Val($ID_Collectivite);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("Montant Total trop perçu");
        prod_Xls_Insert_Val("");

        #		prod_Xls_Insert_Val("");
        #		prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($TotalPercus);
        prod_Xls_Edit_Ligne;
    }

    prod_Xls_Insert_Val("NEW_PAGE");
    prod_Xls_Edit_Ligne;
    &control_Xls('COLL');
    1;
}

sub format_num {
    my $num = shift;

    $num = sprintf( "%.2f", $num );
    $num !~ s/\./,/;
    while ( $num =~ s/(\d+)(\d{3})/$1 $2/ ) { ; }
    return $num;
}

sub control_Xls {
    my $contexte = shift;
    my $ctrl_Ligne;

# FORMAT DES DEUX TYPES DE LIGNES À PRODUIRE POUR LE CONTROLE DE L'EXPLOITATION
#<50>ac016 :`20050701            <51>D058    <5U>058000-CMS`00               <5V>ADH <5W>NIEVRE : CONSEIL GENERAL                                    <5X>0000315612  <5K>VIOLETTE`FRANCOISE                          <5L>            <5M>111,00      <52>Index Controles <53>  2     4  00
#<50>ac016 :`20050701`TOTAL COL. <51>D058    <5U>058000-CMS`00               <5V>ADH <5W>NIEVRE : CONSEIL GENERAL                                    <5X>TOTAL COL.  <5K>TOTAL COL.                                  <5L>59          <5M>4 673,76    <52>Index Controles <53>  2     4  00

    $PEN004a =~ s/\s+$//;

    my $total_col = $CTRL_DAT;
    my @vals =
      ( $total_col, $ODER022, "$ID_Accc026`$ID_Codgrax", $CODPARx, $PEN004a );

    if ( $contexte eq "COLL" ) {
        $vals[0] .= "`TOTAL COL.";
        push( @vals,
            "TOTAL COL.", "TOTAL COL.", $cptAdhL3, format_num($TotalGlobal) );
    }
    else {
        $matricule =~ s/^\s+//;
        $matricule =~ s/ +/ /g;
        $matricule =~ s/\.//g;
        push( @vals,
            $Reference_ACH1NUM, $NomPrenomIdx, $matricule,
            format_num($TotalL4) );
    }

    $ctrl_Ligne = sprintf(
"<50>ac016 :`%-20s<51>%-8s<5U>%-28s<5V>%-4s<5W>%-60s<5X>%-12s<5K>%-44s<5L>%-15s<5M>%-12s<52>Index Controles <53>  2     1  00   ",
        @vals );

    oe_print_to_output_file("${ctrl_Ligne}\n");
    1;
}

sub trtTotAdh() {

    # traitement de fin d'enregistrement pour un adhérent.
    my ( $cle, $val, $element );

    if ( $cptAdhL3 == 0 ) {

    # ON NE FAIT PAS DE TOTAL ADHERENT SI ON A PAS ENCORE TRAITE D'ADHERENT
    # C'EST A DIRE A CHAQUE FOIS QUE L'ON FAIT UN NOUVEAU GROUPE DE COLLECTIVITE
        return 1;
    }

    for ( $element = 0 ; $element <= $#ordreDeGarantie ; $element++ ) {
        $cle = $ordreDeGarantie[$element];
        $val = $cumulParGarantie{$cle};
        $TotalL4 += $val;

# à la demande de MOA, nous supprimons les traitements à part pour les garanties IJ, PC et PB.
#

        #		if      (index ($cle,'IJ') != -1){
        #			$CumulLibPGMi .=$cle." ";
        #			$CumulIJ +=$val;
        #		} elsif (index ($cle,'PC') != -1){
        #			 $CumulLibPGMc .=$cle." ";
        #			$CumulPC +=$val;
        #		} elsif (index ($cle,'PB') != -1){
        #			$CumulLibPGMb .=$cle." ";
        #			$CumulPB +=$val;
        #		} else  {
        $CumulLib .= $cle . " ";
        $CumulCB += $val;

        #		}
    }

    # CALCUL DU CUMUL PAR ADHERENT HORS TROP PERCU
    $TotalL4 += $eL3_RAPPEL + $CumulL6;

# UN TROP PERCU NE DOIS JAMAIS ÊTRE PLUS GRAND EN VALEUR ABSOLUE QUE LE MONTANT PAR GARANTIE
    if ( ( abs $eL3_TPERCU ) > $TotalL4 ) {
        $eL3_TPERCU = ($TotalL4) * (-1);
    }

    # AFFECTATION DU TROP PAYE AU CUMUL PAR ADHERENT
    $TotalL4 += $eL3_TPERCU;

# UNE ECHEANCE NE PEUT PAS ËTRE NEGATIVE : BALANCE DE L'ÉCHÉANCE, RAZ CALCUL TOTAUX ADH
    if ( $TotalL4 < 0 ) {

        #print OUT "<Debug>CTRL (calcul totaux Adh)<Null>";
        $TotalL4 = 0;
        $CumulPC = 0;
        $CumulPB = 0;
        $CumulCB = 0;
        $CumulIJ = 0;
        $CumulL6 = 0;
    }

# ÉDITION DES SOUS TOTAUX
#print OUT sprintf "<#RAPPEL=<SET>%0.2f>\n",                       $eL3_RAPPEL;
#print OUT sprintf "<#TPERCU=<SET>%0.2f>\n",                       $eL3_TPERCU;
#print OUT sprintf "<#Matricu=$matricule> <#CumulL4=$CumulLib<SET>%0.2f><ssTotMC>\n",$CumulCB;
#print OUT sprintf "<#CumulL4=$CumulLibPGMi<SET>%0.2f><ssTtAut>\n",$CumulIJ if $CumulIJ;
#print OUT sprintf "<#CumulL4=$CumulLibPGMc<SET>%0.2f><ssTtAut>\n",$CumulPC if $CumulPC;
#print OUT sprintf "<#CumulL4=$CumulLibPGMb<SET>%0.2f><ssTtAut>\n",$CumulPB if $CumulPB;
#print OUT sprintf "<#CumulL4=             <SET>%0.2f><ssTtAut>\n",$CumulL6 if $CumulL6;

    # CONTROLE DE L'ECHEANCE PAR RAPPORT A L'ECHEANCE ANTERIEURE DE L'ADHERENT
    if ( $EcheanceAnterieur eq 0 ) {
        $EcheanceAntEvol = "NV";
    }
    elsif ( $TotalL4 ne $EcheanceAntEvol ) {
        $EcheanceAntEvol = "DIFF";
        $CPTDIFF++;
    }
    else {
        $EcheanceAntEvol = "<tAdh>";
    }

#print OUT sprintf "<#TotalL4=<SET>%0.2f><#EchAnt=%s><FinAdh>\n",$TotalL4,$EcheanceAnterieur;
    &ligne_adh_Xls();

    # SOMME DES CUMUL POUR LA COLLECTIVITE
    $TotalGlobal +=
      $TotalL4;    #Somme totale pour une collectivité (Total général)
    $TotalGlobalPC += $CumulPC;
    $TotalGlobalPB += $CumulPB;
    $TotalGlobalCB += $CumulCB;
    $TotalGlobalIJ += $CumulIJ;
    $TotalGlobalL6 += $CumulL6;
    $TotalRappels  += $eL3_RAPPEL;
    $TotalPercus   += $eL3_TPERCU;

#print OUT sprintf "<SK>CumulL4=$CumulLib CumulCB=$CumulCB CumulIJ=$CumulIJ CumulPC=$CumulPC CumulPB=$CumulPB\n";
    &initAdh();

    return 1;
}

sub ligne_adh_Xls() {

    prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
    prod_Xls_Insert_Val($matricule);
    prod_Xls_Insert_Val($NumSS);
    prod_Xls_Insert_Val($NomPrenom);
    prod_Xls_Insert_Val($Reference_ACH1NUM);
    $CumulLib =~ s/\~\`/ /g;
    prod_Xls_Insert_Val($CumulLib);

    #	prod_Xls_Insert_Val($CumulAP);
    #	prod_Xls_Insert_Val($CumulAD);
    prod_Xls_Insert_Val($CumulCB);
    prod_Xls_Insert_Val($eL3_RAPPEL);
    prod_Xls_Insert_Val($eL3_TPERCU);
    prod_Xls_Insert_Val($TotalL4);

    if ( $EcheanceAntEvol eq "<tAdh>" ) {
        $EcheanceAntEvol = "";
    }
    prod_Xls_Insert_Val($EcheanceAntEvol);
    prod_Xls_Insert_Val($CumulTaux);
    prod_Xls_Insert_Val($CumulBase);
    prod_Xls_Edit_Ligne();

    if ($CumulIJ) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val($matricule);
        prod_Xls_Insert_Val($NomPrenom);
        prod_Xls_Insert_Val($Reference_ACH1NUM);
        prod_Xls_Insert_Val($CumulLibPGMi);
        prod_Xls_Insert_Val($CumulIJ);
        prod_Xls_Edit_Ligne();
    }

    if ($CumulPC) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val($matricule);
        prod_Xls_Insert_Val($NomPrenom);
        prod_Xls_Insert_Val($Reference_ACH1NUM);
        prod_Xls_Insert_Val($CumulLibPGMc);
        prod_Xls_Insert_Val($CumulPC);
        prod_Xls_Edit_Ligne();
    }

    if ($CumulPB) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val($matricule);
        prod_Xls_Insert_Val($NomPrenom);
        prod_Xls_Insert_Val($Reference_ACH1NUM);
        prod_Xls_Insert_Val($CumulLibPGMb);
        prod_Xls_Insert_Val($CumulPB);
        prod_Xls_Edit_Ligne();
    }

    if ($CumulL6) {
        prod_Xls_Insert_Val("$ID_Accc026/$ID_Codgrax");
        prod_Xls_Insert_Val($matricule);
        prod_Xls_Insert_Val($NomPrenom);
        prod_Xls_Insert_Val($Reference_ACH1NUM);
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val("");
        prod_Xls_Insert_Val($CumulL6);
        prod_Xls_Edit_Ligne();
    }

    &control_Xls('ADH');

    1;
}

sub prepC1() {
    $ID_Codgrax      = $DATATAB[1];
    $ID_Collectivite = $DATATAB[4];

    # APPEL EXIT EVENTUELS POUR REALISER DES TRAITEMENTS SUR CERTAINS CHAMPS
    &gestionAdrC1();

#if ($DATATAB[22]=~/I004|D005|I007|D009|D011|D012|D013|D015|D016|D017|D019|I02A|I02B|D024|D026|D031|D032|D039|D040|D046|D047|D064|D079|I081|D082|D083|D084|I086|D087/){
#	print OUT "<#TxtFin=<TxtFin3>>";
#} elsif ($DATATAB[22]=~/D002|D003|D010|D018|D022|D023|D025|D027|D028|D034|D041|D043|I045|I049|D051|D052|D053|D054|D055|D056|D058|D059|I060|I061|D062|D063|D070|D072|D073|I074|D076|I077|D080|D085|I090|I092|D093|I094|P001|P002|P004/){
#	print OUT "<#TxtFin=<TxtFin2>>";
#} else {
#	print OUT "<#TxtFin=<TxtFin1>>";
#}

    $ODER022 = $DATATAB[22];
    $PEN004a = $DATATAB[4];
    1;
}

sub gestionAdrC1 () {

    # APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR C1) :
    # SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS C1_NOMCDXLIBACH_DD

    # cas 1 : INDCDX =1  + LIBLOCLIBLOC !C Y	: BOIPOSLIBLDT + b + LIBLOCLIBLOC
    # cas 2 : INDCDX =1  + LIBLOCLIBLOC C  Y	: BOIPOSLIBLDT
    # cas 3 : INDCDX =0 					: BOIPOSLIBLDT

    my $Y = "";

    # TRAITEMENT DE L'ADRESSE DU DESTINANTAIRE
    # MISE EN CAPITALE
    oe_clean_addr_line( $DATATAB[2] );
    oe_uc_sans_accents( $DATATAB[2] );  # PENOCOD	@[2] <= (C1) C1_PENOCODTIT_DD
    oe_clean_addr_line( $DATATAB[4] );
    oe_uc_sans_accents( $DATATAB[4] );  # PENOLIB	@[4] <= (C1) C1_PENOLIBLON_DD
    oe_clean_addr_line( $DATATAB[5] );
    oe_uc_sans_accents( $DATATAB[5] );  # PNTREMx	@[5] <= (C1) C1_PNTREM_DD
    oe_clean_addr_line( $DATATAB[6] );
    oe_uc_sans_accents( $DATATAB[6] );  # CPLADRx	@[6] <= (C1) C1_CPLADR_DD
    oe_clean_addr_line( $DATATAB[8] );
    oe_uc_sans_accents( $DATATAB[8] );  # PEADBTQ	@[8] <= (C1) C1_PEADBTQVOI_DD
    oe_clean_addr_line( $DATATAB[10] );
    oe_uc_sans_accents( $DATATAB[10] ); # PEVONAT	@[10] <= (C1) C1_PEVONATVOI_DD
    oe_clean_addr_line( $DATATAB[12] );
    oe_uc_sans_accents( $DATATAB[12] ); # LIBVOIx	@[12] <= (C1) C1_LIBVOI_DD
    oe_clean_addr_line( $DATATAB[14] );
    oe_uc_sans_accents( $DATATAB[14] )
      ;    # BOIPOSL	@[14] <= (C1) C1_BOIPOSLIBLDT_DD
    oe_clean_addr_line( $DATATAB[15] );
    oe_uc_sans_accents( $DATATAB[15] )
      ;    # LIBLOCL	@[15] <= (C1) C1_LIBLOCLIBLOC_DD
    oe_clean_addr_line( $DATATAB[17] );
    oe_uc_sans_accents( $DATATAB[17] )
      ;    # NOMCDXL	@[17] <= (C1) C1_NOMCDXLIBACH_DD
    oe_clean_addr_line( $DATATAB[19] );
    oe_uc_sans_accents( $DATATAB[19] )
      ; # PEPA019	@[19] len 15	<= (C1) C1_PEPALICPAY_DD (LIBELLE COURT DU PAYS.)
    $DATATAB[19] =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE

    # LA REGLE DE GESTION Y...
    $Y = $DATATAB[17]
      ; # ON FABRIQUE LA FAMEUSE VARIABLE Y, SACHANT QU'UNE MÉTHODE OPTIMISÉE N'EST PAS ADAPTÉE AU GÉNIE DU FLUX...
    $Y =~ s/CEDEX.*$/ /
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s*$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE
    $Y =~ s/\s{2,}/ /g
      ; # ON CONCATENE LES BLANCS : ON SAIT JAMAIS COMMENT LES NOM COMPOSES SONT SAISIS...)
    $DATATAB[15] =~ s/\s{2,}/ /g;

    if ( ( index( $DATATAB[15], $Y ) == 0 ) || $DATATAB[13] == 0 )
    {    # SI LIBLOCLIBLOC CONTIENT Y ou INDCDX = 0
        $DATATAB[15] = " ";    # ON EFFACE LIBLOCLIBLOC
    }

    # TRAITEMENT DE L'ADRESSE DE L'EMETTEUR
    oe_clean_addr_line( $DATATAB[23] );
    oe_uc_sans_accents( $DATATAB[23] )
      ; # PENO023	@[23] len 10	<= (C1) C1_PENOCODTIT_ER (NOM DE L ENTITE DE RATTACHEMENT..)
    oe_clean_addr_line( $DATATAB[25] );
    oe_uc_sans_accents( $DATATAB[25] )
      ;    # PENO025	@[25] len 50	<= (C1) C1_PENOLIBLON_ER
    oe_clean_addr_line( $DATATAB[26] );
    oe_uc_sans_accents( $DATATAB[26] )
      ; # PNTR026	@[26] len 32	<= (C1) C1_PNTREM_ER (POINT DE REMISE DU COURRIER.)
    oe_clean_addr_line( $DATATAB[27] );
    oe_uc_sans_accents( $DATATAB[27] )
      ;   # CPLA027	@[27] len 32	<= (C1) C1_CPLADR_ER (COMPLEMENT GEOGRAPHIQUE.)
    oe_clean_addr_line( $DATATAB[28] );
    oe_uc_sans_accents( $DATATAB[28] )
      ;   # PEAD028	@[28] len 09	<= (C1) C1_PEADNUMVOI_ER (NUMERO DANS LA VOIE.)
    oe_clean_addr_line( $DATATAB[29] );
    oe_uc_sans_accents( $DATATAB[29] )
      ;   # PEAD029	@[29] len 02	<= (C1) C1_PEADBTQVOI_ER (CODE BIS/TER/QUATER.)
    oe_clean_addr_line( $DATATAB[31] );
    oe_uc_sans_accents( $DATATAB[31] )
      ;    # PEVO031	@[31] len 05	<= (C1) C1_PEVONATVOI_ER (NATURE DE VOIE.)
    oe_clean_addr_line( $DATATAB[33] );
    oe_uc_sans_accents( $DATATAB[33] )
      ;    # LIBV033	@[33] len 32	<= (C1) C1_LIBVOI_ER (LIBELLE VOIE.)
    oe_clean_addr_line( $DATATAB[35] );
    oe_uc_sans_accents( $DATATAB[35] )
      ; # BOIP035	@[35] len 32	<= (C1) C1_BOIPOSLIBLDT_ER (SI CODE CEDEX : BOITE POSTAL SINON LIEU-DIT.)
    oe_clean_addr_line( $DATATAB[36] );
    oe_uc_sans_accents( $DATATAB[36] )
      ;    # LIBL036	@[36] len 32	<= (C1) C1_LIBLOCLIBLOC_ER (LOCALITE.)
    oe_clean_addr_line( $DATATAB[37] );
    oe_uc_sans_accents( $DATATAB[37] )
      ;    # CODC037	@[37] len 10	<= (C1) C1_CODCDXCODPOS_ER (CODE POSTAL.)
    oe_clean_addr_line( $DATATAB[38] );
    oe_uc_sans_accents( $DATATAB[38] )
      ; # NOMC038	@[38] len 32	<= (C1) C1_NOMCDXLIBACH_ER (D ACHEMINEMENT (LOCALITE SUR 26C).)
    oe_clean_addr_line( $DATATAB[40] );
    oe_uc_sans_accents( $DATATAB[40] )
      ; # PEPA040	@[40] len 15	<= (C1) C1_PEPALICPAY_ER (LIBELLE COURT DU PAYS.)

    $DATATAB[40] =~ s/FRANCE//i
      ; # PEPA040	@[40] <= (C1) C1_PEPALICPAY_ER (LIBELLE COURT DU PAYS.), ON AFFICHE PAS LE PAYS POUR FRANCE
        # LA REGLE DE GESTION Y...
    $Y = $DATATAB[38]
      ; # ON FABRIQUE LA FAMEUSE VARIABLE Y, SACHANT QU'UNE MÉTHODE OPTIMISÉE N'EST PAS ADAPTÉE AU GÉNIE DU FLUX...
    $Y =~ s/CEDEX.*$/ /
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s*$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE
    $Y =~ s/\s{2,}/ /g
      ; # ON CONCATENE LES BLANCS : ON SAIT JAMAIS COMMENT LES NOM COMPOSES SONT SAISIS...)
    $DATATAB[36] =~ s/\s{2,}/ /g;

    if ( ( index( $DATATAB[36], $Y ) == 0 ) || $DATATAB[34] == 0 )
    {    # SI LIBLOCLIBLOC CONTIENT Y ou INDCDX = 0
        $DATATAB[36] = " ";    # ON EFFACE LIBLOCLIBLOC
    }

    1;
}

sub prepC2() {
    $ID_Accc026 = $DATATAB[26];
    if ( $lastnumco != $ID_Accc026 ) {
        $CPTNUMCO++;
    }
    warn "INFORMATION CTTCOLL: $CPTNUMCO";
    1;
}

# FONCTIONS TRAITEMENTS DONNEES v 20070515-084414

sub prepT6() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT T6
    oe_num_sign_x( $DATATAB[2],  2 );    #	MON002e => CC20_T6_MONTANT_HT
    oe_num_sign_x( $DATATAB[3],  2 );    #	MON003f => CC20_T6_MONTANT_TTC
    oe_num_sign_x( $DATATAB[18], 2 )
      ;    #	CONS018 => CC20_T6_TOT_CONSO_TTC_HORS_PER
    oe_num_sign_x( $DATATAB[20], 2 );  #	HORS020 => CC20_T6_TOT_IND_TTC_HORS_PER
    oe_num_sign_x( $DATATAB[22], 2 );  #	HORS022 => CC20_T6_DIFF_TTC_HORS_PER

    1;
}

sub prepT4() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT T4
    oe_num_sign_x( $DATATAB[2], 2 ); #	MON002c => CC20_T4_MONTANT_HT
    oe_num_sign_x( $DATATAB[3], 2 ); #	MON003d => CC20_T4_MONTANT_TTC
    oe_num_sign_x( $DATATAB[9], 2 ); #	CONS009 => CC20_T4_TOT_CONSO_TTC_HORS_PER
    oe_num_sign_x( $DATATAB[11], 2 );  #	HOR011a => CC20_T4_TOT_IND_TTC_HORS_PER
    oe_num_sign_x( $DATATAB[13], 2 );  #	HORS013 => CC20_T4_DIFF_TTC_HORS_PER

    1;
}

sub prepT3() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT T3
    oe_num_sign_x( $DATATAB[2], 2 ); #	PERC002 => CC20_T3_DEJA_PERCU
    oe_num_sign_x( $DATATAB[3], 2 ); #	ANTE003 => CC20_T3_RESTE_DU_ANTERIEUR
    oe_num_sign_x( $DATATAB[5], 2 ); #	CONSOxx => CC20_T3_TOT_CONSO_TTC_HORS_PER
    oe_num_sign_x( $DATATAB[7], 2 ); #	HORSxxx => CC20_T3_TOT_IND_TTC_HORS_PER
    oe_num_sign_x( $DATATAB[9], 2 ); #	HORS009 => CC20_T3_DIFF_TTC_HORS_PER
    oe_num_sign_x( $DATATAB[13], 2 );  #	DETT013 => CC20_T3_MTTOT_DETTE
    oe_num_sign_x( $DATATAB[14], 2 );  #	MTTO014 => CC20_T3_MTTOT_ENC
    oe_num_sign_x( $DATATAB[16], 2 );  #	DEJAxxx => CC20_T3_HT_DEJA_APP_SUR_PER
    oe_num_sign_x( $DATATAB[17], 2 );  #	DEJA017 => CC20_T3_TTC_DEJA_APP_SUR_PER

    1;
}

sub prepL7() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT L7
    oe_num_sign_x( $DATATAB[2], 2 );    #	MON002b => CC20_L7_MONTANT_HT
    oe_num_sign_x( $DATATAB[3], 2 );    #	MON003c => CC20_L7_MONTANT_TTC

    1;
}

sub prepL6() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT L6
    oe_num_sign_x( $DATATAB[2], 2 );    #	MON002a => CC20_L6_MONTANT_HT
    oe_num_sign_x( $DATATAB[3], 2 );    #	MON003b => CC20_L6_MONTANT_TTC
    $CumulL6 += $DATATAB[3];
    1;
}

sub prepL5() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT L5
    oe_num_sign_x( $DATATAB[2], 2 );    #	MONT002 => CC20_L5_MONTANT_HT
    oe_num_sign_x( $DATATAB[3], 2 );    #	MON003a => CC20_L5_MONTANT_TTC

    1;
}

sub prepL4() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT L4
    oe_num_sign_x( $DATATAB[2], 2 );    #	MONTANT => CC20_L4_MONTANT_HT
    oe_num_sign_x( $DATATAB[3], 2 );    #	MONT003 => CC20_L4_MONTANT_TTC

    # GESTION DES LIBELLÉS DES GARARNTIES
    #INSERTION D'UN BLANC INSÉCABLE POUR L'AFFICHAGE DES LIBELLÉS
    $DATATAB[49] =~ s/\s/\~\`/go;
    $DATATAB[46] =~ s/\s/\~\`/go;
    $DATATAB[46] =~ s/\*/\`\*/go
      ; #la présence d'une étoile dans ce champ provoque une erreur lors du test suivant

    $NumRO = $DATATAB[31] . "" . $DATATAB[32];
    $CleRO = $DATATAB[32];
    $NumSS = $NumRO, if ( $DATATAB[13] eq "ASSPRI" );

    my $refLibL4 = "";
    if ( $DATATAB[46] =~ m/EV|ES|HO/ ) {
        $refLibL4 = $DATATAB[49];
    }
    else {
        $refLibL4 = $DATATAB[46];
    }

    unless ( defined $cumulParGarantie{$refLibL4} ) {

        #le comptage compte tous les L4 (CI) d'un Adhérent
        $ordreDeGarantie[ $compteurL4Adh++ ] = $refLibL4;
    }

    # CUMUL DES MONTANTS PAR GARANTIE
    $cumulParGarantie{$refLibL4} += $DATATAB[3];

# ECLATEMENT DES CUMULS PAR ASSURE PRINCIPAL ET AYANT DROIT (mis en place le 15/04)
    $CumulAP += $DATATAB[3], if ( $DATATAB[13] eq "ASSPRI" );
    $CumulAD += $DATATAB[3], if ( $DATATAB[13] ne "ASSPRI" );

    if ( $DATATAB[13] eq "ASSPRI" ) {
        $matricule = $DATATAB[62];
    }
    1;
}

sub prepLJ() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT LJ
    oe_num_sign_x( $DATATAB[11], 2 );    # CC20-LJ-ACJIMAHCOT
    if ( $DATATAB[7] eq 'BASECO' ) {
        $Base      = $DATATAB[11];
        $CumulBase = $Base;
    }
    elsif ( $DATATAB[7] eq 'TAUXCO' ) {    # on doit cumuler les taux.
        $Taux = $DATATAB[11];
        $CumulTaux += $Taux;
    }
    1;
}

sub prepL3() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT L3
    oe_num_sign_x( $DATATAB[5],  2 );      #	PERCUxx => CC20_L3_DEJA_PERCU
    oe_num_sign_x( $DATATAB[6],  2 );      #	ANTERIE => CC20_L3_RESTE_DU_
    oe_num_sign_x( $DATATAB[31], 2 );      #	DETTExx => CC20_L3_MTTOT_DETTE
    oe_num_sign_x( $DATATAB[32], 2 );      #	MTTOTxx => CC20_L3_MTTOT_ENC
    oe_num_sign_x( $DATATAB[34], 2 );      #	PERIODE => CC20_L3_TTC_HORS_PERIODE
    oe_num_sign_x( $DATATAB[36], 2 );      #	MTTO036 => CC20_L3_MTTOT_ECH_PREC
    oe_num_sign_x( $DATATAB[37], 2 )
      ;    #	MTTO036 => CC20_L3_RESTE DU ANTERIEUR_ECH_PREC
    oe_num_sign_x( $DATATAB[38], 2 );   #	MTTO036 => CC20_L3_TROP PERCU_ECH_PREC
         #print OUT "\n<aCollec>\n" if ($cptAdhL3 == 0);
    trtTotAdh();

    #CALCUL DU RAPPEL OU TROP PAYE RESTE
    $eL3_TPERCU = $DATATAB[5];
    $eL3_RAPPEL = $DATATAB[6];

    if ( $DATATAB[34] ge 0 ) {
        $eL3_RAPPEL += $DATATAB[34];
    }
    else {
        $eL3_TPERCU += $DATATAB[34];
    }
    $cptAdhL3++;

    $DATATAB[12]       = ucfirst( lc( $DATATAB[12] ) );
    $DATATAB[13]       = ucfirst( lc( $DATATAB[13] ) );
    $EcheanceAnterieur = $DATATAB[36];
    $RestAnt           = $DATATAB[37];
    $TrpcAnt           = $DATATAB[38];

#Mise en place du MNT1118 pour que chaque mouvement de cotisation soit spécifié par un "DIFF"
    $EcheanceAntEvol = $EcheanceAnterieur + $TrpcAnt + $RestAnt;

    $NomPrenom    = "$DATATAB[12] $DATATAB[13]";
    $NomPrenomIdx = "$DATATAB[12]`$DATATAB[13]";
    $NomPrenomIdx =~ s/^\s+//;
    $Reference_ACH1NUM = $DATATAB[1];

    1;
}

sub prepENT() {

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT ENT
    oe_num_sign_x( $DATATAB[1],  0 );    #	ZZ00001 => ZZ0083C01_E_ZZJOR1
    oe_num_sign_x( $DATATAB[2],  0 );    #	ZZ00002 => ZZ0083C01_E_IDFSYS_ENTETE
    oe_num_sign_x( $DATATAB[21], 0 );    #	ZZ00021 => ZZ0083C01_E_IDFDST
    oe_num_sign_x( $DATATAB[22], 0 );    #	ZZ00022 => ZZ0083C01_E_RELDST
    oe_num_sign_x( $DATATAB[23], 0 );    #	ZZ00023 => ZZ0083C01_E_IDFBL2_2
    oe_num_sign_x( $DATATAB[24], 0 );    #	ZZ00024 => ZZ0083C01_E_RELBL2
    oe_num_sign_x( $DATATAB[25], 0 );    #	ZZ00025 => ZZ0083C01_E_IDFBL3_2
    oe_num_sign_x( $DATATAB[26], 0 );    #	ZZ00026 => ZZ0083C01_E_RELBL3
    oe_num_sign_x( $DATATAB[27], 0 );    #	ZZ00027 => ZZ0083C01_E_IDFGRP

    $DATATAB[11] =~ /(\d{4})(\d{2})/;
    my $annee = $1;
    my $mois  = $2;

    if ( $mois ge 12 ) {
        $mois  = "01";
        $annee = $annee + 1;
    }
    else {
        $mois = $mois + 1;
    }

    #printf OUT "<#ent=<C7m>%0.2d<C7a>%d >\n",$mois,$annee;
    $ent_pour_XLS = "$mois/$annee";
    1;
}

sub prepF5() {
    if ( $flagF5 != 0 ) {    # PAS DE TOTAUX POUR LE PREMIER GRP DE COLLECTIVITE
        &initDoc();
    }
    $flagF5++;

    # APPEL EXIT EVENTUELS POUR REALISER DES TRAITEMENTS SUR CERTAINS CHAMPS
    if ( $DATATAB[16] eq "Adhérent" ) { $DATATAB[16] = "Memb. part."; }

    $F5_LIBLON  = $DATATAB[16];
    $F5_DEBPERx = $DATATAB[64];
    oe_to_date($F5_DEBPERx);
    &deb_Collectivite_Xls();

    $CODPARx  = $DATATAB[14];
    $CTRL_DAT = $DATATAB[64];

    #print OUT "<#ctrlDat=$DATATAB[64]>";
    1;
}

exit main(@ARGV);
