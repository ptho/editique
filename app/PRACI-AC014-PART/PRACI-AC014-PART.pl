#!/usr/bin/perl #-d:ptkdb

use utf8;
use strict;
use warnings;
use Readonly;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEdtk::libXls;
use oEdtk::Outmngr qw(omgr_stats);
use oEdtk::Config qw(config_read);
use oEdtk::DBAdmin qw(db_connect);
use File::Spec;
use File::Spec::Functions;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::OB;
use oEUser::Descriptor::E3;
use oEUser::Descriptor::F3;
use oEUser::Descriptor::H0;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::HE;
use oEUser::Descriptor::J2;
use oEUser::Descriptor::L2;
use oEUser::Descriptor::N1;
use oEUser::Descriptor::OL;
use oEUser::Descriptor::T1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use Date::Calc qw(Mktime Gmtime);
use Data::Dumper;

# DECLARATION DES VARIALBES GLOBALES DE L'APPLICATION :#

# Pour le tracking.
my $TRK;
my $NUMCTR;    #le numéro de contrat individuel
my $NOMPRENOM;
my $AGENCE;
my @tADRDEST;
my @GARANTIE;
my ( $typeAdh, $eT1_montantTotal, $eT1_RAPPEL, $eT1_ECHEANCES,
    $listeGarantieAutre );
my $EDITION_CHEQUE  = 0;
my $EDITION_ECARTEE = 0;
my $EMET;
my $TYPEAPP;
my $TYPEMODR;
my $ZONE_EFE;
my $MOISPREL, my $DATEPREL;
my $MONTANTPREL;
my $PBASECOT = " ";
my $DATEDDE;
my $DATERES;
my $CPTDATERES = 0;
my $POSFLUSFIN;
my $POSFLUSDEB;
my $CPTDOC = 0;
my $CPTE1  = 0;
my @EADDR;
my @DADDR;
my $NUMABA;    # Num ABA = ID-MNT
my $xVILDEST;
my $CODEGTIE;
my $CODEPRT;

# initialisation nouveaux courriers.
my $DOC         = oEdtk::TexDoc->new();
my $TABLEAU     = oEdtk::TexDoc->new();
my $ECHEANCIER  = oEdtk::TexDoc->new();
my $TALON       = oEdtk::TexDoc->new();
my %H_INFO_GTIE = ();
my %H_VAL_GTIE  = ();
Readonly my $LIBCODPRTGS1   => "PL-MNT-GS1";
Readonly my $LIBCODPRTGS2   => "PL-MNT-GS2";
Readonly my $LIBCODPRTPLUPS => "PLU-MNT-PS";
Readonly my $LIBCODPRTPLUDC => "PIDS3-MNT3";
Readonly my $LIBCODE_PRT    => "CODEPRTGTIE";
Readonly my $LIBCOU_GTIE    => "LIBCOUGTIE";
Readonly my $LIBLON_PRT     => "LIBLONGTIE";
Readonly my $VAL_TX_MT      => "VALTAUXMON";

#my $XLS		= oEdtk::TexDoc->new();
# fin d'initialisation

###############################################DEBUT DU MAIN####################################################
sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my ( $user, $type_edition, $flux ) = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        entity => oe_corporation_set(),
        user   => $user,
        keys   => [ 'xIDDEST', 'xNOMDEST', 'xCPDEST', 'MODREGL' ]
    );

    #my $suffixe = oe_corporation_set;
    my $opt_ged = '--edms';    #option GED à oui
    oe_new_job('--index');
    $DOC->append( 'xTYPDOC', 'ACOT' );    ### Type de document

#############################INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION####################################

    my $ENTETE = oEUser::Descriptor::ENTETE::get();

    #$ENTETE->debug();
    $ENTETE->bind_all_c7();
    my $E1 = oEUser::Descriptor::E1::get();

    #$E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2::get();

    #$E2->debug();
    $E2->bind_all_c7();
    my $E3 = oEUser::Descriptor::E3::get();
    $E3->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7::get();
    $E7->bind_all_c7();
    my $F3 = oEUser::Descriptor::F3::get();

    #$F3->debug();
    $F3->bind_all_c7();
    my $T1 = oEUser::Descriptor::T1::get();
    $T1->bind_all_c7();
    my $N1 = oEUser::Descriptor::N1::get();
    $N1->bind_all_c7();
    my $J2 = oEUser::Descriptor::J2::get();
    $J2->bind_all_c7();
    my $L2 = oEUser::Descriptor::L2::get();
    $L2->bind_all_c7();
    my $H0 = oEUser::Descriptor::H0::get();

    #$H0->debug();
    $H0->bind_all_c7();
    my $H1 = oEUser::Descriptor::H1::get();
    $H1->bind_all_c7();
    my $HE = oEUser::Descriptor::HE::get();

    #$HE->debug();
    $HE->bind_all_c7();
    my $OB = oEUser::Descriptor::OB::get();
    $OB->bind( 'OB-TXT', 'MSGOBS' );
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();
    my $OL = oEUser::Descriptor::OL::get();

    #$OL->debug();
    $OL->bind_all();
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $ENTETE,
        'E1'     => $E1,
        'E2'     => $E2,
        'E7'     => $E7,
        'OB'     => $OB,
        'E3'     => $E3,
        'T1'     => $T1,
        'F3'     => $F3,
        'N1'     => $N1,
        'J2'     => $J2,
        'L2'     => $L2,
        'OL'     => $OL,
        'H0'     => $H0,
        'H1'     => $H1,
        'HE'     => $HE,
        'Z1'     => $Z1
    );

###############################################DEBUT DU WHILE###################################################

    while ( my ( $id, $vals ) = $p->next() ) {
############################################TRAITEMENT DU RECORD E1#############################################
        if ( $id eq 'E1' ) {
            $TRK->track( 'Doc', 1, $AGENCE, $NUMCTR, $NOMPRENOM, $NUMABA,
                $xVILDEST )
              if ( $CPTE1 > 0 );
            $CPTE1++;
            emitDoc() if ( $. > 3 );
            $POSFLUSDEB = $.;
            InitAdh();

            # traitement du CTC de rattachement
            CTC_rata($vals);
            $AGENCE    = $vals->{'ODERCOD'};
            $NOMPRENOM = $vals->{'PENOLIB'} . ' ' . $vals->{'PENOPRN'};
            $NOMPRENOM =~ s/ +$//;
            $DOC->append( 'PENOLIB', $vals->{'PENOLIB'} );
            $DOC->append( 'PENOPRN', $vals->{'PENOPRN'} );
            gestionAdrE1($vals);
        }
########################################FIN DU TRAITEMENT DU RECORD E1##########################################

############################################TRAITEMENT DU RECORD E2#############################################
        if ( $id eq 'E2' ) {

            #warn "INFO : dump E2 ". Dumper($vals) ."\n";
            $EMET   = oe_uc_sans_accents( $vals->{'PENO084'} );
            $EMET   = $vals->{'PENO083'} . " " . $vals->{'PENO084'};
            $NUMCTR = $vals->{'ACH1NUM'};

            if ( $EMET ne " " ) {
                $DOC->append( 'xIDEMET', $EMET );
            }
            else {
                $DOC->append( 'xIDEMET', $AGENCE );
            }
            if ( $vals->{'LICMODR'} =~ m/(PX|X|x|px)/ ) {
                my $textepx = 1;
                $DOC->append( 'TEXTEPX', $textepx );
            }
            else {
                my $textepx = 0;
                $DOC->append( 'TEXTEPX', $textepx );
            }
            $DOC->append( 'ACPAMOD',  $vals->{'ACPAMOD'} );
            $DOC->append( 'LICMODR',  $vals->{'LICMODR'} );
            $DOC->append( 'xCODRUPT', $vals->{'LICMODR'} );
            $DOC->append( 'ACCCNUM',  $vals->{'ACCCNUM'} );
            $DOC->append( 'ACGACOD',  $vals->{'ACGACOD'} );
            $DOC->append( 'ACOPCOD',  $vals->{'ACOPCOD'} );
            $DOC->append( 'LIBMODR',  $vals->{'LIBMODR'} );
            $DOC->append( 'ECHPC',    $vals->{'ACFQ065'} );

            # print "\n ECHPC = $vals->{'ACFQ065'} \n ";
            $DOC->append( 'NUMGRP',
                $vals->{'ACCCNUM'} . "/" . $vals->{'ACGACOD'} );

            # traitement type échéance
            traitement_type_eche($vals);

            # traitement de l'expéditeur via le mode de règlement
            traitement_expediteur($vals);

            #Traitement de l'édition
            E2_type_edition($vals);
        }
########################################FIN DU TRAITEMENT DU RECORD E2##########################################
        if ( $id eq 'E7' ) {
            prepE7($vals);
        }
######################################TRAITEMENT DU RECORD OB (ZONE EFE)########################################
        if ( $id eq 'OB' ) {
            $ZONE_EFE = 1;    # $ZONE_EFE = 1 si présent
            prepOB($vals);
        }
        else {
            $ZONE_EFE = 0;    # $ZONE_EFE = 0 si absent
        }
########################################FIN DU TRAITEMENT DU RECORD OB##########################################

############################################TRAITEMENT DU RECORD F3#############################################
        if ( $id eq 'F3' ) {
            $DOC->append( 'PERICOD', $vals->{'PERICOD'} );
            $DOC->append( 'PERIGUI', $vals->{'PERI004'} );
            $DOC->append( 'PERINUM', $vals->{'PERINUM'} );
            $DOC->append( 'PERICLE', $vals->{'PERICLE'} );
            $DOC->append( 'IBAN',    $vals->{'PERIIDT'} );
            $DOC->append( 'BIC',     $vals->{'PERI017'} );
            $DOC->append( 'RUM',     $vals->{'RUMxxxx'} );
            $DOC->append( 'ICS',     $vals->{'ICSxxxx'} );
            prepF3_date_echeance($vals);
        }
########################################FIN DU TRAITEMENT DU RECORD F3##########################################

############################################TRAITEMENT DU RECORD HO#############################################
        if ( $id eq 'H0' ) {

            #            warn "INFO : dump H0 ". Dumper($vals) ."\n";
            $DATERES = ( $vals->{'ADMI011'} );

            # Vérification présence date de résiliation
            traitement_date_res_HO($vals);
        }
########################################FIN DU TRAITEMENT DU RECORD HO##########################################

############################################TRAITEMENT DU RECORD OL#############################################
        if ( $id eq 'OL' ) {

            #            warn "INFO : dump OL ". Dumper($vals) ."\n";
            #            if ( $vals->{'ACH3CODPRT'} eq $LIBCODPRTGS1 ) {
            if ( $vals->{'ACH4CODGAR'} =~ m/IJ/ ) {
                $H_VAL_GTIE{ $vals->{'ACH4CODGAR'} } =
                  $vals->{'ACIJVALMNT'};

            }
            else {
                $H_VAL_GTIE{ $vals->{'ACH4CODGAR'} } =
                  $vals->{'ACKGVALMNT'};
            }

            #            }
        }
########################################FIN DU TRAITEMENT DU RECORD OL##########################################

###########################################TRAITEMENT DU RECORD HE #############################################
        if ( $id eq 'HE' ) {
        }
###########################################FIN DU TRAITEMENT DU RECORD HE ######################################

##########################################TRAITEMENT DU RECORD T1###############################################
        if ( $id eq 'T1' ) {
            $POSFLUSFIN = $.;
            $DOC->append( 'MONTANTTTC', $vals->{'MONTANT'} );
            prepT1($vals);

            # récupération et traitement des échéances pour prélèvement.
            eche_prel($vals);
        }
##########################################FIN DU TRAITEMENT DU RECORD T1########################################

##########################################TRAITEMENT DU RECORD Z1###############################################
        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($vals) ."\n"; die();
            $NUMABA = $vals->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $DOC->append( 'xCLEGEDi', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }
##########################################FIN DU TRAITEMENT DU RECORD T1########################################

        $DOC->append( 'USER', $user );
        date_edition();
    }
###########################################FIN DU While##########################################
    emitDoc();
    $TRK->track( 'Doc', 1, $AGENCE, $NUMCTR, $NOMPRENOM, $NUMABA, $xVILDEST );
    oe_compo_link() if $CPTDOC;
    return 0;
}
################################################FIN DU MAIN#####################################################

#########################################
# FONCTIONS SPECIFIQUES A L'APPLICATION #
#########################################

sub emitDoc {

    #      warn "INFO : dump H_VAL_GTIE \n". Dumper(\%H_VAL_GTIE) ."\n";
    #      warn "INFO : dump H_INFO_GTIE \n". Dumper(\%H_INFO_GTIE) ."\n";
    foreach my $clef ( keys %H_VAL_GTIE ) {

      #        print STDERR "INFO : clef = $clef valeur = $H_VAL_GTIE{$clef}\n";
        if ( exists $H_INFO_GTIE{$clef} ) {
            $H_INFO_GTIE{$clef}{$VAL_TX_MT} = $H_VAL_GTIE{$clef};
        }
    }
    my %TABGAR = ();

    #      warn "INFO : dump H_INFO_GTIE 2 \n". Dumper(\%H_INFO_GTIE) ."\n";
    foreach my $clef ( keys %H_INFO_GTIE ) {

        #         print STDERR "INFO : clef = ".$clef." \n";
        if ( $clef =~ m/PLU-MNT-/ ) {
            $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'LIBGAR'} =
              $H_INFO_GTIE{$clef}{$LIBLON_PRT};
            if (
                defined $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'OPTION'} )
            {
                $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'OPTION'} =
                  $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'OPTION'} . " / "
                  . $H_INFO_GTIE{$clef}{$LIBCOU_GTIE} . " "
                  . $H_INFO_GTIE{$clef}{$VAL_TX_MT};
            }
            else {
                $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'OPTION'} =
                    $H_INFO_GTIE{$clef}{$LIBCOU_GTIE} . " "
                  . $H_INFO_GTIE{$clef}{$VAL_TX_MT};
            }
            if (
                defined $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'MONTTTC'}
              )
            {
                $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'MONTTTC'} =
                  ( $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'MONTTTC'} +
                      $H_INFO_GTIE{$clef}{'MONTTTC'} );
            }
            else {
                $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'MONTTTC'} =
                  $H_INFO_GTIE{$clef}{'MONTTTC'};
            }
        }
        else {
            $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'LIBGAR'} =
              $H_INFO_GTIE{$clef}{$LIBLON_PRT};
            $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'OPTION'} =
                $H_INFO_GTIE{$clef}{$LIBCOU_GTIE} . " "
              . $H_INFO_GTIE{$clef}{$VAL_TX_MT};
            $TABGAR{ $H_INFO_GTIE{$clef}{$LIBCODE_PRT} }{'MONTTTC'} =
              $H_INFO_GTIE{$clef}{'MONTTTC'};
        }
    }

    #     warn "INFO : dump H_INFO_GTIE 3 \n". Dumper(\%H_INFO_GTIE) ."\n";
    #     warn "INFO : dump TABGAR \n". Dumper(\%TABGAR) ."\n";
    foreach my $k ( keys %TABGAR ) {
        $TABLEAU->append( 'LIBGAR',  $TABGAR{$k}{'LIBGAR'} );
        $TABLEAU->append( 'OPTION',  $TABGAR{$k}{'OPTION'} );
        $TABLEAU->append( 'MONTTTC', $TABGAR{$k}{'MONTTTC'} );
        $TABLEAU->append('editpb');
    }
    $CPTDOC++;
    $DOC->append( 'xTYPDOC', 'ACOT' );

    if ( $TYPEMODR eq 'P' ) {

        $DOC->append( 'xCLEGEDiv',  'P' );
        $DOC->append( 'echeancier', $ECHEANCIER );
    }
    if ( $TYPEMODR eq 'C' ) {

        $DOC->append( 'xCLEGEDiv', 'C' );
        $TABLEAU->append('editcheque');
        $TALON->append('talcheque');
        $DOC->append( 'talon', $TALON );
    }
    if ( $TYPEMODR eq 'X' ) {

        $DOC->append( 'xCLEGEDiv', 'X' );
        if (    ( $CODEPRT ne $LIBCODPRTGS1 )
            and ( $CODEPRT ne $LIBCODPRTGS2 ) )
        {
            $TABLEAU->append('editpx');
        }
        $TALON->append('talcheque');
        $DOC->append( 'talon', $TALON );
    }
    $DOC->append( 'tableau', $TABLEAU );
    $DOC->append('ENDDOC');

    oe_print_to_output_file($DOC);
    $DOC        = oEdtk::TexDoc->new();
    $TABLEAU    = oEdtk::TexDoc->new();
    $ECHEANCIER = oEdtk::TexDoc->new();
    $TALON      = oEdtk::TexDoc->new();
    $ZONE_EFE   = "";
    $TYPEMODR   = "";
    $TYPEAPP    = "";
    $DATERES;
    $CPTDATERES = 0;
    $CODEPRT    = "";
    $CODEGTIE   = "";
    return 0;
}

# Fonction pour le Rattachement au CTC

sub CTC_rata {
    my $vals = shift;
    if ( $vals->{'ODERCOD'} =~
/C051|C063|D001|D004|I004|D005|D006|D007|I007|D009|D011|D012|D013|D015|D016|D017|D019|I02A|I02B|D023|D024|D026|D030|D031|D032|D033|D034|D038|D039|D040|D046|D047|I048|D063|D064|D065|D066|D069|D071|D079|I081|D082|D083|D084|I086|D087|P003|P005|P006/
      )
    {
        $DOC->append( 'EADDRCT', 'ADDRC' );
        $DOC->append( 'EADDRFIRST', {'EADDRFIRSTA'} );
    }
    elsif ( $vals->{'ODERCOD'} =~
/D002|D003|D008|D010|C014|D014|D018|D021|D022|D025|D027|D028|D029|D035|D036|D037|D041|I042|D043|D044|I045|D045|D049|I049|D051|D052|D053|D054|D055|D056|D057|I057|D058|D059|I060|D060|I061|D061|D062|I067|I068|D070|I070|D072|D073|I074|D074|D075|D076|I077|D078|D080|D085|D088|D089|D090|I090|D091|I092|D093|I094|D095|P001|P002|P004/
      )
    {
        $DOC->append( 'EADDRCT', 'ADDRB' );
        $DOC->append( 'EADDRFIRST', {'EADDRFIRSTA'} );
    }
    else {
        $DOC->append( 'EADDRCT', 'ADDRA' );
        $DOC->append( 'EADDRFIRST', {'edTitCorp'} );
    }
    return 0;
}

# Traitement type échéance

sub traitement_type_eche {
    my $vals = shift;
    if ( $vals->{'ACOPCOD'} =~ /ACSO|ACAV|ACRC|ACTG/ ) {
        $DOC->append( 'TYPEAPP', {'TYPEAPPNVLADH'} );    # Premier appel
        $TYPEAPP = 1;    # Pour écarter les éditions de masse

    }
    elsif ( $vals->{'ACOPCOD'} =~ /ACMC|ACCC|ACMA/ ) {
        $DOC->append( 'TYPEAPP', {'TYPEAPPREG'} );    # REGULARISATION
        $TYPEAPP = 2;    # Pour écarter les éditions de masse

    }
    else {
        $TYPEAPP = 0;    # ANNUEL
    }
    return 0;
}

# Traitement de l'expéditeur

sub traitement_expediteur {
    my ($vals) = shift;
    if ( $vals->{'LICMODR'} eq "PB" ) {
        $DOC->append( 'EADDRFIRST', {'edTitCorp'} );
        $DOC->append( 'EADDRCT', 'ADDRA' );
        $TYPEMODR = 'P';    # $TYPEMODR = P pour règlement PB
        if ( $TYPEAPP eq 2 ) {
            $DATEDDE = ( $vals->{'ACOP134'} );
            $DOC->append( 'DATEDDE', $DATEDDE );

            #warn 'INFO $DATEDDE :'. $DATEDDE ."\n";
        }
    }
    if ( $vals->{'LICMODR'} eq "Chèque" ) {
        $TYPEMODR = 'C';    # $TYPEMODR = C pour règlement Chèque
    }
    if ( $vals->{'LICMODR'} eq "PX" ) {
        $TYPEMODR = 'X';    # $TYPEMODR = X pour règlement précontentieux
    }
    return 0;
}

# Conversion de date.

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

# fonction permettant la conversion du mois XX en toutes lettres.

sub month_to_name {
    my @mons =
      qw/Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre Décembre/;
    my $MOISPREL = shift;

    return $mons[ $MOISPREL - 1 ];
}

# INITIALISATION DES CONSTANTES DE CHAQUE ADHERENT
sub InitAdh {
    undef @tADRDEST;
    undef @GARANTIE;
    $typeAdh            = "";
    $eT1_montantTotal   = 0;
    $eT1_RAPPEL         = 0;
    $eT1_ECHEANCES      = 0;
    $listeGarantieAutre = "";
    return 0;
}

sub E2_type_edition {
    my ($vals) = shift;
    $DOC->append( 'NUMCTR',  $NUMCTR );
    $DOC->append( 'xIDDEST', $NUMCTR );

    #		if ($vals->{'ACCCNUM'} =~ m/PCI/){
    #			$DOC->append('xCODRUPT', 'PCI');
    #		}
    $DOC->append( 'xNOMDEST', $NOMPRENOM );
    $DOC->append( 'xCPDEST',  $vals->{'CODCDXC'} );
    $DOC->append( 'xVILDEST', $vals->{'NOMCDXL'} );

    $xVILDEST = $vals->{'NOMCDXL'};
    $TRK->track( 'Doc', 1, $vals->{'ACH1NUM'}, $NOMPRENOM,
        $vals->{'CODCDXC'}, $vals->{'LICMODR'} );
    return 0;
}

sub prepOB {
    my ( $vals, $DOC ) = @_;
    $DOC->append($vals);
    return 0;
}

#préparation date échéance.

sub prepF3_date_echeance {
    my ($vals) = shift;
    $DOC->append( 'xCLEGEDiii', $vals->{'DFEPRDx'} );
    $DOC->append( 'DDEPRDx',    $vals->{'DDEPRDx'} );

    if ( $vals->{'DFEPRDx'} !~ /(\d{2})\/(\d{2})\/(\d{4})/ ) {
        die
"ERROR: in prepF3_date_echeance Unexpected date format \'$vals->{'DFEPRDx'}\' at line $. in input file\n";
    }

    if ( $vals->{'DDEPRDx'} !~ /(\d{2})\/(\d{2})\/(\d{4})/ ) {
        die
"ERROR: in prepF3_date_echeance Unexpected date format \'$vals->{'DDEPRDx'}\' at line $. in input file\n";
    }

    ( $vals->{'DDEPRDx'} =~ /(\d{2})\/(\d{2})\/(\d{4})/ );
    my $anneed = $3;

    ( $vals->{'DFEPRDx'} =~ /(\d{2})\/(\d{2})\/(\d{4})/ );
    my $joursf = $1;
    my $moisf  = $2;
    my $anneef = $3;

    if ( $TYPEAPP eq 0 ) {    # Annuel
        $DOC->append( 'TYPEAPP', $anneed );
        $DOC->append( 'DFEPRDx', $vals->{'DFEPRDx'} );
    }
    elsif ( $TYPEAPP eq 1 ) {    # Premier appel
        $DOC->append( 'DFEPRDx', $vals->{'DFEPRDx'} );
    }
    elsif ( $TYPEAPP eq 2 ) {    # REGULATION
        $DOC->append( 'DFEPRDx', $vals->{'DFEPRDx'} );
    }
    $DOC->append( 'TESTEFE', $ZONE_EFE );
    return 0;
}

sub gestionAdrE1 {
    my $vals = shift;
    my $Y;

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $vals->{'LIBLOCL'} );
    oe_clean_addr_line( $vals->{'NOMCDXL'} );
    $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDCDXx'} == 0 ) {

        # CAS 3
        $vals->{'LIBLOCL'} = '';
    }
    elsif ( $vals->{'INDCDXx'} == 1 && index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBLOCL'} = '';
    }

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
    @DADDR = ();
    push( @DADDR,
"$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}"
    );

    oe_clean_addr_line( $vals->{'LIBL038'} );
    oe_clean_addr_line( $vals->{'NOMC040'} );

    @EADDR = ();
    push( @EADDR, $vals->{'PENO027'} );
    return 0;
}

############################################################################################################
# Suite à la norme 38: le nom et prenom de destinataire et de d'émetteur sont restés dans le record E1
# Mais les adresses se trouvent dans E7 sont de norme 38
############################################################################################################

sub prepE7 {
    my ($vals) = @_;
    my $Y;

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $vals->{'LIBLOCL'} );
    oe_clean_addr_line( $vals->{'NOMCDXL'} );
    $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDCDXx'} == 0 ) {

        # CAS 3
        $vals->{'LIBLOCL'} = '';
    }
    elsif ( $vals->{'INDCDXx'} == 1 && index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBLOCL'} = '';
    }

    push( @DADDR, $vals->{'PNTREMx'} );    #PNTREMx
    push( @DADDR, $vals->{'CPLADRx'} );
    push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );                                     #PEADNUM, PEADBTQ, LIBVOIx
    push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );  #BOIPOSL, LIBLOCL,
    push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );  #CODCDXC, NOMCDXL
      # initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $vals->{'PELICCO'} );    #    LICCODP
    $vals->{'LICCODP'} =~ s/FRANCE//i
      if ( defined $vals->{'LICCODP'} );    # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @DADDR, $vals->{'LICCODP'} );     # LICCODP
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

###################################################################
    # Adresse Émetteur
###################################################################

    oe_clean_addr_line( $vals->{'LIBL022'} );    #  LIBL038
    oe_clean_addr_line( $vals->{'NOMC024'} );    #  NOMC040
    $Y = $vals->{'NOMC024'};                     #   NOMC040
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDC020'} == 0 ) {    #INDC036
                                        # CAS 3
        $vals->{'LIBL022'} = '';        #   LIBL038
    }
    elsif ( $vals->{'INDC020'} == 1 && index( $vals->{'LIBL022'}, $Y ) == -1 )
    {                                   # INDC036,   LIBL038
                                        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBL022'} = '';        #  LIBL038
    }

    push( @EADDR, "$vals->{'CPLA015'} $vals->{'PNTR014'}" );  #PNTR028 , CPLA029
    push(
        @EADDR, "$vals->{'PEAD016'} $vals->{'PEAD017'} "
          .    #PEAD030, PEAD031, LICN034 (inexistant dans norme 38))
          "$vals->{'LIBV019'} $vals->{'BOIP021'}"
    );         #LIBV035 ,   BOIP037
    push( @EADDR, "$vals->{'CODC023'} $vals->{'LIBL022'} $vals->{'NOMC024'}" )
      ;        # CODC039 , LIBL038, NOMC040
    @EADDR = user_cleanup_addr(@EADDR);
    $DOC->append_table( 'ADDRA', @EADDR );
    return 0;
}

#Vérification présence date de résiliation

sub traitement_date_res_HO {
    my ($vals) = shift;
    if ( $DATERES eq '' ) {
        if ( $TYPEAPP eq 2 && $TYPEMODR eq 'P' ) {
        }
        else {
            $DATEDDE = ( $vals->{'ACCODDE'} );
        }
        prepHO_date_echeance($vals);
        $H_INFO_GTIE{ $vals->{'TECHxxx'} }{$LIBCODE_PRT} =
          $vals->{'CODExxx'};
        $H_INFO_GTIE{ $vals->{'TECHxxx'} }{$LIBLON_PRT} =
          user_prev_get_option( $vals->{'LIBL009'} );
        $H_INFO_GTIE{ $vals->{'TECHxxx'} }{$LIBCOU_GTIE} =
          $vals->{'LIBCOUx'};
        $H_INFO_GTIE{ $vals->{'TECHxxx'} }{'MONTTTC'} =
          ( $vals->{'MONTANT'} + $vals->{'MONT043'} );
    }
    else {
        $CPTDATERES++;
    }
    return 0;
}

# Préparation du talon pour règlement par chèque.

sub prepHO_date_echeance {
    my ($vals) = shift;

    #$DOC->append(Dumper($vals));
    if ( $DATEDDE !~ /(\d{4})(\d{2})(\d{2})/ ) {
        die
"ERROR: in prepHO_date_echeance Unexpected date format \'$DATEDDE\' at line $. in input file\n";
    }

    ( $DATEDDE =~ /(\d{4})(\d{2})(\d{2})/ );
    my $joursd = $3;
    my $moisd  = $2;
    my $anneed = $1;
    $DOC->append( 'xCLEGEDii', "$joursd/$moisd/$anneed" );

    if ( $TYPEAPP eq 0 ) {    # Annuel
        $DOC->append( 'TYPEAPP', $anneed );
        $joursd = '01';
        $DOC->append( 'DATEDDE', "$joursd/$moisd/$anneed" );
    }
    elsif ( $TYPEAPP eq 1 ) {    # Premier appel
        $joursd = '01';
        $DOC->append( 'DATEDDE', "$joursd/$moisd/$anneed" );
    }
    elsif ( $TYPEAPP eq 2 && $TYPEMODR ne 'P' ) {    # Régulation PC
        $joursd = '01';
        $DOC->append( 'DATEDDE', "$joursd/$moisd/$anneed" );
    }
    elsif ( $TYPEAPP eq 2 && $TYPEMODR eq 'P' ) {    # Régulation PB
        $DOC->append( 'DATEDDE', "$joursd/$moisd/$anneed" );
    }

    #warn "INFORMATION DATE DE REGULE : $joursd/$moisd/$anneed";
    return 0;
}

sub prepT1 {
    my $vals = shift;

    #$DOC->append(Dumper($vals));
    # Calcul du rappel ou trop paye reste
    $eT1_RAPPEL = $vals->{'RESTExx'} - $vals->{'PERCUxx'};
    $DOC->append( 'RAPPEL', $eT1_RAPPEL );
    return 0;
}

sub prep_tab {
    my ($vals) = shift;
    $ECHEANCIER->append( 'MOISPREL', $MOISPREL );
    $ECHEANCIER->append( 'DATEPREL', $DATEPREL );
    $ECHEANCIER->append( 'MONTPREL', $MONTANTPREL );
    $ECHEANCIER->append('editprel');
    $MOISPREL    = '';
    $DATEPREL    = '';
    $MONTANTPREL = '';
    return 0;
}

sub eche_prel {
    my $vals = shift;
    my ( $iter, $libInpu, $libMontant, $libPrev, $nbrMax );

    #warn "INFO : eche_prel ". Dumper($vals) ."\n";
    if ( $vals->{'IMPUxxx'} ne '00000000' && $vals->{'IMPUxxx'} ne undef ) {
        $MOISPREL    = $vals->{'IMPUxxx'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = ( $vals->{'MONTANT'} . "*" );
        $DATEPREL    = convertDate( $vals->{'PRELEVx'} );
        prep_tab();
    }
    $nbrMax = 45;
    $iter   = 5;
    while ( $iter < $nbrMax ) {
        $libInpu    = "IMPU" . sprintf( "%03s", $iter );
        $libMontant = "MONT" . sprintf( "%03s", $iter + 1 );
        $libPrev    = "PREL" . sprintf( "%03s", $iter + 2 );
        if ( $vals->{$libInpu} ne '00000000' && $vals->{$libInpu} ne undef ) {
            $MOISPREL    = $vals->{$libInpu};
            $MOISPREL    = substr( $MOISPREL, 4, 2 );
            $MOISPREL    = month_to_name($MOISPREL);
            $MONTANTPREL = $vals->{$libMontant};
            $DATEPREL    = convertDate( $vals->{$libPrev} );
            prep_tab();
        }
        $iter += 3;
    }
    return 0;
}

sub date_edition {
    my ( $sec, $min, $hour, $day, $month, $year ) = localtime();
    $month++;
    if ( $month . $day >= '1010' ) {
        $DOC->append( 'DATEEDITION', '1' );
    }
    else {
        $DOC->append( 'DATEEDITION', '2' );
    }
    return 0;
}

exit main(@ARGV);
