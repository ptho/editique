#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::HV;
use oEUser::Descriptor::X1;
use oEUser::Descriptor::NE;
use oEUser::Descriptor::NR;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use strict;
my $INFO;
my (
    $TRK,       $GED,    $DOC,       $VALS,         $COURRIER,
    $P,         $DEPART, $NOM_AVANT, $PRENOM_AVANT, $NOM_AD,
    $PRENOM_AD, @LISTE,  $typcour,   $NOMDEST
);
my (
    @FADDR,    $NUMAD,       $NUMABA, $MONTANT,     $CDE_COUR, $nomUser,
    $SUIVIPAR, @tab_options, $ERAFF,  $EMETTEURGED, $CIVUTIL
);
my $OPT_GED;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );

    #$OPT_GED="";
    # SI ON SOUHAITE METTRE LE DOCUMENT EN GED,
    # IL SUFFIT DE PASSER L'OPRION --edms À oe_compo_link COMME CI-DESSOUS

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMADH', 'NUMABA', 'NOMDEST', 'CONTRAT', 'MONTANT' ]
    );
    $OPT_GED = '--edms';
    oe_new_job('--index');
    &init_Doc();
    my $E1 = oEUser::Descriptor::E1->get();

    #	$E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    #	$E2->debug();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    #	$E7->debug();
    $E7->bind_all_c7();
    my $NE = oEUser::Descriptor::NE->get();

    #$NE->debug();
    $NE->bind_all_c7();
    my $NR = oEUser::Descriptor::NR->get();

    #$NR->debug();
    $NR->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();
    $P   = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'NE' => $NE,
        'NR' => $NR,
        'Z1' => $Z1
    );
    $DOC->append( 'xTYPDOC', 'CDNO' );    ### Type de document
    $P->set_motif_to_denormalized_split('\x{01}');
    my $lastid;
    my $firstDoc = 0;
    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {
            if ( !$firstDoc ) {
                $firstDoc = 1;
            }
            else {
                finAdherent_E1($DOC);
                init_Doc();
            }

            &traite_E1();    #traitement du record E1#

        }
        elsif ( $id eq 'E2' ) {
            &traite_E2();    #traitement du record E2#
        }
        elsif ( $id eq 'E7' ) {
            &traite_E7();    #traitement du record E2#
        }

        elsif ( ( $id eq 'NE' ) or ( $id eq 'NR' ) ) {
            &traite_NE();    #traitement du record NE#
            $DEPART = $DEPART + 1;
        }

        elsif ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

        $lastid = $id;

    }

    finAdherent_E1($DOC);
    oe_compo_link();
    return 0;
}

sub init_Doc() {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    $CDE_COUR     = "";
    $nomUser      = "";
    $INFO         = "";
    @LISTE        = ();
    $NOM_AVANT    = "";
    $PRENOM_AVANT = "";
    $GED          = oEdtk::TexDoc->new();
    $DOC          = oEdtk::TexDoc->new();
    $GED->append( 'xSOURCE', 'BUR' );

    1;
}

sub finAdherent_E1 {
    my ( $DOC, $vals ) = @_;
    $DOC->append_table( 'LISTE', @LISTE );
    $DOC->append($typcour);
    $DOC->append('FINDOC');
    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);
    $TRK->track( 'Doc', 1, $NUMAD, $NUMABA, '', '', $MONTANT );
    $CDE_COUR = "";
}

sub traite_E1() {
    $DOC->append('DEBUT');

    #warn "INFO : dump E1 ". Dumper($VALS) ."\n";
    $NOMDEST = (
"$VALS->{'LICCODC'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
    );
    my $adh = $VALS->{'PENOLIB'};
    my $er  = $VALS->{'ODERCOD'};
    my $civDest = $VALS->{'PENOCOD'};
    $DOC->append('civDest',$civDest);
#    if (   $VALS->{'LICCODC'} eq 'Madame'
#        || $VALS->{'LICCODC'} eq 'Mademoiselle' )
#    {
#        my $civadh = "Chère Adhérente";
#        $DOC->append( 'CIVADH',  $civadh );
#        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
#    }
#    else {
#        my $civadh = "Cher Adhérent";
#        $DOC->append( 'CIVADH',  $civadh );
#        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
#    }
    $GED->append( 'xNOMDEST', $adh );
    $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} );
    $GED->append( 'xCPDEST',  oe_iso_country() . $VALS->{'CODCDXC'} );

    # ADRESSE EMETTEUR.
    my @EADDR = ();
    push( @EADDR, "$VALS->{'PENO025'} $VALS->{'PENO027'}" );
    push( @EADDR, "$VALS->{'PNTR028'} $VALS->{'CPLA029'}" );
    push( @EADDR,
"$VALS->{'PEAD030'} $VALS->{'PEAD031'} $VALS->{'LICN034'} $VALS->{'LIBV035'}"
    );
    push( @EADDR, "$VALS->{'CODC039'} $VALS->{'NOMC040'}" );
    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @EADDR, "$VALS->{'BOIP037'} $VALS->{'LIBL038'}" );
    }
    else {
        push( @EADDR, $VALS->{'BOIP037'} );
    }
    @EADDR = user_cleanup_addr(@EADDR);
    $DOC->append_table( 'EADDR', @EADDR );

    #AFFICHAGE NUMERO DE TELEPHONE
    my $tel = $VALS->{"PECO052"};
    $DOC->append( 'tel', $tel );

    #$DOC->append("ENDADH");
    1;
}

sub traite_E2() {

    #warn "INFO : dump E2 ". Dumper($VALS) ."\n";
    $NUMAD = $VALS->{'ACH1NUM'};
    $DOC->append( 'NUMADH',  $NUMAD );
    $GED->append( 'xIDDEST', $NUMAD );
    $nomUser = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
    $DOC->append( 'NOMUSER', $nomUser );
    $CDE_COUR = $VALS->{'CDCOURx'};
  SWITCH: {
        ############################################### CEA ###################################################
        ( $CDE_COUR eq "CEA-03" )
          && do {

            #warn "CEA-03";
            #$DOC->append ('LETTRECC');
            $typcour = 'LETTRECC';
            $OPT_GED = '--edms';
            last SWITCH;
          };
        ( $CDE_COUR eq "CEA-04" )
          && do {

            #$DOC->append ('LETTRERO');
            $typcour = 'LETTRERO';
            $OPT_GED = '--edms';
            last SWITCH;
          };
    }

####################AJOUT DU NOM-PRENOM DU GESTIONNAIRE DANS L'IDEMET POUR SIMPLICATION DE LA GESTION DANS DOCUBASE.############
    my $GEST = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
################################GEST##########################
    $GED->append( 'xIDEMET', $GEST );
    1;
}

sub traite_E7() {

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $VALS->{'LICCODP'} );
    $VALS->{'LICCODP'} =~ s/FRANCE//i;

    #ADRESSE DESTINATAIRE
    my @DADDR = ();
    $VALS->{'ZONEDHx'} =~ s/^\s+//;
    push( @DADDR, $NOMDEST );
    push( @DADDR, "$VALS->{'PNTREMx'} $VALS->{'CPLADRx'}" );
    push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
    );
    push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );
    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
    }
    else {
        push( @DADDR, $VALS->{'BOIPOSL'} );
    }
    push( @DADDR, $VALS->{'LICCODP'} );
    @DADDR = user_cleanup_addr(@DADDR);

    $DOC->append_table( 'DADDR',  @DADDR );
    $DOC->append_table( 'xADRLN', @DADDR );

}

sub traite_NE() {

    #warn "INFO : dump NE ". Dumper($VALS) ."\n";
    $NOM_AD    = $VALS->{'ACNE047'};
    $PRENOM_AD = $VALS->{'ACNE048'};
    if ( $DEPART eq '0' ) {
        push( @LISTE, $NOM_AD . " " . $PRENOM_AD );
        $NOM_AVANT    = $NOM_AD;
        $PRENOM_AVANT = $PRENOM_AD;
    }
    else {
        if ( ( $NOM_AD eq $NOM_AVANT ) and ( $PRENOM_AD eq $PRENOM_AVANT ) ) {
        }
        else {
            push( @LISTE, $NOM_AD . " " . $PRENOM_AD );
            $NOM_AVANT    = $NOM_AD;
            $PRENOM_AVANT = $PRENOM_AD;
        }
    }
    1;
}

exit main(@ARGV);
