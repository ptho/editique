#!/usr/bin/perl
use utf8;
use strict;
use oEdtk::Main;
use Data::Dumper;
use oEdtk::RecordParser;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::Lib;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::OB;
use oEUser::Descriptor::P7;
use oEUser::Descriptor::Q1;
use oEUser::Descriptor::P8;
use oEUser::Descriptor::Q2;
use oEUser::Descriptor::P9;

#################################################################################
# v0.5 21/11/2006 11:56:54 du squelette d'extraction de donnees pour C7
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données suur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

# DECLARATION DES VARIALBES GLOBALES DE L'APPLICATION :
my $E1_COMPTEUR;
my $OB_COMPTEUR;
my $P8_DOSSIER_PREC;
my ( $SOMME_PSLD023, $TOTAL_REGLE );
my ( $TRK, $IDDEST, $AGENCE, $NOMDEST );
my $DCRICODBQE  = "";
my $DCRICODGUI  = "";
my $DCRINUMCPT  = "";
my $DCRICLECPT  = "";
my $PAYS        = "";
my $IBAN        = "";
my $ID_NATIONAL = "";
my $CODEBIC     = "";
my $TIERS;
my $INDCDX;
my $LOCALITE;

my $SOMME_INDUS_DCRC026 = 0;
my $PR00036             = "";
my $hasp8               = 0;
my $hasp9               = 0;
my $DATERBT             = "";
my $TOTDOS              = 0;
my @tADRDEST;
my $LAST_NOMPRE;
my ( $INDEX, $DOC, $PREMIERE_DATE_SOINS, $DERNIERE_DATE_SOINS );

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDDEST', 'AGENCE', 'NOMDEST', 'COMPTE', 'INDUS' ]
    );

    # trame keys => ['IDDEST', 'AGENCE', 'NOMDEST', 'COMPTE', 'INDUS']
    oe_new_job('--index');

    # INITIALISATION PROPRE AU DOCUMENT
    initDoc();

    #	my $E1_ODERCODERA ="";
    $DOC->append( 'xTYPDOC', 'BPAT' );    ### Type de document

    # XXX We're missing the 'PO' record.
    my $E1 = oEUser::Descriptor::E1::get();

    #	$E1->debug();
    $E1->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7::get();

    #$E7->debug();
    $E7->bind_all_c7();
    my $P7 = oEUser::Descriptor::P7::get();

    #	$P7->debug();
    $P7->bind_all_c7();
    my $Q1 = oEUser::Descriptor::Q1::get();

    #	$Q1->debug();
    $Q1->bind_all_c7();
    my $P8 = oEUser::Descriptor::P8::get();

    #	$P8->debug();
    $P8->bind_all_c7();
    my $Q2 = oEUser::Descriptor::Q2::get();

    #	$Q2->debug();
    $Q2->bind_all_c7();
    my $P9 = oEUser::Descriptor::P9::get();

    #	$P9->debug();
    $P9->bind_all_c7();
    my $OB = oEUser::Descriptor::OB::get();
    $OB->bind( 'OB-TXT', 'MSGOBS' );
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E7' => $E7,
        'P7' => $P7,
        'Q1' => $Q1,
        'P8' => $P8,
        'Q2' => $Q2,
        'P9' => $P9,
        'OB' => $OB,
        'PO' => undef
    );
    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'OB' ) {
            $DOC->append($vals);
            $OB_COMPTEUR++;
        }
        elsif ( $id eq 'E1' ) {
            if ($hasp9) {
                if ($hasp8) {
                    $DOC->append('ActNlATot');
                    $hasp8 = 0;
                }
                else {
                    $DOC->append('ActNlBTot');
                }
                $hasp9 = 0;
            }
            if ( $E1_COMPTEUR > 0 ) {    # && $E1_PENOLIB ne $DATATAB[5]) {
                finTableau($DOC);
                $DOC = initDoc();
            }

            $TIERS =
              (     $vals->{'PENOCOD'} . ""
                  . $vals->{'PENOPRN'} . ""
                  . $vals->{'PENOLIB'} );

            $E1_COMPTEUR++;
        }
        elsif ( $id eq 'E7' ) {

            #warn "INFO : dump E7 ". Dumper($vals) ."\n";

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $vals->{'LICCODP'} );

        # APPEL EXIT EVENTUELS POUR REALISER DES TRAITEMENTS SUR CERTAINS CHAMPS
            $vals->{'LICCODP'} =~ s/FRANCE//i;
            $INDCDX = $vals->{'INDCDXx'};
            if ( $INDCDX == 0 || $INDCDX == ' ' ) {
                $LOCALITE = $vals->{'LIBLOCL'};
            }
            else {
                $LOCALITE = $vals->{'NOMCDXL'};
            }

            my @tADRDEST = ();
            push( @tADRDEST, "$TIERS" );
            push( @tADRDEST, "$vals->{'CPLADRx'} $vals->{'PNTREMx'}" );
            push( @tADRDEST,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
            );
            push( @tADRDEST, $vals->{'BOIPOSL'} );
            push( @tADRDEST, "$vals->{'CODCDXC'} $LOCALITE" );
            push( @tADRDEST, $vals->{'LICCODP'} )
              if exists $vals->{'LICCODP'} && defined $vals->{'LICCODP'};

            @tADRDEST = user_cleanup_addr(@tADRDEST);
            $DOC->append_table( 'xADRLN', @tADRDEST );
            $INDEX->append( 'xIDEMET',  $vals->{'ODERCOD'} );
            $INDEX->append( 'xNOMDEST', $vals->{'PENOLIB'} );
            $INDEX->append( 'xVILDEST', $vals->{'NOMCDXL'} );

# $INDEX->append('xCPDEST',	$vals->{'CODCDXC'});
# Activer la ligne suivante et commenter la précédente pour initialiser la gestion du tri par pays
            $INDEX->append( 'xCPDEST', oe_iso_country() . $vals->{'CODCDXC'} );

        }
        elsif ( $id eq 'P7' ) {
            $IDDEST     = $vals->{'CODF036'};
            $DCRICODBQE = $vals->{'DCRICOD'}
              if ( $vals->{'DCRICOD'} ne "" && $DCRICODBQE eq "" );
            $DCRICODGUI = $vals->{'DCRI015'}
              if ( $vals->{'DCRI015'} ne "" && $DCRICODGUI eq "" );
            $DCRINUMCPT = $vals->{'DCRINUM'}
              if ( $vals->{'DCRINUM'} ne "" && $DCRINUMCPT eq "" );
            $DCRICLECPT = $vals->{'DCRICLE'}
              if ( $vals->{'DCRICLE'} ne "" && $DCRICLECPT eq "" );

            $DOC->append( 'IDENTIF', $vals->{'CODF036'} );
            $DOC->append( 'BANQUE',  $DCRICODBQE );
            $DOC->append( 'GUICHET', $vals->{'DCRI015'} );
            $DOC->append( 'NOMTIER', $vals->{'DCRICPT'} );
            $DOC->append( 'NUMCPT',  "$DCRINUMCPT $DCRICLECPT" );

# $DCRICODBQE="";
# $DCRICODGUI="";
# $DCRINUMCPT="";	# a revoir avec Alain, ça ne me semble pas cohérent avec la ligne 137 (affectation de la valeur 10 lignes plus haut...)
# $DCRICLECPT="";
# $PR00036 ="";
#$DOC->append('INFOBQE');

        }
        elsif ( $id eq 'Q1' ) {

            $PAYS = $vals->{'PAYSxxx'}
              if ( $vals->{'PAYSxxx'} ne "" && $PAYS eq "" );
            $IBAN = $vals->{'IBANxxx'}
              if ( $vals->{'IBANxxx'} ne "" && $IBAN eq "" );
            $ID_NATIONAL = $vals->{'NATIONA'}
              if ( $vals->{'NATIONA'} ne "" && $ID_NATIONAL eq "" );
            $CODEBIC = $vals->{'CODExxx'}
              if ( $vals->{'CODExxx'} ne "" && $CODEBIC eq "" );

            $DOC->append( 'PAYS',       $PAYS );
            $DOC->append( 'IBAN',       $IBAN );
            $DOC->append( 'IDNATIONAL', $ID_NATIONAL );
            $DOC->append( 'CODEBIC', $CODEBIC );    #"$DCRINUMCPT $DCRICLECPT");

            #warn "INFO : dump Q1 ". Dumper($vals) ."\n";
            #warn "INFO : ################ TOTO  ############# \n";
            $DOC->append('INFOBQE');

        }
        elsif (
            $id eq 'P8'

#				&& $P8_DOSSIER_PREC ne $vals->{'CONS004'} # pourquoi pas PSDE093 + PSDE063 ?
            && $LAST_NOMPRE ne "$vals->{'CONS006'} $vals->{'CONS007'}"

            # && $PR0005a_LAST_P8 ne $vals->{'CONS005'}
          )
        {
            if ($hasp9) {
                if ($hasp8) {
                    $DOC->append('ActNlATot');
                    $hasp8 = 0;
                }
                else {
                    $DOC->append('ActNlBTot');
                }
                $hasp9 = 0;
            }

            #			$DOC->append('NUMDOSS', $vals->{'CONS004'});
            $DOC->append( 'NUMSS',  $vals->{'CONS005'} );
            $DOC->append( 'NUMFAC', $vals->{'PSDE063'} . $vals->{'PSDE093'} );

#			warn "INFO : TEST DE PASSAGE.!!!!#$vals->{'PSDE093'}#!!!!!!!!!!!!!!!!#$vals->{'PSDE063'}#!!!!!!!!!!!! \n";
#			$DOC->append('DATERBT', $vals->{'CONSTxx'});
            $DATERBT = $vals->{'CONSTxx'};
            $DOC->append( 'DATENAIS',  $vals->{'PSDEDAT'} );
            $DOC->append( 'DATESOINS', $vals->{'PSEPDFE'} );
            $PREMIERE_DATE_SOINS =
              oe_date_smallest( $PREMIERE_DATE_SOINS, $vals->{'PSEPDFE'} );
            $DERNIERE_DATE_SOINS =
              oe_date_biggest( $DERNIERE_DATE_SOINS, $vals->{'PSEPDFE'} );

            $vals->{'CONS006'} = ucfirst( lc( $vals->{'CONS006'} ) );
            $vals->{'CONS007'} = ucfirst( lc( $vals->{'CONS007'} ) );
            $LAST_NOMPRE       = "$vals->{'CONS006'} $vals->{'CONS007'}";
            $DOC->append( 'NOMPRN', $LAST_NOMPRE );
            $SOMME_INDUS_DCRC026 += $vals->{'DCRCMNT'};
            $TRK->track( 'W', 1, '', '', '', '',
                $vals->{'DCRCMNT'} . " =>INDU IN P8" )
              if ( $vals->{'DCRCMNT'} > 0 );
            P8_test_nouveau_decompte( $DOC, $vals );
            $hasp8 = 1;
        }
        elsif ( $id eq 'Q2' ) {

            #warn "INFO : dump Q2 ". Dumper($vals) ."\n";
        }
        elsif ( $id eq 'P9' ) {
            if ($hasp9) {
                if ($hasp8) {
                    $DOC->append('ActNlA');
                    $hasp8 = 0;
                }
                else {
                    $DOC->append('ActNlB');
                }
            }
            $DOC->append($vals);
            $DOC->append( 'CODACT', $vals->{'PSLDCOD'} );
            $DOC->append( 'NBACT',  $vals->{'PSLDQTE'} );
            $vals->{'PSLDCOE'} =~ s/\.0*$//;

            #			warn " vals->{'PSLDCOE'} = ". $vals->{'PSLDCOE'} ."\n";
            $DOC->append( 'COEF',    $vals->{'PSLDCOE'} );
            $DOC->append( 'MONTANT', $vals->{'PSLD022'} );
            $DOC->append( 'MUTMNT',  $vals->{'PSLD023'} );
            $TOTDOS += $vals->{'PSLD023'};
            $DOC->append( 'totdos', $TOTDOS );
            $hasp9 = 1;

            #			$DOC->append('TOTAL', $vals->{'PSLD023'});
            #			$SOMME_PSLD023 += $vals->{'PSLD023'};

            $TOTAL_REGLE += $vals->{'PSLD023'};
        }
    }

    if ($hasp9) {
        if ($hasp8) {
            $DOC->append('ActNlATot');
            $hasp8 = 0;
        }
        else {
            $DOC->append('ActNlBTot');
        }
        $hasp9 = 0;
    }
    finTableau($DOC);
    oe_compo_link();
    warn "INFO : NB enr. E1=$E1_COMPTEUR NB enr OB=$OB_COMPTEUR\n";
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################
sub initDoc {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    $DCRICODBQE  = "";
    $DCRICODGUI  = "";
    $DCRINUMCPT  = "";
    $DCRICLECPT  = "";
    $PR00036     = "";
    $PAYS        = "";
    $IBAN        = "";
    $ID_NATIONAL = "";
    $CODEBIC     = "";

    $IDDEST              = "";
    $AGENCE              = "";
    $P8_DOSSIER_PREC     = "-1";
    $SOMME_PSLD023       = 0;
    $TOTAL_REGLE         = 0;
    $SOMME_INDUS_DCRC026 = 0;
    undef @tADRDEST;
    $PREMIERE_DATE_SOINS = "";
    $DERNIERE_DATE_SOINS = "";

    $INDEX = oEdtk::TexDoc->new();
    $DOC   = oEdtk::TexDoc->new();
}

# FONCTIONS TRAITEMENTS DONNEES v 20061206-133945

sub P8_test_nouveau_decompte {
    my ( $DOC, $vals ) = @_;

    if ( $P8_DOSSIER_PREC ne $vals->{'CONS004'} ) {

        # SI ON A UN NOUVEAU DÉCOMPTE POUR BÉNÉFICIAIRE POUR UN NOUVEAU S.S.
        #		$DOC->append('somPIX', $SOMME_PSLD023) if ($SOMME_PSLD023);
        $DOC->append( 'totdos', $TOTDOS );
        $TOTDOS = 0;

        $INDEX->append( 'xIDDEST', $IDDEST );

        $P8_DOSSIER_PREC = $vals->{'CONS004'};
        $DOC->append( 'NUMSS', $vals->{'CONS005'} );
    }
    else {
        # XXX XXX XXX
    }
}

sub finTableau {
    my ($DOC) = @_;

    $TOTAL_REGLE = $TOTAL_REGLE - $SOMME_INDUS_DCRC026;

    $DOC->append( 'somPIX',  $SOMME_PSLD023 );
    $DOC->append( 'totindu', $SOMME_INDUS_DCRC026 );
    $DOC->append( 'CONSTxx', $DATERBT )
      ;    # si on la passe plus tard elle n'est pas au bon format

    if ( $TOTAL_REGLE >= 0 ) {
        $DOC->append( 'totregl', $TOTAL_REGLE );

    }
    else {
# il faudrait mettre en place le meme genre de règle de gestion que pour le PSRE1
# en attente du métier 10/09/2011 16:57:00
        $DOC->append( 'totregl', 0 );
    }

    #	if ($TOTAL_REGLE != 0) {
    #		$DOC->append('CONSTxx', $DATERBT);
    #	}
    if ( $SOMME_INDUS_DCRC026 != 0 ) {
        $INDEX->append( 'xCODRUPT', 'I' )
          ;    # mise en place d'une rupture sur les indus pour le lotissement
        $DOC->append('indu');

    }
    else {
        $DOC->append('editRgl');
    }

    $INDEX->append( 'xCLEGEDii',  $PREMIERE_DATE_SOINS );
    $INDEX->append( 'xCLEGEDiii', $DERNIERE_DATE_SOINS );
    $INDEX->append( 'xCLEGEDiv',  $DATERBT );
    $DOC->append('findoc');

    oe_print_to_output_file($INDEX);
    oe_print_to_output_file($DOC);

    $TRK->track( 'Doc', 1, $IDDEST, $AGENCE, $NOMDEST, $DCRINUMCPT,
        $SOMME_INDUS_DCRC026 );
}

exit main(@ARGV);
