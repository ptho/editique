use strict;
use warnings;

use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Data::Dumper;

my $TRK;

sub edit_texte {
    my ( $data, $doc ) = @_;

    #	my $variables = $data->{'variables'};
    my $variables = $data;

    my $agencecp;
    my $nom;
    $$doc->append( 'EJ',        $variables->{'entite_Nom'} );
    $$doc->append( 'ER',        $variables->{'agence_etabl_mere'} );
    $$doc->append( 'NUMVOI',    $variables->{'agence_adr'} );
    $$doc->append( 'CODPOSTAL', $variables->{'agence_cp'} );
    $agencecp = ' ';

    #	$agencecp = $variables->{'agence_cp'}|| '';
    $$doc->append( 'LOCALITE', $variables->{'agence_ville'} );

    my @eaddr = ();
    if ( $variables->{'entite_Nom'} ) {
        push( @eaddr, "$variables->{'entite_Nom'}" );
    }
    if ( $variables->{'agence_etabl_mere'} ) {
        push( @eaddr, "$variables->{'agence_etabl_mere'}" );
    }
    if ( $variables->{'agence_adr'} ) {
        push( @eaddr, "$variables->{'agence_adr'}" );
    }
    if ( $variables->{'agence_cp'} || $variables->{'agence_ville'} ) {
        push( @eaddr,
            "$variables->{'agence_cp'} $variables->{'agence_ville'}" );
    }
    $$doc->append_table( 'adresseAgence', @eaddr );

    my $croix = "X";

    $$doc->append( 'DATE', $variables->{'date_prelevement'} );
    my $DP = "X";
    $$doc->append( 'DP', $croix );

    $$doc->append( 'NOMTIT',    $variables->{'titulaire_cpt_nom'} );
    $$doc->append( 'PRENOMTIT', $variables->{'titulaire_cpt_prenom'} );
    $nom =
      "$variables->{'titulaire_cpt_nom'} $variables->{'titulaire_cpt_prenom'}";
    $$doc->append( 'ADDETIT',   $variables->{'titulaire_cpt_ext_nom_voie'} );
    $$doc->append( 'CPLADRTIT', $variables->{'titulaire_cpt_suite_adr'} );
    $$doc->append( 'CDPOSTIT',  $variables->{'titulaire_cpt_cp'} );
    $$doc->append( 'VILLETIT',  $variables->{'titulaire_cpt_ville'} );

    $variables->{'compte_iban'} =~ s/[\t\s]//g;    ### Supprime les espaces
    $$doc->append( 'IBAN',    ( $variables->{'compte_iban'} ) );
    $$doc->append( 'CODEBIC', ( $variables->{'compte_bic'} ) );
    $$doc->append('finadhesion');
    $TRK->track( 'Doc', 1, $agencecp, $nom );

}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        keys   => [ 'CPAGENCE', 'NOMDEST' ]
    );

    oe_new_job('--index');

    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'SEPA' );
    $doc->append( 'xSOURCE', 'coheris' );
    $doc->append( 'xOWNER',  'coheris' );
    my $data = xml_open( \*IN );
    edit_texte( $data, \$doc );
    $doc->append('findoc');
    oe_print_to_output_file("$doc");
    oe_compo_link();
    return 0;
}

exit main(@ARGV);
