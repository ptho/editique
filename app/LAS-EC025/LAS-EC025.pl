#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Descriptor::I5;
use oEUser::Descriptor::I6;
use oEdtk::TexDoc;
use Data::Dumper;
use strict;

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)

my $TRK;
my $tableau;
my $doc;
my $totalMontant;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => ['']
    );

    oe_new_job();

    # INITIALISATION PROPRE AU DOCUMENT
    my $I5 = oEUser::Descriptor::I5->get();

    #$I5->debug();
    $I5->bind_all_c7();
    my $I6 = oEUser::Descriptor::I6->get();

    #$I6->debug();
    $I6->bind_all_c7();
    $doc     = oEdtk::TexDoc->new();
    $tableau = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'LANS' );    ### Type de document
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'I5'     => $I5,
        'I6'     => $I6,
        'ENTETE' => undef
    );
    my $cheque     = 1;
    my $compteurI5 = 0;
    my $adh;

    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'I5' ) {

            #	warn "INFO : I5 dump ". Dumper($vals) ."\n";
            if ( $compteurI5 > 0 ) {
                $doc->append( 'totalMont', $totalMontant );
                finPage();
            }
            $compteurI5++;
            $totalMontant = 0;
            $tableau->append( 'NUMCCOx', $vals->{'NUMCCOx'} )
              ;    # Num contrat collectif
            $tableau->append( 'GRPASSx', $vals->{'GRPASSx'} );    # Num groupe
            $tableau->append( 'DATECHx', convertDate( $vals->{'DATECHx'} ) )
              ;    # Date de début
            $tableau->append( 'DATEOOH', convertDate( $vals->{'DATE008'} ) )
              ;    # Date de fin
            $tableau->append( 'DATEOAA', $vals->{'DATE011'} )
              ;    # Selection date d'encaissement
            $tableau->append( 'DATENCx', convertDate( $vals->{'DATENCx'} ) )
              ;    # Date de début d'encaissement
            $tableau->append( 'DATEOAO', convertDate( $vals->{'DATE010'} ) )
              ;    # Date de fin d'encaissement
        }
        if ( $id eq 'I6' ) {

            #warn "INFO : I6 dump ". Dumper($vals) ."\n";
            $tableau->append( 'CODPART', $vals->{'CODPART'} );    # Code part
            $tableau->append( 'CODERAx', $vals->{'CODERAx'} );    # ER
            $tableau->append( 'NUMCINx', $vals->{'NUMCINx'} )
              ;    # Num contrat individuel
            $adh = $vals->{'SOUSxxx'} . " " . $vals->{'PREN037'};
            $tableau->append( 'MODREG',  $vals->{'MODRGCx'} );
            $tableau->append( 'ADH',     $adh );
            $tableau->append( 'SOLDExx', $vals->{'SOLDExx'} );    # Solde
            $totalMontant += $vals->{'SOLDExx'};
            $tableau->append('ENDLN');
        }
    }

    # Edition
    $doc->append( 'totalMont', $totalMontant );
    finPage();
    $TRK->track( 'Doc', 1, '' );
    oe_compo_link();
    return 0;
}

sub finPage {
    $doc->append( 'tableau', $tableau );
    $doc->append('ENDPG');
    oe_print_to_output_file("$doc");
    $doc     = oEdtk::TexDoc->new();
    $tableau = oEdtk::TexDoc->new();
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

exit main(@ARGV);
