#! /usr/bin/perl
use warnings;
use strict;
use utf8;
use version; our $VERSION = qv(0.1);

use Carp;
use Data::Dumper qw( Dumper );
use Text::CSV;
use oEdtk::Main;    # juste pour tester bzr update
use oEUser::Lib;    # user_get_aneto_user -- useless here
use oEdtk::Tracking;
use charnames ':full';
use English '-no_match_vars';

use Readonly;
Readonly my $CSV_EOL_CHAR   => "\n";
Readonly my $CSV_SEP_CHAR   => q{;};
Readonly my $CSV_ENCODING   => 'utf-8';
Readonly my $ANNEE_BASE_MCM => 1900;

Readonly my $DEBUG => 1;

# GED
Readonly my $OPT_GED         => '--edms';
Readonly my $GED_TAG_DADDR   => 'DADDR';
Readonly my $GED_TAG_DATEEDT => 'DATEEDI';
Readonly my @TRACKING_FIELDS => (qw( CHAMP_1 CHAMP_2 ));
Readonly my $USER_WEB_EDITIQUE =>
  'ATTEST-LABP';    # Cannot be extracted from manually crafted flows
Readonly my $COUNTRY_FRANCE => 'FRANCE';    # ignored in addresses

# French strings constants
Readonly my @FRENCH_MONTHS => (
    'janvier',   'février', 'mars',     'avril',
    'mai',       'juin',     'juillet',  'août',
    'septembre', 'octobre',  'novembre', 'décembre',
);

Readonly my %JOURS_SEM => (
    1 => 'lundi',
    2 => 'mardi',
    3 => 'mercredi',
    4 => 'jeudi',
    5 => 'vendredi',
    6 => 'samedi',
    7 => 'dimanche',
);

# Headers in the CSV
Readonly my $HEADER_ID_MNT     => 'ID MNT';
Readonly my $HEADER_NUMCTR     => 'Contrat individuel';
Readonly my $HEADER_DATE_ADH   => 'Date effet';
Readonly my $HEADER_COTIS_MENS => 'Cotisation 12';
Readonly my $HEADER_COTIS_ANN  => 'Cotisations Annuelle';

Readonly my $HEADER_CIVILITE => 'Civilité';
Readonly my $HEADER_NOM      => 'Nom';
Readonly my $HEADER_PRENOM   => 'Prénom';

Readonly my $HEADER_NUM_VOIE => 'Numéro voie';
Readonly my $HEADER_BIS_TER  => 'Bis ter quarter';
Readonly my $HEADER_NAT_VOIE => 'Nature voie';
Readonly my $HEADER_NOM_VOIE => 'Voie';

Readonly my $HEADER_ESC_ETAGE => 'Escalier - Etage';
Readonly my $HEADER_BOITE_POS => 'Boite postale';
Readonly my $HEADER_COMPL_ADR => 'Complément adresse';

Readonly my $HEADER_BUR_DISTRI => 'Bureau distributeur cedex';
Readonly my $HEADER_LIEU_DIT   => 'Lieu dit';

Readonly my $HEADER_CODE_POSTAL => 'Code postal';
Readonly my $HEADER_CODE_CEDEX  => 'Code cedex';
Readonly my $HEADER_LOCALITE    => 'Localité';

Readonly my $HEADER_PAYS => 'Pays';

# Peut-etre utile pour les regles metier
Readonly my $HEADER_CODE_PRODUIT          => 'Code produit';
Readonly my $HEADER_CONTRAT_COLLECTIF     => 'Contrat collectif';
Readonly my $HEADER_GROUPE_ASSURE         => 'Groupe assure';
Readonly my $HEADER_CC_GA                 => 'CC_GA';
Readonly my $HEADER_LIBELLE_COURT_PRODUIT => 'Libellé court produit';
Readonly my $HEADER_ER                    => 'ER';
Readonly my $HEADER_CODE_OFFRE            => 'Code offre';
Readonly my $HEADER_LIBELLE_COURT_OFFRE   => 'Libellé court offre';

Readonly my @NEEDED_HEADERS => (
    $HEADER_CIVILITE, $HEADER_NOM,     $HEADER_PRENOM,     # civ_nom_prenom
    $HEADER_NUM_VOIE, $HEADER_BIS_TER, $HEADER_NAT_VOIE,
    $HEADER_NOM_VOIE,                                      # adresse ligne 1
    $HEADER_ESC_ETAGE, $HEADER_BOITE_POS, $HEADER_COMPL_ADR,   # adresse ligne 2
    $HEADER_BUR_DISTRI, $HEADER_LIEU_DIT,                      # adresse ligne 3
    $HEADER_CODE_POSTAL, $HEADER_CODE_CEDEX, $HEADER_LOCALITE, # adresse ligne 4
    $HEADER_PAYS,    # adresse ligne inutile
                     # XXX ne pas oublier les trucs pour les regles metier
);

# Global variables
my ( $TRK, $DOC, $DATEEDI, $ANNEE );

my ( $civ_nom_prenom, $er, $num_ctr, $id_mnt, $cot_ann, $cot_men, @adresse,
    $date_adh );

sub today {
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
      localtime time;
    if ( $mday == 1 ) {
        $mday = '1er';
    }
    $ANNEE = ( $ANNEE_BASE_MCM + $year + 1 );
    return sprintf '%s %s %s %d',
      $JOURS_SEM{$wday}, $mday, $FRENCH_MONTHS[$mon],
      $ANNEE_BASE_MCM + $year;
}

sub pull_info {
    my ( $reverse_headers, $row, @fields ) = @_;
    my @res = ();
    foreach my $field (@fields) {
        push @res, ${$row}[ $reverse_headers->{$field} ];
    }
    return join "\N{SPACE}", @res;
}

sub type_courrier {
    my ( $reverse_headers, $row ) = @_;
    my @a = ( $reverse_headers, $row );
    my $contrat_collectif = pull_info( @a, $HEADER_CONTRAT_COLLECTIF );
    $DOC->append( 'xCLEGEDiv', $contrat_collectif );
#    if ( !$contrat_collectif =~ m{\d*-PI}sxm ) {
#        croak "Groupe d'assure '$contrat_collectif' inconnu";
#    }
    my $code_produit = pull_info( @a, $HEADER_CODE_PRODUIT );
    return ( $code_produit =~ m{PLM\d+\-MNT\d*|PLS\d+\-*\D*MNT\d*}sxm )
      ? 'TEXTSTDGMSMNT'    # XXX use a readable constant
      : ( $code_produit =~ m{PLC\d+\-MNT\d*}sxm )
      ? 'TEXTCPLGMSMNT'    # XXX use a readable constant
      : ( $code_produit =~ m{PLMP\d+\-MNT\d*|PLSP\d+\-*\D*MNT\d*}sxm )
      ? 'TEXTSTDGMSSDIS'    # XXX use a readable constant
      : ( $code_produit =~ m{PLCP\d+\-MNT\d*}sxm )
      ? 'TEXTCPLGMSSDIS'    # XXX use a readable constant
      : croak "Code produit '$code_produit' inconnu";
}
#
sub doc_append_emptytag {
    my ($tag_mame_arg) = @_;
    if ( !defined $tag_mame_arg ) {
        croak 'undef tag_mame_arg';
    }
    if ( !$tag_mame_arg =~ m{^\p{IsLower}+$}sixm ) {
        croak "bad tag_mame_arg '$tag_mame_arg'";
    }
    $DOC->append($tag_mame_arg);
    return 0;
}

sub doc_append_string {
    my ( $tag_mame_arg, $string ) = @_;
    if ( !defined $tag_mame_arg ) {
        croak 'undef tag_mame_arg';
    }
    if ( !$tag_mame_arg =~ m{^\p{IsLower}+$}sixm ) {
        croak "bad tag_mame_arg '$tag_mame_arg'";
    }
    if ( !defined $string ) {
        croak "undef string (tag_mame_arg='$tag_mame_arg')";
    }
    $DOC->append( $tag_mame_arg, $string );
    return 0;
}

sub treat_courrier {
    my ( $reverse_headers, $row ) = @_;
    my @a = ( $reverse_headers, $row );
    $civ_nom_prenom =
      pull_info( @a, $HEADER_CIVILITE, $HEADER_NOM, $HEADER_PRENOM );
    $er       = pull_info( @a, $HEADER_ER );
    $num_ctr  = pull_info( @a, $HEADER_NUMCTR );
    $id_mnt   = pull_info( @a, $HEADER_ID_MNT );
    $date_adh = pull_info( @a, $HEADER_DATE_ADH );
    $cot_ann  = pull_info( @a, $HEADER_COTIS_ANN );
    $cot_men  = pull_info( @a, $HEADER_COTIS_MENS );
    my $pays = uc( pull_info( @a, $HEADER_PAYS ) );
    $pays =~ s/FRA|FRANCE//sxim
      ;  # À intégrer à terme dans le oe_clean_addr_line ou user_cleanup_addr
    $cot_ann =~ s/\,/\./sxm;
    $cot_men =~ s/\,/\./sxm;
    my $villedest = pull_info( @a, $HEADER_LOCALITE );
    @adresse = user_cleanup_addr(
        $civ_nom_prenom,
        pull_info(
            @a,               $HEADER_NUM_VOIE, $HEADER_BIS_TER,
            $HEADER_NAT_VOIE, $HEADER_NOM_VOIE
        ),
        pull_info(
            @a, $HEADER_ESC_ETAGE, $HEADER_BOITE_POS, $HEADER_COMPL_ADR
        ),
        pull_info( @a, $HEADER_BUR_DISTRI, $HEADER_LIEU_DIT ),
        pull_info(
            @a, $HEADER_CODE_POSTAL, $HEADER_CODE_CEDEX, $HEADER_LOCALITE
        ),
        $pays,
    );
    $TRK->track( 'Doc', 1, $er, $civ_nom_prenom, $num_ctr, $id_mnt, $cot_ann );
    $DOC->append( 'xIDDEST', $id_mnt );
    $DOC->append( 'xNOMDEST',   $civ_nom_prenom );
    $DOC->append( 'xSOURCE',  'PREV' );
    $DOC->append( 'xIDEMET',  $er );
    $DOC->append( 'xVILDEST',  $villedest );
    $DOC->append( 'xCLEGEDi',  $num_ctr );
    $DOC->append( 'xCLEGEDii',  $cot_ann );
    $DOC->append( 'xCLEGEDiii',  $date_adh );
    $DOC->append_table( $GED_TAG_DADDR, @adresse );
    doc_append_string( $GED_TAG_DATEEDT, $DATEEDI );
    doc_append_string( 'CIVNOMPRENOM',   $civ_nom_prenom );
    doc_append_string( 'ANNEE',          $ANNEE );
    doc_append_string( 'NUMABA',         $id_mnt );
    doc_append_string( 'NUMCTR',         $num_ctr );
    doc_append_string( 'DATEADH',        $date_adh );
    doc_append_string( 'COTMENS',        $cot_men );
    doc_append_string( 'COTTOTAL',       $cot_ann );
    doc_append_emptytag('ATTESTATION');
    doc_append_emptytag( type_courrier( $reverse_headers, $row ) );
    doc_append_emptytag('newdoc');
    reset_var();
    return 0;
}

sub reset_var {
    @adresse        = undef;
    $civ_nom_prenom = undef;
    $er             = undef;
    $num_ctr        = undef;
    $id_mnt         = undef;
    $cot_ann        = undef;
    $cot_men        = undef;
    return 0;
}

sub treat_csv_file {
    my ($filehandle_arg, $csv_arg) = @_;
    my $n_headers;
    my $reverse_headers = {};    # maps headers to column numbers (0-starting)
    while ( my $row = $csv_arg->getline($filehandle_arg) ) {
        if ( defined $n_headers ) {
            my $n_cols = scalar @{$row};
            croak "n_headers=$n_headers n_cols=$n_cols"
              if $n_cols != $n_headers;
            treat_courrier( $reverse_headers, $row );
#            track();
#            $TRK->track( 'Doc', 1, $er, $civ_nom_prenom, $num_ctr, $id_mnt, $cot_ann );
        }
        else {
            $n_headers = scalar @{$row};
            foreach my $header ( @{$row} ) {
                if ( !$header =~ m{\S}sxm ) {
                    croak "Header '$header' all blanks or empty";
                }
                croak "Header '$header' multiply defined"
                  if exists $reverse_headers->{$header};
                $reverse_headers->{$header} = scalar keys %{$reverse_headers};
            }
            foreach my $header (@NEEDED_HEADERS) {
                if ( !exists $reverse_headers->{$header} ) {
                    croak "Needed header '$header' not found";
                }
            }
        }
    }
    return 0;
}

sub treat_csv {
    my ($flow_file) = @_;
    my $csv = Text::CSV->new(
        { binary => 1, eol => $CSV_EOL_CHAR, sep_char => $CSV_SEP_CHAR } );
    open my $io, "<:encoding($CSV_ENCODING)", $flow_file
      or croak "$flow_file: $OS_ERROR";
    treat_csv_file($io, $csv);
    close $io || return 1;
    track_end();
    return 0;
}

sub track_begin {
    my ( $tracking_job_arg, $user ) = @_;
    $TRK = oEdtk::Tracking->new(
        $tracking_job_arg,
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'ER', 'NOMDEST', 'Num CTR', 'Num ABA', 'Cotis Annuel' ]
    );
    return 0;
}


sub track_end {
    $TRK->track( 'W', 1, q{}, q{}, q{}, q{}, 'Fin de traitement' )
      ;    # "W" as in "Warning"
    return 0;
}

sub init {
    my ( $flow_file_arg, $tracking_job_arg ) = @_;
    if ( !defined $flow_file_arg ) { oe_app_usage() }
    if ( !-e $flow_file_arg ) {
        croak "Flow file '$flow_file_arg' does not exist";
    }
    if ( !-r $flow_file_arg ) {
        croak "Flow file '$flow_file_arg' is not readable";
    }
    if ( !defined $tracking_job_arg ) {
        $tracking_job_arg = Cwd::abs_path($flow_file_arg);
        carp "Using trackingJob='$tracking_job_arg'";
    }
    user_corp_file_prefixe( $flow_file_arg, '_' );
    my $user = $USER_WEB_EDITIQUE;    # user_get_aneto_user($flowFile);
    $DOC = oEdtk::TexDoc->new();
    $DOC->append( 'xTYPDOC', 'ALAB' );
    $DATEEDI = today();
    track_begin( $tracking_job_arg, $user );
    return 0;
}

sub main {
    my ( $flow_file_arg, $tracking_job_arg ) = @_;
    if ( !defined $flow_file_arg || !-e $flow_file_arg ) {
        croak "Usage: $PROGRAM_NAME <flowFile>";
    }
    init( $flow_file_arg, $tracking_job_arg );
    oe_new_job('--index');
    treat_csv($flow_file_arg);
    oe_print_to_output_file($DOC);
    $DOC = oEdtk::TexDoc->new;
    oe_compo_link($OPT_GED);    # XXX
    return 0;
}

exit main(@ARGV);
