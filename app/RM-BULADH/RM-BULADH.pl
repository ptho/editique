#!/usr/bin/env perl
use utf8;
use strict;
use warnings;

#******************************************************************************
#------------------------------ LIBRAIRIES ------------------------------------
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Data::Dumper;
use Readonly;
use warnings;

#******************************************************************************
#-------------------------- VARIABLES TEMPORAIRES -----------------------------
my ($TRK);
my $DATEEFFET     = "";
my $NUMCOLLECT    = "";
my $NOMDEST       = "";
my $NOMJFDEST     = "";
my $PRENOMDEST    = "";
my $ADRDEST       = "";
my $CPDEST        = "";
my $VILLEDEST     = "";
my $NOMSECTION    = "";
my $MATRICULE     = "";
my $CODEOPER      = "";
my $MOISOFFERT    = "";
my $NBR_BENEF     = 0;
my $PRESENCE_CONJ = 1;
my $PRESENCE_ENF  = 1;
my %H_BENEF       = ();
my %H_GTIE        = ();

#******************************************************************************
#-------------------- INITIALISATION VARIABLES CONSTANTES ---------------------
Readonly my $NBR_BENEF_MAX => 4;
Readonly my $TYPCONJ       => 'Conjoint';
Readonly my $TYPENF        => 'Enfant';
Readonly my $TYPAUTR       => 'Autre';

Readonly my $LIBPROFIL => 'profils';
Readonly my $LIBCONFO  => 'conforts';
Readonly my $LIBOPT    => 'options';

Readonly my $LIBWEB     => 'Web';
Readonly my $LIBInterne => 'Interne';

Readonly my $BNOM     => 'benefnom';
Readonly my $BPRENOM  => 'benefprenom';
Readonly my $BDTNAISS => 'benefdtnaiss';
Readonly my $BSS      => 'benefss';
Readonly my $BSEXE    => 'benefsexe';
Readonly my $BTYPE    => 'beneftype';

Readonly my @TAB_LIB => ( $BNOM, $BPRENOM, $BDTNAISS, $BSS, $BSEXE, $BTYPE );
Readonly my @TAB_TYP => ( $TYPCONJ, $TYPENF, $TYPAUTR );

#******************************************************************************
#------------- INITIALISATION TABLE FORMATAGE SITUATION FAMILIALE -------------
Readonly my %H_FAM => (
    "Célibataire" => 1,
    "Marié(e)"    => 2,
    "Union"        => 3,
    "Divorcé(e)"  => 4,
    "Veuf(ve)"     => 5
);

Readonly my %H_BASEFOY => ( 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0 );

#******************************************************************************
#-------------------- INITIALISATION TABLE FORMATAGE MOIS ---------------------
Readonly my %H_FORMAT_MOIS => (
    "00" => 0,
    "01" => 1,
    "02" => 2,
    "03" => 3,
    "04" => 4,
    "05" => 5,
    "06" => 6,
    "07" => 7,
    "08" => 8,
    "09" => 9,
    "10" => 10,
    "11" => 11,
    "12" => 12
);

#******************************************************************************
#----------------- INITIALISATION TABLE FORMATAGE CHIFFRE ---------------------
Readonly my %H_FORMAT_LET => (
    1 => "Un",
    2 => "Deux",
    3 => "Trois",
    4 => "Quatre",
);

#******************************************************************************
#-------------------- INITIALISATION TABLE FORMATAGE SEXE ---------------------
Readonly my %H_FORMAT_SEXE => (
    "F" => 1,
    "H" => 2
);

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------- FONCTION PRINCIPALE -----------------------------------
sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        entity => oe_corporation_set(),
        keys   => [ 'MATRICUL', 'NOMDEST', 'CPAGENCE' ]
    );

    oe_new_job('--index');

    my $doc  = oEdtk::TexDoc->new();
    my $data = xml_open( \*IN );

    lecture_infoCom($data);
    edit_index( \$doc );
    edit_bulletin( $data, \$doc );
    oe_print_to_output_file("$doc");
    oe_compo_link();
    $TRK->track( 'W', 1, '', '', '', '', 'Fin de traitement' );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------- LECTURE D UNE BALISE ------------------------------
sub lire_balis {
    my ( $data, $lib, $nbrMax ) = @_;
    my $valRetour = $data->{$lib} || "";
    if ( $nbrMax > 0 ) {
        $valRetour = substr( $valRetour, 0, $nbrMax );
    }
    return $valRetour;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------------- FONCTION LECTURE INFO COMMUNES------------------------
sub lecture_infoCom {
    my ($data) = @_;
    $DATEEFFET = $data->{'date_effet'} || "";
    $MATRICULE  = lire_balis( $data, 'pers01_matricule',      20 );
    $NUMCOLLECT = lire_balis( $data, 'num_contrat_collectif', 15 );
    $NOMDEST    = lire_balis( $data, 'pers01_nom',            26 );
    $NOMJFDEST  = lire_balis( $data, 'pers_01_nom_jf',        26 );
    $PRENOMDEST = lire_balis( $data, 'pers01_prenom',         26 );
    $ADRDEST    = lire_balis( $data, 'prct_adr',              78 );
    $VILLEDEST  = lire_balis( $data, 'prct_ville',            56 );
    $CPDEST     = $data->{'prct_cp'}             || "";
    $NOMSECTION = $data->{'nom_sectionRattache'} || "";

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------FONCTION EDITION BULLETIN -------------------------------
sub edit_bulletin {
    my ( $data, $doc ) = @_;

    #--------------------------------------------------------------------------
    #------------------ VERIFICATION ET EDITION CONTEXTE ----------------------
    verifAndEdit_Contexte( $data, \$$doc );

    #--------------------------------------------------------------------------
    #------------------ VERIFICATION  ET EDITIONDATE D'EFFET ------------------
    verifAndEdit_DateEffet( \$$doc );

    #--------------------------------------------------------------------------
    #----------------------------- TRACKING VALUES ----------------------------
    trackingValues($data);

    #--------------------------------------------------------------------------
    #------------------ EDITION INFOS MNT PERSONNELLES ET GARANTIES -----------
    edit_InfoMNT( $data, \$$doc );
    edit_InfoPerso( $data, \$$doc );
    edit_InfoProf( $data, \$$doc );
    edit_InfoGarantie( $data, \$$doc );

    #--------------------------------------------------------------------------
    #------------------------ EDITION DES BENEFICIAIRES------------------------
    ## -- Lecture des beneficiaires
    lecture_beneficiaire($data);
    ## -- Edition des beneficiaires
    edit_beneficiaire( \$$doc );

    #--------------------------------------------------------------------------
    #------------------------------ FIN DOCUMENT ------------------------------
    $$doc->append('finbulletin');

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------------- FONCTION VERIFICATION CONTEXTE -------------------------
sub verifAndEdit_Contexte {
    my ( $data, $doc ) = @_;
    my $context = $data->{'contexte'} || "";
    if ( $context ne $LIBWEB && $context ne $LIBInterne ) {
        die
"Error: Le contexte du flux ($context) est different de <<WEB>> et <<Interne>>.\n";
    }
    $$doc->append( 'contexte', $context );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------------- FONCTION VERIFICATION DATE EFFET ----------------------
sub verifAndEdit_DateEffet {
    my ($doc) = @_;
    my @splitDate = split( '/', $DATEEFFET, -1 );
    my $nbr = @splitDate;
    if ( $nbr != 3 ) {
        die
"Error: La date d'effet est de format(JJ/MM/AAAA) et est obligatoire.\n";
    }
    $$doc->append( 'dateEffet', $DATEEFFET );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------------- FONCTION VERIFICATION CODE PROMO -----------------------
sub verif_CodePromo {
    my ($data) = @_;

    my $codePromo = $data->{'code_promo'} || "";
    my @TabPromo = split( "-", $codePromo, -1 );
    my $nbrTmp = @TabPromo;

    if ( !exists( $H_FORMAT_MOIS{ $TabPromo[2] } ) || $nbrTmp <= 2 ) {
        die "Error: Le format du code promo($codePromo) n'est pas correcte.\n";
    }
    $CODEOPER   = $TabPromo[1];
    $MOISOFFERT = $H_FORMAT_MOIS{ $TabPromo[2] };

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------- FONCTION EDITION DES INFOS DE LA MNT -----------------------
sub edit_InfoMNT {
    my ( $data, $doc ) = @_;
    verif_CodePromo($data);
    $$doc->append( 'codeOperation',   $CODEOPER );
    $$doc->append( 'nbrMoisOffert',   $MOISOFFERT );
    $$doc->append( 'numCollectif',    $NUMCOLLECT );
    $$doc->append( 'numGroupeAssure', $data->{'groupe_assures'} || "" );
    $$doc->append( 'idMNT',           lire_balis( $data, 'id_mnt', 8 ) );
    $$doc->append( 'numTransacCb', $data->{'num_transac_cb'} || "" );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------- FONCTION EDITION DES INFOS PERSONNELLES ----------------------
sub edit_InfoPerso {
    my ( $data, $doc ) = @_;
    my @TabEmail  = format_eMail($data);
    my $numFam    = -1;
    my $situatFam = $data->{'pers_01_situation'} || "";

    if ( exists( $H_FAM{$situatFam} ) ) { $numFam = $H_FAM{$situatFam}; }
    $$doc->append( 'persFam',        $numFam );
    $$doc->append( 'persEmailLocal', $TabEmail[0] );
    $$doc->append( 'persEmailDNS',   $TabEmail[1] );

    $$doc->append( 'persNom',       $NOMDEST );
    $$doc->append( 'persNomJf',     $NOMJFDEST );
    $$doc->append( 'persPrenom',    $PRENOMDEST );
    $$doc->append( 'persAdr',       $ADRDEST );
    $$doc->append( 'persCp',        $CPDEST );
    $$doc->append( 'persVille',     $VILLEDEST );
    $$doc->append( 'persCiv',       $data->{'prct_civ'} || "" );
    $$doc->append( 'persDteNais',   $data->{'pers01_dt_naiss'} || "" );
    $$doc->append( 'persRegime',    $data->{'pers01_regime'} || "" );
    $$doc->append( 'persSecu',      $data->{'pers01_ss'} || "" );
    $$doc->append( 'persTelDom',    $data->{'tel_domicile'} || "" );
    $$doc->append( 'persTelMobile', $data->{'tel_mobile'} || "" );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------- FONCTION EDITION DES INFOS PROFESSIONNELLES ------------------
sub edit_InfoProf {
    my ( $data, $doc ) = @_;
    $$doc->append( 'matricule',  $MATRICULE );
    $$doc->append( 'employeur',  lire_balis( $data, 'employeur_nom', 39 ) );
    $$doc->append( 'filiere',    lire_balis( $data, 'employeur_filiere', 22 ) );
    $$doc->append( 'profession', lire_balis( $data, 'pers01_profession', 24 ) );
    $$doc->append( 'dateEmbauche',  $data->{'date_embauche'}    || "" );
    $$doc->append( 'persSatut',     $data->{'pers01_statut'}    || "" );
    $$doc->append( 'persCategorie', $data->{'pers01_categorie'} || "" );
    $$doc->append( 'numSiret',      $data->{'siret'}            || "" );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------- FONCTION EDITION DES INFOS GARANTIES --------------------------
sub edit_InfoGarantie {
    my ( $data, $doc ) = @_;
    my $baseEnf   = "";
    my $baseFoyer = "";
    if ( $data->{'garanties'} ne "" ) {
        $baseFoyer = $data->{'garanties'}->{'base_foyer'} || "";
        $baseEnf   = $data->{'garanties'}->{'base_enf'}   || "";
    }

    if ( !exists( $H_BASEFOY{$baseFoyer} ) ) {
        die "Erreur: La garantie proposee ($baseFoyer) n est pas correcte.\n";
    }

    my $nbrProfil  = lecture_garanties( $data, $LIBPROFIL );
    my $nbrConfort = lecture_garanties( $data, $LIBCONFO );
    my $nbrOption  = lecture_garanties( $data, $LIBOPT );

    $$doc->append( 'baseFoyer',   $baseFoyer );
    $$doc->append( 'baseEnf',     $baseEnf );
    $$doc->append( 'nbrProfils',  $nbrProfil );
    $$doc->append( 'nbrConforts', $nbrConfort );
    $$doc->append( 'nbrOptions',  $nbrOption );
    edit_garantie( \$$doc, $LIBPROFIL );
    edit_garantie( \$$doc, $LIBCONFO );
    edit_garantie( \$$doc, $LIBOPT );
    $$doc->append( 'cotisMens',    $data->{'devis_cotis_mens'}       || "" );
    $$doc->append( 'modalitePaie', $data->{'modalite_paiement'}      || "" );
    $$doc->append( 'optinMNT',     $data->{'optin-email-mnt'}        || "" );
    $$doc->append( 'optinPart',    $data->{'optin-email-partenaire'} || "" );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------------- FONCTION LECTURE DES GARANTIES -----------------------
sub lecture_garanties {
    my ( $data, $options ) = @_;
    my $nbrSsgtie = 0;
    my $ssOption = substr( $options, 0, length($options) - 1 );
    if ( exists( $data->{'garanties'}->{$options} )
        && $data->{'garanties'}->{$options} ne "" )
    {
        if ( ref( $data->{'garanties'}->{$options}->{$ssOption} ) eq "ARRAY" ) {
            foreach
              my $key ( @{ $data->{'garanties'}->{$options}->{$ssOption} } )
            {
                $nbrSsgtie++;
                $H_GTIE{$options}{$nbrSsgtie} = $key;
            }
        }
        else {
            $nbrSsgtie++;
            $H_GTIE{$options}{$nbrSsgtie} =
              $data->{'garanties'}->{$options}->{$ssOption};
        }
    }

    return $nbrSsgtie;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------ FONCTION EDITION DES GARANTIES ----------------------------
sub edit_garantie {
    my ( $doc, $options ) = @_;
    my @tabSsGtie = sort keys( %{ $H_GTIE{$options} } );
    my $nbrSsgtie = @tabSsGtie;

    foreach my $num (@tabSsGtie) {
        $$doc->append( $options . $H_FORMAT_LET{$num},
            $H_GTIE{$options}{$num} );
    }

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------- FONCTION LECTURE DES BENEFICIAIRES -----------------------
sub lecture_beneficiaire {
    my ($data) = @_;
    if ( $data->{'beneficiaires'} eq "" ) {
        warn "pas de bénéficiaire \n";
    }
    else {
        if ( defined( $data->{'beneficiaires'}->{'beneficiaire'} ) ) {
            if ( ref( $data->{'beneficiaires'}->{'beneficiaire'} ) eq "ARRAY" )
            {    ### Il y a plusieurs bénéficiaires à stocker
                foreach
                  my $key ( @{ $data->{'beneficiaires'}->{'beneficiaire'} } )
                {
                    $NBR_BENEF++;
                    stock_beneficiaire($key);
                }
            }
            else {    ### Il n'y a qu'un bénéficiaire
                $NBR_BENEF++;
                stock_beneficiaire(
                    $data->{'beneficiaires'}->{'beneficiaire'} );
            }
        }
    }

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------- FONCTION EDITION DES BENEFICIAIRES ----------------------------
sub edit_beneficiaire {
    my ($doc) = @_;
    my ( $ittype, $itnum, $itlib, $cptBenef, $nbrBenefAff );
    my ( @tabNum, @tabData );
    $cptBenef = 0;
    for ( $ittype = 0 ; $ittype < @TAB_TYP ; $ittype++ ) {
        @tabNum = sort keys( %{ $H_BENEF{ $TAB_TYP[$ittype] } } );
        for ( $itnum = 0 ; $itnum < @tabNum ; $itnum++ ) {
            $cptBenef++;
            if ( $cptBenef <= $NBR_BENEF_MAX ) {
                @tabData = $H_BENEF{ $TAB_TYP[$ittype] }{ $tabNum[$itnum] };
                for ( $itlib = 0 ; $itlib < @TAB_LIB ; $itlib++ ) {
                    $$doc->append(
                        $TAB_LIB[$itlib] . $H_FORMAT_LET{$cptBenef},
                        $H_BENEF{ $TAB_TYP[$ittype] }{ $tabNum[$itnum] }
                          { $TAB_LIB[$itlib] }
                    );
                }
            }
        }
    }

    ## -- Message avertissement
    if ( $NBR_BENEF > $NBR_BENEF_MAX ) {
        if ( $PRESENCE_CONJ == 0 || $PRESENCE_ENF == 0 ) {
            $nbrBenefAff = $NBR_BENEF_MAX;
        }
        else {
            $nbrBenefAff = $NBR_BENEF_MAX - 1;
        }
        warn
"Attention: Seuls les $nbrBenefAff premiers beneficiaires seront affiches.\n";
    }
    else {
        $nbrBenefAff = $NBR_BENEF;
    }
    $$doc->append( 'nbrBenef',    $nbrBenefAff );
    $$doc->append( 'benefEnfant', $PRESENCE_ENF );
    $$doc->append( 'benefConj',   $PRESENCE_CONJ );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------- FONCTION STOCKAGE DES BENEFICIAIRES ----------------------
sub stock_beneficiaire {
    my ($data) = @_;
    my ( $nom, $prenom, $typebenef, $numsexe, $sexe, $dtenaiss, $secu, $type );
    $type = $data->{'pers_role'} || "";
    $nom = mise_en_formeDonnee( $data, 'pers_nom' );
    $nom = substr( $nom, 0, 16 );
    $prenom = mise_en_formeDonnee( $data, 'pers_prenom' );
    $prenom = substr( $prenom, 0, 16 );
    $dtenaiss = $data->{'pers_dt_naiss'} || "";
    $secu     = $data->{'pers_ss'}       || "";
    $sexe     = $data->{'pers_sexe'}     || "";
    $numsexe  = -1;

    if ( substr( $type, 0, 6 ) eq $TYPENF ) {
        $type = substr( $type, 0, 6 );
    }

    if ( exists( $H_FORMAT_SEXE{$sexe} ) ) {
        $numsexe = $H_FORMAT_SEXE{$sexe};
    }

    if ( $type eq $TYPCONJ ) {
        $typebenef     = $TYPCONJ;
        $PRESENCE_CONJ = 0;
    }
    elsif ( $type eq $TYPENF ) {
        $typebenef    = $TYPENF;
        $PRESENCE_ENF = 0;
    }
    else {
        $typebenef = $TYPAUTR;
    }

    $H_BENEF{$typebenef}{$NBR_BENEF}{$BNOM}     = $nom;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BPRENOM}  = $prenom;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BDTNAISS} = $dtenaiss;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BSS}      = $secu;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BSEXE}    = $numsexe;
    $H_BENEF{$typebenef}{$NBR_BENEF}{$BTYPE}    = $typebenef;

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------- FONCTION FORMATAGE ADRESSE MAIL -------------------------
sub format_eMail {
    my ($data) = @_;
    my @tab = ( "", "" );
    my $adresMail = $data->{'email'} || "";
    if ( $adresMail ne "" ) {
        @tab = split( '@', $adresMail, -1 );
        my $nbrTmp = @tab;
        die
          "Error: Le format de l'adresse mail($adresMail) n'est pas correcte.\n"
          unless ( $nbrTmp == 2 );
    }

    return @tab;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------------- FONCTION TRACKING ---------------------------------
sub trackingValues {
    my ($data) = @_;
    $TRK->track( 'Doc', 1, $MATRICULE, $NOMDEST . " " . $PRENOMDEST,
        $NOMSECTION );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------------- REMPLISSAGE FICHIER INDEX -------------------------
sub edit_index {
    my ($doc) = @_;
    $$doc->append( 'xTYPDOC',  'BADH' );
    $$doc->append( 'xSOURCE',  'coheris' );
    $$doc->append( 'xOWNER',   'coheris' );
    $$doc->append( 'xIDDEST',  $NUMCOLLECT );
    $$doc->append( 'xNOMDEST', $NOMDEST . " " . $PRENOMDEST );
    $$doc->append( 'xVILDEST', $VILLEDEST );
    $$doc->append( 'xCPDEST',  $CPDEST );
    $$doc->append( 'xIDEMET',  $NOMSECTION );
    $$doc->append_table( 'xADRLN', $ADRDEST );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------- FONCTION MISE EN FORME DONNEE ----------------------
sub mise_en_formeDonnee {
    my ( $data, $libin ) = @_;
    my $valRetour = "";
    my @splitelt = split( ' ', lc( $data->{$libin} || "" ), -1 );
    if ( @splitelt > 0 ) {
        $valRetour = ucfirst( $splitelt[0] );
        shift(@splitelt);
        foreach my $elt (@splitelt) {
            $valRetour .= " " . ucfirst($elt);
        }
    }

    return $valRetour;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------- EXIT MAIN ---------------------------------
exit main(@ARGV);
