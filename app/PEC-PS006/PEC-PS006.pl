#!/usr/bin/perl
use utf8;
#########################	RECENSEMENT DES EVOLUTIONS	################################################
#Ajout des garanties G2 et G3 pour le SDIS69. Demande sésame 1066945 le 28/01/2014.
#Neutralisation de la règle d'exclusion sur les conventions et groupes fermés. 30/01/2014.
#Ajout de la règle de calcul pour les conventions et groupes fermés. 1068952 le 07/02/2014.
#Retrait des garanties G2 et G3 dans la gestion 1,2,3 santé ces dernières seront dans les cas "autre".
######################################################################################################

use oEdtk::Main 0.50;
use oEUser::Lib;
use Data::Dumper;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::PA;
use oEUser::Descriptor::PB;
use oEUser::Descriptor::PC;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use strict;
use IO::File;
use File::Copy qw/ copy /;
use Readonly;
use File::Basename;
use File::Spec;
use oEdtk::Config qw( config_read );
#################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
#################################################################################

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)
my $TRK;
my $NUMCTR = "";
my $NUMABA;
my $nomPS;
my $nomUser = "";
my $nomadh;
my $adh     = "";
my $numSS   = "";
my $montant = 0;
my $ctact   = "";
my $faxER;
my $codfiness = "";
my $date;
my $decider;
my $pdp   = 0;
my $duree = 0;
my $total;
my $type;
my $produit;
my $cptPC = 0;
my $localite;
my $montantRC;
my $texte;
my $listeGarantieAutre;
my $rfh         = "";
my $ambulatoire = 0;
my $limite;
my $nomdest;
my $pluriel;
my ( $codDMT, $codMT, $libDMT, $libMT, $affObDMT, $affObMT );
my @quantite = ();
my @Garantie;
my %H_VAL_INDEX = ();
Readonly my $NOMAPP   => "NOM-APP";
Readonly my $REFDOC   => "REF-DOC";
Readonly my $TYPDOC   => "TYPE-DOC";
Readonly my $IDPEC    => "ID-PEC";
Readonly my $NUMETA   => "NUM-ETAB";
Readonly my $NOMASSUR => "NOM-ASSURE";
Readonly my $NOMDEST  => "NOM-DEST";
Readonly my $ADRDEST  => "ADRES-DEST";
Readonly my $CPTDEST  => "CPT-DEST";
Readonly my $VILDEST  => "VILLE-DEST";
Readonly my $IDEMET   => "ID-EMETEUR";
Readonly my $DTEEDIT  => "DATE-EDIT";
Readonly my $NUMCTRT  => "NUM-CONTRAT";
Readonly my $NUMSECU  => "NUM-SECU";
Readonly my $MT       => "MONTANT";
Readonly my $ENTITE   => "ENTITE";
Readonly my $NUMDEM   => "NUM-DEM";
Readonly my $DTEARR   => "DATE-ARR";
Readonly my $MSGDCPTE => "MSG-DCPTE";

Readonly my @TAB_TITLE => (
    $NOMAPP,   $REFDOC,  $TYPDOC,  $IDPEC,   $NUMETA,
    $NOMASSUR, $NOMDEST, $ADRDEST, $CPTDEST, $VILDEST,
    $IDEMET,   $DTEEDIT, $NUMCTRT, $NUMSECU, $MT,
    $ENTITE,   $NUMDEM,  $DTEARR,  $MSGDCPTE
);
my ( $sec, $min, $hour, $mday, $mon, $year ) = gmtime(time);

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NOMDEST', 'IDDEST', 'NUMCTR', 'NUMABA', 'IDEMET' ]
    );

    oe_new_job('--index');

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    # avec initialisation propre au document

    my $E1 = oEUser::Descriptor::E1->get();

    #$E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    #	$E2->debug();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    #	$E7->debug();
    $E7->bind_all_c7();
    my $PA = oEUser::Descriptor::PA->get();

    #	$PA->debug();
    $PA->bind_all_c7();
    my $PB = oEUser::Descriptor::PB->get();

    #   $PB->debug();

    #$PB->bind_all();
    $PB->bind_all_c7();
    my $PC = oEUser::Descriptor::PC->get();

    #$PC->debug();
    $PC->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', "CPEC" );    ### Type de document
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'PA' => $PA,
        'PB' => $PB,
        'PC' => $PC,
        'Z1' => $Z1
    );

    InitAdh();
    $year = sprintf( "%04d", $year + 1900 );
    $mon  = sprintf( "%02d", $mon + 1 );
    $mday = sprintf( "%02d", $mday );
    $H_VAL_INDEX{$NOMAPP}  = "PEC-PS006";
    $H_VAL_INDEX{$REFDOC}  = oe_ID_LDOC();
    $H_VAL_INDEX{$TYPDOC}  = "CPEC";
    $H_VAL_INDEX{$ENTITE}  = oe_corporation_set();
    $H_VAL_INDEX{$DTEEDIT} = $year . $mon . $mday;
    my $lastid;
    ################$doc->append (oe_corporation_tag);######
    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'E1' ) {

            my $civDest = $vals->{'PENOCOD'};
            $doc->append( 'civDest', $civDest );
            # warn "INFO : dump E1 ". Dumper($vals) ."\n";
            $doc->append( 'DEBUT', '' );
            $nomdest = (
"$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}"
            );

            $doc->append( 'LOCALITE', 'Bourges' );
            $pdp   = 0;
            $faxER = $vals->{'PECO049'};
            $doc->append( 'FAXER', $faxER );
            $doc->append( 'PDP',   $pdp );

            $nomPS = $vals->{'PENOLIB'};

            $doc->append( 'xNOMDEST', $vals->{'PENOLIB'} );
            $doc->append( 'xCPDEST',  $vals->{'CODCDXC'} );
            $doc->append( 'xVILDEST', $vals->{'NOMCDXL'} );

            $doc->append( 'xSOURCE', 'CTS018' );
            $doc->append("ENDADH");
        }
        elsif ( $id eq 'E2' ) {

            $NUMCTR = $vals->{'ACH1NUM'};
            $doc->append( 'NUMCTR',    $NUMCTR );
            $doc->append( 'xCLEGEDii', $NUMCTR );

            $nomUser =
                $vals->{'PENO079'} . " "
              . $vals->{'PENO083'} . " "
              . $vals->{'PENO084'};
            $doc->append( 'NOMUSER', $nomUser );
            $doc->append("INFO");
            $doc->append( 'xIDEMET', $nomUser );

            $H_VAL_INDEX{$IDEMET}  = $nomUser;
            $H_VAL_INDEX{$NUMCTRT} = $NUMCTR;
        }
        elsif ( $id eq 'E7' ) {

            $vals->{'LICCODP'} =~ s/FRANCE//i;

            #ADRESSE DESTINATAIRE
            my @DADDR = ();
            push( @DADDR, $nomdest );
            push( @DADDR, "$vals->{'PNTREMx'} $vals->{'CPLADRx'}" );
            push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
            );
            push( @DADDR, "$vals->{'BOIPOSL'}" );
            push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );
            push( @DADDR, $vals->{'LICCODP'} );

            @DADDR = user_cleanup_addr(@DADDR);
            $doc->append_table( 'xADRLN', @DADDR );

            my @ADRDEST = (
                $vals->{'CPLADRx'}, $vals->{'PEADNUM'}, $vals->{'PEADBTQ'},
                $vals->{'PEVONAT'}, $vals->{'LIBVOIx'}
            );
            $nomdest =~ s/^\s+//;
            $nomdest =~ s/\s+$//;

            $H_VAL_INDEX{$NOMDEST} = $nomdest;
            $H_VAL_INDEX{$ADRDEST} = calAdresDest( \@ADRDEST );
            $H_VAL_INDEX{$CPTDEST} = $vals->{'CODCDXC'};
            $H_VAL_INDEX{$VILDEST} = $vals->{'NOMCDXL'};
        }
        elsif ( $id eq 'PA' ) {

            # warn "INFO : dump PA ". Dumper($vals) ."\n";
            $doc->append("DEBPA");
            prep_PA( $vals, $doc );
            $codfiness = $vals->{'CODFRSx'};
            $doc->append( 'CODFINESS', $codfiness );
            $doc->append( 'xIDDEST',   $vals->{'CODFRSx'} );
            $doc->append("ENDPA");
            $H_VAL_INDEX{$IDPEC}  = $vals->{'PSDEIDF'};
            $H_VAL_INDEX{$NUMETA} = $vals->{'CODFRSx'};
        }
        elsif ( $id eq 'PB' ) {

            # warn "INFO : dump PB ". Dumper($vals) ."\n";
            $doc->append("DEBPB");
            $date      = convertDate( $vals->{'PSEPDDE'} );
            $montantRC = $vals->{'PSDE045'};
            $doc->append( 'DATE', $date );

#INSERTION DU NUMERO DE CONTACT (ZONE COMPLEMENTAIRE SAISIE PAR LE GESTIONAIRE "PAS TOUJOURS RENSEIGNEE")

            $ctact = $vals->{'PSZZMSG'} ne "" ? $vals->{'PSZZMSG'} : "";
            $doc->append( 'CONTACT', $ctact );

            $adh    = $vals->{'LIBCIVx'} . " " . $vals->{'PSDEPRN'};
            $nomadh = $vals->{'PSDENOM'};
            $doc->append( 'NOMADH', $nomadh );
            $doc->append( 'ADH',    $adh );
            $numSS = $vals->{'PSEPETE'} . " " . $vals->{'PSEOCLE'};
            $doc->append( 'NUMSS',    $numSS );
            $doc->append( 'xCLEGEDi', $numSS );

            #SWITCH DES TYPES DE COURRIER ACCORD OU REFUS
            $decider = $vals->{'DECISIO'};
            if ( $decider eq "0" ) {
                $doc->append( 'LETTRE', {'REFUS'} );
                $doc->append('FINPB');
            }
            elsif ( $decider eq "1" ) {
                $doc->append( 'LETTRE', {'ACCORD'} );
                $doc->append('FINPB');
            }

          #CONTROLE DMT (Discipline médico-tarifaire) et MT (Montant tarifaire)
            $codDMT = $vals->{'PSLNCOD'} ne "" ? $vals->{'PSLNCOD'} : "";
            $libDMT = $vals->{'PSLNLIB'} ne "" ? $vals->{'PSLNLIB'} : "";
            $codMT  = $vals->{'PSLN060'} ne "" ? $vals->{'PSLN060'} : "";
            $libMT  = $vals->{'PSLN066'} ne "" ? $vals->{'PSLN066'} : "";
            if ( length($codDMT) > 0 || length($libDMT) > 0 ) {
                $affObDMT = 0;
            }
            else { $affObDMT = 1; }
            if ( length($codMT) > 0 || length($libMT) > 0 ) {
                $affObMT = 0;
            }
            else { $affObMT = 1; }
            $doc->append( 'OBJDMT', $affObDMT );
            $doc->append( 'OBJMTR', $affObMT );
            if ( length($codDMT) > 0 ) { $doc->append( 'CODEDMT', $codDMT ); }
            if ( length($libDMT) > 0 ) { $doc->append( 'LIBDMT',  $libDMT ); }
            if ( length($codMT) > 0 )  { $doc->append( 'CODEMTR', $codMT ); }
            if ( length($libMT) > 0 )  { $doc->append( 'LIBMTR',  $libMT ); }

            $H_VAL_INDEX{$NUMSECU} = $numSS;
            $H_VAL_INDEX{$NUMDEM}  = $vals->{'PSDE034'};
            $H_VAL_INDEX{$DTEARR}  = $vals->{'PSDE033'};
            $H_VAL_INDEX{$NOMASSUR} =
              $vals->{'PSDEPRN'} . " " . $vals->{'PSDENOM'};
            $H_VAL_INDEX{$MSGDCPTE} = $ctact;
        }
        elsif ( $id eq 'PC' ) {

            # warn "INFO : dump PB ". Dumper($vals) ."\n";
            $cptPC++;
            $limite  = $vals->{'PSEVCOD'};
            $duree   = int $vals->{'PSLDQTE'};
            $type    = $vals->{'PSLDCOD'};
            $produit = $vals->{'PSZZCOD'};

            push( @quantite, $vals->{'PSLDQTE'} );
            prep_PC( $vals, $doc );

        }
        elsif ( $id eq 'Z1' ) {

            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $vals->{'PERF003'};
            $doc->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $doc->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

        $doc->append('ENDPC');

    }

#     REGLES DE GESTION SUR LE CALCUL QUANTITE-MONTANT EN FONCTION DU NOMBRE DE LIGNE PC ET DE LA LIMITE.
    if ( $quantite[0] != 0 ) {
        if ( $cptPC == 2 && $quantite[0] == $quantite[1] && $limite eq ' ' ) {
            $total = $montantRC / $duree;
        }
        elsif ($cptPC == 2
            && $quantite[0] != $quantite[1]
            && $limite eq ' ' )
        {
            $duree = $quantite[0] + $quantite[1];
            $total = $montantRC / $duree;
        }
        elsif ( $cptPC == 2 && $limite eq 'LI' ) {
            $duree = int $quantite[0];
            $total = $montantRC / $duree;
        }
        else {
            $total = $montantRC / $duree;
        }

    }
    else {
        $total = $montantRC;
    }

    if ( $duree > 1 ) {
        $pluriel = 1;
    }
    else {
        $pluriel = 0;
    }

    $doc->append( 'LIMITE',    $limite );
    $doc->append( 'DUREE',     $duree );
    $doc->append( 'PLURIEL',   $pluriel );
    $doc->append( 'MONTANT',   $total );
    $doc->append( 'xCLEGEDiv', $total );

    $H_VAL_INDEX{$MT} = $total;
    $doc->append('FINDOC');

    $TRK->track( 'Doc', 1, $nomPS, $codfiness, $NUMCTR, $NUMABA, $nomUser );

    #LECTURE REPERTOIRE TRANSIT PEC (COMPORTANT INDEX2 ET COPIE PDF)
    my $pathPec = config_read()->{'EDTK_DIR_TRANSITPEC'};
    if ( !-d "$pathPec" ) {
        die "Error: Le repertoire Transit pec n'existe pas.\n";
    }

    #SI GROUPE FERME OU CONVENTION ON SORT, POUR TRAITEMENT NORMAL ON EDITE.
    oe_print_to_output_file($doc);
    $doc->reset();
    oe_compo_link();

    #ECITURE FICHIER INDEX 2 ET COPIE PDF
    writeIndex2AndCopyPDF($pathPec);
    return 0;
}

sub InitAdh {

    # INITIALISATION DES CONSTANTES DE CHAQUE ADHERENT
    undef @quantite;    # TABLEAU DES ACTES
    undef @Garantie;
    $listeGarantieAutre = "";
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub prep_PA {
    my ( $vals, $doc ) = @_;

    $rfh = $vals->{'NOMETBx'};
    return 0;
}

sub prep_PC {
    my ( $vals, $doc ) = @_;

    #	  warn "INFO : dump PC ". Dumper($vals) ."\n";
    #   GESTION DE L'AFFICHAGE DES PARAGRAPHES EN FONCTION DU TYPE
    if (   $type =~ /CP|CPRS|RCMA|RCME|RCMO|RCCH|RCSU|RCMS|CHCA/
        && $produit eq '' )
    {
        $texte = 1;
    }
    else {
        if ( $quantite[0] == 0 ) {
            $texte = 0;
        }
        elsif ( $type eq "BOX" ) {
            $texte       = 3;
            $ambulatoire = 1;
        }
        elsif ( $type eq "BOXRS" ) {
            $texte       = 4;
            $ambulatoire = 1;
        }
        elsif ( $type eq "CPRS" ) {
            $texte = 2;
        }
        else {
            $texte = 5;
        }

    }
    $doc->append( 'AMBULATOIRE', $ambulatoire );
    $doc->append( 'TEXTE',       $texte );

    return 0;
}

sub writeIndex2AndCopyPDF {
    my ($path) = @_;

    #------- NOMS DES FICHIERS SANS EXTENSION A METTRE DANS LE TRANSIT PEC  ---
    my $NomAppAIDDOC = $H_VAL_INDEX{$NOMAPP} . "-" . $H_VAL_INDEX{$REFDOC};
    my $ficSSExt = File::Spec->catfile( $path, $NomAppAIDDOC );

    #--------------------- CREATION FICHIER INDEX 2 ---------------------------
    open( my $fileIndex, ">:encoding(UTF-8)", "$ficSSExt.idx2" )
      or die "Error: Imppossible d'écrire le fichier $ficSSExt.idx2. \n";
    my $csv = Text::CSV->new( { binary => 1, sep_char => ";", eol => "\n" } );

    #--- Titre du fichier index
    $csv->print( $fileIndex, \@TAB_TITLE );

    #--- Ligne de l'index
    my $lineidx2 = "";
    foreach my $elt (@TAB_TITLE) {
        $lineidx2 .= $H_VAL_INDEX{$elt} . ";";
    }
    my @tabLineidx2 = split( ";", $lineidx2 );
    $csv->print( $fileIndex, \@tabLineidx2 );

    close($fileIndex);

    #---------------- COPIE DU PDF SUR LE REPERTOIRE /TRANSIT/PEC --------------
    copy( $H_VAL_INDEX{$NOMAPP} . ".pdf", $ficSSExt . ".pdf" )
      or die "Error: Impossible de copier le fichier $ficSSExt.pdf $!";

    return 0;
}

sub calAdresDest {
    my ($tab)   = @_;
    my @tab_adr = @$tab;
    my $adres   = "";

    #--------------------- Supression blanc debut et fin
    for ( my $iter = 0 ; $iter < @tab_adr ; $iter++ ) {
        $tab_adr[$iter] =~ s/^\s+//;
        $tab_adr[$iter] =~ s/\s+$//;
        if ( $tab_adr[$iter] ne "" ) {
            if ( $adres eq "" ) { $adres = $tab_adr[$iter]; }
            else                { $adres .= " $tab_adr[$iter]"; }
        }
    }
    return $adres;
}

exit main(@ARGV);

