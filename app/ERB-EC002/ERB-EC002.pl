#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Descriptor::EV;
use oEUser::Descriptor::EW;
use oEdtk::TexDoc;
use Data::Dumper;

use strict;
use warnings;

#################################################################################
# v0.5 21/11/2006 11:56:54 du squelette d'extraction de donnees pour C7
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données suur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

# CORPS PRINCIPAL DE L'APPLICATION :

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)

my $TRK;
my $nbrPieces;
my $total;
my $doc;
my $agence;
my $date;
my $adh;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $date = getDateEntete( $argv[0] );

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'AGENCE', 'ADH' ]
    );
    oe_new_job();

    # INITIALISATION PROPRE AU DOCUMENT
    my $EV = oEUser::Descriptor::EV->get();
    $EV->debug();
    $EV->bind_all_c7();
    my $EW = oEUser::Descriptor::EW->get();
    $EW->bind_all_c7();
    $doc = oEdtk::TexDoc->new();
    my $tableau = oEdtk::TexDoc->new();
    my $p = oEdtk::RecordParser->new( \*IN, 'EV' => $EV, 'EW' => $EW );
    $doc->append( 'xTYPDOC', 'EDRB' );    ### Type de document
    initPage();

    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'EV' ) {

            #warn "INFO : dump EV ".Dumper($vals)."\n";
            if ( ( $agence ne $vals->{'NUMSECT'} ) && ( $agence ne 0 ) ) {
                finPage($tableau);
                $tableau = oEdtk::TexDoc->new();
                initPage();
            }
            $agence = $vals->{'NUMSECT'};
            $tableau->append( 'NOMBQEx', $vals->{'NOMBQEx'} );
            $tableau->append( 'NUMCHQx', $vals->{'NUMCHQx'} );
            $tableau->append( 'MTRGDEV', $vals->{'MTRGDEV'} );
            $total += $vals->{'MTRGDEV'};
        }
        if ( $id eq 'EW' ) {
            $tableau->append( 'NUMCONT', $vals->{'NUMCONT'} );
            $tableau->append( 'CIVILxx', $vals->{'CIVILxx'} );
            $tableau->append( 'NOMPAYx', $vals->{'NOMPAYx'} );
            $tableau->append( 'PRENPAY', $vals->{'PRENPAY'} );
            $adh = $vals->{'NOMPAYx'} . " " . $vals->{'PRENPAY'};
            $nbrPieces++;
            $tableau->append('ENDLN');
        }
    }

    # Edition
    finPage($tableau);
    oe_compo_link();
    return 0;
}

sub getDateEntete {
    my $file = shift;
    open( my $fh, '<', "$file" ) or die $!;
    my $line = <$fh>;

    my $date = unpack( 'x157 A8', $line );

    if ($date)
    {    # dans les batch le user est sur la première ligne de données (FLUX)
        close($fh);
        return convertDate($date);
    }

    $line = <$fh>;    # dans les TP le user est sur la seconde ligne (ENTETE)
    close($fh);
    $date = unpack( 'x157 A8', $line );
    return convertDate($date);
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub finPage {
    my $tableau = shift;
    $doc->append( 'tableau',   $tableau );
    $doc->append( 'nbrPieces', $nbrPieces );
    $doc->append( 'total',     $total );
    $doc->append('ENDPG');
    $TRK->track( 'Doc', 1, $adh );
    oe_print_to_output_file("$doc");

    $doc = oEdtk::TexDoc->new();
}

sub initPage {
    ### Edite les données de l'entête
    $doc->append( 'dateET', $date );
    $nbrPieces = 0;
    $total     = 0;
    $agence    = 0;
}

exit main(@ARGV);
