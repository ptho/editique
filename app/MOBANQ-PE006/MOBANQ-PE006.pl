#!/usr/bin/perl
use utf8;
use strict;
use warnings;

use oEdtk::Main 0.50;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Lib;

# use oEUser::XML::Generic;
use Data::Dumper;
use POSIX qw(strftime);

#descriptors
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::V1;
use oEUser::Descriptor::V2;
use oEUser::Descriptor::V3;
use oEUser::Descriptor::Z0;
use oEUser::Descriptor::Z1;

my $file_error = './MOBANQ-PE006_error.txt';
my $date_log = strftime "%d/%m/%Y %H:%M:%S", localtime;

my ( $TRK, $MATRICULE, $AGENCECP, $NOM );
my @EADDR;
my @DADDR;
my $PROFCOD;
my $MODREGL;
my @adr_CTC;
my $DOC = oEdtk::TexDoc->new();
my ( $ID_MNT, $NUM_CONTRAT, $NOMPRENOM );

sub recup_data {

    print 'sub recup_data' . "\n";

    # sleep (1);
    #############################INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION####################################

    my $ENTETE = oEUser::Descriptor::ENTETE::get();

    #$ENTETE->debug();
    $ENTETE->bind_all_c7();

    my $E1 = oEUser::Descriptor::E1::get();

    #$E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2::get();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7::get();
    $E7->bind_all_c7();

    my $V1 = oEUser::Descriptor::V1::get();
    $V1->bind_all_c7();

    my $V2 = oEUser::Descriptor::V2::get();
    $V2->bind_all_c7();

    my $V3 = oEUser::Descriptor::V3::get();
    $V3->bind_all_c7();

    my $Z0 = oEUser::Descriptor::Z0::get();
    $Z0->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1::get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $ENTETE,
        'E1'     => $E1,
        'E2'     => $E2,
        'E7'     => $E7,
        'V1'     => $V1,
        'V2'     => $V2,
        'V3'     => $V3,
        'Z0'     => $Z0,
        'Z1'     => $Z1
    );

    print 'Chargement des donnees du flux termine' . "\n";
    return $p;
}

sub edit_courrier {

    my ( $oparser, $type_doc ) = @_;

    print 'sub edit_courrier' . " $type_doc \n";

    # sleep (5);

    my $cpt_ligne = 0;
    my $cpt_E1    = 0;
    my $bool_edit =
      0;    # passe a 1 si au moins un champs d'enregistrement est nul

    while ( my ( $id, $vals ) = $oparser->next() ) {

  #dump du flux dans un fichier test
  # if ( $cpt_ligne == 0 ) {open (my $desc,'>','./flux_test.txt');close $desc; }
  # open (my $desc,'>>','./flux_test.txt');
  # print $desc $id . "\n";
  # print $desc Dumper($vals) ."\n";
  # close $desc;

        if ( $id eq 'E1' ) {

            # Des que l'on retrouve un E1 on ecrit un nouveau doc
            if ( $cpt_E1 > 0 ) {

                # si ID_MNT ou NUM_CONTRAT definis
                if ( $ID_MNT or $NUM_CONTRAT ) {

                    # tout va bien
                    # print 'ID_MNT =<'. $ID_MNT .">\n";
                    # print 'NUM_CONTRAT =<'. $NUM_CONTRAT .">\n";
                    # print 'bool_edit =<'. $bool_edit .">\n";
                    # sleep (2);
                }
                else {
                    # erreur il manque ID_MNT et NUM_CONTRAT
                    $bool_edit =
                      &gestion_erreur( 'CRITIQUE ID_MNT + NUM_CONTRAT',
                        '', $cpt_ligne, $bool_edit );

                }

                $bool_edit = &emitDoc( $type_doc, $bool_edit );
            }

            ########################
            # Verif champs nuls E1 #
            ########################
            # tout nouveau champ doit etre ajoute a la liste
            $bool_edit =
              &gestion_erreur( 'AGENCE', $vals->{'ODERCOD'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'NOM', $vals->{'PENOLIB'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'PRENOM', $vals->{'PENOPRN'}, $cpt_ligne,
                $bool_edit );
            $bool_edit = &gestion_erreur( 'CIVILITE COURT',
                $vals->{'PENOCOD'}, $cpt_ligne, $bool_edit );
            $bool_edit = &gestion_erreur( 'CIVILITE LONG',
                $vals->{'LICCODC'}, $cpt_ligne, $bool_edit );
            ########################

            my $AGENCE = $vals->{'ODERCOD'};

            @adr_CTC = user_get_tag_adr_CTC($AGENCE);

            # SANTE ou PREVOYANCE
            # SI ODCGCOD = MNT-RC|MUT-RC|MNT-RO => SANTE
            # SINON => PREVOYANCE

            # my $type_presta = 'SANTEPREV';

            if ( $vals->{'ODCGCOD'} =~ /MNT\-RC|MUT\-RC|MNT\-RO/i ) {
                $DOC->append( 'TYPPRESTA', 'SANTE' );
            }
            else {
                $DOC->append( 'TYPPRESTA', 'PREVOYANCE' );
            }

            # elsif ( $type_presta eq 'SANTEPREV')
            # {
            # $DOC->append( 'TYPPRESTA' , 'SANTEPREV' );
            # }
            # else
            # {
            # warn 'Type prestation inconnu ' . "\n";

            # }

            #warn "INFO : $AGENCE => @adr_CTC \n";

            $NOMPRENOM = ucfirst( lc( $vals->{'PENOLIB'} ) ) . " "
              . ucfirst( lc( $vals->{'PENOPRN'} ) );
            $NOMPRENOM =~ s/ +$//;

            $DOC->append( 'NOMPRENOM', $NOMPRENOM );
            $DOC->append( 'PENOCOD',   $vals->{'PENOCOD'} );    #civilite court

            gestionAdrE1( $DOC, $vals );

            $DOC->append( 'LICCODC', $vals->{'LICCODC'} );      #civilite long

            # open (my $desc, '>>' , 'c:\ODCGCOD.txt');
            # print $desc $NOMPRENOM .'   ' . $vals->{'ODCGCOD'} ."\n";
            # close $desc;

            $cpt_E1++;
        }

        if ( $id eq 'E2' ) {

            $DOC->append( 'ACH1NUM', $vals->{'ACH1NUM'} )
              ;    # Numéro de contrat individuel
            $NUM_CONTRAT = $vals->{'ACH1NUM'};
        }

        if ( $id eq 'V1' ) {

            ########################
            # Verif champs nuls V1 #
            ########################
            # tout nouveau champ doit etre ajoute a la liste
            $bool_edit =
              &gestion_erreur( 'PERIREF', $vals->{'PERIREF'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'PERIDAT', $vals->{'PERIDAT'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'CODPA2x', $vals->{'CODPA2x'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'CLEIBAx', $vals->{'CLEIBAx'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'IDTNATx', $vals->{'IDTNATx'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'CODP058', $vals->{'CODP058'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'CLEI059', $vals->{'CLEI059'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'IDTN060', $vals->{'IDTN060'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'CODBICx', $vals->{'CODBICx'}, $cpt_ligne,
                $bool_edit );
            $bool_edit =
              &gestion_erreur( 'CODB061', $vals->{'CODB061'}, $cpt_ligne,
                $bool_edit );
            ########################

            $DOC->append( 'PERIREF', $vals->{'PERIREF'} )
              ;    # reference dossier mobilite bancaire
            $DOC->append( 'PERIDAT', convertDate( $vals->{'PERIDAT'} ) )
              ;    # date de signature du mandat mobilite bancaire

            my $IBANORI =
              $vals->{'CODPA2x'} . $vals->{'CLEIBAx'} . $vals->{'IDTNATx'};
            $IBANORI = &format_IBAN( $IBANORI, $ID_MNT );

            my $IBANCIB =
              $vals->{'CODP058'} . $vals->{'CLEI059'} . $vals->{'IDTN060'};
            $IBANCIB = &format_IBAN( $IBANCIB, $ID_MNT );

            $DOC->append( 'IBANORI', $IBANORI );            # IBAN origine
            $DOC->append( 'IBANCIB', $IBANCIB );            # IBAN cible
            $DOC->append( 'CODBICx', $vals->{'CODBICx'} )
              ;    # id banque origine mais pas de libelle
            $DOC->append( 'CODB061', $vals->{'CODB061'} )
              ;    # id banque cible mais pas de libelle

        }

        if ( $id eq 'V2' ) {

            # si on a des prelevements MAQUETTE 2,3 ou 4
            $DOC->append( 'BOOLPRELEVEMENT', 1 );

            $DOC->append( 'ECC1DAT', convertDate( $vals->{'ECC1DAT'} ) )
              ;    # date de la dernière échéance de prélèvement
            if ( $vals->{'ECD1DAT'} ) {
                $DOC->append( 'ECD1DAT', convertDate( $vals->{'ECD1DAT'} ) )
                  ; # date de la prochaine échéance théorique de prélèvement
            }
            else {
                $DOC->append( 'ECD1DAT', '[date inconnnue]' );
            }
        }

        if ( $id eq 'E7' ) {

            # warn "INFO E7 :  dump ". Dumper($vals) ."\n";
            prepE7( $vals, $DOC );

            # traitement_expediteur();
            # E7_type_edition();
        }

        if ( $id eq 'Z1' ) {

            #BLOC ID-MNT
            # $NUMABA = $vals->{'PERF003'};

            $DOC->append( 'NUMABA', $vals->{'PERF003'} );

            #INDEXATION GED
            $DOC->append( 'xCLEGEDi', $vals->{'PERF003'} );
            $ID_MNT = $vals->{'PERF003'};
        }

        # VIREMENTS OU PRELEVEMENTS

        # si on a des virements MAQUETTE 1,3 ou 4
        # si on a un enregistrement V3 alors on a des prestations (= virement)
        if ( $id eq 'V3' ) {
            $DOC->append( 'BOOLVIREMENT', 1 );
        }

        # si on n'a pas une date de virement (plus utilise)
        #variable test
        # my $date_virement = '';

        # if ( $date_virement )
        # {
        # $DOC->append( 'BOOLDATEVIREMENT', 1 );
        # }

        $cpt_ligne++;

    }

    # edite le dernier adherent (E1 - Z1)
    # si ID_MNT ou NUM_CONTRAT definis
    if ( $ID_MNT or $NUM_CONTRAT ) {

        # tout va bien
        # print 'ID_MNT =<'. $ID_MNT .">\n";
        # print 'NUM_CONTRAT =<'. $NUM_CONTRAT .">\n";
        # print 'bool_edit =<'. $bool_edit .">\n";
        # sleep (2);
    }
    else {
        # erreur il manque ID_MNT et NUM_CONTRAT
        $bool_edit = &gestion_erreur( 'CRITIQUE ID_MNT + NUM_CONTRAT',
            '', $cpt_ligne, $bool_edit );

    }

    $bool_edit = &emitDoc( $type_doc, $bool_edit );

    return 0;
}

sub emitDoc {

    my ( $type_doc, $bool_edit ) = @_;

    print 'sub emitDoc ' . $type_doc . " avec bool_edit = " . $bool_edit . "\n";

    # sleep (5);

    if ( $bool_edit == 0 ) {
        if ( $type_doc == 1 ) {
            $DOC->append('EditDocUn');
        }
        elsif ( $type_doc == 2 ) {
            $DOC->append('EditDocDe');
        }
        elsif ( $type_doc == 3 ) {
            $DOC->append('EditDocTr');
        }
        elsif ( $type_doc == 4 ) {
            $DOC->append('EditDocQu');
        }
        elsif ( $type_doc == 5 ) {
            $DOC->append('EditDocUn');
            $DOC->append('EditDocDe');
            $DOC->append('EditDocTr');
            $DOC->append('EditDocQu');

        }

        # $DOC->append('PIEDDEPAGE');
        $DOC->append('ENDDOC');
        oe_print_to_output_file($DOC);
    }

    # on reinitialise bool_edit = 0
    $bool_edit = 0;

    # on reinitialise les identifiants
    $ID_MNT      = '';
    $NUM_CONTRAT = '';

    # reinitialise DOC pour changement de dossier
    $DOC = oEdtk::TexDoc->new();
    return $bool_edit;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub gestionAdrE1 {
    my ( $doc, $vals ) = @_;

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
    @DADDR = ();
    push( @DADDR,
"$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}"
    );

    @EADDR = ();
    push( @EADDR, $vals->{'PENO027'} );
    push( @EADDR, $vals->{'PENO025'} );

    return 0;
}

############################################################################################################
# Suite à la norme 38: le nom et prenom de destinataire et de d'émetteur sont restés dans le record E1
# Mais les adresses se trouvent dans E7 sont de norme 38
############################################################################################################

sub prepE7 {
    my ( $vals, $DOC ) = @_;
    my $Y;

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $vals->{'LIBLOCL'} );
    oe_clean_addr_line( $vals->{'NOMCDXL'} );
    $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDCDXx'} == 0 ) {

        # CAS 3
        $vals->{'LIBLOCL'} = '';
    }
    elsif ( $vals->{'INDCDXx'} == 1 && index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBLOCL'} = '';
    }

    push( @DADDR, $vals->{'PNTREMx'} );    #PNTREMx
    push( @DADDR, $vals->{'CPLADRx'} );
    push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );                                     #PEADNUM, PEADBTQ, LIBVOIx
    push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );  #BOIPOSL, LIBLOCL,
    push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );  #CODCDXC, NOMCDXL
      # initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $vals->{'PELICCO'} );   #    LICCODP
    $vals->{'LICCODP'} =~ s/FRANCE//i;      # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @DADDR, $vals->{'LICCODP'} );     # LICCODP
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

###################################################################
    # Adresse Émetteur
###################################################################

    oe_clean_addr_line( $vals->{'LIBL022'} );    #  LIBL038
    oe_clean_addr_line( $vals->{'NOMC024'} );    #  NOMC040
    $Y = $vals->{'NOMC024'};                     #   NOMC040
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDC020'} == 0 ) {    #INDC036
                                        # CAS 3
        $vals->{'LIBL022'} = '';        #   LIBL038
    }
    elsif ( $vals->{'INDC020'} == 1 && index( $vals->{'LIBL022'}, $Y ) == -1 )
    {                                   # INDC036,   LIBL038
                                        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBL022'} = '';        #  LIBL038
    }

    push( @EADDR, "$vals->{'CPLA015'} $vals->{'PNTR014'}" );  #PNTR028 , CPLA029
    push(
        @EADDR, "$vals->{'PEAD016'} $vals->{'PEAD017'} "
          .    #PEAD030, PEAD031, LICN034 (inexistant dans norme 38))
          "$vals->{'LIBV019'} $vals->{'BOIP021'}"
    );         #LIBV035 ,   BOIP037
    push( @EADDR, "$vals->{'CODC023'} $vals->{'LIBL022'} $vals->{'NOMC024'}" )
      ;        # CODC039 , LIBL038, NOMC040
    @EADDR = user_cleanup_addr(@EADDR);

    if ( $PROFCOD eq 'CMS-ACS' ) {
        my @ACS = (
            'TSA 30016',
            '42-44 RUE DU GÉNÉRAL DE LARMINAT',
            '33044 BORDEAUX'
        );
        $DOC->append( 'ACS', 1 );
        $DOC->append_table( 'ADDRSECT', @ACS );
        @adr_CTC = ();
        @adr_CTC = (
            'TSA 30016',
            '42-44 RUE DU GÉNÉRAL DE LARMINAT',
            '33044 BORDEAUX'
        );
    }
    else {
        $DOC->append( 'ACS', 0 );
        $DOC->append_table( 'ADDRSECT', @EADDR );

        #       @EADDR = ();
    }

    if ( $MODREGL eq 'Chèque' || $MODREGL eq 'PX' ) {
        if ( $adr_CTC[0] eq 'NULL' || oe_corporation_set() ne 'MNT' ) {    #
                #				$DOC->append		('EADDRFIRST',{'edTitCorp'});
            $DOC->append( 'EADDR', 'ADDRSECT' );
            $DOC->append( 'MSG',      {'MSGA'} );
            $DOC->append( 'MSGTALON', {'MSGTALONA'} );

        }
        else {
            #				$DOC->append		('EADDRFIRST',{'edTitCorp'});
            $DOC->append_table( 'ADDRCTC', @adr_CTC );
            $DOC->append( 'EADDR', 'ADDRCTC' );
            $DOC->append( 'MSG',      {'MSGB'} );
            $DOC->append( 'MSGTALON', {'MSGTALONB'} );
        }

    }
    else {
        $DOC->append( 'EADDR', 'ADDRSECT' );
    }
    return 0;
}

sub gestion_erreur {
    my ( $nom_variable, $ref_variable, $cpt_ligne, $bool_edit ) = @_;

    my $cpt_ligne_erreur = $cpt_ligne + 1;

    # on conserve la valeur 1 si $bool_edit deja egal a 1
    if ( $bool_edit == 1 ) {
        return 1;
    }

    # si valeur de la variable est nulle
    unless ($ref_variable) {
        open( my $desc_error, '>>', $file_error );

        # imprime dans la log
        print $desc_error $date_log
          . '	ERREUR: '
          . $nom_variable
          . " non defini a la ligne num. $cpt_ligne_erreur du flux INFINITE. Document non genere.\n";

        close $desc_error;

        return 1;
    }

    #pas d'erreur rencontree
    return 0;

}

sub format_IBAN {

    # formate IBAN et masque avec des *

    my ( $IBAN, $ACCOUNTSWITCHID ) = @_;

    if ( $IBAN =~
/ ( [a-z]{2} ) ( [0-9a-z]{2} ) ( [0-9a-z]{4} ) ( [0-9a-z]{4} ) ( [0-9a-z]{2} ) ( [0-9a-z]{2} ) ( [0-9a-z]{4} ) ( [0-9a-z]{4} ) ( [0-9a-z]{3} ) /xi
      )
    {
        # masquage IBAN par des *
        # $IBANORI = "$1$2 $3 $4 $5$6 $7 $8 $9";
        $IBAN = "$1** **** **** **$6 $7 $8 ***";

    }
    else {
        open( my $desc_error, '>>', $file_error );
        print $desc_error $date_log
          . "	PB format d'IBAN dans le flux INFINITE pour REF $ACCOUNTSWITCHID \n";
        close $desc_error;
    }

    return $IBAN;

}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user    = user_get_aneto_user( $argv[0] );
    my $OPT_GED = '--edms';

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'ID-MNT', 'NOMDEST', 'NUMADH' ]
    );

    oe_new_job('--index');

    $DOC->append( 'xTYPDOC', 'MOBQ' );
    $DOC->append( 'xSOURCE', 'coheris' );
    $DOC->append( 'xOWNER',  'coheris' );

    my $oparser = &recup_data();

    #type de doc que l'on veut imprimer
    # my $TYPDOC = 1;
    # my $TYPDOC = 2;
    # my $TYPDOC = 3;
    # my $TYPDOC = 4;
    my $TYPDOC = 5;    # ALL DOCS

    print 'IMPRESSION DU DOC ' . $TYPDOC . " ...\n";

    # sleep (5);
    &edit_courrier( $oparser, $TYPDOC );

    # lance la generation du PDF
    oe_compo_link($OPT_GED);

    $TRK->track( 'Doc', 1, $ID_MNT, $NOMPRENOM, $NUM_CONTRAT );
    return 0;
}

exit main(@ARGV);
