#!/usr/bin/perl
use utf8;

#-d:ptkdb
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::Tracking;
use Data::Dumper;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E3;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::OB;
use oEUser::Descriptor::F3;
use oEUser::Descriptor::F3;
use oEUser::Descriptor::H0;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::J2;
use oEUser::Descriptor::L2;
use oEUser::Descriptor::N1;
use oEUser::Descriptor::T1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use Getopt::Long;
use Date::Calc qw(Mktime Gmtime);
use strict;

# DECLARATION DES VARIALBES GLOBALES DE L'APPLICATION :#
my $DOC        = oEdtk::TexDoc->new();
my $TABLEAU    = oEdtk::TexDoc->new();
my $ECHEANCIER = oEdtk::TexDoc->new();
my $TALON      = oEdtk::TexDoc->new();

# Pour le tracking.
my ( $TRK, $NOMPRENOM, $AGENCE );
my @tADRDEST;
my @Garantie;    # TABLEAU DES GARANTIES
my ( $typeAdh, $eT1_montantTotal, $eT1_RAPPEL, $eT1_ECHEANCES,
    $listeGarantieAutre );
my $EDITION_CHEQUE  = 0;
my $EDITION_ECARTEE = 0;
my $EDITION_PB      = 0;
my $FLAG_DOC_CHEQUE = 1;
my ( $USER, $TYPE_EDITION, $FLUX );

# en cas de forçage de la date d'échéance, par exemple quand quelqu'un oublie l'envoi dans le lotissement :
#	-> dans ce cas il faut définir une valeur pour $DTECH_FORCEE, et penser à la supprimer après l'opération d'urgence
my $DTECH_FORCEE;

# $DTECH_FORCEE = "10/07/2013";
my $EMET;
my $NUMABA;
my $TYPEAPP;
my $TYPEMODR;
my $DATEDDE;
my $MOISPREL, my $DATEPREL;
my $MONTANTPREL;
my $MONTANTTOT;
my $TOTMONTTTC;
my $ZONE_EFE = 0;
my $DATIMPU;
my $CUMULMONTANTPREL;
my @adr_CTC;

#my @EADDR;
my $DATCHQ;
my $MODREGL;
my $DATERES;
my @EADDR;
my @DADDR;
my $PROFCOD;
my $IDDEST;
my $NOMDEST;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    Getopt::Long::Configure qw(pass_through);

    my %options = ( date => 0 );
    oEdtk::Main::myGetOptions( \%options, 'date=s' );

    if ( $options{date} ) {
        $DTECH_FORCEE = $options{date};
    }

    user_corp_file_prefixe( $ARGV[0], '_' );

    ( $USER, $TYPE_EDITION, $FLUX ) = user_get_aneto_user( $argv[0] );

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        entity => oe_corporation_set(),
        user   => $USER,
        keys   => [ 'IDDEST', 'AGENCE', 'NOMDEST', 'MODREGL' ]
    );

# POUR LES FLUX AC014, UN MÊME FLUX PEUT SERVIR A LA PRODUCTION DE MASSE ET A LA PRODUCTION A LA DEMANDE
# IL FAUT DISTINGUER LES CAS EN FONCTION DES VALEURS DE L'ENTÊTE DE FLUX ($USER, $TYPE_EDITION, $FLUX)
# POUR LES AC014, AC016, PSRE1, PSRE2 LE MÊME FLUX EST DECLENCHE UNE FOIS POUR LE TRAITEMENT DE MASSE ET UNE FOIS PAR AGENCE
# SEUL LE PREMIER LANCEMENT DOIT ÊTRE TRAITÉ EN MASSE

    if (
           defined($TYPE_EDITION)
        && defined($FLUX)

        # ON ISOLE LES 3 CRITERES QUI DEFINISSENT L'EDITION DE MASSE
        && $USER eq "EDITBA"
        && $TYPE_EDITION eq "EDITAC014"
        && $FLUX eq "AC014"
      )
    {
        $TRK->track( 'Warn', 1, '', '', '',
            "INFO: TRT MASSE ? $FLUX $TYPE_EDITION $DTECH_FORCEE" );
    }
    else {
        $TRK->track( 'Warn', 1, '', '', '',
            "INFO: TRT LOCAL $FLUX $TYPE_EDITION" );
    }

    oe_new_job('--index');
    $DOC->append( 'xTYPDOC', 'ACOT' );    ### Type de document

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    my $E1 = oEUser::Descriptor::E1::get();

    # $E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2::get();

    # $E2->debug();
    $E2->bind_all_c7();
    my $E3 = oEUser::Descriptor::E3::get();
    $E3->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7::get();

    # $E7->debug();
    $E7->bind_all_c7();
    my $F3 = oEUser::Descriptor::F3::get();

    #$F3->debug();
    $F3->bind_all_c7();
    my $H0 = oEUser::Descriptor::H0::get();

    #$H0->debug();
    $H0->bind_all_c7();
    my $H1 = oEUser::Descriptor::H1::get();

    #$H1->debug();
    $H1->bind_all_c7();
    my $J2 = oEUser::Descriptor::J2::get();

    #$J2->debug();
    $J2->bind_all_c7();
    my $L2 = oEUser::Descriptor::L2::get();
    $L2->bind_all_c7();
    my $N1 = oEUser::Descriptor::N1::get();

    #$N1->debug();
    $N1->bind_all_c7();
    my $T1 = oEUser::Descriptor::T1::get();

    #$T1->debug();
    $T1->bind_all_c7();
    my $OB = oEUser::Descriptor::OB::get();
    $OB->bind( 'OB-TXT', 'MSGOBS' );
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'OB' => $OB,
        'E3' => $E3,
        'T1' => $T1,
        'F3' => $F3,
        'N1' => $N1,
        'J2' => $J2,
        'L2' => $L2,
        'H0' => $H0,
        'H1' => $H1,
        'Z1' => $Z1
    );

    my $first = 1;
    while ( my ( $id, $vals ) = $p->next() ) {

        # if ($id eq 'T1') {
        # prepT1($vals, $DOC);
        # }

        if ( $id eq 'E1' ) {
            if ($first) {
                $first = 0;
            }
            else {
                if ( $eT1_montantTotal > 0 ) {
                    InitAdh();
                }
            }
            $AGENCE = $vals->{'ODERCOD'};

            @adr_CTC = user_get_tag_adr_CTC($AGENCE);

            #warn "INFO : $AGENCE => @adr_CTC \n";

            $NOMPRENOM = $vals->{'PENOLIB'} . $vals->{'PENOPRN'};
            $NOMPRENOM =~ s/ +$//;
            $DOC->append( 'PENOLIB', $vals->{'PENOLIB'} );
            $DOC->append( 'PENOPRN', $vals->{'PENOPRN'} );
            gestionAdrE1( $DOC, $vals );

        }

############################################TRAITEMENT DU RECORD E2#############################################
        if ( $id eq 'E2' ) {

            # warn "INFO : dump E2 ". Dumper($vals) ."\n";
            $PROFCOD = $vals->{'PROFCOD'};

            $EMET = oe_uc_sans_accents( $vals->{'PENO084'} );
            $EMET = $vals->{'PENO083'} . " " . $vals->{'PENO084'};

            $MODREGL = $vals->{'LICMODR'};
            if ( $EMET ne " " ) {
                $DOC->append( 'xIDEMET', $EMET );
            }
            else {
                $DOC->append( 'xIDEMET', $AGENCE );
            }

            $DOC->append( 'ACPAMOD',  $vals->{'ACPAMOD'} );
            $DOC->append( 'LICMODR',  $vals->{'LICMODR'} );
            $DOC->append( 'xCODRUPT', $vals->{'LICMODR'} );
            $DOC->append( 'ACCCNUM',  $vals->{'ACCCNUM'} );
            $DOC->append( 'ACGACOD',  $vals->{'ACGACOD'} );
            $DOC->append( 'ACOPCOD',  $vals->{'ACOPCOD'} );
            $DOC->append( 'LIBMODR',  $vals->{'LIBMODR'} );
            $DOC->append( 'ECHPC',    $vals->{'ACFQ065'} );
            $DOC->append( 'NUMGRP',
                $vals->{'ACCCNUM'} . "/" . $vals->{'ACGACOD'} );

            $MODREGL = $vals->{'LICMODR'};

            # if ($vals->{'LICMODR'} eq 'Chèque'){
            # if ($adr_CTC[0] eq 'NULL' || oe_corporation_set() ne 'MNT') { #
            # $DOC->append		('EADDRFIRST',{'edTitCorp'});
            # $DOC->append		('EADDR',		'ADDRSECT');
            # $DOC->append		('MSG',		{ 'MSGA' });
            # $DOC->append		('MSGTALON',	{ 'MSGTALONA'});

            # } else {
            # $DOC->append		('EADDRFIRST',{'edTitCorp'});
            # $DOC->append_table	('ADDRCTC',	@adr_CTC);
            # $DOC->append		('EADDR',		'ADDRCTC');
            # $DOC->append		('MSG',		{ 'MSGB' });
            # $DOC->append		('MSGTALON',	{ 'MSGTALONB' });
            # }

            # }else{
            # $DOC->append		('EADDR',		'ADDRSECT');
            # }

            # traitement type échéance
            traitement_type_eche($vals);

            # traitement de l'expéditeur via le mode de règlement
            $DATEDDE = ( $vals->{'ACOP134'} );

            # traitement_expediteur($vals);
            #Traitement de l'édition
            $IDDEST  = $vals->{'ACH1NUM'};
            $NOMDEST = $vals->{'PENOLIB'};

            # E2_type_edition($vals);
        }

        if ( $id eq 'E7' ) {
            prepE7( $vals, $DOC );
            traitement_expediteur();
            E7_type_edition();
        }

########################################FIN DU TRAITEMENT DU RECORD E2##########################################

######################################TRAITEMENT DU RECORD OB (ZONE EFE)########################################
        if ( $id eq 'OB' ) {
            $ZONE_EFE = 1;    # $ZONE_EFE = 1 si présent
            prepOB($vals);
        }
        else {
            $ZONE_EFE = 0;    # $ZONE_EFE = 0 si absent
        }
########################################FIN DU TRAITEMENT DU RECORD OB##########################################

        if ( $id eq 'F3' ) {

            #warn "INFO : dump F3 ". Dumper($vals) ."\n";
            $DOC->append( 'PERICOD', $vals->{'PERICOD'} );
            $DOC->append( 'PERIGUI', $vals->{'PERI004'} );
            $DOC->append( 'PERINUM', $vals->{'PERINUM'} );
            $DOC->append( 'PERICLE', $vals->{'PERICLE'} );
            $DOC->append( 'IBAN',    $vals->{'PERIIDT'} );
            $DOC->append( 'BIC',     $vals->{'PERI017'} );
            $DOC->append( 'ICS',     $vals->{'ICSxxxx'} );
            $DOC->append( 'RUM',     $vals->{'RUMxxxx'} );

            prepF3_date_echeance( $vals, $DOC );
        }

        if ( $id eq 'H0' ) {

            #warn "INFO :  dump ". Dumper($vals) ."\n";
            H0_triGarantie($vals);
        }

        if ( $id eq 'H1' || $id eq 'N1' ) {

            #warn "INFO N1 :  dump ". Dumper($vals) ."\n";
            AyantDroit($DOC);
            $typeAdh = $vals->{'ACH2COD'};

            #warn "INFO : DUMP = $typeAdh \n";
        }

##########################################TRAITEMENT DU RECORD T1###############################################
        if ( $id eq 'T1' ) {

            #warn "INFO N1 :  dump ". Dumper($vals) ."\n";
            $DOC->append( 'MONTANTTTC', $vals->{'MONTANT'} );
            prepT1( $vals, $DOC );

            # récupération et traitement des échéances pour prélèvement.
            eche_prel($vals);
        }
##########################################FIN DU TRAITEMENT DU RECORD T1########################################

##########################################TRAITEMENT DU RECORD Z1###############################################
        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $vals->{'PERF003'};
            $DOC->append( 'NUMABA', $vals->{'PERF003'} );
            ######INDEXATION GED###################
            $DOC->append( 'xCLEGEDi', $vals->{'PERF003'} )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }
##########################################FIN DU TRAITEMENT DU RECORD Z1###############################################

    }

    emitDoc($DOC);

#warn "CHECK: Editions 'cheque' = $EDITION_CHEQUE / 'ecartee' = $EDITION_ECARTEE\n";

    oe_compo_link();
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################
sub emitDoc {
    if ( $TYPEMODR eq 'A' ) {
        $TABLEAU->append('editpb');
        $DOC->append( 'echeancier', $ECHEANCIER );
    }
    if ( $TYPEMODR eq 'B' ) {
        $TABLEAU->append('editcheque');
        $TALON->append('talcheque');
        $DOC->append( 'talon', $TALON );
    }
    $DOC->append( 'tableau', $TABLEAU );
    $DOC->append('ENDDOC');
    oe_print_to_output_file($DOC);

    #$DOC 		= oEdtk::TexDoc->new();
    $TABLEAU    = oEdtk::TexDoc->new();
    $ECHEANCIER = oEdtk::TexDoc->new();
    $TALON      = oEdtk::TexDoc->new();
    $ZONE_EFE   = 0;
    $TYPEMODR   = "";
    $TYPEAPP    = "";

    return oEdtk::TexDoc->new();
}

# FONCTIONS TRAITEMENTS DONNEES v 20061116-132259
# Conversion de date.

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

# fonction permettant la conversion du mois XX en toutes lettres.

sub month_to_name {
    my @mons =
      qw/Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre Décembre/;
    my $MOISPREL = shift;

    return $mons[ $MOISPREL - 1 ];
}

sub InitAdh {

    # INITIALISATION DES CONSTANTES DE CHAQUE ADHERENT
    undef @tADRDEST;
    undef @Garantie;    # TABLEAU DES GARANTIES
    $typeAdh            = "";
    $eT1_montantTotal   = 0;
    $eT1_RAPPEL         = 0;
    $eT1_ECHEANCES      = 0;
    $listeGarantieAutre = "";
    $DOC                = emitDoc($DOC);
}

# Traitement type édition

# Traitement type échéance

sub traitement_type_eche {
    my $vals = shift;
    if ( $vals->{'ACOPCOD'} =~ /ACSO|ACAV|ACRC|ACTG/ ) {
        $DOC->append( 'TYPEAPP', {'TYPEAPPNVLADH'} );    # Premier appel
        $TYPEAPP = 1;    # Pour écarter les éditions de masse

    }
    elsif ( $vals->{'ACOPCOD'} =~ /ACMC|ACCC|ACMA/ ) {
        $DOC->append( 'TYPEAPP', {'TYPEAPPREG'} );    # REGULARISATION
        $TYPEAPP = 2;    # Pour écarter les éditions de masse

    }
    else {
        $TYPEAPP = 0;    # ANNUEL
    }
    1;
}

# Traitement de l'expéditeur

sub traitement_expediteur {

    #	my ($vals) = shift;
    if ( $MODREGL eq "PB" ) {
        $DOC->append( 'EADDRFIRST', {'edTitCorp'} );
        $DOC->append( 'EADDRCT', 'ADDRA' );
        $TYPEMODR = 'A';    # $TYPEMODR = 1 pour règlement PB
        if ( $TYPEAPP eq 2 ) {

            #			$DATEDDE = ($vals->{'ACOP134'});
            $DOC->append( 'DATEDDE', $DATEDDE );

            #warn 'INFO $DATEDDE :'. $DATEDDE ."\n";
        }
    }
    if ( $MODREGL eq "Chèque" || $MODREGL eq 'PX' ) {
        $TYPEMODR = 'B';    # $TYPEMODR = 2 pour règlement Chèque
    }
    1;
}

sub E7_type_edition {
    my ($vals) = shift;

    $DOC->append( 'xIDDEST',  $IDDEST );
    $DOC->append( 'xNOMDEST', $NOMDEST );
    $DOC->append( 'xCPDEST',  $vals->{'CODC065'} );    #  CODCDXC
    $DOC->append( 'xVILDEST', $vals->{'NOMC066'} );    #  NOMCDXL
    $TRK->track( 'Doc', 1, $IDDEST, $AGENCE, $NOMDEST, $MODREGL );
    1;
}

sub prepF3_date_echeance {
    my ( $vals, $doc ) = @_;

    #warn "INFO : F3 dump ". Dumper($vals) ."\n";

    $doc->append( 'DDEPRDx', $vals->{'DDEPRDx'} );
    $doc->append( 'DFEPRDx', $vals->{'DFEPRDx'} );

   # 	warn "date debut $vals->{'DDEPRDx'}  date de fin : $vals->{'DFEPRDx'} \n";

    if ( $MODREGL eq "Chèque" || $MODREGL eq 'PX' ) {
        $doc->append( 'xCLEGEDii', $vals->{'DDEPRDx'} );
    }
    $doc->append( 'xCLEGEDiii', $vals->{'DFEPRDx'} );

    if ( $vals->{'DDEPRDx'} !~ /(\d{2})\/(\d{2})\/(\d{4})/ ) {
        die
"ERROR: Unexpected date format \'$vals->{'DDEPRDx'}\' at line $. in input file\n";
    }
    my ( $day, $month, $year ) = ( $1, $2, $3 );
    my $time = Mktime( $year, $month, $day, 0, 0, 0 );
    my $now = time;

    #if ($now > $time) {
    #	# Date de debut d'echeance anterieure a la date d'edition.
    #	$time = $now;
    #}

    #($year,$month,$day) = (Gmtime($time))[0..2];
    ( $year, $month, $day ) = ( Gmtime($now) )[ 0 .. 2 ];
    if ( $USER eq "EDITBA" && defined $DTECH_FORCEE && $DTECH_FORCEE != "" ) {

# en cas de forçage de la date d'échéance, par exemple quand quelqu'un oublie l'envoi dans le lotissement :
#	-> dans ce cas il faut définir une valeur pour $DTECH_FORCEE, et penser à la supprimer après l'opération d'urgence
        $doc->append( 'DTECH',     $DTECH_FORCEE );
        $doc->append( 'xCLEGEDiv', $DTECH_FORCEE );
    }
    else {    # On force la date au 10 du mois suivant
        $day = 10;
        if ( $month eq '12' ) {    # pour le cas de changement d'année
            $year++;
            $month = 1;
        }
        else {
            $month++;
        }
        $doc->append( 'DTECH',
            sprintf( "%02d/%02d/%04d", $day, $month, $year ) );
        $doc->append( 'xCLEGEDiv',
            sprintf( "%02d/%02d/%04d", $day, $month, $year ) );
    }
    ( $vals->{'DDEPRDx'} =~ /(\d{2})\/(\d{2})\/(\d{4})/ );
    my $anneed = $3;
    ( $vals->{'DFEPRDx'} =~ /(\d{2})\/(\d{2})\/(\d{4})/ );
    my $joursf = $1;
    my $moisf  = $2;
    my $anneef = $3;
    if ( $TYPEAPP eq 0 ) {    # Annuel
        $DOC->append( 'TYPEAPP', $anneed );
        $DOC->append( 'DFEPRDx', $vals->{'DFEPRDx'} );
    }
    elsif ( $TYPEAPP eq 1 ) {    # Premier appel
        $DOC->append( 'DFEPRDx', $vals->{'DFEPRDx'} );
    }
    elsif ( $TYPEAPP eq 2 ) {    # REGULATION
        $DOC->append( 'DFEPRDx', $vals->{'DFEPRDx'} );
    }
    $DOC->append( 'TESTEFE', $ZONE_EFE );

    #warn "INFO EFE: $ZONE_EFE";
}

sub AyantDroit {
    my ($doc) = @_;

    if ( $typeAdh eq 'ASSPRI' ) {
        my @gars = grep( $_, @Garantie );
        $doc->append( 'Garantie', join( ' ', @gars ) );
    }
}

sub prepOB {
    my ( $vals, $DOC ) = @_;
    $DOC->append($vals);
    1;
}

sub H0_triGarantie {
    my ($vals) = @_;

    #warn "INFO : LIBCOUx = ".$vals->{'LIBCOUx'}."\n"; getc();
    $DATERES = $vals->{'ADMI011'};

    #   warn "INFO : DATERES = ".$vals->{'ADMI011'}."\n";

    if ( $typeAdh ne 'ASSPRI' ) {
        return 1;
    }

#ON NE PREND QUE LES GARANTIES QUI N'ONT PAS DE DATE DE FIN D'EFFET ADMINISTRATIVE.
    if ( $DATERES eq "" ) {

#     warn "INFO : LIBCOUx = ".$vals->{'LIBCOUx'}."\n";
# POUR CHAQUE RANG D ADHERENT (DATATAB 11) ON MEMORISE LA GARANTIE EN LUI DONNANT UN NUMERO D ORDRE.
        if (   ( $vals->{'LIBCOUx'} eq "EV" )
            || ( $vals->{'LIBCOUx'} eq "ES" )
            || ( $vals->{'LIBCOUx'} eq "HO" ) )
        {
            $Garantie[0] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "C1" ) {
            $Garantie[1] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "C2" ) {
            $Garantie[2] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "C3" ) {
            $Garantie[3] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "C4" ) {
            $Garantie[4] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "C5" ) {
            $Garantie[5] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "C6" ) {
            $Garantie[6] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "IH1" ) {
            $Garantie[7] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "IH2" ) {
            $Garantie[8] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "SE" ) {
            $Garantie[9] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "MP" ) {
            $Garantie[10] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBCOUx'} eq "CMU" ) {
            $Garantie[11] = $vals->{'LIBCOUx'};
        }
        elsif ( $vals->{'LIBC008'} eq 'MPCT' ) {
            $Garantie[12] = $vals->{'LIBCOUx'};
        }
        else {
#$vals->{'LIBCOUx'}=$vals->{'LIBC008'}; # on met la garantie courante dans $vals->{'LIBCOUx'}

            if ( !( $Garantie[12] ) )
            { # si on a pas encore de garantie autre alors on ajoutre la garantie courante
                    #warn "Garantie de 12 pas initialisée\n";
                $Garantie[12] = $vals->{'LIBCOUx'};
                $listeGarantieAutre .= " $vals->{'LIBCOUx'}"
                  ;    # on liste toutes les garanties pour éviter les doublons
            }
            else {
                if ( !( $listeGarantieAutre =~ m/$vals->{'LIBCOUx'}/ ) ) {
                    $listeGarantieAutre .= " $vals->{'LIBCOUx'}"
                      ; # on liste toutes les garanties pour ?viter les doublons
                    push( @Garantie, $vals->{'LIBCOUx'} );
                }
            }
        }
    }

}

sub prepT1 {
    my ($vals) = shift;

    #warn "INFO : T1 dump ". Dumper($vals) ."\n";

    $DATIMPU = convertDate( $vals->{'IMPUxxx'} );
    $DOC->append( 'DDEPBx', $DATIMPU );

    if ( $MODREGL eq "PB" ) {
        $DOC->append( 'xCLEGEDii', $DATIMPU );
    }

    # Calcul du rappel ou trop paye reste
    $eT1_RAPPEL = $vals->{'RESTExx'} - $vals->{'PERCUxx'};

    # Calcul du montant
    #warn "Valeur montant= $vals->{'MONTANT'}  et rappel = $eT1_RAPPEL  \n";

    $eT1_ECHEANCES = $vals->{'MONTANT'} - $eT1_RAPPEL;

    # Test sur le $DATATAB 3 si montant total < 0.
    if ( $vals->{'MONTANT'} < 0 ) {
        $eT1_montantTotal = 0;
    }
    else {
        $eT1_montantTotal = $vals->{'MONTANT'};
    }
    if ( $vals->{'LICMODR'} eq 'Chèque' ) {
        $DOC->append( 'MNTTOT', $eT1_montantTotal );
    }
    $DOC->append( 'RAPPEL',  $eT1_RAPPEL );
    $DOC->append( 'MONTANT', $eT1_ECHEANCES );

#warn "INFORMATION MONTANT : $eT1_montantTotal ## $eT1_RAPPEL ## $eT1_ECHEANCES ## $vals->{'RESTExx'} ## $vals->{'PERCUxx'}";
}

sub gestionAdrE1 {
    my ( $doc, $vals ) = @_;

# APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR E1) :
# SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS E1-ZZZZ-NOMCDXLIBACH
# cas 1 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC !C Y	: E1-ZZZZ-BOIPOSLIBLDT + b + E1-ZZZZ-LIBLOCLIBLOC
# cas 2 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC C  Y	: E1-ZZZZ-BOIPOSLIBLDT
# cas 3 : E1-ZZZZ-INDCDX =0 				: E1-ZZZZ-BOIPOSLIBLDT

# ADRESSE DESTINATAIRE.
#	oe_clean_addr_line($vals->{'LIBLOCL'});
#	oe_clean_addr_line($vals->{'NOMCDXL'});
#	$Y = $vals->{'NOMCDXL'};
#	$Y =~ s/CEDEX.*$//;	# ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
#	$Y =~ s/\s+$//;		# ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    #	if ($vals->{'INDCDXx'} == 0) {
    # CAS 3
    #		$vals->{'LIBLOCL'} = '';
    #	} elsif ($vals->{'INDCDXx'} == 1 && index($vals->{'LIBLOCL'}, $Y) == -1) {
    # CAS 1
    #	} else {
    # CAS 2
    #		$vals->{'LIBLOCL'} = '';
    #	}

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
    @DADDR = ();
    push( @DADDR,
"$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}"
    );

#	push(@DADDR, $vals->{'PNTREMx'});
#	push(@DADDR, $vals->{'CPLADRx'});
#	push(@DADDR, "$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}");
#	push(@DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}");
#	push(@DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}");

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
#	oe_iso_country($vals->{'LICCODP'});
#	$vals->{'LICCODP'} =~s/FRANCE//i; 	# ON AFFICHE PAS LE PAYS POUR FRANCE
#	push(@DADDR, $vals->{'LICCODP'});
#	@DADDR = user_cleanup_addr(@DADDR);
#	$doc->append_table('xADRLN', @DADDR);

# AU TOUR DE L'ADRESSE ?METTEUR.
# cas 1 : E1-ZZZZ-INDCDX-2 =1  + LIBLOCLIBLOC-2 !C Y	: E1-ZZZZ-BOIPOSLIBLDT + b + E1-ZZZZ-LIBLOCLIBLOC
# cas 2 : E1-ZZZZ-INDCDX-2 =1  + LIBLOCLIBLOC-2 C  Y	: E1-ZZZZ-BOIPOSLIBLDT
# cas 3 : E1-ZZZZ-INDCDX-2 =0 				: E1-ZZZZ-BOIPOSLIBLDT

#	oe_clean_addr_line($vals->{'LIBL038'});
#	oe_clean_addr_line($vals->{'NOMC040'});
#	$Y = $vals->{'NOMC040'};
#	$Y =~ s/CEDEX.*$//;	# ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
#	$Y =~ s/\s+$//;		# ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    #	if ($vals->{'INDC036'} == 0) {
    # CAS 3
    #		$vals->{'LIBL038'} = '';
    #	} elsif ($vals->{'INDC036'} == 1 && index($vals->{'LIBL038'}, $Y) == -1) {
    # CAS 1
    #	} else {
    # CAS 2
    #		$vals->{'LIBL038'} = '';
    #	}

    @EADDR = ();
    push( @EADDR, $vals->{'PENO027'} );
    push( @EADDR, $vals->{'PENO025'} );

    #	push(@EADDR, "$vals->{'CPLA029'} $vals->{'PNTR028'}");
    #	push(@EADDR, "$vals->{'PEAD030'} $vals->{'PEAD031'} $vals->{'LICN034'} " .
    #	    "$vals->{'LIBV035'} $vals->{'BOIP037'}");
    #	push(@EADDR, "$vals->{'CODC039'} $vals->{'LIBL038'} $vals->{'NOMC040'}");
    #	@EADDR = user_cleanup_addr(@EADDR);
    #	$doc->append_table('ADDRSECT', @EADDR);

    #	$doc->append_table('xADREM', @EADDR);
}

#sub prepT1 {
#	my $vals = shift;
#$DOC->append(Dumper($vals));
# Calcul du rappel ou trop paye reste
#	$eT1_RAPPEL = $vals->{'RESTExx'} - $vals->{'PERCUxx'};
#	$MONTANTTOT = $TOTMONTTTC - $eT1_RAPPEL;
#	$DOC->append('RAPPEL', $eT1_RAPPEL);
#1;
#}

############################################################################################################
# Suite à la norme 38: le nom et prenom de destinataire et de d'émetteur sont restés dans le record E1
# Mais les adresses se trouvent dans E7 sont de norme 38
############################################################################################################

sub prepE7 {
    my ( $vals, $DOC ) = @_;
    my $Y;

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $vals->{'LIBLOCL'} );
    oe_clean_addr_line( $vals->{'NOMCDXL'} );
    $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDCDXx'} == 0 ) {

        # CAS 3
        $vals->{'LIBLOCL'} = '';
    }
    elsif ( $vals->{'INDCDXx'} == 1 && index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBLOCL'} = '';
    }

    push( @DADDR, $vals->{'PNTREMx'} );    #PNTREMx
    push( @DADDR, $vals->{'CPLADRx'} );
    push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );                                     #PEADNUM, PEADBTQ, LIBVOIx
    push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );  #BOIPOSL, LIBLOCL,
    push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );  #CODCDXC, NOMCDXL
      # initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $vals->{'PELICCO'} );   #    LICCODP
    $vals->{'LICCODP'} =~ s/FRANCE//i;      # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @DADDR, $vals->{'LICCODP'} );     # LICCODP
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

###################################################################
    # Adresse Émetteur
###################################################################

    oe_clean_addr_line( $vals->{'LIBL022'} );    #  LIBL038
    oe_clean_addr_line( $vals->{'NOMC024'} );    #  NOMC040
    $Y = $vals->{'NOMC024'};                     #   NOMC040
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDC020'} == 0 ) {    #INDC036
                                        # CAS 3
        $vals->{'LIBL022'} = '';        #   LIBL038
    }
    elsif ( $vals->{'INDC020'} == 1 && index( $vals->{'LIBL022'}, $Y ) == -1 )
    {                                   # INDC036,   LIBL038
                                        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBL022'} = '';        #  LIBL038
    }

    push( @EADDR, "$vals->{'CPLA015'} $vals->{'PNTR014'}" );  #PNTR028 , CPLA029
    push(
        @EADDR, "$vals->{'PEAD016'} $vals->{'PEAD017'} "
          .    #PEAD030, PEAD031, LICN034 (inexistant dans norme 38))
          "$vals->{'LIBV019'} $vals->{'BOIP021'}"
    );         #LIBV035 ,   BOIP037
    push( @EADDR, "$vals->{'CODC023'} $vals->{'LIBL022'} $vals->{'NOMC024'}" )
      ;        # CODC039 , LIBL038, NOMC040
    @EADDR = user_cleanup_addr(@EADDR);

    if ( $PROFCOD eq 'CMS-ACS' ) {
        my @ACS = (
            'TSA 30016',
            '42-44 RUE DU GÉNÉRAL DE LARMINAT',
            '33044 BORDEAUX'
        );
        $DOC->append( 'ACS', 1 );
        $DOC->append_table( 'ADDRSECT', @ACS );
        @adr_CTC = ();
        @adr_CTC = (
            'TSA 30016',
            '42-44 RUE DU GÉNÉRAL DE LARMINAT',
            '33044 BORDEAUX'
        );
    }
    else {
        $DOC->append( 'ACS', 0 );
        $DOC->append_table( 'ADDRSECT', @EADDR );

        #       @EADDR = ();
    }

    if ( $MODREGL eq 'Chèque' || $MODREGL eq 'PX' ) {
        if ( $adr_CTC[0] eq 'NULL' || oe_corporation_set() ne 'MNT' ) {    #
                #				$DOC->append		('EADDRFIRST',{'edTitCorp'});
            $DOC->append( 'EADDR', 'ADDRSECT' );
            $DOC->append( 'MSG',      {'MSGA'} );
            $DOC->append( 'MSGTALON', {'MSGTALONA'} );

        }
        else {
            #				$DOC->append		('EADDRFIRST',{'edTitCorp'});
            $DOC->append_table( 'ADDRCTC', @adr_CTC );
            $DOC->append( 'EADDR', 'ADDRCTC' );
            $DOC->append( 'MSG',      {'MSGB'} );
            $DOC->append( 'MSGTALON', {'MSGTALONB'} );
        }

    }
    else {
        $DOC->append( 'EADDR', 'ADDRSECT' );
    }

}

sub prep_tab {
    my ($vals) = shift;
    $ECHEANCIER->append( 'MOISPREL', $MOISPREL );
    $ECHEANCIER->append( 'DATEPREL', $DATEPREL );
    $ECHEANCIER->append( 'MONTPREL', $MONTANTPREL );
    $ECHEANCIER->append('editprel');
    $MOISPREL = '', $DATEPREL = '';
    $MONTANTPREL = '';
    1;
}

sub eche_prel {
    my $vals = shift;

    #warn "INFO : eche_prel ". Dumper($vals) ."\n";
    if ( $vals->{'IMPUxxx'} ne '00000000' && $vals->{'IMPUxxx'} ne undef ) {
        $MOISPREL    = $vals->{'IMPUxxx'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = ( $vals->{'MONTANT'} . "*" );
        $DATEPREL    = convertDate( $vals->{'PRELEVx'} );
        prep_tab();
    }

    if ( $vals->{'IMPU005'} ne '00000000' && $vals->{'IMPU005'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU005'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT006'};
        $DATEPREL    = convertDate( $vals->{'PREL007'} );
        prep_tab();
    }

    if ( $vals->{'IMPU008'} ne '00000000' && $vals->{'IMPU008'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU008'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT009'};
        $DATEPREL    = convertDate( $vals->{'PREL010'} );
        prep_tab();
    }

    if ( $vals->{'IMPU011'} ne '00000000' && $vals->{'IMPU011'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU011'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT012'};
        $DATEPREL    = convertDate( $vals->{'PREL013'} );
        prep_tab();
    }

    if ( $vals->{'IMPU014'} ne '00000000' && $vals->{'IMPU014'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU014'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT015'};
        $DATEPREL    = convertDate( $vals->{'PREL016'} );
        prep_tab();
    }

    if ( $vals->{'IMPU017'} ne '00000000' && $vals->{'IMPU017'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU017'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT018'};
        $DATEPREL    = convertDate( $vals->{'PREL019'} );
        prep_tab();
    }

    if ( $vals->{'IMPU020'} ne '00000000' && $vals->{'IMPU020'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU020'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT021'};
        $DATEPREL    = convertDate( $vals->{'PREL022'} );
        prep_tab();
    }

    if ( $vals->{'IMPU023'} ne '00000000' && $vals->{'IMPU023'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU023'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT024'};
        $DATEPREL    = convertDate( $vals->{'PREL025'} );
        prep_tab();
    }

    if ( $vals->{'IMPU026'} ne '00000000' && $vals->{'IMPU026'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU026'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT027'};
        $DATEPREL    = convertDate( $vals->{'PREL028'} );
        prep_tab();
    }

    if ( $vals->{'IMPU029'} ne '00000000' && $vals->{'IMPU029'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU029'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT030'};
        $DATEPREL    = convertDate( $vals->{'PREL031'} );
        prep_tab();
    }

    if ( $vals->{'IMPU032'} ne '00000000' && $vals->{'IMPU032'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU032'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT033'};
        $DATEPREL    = convertDate( $vals->{'PREL034'} );
        prep_tab();
    }

    if ( $vals->{'IMPU035'} ne '00000000' && $vals->{'IMPU035'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU035'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT036'};
        $DATEPREL    = convertDate( $vals->{'PREL037'} );
        prep_tab();
    }

    if ( $vals->{'IMPU038'} ne '00000000' && $vals->{'IMPU038'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU038'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT039'};
        $DATEPREL    = convertDate( $vals->{'PREL040'} );
        prep_tab();
    }

    if ( $vals->{'IMPU041'} ne '00000000' && $vals->{'IMPU041'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU041'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT042'};
        $DATEPREL    = convertDate( $vals->{'PREL043'} );
        prep_tab();
    }

    if ( $vals->{'IMPU044'} ne '00000000' && $vals->{'IMPU044'} ne undef ) {
        $MOISPREL    = $vals->{'IMPU044'};
        $MOISPREL    = substr( $MOISPREL, 4, 2 );
        $MOISPREL    = month_to_name($MOISPREL);
        $MONTANTPREL = $vals->{'MONT045'};
        $DATEPREL    = convertDate( $vals->{'PREL046'} );
        prep_tab();
    }

    $CUMULMONTANTPREL =
      $vals->{'MONTANT'} +
      $vals->{'MONT006'} +
      $vals->{'MONT009'} +
      $vals->{'MONT012'} +
      $vals->{'MONT015'} +
      $vals->{'MONT018'} +
      $vals->{'MONT021'} +
      $vals->{'MONT024'} +
      $vals->{'MONT027'} +
      $vals->{'MONT030'} +
      $vals->{'MONT033'} +
      $vals->{'MONT036'} +
      $vals->{'MONT039'} +
      $vals->{'MONT042'} +
      $vals->{'MONT045'};

    if ( $vals->{'LICMODR'} ne 'Chèque' ) {
        $DOC->append( 'MNTTOT', $CUMULMONTANTPREL );
    }
    return 0;
}
exit main(@ARGV);
