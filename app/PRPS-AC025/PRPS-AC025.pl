#!/usr/bin/perl
use oEdtk::Main 0.50;
use Data::Dumper;
use oEUser::Lib;
use strict;
use oEdtk::Tracking;
use oEUser::Descriptor::C1;
use oEUser::Descriptor::C2;
use oEUser::Descriptor::E3;
use oEUser::Descriptor::F5;
use oEUser::Descriptor::L3;
use oEUser::Descriptor::L4;
use oEUser::Descriptor::L5;
use oEUser::Descriptor::L6;
use oEUser::Descriptor::L7;
use oEUser::Descriptor::LJ;
use oEUser::Descriptor::T3;
use oEUser::Descriptor::T4;
use oEUser::Descriptor::T5;
use oEUser::Descriptor::T6;
use oEUser::Descriptor::T7;
use oEUser::Descriptor::OD;
use oEUser::Descriptor::OE;
use oEUser::Descriptor::ENTETE;
use oEdtk::RecordParser;
use oEdtk::TexDoc;

#use oEdtk::Config (config_read);
use File::Basename;

#use Cwd;
my (
    $TRK,     $DOC_parent, $DOC,      $GED,     $IDCOLL,
    $SECTION, $NOMDEST,    $PART,     $CODGRAx, $CODPARx,
    $ACH1NUM, $NOMIDX,     $DATMPREV, $ACOPLIB
);

my $CompteurC1     = 0;    # INITIALISATION DÉCOMPTE COLLECTIVITÉS
my $cumulTotGLot   = 0;    # CUMUL GÉNÉRAL POUR CONTROL DU LOT
my $cumulTotAgtLot = 0;    # CUMUL GENERAL DES AGENTS POUR LE LOT
my %cumulParGarantie;
my @ordreDeGarantie;
my %Basedesgaranties
  ; #   elle sera remplie dans l'enregistrement LJ mais initialisée à vide dans L4 car chaque L4 a au moins 2 enregistrements LJ : 1 pour taux et 1 pour base
my %Tauxdesgaranties
  ; #   elle sera remplie et cumulée les taux dans l'enregistrement LJ mais initialisée à vide dans L4 car chaque L4 a au moins 2 enregistrements LJ : 1 pour taux et 1 pour base
my %CotPEmp
  ; # elle sera remplie et cumulée les part de cotisation de l'employeur dans l'enregistrement LJ
my %CodeProduit
  ; # elle sera remplie dans L4, c'est le lien entre la libelle de produit(tableau Ordredesgarantie) et la base et le montant par produit.
my $TotalGlobal   = 0;
my $TotalGlobalCB = 0;
my $TotalGlobalPB = 0;
my $TotalGlobalPC = 0;
my $TotalGlobalIJ = 0;
my $TotalGlobalL6 = 0;
my $TotalRappels  = 0;
my $TotalTPercus  = 0;
my $cptAdhL3      = 0;    #Re-initialisaton du compteur L3
my $flagF5        = 0;    # Initialisation flag groupe d'assurés
my $compteurL4Adh = 0;
my $matricule     = "";
my @DADDR;
my $NOMDEST;
my $IDCOLL;
my $CODGRAx;
my $ODGGCOD
  ; # code de groupe de gestion à la position 552 du C2. il conditionne l'affichage des lignes de cotisation globale
my $cde_ctr_coll;
my $toulouse;
my $SECTION;
my $payeur;
my @eaddr;
my %TAB_INTER
  ; # cette table haschage permet de sauvegarder la balise aCollec, NOMPRN, NUMADH et leur valeur au moment L3 et restituer dans L4 une fois le code de produit est connu, et
my %AdhCot
  ; # table haschage qui saugarde le matricule => la cotisation globale et la part adhérent séparé par deux points ':'. Cette table permet de réutiliser pour la part employeur. C'est à dire l'enregistrement F5 qui suit de même C1 et C2
my $NumAdhCot;
my $CTRL_DAT;
my $EcheanceAntEvol;
my $Restant    = 0;
my $TrpcAnt    = 0;
my $eL3_RAPPEL = 0;
my $eL3_TPERCU = 0;
my $CumulL6    = 0
  ; # Ce cumul permet de d'afficher le total des cotisations exceptionnelles en dessous de la ligne "Sous total"
my $CumulCB = 0;    #
my $CumulPB = 0
  ; # le cumul de garantie Prévoyance de base(PB) dans le cas de garantie PGM qui n'existe pas en gestion de prévoyance
my $CumulPC = 0
  ; # le cumul de garantie Prévoyance Complémentaire(PC) dans le cas de garantie PGM qui n'existe pas en gestion de prévoyance
my $CumulIJ = 0
  ; # le cumul de garantie Indemnité Journalière(IJ) dans le cas de garantie PGM qui n'existe pas en gestion de prévoyance
my $CumulLib     = "";
my $CumulLibPGMi = "";
my $CumulLibPGMc = "";
my $CumulLibPGMb = "";
my $TotalL4;
my $EcheanceAnterieur;
my $NomPreAdh;
my $Payeur;
my $Nouveaubord = 1
  ; # un flag de nouveau bordereau et si nouveau bordereau, il faut initiatiser le tableau %AdhCot qui sauvegarde les cotisations des adhérants
my $cptPLM            = 1;
my $flagLabel         = 0;    #  le flag d une presence label
my $adherentsEnErreur = 0;
my %RIB;

######################################################################################################################################
# cinématique suivante :
#o	C1 : Entête 1, standard collectif, pour chaque collectivité
#o	C2 : Entête 2, standard collectif, pour chaque C1
#o	E3 : Entête 3, pour chaque C1
#o	F5 : Entête de l'édition des groupes d'assurés, de 1 à n pour chaque C1
#o	L3 : informations concernant le Contrat Individuel, de 1 à n pour chaque F5
#o	L4 - informations sur les éléments de prime, de 0 à n par L3
#o	L5 - détail des frais du contrat collectif, de 0 à n par L3
#o	L6 - informations sur les cotisations particulières, de 0 à n par L3
#	LJ - Informations sur les TAUX et BASE de cotisation, de 2 à n par L4
#o	L7 - informations sur les arrondis du contrat individuel, de 0 à n par L3
#o	T3 - informations sur la part du groupe d'assurés, de 1 par F5
#o	T4 - total des cotisations par garantie technique, de 1 à n par F5
#o	T5 - total des frais sur la part, de 1 par F5
#o	T6 - total des cotisations particulières, de 1 par F5
#o	T7 - total des arrondis, de 1 par F5
#####################################################################################################################################
# Date : 25/04/2012  V2
# Projet : PREVOYANCE CIBLE /LOT 1.2 suite au sesame 950511
# Nom Application : PRPS-AC016
# Flux entrée : AC016
# En sorite : AVIS D'APPEL DE COTISATIONS PREVOYANCE INDIVIDUELLE
#Description de fonctionnement :
#
# Changement de l'adresse expediteur des Centre de Traitement des Cotisations (CTC) à la place de l'adresse des sections
# lister les montants par ligne de produit et ajouter les sous totaux
# Ajouter les colonnes taux et base grace à l"enregistrement LJ
#####################################################################################################################################
# Evolution Date : 08/11/2012
# Demande sésame MCO : 985749 labellisation de lot 1
# Description : Ajout de la labellisation sur le l'entete selon le code de produit du premier adhérent
#  et le masquage des matricules temporaires
#####################################################################################################################################
# Evolution Date : 28/12/2012
# Demande sésame MCO : 994037 labellisation de lot 2
# Description : Ajout des messages de cotisation globale et les parts adhérent et employeur pour chaque ligne du tableau adhérent
#			 Cette gestion est dans le cadre du GROUPE DE GESTION = M2-PCL-T2 sinon on garde le même principe de gestion au précédent avec les lignes "sous total"
#			s'il n'y a pas de part adhérent alors part ahérent = 0 et part employeur = cotisation globale
#			par contre, la part employeur doit toujours exister avec la ligne LJ (PEMPCO))
#####################################################################################################################################
# Evolution Date : 03/04/2013
# Demande sésame Interne DSI: 1010018  (projet piloté par l'équipe "Développement spécifique" DS)
# Description : L'équipe DS ajoute deux enregistrements OD, OE dans le flux qui apporte des informations sur l'adresse de CTC et de relevé indentité bancaire (RIB)
#			L'état éditique est évolué pour prendre en compte l'adresse CTC à partir de cette enregistrement OD et non pas de son fichier paramètre.
#			et aussi pour afficher le RIB à la fin de de bordereau
#####################################################################################################################################
# Evolution Date : 17/07/2013
# Demande sésame lotissement Interne DSI: 1034498
# Description :  Réduction de l'affichage des références bancaires et l'adresse CTC, les regrouper horizontalement au même niveau
#####################################################################################################################################
# Evolution :
# date : 23/09/2013
# Description : masquage de la colonne de BASE et TAUX avec sesame 1039944; il convient que la gestion de gestion reste identique, la modification est essentiellement sur LATEX pour le masquage et pour le
#               remplacement de l'adresse émetteur actuellement l'adresse CTC par l'adresse des sections sésame 1040199
#
#####################################################################################################################################
# Evolution :
# date : 12/06/2014
# Sésame : 1085340
# Description : Mise en place du CODRUPT pour les collectivités Toulous Mairie et Toulouse Métropole. Ces document ne doivent pas être envoyés
#               au façonnier
#####################################################################################################################################

##################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
###################################################################################

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' )
      ; # fonction retour les entités juridiques pour Latex: xMNT, FondCPL, MUTACI etc ...T
    my $user = user_get_aneto_user( $argv[0] );

    # warn "DEBUG: Premier Argument  $argv[0] \n";
    # warn "DEBUG: Deuxième Argument  $argv[0] \n";
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDCOLL', 'SECTION', 'NOMDEST', 'PART', 'Tpercu' ]
    );

    # trame keys => ['IDDEST', 'SECTION', 'NOMDEST', 'COMPTE', 'INDUS']
    my $suffixe = oe_corporation_set;
    oe_new_job('--index');

    # OUVERTURE DU FICHIER DE CONTROLE
    my $ctrl_idx2 = basename($0);
    $ctrl_idx2 =~ s/\.pl$/.idx2/i;
    open( my $CTRL, '>', $ctrl_idx2 ) or die "ERROR: $!\n";

    emitDoc();

    # INITIALISATION PROPRE AU DOCUMENT
    initDoc();
    $DOC->append( 'xTYPDOC', 'ACOT' );    ### Type de document

    # Initialisation des records.
    my $C1 = oEUser::Descriptor::C1::get();

    #$C1->debug();
    $C1->bind_all_c7();
    my $C2 = oEUser::Descriptor::C2::get();

    #	$C2->debug();
    $C2->bind_all_c7();
    my $E3 = oEUser::Descriptor::E3::get();
    $E3->bind_all_c7();
    my $OD = oEUser::Descriptor::OD::get();

    #	$OD->debug();
    $OD->bind_all_c7();
    my $OE = oEUser::Descriptor::OE::get();

    #	$OE->debug();
    $OE->bind_all_c7();
    my $F5 = oEUser::Descriptor::F5::get();

    #	$F5->debug();
    $F5->bind_all_c7();
    my $L3 = oEUser::Descriptor::L3::get();

    #	$L3->debug();
    $L3->bind_all_c7();
    my $L4 = oEUser::Descriptor::L4::get();

    #	$L4->debug();
    $L4->bind_all_c7();
    my $L5 = oEUser::Descriptor::L5::get();
    $L5->bind_all_c7();
    my $L6 = oEUser::Descriptor::L6::get();

    #$L6->debug();
    $L6->bind_all_c7();

# 	AJOUTE pour apporter le taux et la base selon le code assiette = BASECO ou TAUXCO ou PEMPCO
    my $LJ = oEUser::Descriptor::LJ::get();

    # 	$LJ->debug();
    $LJ->bind_all_c7();
    my $L7 = oEUser::Descriptor::L7::get();
    $L7->bind_all_c7();
    my $T3 = oEUser::Descriptor::T3::get();
    $T3->bind_all_c7();
    my $T4 = oEUser::Descriptor::T4::get();
    $T4->bind_all_c7();
    my $T5 = oEUser::Descriptor::T5::get();
    $T5->bind_all_c7();
    my $T6 = oEUser::Descriptor::T6::get();
    $T6->bind_all_c7();
    my $T7 = oEUser::Descriptor::T7::get();
    $T7->bind_all_c7();
    my $ENTETE = oEUser::Descriptor::ENTETE::get();
    $ENTETE->bind_all_c7();

 #	my $p = oEdtk::RecordParser->new(\*IN, 'C1' => $C1, 'C2' => $C2, 'E3' => $E3,
 #	    'F5' => $F5, 'ENTETE' => $ENTETE, 'L3' => $L3, 'L4' => $L4, 'L5' => $L5,
 #	    'L6' => $L6, 'L7' => $L7, 'T3' => $T3, 'T4' => $T4, 'T5' => $T5,
 #	    'T6' => $T6, 'T7' => $T7);
 # PREVISION
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'C1'     => $C1,
        'C2'     => $C2,
        'E3'     => $E3,
        'F5'     => $F5,
        'ENTETE' => $ENTETE,
        'L3'     => $L3,
        'L4'     => $L4,
        'L5'     => $L5,
        'L6'     => $L6,
        'L7'     => $L7,
        'LJ'     => $LJ,
        'T3'     => $T3,
        'T4'     => $T4,
        'T5'     => $T5,
        'T6'     => $T6,
        'T7'     => $T7,
        'OD'     => $OD,
        'OE'     => $OE
    );

    my $first = 1;
    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'ENTETE' ) {
            prepENT($vals);
        }
        elsif ( $id eq 'C1' ) {
            $Nouveaubord = 1
              ; # permettant d'identifier le nouveau bordereau qd on rencontre un C1 pour initialiser le tableau %AdhCot dans L3

            #			if (!$first) {
            #				emitDoc();
            #			} else {
            #				$first = 0;
            #			}
            initDoc();
            $GED = oEdtk::TexDoc->new();
            $GED->append( 'xCLEGEDii', $DATMPREV );

# LA RÉNITIALISATION DES ADRESSES NE DOIT SE FAIRE QU'À CHAQUE NOUVEAU C1
# pas dans initdoc car il peut y avoir 1 adresse pour plusieurs F5 (qui font un initdoc)
            @DADDR = ();

            $CODGRAx = $vals->{'CODGRAx'};
            $NOMDEST = $vals->{'PENOLIB'};
            $NOMDEST =~ s/\s+$//;
            $NOMDEST =~ s/\;/:/g;
            $DOC->append( 'MPRELEV', $DATMPREV );
            prepC1($vals);

            $GED->append( 'xNOMDEST', $NOMDEST );
            $DOC->append( 'COLL',     $NOMDEST );

        }
        elsif ( $id eq 'C2' ) {

            #			warn "INFO :C2 dump ". Dumper($vals) ."\n";
            if ( ( $vals->{'ACCCNUM'} =~ m/PCI/ ) && ( $suffixe =~ m/MNT/ ) ) {
                $DOC->append( 'xCODRUPT', 'PCI' );
            }
            $IDCOLL  = $vals->{'ACCCNUM'};
            $ODGGCOD = $vals->{'ODGGCOD'};
            my $cde_ctr_coll = $vals->{'ACCCNUM'} . " " . $CODGRAx;
            $toulouse = $cde_ctr_coll;

            $GED->append( 'xIDDEST', $cde_ctr_coll )
              ;    # ajout du code assuré au n° contrat
            $DOC->append( 'IDCOLL', $IDCOLL );
        }
        elsif ( $id eq 'F5' ) {

            #			warn "INFO : F5 dump ". Dumper($vals) ."\n";
            prepF5($vals);
            $DOC->append( 'CODGRA', $vals->{'CODGRAx'} );
            $DOC->append( 'PAYEUR', $vals->{'LIBLONC'} );

            my ( $jour, $mois, $annee ) = split( /\//, $vals->{'DEBPERx'} );
            $DOC->append( 'ECHEANCE',   "$mois/$annee" );
            $GED->append( 'xCLEGEDiii', "$mois/$annee" );
        }
        elsif ( $id eq 'L3' ) {

# je suis dans L3 suivant, je vérifie si je suis dans le premier ahérent et si je n'ai pas de garantie maintien de salaire alors c'est une erreur sinon je réinitialise à 0
            if ( ( $cptPLM == 0 ) and ( $cptAdhL3 == 0 ) ) {
                die
"ERROR : Absent de produit Maintien Salaire pour ahérent : $NomPreAdh \n  ";
            }
            else {
                $cptPLM = 0;
            }
            prepL3($vals);
        }
        elsif ( $id eq 'L4' ) {

            # warn "INFO : L4 dump ". Dumper($vals) ."\n";
            prepL4($vals);
        }
        elsif ( $id eq 'L6' ) {
            $CumulL6 += $vals->{'MONT003'};
        }
        elsif ( $id eq 'LJ' ) {

            # 			warn "INFO : L4 dump ". Dumper($vals) ."\n";
            prepLJ($vals);
        }
        elsif ( $id eq 'OD' ) {
            prepOD($vals);
        }
        elsif ( $id eq 'OE' ) {
            prepOE($vals);
        }

    }
    finCollectivite();

    #	emitDoc();

    if ( $cumulTotGLot == 0 && $cumulTotAgtLot == 0 ) {
        exit 99;
    }
    oe_compo_link();
    close($CTRL);
    return 0;
}

sub format_num {
    my $num = shift;

    $num = sprintf( "%.2f", $num );
    $num !~ s/\./,/;
    while ( $num =~ s/(\d+)(\d{3})/$1 $2/ ) { ; }
    return $num;
}

sub control_out {
    my ($context) = shift;

    my @vals = ( $CTRL_DAT, $SECTION, "$IDCOLL`$CODGRAx", $CODPARx, $NOMDEST );
    if ( $context eq 'COLL' ) {
        $vals[0] .= "`TOTAL COL.";
        push( @vals,
            "TOTAL COL.", "TOTAL COL.", $cptAdhL3, format_num($TotalGlobal) );
    }
    else {
        $matricule =~ s/^\s+//;
        $matricule =~ s/ +/ /g;
        $matricule =~ s/\.//g;
        push( @vals, $ACH1NUM, $NOMIDX, $matricule, format_num($TotalL4) );
    }

    my $line = sprintf(
"<50>ac016 :`%-20s<51>%-8s<5U>%-28s<5V>%-4s<5W>%-60s<5X>%-12s<5K>%-44s<5L>%-15s<5M>%-12s<52>Index Controles <53>  2     1  00   ",
        @vals );
    print CTRL "$line\n";
}

sub initDoc {
    if ( $CompteurC1 > 0 ) {
        finCollectivite();
    }
    $compteurL4Adh = 0;
    $TotalGlobal   = 0;
    $TotalGlobalCB = 0;
    $TotalGlobalPB = 0;
    $TotalGlobalPC = 0;
    $TotalGlobalIJ = 0;
    $TotalGlobalL6 = 0;
    $TotalRappels  = 0;
    $TotalTPercus  = 0;
    $cptAdhL3      = 0;
    $cptPLM        = 1;
    $NOMDEST       = "";
    initAdh();

    $flagF5 = 0;    # Initialisation flag groupe d'assurés
}

sub emitDoc {
    oe_print_to_output_file($GED);

    if ( $adherentsEnErreur != $cptAdhL3 ) {
        $DOC_parent->append( 'document', $DOC );
        $DOC_parent->append('editDoc');
    }
    oe_print_to_output_file($DOC_parent);
    $DOC               = oEdtk::TexDoc->new();
    $DOC_parent        = oEdtk::TexDoc->new();
    $adherentsEnErreur = 0;
}

sub initAdh {
    $matricule     = "";
    $Restant       = 0;
    $TrpcAnt       = 0;
    $eL3_RAPPEL    = 0;
    $eL3_TPERCU    = 0;
    $compteurL4Adh = 0;
    $CumulL6       = 0;
    $CumulCB       = 0;
    $CumulPB       = 0;
    $CumulPC       = 0;
    $CumulIJ       = 0;
    $CumulLib      = "";
    $CumulLibPGMi  = "";
    $CumulLibPGMc  = "";
    $CumulLibPGMb  = "";
    $TotalL4       = 0;
    $ACH1NUM       = "";

    undef %cumulParGarantie;
    undef @ordreDeGarantie;     # réinitialiser
    undef %Basedesgaranties;    # réinitialiser
    undef %Tauxdesgaranties;    # réinitialiser
    undef %CotPEmp;
    undef %CodeProduit;         # réinitialiser
    undef @eaddr;
    undef %TAB_INTER;

}

sub finCollectivite {

    # CLOTURE DU DERNIER ADHÉRENT
    trtTotAdh();

    my @libelles = ();
    my @montants = ();

    # VENTILATION DES SOUS TOTAUX PAR FAMILLE
    #	if ($TotalGlobalCB != 0) {
    #		push(@libelles, "Montant médico chirurgical :");
    #		push(@montants, sprintf("%0.2f", $TotalGlobalCB));
    #	}
    #	if ($TotalGlobalPB != 0) {
    #		push(@libelles, "Montant prévoyance de base :");
    #		push(@montants, sprintf("%0.2f", $TotalGlobalPB));
    #	}
    #	if ($TotalGlobalPC != 0) {
    #		push(@libelles, "Montant prévoyance complémentaire :");
    #		push(@montants, sprintf("%0.2f", $TotalGlobalPC));
    #	}
    #	if ($TotalGlobalIJ != 0) {
    #		push(@libelles, "Montant indemnité journalière :");
    #		push(@montants, sprintf("%0.2f", $TotalGlobalIJ));
    #	}
    # affiche le total des sous totaux pour l'échéance.
    my $TotalEcheance =
      $TotalGlobalCB +
      $TotalGlobalPB +
      $TotalGlobalPC +
      $TotalGlobalIJ +
      $TotalGlobalL6;
    if ( $TotalEcheance != 0 ) {
        push( @libelles, "Total pour échéance :" );
        push( @montants, sprintf( "%0.2f", $TotalEcheance ) );
    }

    if ( $TotalGlobalL6 != 0 ) {
        push( @libelles, "Montant cotisation exceptionnelle :" );
        push( @montants, sprintf( "%0.2f", $TotalGlobalL6 ) );
    }
    if ( $TotalRappels != 0 ) {
        push( @libelles, "Montant Total Rappel :" );
        push( @montants, sprintf( "%0.2f", $TotalRappels ) );
    }
    if ( $TotalTPercus != 0 ) {
        push( @libelles, "Montant Total trop perçu :" );
        push( @montants, sprintf( "%0.2f", $TotalTPercus ) );

    }

    # CONTROLE DES CUMULS CALCULÉS ET DES TOTAUX TRANSMIS
    my $sommeCumul =
      $TotalGlobalCB +
      $TotalGlobalPB +
      $TotalGlobalPC +
      $TotalGlobalIJ +
      $TotalGlobalL6 +
      $TotalRappels +
      $TotalTPercus;

#my $debug = sprintf "<Debug>CTRL (%0.2f/%0.2f)<Null>",$sommeCumul,$TotalGlobal;
#print OUT $debug if ($sommeCumul ne $TotalGlobal);

    # EDITION SI ON A BIEN TRAITÉ AU MOINS UN ADHÉRENT POUR LA COLLECTIVITÉ
    if ( $cptAdhL3 != 0 ) {    # GÉRER ICI LES COLLECTIVITÉS À ZÉRO ?

        #		$DOC->append('TotTpercu', 	sprintf("%0.2f", $TotalTPercus));
        $DOC->append('fermetureLT');
        $DOC->append( 'TotGen',    sprintf( "%0.2f", $TotalGlobal ) );
        $GED->append( 'xCLEGEDiv', sprintf( "%0.2f", $TotalGlobal ) );
        $DOC->append( 'TotAgt',    $cptAdhL3 );
        $DOC->append_table( 'LIBELLES', @libelles );
        $DOC->append_table( 'MONTANTS', @montants );
        if ( $flagLabel eq 1 ) {
            $DOC->append( 'Mesg',
'Ces garanties labellisées sont éligibles à la participation employeur'
            );
            $flagLabel = 0;
        }
        else {
            $DOC->append( 'Mesg', '' );
        }
        $DOC->append( 'BANQUE',        $RIB{'BANQUE'} );
        $DOC->append( 'GUICHET',       $RIB{'GUICHET'} );
        $DOC->append( 'COMPTE',        $RIB{'COMPTE'} );
        $DOC->append( 'CLE',           $RIB{'CLE'} );
        $DOC->append( 'DEVISE',        $RIB{'DEVISE'} );
        $DOC->append( 'DOMICILIATION', $RIB{'DOMICILIATION'} );
        $DOC->append( 'IBAN',          $RIB{'IBAN'} );
        $DOC->append( 'BIC',           $RIB{'BIC'} );
        $DOC->append('fCollec');
        control_out('COLL');
        emitDoc();
    }
    $TRK->track( 'Doc', 1, $IDCOLL, $SECTION, $NOMDEST, $PART, $TotalTPercus );
    $PART = '';

    $cumulTotGLot   += $TotalGlobal;    # CUMUL GÉNÉRAL POUR CONTROL DU LOT
    $cumulTotAgtLot += $cptAdhL3;       # CUMUL GENERAL DES AGENTS POUR LE LOT
    return 1;
}

sub trtTotAdh {

    #  my $CumulInter=0;
    # traitement de fin d'enregistrement pour un adhérent.
    my ( $cle, $val, $element, $CumulCotEmp, $CotisGlob, $PartAgent );
    if ( $cptAdhL3 == 0 ) {

    # ON NE FAIT PAS DE TOTAL ADHERENT SI ON A PAS ENCORE TRAITE D'ADHERENT
    # C'EST A DIRE A CHAQUE FOIS QUE L'ON FAIT UN NOUVEAU GROUPE DE COLLECTIVITE
        return 1;
    }
    my $indice = @ordreDeGarantie;
    if ( $indice == 0 ) {
        die "ERROR: Adhérent $NomPreAdh ; Enregistrement L4 manquant  \n";
    }

    for ( $element = 0 ; $element <= $#ordreDeGarantie ; $element++ )
    {    # boucle de calcul de montant
        $cle = $ordreDeGarantie[$element];
        $val = $cumulParGarantie{$cle};

        #	 	warn"montant garantie= $val    \n";
        $TotalL4 += $val;
        if ( index( $cle, 'IJ' ) != -1 ) {
            $CumulLibPGMi .= "$cle ";
            $CumulIJ += $val;
        }
        elsif ( index( $cle, 'PC' ) != -1 ) {

            $CumulLibPGMc .= "$cle ";
            $CumulPC += $val;
        }
        elsif ( index( $cle, 'PB' ) != -1 ) {
            $CumulLibPGMb .= "$cle ";
            $CumulPB += $val;
        }
        else {
            $CumulLib .= "$cle ";
            $CumulCB += $val;    # colonne affiché dans : Montant par garantie
        }
        $DOC->append( 'MATRICULE', $matricule );
        if ( $element != 0 ) {
            $DOC->append( 'MATRICULE', '' );
            $DOC->append( 'NOMPRN',    '' );
            $DOC->append( 'NUMADH',    '' );

            #	   $DOC->append('RAPPEL',	sprintf("%0.2f", 0));
            #  	 $DOC->append('TPERCU',	sprintf("%0.2f", 0));
        }

        #   else {
        #	   $DOC->append('RAPPEL',	sprintf("%0.2f", $eL3_RAPPEL));
        #  	 $DOC->append('TPERCU',	sprintf("%0.2f", $eL3_TPERCU));
        #  	 $DOC->append('TPERCU',	sprintf("%0.2f", 0));
        #   }
        #  $DOC->append('RAPPEL',	sprintf("%0.2f", 0));
        #  $DOC->append('TPERCU',	sprintf("%0.2f", 0));

        $DOC->append( 'RAPPEL',    '' );
        $DOC->append( 'TPERCU',    '' );
        $DOC->append( 'GARANTIES', $cle );
        if ( exists $Basedesgaranties{ $CodeProduit{$cle} } ) {

#		$DOC->append('BASE',sprintf("%0.2f", $Basedesgaranties{$CodeProduit{$cle}}));
# la sésame 1039944 nous demande de masquer la colonne BASE, on envoie BLANC à LATEX
            $DOC->append( 'BASE', ' ' );
        }
        else {
            die
"ERROR: Adhérent $NomPreAdh : Enregistrement LJ de base manquant  \n";
        }
        if ( exists( $Tauxdesgaranties{ $CodeProduit{$cle} } ) ) {

#		$DOC->append('TAUX',sprintf("%0.2f", $Tauxdesgaranties{$CodeProduit{$cle}}));
# la sésame 1039944 nous demande de masquer la colonne TAUX, on envoie BLANC à LATEX
            $DOC->append( 'TAUX', ' ' );
        }
        else {
            die
"ERROR: Adhérent $NomPreAdh: Enregistrement LJ de taux manquant  \n \n";
        }

        if ( $ODGGCOD eq 'M2-PCL-T2' ) {
            if ( exists( $CotPEmp{ $CodeProduit{$cle} } ) )
            {    # s'il existe PEMPCO alors je cumule la cotisation des produits
                $CumulCotEmp += $CotPEmp{ $CodeProduit{$cle} };
            }
            else {
                die
"ERROR: Adhérent $NomPreAdh: Enregistrement LJ de Part employeur manquant  \n \n";

#			warn "ERROR: Adhérent $NomPreAdh: Enregistrement LJ de Part employeur manquant  \n \n";
                $adherentsEnErreur++;
                initAdh();
                return (1);
            }
        }

        $DOC->append( 'MONTGAR', sprintf( "%0.2f", $val ) );

        #	$DOC->append('RAPPEL',	'NC');
        #	$DOC->append('TPERCU',	'NC');
        #	$DOC->append('TOTAL',	sprintf("%0.2f", $val));
        $DOC->append( 'TOTAL', '' );

        #	$CumulInter=0;
        $DOC->append( 'EchAnt', ' ' );
        $DOC->append('FinAdhA');

    }    # fin de boucle for pour chaque produit

# ici j'ecris la ligne des cotisations globales et la part des cotisations ahérent/employeur
#	$DOC->append('NOMPRN','');
#	$DOC->append('NUMADH','');
#	$DOC->append('MATRICULE','');
    if ( $ODGGCOD eq 'M2-PCL-T2' ) {
        if ( $Payeur eq 'Employeur' ) {
            if ( exists( $AdhCot{$NumAdhCot} ) ) {
                ( $CotisGlob, $PartAgent ) = split( /:/, $AdhCot{$NumAdhCot} );
                $DOC->append( 'COTGLOB',  "$CotisGlob" );
                $DOC->append( 'PARTICIP', "$PartAgent" );
            }
            else {
                $DOC->append( 'COTGLOB', "$CumulCB" );
                $DOC->append( 'PARTICIP', sprintf( "%0.2f", 0 ) );
            }
            $DOC->append('LigEmpl');

        }
        else
        { # je suis dans le bloc adhérent => alors je cumule les part employeur, cotisation globale, j'ecris les lignes adhérents
            my $CumulCot = $CumulCotEmp + $CumulCB;
            $DOC->append( 'COTGLOB',  "$CumulCot" );
            $DOC->append( 'PARTICIP', "$CumulCotEmp" );
            $DOC->append('LigAdh');

# je sauvegarde pour la prochaine F5 part Employeur. Chaque numero adhérent je sauvegarde la cotisation globale et la part employeur
            $CumulCotEmp += $CumulCB;
            $AdhCot{$NumAdhCot} = "$CumulCotEmp:$CumulCB";
        }

    }

    # ici j'écris la ligne des sous totaux ou de participation
    # CALCUL DU CUMUL PAR ADHERENT HORS TROP PERCU
    $TotalL4 += $eL3_RAPPEL + $CumulL6;

# UN TROP PERCU NE DOIS JAMAIS ÊTRE PLUS GRAND EN VALEUR ABSOLUE QUE LE MONTANT PAR GARANTIE
#	warn"$eL3_TPERCU   $TotalL4   \n";
    if ( ( abs $eL3_TPERCU ) > $TotalL4 ) {
        $eL3_TPERCU = ($TotalL4) * (-1);
    }

    #	warn"resultat = $eL3_TPERCU      \n";

    # AFFECTATION DU TROP PAYE AU CUMUL PAR ADHERENT
    $TotalL4 += $eL3_TPERCU;

# UNE ECHEANCE NE PEUT PAS ËTRE NEGATIVE : BALANCE DE L'ÉCHÉANCE, RAZ CALCUL TOTAUX ADH
    if ( $TotalL4 < 0 ) {

        #print OUT "<Debug>CTRL (calcul totaux Adh)<Null>";
        $TotalL4 = 0;
        $CumulPC = 0;
        $CumulPB = 0;
        $CumulCB = 0;
        $CumulIJ = 0;
        $CumulL6 = 0;
    }

    # CONTROLE DE L'ECHEANCE PAR RAPPORT A L'ECHEANCE ANTERIEURE DE L'ADHERENT
    if ( $EcheanceAnterieur == 0 ) {
        $EcheanceAntEvol = 'NV';
    }
    elsif ( $TotalL4 ne $EcheanceAntEvol ) {
        $EcheanceAntEvol = 'DIFF';
    }
    else {
        $EcheanceAntEvol = {'tAdh'};
    }

    my $montGar = $CumulCB;

    # ÉDITION DES SOUS TOTAUX
    $DOC->append( 'NOMPRN',    '' );
    $DOC->append( 'NUMADH',    '' );
    $DOC->append( 'MATRICULE', '' );
    $DOC->append( 'GARANTIES', '' );
    $DOC->append( 'BASE',      '' );

#	$DOC->append('TAUX','Sous total1');   # sur la ligne sous total, j'écris "sous total dans la colonne TAUX, "
    $DOC->append( 'RAPPEL', $eL3_RAPPEL );

    #	warn "AFF TPERCU=$eL3_TPERCU      \n";
    $DOC->append( 'TPERCU', sprintf( "%0.2f", $eL3_TPERCU ) );
    $DOC->append( 'TOTAL',  sprintf( "%0.2f", $TotalL4 ) );
    $DOC->append( 'EchAnt', $EcheanceAntEvol );

    #$DOC->append('FinAdhA');

    my @rows = ();
    if ($CumulIJ)
    { # cette gestion est dans la cadre de la santé PGM , il n'est plus nécessaire pour la PREVOYANCE
        my $rdoc = oEdtk::TexDoc->new();
        $rdoc->append( 'CUMULIJ', $CumulIJ );
        $montGar += sprintf( "%0.2f", $CumulIJ );
    }
    if ($CumulPC)
    { # cette gestion est dans la cadre de la santé PGM , il n'est plus nécessaire pour la PREVOYANCE
        my $rdoc = oEdtk::TexDoc->new();
        $montGar += sprintf( "%0.2f", $CumulPC );
    }
    if ($CumulPB)
    { # cette gestion est dans la cadre de la santé PGM , il n'est plus nécessaire pour la PREVOYANCE
        my $rdoc = oEdtk::TexDoc->new();
        $montGar += sprintf( "%0.2f", $CumulLibPGMb );
    }
    if ($CumulL6) {
        my $rdoc = oEdtk::TexDoc->new();
        $montGar += sprintf( "%0.2f", $CumulL6 );
    }
    $DOC->append( 'MONTGAR', $montGar );    # Édite la somme des garanties
    if ( $ODGGCOD eq 'M2-PCL-T2' )
    { # je suis dans une gestion de label et j'appelle SSTotLabel pour afficher les participations
        if ( $Payeur eq 'Employeur' ) {
            $DOC->append( 'TAUX', 'Participation employeur' );
        }
        else {
            $DOC->append( 'TAUX', 'Payé par l\'agent' );
        }
        $DOC->append('SSTotLabel');
    }
    else {    # je suis dans une gestion normale
        $DOC->append( 'TAUX', 'Sous total' );
        if ( @rows > 0 ) {

            #		$DOC->append('TAUX','Sous total');
            $DOC->append('FinAdhATot');
        }
        else {
            #		$DOC->append('FinAdhA');
            $DOC->append('FinAdhSSTot');
        }

    }

# print OUT sprintf "<#TotalL4=<SET>%0.2f><#EchAnt=%s><FinAdh>\n",$TotalL4,$EcheanceAntEvol;
    control_out('ADH');

    # SOMME DES CUMUL POUR LA COLLECTIVITE
    $TotalGlobal +=
      $TotalL4;    #Somme totale pour une collectivité (Total général)
    $TotalGlobalPC += $CumulPC;
    $TotalGlobalPB += $CumulPB;
    $TotalGlobalCB += $CumulCB;
    $TotalGlobalIJ += $CumulIJ;
    $TotalGlobalL6 += $CumulL6;
    $TotalRappels  += $eL3_RAPPEL;
    $TotalTPercus  += $eL3_TPERCU;

    initAdh();
}    #fin de trtTotAdh

sub prepOD {
    my ($vals) = @_;
    my @adr_CTC;

    #		warn "INFO OD: dump ". Dumper($vals) ."\n";

    if ( $vals->{'LIGNExx'} ne '' ) { push( @adr_CTC, $vals->{'LIGNExx'} ); }
    if ( $vals->{'LIGN003'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN003'} );
    }
    if ( $vals->{'LIGN004'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN004'} );
    }
    if ( $vals->{'LIGN005'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN005'} );
    }
    if ( $vals->{'LIGN006'} ne '' ) {    #$adr_CTC[4] = 'NULL';}
        push( @adr_CTC, $vals->{'LIGN006'} );
    }
    $vals->{'CODEPOS'} =~ s/^\s+//;
    $vals->{'CODEPOS'} =~ s/\s+$//;
    $vals->{'VILLExx'} =~ s/^\s+//;
    $vals->{'VILLExx'} =~ s/\s+$//;
    my $CP = $vals->{'CODEPOS'} . ' ' . $vals->{'VILLExx'};
    if ( $CP ne '' ) {
        push( @adr_CTC, $CP );
    }
    $vals->{'PAYSxxx'} =~ s/^\s+//;
    if ( $vals->{'PAYSxxx'} ne '' ) {
        push( @adr_CTC, $vals->{'PAYSxxx'} );
    }

    # 		if (join (' ', @adr_CTC)=~/\s+33\d{3}\s+/){
    # 			#warn "DEBUG: mise en place code rupture CTC33 \n";
    # 			$DOC->append('xCODRUPT', 'CTC33');
    # 		}
    $DOC->append( 'retours',
        "Un exemplaire de ce document est à renvoyer à l'adresse suivante :"
    );

# l'adresse emetteur est remplace par l'adresse des sections dans la fonction gestionAdrC1()
#		$DOC->append_table('xADREM', @adr_CTC);
    $DOC->append_table( 'AdrCTC', @adr_CTC );

}

sub prepOE {
    my ($vals) = @_;

    #		warn "INFO OE: dump ". Dumper($vals) ."\n";
    $RIB{'BANQUE'}        = $vals->{'BANQUEx'};
    $RIB{'GUICHET'}       = $vals->{'GUICHET'};
    $RIB{'COMPTE'}        = $vals->{'COMPTEx'};
    $RIB{'CLE'}           = $vals->{'CLExxxx'};
    $RIB{'DEVISE'}        = $vals->{'DEVISEx'};
    $RIB{'DOMICILIATION'} = $vals->{'Domicil'};
    $RIB{'IBAN'}          = $vals->{'IBANxxx'};
    $RIB{'BIC'}           = $vals->{'BICxxxx'};

}

sub prepC1 {
    my ($vals) = @_;

    # APPEL EXIT EVENTUELS POUR REALISER DES TRAITEMENTS SUR CERTAINS CHAMPS
    gestionAdrC1($vals);
    $SECTION = $vals->{'ODERCOD'};

# GESTION EXCEPTIONNELLE DES NOUVELLES SECTIONS SL ÉCARTÉES POUR AJOUT D'ENCART 10/10/2011 18:11:22
#	if ($SECTION =~/D008|D031|D033|D037|D044|D066|D069|D973|P005/){
#		$DOC->append('xCODRUPT', 'B');
#		$DOC->append('xREFIMP', 'LETTREBSL')
#	};

    $GED->append( 'xIDEMET',  $vals->{'ODERCOD'} );
    $GED->append( 'xVILDEST', $vals->{'NOMCDXL'} );

    # la ligne suivante initialise la gestion du tri par pays
    $GED->append( 'xCPDEST', oe_iso_country() . $vals->{'CODCDXC'} );

    #	my @adr_CTC = user_get_adr_ret('PREVOYANCE',$SECTION);
    # warn "DEBUG: $SECTION => @adr_CTC \n";
    #	if (join (' ', @adr_CTC)=~/\s+33\d{3}\s+/){
    #warn "DEBUG: mise en place code rupture CTC33 \n";
    #		$DOC->append('xCODRUPT', 'CTC33');
    #	}

#	if ($adr_CTC[0] eq 'NULL' || oe_corporation_set() ne 'MNT' ) { #
#	if ($adr_CTC[0] eq 'NULL'|| oe_corporation_set() eq 'MUTACITE') { #
#		$DOC->append('retours', "Un exemple de ce document est à renvoyer à la section après validation.");
#		$adr_CTC[0] = "~";
#		$DOC->append_table('xADREM', @eaddr);
# Fin ajout
#		$DOC->append_table('AdrCTC', @eaddr);

#	} else {
#		@adr_CTC = ("MUTUELLE NATIONALE TERRITORIALE",@adr_CTC);
#		$DOC->append('retours', "Un exemplaire de ce document est à renvoyer à l'adresse suivante :");
# ajout par ANN
#		$DOC->append_table('xADREM', @adr_CTC);
# Fin ajout
#		$DOC->append_table('AdrCTC', @adr_CTC);
#	}

#	#SI VOUS AFFECTEZ UNE SECTION A UN CENTRE DE PAIEMENT SUR AC016 REPERCUTEZ CETTE INFO SUR AC014
#	if ($SECTION =~ /C051|C063|D001|D004|I004|D005|D006|D007|I007|D009|D011|D012|D013|D015|D016|D017|D019|I02A|I02B|D023|D024|D026|D030|D031|D032|D033|D034|D038|D039|D040|D046|D047|I048|D063|D064|D065|D066|D069|D071|D079|I081|D082|D083|D084|I086|D087|P003|P005|P006/){
#		$DOC->append('TextFin', { 'TextFinC' });
#	} elsif ($SECTION =~ /D002|D003|D008|D010|C014|D014|D018|D021|D022|D025|D027|D028|D029|D035|D036|D037|D041|I042|D043|D044|I045|D045|D049|I049|K050|I050|D051|D052|D053|D054|D055|D056|D057|I057|D058|D059|I060|D060|I061|D061|D062|I067|I068|D070|I070|D072|D073|I074|D074|D075|D076|I077|D078|D080|D085|D088|D089|D090|I090|D091|I092|D093|I094|D095|P001|P002|P004/){
#		$DOC->append('TextFin', { 'TextFinB' });
#	} elsif ((oe_corporation_set() eq "MUT")){
#		$DOC->append('TextFin', { 'TextFinA' });
#	} else {
#		$DOC->append('TextFin', { 'TextFinA' });
#	}

    $CompteurC1++;
}

sub gestionAdrC1 {
    my ($vals) = @_;

    # APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR C1) :
    # SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS C1_NOMCDXLIBACH_DD

    # cas 1 : INDCDX =1  + LIBLOCLIBLOC !C Y	: BOIPOSLIBLDT + b + LIBLOCLIBLOC
    # cas 2 : INDCDX =1  + LIBLOCLIBLOC C  Y	: BOIPOSLIBLDT
    # cas 3 : INDCDX =0 					: BOIPOSLIBLDT

    my $Y = '';

# TRAITEMENT DE L'ADRESSE DU DESTINANTAIRE
# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $vals->{'PEPALIC'} );
    $vals->{'PEPALIC'} =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE

    # LA REGLE DE GESTION Y...
    $Y = $vals->{'NOMCDXL'}
      ; # ON FABRIQUE LA FAMEUSE VARIABLE Y, SACHANT QU'UNE MÉTHODE OPTIMISÉE N'EST PAS ADAPTÉE AU GÉNIE DU FLUX...
    $Y =~ s/CEDEX.*$/ /
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s*$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE
    $Y =~ s/\s{2,}/ /g
      ; # ON CONCATENE LES BLANCS : ON SAIT JAMAIS COMMENT LES NOM COMPOSES SONT SAISIS...)
    $vals->{'LIBLOCL'} =~ s/\s{2,}/ /g;

    if ( ( index( $vals->{'LIBLOCL'}, $Y ) == 0 ) || $vals->{'INDCDXx'} == 0 )
    {    # SI LIBLOCLIBLOC CONTIENT Y ou INDCDX = 0
        $vals->{'LIBLOCL'} = ' ';    # ON EFFACE LIBLOCLIBLOC
    }
    push( @DADDR, "$vals->{'PENOCOD'} $vals->{'PENOLIB'}" );
    push( @DADDR, $vals->{'PNTREMx'} );
    push( @DADDR, $vals->{'CPLADRx'} );
    push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );
    push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );
    push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );
    push( @DADDR, $vals->{'PEPALIC'} );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

    # TRAITEMENT DE L'ADRESSE DE L'EMETEUR
    $vals->{'PEPA040'} =~ s/FRANCE//i
      ; # PEPA040	@[40] <= (C1) C1_PEPALICPAY_ER (LIBELLE COURT DU PAYS.), ON AFFICHE PAS LE PAYS POUR FRANCE
        # LA REGLE DE GESTION Y...
    $Y = $vals->{'NOMC038'}
      ; # ON FABRIQUE LA FAMEUSE VARIABLE Y, SACHANT QU'UNE MÉTHODE OPTIMISÉE N'EST PAS ADAPTÉE AU GÉNIE DU FLUX...
    $Y =~ s/CEDEX.*$/ /
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s*$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE
    $Y =~ s/\s{2,}/ /g
      ; # ON CONCATENE LES BLANCS : ON SAIT JAMAIS COMMENT LES NOM COMPOSES SONT SAISIS...)
    $vals->{'LIBL036'} =~ s/\s{2,}/ /g;

    if ( ( index( $vals->{'LIBL036'}, $Y ) == 0 ) || $vals->{'INDC034'} == 0 )
    {    # SI LIBLOCLIBLOC CONTIENT Y ou INDCDX = 0
        $vals->{'LIBL036'} = ' ';    # ON EFFACE LIBLOCLIBLOC
    }

    #my @eaddr = ();
    @eaddr = ();
    push( @eaddr, "$vals->{'PENO023'} $vals->{'PENO025'}" );
    push( @eaddr, $vals->{'PNTR026'} );
    push( @eaddr, $vals->{'CPLA027'} );

#	push(@eaddr, "$vals->{'PEAD028'} $vals->{'PEAD029'} $vals->{'PEVO031'} $vals->{'LIBV033'}");
    push( @eaddr,
"$vals->{'PEAD028'} $vals->{'PEAD029'} $vals->{'PEAD032'} $vals->{'LIBV033'}"
    );
    push( @eaddr, "$vals->{'BOIP035'} $vals->{'LIBL036'}" );
    push( @eaddr, "$vals->{'CODC037'} $vals->{'NOMC038'}" );
    push( @eaddr, $vals->{'PEPA040'} );
    @eaddr = user_cleanup_addr(@eaddr);

#warn" adr_CTC[0]=$adr_CTC[0]"."FINTEST   \n";
#if ($adr_CTC[0] eq 'NULL' ) { #
#	$DOC->append('retours', "Un exemple de ce document est à renvoyer à la section après validation.");
#		$adr_CTC[0] = "~";
#		$adr_CTC[0] = " ";
#		$DOC->append_table('xADREM', @eaddr);
# Fin ajout
#	$DOC->append_table('AdrCTC', @eaddr);

    #}
    #ANN  @adr_CTC
    $DOC->append_table( 'xADREM', @eaddr );
}

# FONCTIONS TRAITEMENTS DONNEES v 20070515-084414

sub prepL4 {
    my ($vals) = @_;
    my $Codprod;
    my $LiblonAG = $vals->{'ACOPLIB'};

    # GESTION DES LIBELLÉS DES GARARNTIES
    #INSERTION D'UN BLANC INSÉCABLE POUR L'AFFICHAGE DES LIBELLÉS
    # XXX
    $ACOPLIB = $vals->{'ACOPLIB'};
    $vals->{'PRGALIB'} =~ s/\s/~/g;
    $vals->{'PRPRLIB'} =~ s/\s/~/g;
    $Codprod = $vals->{'PRPRCOD'};
    $Codprod =~ s/\s/~/g;
    $vals->{'PRPRLIB'} =~ s/\*/\`\*/g
      ; #la présence d'une étoile dans ce champ provoque une erreur lors du test suivant
        # Libelle de garantie ou de produit de garantie
        #my $refLibL4 = '';
        #if ($vals->{'PRPRLIB'} =~ /EV|ES|HO/) {
        #	$refLibL4 = $vals->{'PRGALIB'};
        #} else {
        #	$refLibL4 = $vals->{'PRPRLIB'};
        #}
     # Modification par AN pour prendre la libelle longue du produit et non plus le code de produit
    my $refLibL4 = user_prev_get_option( $vals->{'PRPR047'} );

    #fin modif

# si cptAdhL3 == 0 c'est à dire si premier adhérent alors je recherche le code de produit pour labellisation
# selon le code de produit, on affiche LAbel groupA,groupB,groupC ou groupD  et PUIS appeler aCollec, puis afficher NOMPRN, NUMADH a partir du tableau stocké dans table TAB_INTER pendant la tecture de L3
#warn "cptAdhL3 = $cptAdhL3    et Codprod = $Codprod    \n";
    if ( $cptAdhL3 == 0 ) {

        if ( substr( $Codprod, 0, 2 ) eq ('PL') ) {
            if ( substr( $Codprod, 3, 1 ) eq '1' ) {
                if ( $toulouse eq '031555-PVI 10-PS' ) {
                    $DOC->append( 'xCODRUPT', 'AC016031' );
                }
                elsif ( $toulouse eq '031555-PVI 00-PS' ) {
                    $DOC->append( 'xCODRUPT', 'AC016031' );
                }
                else {
                    $DOC->append( 'LABEL', {'LABGROUPA'} );
                    $DOC->append( 'xCODRUPT', 'AC016PCL' );
                }
            }
            elsif ( substr( $Codprod, 3, 1 ) eq '2' ) {
                if ( $toulouse eq '031555-PVI 10-PS' ) {
                    $DOC->append( 'xCODRUPT', 'AC016031' );
                }
                elsif ( $toulouse eq '031555-PVI 00-PS' ) {
                    $DOC->append( 'xCODRUPT', 'AC016031' );
                }
                else {
                    $DOC->append( 'LABEL', {'LABGROUPB'} );
                    $DOC->append( 'xCODRUPT', 'AC016PCL' );
                }
            }
            elsif ( substr( $Codprod, 3, 1 ) eq 'P' ) {
                if ( $toulouse eq '031555-PVI 10-PS' ) {
                    $DOC->append( 'xCODRUPT', 'AC016031' );
                }
                elsif ( $toulouse eq '031555-PVI 00-PS' ) {
                    $DOC->append( 'xCODRUPT', 'AC016031' );
                }
                else {
                    $DOC->append( 'LABEL', {'LABGROUPC'} );
                    $DOC->append( 'xCODRUPT', 'AC016PCL' );
                }
            }
            $flagLabel = 1;
            $DOC->append('aCollec');
            $DOC->append( 'NOMPRN', $TAB_INTER{'NOMPRN'} );
            $DOC->append( 'NUMADH', $TAB_INTER{'NUMADH'} );
            $cptAdhL3++
              ; # une fois le label trouvé, je considère que le premier adhérent est traité
            $cptPLM++
              ; # lorsque je trouve PLM alors j'incrémente le compteur de garantie de Maintien de salaire
        }
        elsif ( substr( $Codprod, 0, 3 ) eq 'PLD' )
        {       # si c'est une produit de deces

        }
        else {    #  si le groupD est une édition standard(non labellisé)
            if ( $toulouse eq '031555-PVI 10-PS' ) {
                $DOC->append('aCollec');
                $DOC->append( 'NOMPRN', $TAB_INTER{'NOMPRN'} );
                $DOC->append( 'NUMADH', $TAB_INTER{'NUMADH'} );
                $cptAdhL3++;
                $DOC->append( 'xCODRUPT', 'AC016031' );
            }
            elsif ( $toulouse eq '031555-PVI 00-PS' ) {
                $DOC->append('aCollec');
                $DOC->append( 'NOMPRN', $TAB_INTER{'NOMPRN'} );
                $DOC->append( 'NUMADH', $TAB_INTER{'NUMADH'} );
                $cptAdhL3++;
                $DOC->append( 'xCODRUPT', 'AC016031' );
            }
            else {
                $DOC->append( 'LABEL', {'LABGROUPD'} );
                $DOC->append('aCollec');
                $DOC->append( 'NOMPRN', $TAB_INTER{'NOMPRN'} );
                $DOC->append( 'NUMADH', $TAB_INTER{'NUMADH'} );
                $cptAdhL3++;

            }
        }

    }

    $CodeProduit{$refLibL4} = $vals->{'PRPRCOD'};
    $DOC->append( 'LIBLONAG', $LiblonAG );
    unless ( defined $cumulParGarantie{$refLibL4} ) {

        #le comptage compte tous les L4 (CI) d'un Adhérent
        $ordreDeGarantie[ $compteurL4Adh++ ] = $refLibL4;
    }

    # CUMUL DES MONTANTS PAR GARANTIE
    $cumulParGarantie{$refLibL4} += $vals->{'MONT003'};
    if ( $vals->{'CODTYPx'} eq 'ASSPRI' ) {
        $matricule = $vals->{'PEPPLIB'};

# demande du métier de retirer le point lorsque les numéro de matricules sont alimentés avec un point (21/07/09)
        $matricule =~ s/\.//;

        #troncature des matricules à gauche par sécurité
        if ( length($matricule) > 10 ) {

#warn "INFO : matricule >10 chars, \"$matricule##$cde_ctr_coll\" truncaturé à gauche\n";
            $matricule =~ s/^.*(.{10})$/$1/;
        }

#$matricule = ''; # incident matricule suite à RDO Prevoyance, demande métier de mettre la zone à blanc (23/10/2012)
    }
}

# #  AJOUTE pour apporter le taux et la base selon le code assiette = BASECO ou TAUXCO
sub prepLJ {
    my ($vals) = @_;
    my $dernierValeur = $#ordreDeGarantie;

# on prend toujours la première des base des assiettes ( des garanties) c'a'd si la table des garanties de bases est encore vide
    if (    ( $vals->{'PRASCOD'} eq 'BASECO' )
        and ( not exists( $Basedesgaranties{ $vals->{'PRPRCOD'} } ) ) )
    {
        $Basedesgaranties{ $vals->{'PRPRCOD'} } =
          $vals->{'ACJIMAH'};    #  le champ ACJIMAH = la valeur de Base
    }
    elsif ( $vals->{'PRASCOD'} eq 'TAUXCO' ) {    # on doit cumuler les taux.
        $Tauxdesgaranties{ $vals->{'PRPRCOD'} } +=
          $vals->{'ACJIMAH'};    #  le champ ACJIMAH = la valeur de Taux
    }
    elsif ( ( $vals->{'PRASCOD'} eq 'PEMPCO' ) and ( $ODGGCOD eq 'M2-PCL-T2' ) )
    { # si GG= M2-PCL-T2 et ligne LJ est PEMPCO je sauvegarde la valeur de cotisation
        $CotPEmp{ $vals->{'PRPRCOD'} } += $vals->{'ACJIMAH'}
          ; # cumul des parts employeur de cotisation dans les blocs adhérent mais pas dans le bloc employeur
    }

}

sub prepL3 {
    my ($vals) = @_;

    #warn "INFO : L3 dump ". Dumper($vals) ."\n";
    #	$DOC->append('aCollec') if $cptAdhL3 == 0;
    trtTotAdh();
    if ( $Nouveaubord == 1 ) {
        undef %AdhCot;

        #		undef %CotPEmp;
        $Nouveaubord = 0;
    }

#CALCUL DU RAPPEL OU TROP PAYE RESTE
#	warn "Trop percu = $vals->{'PERCUxx'}  ; anterieur = $vals->{'ANTERIE'} ;  periode =$vals->{'PERIODE'} \n";
    $eL3_TPERCU = $vals->{'PERCUxx'};

    #warn "anterieur = $vals->{'ANTERIE'}: periode= $vals->{'PERIODE'} \n";
    $eL3_RAPPEL = $vals->{'ANTERIE'};
    if ( $vals->{'PERIODE'} > 0 ) {
        $eL3_RAPPEL += $vals->{'PERIODE'};
    }
    else {
        #	warn"avant eL3_TPERCU = $eL3_TPERCU   \n";

        $eL3_TPERCU += $vals->{'PERIODE'};
    }

    #	warn"eL3_TPERCU = $eL3_TPERCU   \n";
    #	$cptAdhL3++;

    #	$vals->{'PENOLIB'} = ucfirst(lc($vals->{'PENOLIB'}));
    $vals->{'PENOLIB'} = uc( $vals->{'PENOLIB'} );
    $vals->{'PENOPRN'} = ucfirst( lc( $vals->{'PENOPRN'} ) );
    $NomPreAdh         = $vals->{'PENOLIB'} . ' ' . $vals->{'PENOPRN'};
    if ( $cptAdhL3 == 0 )
    { # si premier adhérent je sauvegarde pour ecrire au prochain L4 et encrémenter lorsque j'aurais le code de produit pour déterminer la labellisation.
        $TAB_INTER{'NOMPRN'} = "$vals->{'PENOLIB'} $vals->{'PENOPRN'}";
    }
    else {
        $DOC->append( 'NOMPRN', "$vals->{'PENOLIB'} $vals->{'PENOPRN'}" );
        $cptAdhL3++;

    }
    $NOMIDX = "$vals->{'PENOLIB'}`$vals->{'PENOPRN'}";
    $NOMIDX =~ s/^\s+//;

    $ACH1NUM   = $vals->{'ACH1NUM'};
    $NumAdhCot = $ACH1NUM;
    if ( $cptAdhL3 == 0 ) {
        $TAB_INTER{'NUMADH'} = "$ACH1NUM";
    }
    else {

        $DOC->append( 'NUMADH', $ACH1NUM );
    }
    $Restant           = $vals->{'RESTExx'};
    $TrpcAnt           = $vals->{'PERC038'};
    $EcheanceAnterieur = $vals->{'MTTO036'};

#Mise en place du MNT1118 pour que chaque mouvement de cotisation soit spécifié par un "DIFF"
    $EcheanceAntEvol = $EcheanceAnterieur + $TrpcAnt + $Restant;
}

sub prepENT {
    my ($vals) = @_;

    #  warn"date prelevement = $vals->{'DATTRTx'}    \n";
    $vals->{'DATTRTx'} =~ /(\d{4})(\d{2})/;
    my $annee = $1;
    my $mois  = $2;

    if ( $mois >= 12 ) {
        $mois = 1;
        $annee++;
    }
    else {
        $mois++;
    }

    $DOC->append( 'MPRELEV', sprintf( "%02d/%d", $mois, $annee ) );
    $DATMPREV = sprintf( "%02d/%d", $mois, $annee );
}

sub prepF5 {
    my ($vals) = @_;

    if ( $flagF5 != 0 ) {   # PAS DE TOTAUX AVANT LE PREMIER GRP DE COLLECTIVITE
        initDoc();
        $DOC->append_table( 'xADRLN', @DADDR );
    }
    $flagF5++;

    # APPEL EXIT EVENTUELS POUR REALISER DES TRAITEMENTS SUR CERTAINS CHAMPS
    $Payeur = $vals->{'LIBLONC'};
    if ( $vals->{'LIBLONC'} eq "Adhérent" ) {
        $vals->{'LIBLONC'} = "Membre participant";
    }
    $GED->append( 'xCLEGEDi', $vals->{'LIBLONC'} );
    $PART = $vals->{'LIBLONC'};
    $DOC->append( 'PART',    $PART );
    $DOC->append( 'ctrlDat', $vals->{'DEBPERx'} );
    $cde_ctr_coll = $IDCOLL . " " . $CODGRAx;
    $GED->append( 'xIDDEST', $cde_ctr_coll );

    $CTRL_DAT = $vals->{'DEBPERx'};
    if ( $CTRL_DAT !~ /^(\d{2})\/(\d{2})\/(\d{4})$/ ) {
        die
"ERROR: Unexpected date format \'$CTRL_DAT\' at line $. in input file\n";
    }
    $CTRL_DAT = "$3$2$1";
    $CODPARx  = $vals->{'CODPARx'};
}

exit main(@ARGV);
