#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEdtk::RecordParser;
use oEdtk::Tracking;
use oEdtk::libXls;

use oEUser::Lib;
use strict;
use warnings;

#my %hRECORDS;
my $TRK;
my $VALS;
my $DOC;

my $ER;
my $PRELEV;
my $ECHEANCE;
my $TYPE;
my $CONTCOLL;
my $LIBELLE;
my $CONTRAT;
my $NOM;
my $PRENOM;
my $MONTANT;
my $BOOL = 0;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'CSV',
        entity => oe_corporation_set(),
        keys   => [ 'AGENCE', 'IDDEST', 'NUMIND', 'NUMCOLL', 'TYPE' ]
    );

    user_corp_file_prefixe( $argv[0], '_' );

    #	oe_new_job('--index');

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION

    init_XLS();

    #	my $DOC = oEdtk::TexDoc->new();
    while ( my $ligne = <IN> ) {
        chomp($ligne);
        oe_trimp_space($ligne);
        @DATATAB = split( /\,/, $ligne );
        if ( $BOOL == '0' ) {
            $BOOL = 1;
        }
        else {
            initDoc();
            trt_ENR();
        }
    }
    return 0;
}

sub initDoc {

    #my ($DOC) = @_;

    $ER       = $DATATAB[0];
    $PRELEV   = $DATATAB[1];
    $ECHEANCE = $DATATAB[2];
    $TYPE     = $DATATAB[3];
    $CONTCOLL = $DATATAB[4];
    $LIBELLE  = $DATATAB[5];
    $CONTRAT  = $DATATAB[6];
    $NOM      = $DATATAB[7];
    $PRENOM   = $DATATAB[8];
    $MONTANT  = $DATATAB[9];

    $TRK->track( 'Doc', 1, $ER, $NOM, $CONTRAT, $CONTCOLL, $TYPE );

    1;
}

sub trt_ENR() {
    edit_ligne_XLS();

    1;
}

sub edit_ligne_XLS {

    prod_Xls_Insert_Val($ER);
    prod_Xls_Insert_Val($PRELEV);
    prod_Xls_Insert_Val($ECHEANCE);
    prod_Xls_Insert_Val($TYPE);
    prod_Xls_Insert_Val($CONTCOLL);
    prod_Xls_Insert_Val($LIBELLE);
    prod_Xls_Insert_Val($CONTRAT);
    prod_Xls_Insert_Val($NOM);
    prod_Xls_Insert_Val($PRENOM);
    prod_Xls_Insert_Val($MONTANT);

    prod_Xls_Edit_Ligne();

}

sub init_XLS {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large , Ac7 = Alpha Center Wrap... , NR7 = Numérique Right...  )
    prod_Xls_Col_Init(
        'AC10', 'AC10', 'AC10', 'AC10', 'AC10', 'AL30',
        'AC10', 'AL15', 'AL15', 'AC10'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init("EC001");    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES

    prod_Xls_Insert_Val("ER");
    prod_Xls_Insert_Val("Prélev");
    prod_Xls_Insert_Val("Echéance");
    prod_Xls_Insert_Val("Type");
    prod_Xls_Insert_Val("Cont. Coll");
    prod_Xls_Insert_Val("Libellé");
    prod_Xls_Insert_Val("Contrat");
    prod_Xls_Insert_Val("Nom");
    prod_Xls_Insert_Val("Prénom");
    prod_Xls_Insert_Val("Montant");

    #EDITION DE LA TETE DE COLONNE
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );
    1;
}

exit main(@ARGV);
