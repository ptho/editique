#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::D3;
use oEUser::Descriptor::D8;
use oEUser::Descriptor::OK;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;
use Time::Piece ();
use Time::Seconds;
use Encode qw(encode decode);

###################################################################
########## GESTION DES INDUS : LOT1 - LES COURRIERS EN AUTOMATIQUES
########## FLUX DC003
########## SESAME 1121699 - LOTISSEMENT 1158652
########## DAM : MR ROTAT ANTHONY
########## ECM : MR BEN YOUNES SOUFIANE
###################################################################

my ( $TRK, $DOC, $DOC_parent, $VALS, $GED );
my $RELA2 = 0;
my $EMETTEUR;
my $localite;
my $xNOMDEST;
my $OPT_GED;
my $BASDEPAGE  = '';
my $TYPEDEST   = '';
my $TYPE       = '';    #TYPE DE COURRIER (CTI-01 02 03...)
my $TYPELETTRE = 0;
my $REFDOS     = '';
my ( $NFACT, $NUMCTR, $FINESS );
my $NbE1 = 0;
my ( $xVILDEST, $xCPDEST );
my $CODCENTRE;
my @DADDR;
my $NUMABA;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'MOTIFCOUR', 'TYPECOUR', 'NOMDEST', 'REFDOS', 'MONTANT' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    #$E1->debug();
    $E1->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    #$E1->debug();
    $E7->bind_all_c7();
    my $D3 = oEUser::Descriptor::D3->get();

    # $D3->debug();
    $D3->bind_all_c7();
    my $D8 = oEUser::Descriptor::D8->get();

    #$D8->debug();
    $D8->bind_all_c7();
    my $OK = oEUser::Descriptor::OK->get();

    #$OK->debug();
    $OK->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC        = oEdtk::TexDoc->new();
    $DOC_parent = oEdtk::TexDoc->new();
    $GED        = oEdtk::TexDoc->new();
    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E7' => $E7,
        'D3' => $D3,
        'D8' => $D8,
        'OK' => $OK,
        'Z1' => $Z1
    );

    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n";
            if ( $NbE1 != 0 ) {
                if ( $RELA2 == 0 ) {
                    oe_print_to_output_file($GED);

                    #print $DOC_parent $DOC;
                    #print OUT $DOC_parent;
                    oe_print_to_output_file($DOC);

                    #warn "ENVOI OUT DEBUT\n";
                }
                else {
                    $RELA2 = 0;
                }
            }
            init_Doc();
            $DOC->append( 'xTYPDOC', 'CIND' );    ### Type de document

            $NbE1++;
            @DADDR = ();
            my $civDest = $VALS->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );
            push( @DADDR,
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
            );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
                "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );
            push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

            $VALS->{'PELICCO'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'PELICCO'} );

            ################CENTRE DE GESTION###############################
            $CODCENTRE = $VALS->{'ODCGCOD'};

# warn"CODCENTRE = $CODCENTRE  \n";
# if ($CODCENTRE ne 'MNT-RC') {
# die "\nERROR: Ces courriers sont générés uniquement pour le domaine SANTE \n\n";
# }

            ########### GESTION DE CIVILITE ##############
            # my $CIVILITE = $VALS->{'LICCODC'};
            my $CIVILITE = $VALS->{'PENOCOD'};

            if ( $CIVILITE eq 'M' ) {
                $DOC->append( 'CIVILITE', "Cher Adhérent" );
            }
            elsif ( $CIVILITE eq 'MME' || $CIVILITE eq 'MLLE' ) {
                $DOC->append( 'CIVILITE', "Chère Adhérente" );
            }
            else {
                $DOC->append( 'CIVILITE', "Chère Adhérente, Cher Adhérent" );
            }

            my $CIV = 0;
            $EMETTEUR = $VALS->{'ODERCOD'};

            #warn"EMETTEUR  = $EMETTEUR   \n \n" ;

            if ( $EMETTEUR eq '' ) {
                $EMETTEUR = "ER";
            }
            else {
                $CIV = 1;
            }

            $DOC->append( 'EMETTEUR', $EMETTEUR );
            $DOC->append( 'CIV',      $CIV );

            # $DOC->append('ENTETE');

            if (   $EMETTEUR eq 'D044'
                || $EMETTEUR eq 'D007'
                || $EMETTEUR eq 'D033'
                || $EMETTEUR eq 'D066' )
            {
                ################ADRESSE EMETTEUR#########################
                my $AGENCE = $VALS->{'PENO027'};
                $DOC->append( 'AGENCE', $AGENCE );
            }
            $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
            $xVILDEST = $VALS->{'NOMCDXL'};
            $xCPDEST  = oe_iso_country() . $VALS->{'CODCDXC'};

        }
        elsif ( $id eq 'E7' ) {
            addrE7($VALS);

        }
        elsif ( $id eq 'D3' ) {

            # warn "INFO : dump D3 ". Dumper($VALS) ."\n";
            courrier( $VALS, $DOC );

        }
        elsif ( $id eq 'D8' ) {
            my $CDECOURRIER =
              $VALS->{'DCRHREF'};    #Référence Courrier dans le Flux
                #warn"CDECOURRIER =$CDECOURRIER   typedest = $TYPEDEST  \n" ;
            ( $NFACT, $NUMCTR ) = split( '/', $REFDOS );

            if ( $CDECOURRIER eq 'RELA1' && $TYPEDEST eq 'TIERS' )
            {    #CTI-12 Relance PS J+30
                $DOC->append( 'NFACT',  $NFACT );
                $DOC->append( 'NUMCTR', $NUMCTR );
                $DOC->append( 'LETTRE', {'TYPDOUZE'} );
                $localite  = 'Bourges';
                $BASDEPAGE = 'BASPAGEBourges';
                $TYPE      = 'CTI-12';

                #warn" Courrier 12  \n";

            }
            elsif ( $CDECOURRIER eq 'RELA1' && $TYPEDEST eq 'ADH' )
            {    #CTI-13 Relance Adh J+30
                $DOC->append( 'NFACT',  $NFACT );
                $DOC->append( 'NUMCTR', $NUMCTR );
                $DOC->append( 'LETTRE', {'TYPTREIZE'} );
                $localite  = 'Villeurbanne';
                $BASDEPAGE = 'BASPAGEVilleurbanne';
                $TYPE      = 'CTI-13';

                #warn" Courrier 13 \n";
            }
            elsif ( $CDECOURRIER eq 'RELA2' ) {
                $RELA2 = 1;
            }
            elsif ( $CDECOURRIER eq 'INDUS' ) {
                if ( $TYPELETTRE == 1 ) {
                    $DOC->append( 'NFACT',  $NFACT );
                    $DOC->append( 'NUMCTR', $NUMCTR );
                    $DOC->append( 'LETTRE', {'TYPUN'} );

                    #warn" Courrier 1  \n";
                }
                elsif ( $TYPELETTRE == 2 ) {
                    $DOC->append( 'NFACT',  $NFACT );
                    $DOC->append( 'NUMCTR', $NUMCTR );
                    $DOC->append( 'LETTRE', {'TYPDEUX'} );

                    #warn" Courrier 2";
                }
                elsif ( $TYPELETTRE == 3 ) {
                    $DOC->append( 'NFACT',  $NFACT );
                    $DOC->append( 'NUMCTR', $NUMCTR );
                    $DOC->append( 'LETTRE', {'TYPTROIS'} );

                    #warn" Courrier 3 \n";
                }
                elsif ( $TYPELETTRE == 4 ) {
                    $DOC->append( 'NFACT',  $NFACT );
                    $DOC->append( 'NUMCTR', $NUMCTR );
                    $DOC->append( 'LETTRE', {'TYPQUATRE'} );

                    #warn" Courrier 4 \n";
                }
                elsif ( $TYPELETTRE == 5 ) {
                    $DOC->append( 'NFACT',  $NFACT );
                    $DOC->append( 'NUMCTR', $NUMCTR );
                    $DOC->append( 'LETTRE', {'TYPCINQ'} );

                    #warn" Courrier 5 \n";
                }
                elsif ( $TYPELETTRE = 6 ) {
                    $DOC->append( 'NFACT',  $NFACT );
                    $DOC->append( 'NUMCTR', $NUMCTR );
                    $DOC->append( 'LETTRE', {'TYPSIX'} );

                    #warn" Courrier 6 \n";
                }

                # Déja traité  avec D3

            }
            else {
                die
"\nERROR: REFERENCE COURRIER INCONNU -$CDECOURRIER- $TYPELETTRE\n\n";
            }
            ######INDEXATION GED###################
#$GED->append('xOWNER', 'CTI');  #Emetteur ======> A VERIFIER AVEC MR ROTAT
#$GED->append('xIDEMET', $EMETTEUR);  #Emetteur ======> A VERIFIER AVEC MR ROTAT
            $GED->append( 'xNOMDEST', $xNOMDEST );    #Nom du destinataire
            $GED->append( 'xVILDEST', $xVILDEST )
              ;    #Ville du destinataire        $VALS->{'NOMCDXL'}
            $GED->append( 'xCPDEST', $xCPDEST )
              ;    #oe_iso_country() . $VALS->{'CODCDXC'}
            if ( $TYPEDEST eq 'ADH' ) { $GED->append( 'xIDDEST', $NUMCTR ); }
            else {
                $GED->append( 'xIDDEST', $FINESS );
            }      #Numéro de contrat adhérent ou de fournisseur (finess))
            $GED->append( 'xCLEGEDi',  $TYPE );   #Numéro de contrat individuel
            $GED->append( 'xCLEGEDii', '' );      # nom du gestionnaire sur E2
                 # $GED->append('xCLEGEDiii','');#Date du Traitement d'Edition
            $GED->append( 'xSOURCE', 'SANTE' );    #Domaine metier

        }
        elsif ( $id eq 'OK' ) {
            $DOC->append( 'NUMSS', $VALS->{'NUMROxx'} );
            $DOC->append('COURRIER');
            $DOC->append( 'BASDEPAGE', {$BASDEPAGE} );
            $DOC->append('FINDOC');
        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }

    if ( $RELA2 == 0 ) {

        #print $DOC_parent $DOC;
        #print OUT $DOC_parent;

        oe_print_to_output_file($GED);
        oe_print_to_output_file($DOC);

        #warn "ENVOI OUT FIN\n";

    }
    oe_compo_link($OPT_GED);
    return 0;
}

sub courrier {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT

    if ( $CODCENTRE eq 'MNT-RC' ) {
        $DOC->append( 'CENTRE', 'la Mutuelle Nationale Territoriale' );
    }
    elsif ( $CODCENTRE eq 'MUT-RC' ) {
        $DOC->append( 'CENTRE', 'Mutacité' );
    }

    #Indicateur de Compensation
    my $TYPEINDU = $VALS->{'DCIHIND'};

    #warn"Indic compensation = $TYPEINDU \n";
    my $COMPENSATION = 0;
    if ( $TYPEINDU eq "1" ) {
        $COMPENSATION = 1;
    }

    #Indu clôturé
    if ( $VALS->{'DCIHSLD'} eq "0" && $VALS->{'DCIHCOD'} eq 'CL' ) {
        die
"\nERROR: Les courriers avec Indu clôturé ne doivent pas être imprimés \n\n";
    }

    #Initialisation des motifs de courriers (à vérifier avec Anthony ROTAT)
    my $MOTIFUN     = 'DP';    #Double paiement
    my $MOTIFDEUX   = 'ED';    #Erreur destinataire règlement
    my $MOTIFTROIS  = 'EM';    #Montant erroné
    my $MOTIFQUATRE = 'RD';    #Radiation bénéficiaire
    my $MOTIFCINQ   = 'SS';    #Flux Sécurité Sociale
    my $MOTIFSIX    = 'BE';    #Erreur bénéficiaire
    my $MOTIFSEPT   = 'RA';    #Courriers de radiation Contrat ADH

    #Type de destinataire du paiement TIERS/ADH
    #warn" CODTYPD=   $VALS->{'CODTYPD'}   \n";
    #warn" ENFNTYP=   $VALS->{'ENFNTYP'}   \n";
    #warn" ENFNCOD=   $VALS->{'ENFNCOD'}   \n";

    if ( $VALS->{'CODTYPD'} eq 'FR'
        && ( $VALS->{'ENFNTYP'} ne '' && $VALS->{'ENFNCOD'} ne '' ) )
    {
        $TYPEDEST = 'TIERS';
    }
    elsif ( $VALS->{'CODTYPD'} eq 'CI'
        && ( $VALS->{'ENFNTYP'} eq '' && $VALS->{'ENFNCOD'} eq '' ) )
    {
        $TYPEDEST = 'ADH';
    }

    #warn"EMETTEUR  = $EMETTEUR  \n" ;
    #warn"TYPEDEST = $TYPEDEST  \n";
    my $MOTIF = $VALS->{'DCINMOT'};    #Motif Courrier dans le Flux
                                       #warn"MOTIF =$MOTIF \n" ;

    if (
        (
               $MOTIF eq $MOTIFSIX
            || $MOTIF eq $MOTIFUN
            || $MOTIF eq $MOTIFDEUX
            || $MOTIF eq $MOTIFTROIS
        )
        && $TYPEDEST eq 'TIERS'
      )
    {                                  #CTI-01 Indus PS automatique
        $TYPELETTRE = 1;
        $localite   = 'Bourges';
        $BASDEPAGE  = 'BASPAGEBourges';
        $TYPE       = 'CTI-01';
    }
    elsif (
        (
               $MOTIF eq $MOTIFSEPT
            || $MOTIF eq $MOTIFSIX
            || $MOTIF eq $MOTIFUN
            || $MOTIF eq $MOTIFDEUX
            || $MOTIF eq $MOTIFTROIS
            || $MOTIF eq $MOTIFQUATRE
        )
        && $COMPENSATION == 0
        && $TYPEDEST eq 'ADH'
      )
    {    #CTI-02 Indus Adh automatique NC
        $localite   = 'Villeurbanne';
        $BASDEPAGE  = 'BASPAGEVilleurbanne';
        $TYPELETTRE = 2;
        $TYPE       = 'CTI-02';
    }
    elsif (
        (
               $MOTIF eq $MOTIFSEPT
            || $MOTIF eq $MOTIFSIX
            || $MOTIF eq $MOTIFUN
            || $MOTIF eq $MOTIFDEUX
            || $MOTIF eq $MOTIFTROIS
            || $MOTIF eq $MOTIFQUATRE
        )
        && $COMPENSATION == 1
        && $TYPEDEST eq 'ADH'
      )
    {    #CTI-03 Indus Adh automatique C
        $TYPELETTRE = 3;
        $localite   = 'Villeurbanne';
        $BASDEPAGE  = 'BASPAGEVilleurbanne';
        $TYPE       = 'CTI-03';
    }
    elsif ( $MOTIF eq $MOTIFCINQ && $TYPEDEST eq 'TIERS' )
    {    #CTI-04 Indus PS suite télétransmission
        $TYPELETTRE = 4;
        $localite   = 'Bourges';
        $BASDEPAGE  = 'BASPAGEBourges';
        $TYPE       = 'CTI-04';
    }
    elsif ( $MOTIF eq $MOTIFCINQ && $COMPENSATION == 1 && $TYPEDEST eq 'ADH' )
    {    #CTI-05 Indus Adh suite télétransmission C
        $TYPELETTRE = 5;
        $localite   = 'Villeurbanne';
        $BASDEPAGE  = 'BASPAGEVilleurbanne';
        $TYPE       = 'CTI-05';
    }
    elsif ( $MOTIF eq $MOTIFCINQ && $COMPENSATION == 0 && $TYPEDEST eq 'ADH' )
    {    #CTI-06 Indus Adh suite télétransmission NC
        $TYPELETTRE = 6;
        $localite   = 'Villeurbanne';
        $BASDEPAGE  = 'BASPAGEVilleurbanne';
        $TYPE       = 'CTI-06';
    }
    else {
        die "\nERROR: MOTIF DE COURRIER INCONNU -$MOTIF- pour $TYPEDEST\n\n";
    }

    $DOC->append( 'VILLE', $localite );
    $FINESS = $VALS->{'ENFNCOD'};
    if ( $VALS->{'ENFNCOD'} ne '' ) {
        $DOC->append( 'FINESS', $VALS->{'ENFNCOD'} );
    }
    else { $DOC->append( 'FINESS', 0 ); }
    $REFDOS = $VALS->{'DCINREF'};
    $DOC->append( 'OBJET',   $VALS->{'LIBEMOT'} );
    $DOC->append( 'MONTANT', $VALS->{'DCINMNT'} );
    $DOC->append( 'dtDEB',   convertDate( $VALS->{'DCINDDE'} ) );
    $DOC->append( 'dtFIN',   convertDate( $VALS->{'DCINDFE'} ) );
    $DOC->append( 'PRNASS',  $VALS->{'DCINPRN'} );
    $DOC->append( 'NOMASS',  $VALS->{'DCINNOM'} );

    my $dtCOUR  = $VALS->{'DCINDAT'};
    my $numJOUR = 30;

    my $dt = Time::Piece->strptime( $dtCOUR, '%Y%m%d' );

    #$dt += ONE_DAY * $numJOUR;
    $dtCOUR = $dt->strftime('%Y%m%d');
    $DOC->append( 'dtCOUR', convertDate($dtCOUR) );

    $TRK->track( 'Doc', 1, $MOTIF, $TYPE, $xNOMDEST, $VALS->{'DCINREF'},
        $VALS->{'DCINMNT'} );
    1;
}

sub addrE7 {
    my ($vals) = @_;
    my $Y;

# APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR E1) :
# SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS E1-ZZZZ-NOMCDXLIBACH
# cas 1 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC !C Y	: E1-ZZZZ-BOIPOSLIBLDT + b + E1-ZZZZ-LIBLOCLIBLOC
# cas 2 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC C  Y	: E1-ZZZZ-BOIPOSLIBLDT
# cas 3 : E1-ZZZZ-INDCDX =0 				: E1-ZZZZ-BOIPOSLIBLDT

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $VALS->{'LIBLOCL'} );
    oe_clean_addr_line( $VALS->{'NOMCDXL'} );
    $Y = $VALS->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $VALS->{'INDCDXx'} == 0 ) {

        # CAS 3
        $VALS->{'LIBLOCL'} = '';
    }
    elsif ( $VALS->{'INDCDXx'} == 1 && index( $VALS->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $VALS->{'LIBLOCL'} = '';
    }

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
# my @DADDR = ();
# push(@DADDR, "$VALS->{'PENOCOD'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"); # $VALS->{'PENOCOD'}
    if ( $VALS->{'PNTREMx'} != '' ) {
        @DADDR = ();
        push( @DADDR, $VALS->{'PNTREMx'} );
        push( @DADDR, $VALS->{'CPLADRx'} );
        push( @DADDR,
            "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );
        push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
        push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );
        $VALS->{'PELICCO'} =~ s/FRANCE//i;  # ON AFFICHE PAS LE PAYS POUR FRANCE
        push( @DADDR, $VALS->{'PELICCO'} );
    }
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

    my $adresse =
"$vals->{'CPLA015'} $vals->{'PNTR014'} $vals->{'PEAD016'} $vals->{'PEAD017'} $vals->{'LIBV019'}";
    $DOC->append( 'ADRESSE', $adresse );

    $DOC->append( 'BOITPOS', $vals->{'BOIP021'} );
    $DOC->append( 'CODPOS',  $vals->{'CODC023'} );

    $localite = "$vals->{'LIBL022'} $vals->{'NOMC024'}";
    $DOC->append( 'LOCALITE', $localite );
}

sub init_Doc {
    $DOC->reset();
    $DOC_parent->reset();
    $GED->reset();
    $EMETTEUR = '';
    $localite = '';
    $xNOMDEST = '';
    1;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

exit main(@ARGV);
