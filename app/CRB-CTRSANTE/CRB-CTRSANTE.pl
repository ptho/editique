#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::H8;
use oEUser::Descriptor::H9;
use oEUser::Descriptor::HQ;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;
use Date::Calc qw(Delta_Days
  Add_Delta_Days
  Delta_YMD);
use Readonly;

my ( $TRK, $DOC, $GED, $VALS );
my $TYPECOUR;
my $LIBCENTRE;
my $ACTEGEST;
my $CIV;
my $CODECONTEXTE;
my $NbBENEF     = 0;
my $NbBenefCERT = 0;
my $RangASSPRI;       #RANG DE l'ASSURE PRINCIPAL
my $dtStageASSPRI;    #DATE DE FIN DE STAGE DE l'ASSURE PRINCIPAL
my @tabBENEF;
my @DADDR;
my @tabBenefATTEST;   #LA LISTE DES BENEF(5) POUR L'ATTESTATION DE LABELLISATION
my @tabBenefCERT;     #LA LISTE DES BENEF(S) POUR UN CERTIFICAT D'INSCRIPTION
my @tabGarCERT;
my $NUMCTR;
my $NUMABA;
my $NOMDEST;
my $xNOMDEST;
my $EMETTEUR;
my $DATEACT;
my $dtEFFEGES;
my $GRPGESTbool     = 0;
my $ATTESTLABELbool = 0;
my $COTANNUELLE =
  0;    # COTISATION ANNUELLE POUR L'ATTESTATION DE LABELLISATION = H9
my $COT_ANN = 0;    # COTISATION ANNUELLE POUR UN CERTIFICAT D'INSCRIPTION = H4
my $DATEFINCOT;     # DATE FIN EFFET PERIODE DE COTISATION
my ( $MODEREGLCOT, $EXPMODEREGL, $PERIODICITE, $TERME, $EXPLTERME );
my $RecordH9 = 0;
my $PROFCOD;
my ( $j, $m, $y );
my $TOTALREDUCTION=0;
my $DateMinFinAttest = 22000101;
my $DateDebutAttest;
my $H9DateMaxFinCOT = 19000101;
my %TabRANG;
my ( $Dy, $Dm, $Dd, $periodValidite );
my $MCOTPERI;
my $SORTIEACS=0;
Readonly my $CODESORTIEACS => 'ACS-P-PRI1';

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMCTR', 'NOMDEST', 'EMETTEUR', 'ACTEGESTION', 'NUMABA' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # $E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # $E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    # $E7->debug();
    $E7->bind_all_c7();

    my $H1 = oEUser::Descriptor::H1->get();

    # $H1->debug();
    $H1->bind_all_c7();

    my $H4 = oEUser::Descriptor::H4->get();

    # $H4->debug();
    $H4->bind_all_c7();

    my $H8 = oEUser::Descriptor::H8->get();

    # $H8->debug();
    $H8->bind_all_c7();

    my $H9 = oEUser::Descriptor::H9->get();

    # $H9->debug();
    $H9->bind_all_c7();

    my $HQ = oEUser::Descriptor::HQ->get();

    # $HQ->debug();
    $HQ->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();
    my $LISTE_BENEF_CERT =
      oEdtk::TexDoc->new();   ## LA LISTE DES BENEFICIAIRES AVEC LEURS GARANTIES
    my $LISTE_BENEF_RAD = oEdtk::TexDoc->new();    ## CODE ACTE - ACRD
    my $DELAIS_STAGE_AJ = oEdtk::TexDoc->new();    ## CODE ACTE - ACAJ
    my $LISTE_BENEF     = oEdtk::TexDoc->new();    ## CODE ACTE - ACCC / ACSO
    my $LISTE_BENEF_ATTEST =
      oEdtk::TexDoc->new(); ## LA LISTE DES BENEFICIAIRES POUR L'ATTESTATION LAB

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'H1' => $H1,
        'H4' => $H4,
        'H8' => $H8,
        'H9' => $H9,
        'HQ' => $HQ,
        'Z1' => $Z1
    );

    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n";

            $DOC->append( 'DEBUT', '' );

            $NOMDEST =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            #####GESTION DE LA CIVILITE ENTRE TIERS ET ADHERENT#########
            my $civDest = $VALS->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );
            my $CIVILITE = $VALS->{'PENOCOD'};
            if ( $CIVILITE eq 'M' ) {
                $DOC->append( 'CIVILITE', 'Cher adhérent' );
            }
            elsif ( $CIVILITE eq 'MME' || $CIVILITE eq 'MLLE' ) {
                $DOC->append( 'CIVILITE', 'Chère adhérente' );
            }
            else {
                $DOC->append( 'CIVILITE', 'chère adhérente, cher adhérent' );
            }

            $EMETTEUR = $VALS->{'ODERCOD'};
            if ( $EMETTEUR eq '' ) {
                $CIV      = 0;
                $EMETTEUR = "ER";
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }
            else {
                $CIV = 1;
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }

            $DOC->append( 'CIV', $CIV );

            $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
            $DOC->append( 'xNOMDEST', $xNOMDEST );
            $DOC->append( 'CIVLONG',  $VALS->{'LICCODC'} );

            ################CENTRE DE GESTION###############################
            my $CODCENTRE = $VALS->{'ODCGCOD'};

            if ( $CODCENTRE eq 'MNT-RC' ) {
                $LIBCENTRE = 'Complémentaire Santé ';
            }
            else {
                $LIBCENTRE = 'Prévoyance ';
            }
            $DOC->append( 'LIBCENTRE', $LIBCENTRE );

            ################ADRESSE EMETTEUR#########################
            my $AGENCE = $VALS->{'PENO027'};
            $DOC->append( 'AGENCE', $AGENCE );    #

            my $adresse =
                $VALS->{'PEAD030'} . ' '
              . $VALS->{'LICB032'} . ' '
              . $VALS->{'LICN034'} . ' '
              . $VALS->{'LIBV035'};
            $adresse =~ s/  / /i;
            $DOC->append( 'ADRESSE', $adresse );
            my $boipost = $VALS->{'BOIP037'};
            if ( $boipost != undef ) {
                $DOC->append( 'BOITPOS', $boipost );
            }
            $DOC->append( 'CODPOS', $VALS->{'CODC039'} );

            my $localite = ( $VALS->{'LIBL038'} );
            $DOC->append( 'LOCALITE', $localite );

            # $DOC->append('BASDEPAGE');

            ######INDEXATION GED###################
            # $GED->append('xOWNER', 'BUR');  #-	Domaine de métier
            $GED->append( 'xIDEMET',  $EMETTEUR );        #-	Emetteur
            $GED->append( 'xNOMDEST', $xNOMDEST );        #-	Nom du destinataire
            $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
              ;    #-	Ville du destinataire
               # $GED->append('xCPDEST', oe_iso_country() . $VALS->{'CODCDXC'});

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E2' ) {

#             warn "INFO : dump E2 ". Dumper($VALS) ."\n";
            $PROFCOD = $VALS->{'PROFCOD'};
            if ( $PROFCOD eq 'CMS-ACS' ) {
                $DOC->append( 'PROFCOD', 1 );
            }
            else {
                $DOC->append( 'PROFCOD', 0 );
            }
#            print STDERR "INFO : PROFCOD = $PROFCOD \n";
            $dtEFFEGES = $VALS->{'ACOPDDE'}
              ;    #DATE D'EFFET DE L'ACTE DE GESTION NON CONVERTI EN jj/mm/aaaa
            $DATEACT = convertDate( $VALS->{'ACOPDDE'} )
              ;    #DATE D'EFFET DE L'ACTE DE GESTION
            my $DATEEDT =
              convertDate( $VALS->{'DATEDTx'} );   #Date du Traitement d'Edition
            my $DATESOUSCRIP =
              convertDate( $VALS->{'ACH1DDE'} );    #DATE SOUSCRIPTION

            $DOC->append( 'DATEACT', $DATEACT );
            $DOC->append( 'DATEEDT', $DATEEDT );

            #########NUMERO CONTRAT INDIVIDUEL############
            $NUMCTR = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMCTR', $NUMCTR );

            #############INDEXATION GED###################
            $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
              ;    #-	Numéro de contrat collectif
            $GED->append( 'xCLEGEDi', $NUMCTR )
              ;    #-	Numéro de contrat individuel

            my $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
            $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
            $GED->append( 'xCLEGEDiv', $DATEEDT ); #Date du Traitement d'Edition

            ###########CODE COURRIER + CODE CONTEXTE###################
            $ACTEGEST     = $VALS->{'ACOPCOD'};
            $CODECONTEXTE = $VALS->{'ACOP130'};
            if ( $ACTEGEST eq 'ACAJ' ) {
                $DOC->append( 'xTYPDOC', 'CACO' );    ### Type de document
                $TYPECOUR = 'Ajout de bénéficiaire(s)';
                $DOC->append( 'LETTRE', {$ACTEGEST} );
                $DOC->append( 'OBJET', $TYPECOUR );

            }
            elsif ( $ACTEGEST eq 'ACRD' ) {
                $DOC->append( 'xTYPDOC', 'CRBE' );    ### Type de document
                $TYPECOUR = 'Radiation de bénéficiaire(s)';
                $DOC->append( 'LETTRE', {$ACTEGEST} );
                $DOC->append( 'OBJET', $TYPECOUR );

            }
            elsif ( $ACTEGEST eq 'ACSO' ) {
                $DOC->append( 'xTYPDOC', 'CADC' );    ### Type de document
                if (   $CODECONTEXTE eq 'E-AC'
                    || $CODECONTEXTE eq 'E-AS'
                    || $CODECONTEXTE eq 'E-AU'
                    || $CODECONTEXTE eq 'E-ON'
                    || $CODECONTEXTE eq 'E-TB'
                    || $CODECONTEXTE eq 'CTF' )
                {                                     # CTF = ACS
                     # if ($PROFCOD eq 'CMS-ACS') {$DOC->append('TYPE','Accès Santé');} else {$DOC->append('TYPE','')};  #ACS
                    $TYPECOUR = 'Adhésion';
                    $DOC->append( 'LETTRE', {$ACTEGEST} );
                    $DOC->append( 'OBJET', $TYPECOUR );
                }
                else {
                    die
"\nERROR:CODE ACTE - $ACTEGEST - ASSOCIE A UN CODE CONTEXTE INCONNU - $CODECONTEXTE\n\n";
                }

            }
            elsif ( $ACTEGEST eq 'ACCC' ) {
                $DOC->append( 'xTYPDOC', 'CACO' );    ### Type de document

# if ($PROFCOD eq 'CMS-ACS') {$DOC->append('TYPE','Accès Santé');} else {$DOC->append('TYPE','')};  # ACS
                $TYPECOUR = 'Modification de garanties';
                $DOC->append( 'LETTRE', {$ACTEGEST} );
                $DOC->append( 'OBJET', $TYPECOUR );

            }
            elsif ( $ACTEGEST eq 'ACMA' ) {
                $DOC->append( 'xTYPDOC', 'CREA' );    ### Type de document
                if ( $CODECONTEXTE eq 'SIE' ) {
                    $TYPECOUR = "Accord d'une minoration";
                    $DOC->append( 'LETTRE', { $ACTEGEST . $CODECONTEXTE } );
                    $DOC->append( 'OBJET', $TYPECOUR );

                }
                elsif ( $CODECONTEXTE eq 'CDT' ) {
                    $TYPECOUR =
                      "Modification d'affiliation à la sécurité sociale";
                    $DOC->append( 'LETTRE', { $ACTEGEST . $CODECONTEXTE } );
                    $DOC->append( 'OBJET', $TYPECOUR );

                }
                else {
                    die
"\nERROR:CODE ACTE - $ACTEGEST - ASSOCIE A UN CODE CONTEXTE INCONNU - $CODECONTEXTE\n\n";
                }

            }
            else {
                die "\nERROR:CODE ACTE INCONNU - $ACTEGEST\n\n";
            }

            ###########GROUPE DE GESTION#######################
            my $GRPGEST = substr $VALS->{'ACH1COD'}, 0, 3;
            if ( $GRPGEST eq 'I1-' ) {
                $GRPGESTbool = 1;
            }

            ############ DATE INI POUR ATTESTATION LAB ##############
            my @tabDt1 = split( '/', $DATESOUSCRIP );
            my @tabDt2 = split( '/', $DATEACT );
            if ( $tabDt1[2] < $tabDt2[2] ) {
                $DOC->append( 'DATEINI', '01/01/' . $tabDt2[2] );
            }
            elsif ( $tabDt1[2] == $tabDt2[2] ) {
                $DOC->append( 'DATEINI', $DATESOUSCRIP );
            }

            ############ DATE FIN POUR ATTESTATION LAB ##############
            $DOC->append( 'DATEFIN', '31/12/' . $tabDt2[2] );

            ############ CERTIFICATION D'INSCRIPTION ##############
            my $MODEPAIE =
              $VALS->{'ACPAMOD'};    #MODE DE REGLEMENT DES COTISATIONS
            my $GRPGES = substr $VALS->{'ACH1COD'}, 0, 1;

            if ( $MODEPAIE ne '' ) {
                if ( $MODEPAIE eq 'C' || $MODEPAIE eq 'X' ) {
                    $MODEREGLCOT = 'Paiement par chèque';
                    $EXPMODEREGL =
"Veuillez nous adresser un chèque à chaque échéance : un avis d'appel vous sera envoyé pour vous rappeler votre solde.";
                }
                elsif ( $MODEPAIE eq 'P' ) {
                    $MODEREGLCOT = 'Prélèvement bancaire';
                    $EXPMODEREGL =
"Votre cotisation sera automatiquement prélevée sur votre compte bancaire.";
                }
                elsif ( $MODEPAIE eq 'R' ) {
                    $MODEREGLCOT = 'Prélèvement sur retraire';
                    $EXPMODEREGL =
"Votre cotisation sera automatiquement prélevée sur votre retraite CNRACL sans que vous n'ayez à faire aucune démarche.";
                }
                elsif ( $MODEPAIE eq 'S' ) {
                    $MODEREGLCOT = 'Prélèvement sur salaire';
                    $EXPMODEREGL =
"Votre cotisation sera automatiquement prélevée sur votre salaire sans que vous n'ayez à faire aucune démarche.";
                }
                elsif ( $MODEPAIE eq 'U' ) {
                    $MODEREGLCOT = 'Prélèvement sur participation employeur';
                    $EXPMODEREGL = '';
                }
                elsif ( $MODEPAIE eq 'V' ) {
                    $MODEREGLCOT = 'Paiement par virement';
                    $EXPMODEREGL = '';
                }
                else {
                    die "\nERROR:MODE DE PAIEMENT INCONNU - $MODEPAIE \n\n";
                }
            }
            elsif ( $MODEPAIE eq '' && $GRPGES eq 'M' ) {
                $MODEREGLCOT = 'Prélèvement sur salaire';
                $EXPMODEREGL =
"Votre cotisation sera automatiquement prélevée sur votre salaire sans que vous n'ayez à faire aucune démarche.";
            }
            else {
                $MODEREGLCOT = '';
                $EXPMODEREGL = '';
            }

            $PERIODICITE = $VALS->{'ACFQ065'};    #PERIODICITE DE PAIEMENT
            if ( $PERIODICITE eq '' ) { $PERIODICITE = 'MENSUEL'; }

            my $ECHPRLV = $VALS->{'ACPAECH'}; #TERME : ECHEANCIER DE PRELEVEMENT
            if ( $ECHPRLV eq '' ) {
                $TERME     = '';
                $EXPLTERME = '';
            }
            elsif ( $ECHPRLV eq 'A1' ) {
                $TERME = 'Echoir le 05';
                $EXPLTERME =
"Vous payez vos cotisations par anticipation : votre cotisation de l'échéance en cours  sera requise pour le 5 du mois.";
            }
            elsif ( $ECHPRLV eq 'A3' ) {
                $TERME = 'Echoir le 15';
                $EXPLTERME =
"Vous payez vos cotisations par anticipation : votre cotisation de l'échéance en cours  sera requise pour le 15 du mois.";
            }
            elsif ( $ECHPRLV eq 'E1' ) {
                $TERME = 'Echu le 05';
                $EXPLTERME =
"Vous payez vos cotisations a posteriori : votre cotisation de l'échéance en cours  sera requise pour le 5 du mois suivant.";
            }
            elsif ( $ECHPRLV eq 'E3' ) {
                $TERME = 'Echu le 15';
                $EXPLTERME =
"Vous payez vos cotisations a posteriori : votre cotisation de l'échéance en cours  sera requise pour le 15 du mois suivant.";
            }
            elsif ( $ECHPRLV eq 'PA' ) {
                $TERME     = 'Précompte';
                $EXPLTERME = '';
            }
            elsif ( $ECHPRLV eq 'A2' ) {   # demandé dans l'évolution ACS lot1
                $TERME = 'Echoir le 10';
                $EXPLTERME =
"Vous payez vos cotisations par anticipation : votre cotisation de l'échéance en cours sera requise pour le 10 du mois";
            }
            elsif ( $ECHPRLV eq 'E2' ) {   # demandé dans l'évolution ACS lot1
                $TERME = 'Echu le 10';
                $EXPLTERME =
"Vous payez vos cotisations a posteriori : votre cotisation de l'échéance en cours sera requise pour le 10 du mois suivant";
            }
            else {
                die "\nERROR:ECHEANCIER DE PRELEVEMENT INCONNU - $ECHPRLV \n\n";
            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E7' ) {

            ############BLOC ADRESSE DESTINATAIRE##################
            my @DADDR = ();
            push( @DADDR, $NOMDEST );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
            );
            push( @DADDR, $VALS->{'BOIPOSL'} );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $VALS->{'LICCODP'} );
            $VALS->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $DOC->append_table( 'DADDR', @DADDR );

            # warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";

        }

        if ( $id eq 'H1' ) {

            # warn "INFO : dump H1 ". Dumper($VALS) ."\n";

            my $dtADHESION = convertDate( $VALS->{'ACH2DDE'} )
              ;    #DATE EFFET DE 1ERE ADHESION DE L'ASSURE
            my $dtRAD = convertDate( $VALS->{'ACH2023'} )
              ;    #DATE EFFET DE LA DERNIERE RADIATION DE L'ASSURE
            my $BENEF = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
              ;    #NOM ET PRENOM BENEFICIAIRE
            my $RangBENEF = $VALS->{'ACH2ACA'};    #RANG DE l'ASSURE

            if ( $VALS->{'ACH2COD'} ne 'ASSPRI' ) {
                if ( $dtEFFEGES eq $VALS->{'ACH2DDE'} && $ACTEGEST eq 'ACAJ' ) {
                    $NbBENEF++;
                    push( @tabBENEF, "$RangBENEF/$dtEFFEGES/$BENEF" );
                }

                if ( $dtEFFEGES eq $VALS->{'ACH2023'} && $ACTEGEST eq 'ACRD' ) {
                    $NbBENEF++;
                    $LISTE_BENEF_RAD->append( "NOMBENEF", $BENEF );
                    $LISTE_BENEF_RAD->append( "DATERAD",  $dtRAD );
                    $LISTE_BENEF_RAD->append("EditLigneRAD");
                }
            }

            if ( $ACTEGEST eq 'ACCC' || $ACTEGEST eq 'ACSO' ) {
                if (
                    $VALS->{'ACH2COD'} ne 'ASSPRI'
                    && (   $VALS->{'ACH2023'} > $dtEFFEGES
                        || $VALS->{'ACH2023'} eq '' )
                  )
                {
                    $NbBENEF++;
                    push( @tabBENEF, "$RangBENEF/$dtEFFEGES/$BENEF" );
                }

                if ( $VALS->{'ACH2COD'} eq 'ASSPRI' ) {
                    $NbBENEF++;
                    $RangASSPRI    = $RangBENEF;
                    $dtStageASSPRI = $dtEFFEGES;
                }
            }

            #LA LISTE DES BENEF(S) POUR UNE ATTESTATION DE LABELLISATION
            if ( $VALS->{'ACH2023'} > $dtEFFEGES || $VALS->{'ACH2023'} eq '' ) {
                push( @tabBenefATTEST, "$RangBENEF/$BENEF/0" );
            }

            #LA LISTE DES BENEF(S) POUR LE CERTIFICAT D'INSCRIPTION
            if (   $ACTEGEST eq 'ACSO'
                || $ACTEGEST eq 'ACAJ'
                || $ACTEGEST eq 'ACRD'
                || $ACTEGEST eq 'ACCC'
                || ( $ACTEGEST eq 'ACMA' && $CODECONTEXTE eq 'SIE' ) )
            {
                push( @tabBenefCERT,
"$RangBENEF/$VALS->{'PENOCOD'} $VALS->{'PENOPRN'} $VALS->{'PENOLIB'}/$VALS->{'PECPDAT'}"
                );
                $NbBenefCERT++;
            }

            # warn "INFO : dump tabBenefCERT ". Dumper(@tabBenefCERT) ."\n";

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n"; die();
        }

        if ( $id eq 'H4' ) {

#             warn "INFO : dump H4 ". Dumper($VALS) ."\n";
#            print STDERR "INFO :\nACH4006 =  $VALS->{'ACH4006'}\nSORTIE ACS = $CODESORTIEACS\n";
            $SORTIEACS = 1 if ($VALS->{'ACH4006'} eq $CODESORTIEACS && $VALS->{'ACH4010'} eq undef);
            my $codeGAR = $VALS->{'ACH4COD'};    #code garantie souscrite
            my $dateRAD = $VALS->{'ACH4010'};    #date de radiation
            my $rangAss = $VALS->{'ACH4ACG'};    #RANG DE L'ASSURE
            my $COTMENS = $VALS->{'ACCOMAO'}
              ; #COTIS MENS : MONTANT TTC DE LA GARANTIE POUR L'EXERCICE PAR BENEF
            my $libGAR =
              $VALS->{'PRGA003'};    #LIBELLE LONG DE LA GARANTIE SOUSCRITE
            my $dateADH = $VALS->{'ACH4DDE'};    #DATE D'ADHESION DE LA GARANTIE

       # warn "INFO : dtEFFEGES ".$dtEFFEGES."\n INFO : dateRAD ".$dateRAD."\n";
            ##################### Attestation de labellisation ###############
            # warn"dateRAD = $dateRAD, codeGAR =$codeGAR  \n";
            if (
                $dateRAD eq ''
                && (
                       $codeGAR eq 'CBNES'
                    || $codeGAR eq 'CBNEV'
                    || $codeGAR =~ /^(EV)/

                    # JLE 28/07/2017 modif de la regle de gestion
                    || $codeGAR =~ /MNT[1-9]{1}/i
                )
              )
            {
                $ATTESTLABELbool = 1;

                # ecriture test
                # open (my $desc,'>>','c:/codeGAR.txt');
                # print $desc $codeGAR."\n";
                # close $desc;
            }

#            warn "INFO : COTIS MENS "
#              . $COTMENS
#              . " - RANG BENEF "
#              . $rangAss . "\n";
            if ( $dateRAD eq '' || $dateRAD > $dtEFFEGES ) {
                my $i = 0;
                foreach (@tabBenefATTEST) {
                    my @tab = split( '/', $_ );

                    if ( $tab[0] eq $rangAss ) {
                        my $cot    = $tab[2] * 1;
                        my $NewCot = $cot + $COTMENS;

          # warn "INFO : BENEF ATTEST N ".$i." : ". $_ ." - COT ".$COTMENS."\n";
                        @tabBenefATTEST[$i] =
                          $tab[0] . "/" . $tab[1] . "/" . $NewCot;
                    }
                    $i++;
                }

                $COT_ANN += $COTMENS
                  ;    # COTISATION ANNUELLE POUR UN CERTIFICAT D'INSCRIPTION

                my $j = 0;

                foreach (@tabBenefCERT) {
                    my @tabC = split( '/', $_ );
                    if ( $tabC[0] eq $rangAss ) {
                        push( @tabGarCERT,
                                $tabC[0] . "/"
                              . $codeGAR . "/"
                              . $libGAR . "/"
                              . $dateADH
                              . "/ViDe" );
                    }
                    $j++;
                }
            }

#             warn "INFO : dump tabGarCERT ". Dumper(@tabGarCERT) ."\n";
            # warn "INFO : dump tabBenefCERT ". Dumper(@tabBenefCERT) ."\n";
            # warn "INFO : dump DOC ". Dumper($DOC) ."\n"; die();
        }

        if ( $id eq 'H8' ) {

            # warn "INFO : dump H8 ". Dumper($VALS) ."\n";

            my $RangBENEF  = $VALS->{'ACSTACA'};    #RANG DE l'ASSURE
            my $DureeStage = $VALS->{'ACSTDUR'};    #DUREE DE STAGE
            my $dtSTAGE    = $VALS->{'ACSTDFE'};    #DATE DE FIN DE STAGE

            if ( $DureeStage ne '00' ) {
                ###########Affichage du 1er jour du mois suivant
                $dtSTAGE = convertDate( $VALS->{'ACSTDFE'} );
                my @tabDt = split( '/', $dtSTAGE );
                if ( $tabDt[1] < 12 && $tabDt[1] > 0 ) {
                    $tabDt[1]++;
                    if ( $tabDt[1] < 10 ) {
                        $dtSTAGE = $tabDt[2] . "0" . $tabDt[1] . "01";
                    }
                    else {
                        $dtSTAGE = $tabDt[2] . $tabDt[1] . "01";
                    }
                }
                elsif ( $tabDt[1] == 12 ) {
                    $tabDt[2]++;
                    $dtSTAGE = $tabDt[2] . "0101";
                }
            }

            if ( $ACTEGEST eq 'ACAJ' ) {

                my $i = 0;
                foreach (@tabBENEF) {
                    my @tab = split( '/', $_ );
                    if ( $tab[0] eq $RangBENEF ) {
                        $tabBENEF[$i] =
                          $RangBENEF . "/" . $dtSTAGE . "/" . $tab[2];
                    }
                    $i++;
                }
            }

            if ( $ACTEGEST eq 'ACCC' || $ACTEGEST eq 'ACSO' ) {

                if ( $RangBENEF eq $RangASSPRI ) {
                    if ( $dtSTAGE > $dtStageASSPRI ) {
                        $dtStageASSPRI = $dtSTAGE;
                    }

                }
                else {
                    my $i = 0;
                    foreach (@tabBENEF) {
                        my @tab = split( '/', $_ );
                        if ( $tab[0] eq $RangBENEF && $dtSTAGE > $tab[1] ) {
                            $tabBENEF[$i] =
                              $RangBENEF . "/" . $dtSTAGE . "/" . $tab[2];
                        }
                        $i++;
                    }
                }
            }

            my $j = 0;
            foreach (@tabGarCERT) {
                my @tabC = split( '/', $_ );
                if ( $tabC[0] eq $RangBENEF && $tabC[1] eq $VALS->{'ACH4COD'} )
                {
                    @tabGarCERT[$j] =
                        $tabC[0] . "/"
                      . $tabC[1] . "/"
                      . $tabC[2] . "/"
                      . $tabC[3] . "/"
                      . $DureeStage;
                }
                $j++;
            }

            # warn "INFO : dump tabGarCERT ". Dumper(@tabGarCERT) ."\n";
            # warn "INFO : dump DOC ". Dumper($DOC) ."\n"; die();
        }

        if ( $id eq 'H9' ) {

#             warn "INFO : dump H9 ". Dumper($VALS) ."\n";
            $RecordH9 = 1;    #PRESENCE D'UN RECORD H9 DANS LE FLUX
            my $DATEFIN =
              $VALS->{'ACCODFI'};    #DATE FIN EFFET PERIODE DE COTISATION
            if ( $DATEFINCOT < $DATEFIN ) { $DATEFINCOT = $DATEFIN; }
#            print STDERR "INFO : \nPROFCOD = $PROFCOD et SORTIEACS = $SORTIEACS\n";
            if ( $PROFCOD eq 'CMS-ACS' & $SORTIEACS != 1) {
#                print STDERR "INFO : ACELCOD = $VALS->{'ACELCOD'}\n";
                if ( $VALS->{'ACELCOD'} ne 6 )
                { # cas ACS on prends sur H9 mais pour le calcul du montant de la cotisation annuelle, il faut prendre les valeurs ACS différent à 6
                    if ( ( $VALS->{'ACCODFI'} ne '' ) and ( $dtEFFEGES ne '' ) )
                    {
                        if (
                            Delta_Days(
                                substr( $dtEFFEGES,         0, 4 ),
                                substr( $dtEFFEGES,         4, 2 ),
                                substr( $dtEFFEGES,         6, 2 ),
                                substr( $VALS->{'ACCODFI'}, 0, 4 ),
                                substr( $VALS->{'ACCODFI'}, 4, 2 ),
                                substr( $VALS->{'ACCODFI'}, 6, 2 )
                            ) > 0
                          )
                        {

# date de fin de période de cotisation doit être supérieur à la date d'effet de l'acte
#                           print STDERR "INFO : montant ACS = $VALS->{'ACCOMAO'} \n"; 
                           $COTANNUELLE += $VALS->{'ACCOMAO'}
                              ; #MONTANT TTC DE LA GARANTIE POUR LA PERIODE PAR BENEF
#                           print STDERR "INFO : cumul montant PEC ACS = $COTANNUELLE\n";
                        }
                    }
                }
            }
            else {
                my $COTHT = $VALS->{'ACCOMPH'}
                  ;    #MONTANT HT DE LA GARANTIE POUR LA PERIODE PAR BENEF
                $COTANNUELLE += $COTHT;    #COTIS ANNUELLE
            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n"; die();
        }

        if ( $id eq 'HQ' ) {

            if ( $PROFCOD eq 'CMS-ACS' & $SORTIEACS != 1)
            { # cas ACS on prends sur H9 mais pour le montant cotisation MENSUELle, il ne faut pas prendre les valeurs ACS différent à 6
                 # on recherche la date min de fin d'attestation pour afficher la date de fin de PERIODE et en même temps on sauvegarde la date de debut d'attestation pour le calcul de période
                if ( $DateMinFinAttest > $VALS->{'ACAADFE'} ) {
                    $DateMinFinAttest = $VALS->{'ACAADFE'};
                    $DateDebutAttest  = $VALS->{'ACAADDE'};

# sachant que la date de début et de fin d'attestation sont identiques pour toutes les lignes HQ (ACS pour même foyer) et en plus  la date de début commence toujours au premier  du mois
                }

# On prends en compte uniquement des records dont la date de fin d'attestation FORCE (ACAADFF)) supérieure à la date d'effet de l'acte
# warn" dtEFFEGES = $dtEFFEGES , Date fin attestation forcé = $VALS->{'ACAADFF'} , Montant = $VALS->{'ACAAMNT'},  TOTALREDUCTION=$TOTALREDUCTION  \n" ;
                if (
                    ( $VALS->{'ACAADFF'} eq '' )
                    or (
                        Delta_Days(
                            substr( $dtEFFEGES,         0, 4 ),
                            substr( $dtEFFEGES,         4, 2 ),
                            substr( $dtEFFEGES,         6, 2 ),
                            substr( $VALS->{'ACAADFF'}, 0, 4 ),
                            substr( $VALS->{'ACAADFF'}, 4, 2 ),
                            substr( $VALS->{'ACAADFF'}, 6, 2 )
                        ) > 0
                    )
                  )
                {
                    $TOTALREDUCTION += $VALS->{'ACAAMNT'}
                      ;    #MONTANT TTC DE LA GARANTIE POUR LA PERIODE PAR BENEF
                }

# else
# {
# die " La date de fin attestation forcé $VALS->{'ACAADFF'} est inférieur à la date effet de gestion $dtEFFEGES ";
# }
            }
        }    # FIN de HQ

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $VALS->{'PERF003'} );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $VALS->{'PERF003'} )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }    # FIN DE BOUCLE WHILE

    $DOC->append( 'NbBENEF', $NbBENEF );

    if ( $ACTEGEST eq 'ACAJ' ) {
        foreach (@tabBENEF) {
            my @tab1 = split( '/', $_ );

            if ( $tab1[1] eq '' ) { $tab1[1] = $dtEFFEGES; }

            my $dateSTAGE = convertDate( $tab1[1] );    #DATE DE FIN DE STAGE

            $DELAIS_STAGE_AJ->append( "NOMBENEF",  $tab1[2] );
            $DELAIS_STAGE_AJ->append( "DATESTAGE", $dateSTAGE );
            $DELAIS_STAGE_AJ->append("EditLigneAJ");
        }
        $DOC->append( 'DelaisStageAJ', $DELAIS_STAGE_AJ );

    }

    if ( $ACTEGEST eq 'ACRD' ) {
        $DOC->append( 'ListeBenefRAD', $LISTE_BENEF_RAD );
    }

    if ( $ACTEGEST eq 'ACCC' || $ACTEGEST eq 'ACSO' ) {
        ## SUITE A LA DEMANDE DE VIVIANE BOST:
        ## SI LA DATE DE FIN DE STAGE EST VIDE OU INFERIEURE A LA DATE D'EFFET DE L'ACTE DE GESTION
        ## ON AFFICHE LA DATE D'EFFET DE L'ACTE DE GESTION DANS LE COURRIER

        foreach (@tabBENEF) {
            my @tab1 = split( '/', $_ );

#           warn "INFO : DATE DE FIN DE STAGE ". $tab1[1] ." - ". $dtEFFEGES ."\n";
#            warn "INFO : dump tabBENEF ". Dumper(@tabBENEF) ."\n";
            if ( $tab1[1] eq '' || $tab1[1] < $dtEFFEGES ) {
                $tab1[1] = $dtEFFEGES;
            }

            my $dateSTAGE = convertDate( $tab1[1] );    #DATE DE FIN DE STAGE
            $LISTE_BENEF->append( "NOMBENEF",  $tab1[2] );
            $LISTE_BENEF->append( "DATESTAGE", $dateSTAGE );
            $LISTE_BENEF->append("EditLigne");
        }
        $DOC->append( 'ListeBenef', $LISTE_BENEF );

#        warn "INFO : dump LISTE_BENEF ". Dumper($LISTE_BENEF) ."\n";

        if ( $dtStageASSPRI eq '' || $dtStageASSPRI < $dtEFFEGES ) {
            $dtStageASSPRI = $dtEFFEGES;
        }

        my $dateSTAGEASSPRI =
          convertDate($dtStageASSPRI);    #DATE DE FIN DE STAGE ASSURE PRINCIPAL
        $DOC->append( 'DTSTAGEASSPRI', $dateSTAGEASSPRI );
    }

    $DOC->append( 'GRPGEST', $GRPGESTbool );    #Echéancier de cotisation

#Ne pas générer d'attestation de labellisation en l'absence de record H9 dans le flux d'entrée AC010
    if ( ( $ATTESTLABELbool == 1 ) and ( $RecordH9 eq 1 ) ) {
        $DOC->append( 'ATTESTLABEL', 1 );
    }
    else {
        $DOC->append( 'ATTESTLABEL', 0 );
    }

    if ( $RecordH9 eq 1 ) {

        #     $DOC->append ('ATTESTLABEL', 1);
        $DOC->append( 'RecordHneuf', 1 );
        if ( $PROFCOD eq 'CMS-ACS' & $SORTIEACS != 1) {    #  Si ACS
            if ( $DATEFINCOT > $DateMinFinAttest ) {    # recherche la date min
                $DATEFINCOT = $DateMinFinAttest;
            }
        }

        $DOC->append( 'DATEFINCOT', convertDate($DATEFINCOT) );

    }
    else {
        #     $DOC->append ('ATTESTLABEL', 0);
    }

    if ( $GRPGESTbool == 1 || $ATTESTLABELbool == 1 ) {
        $DOC->append( 'AffPARAFD', 1 );
    }
    else {
        $DOC->append( 'AffPARAFD', 0 );
    }

    $DOC->append('COURRIER');
    $DOC->append('FINCOURRIER');

    if ( $ATTESTLABELbool == 1 && $RecordH9 eq 1 ) {

        #      $DOC->append ('ATTESTLABEL', 1);
        foreach (@tabBenefATTEST) {
            my @tab2 = split( '/', $_ );
            my $CotisMENS = $tab2[2] / 12;
            $LISTE_BENEF_ATTEST->append( "NOMBENEF", $tab2[1] );
            $LISTE_BENEF_ATTEST->append( "COTMENS",  $CotisMENS );
            $LISTE_BENEF_ATTEST->append("EditLigneATTEST");
        }

        $DOC->append( 'ListeBenefATTEST', $LISTE_BENEF_ATTEST );
        $DOC->append( 'COTANNUELLE',      $COTANNUELLE );

        $DOC->include( "CRB-CTRSANTE-ATTEST.tex", "EDTK_DIR_SCRIPT" );
        $DOC->append('StartATTEST');
        $DOC->append('EditENTETE');
        $DOC->append('EditPeriode');
        $DOC->append('TextSTANDARD');
        $DOC->append('FinATTEST');
    }

    if (   $ACTEGEST eq 'ACSO'
        || $ACTEGEST eq 'ACAJ'
        || $ACTEGEST eq 'ACRD'
        || $ACTEGEST eq 'ACCC'
        || ( $ACTEGEST eq 'ACMA' && $CODECONTEXTE eq 'SIE' ) )
    {

        $DOC->append( 'CERTINSCRIPTION', 1 );
        $DOC->append( 'NbBenefCERT',     $NbBenefCERT );
        $DOC->append( 'MODEREGLCOT',     $MODEREGLCOT );    #MODE DE PAIEMENT
        $DOC->append( 'EXPMODEREGL',     $EXPMODEREGL )
          ;    #EXPLICATION: MODE DE PAIEMENT
        $DOC->append( 'PERIODICITE', $PERIODICITE )
          ;    #PERIODICITE DE PAIEMENT  pour la ligne Périodicité de paiement

#   Si ACS alors il faut
#        - Monatnt de la cotisation MENSUELle
#        - Calculer le MONTANT REDUCTION    \MontantACS
#        - Ajouter la ligne "Montant de la cotisation MENSUELle effective"   \MontantEFF
#        - Ajouter la ligne "Montant de la cotisation MENSUEL, semestriel, "  \PERIODECOT  \MCOTPERI
        if ( $PROFCOD eq 'CMS-ACS' & $SORTIEACS != 1 ) {    #  Si ACS
             # si ACS le montant de cotisation MENSUELle est pris sur le H9 (calculé dans la variable $COTMENSUELLE) sinon il est sur le H4 calcule dans la variable  $COT_ANN
            $DOC->append( 'COTANNCERT', $COTANNUELLE );     # cotisation Anuelle
            $DOC->append( 'MontantACS', $TOTALREDUCTION);   # Monatnt réduction ACS
            my $calcul = $COTANNUELLE - $TOTALREDUCTION;
            if ( $calcul > 0 ) { $DOC->append( 'MontantEFF', $calcul ); }
            else { $DOC->append( 'MontantEFF', 0 ); }    # Cotisation Effective
            my $nbrmois = Delta_month( $DateDebutAttest, $DateMinFinAttest );

            if ( $PERIODICITE eq 'ANNUEL' )
            {    # si période = ANNUELle , je ne fais rien

#            $DOC->append ('PERIODECOT', $PERIODICITE);   #PERIODE DE cotisation   :  si absence de variable \PERIODECOT  alors Latex n'affiche pas la ligne "période de cotisation"
            }
            elsif ( $PERIODICITE eq 'MENSUEL' ) {    # si
                $DOC->append( 'PERIODECOT', 'mensuelle' )
                  ;                                  # PERIODE de cotisation
                my $COT =
                  ( $COTANNUELLE / 12 );    # Montant de la cotisation MENSUELle
                $MCOTPERI = ( $TOTALREDUCTION / $nbrmois )
                  ; # le montant d'aide MENSUELle = total aide divisé par le nombre de mois
                $MCOTPERI = ( $COT - $MCOTPERI )
                  ; # Montant MENSUEL de la cotisation effective = montant cotisation menuselle - montant aide MENSUEL
                if ( $MCOTPERI < 0 ) { $DOC->append( 'MCOTPERI', 0 ); }
                else {
                    $DOC->append( 'MCOTPERI', $MCOTPERI );
                } # afficher sur le certificat le montant de la cotisation périodique
            }
            elsif ( $PERIODICITE eq 'SEMESTRIEL' ) {
                $DOC->append( 'PERIODECOT', 'semestrielle' )
                  ;    # PERIODE de cotisation
                my $COT =
                  ( $COTANNUELLE / 2 );  # Montant de la cotisation semestrielle
                $MCOTPERI = ( $TOTALREDUCTION / $nbrmois ) * 6
                  ; # le montant d'aide MENSUELle = total aide divisé par le nombre de mois  et multiplié par 6 car c'est le semestre
                $MCOTPERI = ( $COT - $MCOTPERI )
                  ; # Montant MENSUEL de la cotisation effective = montant cotisation menuselle - montant aide MENSUEL
                if ( $MCOTPERI < 0 ) { $DOC->append( 'MCOTPERI', 0 ); }
                else {
                    $DOC->append( 'MCOTPERI', $MCOTPERI );
                } #  afficher sur le certificat le montant de la cotisation périodique
            }
            elsif ( $PERIODICITE eq 'TRIMESTRIEL' ) {
                $DOC->append( 'PERIODECOT', 'trimestrielle' )
                  ;    # PERIODE de cotisation
                my $COT =
                  ( $COTANNUELLE / 4 ); # Montant de la cotisation TRIMESTRIELLE
                $MCOTPERI = ( $TOTALREDUCTION / $nbrmois ) * 3
                  ; # le montant d'aide trimestriel = total aide divisé par le nombre de mois  et multiplié par 3 car c'est le trimestre
                $MCOTPERI = ( $COT - $MCOTPERI )
                  ; # Montant MENSUEL de la cotisation effective = montant cotisation menuselle - montant aide MENSUEL
                if ( $MCOTPERI < 0 ) { $DOC->append( 'MCOTPERI', 0 ); }
                else                 { $DOC->append( 'MCOTPERI', $MCOTPERI ); }
                ; #  afficher sur le certificat le montant de la cotisation périodique
            }
        }
        elsif ( $PROFCOD eq 'CMS-ACS' & $SORTIEACS == 1 ){
            $DOC->append( 'COTANNCERT', $COTANNUELLE );     # cotisation Anuelle
            $DOC->append( 'MontantACS', $TOTALREDUCTION);   # Monatnt réduction ACS
            my $calcul = $COTANNUELLE - $TOTALREDUCTION;
            if ( $calcul > 0 ) { $DOC->append( 'MontantEFF', $calcul ); }
            else { $DOC->append( 'MontantEFF', 0 ); }    # Cotisation Effective
        }
        else {    # si ce n'est pas de ACS
            $DOC->append( 'COTANNCERT', $COT_ANN )
              ; # si non ACS alors afficher le montant de la cotisation annuelle

        }

        if ( $TERME ne '' ) {
            $DOC->append( 'TERME',     $TERME );        #TERME
            $DOC->append( 'EXPLTERME', $EXPLTERME );    # EXPLICATION TERME
        }

        #      warn "INFO : dump LISTE_BENEF ". Dumper(@tabBenefCERT) ."\n";
        # SUPPRIMER LES BENEF(S) RADIEES
        my @NewtabBenefCERT;
        foreach (@tabBenefCERT) {
            my @tab6 = split( '/', $_ );
            my $myRang = 0;

#          warn "INFO : AVANT TRAITEMENT - RANG < ".$tab6[0]." > - LIGNE < ".$_." > - EXIST < ".$myRang." > \n";

            foreach (@tabGarCERT) {
                my @tab7 = split( '/', $_ );
                if ( $tab6[0] eq $tab7[0] ) {
                    $myRang = 1;
                }
            }
            if ( $myRang eq 1 ) {
                push( @NewtabBenefCERT, $_ );
            }

#          warn "INFO : APRES TRAITEMENT - RANG < ".$tab6[0]." > - LIGNE < ".$_." > - EXIST < ".$myRang." > \n \n";
        }

        #      warn "INFO : dump LISTE_BENEF ". Dumper(@NewtabBenefCERT) ."\n";

# VERIFIER L'EXISTANCE D'UN RECORD H8 POUR AFFICHER OU NON LA COLONNE DE STAGE SUR LE CERTIFICAT
        my $certAvecStage = 0;
        my $j             = 0;
        foreach (@tabGarCERT) {
            my @tab3 = split( '/', $_ );
            if ( $tab3[4] ne 'ViDe' ) { $certAvecStage = 1; }
            if ( $tab3[4] eq 'ViDe' ) {
                @tabGarCERT[$j] =
                    $tab3[0] . "/"
                  . $tab3[1] . "/"
                  . $tab3[2] . "/"
                  . $tab3[3] . "/00";
            }
            $j++;
        }

        $DOC->append( 'CERTAVECSTAGE', $certAvecStage );

        # CREATION DE LA LISTE DES BENEF(S) ET AFFECTATION DES GARANTIES
        foreach (@NewtabBenefCERT) {
            my @tab4 = split( '/', $_ );
            $LISTE_BENEF_CERT->append( "BENEFCERT",   $tab4[1] );
            $LISTE_BENEF_CERT->append( "dtNaissCERT", convertDate( $tab4[2] ) );

            my ( @LisGar, @LisDate, @LisStage );
            foreach (@tabGarCERT) {
                my @tab5 = split( '/', $_ );
                if ( $tab4[0] eq $tab5[0] ) {
                    push( @LisGar,  $tab5[2] );
                    push( @LisDate, convertDate( $tab5[3] ) );
                    if ( $certAvecStage eq 1 ) {
                        push( @LisStage, $tab5[4] . ' mois' );
                    }
                }
            }

            $LISTE_BENEF_CERT->append_table( "GARCERT",   @LisGar );
            $LISTE_BENEF_CERT->append_table( "dtADHCERT", @LisDate );
            if ( $certAvecStage eq 1 ) {
                $LISTE_BENEF_CERT->append_table( "StageCERT", @LisStage );
            }

            $LISTE_BENEF_CERT->append('EditLigneGarCERT');
        }

        # warn "INFO : dump LISTE_BENEF_CERT ". Dumper($LISTE_BENEF_CERT) ."\n";
        $DOC->append( 'ListeBenefCERT', $LISTE_BENEF_CERT );

        $DOC->include( "CRB-CTRSANTE-CERT.tex", "EDTK_DIR_SCRIPT" );
        $DOC->append('StartCERT');
        $DOC->append('EditEnteteCert');
        $DOC->append('EditTabGar');
        $DOC->append('EditTabCot');

        # $DOC->append('EditSign');
        $DOC->append('FinCERT');

    }

    # $GED->append('xSOURCE', 'SANTE'); #-	Domaine métier
    # $GED->append('xOWNER', 'BUR');  #-	Domaine de métier

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";
    # warn "INFO : dump GED ". Dumper($GED) ."\n";
    # die();
    my $ACTE = $ACTEGEST . " " . $CODECONTEXTE;
    $TRK->track( 'Doc', 1, $NUMCTR, $xNOMDEST, $EMETTEUR, $ACTE, $NUMABA );
    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    # $DOC->reset();
    # $GED->reset();
    oe_compo_link();
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}
##########################################################
# il recoit le prarametre 1 : la date min au format (yyymmdd) puis le paramètre 2 = date max au format (yyymmdd)
# il retourne le nombre de mois arrondi au supérieur
##########################################################
sub Delta_month {
    my $datemin = $_[0];
    my $datemax = $_[1];

    ( $Dy, $Dm, $Dd ) = Delta_YMD(
        substr( $datemin, 0, 4 ),
        substr( $datemin, 4, 2 ),
        substr( $datemin, 6, 2 ),
        substr( $datemax, 0, 4 ),
        substr( $datemax, 4, 2 ),
        substr( $datemax, 6, 2 )
    );
    $Dm = $Dm + ( $Dy * 12 );
    if ( $Dd > 0 ) {
        $Dm = $Dm + 1;
    }    # on veut arrondir au supérieur le nombre de mois

    #  $refVar=~s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $Dm;
}

exit main(@ARGV);
