#!/usr/bin/perl
use oEdtk::Main;
use oEUser::Lib;

#use oEUser::Label;
use oEdtk::Config qw(config_read);

use oEdtk::RecordParser;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::Descriptor::C1;
use oEUser::Descriptor::C2;
use oEUser::Descriptor::E3;

#use oEUser::Descriptor::ENT;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::F5;
use oEUser::Descriptor::L3;
use oEUser::Descriptor::L4;
use oEUser::Descriptor::L5;
use oEUser::Descriptor::L6;
use oEUser::Descriptor::L7;
use oEUser::Descriptor::T3;
use oEUser::Descriptor::T4;
use oEUser::Descriptor::T5;
use oEUser::Descriptor::T6;
use oEUser::Descriptor::T7;
use oEUser::Descriptor::OD;
use oEUser::Descriptor::OE;
use Data::Dumper;
use strict;
use oEdtk::Dict;

use File::Basename;

my ( $TRK, $DOC, $CTRL_DAT, $ECHEANCE, $ACOPLIB );
my (
    $GED,     $IDCOLL,  $SECTION, $NOMDEST, $PART,
    $CODGRAx, $CODPARx, $ACH1NUM, $NOMIDX,  $DATMPREV
);

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)
my $CDE_CTR_COLL;
my $CPT_C1            = 0;    # INITIALISATION DÉCOMPTE COLLECTIVITÉS
my $CPT_L3_ADH        = 0;    # Re-initialisaton du compteur L3
my $CPT_L4_ADH        = 0;
my $CUMUL_CB          = 0;
my $CUMUL_IJ          = 0;
my $CUMUL_L6          = 0;
my $CUMUL_LIB         = "";
my $CUMUL_LIB_PG_Mb   = "";
my $CUMUL_LIB_PG_Mc   = "";
my $CUMUL_LIB_PG_Mi   = "";
my $CUMUL_PB          = 0;
my $CUMUL_PC          = 0;
my $CUMUL_TOT_AGT_LOT = 0;    # CUMUL GENERAL DES AGENTS POUR LE LOT
my $CUMUL_TOT_G_LOT   = 0;    # CUMUL GÉNÉRAL POUR CONTROL DU LOT
my $ECHEANCE_ANT_EVOL;
my $ECHEANCE_ANTERIEUR;
my $eL3_RAPPEL      = 0;
my $eL3_TPERCU      = 0;
my $FLAG_F5         = 0;      # Initialisation flag groupe d'assurés
my $MATRICULE       = "";
my $RESTANT         = 0;
my $TOTAL_GLOBAL_CB = 0;
my $TOTAL_GLOBAL_IJ = 0;
my $TOTAL_GLOBAL_L6 = 0;
my $TOTAL_GLOBAL_PB = 0;
my $TOTAL_GLOBAL_PC = 0;
my $TOTAL_GLOBAL    = 0;
my $TOTAL_L4        = 0;
my $TOTAL_RAPPELS   = 0;
my $TOTAL_T_PERCUS  = 0;
my $TROP_PERCU_ANT  = 0;
my %CUMUL_PAR_GAR;
my @DADDR;
my @ORDRE_DE_GARANTIE;
my %RIB;
my $cfg;

#  my $ini_file;
my @exclus;
my @inclus;
my $Label;

#  my $myMesg;
my $message;

#  my $AFF_MSG=0;
my $produit_MNT  = 0;
my $produit_MPCT = 0;
#############################################################################
# cinématique suivante :
#	C1 : Entête 1, standard collectif, pour chaque collectivité
#	C2 : Entête 2, standard collectif, pour chaque C1
#	E3 : Entête 3, pour chaque C1
#	F5 : Entête de lédition des groupes dassurés, de 1 à n pour chaque C1
#	L3 : informations concernant le Contrat Individuel, de 1 à n pour chaque F5
#	L4 - informations sur les éléments de prime, de 0 à n par L3
#	L5 - détail des frais du contrat collectif, de 0 à n par L3
#	L6 - informations sur les cotisations particulières, de 0 à n par L3
#	L7 - informations sur les arrondis du contrat individuel, de 0 à n par L3
#	T3 - informations sur la part du groupe dassurés, de 1 par F5
#	T4 - total des cotisations par garantie technique, de 1 à n par F5
#	T5 - total des frais sur la part, de 1 par F5
#	T6 - total des cotisations particulières, de 1 par F5
#	T7 - total des arrondis, de 1 par F5

#################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
#################################################################################
##########################################################################################################################
# Evolution Date : 10/04/2013
# Demande sésame Interne DSI: 1010018  (projet piloté par l'équipe "Développement spécifique" DS)
# Description : L'équipe DS ajoute deux enregistrements OD, OE dans le flux qui apporte des informations sur l'adresse de CTC et de relevé indentité bancaire (RIB)
#			L'état éditique est évolué pour prendre en compte l'adresse CTC à partir de cette enregistrement OD et non pas de son fichier paramètre.
#			et aussi pour afficher le RIB à la fin de de bordereau
##########################################################################################################################
#  Evolution Date : 26/06/2013
#  Demande sesame lotissement : 1034498 et sesame : 1015387
#  Description :
#       sesame 1040098 : affichage des messages labellisés à l'entête pour les contrats *-CMS sauf ceux qui sont dans la partie exclus du fichier paramètre LABEL.ini, ou Ceux qui sont dans la partie inclus du même fichier
#                                et contrairement à l'EB, il faut mettre un indicateur * si dans la liste des garanties pour un adhérent, il n'existe pas de garantie ES ou EV
#       sesame 1034498 : Reduire la taille des cadres d'adresse CTC et de références bancaires pour placer cote à cote en bas de page à gauche et à droite respectivement.
############################################################################################################################
#  Evolution Date : 25/11/2013
#  Demande sesame lotissement : 1056065
#  Description : Intégration du produit MPCT
###########################################################################################################################
#  Evolution Date : 07/01/2013
#  Demande sesame lotissement : 1061206
#  Description : La gestion des contrats labellisé pour les contrats à exclure CMS en prenant compte les groupes assurés
###########################################################################################################################
#
#

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDCOLL', 'SECTION', 'NOMDEST', 'PART', 'Tpercu' ]
    );

    # trame keys => ['IDDEST', 'SECTION', 'NOMDEST', 'COMPTE', 'INDUS']

    oe_new_job('--index');

    # OUVERTURE DU FICHIER DE CONTROLE
    my $ctrl_idx2 = basename($0);
    $ctrl_idx2 =~ s/\.pl$/.idx2/i;
    open( CTRL, '>', $ctrl_idx2 ) or die "ERROR: $!\n";

    $DOC = oEdtk::TexDoc->new();
    initDoc();

    # Initialisation des records.
    my $C1 = oEUser::Descriptor::C1::get();
    $C1->bind_all_c7();
    my $C2 = oEUser::Descriptor::C2::get();
    $C2->bind_all_c7();
    my $E3 = oEUser::Descriptor::E3::get();
    $E3->bind_all_c7();
    my $OD = oEUser::Descriptor::OD::get();

    #	$OD->debug();
    $OD->bind_all_c7();
    my $OE = oEUser::Descriptor::OE::get();

    #	$OE->debug();
    $OE->bind_all_c7();
    my $F5 = oEUser::Descriptor::F5::get();

    # $F5->debug();
    $F5->bind_all_c7();
    my $L3 = oEUser::Descriptor::L3::get();
    $L3->bind_all_c7();
    my $L4 = oEUser::Descriptor::L4::get();
    $L4->bind_all_c7();
    my $L5 = oEUser::Descriptor::L5::get();
    $L5->bind_all_c7();
    my $L6 = oEUser::Descriptor::L6::get();
    $L6->bind_all_c7();
    my $L7 = oEUser::Descriptor::L7::get();
    $L7->bind_all_c7();
    my $T3 = oEUser::Descriptor::T3::get();
    $T3->bind_all_c7();
    my $T4 = oEUser::Descriptor::T4::get();
    $T4->bind_all_c7();
    my $T5 = oEUser::Descriptor::T5::get();
    $T5->bind_all_c7();
    my $T6 = oEUser::Descriptor::T6::get();
    $T6->bind_all_c7();
    my $T7 = oEUser::Descriptor::T7::get();
    $T7->bind_all_c7();

    #	my $ENT = oEUser::Descriptor::ENT::get();
    #	$ENT->bind_all_c7();
    my $ENTETE = oEUser::Descriptor::ENTETE::get();
    $ENTETE->bind_all_c7();

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $ENTETE,
        'C1'     => $C1,
        'C2'     => $C2,
        'E3'     => $E3,
        'F5'     => $F5,
        'L3'     => $L3,
        'L4'     => $L4,
        'L5'     => $L5,
        'L6'     => $L6,
        'L7'     => $L7,
        'T3'     => $T3,
        'T4'     => $T4,
        'T5'     => $T5,
        'T6'     => $T6,
        'T7'     => $T7,
        'OD'     => $OD,
        'OE'     => $OE
    );

    my $first = 1;
    Charge_Label('AC016SANTE');

    #	Charge_Labelpm('AC016SANTE',\@exclus,\@inclus);  # en cas de test pour
    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'ENTETE' ) {

            # warn "DEBUG: ENTETE = ". $p->dumper($vals) ."\n";
            prepENT($vals);

        }
        elsif ( $id eq 'C1' ) {
            if ( !$first ) {
                emitDoc();
            }
            else {
                $first = 0;
            }
            $GED = oEdtk::TexDoc->new();
            $GED->append( 'xCLEGEDii', $DATMPREV );
            initDoc();

# LA RÉNITIALISATION DES ADRESSES NE DOIT SE FAIRE QU'À CHAQUE NOUVEAU C1
# pas dans initdoc car il peut y avoir 1 adresse pour plusieurs F5 (qui font un initdoc)
            @DADDR = ();

            $CODGRAx = $vals->{'CODGRAx'};
            $NOMDEST = $vals->{'PENOLIB'};
            $NOMDEST =~ s/\s+$//;
            $NOMDEST =~ s/\;/:/g;

            prepC1($vals);

            $GED->append( 'xNOMDEST', $NOMDEST );
            $DOC->append( 'COLL',     $NOMDEST );

        }
        elsif ( $id eq 'C2' ) {
            $IDCOLL = $vals->{'ACCCNUM'};
            my $CDE_CTR_COLL = $vals->{'ACCCNUM'} . " " . $CODGRAx;
            $GED->append( 'xIDDEST', $CDE_CTR_COLL )
              ;    # ajout du code assuré au n° contrat
            $DOC->append( 'IDCOLL', $IDCOLL );

        }
        elsif ( $id eq 'F5' ) {
            prepF5($vals);
            $DOC->append( 'CODGRA', $vals->{'CODGRAx'} );
            $DOC->append( 'PAYEUR', $vals->{'LIBLONC'} );
            my ( $jour, $mois, $annee ) = split( /\//, $vals->{'DEBPERx'} );
            $ECHEANCE = "$mois/$annee";
            $DOC->append( 'ECHEANCE',   $ECHEANCE );
            $GED->append( 'xCLEGEDiii', $ECHEANCE );

            #      $Label = Label_AC016SANTE($IDCOLL);
            $Label = Label_AC016SANTE($CDE_CTR_COLL);

            # warn "INFO: LABEL= $Label \n";
            if ($Label) {
                $message = 'Montant dû par le payeur';
                $DOC->append( 'COLMONTANT', $message );
                $message =
"Nous vous remercions de procéder au versement des cotisations dues à notre organisme au plus tard avant la date d'échéance. A défaut, les garanties concernées pourraient être suspendues";
                $DOC->append( 'MESLABUN', $message );
                $DOC->append( 'TITRE',    "AVIS D'APPEL DE COTISATIONS" );
                $DOC->append( 'MESTITRE',
'Des adhérents au règlement mutualiste labellisé Offre Santé de la MNT'
                );
            }
            else {
                $DOC->append( 'COLMONTANT', 'Montant par garantie' );
                $DOC->append( 'MESLABUN',   ' ' );
                $DOC->append( 'TITRE',    'ETAT DES PRELEVEMENTS SUR SALAIRE' );
                $DOC->append( 'MESTITRE', '' );

            }

        }
        elsif ( $id eq 'L3' ) {
            prepL3($vals);

        }
        elsif ( $id eq 'L4' ) {
            prepL4($vals);

        }
        elsif ( $id eq 'L6' ) {
            $CUMUL_L6 += $vals->{'MONT003'};
        }
        elsif ( $id eq 'OD' ) {
            prepOD($vals);
        }
        elsif ( $id eq 'OE' ) {
            prepOE($vals);

        }

    }

    finCollectivite();

    #	emitDoc();
    #    			if ($MATRICULE == '00204594'){
    # 			warn "NOM: $vals->{'PENOLIB'} $vals->{'PENOPRN'}\n";
    # 			warn "MATRICULE: $MATRICULE \n";
    # 			warn "MONTANT PAR GARANTIE: $CUMUL_CB \n";
    # 			warn "TOTAL DES GARANTIES: $TOTAL_L4 \n";
    # 			warn "RESTE DU: $eL3_RAPPEL ### $vals->{'ANTERIE'}\n";
    # 			warn "TROP PERCU: $vals->{'PERCUxx'} \n";
    # 			warn "MONTANT APPELE: $TOTAL_GLOBAL \n";
    # 			warn "ECART: $ECHEANCE_ANT_EVOL \n";
    # 			warn "\n";
    #
    # 			}

    if ( $CUMUL_TOT_G_LOT == 0 && $CUMUL_TOT_AGT_LOT == 0 ) {
        exit 99;
    }

    oe_compo_link();
    close(CTRL);
    return 0;
}

sub format_num {
    my $num = shift;

    $num = sprintf( "%.2f", $num );
    $num !~ s/\./,/;
    while ( $num =~ s/(\d+)(\d{3})/$1 $2/ ) { ; }
    return $num;
}

sub control_out {
    my ($context) = shift;

    my @vals = ( $CTRL_DAT, $SECTION, "$IDCOLL`$CODGRAx", $CODPARx, $NOMDEST );
    if ( $context eq 'COLL' ) {
        $vals[0] .= "`TOTAL COL.";
        push( @vals,
            "TOTAL COL.", "TOTAL COL.", $CPT_L3_ADH,
            format_num($TOTAL_GLOBAL) );
    }
    else {
        $MATRICULE =~ s/^\s+//;
        $MATRICULE =~ s/ +/ /g;
        $MATRICULE =~ s/\.//g;
        push( @vals, $ACH1NUM, $NOMIDX, $MATRICULE, format_num($TOTAL_L4) );
    }

    my $line = sprintf(
"<50>ac016 :`%-20s<51>%-8s<5U>%-28s<5V>%-4s<5W>%-60s<5X>%-12s<5K>%-44s<5L>%-15s<5M>%-12s<52>Index Controles <53>  2     1  00   ",
        @vals );
    print CTRL "$line\n";
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################
sub initDoc {
    if ( $CPT_C1 > 0 ) {
        finCollectivite();
    }
    $CPT_L4_ADH      = 0;
    $TOTAL_GLOBAL    = 0;
    $TOTAL_GLOBAL_CB = 0;
    $TOTAL_GLOBAL_PB = 0;
    $TOTAL_GLOBAL_PC = 0;
    $TOTAL_GLOBAL_IJ = 0;
    $TOTAL_GLOBAL_L6 = 0;
    $TOTAL_RAPPELS   = 0;
    $TOTAL_T_PERCUS  = 0;
    $CPT_L3_ADH      = 0;
    $NOMDEST         = "";
    initAdh();
    $Label   = 0;
    $FLAG_F5 = 0;    # Initialisation flag groupe d'assurés
}

sub emitDoc {
    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    #	$DOC->reset();
    $DOC = oEdtk::TexDoc->new();
}

sub initAdh() {
    $MATRICULE       = "";
    $RESTANT         = 0;
    $TROP_PERCU_ANT  = 0;
    $eL3_RAPPEL      = 0;
    $eL3_TPERCU      = 0;
    $CPT_L4_ADH      = 0;
    $CUMUL_L6        = 0;
    $CUMUL_CB        = 0;
    $CUMUL_PB        = 0;
    $CUMUL_PC        = 0;
    $CUMUL_IJ        = 0;
    $CUMUL_LIB       = "";
    $CUMUL_LIB_PG_Mi = "";
    $CUMUL_LIB_PG_Mc = "";
    $CUMUL_LIB_PG_Mb = "";
    $TOTAL_L4        = 0;
    $ACH1NUM         = "";
    $produit_MNT     = 0;
    $produit_MPCT    = 0;
    undef %CUMUL_PAR_GAR;
    undef @ORDRE_DE_GARANTIE;
}

sub finCollectivite {

    # CLOTURE DU DERNIER ADHÉRENT
    trtTotAdh();

    my @libelles = ();
    my @montants = ();

    # VENTILATION DES SOUS TOTAUX PAR FAMILLE
    if ( $TOTAL_GLOBAL_CB != 0 ) {
        push( @libelles, "Montant médico chirurgical :" );
        push( @montants, sprintf( "%0.2f", $TOTAL_GLOBAL_CB ) );
    }
    if ( $TOTAL_GLOBAL_PB != 0 ) {
        push( @libelles, "Montant prévoyance de base :" );
        push( @montants, sprintf( "%0.2f", $TOTAL_GLOBAL_PB ) );
    }
    if ( $TOTAL_GLOBAL_PC != 0 ) {
        push( @libelles, "Montant prévoyance complémentaire :" );
        push( @montants, sprintf( "%0.2f", $TOTAL_GLOBAL_PC ) );
    }
    if ( $TOTAL_GLOBAL_IJ != 0 ) {
        push( @libelles, "Montant indemnité journalière :" );
        push( @montants, sprintf( "%0.2f", $TOTAL_GLOBAL_IJ ) );
    }
    if ( $TOTAL_GLOBAL_L6 != 0 ) {
        push( @libelles, "Montant cotisation exceptionnelle :" );
        push( @montants, sprintf( "%0.2f", $TOTAL_GLOBAL_L6 ) );
    }
    if ( $TOTAL_RAPPELS != 0 ) {
        push( @libelles, "Montant Total Rappel :" );
        push( @montants, sprintf( "%0.2f", $TOTAL_RAPPELS ) );
    }
    if ( $TOTAL_T_PERCUS != 0 ) {
        push( @libelles, "Montant Total trop perçu :" );
        push( @montants, sprintf( "%0.2f", $TOTAL_T_PERCUS ) );

    }

# CONTROLE DES CUMULS CALCULÉS ET DES TOTAUX TRANSMIS
# my $sommeCumul = $TOTAL_GLOBAL_CB + $TOTAL_GLOBAL_PB + $TOTAL_GLOBAL_PC + $TOTAL_GLOBAL_IJ +
#    $TOTAL_GLOBAL_L6 + $TOTAL_RAPPELS + $TOTAL_T_PERCUS;
#print OUT $debug if ($sommeCumul ne $TOTAL_GLOBAL);

    # EDITION SI ON A BIEN TRAITÉ AU MOINS UN ADHÉRENT POUR LA COLLECTIVITÉ
    if ( $CPT_L3_ADH != 0 ) {    # GÉRER ICI LES COLLECTIVITÉS À ZÉRO ?
        $DOC->append( 'TotGen',    sprintf( "%0.2f", $TOTAL_GLOBAL ) );
        $GED->append( 'xCLEGEDiv', sprintf( "%0.2f", $TOTAL_GLOBAL ) );
        $DOC->append( 'TotAgt',    $CPT_L3_ADH );
        $DOC->append_table( 'LIBELLES', @libelles );
        $DOC->append_table( 'MONTANTS', @montants );

        #    if (($Label)and ($AFF_MSG)){
        if ($Label) {

#       $message="* Ces garanties labellisées sont éligibles à l'offre labllisée";
#      $DOC->append('MESLABA',$message);
            $DOC->append( 'MESLABA', ' ' );

#       $message="* Ces garanties ne sont pas labellisées ; elles ne sont pas éligibles à l'offre labellisée";
#       $message="Hormis les garanties suivies d'une astérisque, ces garanties sont labellisées et sont donc éligibles à l'offre labellisée";
            $message =
"Ces garanties labellisées sont éligibles à la participation employeur, en dehors de celles suivies d'un astérisque.";
            $DOC->append( 'MESLABB', $message );

            #      $AFF_MSG = 0;
        }
        else {
            $DOC->append( 'MESLABA', '' );
            $DOC->append( 'MESLABB', '' );
        }

        $DOC->append( 'BANQUE',        $RIB{'BANQUE'} );
        $DOC->append( 'GUICHET',       $RIB{'GUICHET'} );
        $DOC->append( 'COMPTE',        $RIB{'COMPTE'} );
        $DOC->append( 'CLE',           $RIB{'CLE'} );
        $DOC->append( 'DEVISE',        $RIB{'DEVISE'} );
        $DOC->append( 'DOMICILIATION', $RIB{'DOMICILIATION'} );
        $DOC->append( 'IBAN',          $RIB{'IBAN'} );
        $DOC->append( 'BIC',           $RIB{'BIC'} );

        $DOC->append('fCollec');
        control_out('COLL');
    }
    emitDoc();
    $TRK->track( 'Doc', 1, $IDCOLL, $SECTION, $NOMDEST, $PART,
        $TOTAL_T_PERCUS );
    $PART = '';

    $CUMUL_TOT_G_LOT   += $TOTAL_GLOBAL;  # CUMUL GÉNÉRAL POUR CONTROL DU LOT
    $CUMUL_TOT_AGT_LOT += $CPT_L3_ADH;    # CUMUL GENERAL DES AGENTS POUR LE LOT
    return 1;
}

#####################################################################################
# Fonction : Charge_Label
# reception : le nom de l'application qui se trouve dans le fichier config/label.ini
#             exemple : AC016SANTE
# Retourne 1 pour le bon fonctionnement
#  Description : Cette fonction charge dans les deux tableaux des contrats INCLUS
#               et EXCLUS qui serviront pour gérer la labellisation par la suite
#
#######################################################################################
sub Charge_Label {
    my $Appli = @_[0];
    my $cfg   = config_read();
    my $value;
    my $Fic;
    my $Trouve     = 0;
    my $Lectinclus = 0;
    my $Lectexclus = 0;
    my $Lect;

    my $ini_file = $cfg->{'EDTK_DIR_CONFIG'} . "/label.ini";

    # warn "INFO : Lecture du fichier paramètre de label : $ini_file  \n";
    open( FIC, "<", $ini_file )
      or die "ERROR: Cannot open file " . $ini_file . " : $!\n";
    while ( $Lect = <FIC> ) {
        $Lect =~ s/\r\n//g;
        $Lect =~ s/\n//g;
        if ( $Lect =~ /$Appli/ ) {
            $Trouve = 1;
        }
        elsif ($Trouve) {
            if ( $Lect =~ /EXCLUS/ ) {
                $Lectexclus = 1;
                $Lectinclus = 0;
            }
            elsif ( $Lect =~ /INCLUS/ ) {    # inclus
                $Lectexclus = 0;
                $Lectinclus = 1;
            }
            elsif ( $Lect =~ /FIN/ ) {
                last;
            }
            elsif ($Lectexclus) {

                #            chomp($Lect);
                if ( $Lect ne '' ) { push( @exclus, $Lect ); }
            }
            elsif ($Lectinclus) {

                #            chomp($Lect);
                if ( $Lect ne '' ) { push( @inclus, $Lect ); }
            }
        }

        #          warn"Lecture = $Lect";
    }
    close(FIC);
    return (1);
}
#####################################################################################
#  Fonction :
#  reception de n° contrat
#  retour : 1 si contrat est labellisé; 0 si contrat n'est pas labellisé
#  Description : Cette fonction verifie la règle de gestion pour labellisation de AC016 sante : si le contrat reçu est un CMS qui ne fait pas partie de la liste exclus ou le contrat de CCS ou CPS dans la liste inclus.
#  Ces listes des contrats inclus ou exclus se trouvent dans deux tables @inclus et @exclus
######################################################################################
sub Label_AC016SANTE {

    my $NContrat = @_[0];

    #my @Texclus = @_[1];
    #my @Tinclus = @_[2];
    my $ind = 0;

    #         while ($ind <= $#exclus){
    #                 warn" $exclus[$ind] \n";
    #                 $ind++;
    #          }
    #      $ind = 0;
    if ( $NContrat =~ /CMS/ ) {

        #        my $testB=$#exclus;
        if ( $#exclus < 0 ) {

            # warn "INFO : Absence des contrats à exclure    \n ";
            return 1;
        }
        else {
         #          warn"Contrat = $NContrat \n ";
         #         while (($NContrat ne $exclus[$ind] ) and ($ind <= $#exclus)){
            while ( ( $NContrat !~ /$exclus[$ind]/ ) and ( $ind <= $#exclus ) )
            {

                #         warn"contrat = $NContrat , tableau=$exclus[$ind] \n";
                $ind++;
            }

 #          warn "ind=$ind ; indtableau = $#exclus ; $exclus[$ind] $NContrat\n";
 #          if ($NContrat eq $exclus[$ind] ){
 #          if ($NContrat =~ $exclus[$ind] ){
            if ( $ind > $#exclus ) {
                return
                  1
                  ; # si l'indice est supérieur au tableau  => on ne trouve pas dans la table des exclus => il est labellisé => retourne 1
            }
            else {
                return 0;    # sinon il n'est pas labellisé => retourne 0
            }
        }
    }
    elsif ( ( $NContrat =~ /CPS/ ) or ( $NContrat =~ /CCS/ ) ) {

        if ( $#inclus < 0 ) {
            warn "INFO : Absence des contrats à inclure    \n ";
            return 0;
        }
        else {
            #        warn"Lecture du tableau INCLUS : $#inclus \n" ;
            $ind = 0;

         #          warn"Contrat = $NContrat \n ";
         #          while (($NContrat ne $inclus[$ind] )and ($ind <= $#inclus)){
            while ( ( $NContrat !~ /$inclus[$ind]/ ) and ( $ind <= $#inclus ) )
            {
                $ind++;
            }

            #          warn "ind=$ind ; indtableau = $#inclus \n";
            #          if ($NContrat eq $inclus[$ind] ){
            #          if ($NContrat =~  $inclus[$ind] ){
            if ( $ind > $#inclus ) {
                return
                  0
                  ; # l'indice est supérieur au tableau  => il n'est pas dans la table de inclus et donc il n'est pas labellisé => retour 0
            }
            else {
                return 1;    # sinon retourne 1 => labellisé
            }
        }
    }
    else {
        return 0;
    }

}
#####################################################################################
# traitement de fin d'enregistrement pour un adhérent.
#

sub trtTotAdh {
    my ( $cle, $val, $element );

    if ( $CPT_L3_ADH == 0 ) {

    # ON NE FAIT PAS DE TOTAL ADHERENT SI ON A PAS ENCORE TRAITE D'ADHERENT
    # C'EST A DIRE A CHAQUE FOIS QUE L'ON FAIT UN NOUVEAU GROUPE DE COLLECTIVITE
        return 1;
    }

    for ( $element = 0 ; $element <= $#ORDRE_DE_GARANTIE ; $element++ ) {
        $cle = $ORDRE_DE_GARANTIE[$element];
        $val = $CUMUL_PAR_GAR{$cle};
        $TOTAL_L4 += $val;

        if ( index( $cle, 'IJ' ) != -1 ) {
            $CUMUL_LIB_PG_Mi .= "$cle ";
            $CUMUL_IJ += $val;

        }
        elsif ( index( $cle, 'PC' ) != -1 ) {
            $CUMUL_LIB_PG_Mc .= "$cle ";
            $CUMUL_PC += $val;

        }
        elsif ( index( $cle, 'PB' ) != -1 ) {
            $CUMUL_LIB_PG_Mb .= "$cle ";
            $CUMUL_PB += $val;

        }
        else {
            $CUMUL_LIB .= "$cle ";
            $CUMUL_CB += $val;
        }
    }

    # CALCUL DU CUMUL PAR ADHERENT HORS TROP PERCU
    $TOTAL_L4 += $eL3_RAPPEL + $CUMUL_L6;

# UN TROP PERCU NE DOIS JAMAIS ÊTRE PLUS GRAND EN VALEUR ABSOLUE QUE LE MONTANT PAR GARANTIE
    if ( ( abs $eL3_TPERCU ) > $TOTAL_L4 ) {
        $eL3_TPERCU = ($TOTAL_L4) * (-1);
    }

    # AFFECTATION DU TROP PAYE AU CUMUL PAR ADHERENT
    $TOTAL_L4 += $eL3_TPERCU;

# UNE ECHEANCE NE PEUT PAS ËTRE NEGATIVE : BALANCE DE L'ÉCHÉANCE, RAZ CALCUL TOTAUX ADH
    if ( $TOTAL_L4 < 0 ) {
        $TOTAL_L4 = 0;
        $CUMUL_PC = 0;
        $CUMUL_PB = 0;
        $CUMUL_CB = 0;
        $CUMUL_IJ = 0;
        $CUMUL_L6 = 0;
    }

    # ÉDITION DES SOUS TOTAUX
    $DOC->append( 'MATRICULE', $MATRICULE );
    $DOC->append( 'RAPPEL',    sprintf( "%0.2f", $eL3_RAPPEL ) );
    $DOC->append( 'TPERCU',    sprintf( "%0.2f", $eL3_TPERCU ) );

    #	warn"GARANTIES Avant test = $CUMUL_LIB \n";
    if ($Label)
    {    # si Label alors afficher * ou ** selon la garantie sinon non label
        $CUMUL_LIB =~ s/^\s+//;    # enlever les space devant
        $CUMUL_LIB =~ s/\s+$//;    # enlever les space après
        if (   ( $CUMUL_LIB =~ /ES/ )
            or ( $CUMUL_LIB =~ /EV/ )
            or ( $CUMUL_LIB eq '' ) )
        {
            $DOC->append( 'GARANTIES', $CUMUL_LIB );
        }
        else {
            $DOC->append( 'GARANTIES', $CUMUL_LIB . "*" );

            #	   $AFF_MSG = 1;
        }
    }
    else {
        $DOC->append( 'GARANTIES', $CUMUL_LIB );
    }
    $DOC->append( 'MONTGAR', sprintf( "%0.2f", $CUMUL_CB ) );
    $DOC->append( 'TOTAL',   sprintf( "%0.2f", $TOTAL_L4 ) );
    $DOC->append( 'ACOPLIB', $ACOPLIB );

    # CONTROLE DE L'ECHEANCE PAR RAPPORT A L'ECHEANCE ANTERIEURE DE L'ADHERENT
    if ( $ECHEANCE_ANTERIEUR == 0 ) {
        $ECHEANCE_ANT_EVOL = 'NV';
    }
    elsif ( $TOTAL_L4 ne $ECHEANCE_ANT_EVOL ) {
        $ECHEANCE_ANT_EVOL = 'DIFF';
    }
    else {
        $ECHEANCE_ANT_EVOL = {'tAdh'};
    }
    $DOC->append( 'EchAnt', $ECHEANCE_ANT_EVOL );

    my @rows = ();
    if ($CUMUL_IJ) {
        my $rdoc = oEdtk::TexDoc->new();
        $rdoc->append( 'GARANTIES', $CUMUL_LIB_PG_Mi );

        #		$rdoc->append('GARANTIES',	$CUMUL_LIB_PG_Mi);
        $rdoc->append( 'MONTGAR', sprintf( "%0.2f", $CUMUL_IJ ) );
        push( @rows, $rdoc );
    }
    if ($CUMUL_PC) {
        my $rdoc = oEdtk::TexDoc->new();
        $rdoc->append( 'GARANTIES', $CUMUL_LIB_PG_Mc );
        $rdoc->append( 'MONTGAR', sprintf( "%0.2f", $CUMUL_PC ) );
        push( @rows, $rdoc );
    }
    if ($CUMUL_PB) {
        my $rdoc = oEdtk::TexDoc->new();

        $rdoc->append( 'GARANTIES', $CUMUL_LIB_PG_Mb );

        $rdoc->append( 'MONTGAR', sprintf( "%0.2f", $CUMUL_PB ) );
        push( @rows, $rdoc );
    }
    if ($CUMUL_L6) {
        my $rdoc = oEdtk::TexDoc->new();
        $rdoc->append( 'GARANTIES', '' );
        $rdoc->append( 'MONTGAR', sprintf( "%0.2f", $CUMUL_L6 ) );
        push( @rows, $rdoc );
    }

    if ( @rows > 0 ) {
        $DOC->append('FinAdhATot');
    }
    else {
        $DOC->append('FinAdhA');
    }

    foreach my $i ( 0 .. $#rows ) {
        $DOC->append( $rows[$i] );
        if ( $i < $#rows ) {
            $DOC->append('FinAdhB');
        }
        else {
            $DOC->append('FinAdhBTot');
        }
    }

    control_out('ADH');

    # SOMME DES CUMUL POUR LA COLLECTIVITE
    $TOTAL_GLOBAL +=
      $TOTAL_L4;    #Somme totale pour une collectivité (Total général)
    $TOTAL_GLOBAL_PC += $CUMUL_PC;
    $TOTAL_GLOBAL_PB += $CUMUL_PB;
    $TOTAL_GLOBAL_CB += $CUMUL_CB;
    $TOTAL_GLOBAL_IJ += $CUMUL_IJ;
    $TOTAL_GLOBAL_L6 += $CUMUL_L6;
    $TOTAL_RAPPELS   += $eL3_RAPPEL;
    $TOTAL_T_PERCUS  += $eL3_TPERCU;

    initAdh();
}

sub prepC1 {
    my ($vals) = @_;

    # APPEL EXIT EVENTUELS POUR REALISER DES TRAITEMENTS SUR CERTAINS CHAMPS
    gestionAdrC1($vals);
    $SECTION = $vals->{'ODERCOD'};

    $GED->append( 'xIDEMET',  $vals->{'ODERCOD'} );
    $GED->append( 'xVILDEST', $vals->{'NOMCDXL'} );

    # 	Initialiser la gestion du tri par pays + CP :
    $GED->append( 'xCPDEST', oe_iso_country() . $vals->{'CODCDXC'} );

    #	my @adr_CTC = user_get_tag_adr_CTC($SECTION);
    #warn "DEBUG: $SECTION => @adr_CTC \n";
    #	if (join (' ', @adr_CTC)=~/\s+33\d{3}\s+/){
    #warn "DEBUG: mise en place code rupture CTC33 \n";
    #		$DOC->append('xCODRUPT', 'CTC33');
    #	}

#	if ($adr_CTC[0] eq 'NULL' || oe_corporation_set() ne 'MNT' ) { #
#		$DOC->append('retours', "Un exemple de ce document est à renvoyer à la section après validation.");
#		$adr_CTC[0] = "~";

#	} else {
#		$DOC->append('retours', "Un exemplaire de ce document est à renvoyer à l'adresse suivante :");
#	}

    #	$DOC->append_table('AdrCTC', @adr_CTC);
    $CPT_C1++;
}

sub prepOD {
    my ($vals) = @_;
    my @adr_CTC;

    #		warn "INFO OD: dump ". Dumper($vals) ."\n";

    if ( $vals->{'LIGNExx'} ne '' ) { push( @adr_CTC, $vals->{'LIGNExx'} ); }
    if ( $vals->{'LIGN003'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN003'} );
    }
    if ( $vals->{'LIGN004'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN004'} );
    }
    if ( $vals->{'LIGN005'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN005'} );
    }
    if ( $vals->{'LIGN006'} ne '' ) {    #$adr_CTC[4] = 'NULL';}
        push( @adr_CTC, $vals->{'LIGN006'} );
    }
    $vals->{'CODEPOS'} =~ s/^\s+//;
    $vals->{'CODEPOS'} =~ s/\s+$//;
    $vals->{'VILLExx'} =~ s/^\s+//;
    $vals->{'VILLExx'} =~ s/\s+$//;
    my $CP = $vals->{'CODEPOS'} . ' ' . $vals->{'VILLExx'};
    if ( $CP ne '' ) {
        push( @adr_CTC, $CP );
    }
    $vals->{'PAYSxxx'} =~ s/^\s+//;
    if ( $vals->{'PAYSxxx'} ne '' ) {
        push( @adr_CTC, $vals->{'PAYSxxx'} );
    }
    if ( join( ' ', @adr_CTC ) =~ /\s+33\d{3}\s+/ ) {

        #warn "DEBUG: mise en place code rupture CTC33 \n";
        $DOC->append( 'xCODRUPT', 'CTC33' );
    }

#		$DOC->append('retours', "Un exemplaire de ce document est à renvoyer à l'adresse suivante :");
#		$DOC->append_table('xADREM', @adr_CTC);
    if ( $adr_CTC[0] eq 'NULL' || oe_corporation_set() ne 'MNT' ) {    #
        $DOC->append( 'retours',
"Un exemple de ce document est à renvoyer à la section après validation."
        );
        $adr_CTC[0] = "~";

    }
    else {
        $DOC->append( 'retours',
"Un exemplaire de ce document est à renvoyer à l'adresse suivante :"
        );
    }

    $DOC->append_table( 'AdrCTC', @adr_CTC );
}

sub prepOE {
    my ($vals) = @_;

    #		warn "INFO OE: dump ". Dumper($vals) ."\n";
    $RIB{'BANQUE'}        = $vals->{'BANQUEx'};
    $RIB{'GUICHET'}       = $vals->{'GUICHET'};
    $RIB{'COMPTE'}        = $vals->{'COMPTEx'};
    $RIB{'CLE'}           = $vals->{'CLExxxx'};
    $RIB{'DEVISE'}        = $vals->{'DEVISEx'};
    $RIB{'DOMICILIATION'} = $vals->{'Domicil'};
    $RIB{'IBAN'}          = $vals->{'IBANxxx'};
    $RIB{'BIC'}           = $vals->{'BICxxxx'};

}

sub gestionAdrC1 {
    my ($vals) = @_;

    # APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR C1) :
    # SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS C1_NOMCDXLIBACH_DD

    # cas 1 : INDCDX =1  + LIBLOCLIBLOC !C Y	: BOIPOSLIBLDT + b + LIBLOCLIBLOC
    # cas 2 : INDCDX =1  + LIBLOCLIBLOC C  Y	: BOIPOSLIBLDT
    # cas 3 : INDCDX =0 					: BOIPOSLIBLDT

    my $Y = '';

# TRAITEMENT DE L'ADRESSE DU DESTINATAIRE #################################
# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $vals->{'PEPALIC'} );
    $vals->{'PEPALIC'} =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE

    # LA REGLE DE GESTION Y...
    $Y = $vals->{'NOMCDXL'}
      ; # ON FABRIQUE LA FAMEUSE VARIABLE Y, SACHANT QU'UNE MÉTHODE OPTIMISÉE N'EST PAS ADAPTÉE AU GÉNIE DU FLUX...
    $Y =~ s/CEDEX.*$/ /
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s*$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE
    $Y =~ s/\s{2,}/ /g
      ; # ON CONCATENE LES BLANCS : ON SAIT JAMAIS COMMENT LES NOM COMPOSES SONT SAISIS...)
    $vals->{'LIBLOCL'} =~ s/\s{2,}/ /g;

    if ( ( index( $vals->{'LIBLOCL'}, $Y ) == 0 ) || $vals->{'INDCDXx'} == 0 )
    {    # SI LIBLOCLIBLOC CONTIENT Y ou INDCDX = 0
        $vals->{'LIBLOCL'} = ' ';    # ON EFFACE LIBLOCLIBLOC
    }
    push( @DADDR, "$vals->{'PENOCOD'} $vals->{'PENOLIB'}" );
    push( @DADDR, $vals->{'PNTREMx'} );
    push( @DADDR, $vals->{'CPLADRx'} );
    push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );
    push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );
    push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );
    push( @DADDR, $vals->{'PEPALIC'} );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

    #	warn "DEBUG: adresse @DADDR\n";

    # TRAITEMENT DE L'ADRESSE DE L'EMETEUR
    $vals->{'PEPA040'} =~ s/FRANCE//i
      ; # PEPA040	@[40] <= (C1) C1_PEPALICPAY_ER (LIBELLE COURT DU PAYS.), ON AFFICHE PAS LE PAYS POUR FRANCE
        # LA REGLE DE GESTION Y...
    $Y = $vals->{'NOMC038'}
      ; # ON FABRIQUE LA FAMEUSE VARIABLE Y, SACHANT QU'UNE MÉTHODE OPTIMISÉE N'EST PAS ADAPTÉE AU GÉNIE DU FLUX...
    $Y =~ s/CEDEX.*$/ /
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s*$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE
    $Y =~ s/\s{2,}/ /g
      ; # ON CONCATENE LES BLANCS : ON SAIT JAMAIS COMMENT LES NOM COMPOSES SONT SAISIS...)
    $vals->{'LIBL036'} =~ s/\s{2,}/ /g;

    if ( ( index( $vals->{'LIBL036'}, $Y ) == 0 ) || $vals->{'INDC034'} == 0 )
    {    # SI LIBLOCLIBLOC CONTIENT Y ou INDCDX = 0
        $vals->{'LIBL036'} = ' ';    # ON EFFACE LIBLOCLIBLOC
    }
    my @eaddr = ();
    push( @eaddr, "$vals->{'PENO023'} $vals->{'PENO025'}" );
    push( @eaddr, $vals->{'PNTR026'} );
    push( @eaddr, $vals->{'CPLA027'} );
    push( @eaddr,
"$vals->{'PEAD028'} $vals->{'PEAD029'} $vals->{'PEVO031'} $vals->{'LIBV033'}"
    );
    push( @eaddr, "$vals->{'BOIP035'} $vals->{'LIBL036'}" );
    push( @eaddr, "$vals->{'CODC037'} $vals->{'NOMC038'}" );
    push( @eaddr, $vals->{'PEPA040'} );
    @eaddr = user_cleanup_addr(@eaddr);
    $DOC->append_table( 'xADREM', @eaddr );
}

sub prepL4 {
    my ($vals) = @_;

    # warn "INFO L4: dump ". Dumper($vals) ."\n";
    # GESTION DES LIBELLÉS DES GARARNTIES
    #INSERTION D'UN BLANC INSÉCABLE POUR L'AFFICHAGE DES LIBELLÉS
    # XXX
    #$vals->{'ACOPLIC'} =;
    $vals->{'PRGALIB'} =~ s/\s/~/g;
    $vals->{'PRPRLIB'} =~ s/\s/~/g;
    $vals->{'PRPRLIB'} =~ s/\*/\`\*/g
      ; #la présence d'une étoile dans ce champ provoque une erreur lors du test suivant
    $ACOPLIB = $vals->{'ACOPLIB'};
    my $refLibL4 = '';
    if ( $vals->{'PRPRLIB'} =~ /EV|ES|HO/ ) {
        $refLibL4    = $vals->{'PRGALIB'};
        $produit_MNT = 1
          ; #  les variables  $produit_MNT et $produit_MPCT  sont des marqueurs pour savoir s'il y a des produits MPCT et MNT en même temps
    }
    elsif ( $vals->{'PRPRLIB'} =~ /MPCT/ ) {

        #	   warn" $NOMIDX \n";
        $refLibL4     = $vals->{'PRGALIB'};
        $produit_MPCT = 1
          ; #   les variables  $produit_MNT et $produit_MPCT  sont des marqueurs pour savoir s'il y a des produits MPCT et MNT en même temps
    }
    elsif ( $vals->{'PRPRLIB'} =~ /CI/ ) {

#		$produit_MNT=0;                 #   le produit CI est le credit impot qui n'est pas considèré comme un produit proprement dit, et donc il est autorise à être coexisté avce un produit MPCT et je ne modifie pas la variable $produit_MNT
        $refLibL4 = $vals->{'PRGALIB'};
    }
    elsif ( $SECTION =~ /D058|I074/ ) {
        $produit_MNT = 1
          ; #   le produit CI est le credit impot qui n'est pas considèré comme un produit proprement dit, et donc il est autorise à être coexisté avce un produit MPCT et je ne modifie pas la variable $produit_MNT
        $refLibL4 = $vals->{'PRPRLIB'};
    }
    elsif ( $IDCOLL =~ /[013|016|033]*-CVS/ ) {
        $produit_MNT = 1
          ; #   le produit CI est le credit impot qui n'est pas considèré comme un produit proprement dit, et donc il est autorise à être coexisté avce un produit MPCT et je ne modifie pas la variable $produit_MNT
        $refLibL4 = $vals->{'PRGALIB'};
    }
    else {
        $refLibL4    = $vals->{'PRPRLIB'};
        $produit_MNT = 1
          ; #   les variables  $produit_MNT et $produit_MPCT  sont des marqueurs pour savoir s'il y a des produits MPCT et MNT en même temps
    }

    if ( ( $produit_MNT == 1 ) and ( $produit_MPCT == 1 ) ) {
        die
" ERROR : Adhérent $NOMIDX possède deux produits MPCT et MNT en même temp";
    } # A la demande de MOA, le programme s'arrête lorsqu'un ahdérent possède deux produits à la fois MNT et MPCT
    unless ( defined $CUMUL_PAR_GAR{$refLibL4} ) {

        #le comptage compte tous les L4 (CI) d'un Adhérent
        $ORDRE_DE_GARANTIE[ $CPT_L4_ADH++ ] = $refLibL4;
    }

    # CUMUL DES MONTANTS PAR GARANTIE
    $CUMUL_PAR_GAR{$refLibL4} += $vals->{'MONT003'};

    if ( $vals->{'CODTYPx'} eq 'ASSPRI' ) {
        $MATRICULE = $vals->{'PEPPLIB'};

# demande du métier de retirer le point lorsque les numéro de matricules sont alimentés avec un point (21/07/09)
        $MATRICULE =~ s/\.//;

# incident matricule suite à RDO Prevoyance, demande métier de mettre la zone à blanc (23/10/2012)
#		$MATRICULE = "";
    }
}

sub prepL3 {
    my ($vals) = @_;

    $DOC->append('aCollec') if $CPT_L3_ADH == 0;
    trtTotAdh();

    #CALCUL DU RAPPEL OU TROP PAYE RESTE
    $eL3_TPERCU = $vals->{'PERCUxx'};
    $eL3_RAPPEL = $vals->{'ANTERIE'};

    if ( $vals->{'PERIODE'} > 0 ) {
        $eL3_RAPPEL += $vals->{'PERIODE'};
    }
    else {
        $eL3_TPERCU += $vals->{'PERIODE'};
    }
    $CPT_L3_ADH++;

    $vals->{'PENOLIB'} = ucfirst( lc( $vals->{'PENOLIB'} ) );
    $vals->{'PENOPRN'} = ucfirst( lc( $vals->{'PENOPRN'} ) );
    $DOC->append( 'NOMPRN', "$vals->{'PENOLIB'} $vals->{'PENOPRN'}" );
    $NOMIDX = "$vals->{'PENOLIB'}`$vals->{'PENOPRN'}";
    $NOMIDX =~ s/^\s+//;

    $ACH1NUM = $vals->{'ACH1NUM'};
    $DOC->append( 'NUMADH', $ACH1NUM );
    $RESTANT            = $vals->{'RESTExx'};
    $TROP_PERCU_ANT     = $vals->{'PERC038'};
    $ECHEANCE_ANTERIEUR = $vals->{'MTTO036'};

#Mise en place du MNT1118 pour que chaque mouvement de cotisation soit spécifié par un "DIFF"
    $ECHEANCE_ANT_EVOL = $ECHEANCE_ANTERIEUR + $TROP_PERCU_ANT + $RESTANT;
}

sub prepENT {
    my ($vals) = @_;

    $vals->{'DATTRTx'} =~ /(\d{4})(\d{2})/;
    my $annee = $1;
    my $mois  = $2;

    if ( $mois >= 12 ) {
        $mois = 1;
        $annee++;
    }
    else {
        $mois++;
    }

#warn "DEBUG: TEST DE PASSAGE.!!!####$mois###$annee###########$vals->{'DATTRTx'}###############################\n";

    $DOC->append( 'MPRELEV', sprintf( "%02d/%d", $mois, $annee ) );
    $DATMPREV = sprintf( "%02d/%d", $mois, $annee );
}

sub prepF5 {
    my ($vals) = @_;

    if ( $FLAG_F5 != 0 ) {  # PAS DE TOTAUX AVANT LE PREMIER GRP DE COLLECTIVITE
        initDoc();
        $DOC->append_table( 'xADRLN', @DADDR );
    }
    $FLAG_F5++;

    # APPEL EXIT EVENTUELS POUR REALISER DES TRAITEMENTS SUR CERTAINS CHAMPS
    if ( $vals->{'LIBLONC'} eq "Adhérent" ) {
        $vals->{'LIBLONC'} = "Membre participant";
    }
    $GED->append( 'xCLEGEDi', $vals->{'LIBLONC'} );
    $PART = $vals->{'LIBLONC'};
    $DOC->append( 'PART',    $PART );
    $DOC->append( 'ctrlDat', $vals->{'DEBPERx'} );
    $CDE_CTR_COLL = $IDCOLL . " " . $CODGRAx;
    $GED->append( 'xIDDEST', $CDE_CTR_COLL );

    $CTRL_DAT = $vals->{'DEBPERx'};
    if ( $CTRL_DAT !~ /^(\d{2})\/(\d{2})\/(\d{4})$/ ) {
        die
"ERROR: Unexpected date format \'$CTRL_DAT\' at line $. in input file\n";
    }
    $CTRL_DAT = "$3$2$1";
    $CODPARx  = $vals->{'CODPARx'};
}

exit main(@ARGV);
