#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls 0.40
  ; # utilisation d'une librairie de fonctions standards dédiée à la production Excel
use oEdtk::logger;
use oEdtk::Tracking;
use Text::CSV;
use DBI;
use oEdtk::DBAdmin qw (@INDEX_COLS);
use oEdtk::RecordParser;
use strict;
use warnings;
use Archive::Zip qw(:ERROR_CODES);

#use	Archive::Zip;
use File::Copy;
use oEdtk::Run;
use Cwd;
use oEdtk::Config qw(config_read);
use oEUser::XML::Generic;
use Data::Dumper;

my ( $ER, $DatePrel, $DateEcheance, $DateEdition );
my @DATATAB;
my ( %hPARM, %hIDX, $VALS );
my $TRK;
my $app;
my $data;

####################################################################################################
# Date : 31/07/2013
# Sesame : lotissement 1030952 demande de créer un excel des rejet pour envoyer au scopmaster
# Nom Application : PRLREJ-AC016
# Flux entrée : XML en lecture le fichier de rejet du flux AC016
#      Sa structure :
#<Rejets>
#   <Rejet>
#		 < Nom_Adherent>	      < /Nom_Adherent>
#		 < Prenom_Adherent>	    < /Prenom_Adherent>
#		 < N_Adherent >	        < /N_Adherent>
#		 < N_Contrat >		      < /N_Contrat>
#		 < Groupe_Assure>	      < /Groupe_Assure>
#		 < Type_Rejet>	        < /Type_Rejet>
#		 < Code_Agence>	      < /Code_Agence>
#		 < Code_Produit>	      < /Code_Produit >
#		 < Date_Prelevement>	  < /Date_Prelevement>
#		 < Date_Echeance>	      < /Date_Echeance>
#		 < Date_Edition>	      < /Date_Edition>
#	  </Rejet>
#	  <Rejet>
#   </Rejet>
#<Rejets>
#
# En sortie : Fichier excel
#
#Description de fonctionnement :  Ce développement est dans le cadre de EFE4 qui retire les adhérents et les bordereaux erronnés
# enfin que le flux AC016 prévoyance puisse passer dans l'application PRPS-AC016 sans incident. Ce traitement est développé par l'équipe "DEVEOPPEMENT SPECIFIQUE"
# et nous envoie deux fichiers par la suite . l'un est le flux AC016 prév propre et l'autre est le fichier de rejet au format XML. Ce programme transforme le format XML en excel pour envoyer au scopmaster
# et crée aussi un fichier index qui n'a pas d'utilié car il n'y a pas de archivage DOCUBASE, cependant je garde dans le traitement pour des évolutions ultérieure
################################################################################################################################################################################################################

# CORPS PRINCIPAL DE L'APPLICATION :

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    my $fi;
    $0 =~ /([\w-]+)[\.plmex]*$/;

    $app = $1;
    warn "Appli = $app;   $0 \n";

    #	my $user = user_get_aneto_user($argv[0]);
    #	warn "user = $user    \n";
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'CSV',
        entity => oe_corporation_set(),
        keys   => [ 'IDCOLL', 'AGENCE', 'NOMPRE', 'DATEECHEANCE', 'TYPEERR' ]
    );
    user_corp_file_prefixe( $argv[0], '_' );

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );

    #	oe_new_job();
    #	oe_new_job('--index');

    # CREATION DE DOCUMENT EXCEL
    initDoc();

    #warn "Initialisation EXCEL     \n";
    # INITIALISATION PROPRE des paramètres pour la lecture du fichier XML
    init_CSV();
    warn "Ouverture fichier  $hPARM{input}   \n";

    # ouverture en lecture le fichier XML
    open( $fi, "<" . $hPARM{encode}, $hPARM{input} )
      or die "ERROR: Cannot open file " . $hPARM{input} . " : $!\n";

    #  Lire le fichier XML;
    my $data = xml_open($fi);

    #	print Dumper($data);

# Boucle de remplissage des lignes de fichier excel
#	print Dumper($data);
# S'il y a plusieurs éléments de retour, alors il retourne un tableau des haschages, sinon il retourne la référence d'un hascage
    if ( ref( $data->{Rejet} ) eq "ARRAY" )
    {    ### Il y a plusieurs adhérent à éditer
        foreach my $personne ( @{ $data->{'Rejet'} } ) {
            $DateEdition  = $personne->{'date_Edition'};
            $DateEcheance = $personne->{'date_Echeance'};
            $DatePrel     = $personne->{'date_Prelevement'};
            $ER           = $personne->{'code_Agence'};

            prod_Xls_Insert_Val( $personne->{'nom_Adherent'} );
            prod_Xls_Insert_Val( $personne->{'prenom_Adherent'} );
            prod_Xls_Insert_Val( $personne->{'n_Adherent'} );
            prod_Xls_Insert_Val( $personne->{'n_Contrat'} );
            prod_Xls_Insert_Val( $personne->{'groupe_Assure'} );
            prod_Xls_Insert_Val( $personne->{'type_Rejet'} );
            prod_Xls_Insert_Val( $personne->{'message_Rejet'} );
            prod_Xls_Insert_Val($ER);
            prod_Xls_Insert_Val( $personne->{'code_Produit'} );
            prod_Xls_Insert_Val( $personne->{'code_Garantie'} );
            prod_Xls_Insert_Val( $personne->{'date_Prelevement'} );
            prod_Xls_Insert_Val( $personne->{'date_Echeance'} );
            prod_Xls_Insert_Val( $personne->{'date_Edition'} );

            prod_Xls_Edit_Ligne();

    # Le tracking est fait pour chaque ligne de rejet, donc chaque ligne d'Excel
            $TRK->track(
                'DOC',
                1,
                $personne->{'n_Contrat'},
                $ER,
                $personne->{'nom_Adherent'} . ' '
                  . $personne->{'prenom_Adherent'},
                $personne->{'date_Echeance'},
                $personne->{'type_Rejet'}
            );

           #		warn"ER = $VALS->{'ER'}  $VALS->{'NUMCONTRAT'} $VALS->{'NOM'} \n";

        }
    }
    else {    # Ici il retourne d'un haschage
         #warn "je ne suis pas un tableau  : $data->{Rejet}->{'nom_Adherent'}  \n";
        $DateEdition  = $data->{'Rejet'}->{'date_Edition'};
        $DateEcheance = $data->{'Rejet'}->{'date_Echeance'};
        $DatePrel     = $data->{'Rejet'}->{'date_Prelevement'};
        $ER           = $data->{'Rejet'}->{'code_Agence'};

        prod_Xls_Insert_Val( $data->{'Rejet'}->{'nom_Adherent'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'prenom_Adherent'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'n_Adherent'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'n_Contrat'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'groupe_Assure'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'type_Rejet'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'message_Rejet'} );
        prod_Xls_Insert_Val($ER);
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'code_Produit'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'code_Garantie'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'date_Prelevement'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'date_Echeance'} );
        prod_Xls_Insert_Val( $data->{'Rejet'}->{'date_Edition'} );

        prod_Xls_Edit_Ligne();

    # Le tracking est fait pour chaque ligne de rejet, donc chaque ligne d'Excel
        $TRK->track(
            'DOC',
            1,
            $data->{'Rejet'}->{'n_Contrat'},
            $ER,
            $data->{'Rejet'}->{'nom_Adherent'} . ' '
              . $data->{'Rejet'}->{'prenom_Adherent'},
            $data->{'Rejet'}->{'date_Echeance'},
            $data->{'Rejet'}->{'type_Rejet'}
        );
    }

    # cloture fichier index
    close($fi) or die "ERROR: fermeture $fi - code retour $!\n";

    # cloture fichier excel
    prod_Xls_Close();
    foreach my $FicExcel ( prod_Xls_Liste_Output() ) {

        # print "test = $FicExcel   \n";

        # CRÉATION D'UN INDEX EDITIQUE ASSOCIÉ AU FICHIER EXCEL
        # Ecriture index
        Creation_Idx($FicExcel);

        # cloture index
    }
    return 0;
}

sub init_CSV {
    %hPARM = (

        #		"encode"		=> ":encoding(UTF-8)",
        "encode" => "",
        "input"  => $ARGV[0],

        #		"output"		=> $app.'.'.oe_ID_LDOC().".idx",
        "output"     => $app . '.' . oe_ID_LDOC(),
        "sep_char"   => ',',
        "quote_char" => '"'
    );

    #	$hPARM{csv} = Text::CSV->new({ binary => 1, sep_char => $hPARM{sep_char},
    #quote_char => $hPARM{quote_char}});
    $hPARM{idx} = Text::CSV->new(
        { binary => 1, sep_char => ';', eol => "\n", quote_space => 0 } );
}

sub initDoc {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
    #
    # 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
    # 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ;
    #	 NR7 = Numérique Right ; AL12 = Alpha Left 12 de large...  )
    #
    prod_Xls_Col_Init(
        'AC12', 'AL20', 'AC12', 'AC10', 'AC20', 'AC12', 'AL40', 'AC12',
        'AL12', 'AL12', 'AC12', 'AC12', 'AC12',
    );

    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    #
    prod_Xls_Init( "", "LISTE DES REJETS DE AC016 PREVOYANCE" )
      ;    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("NOM");
    prod_Xls_Insert_Val("PRENOM");
    prod_Xls_Insert_Val("N°ADHERENT");
    prod_Xls_Insert_Val("N° CONTRAT");
    prod_Xls_Insert_Val("GROUPE ASSURE");
    prod_Xls_Insert_Val("TYPE REJET");
    prod_Xls_Insert_Val("MESSAGE");
    prod_Xls_Insert_Val("CODE AGENCE");
    prod_Xls_Insert_Val("CODE PRODUIT");
    prod_Xls_Insert_Val("CODE GARANTIE");
    prod_Xls_Insert_Val("DATE PRELEVEMENT");
    prod_Xls_Insert_Val("DATE ECHEANCE");
    prod_Xls_Insert_Val("DATE EDITION");

    # ETC.

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );

    1;
}

sub Creation_Idx {
    my $NameFile = shift;
    my @tNameFile = split( /\./, $NameFile );
    my $IndexFile;
    my $SQL;
    my $ligne;

    # ouverture de fichier Index
    $IndexFile = $tNameFile[0] . '.' . oe_ID_LDOC() . '.' . $tNameFile[1];
    warn "IndexFile=$IndexFile   \n";
    open( $hPARM{fo}, ">" . $hPARM{encode}, $IndexFile . '.idx1' )
      or die "ERROR: Cannot open file " . $IndexFile . '.idx' . " : $!\n";

# l'order des champs doivent être respectés
#ED_REFIDDOC,ED_IDLDOC,ED_IDSEQPG,ED_SEQDOC,ED_CPDEST,ED_VILLDEST,ED_IDDEST,ED_NOMDEST,ED_IDEMET,ED_DTEDTION,ED_TYPPROD,ED_PORTADR,ED_ADRLN1,ED_CLEGED1,ED_ADRLN2,ED_CLEGED2,ED_ADRLN3,ED_CLEGED3,ED_ADRLN4,ED_CLEGED4,ED_ADRLN5,ED_CORP,ED_DOCLIB,ED_REFIMP,ED_ADRLN6,ED_SOURCE,ED_OWNER,ED_HOST,ED_IDIDX,ED_CATDOC,ED_CODRUPT,ED_IDLOT,ED_SEQLOT,ED_DTLOT,ED_IDFILIERE,ED_SEQPGDOC,ED_NBPGDOC,ED_POIDSUNIT,ED_NBENC,ED_ENCPDS,ED_BAC_INSERT,ED_TYPED,ED_MODEDI,ED_FORMATP,ED_PGORIEN,ED_FORMFLUX,ED_IDPLI,ED_NBDOCPLI,ED_NUMPGPLI,ED_NBPGPLI,ED_NBFPLI,ED_LISTEREFENC,ED_PDSPLI,ED_TYPOBJ,ED_STATUS,ED_DTPOSTE
# remplier fichier index
    $hIDX{'ED_REFIDDOC'} = $app;    # EN DUR A RECHERCHER lA METHODE
    $hIDX{'ED_IDLDOC'}   = oe_ID_LDOC() . $tNameFile[1];
    $hIDX{'ED_IDSEQPG'}  = '1';
    $hIDX{'ED_SEQDOC'}   = '1';
    $hIDX{'ED_IDEMET'}   = $ER;
    $hIDX{'ED_DTEDTION'} = $DateEdition;

    #	$hIDX{'ED_NOMDEST'}		= $VALS->{'ED_NOMDEST'};
    $hIDX{'ED_CLEGED1'} = 'REJET';
    $hIDX{'ED_CLEGED2'} = $DatePrel;
    $hIDX{'ED_CLEGED3'} = $DateEcheance;
    $hIDX{'ED_CORP'}    = oe_corporation_set();
    $hIDX{'ED_DOCLIB'}  = $IndexFile . '.xls';
    $hIDX{'ED_SOURCE'}  = 'PREV';               # EN DUR A RECHERCHER lA METHODE

    #	$hIDX{'ED_HOST'}		= 'M100E038'; # EN DUR A RECHERCHER lA METHODE
    #	$hIDX{'ED_IDIDX'}		= 'OEDTK04'; # EN DUR A RECHERCHER lA METHODE

    #	$hIDX{'ED_DOCLIB'}		= $VALS->{'ED_DOCLIB'};
    # remplir le fichier index : une ligne par fichier excel
    edit_ligne_index();

    # fermeture de fichier index
    close( $hPARM{fo} ) or die "ERROR: fermeture $IndexFile - code retour $!\n";

    # Archiver les fichiers
    my $Dir = getcwd();
    my $zip = Archive::Zip->new();
    $zip->addFile( $IndexFile . '.idx1', $IndexFile . '.idx1' );
    $zip->addFile( $NameFile,            $IndexFile . '.xls' );

#	unless $zip->writeToFileNamed("C:\Sources\edtk_MNT\transit.nocvs\ged.nocvs\$IndexFile.zip") == AZ_OK;
    my $cfg = config_read('EDOCMNGR');

    #	my $zipfile = "$cfg->{'EDTK_DIR_EDOCMNGR'}/$IndexFile.zip";
    my $zipfile =
        "$cfg->{'EDTK_DIR_EDOCMNGR'}/$tNameFile[0]."
      . oe_ID_LDOC()
      . "_$tNameFile[1].out.zip";
    warn "zipfile=$zipfile  \n";

    my $status = $zip->writeToFileNamed($zipfile);
    die "ERROR: Could not create zip archive\n" if ( $status != AZ_OK );

}

sub edit_ligne_index {
    my @idxcols = map { $$_[0] } @INDEX_COLS[ 0 .. 28 ];
    my @fields;
    foreach my $key (@idxcols) {

        #			warn " $key = $hIDX{$key} \n";
        push( @fields, $hIDX{$key} );
    }

    $hPARM{idx}->print( $hPARM{fo}, \@fields );
}
exit main(@ARGV);
