#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;

my ( $TRK, $DOC, $GED, $VALS );
my $CODCOUR;    #CODE COURRIER
my @DADDR;
my $AGENCE;
my $NOMDEST;
my $NUMCTR;
my $NUMABA;
my $xNOMDEST;
my $GESTIONNAIRE;
my $CODCENTRE;
my $LIBCENTRE;
my $TYPECTR;
my $OPT_GED;
my $DateRD;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys =>
          [ 'NUMCTR', 'NUMABA', 'xNOMDEST', 'CODECOURRIER', 'dtRADIATION' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # $E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # $E2->debug();
    $E2->bind_all_c7();

    my $H1 = oEUser::Descriptor::H1->get();

    # $H1->debug();
    $H1->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();
    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'H1' => $H1,
        'Z1' => $Z1
    );

    my $id;

    $DOC->append( 'xTYPDOC', 'CRDC' );    ### Type de document

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n"; die();

            $DOC->append( 'DEBUT', '' );

            ################CENTRE DE GESTION###############################
            $CODCENTRE = $VALS->{'ODCGCOD'};

            if ( $CODCENTRE eq 'MNT-RC' ) {
                $TYPECTR   = 'SANTE';
                $LIBCENTRE = 'Complémentaire santé ';
            }
            else {
                $TYPECTR   = 'PREV';
                $LIBCENTRE = 'Prévoyance ';
            }
            $DOC->append( 'LIBCENTRE',   $LIBCENTRE );
            $DOC->append( 'TYPECONTRAT', $TYPECTR );

            ################ADRESSE EMETTEUR#########################
            $AGENCE = $VALS->{'PENO027'};
            $DOC->append( 'AGENCE', $AGENCE );
	    $DOC->append( 'xNOM_AGENCE', $AGENCE );
	    	    
            my $adresse =
                $VALS->{'PEAD030'} . ' '
              . $VALS->{'LICN034'} . ' '
              . $VALS->{'LIBV035'};
            $DOC->append( 'ADRESSE', $adresse );
	    $DOC->append( 'xADRESSE_AGENCE', $adresse . ' - ' . $VALS->{'CODC039'} . ' ' . $VALS->{'LIBL038'});
	    
            $DOC->append( 'BOITPOS', $VALS->{'BOIP037'} );
            $DOC->append( 'CODPOS',  $VALS->{'CODC039'} );

            my $localite = ( $VALS->{'LIBL038'} );
            $DOC->append( 'LOCALITE', $localite );

            $DOC->append('BASDEPAGE');

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";

        }

        if ( $id eq 'E2' ) {

            # warn "INFO : dump E2 ". Dumper($VALS) ."\n";

# my $DATEACT = convertDate($VALS->{'ACOPDDE'});	#DATE D'EFFET DE L'ACTE DE GESTION
            my $DATEACT =
              convertDate( $VALS->{'DATEDTx'} );   #Date du Traitement d'Edition
            $DOC->append( 'DATEACT', $DATEACT );

            #########NUMERO CONTRAT INDIVIDUEL
            $NUMCTR = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMCTR', $NUMCTR );

            ######INDEXATION GED###################
            $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
              ;    #-	Numéro de contrat collectif
            $GED->append( 'xCLEGEDi', $NUMCTR )
              ;    #-	Numéro de contrat individuel

            $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
            $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
              # $GED->append('xCLEGEDiii',$DATEACT); #Date du Traitement d'Edition

            #CODE COURRIER
            $CODCOUR = $VALS->{'CDCOURx'};

            if ( $CODCOUR ne 'RESDCS' && $CODCOUR ne 'RESDCP' ) {
                die "\nERROR:CODE COURRIER INCONNU\n\n";
            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'H1' ) {

            # warn "INFO : dump H1 ". Dumper($VALS) ."\n"; die();

            my $TYPEASS = $VALS->{'ACH2COD'};    #le code type d'assuré

            if ( $TYPEASS eq 'ASSPRI' ) {
                $DateRD = $VALS->{'ACH2023'}
                  ;    #Date Effet de la Dernière Radiation de l'Assuré

                if ( $DateRD ne '' ) {
                    $DateRD = convertDate($DateRD)
                      ;    #Date Effet de la Dernière Radiation de l'Assuré
                    $DOC->append( 'DATERD', $DateRD );
                    $DOC->append( 'NOM',    $VALS->{'PENOLIB'} );
                    $DOC->append( 'PRENOM', $VALS->{'PENOPRN'} );

                }
                else {
                    die
"\nERROR:Impossible de generer le courrier si la date de radiation est vide\n\n";
                }
            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'OPTION' ) {

#             warn "INFO : dump OPTIONU5064 ". Dumper($VALS) ."\n";

            if ( $VALS->[0] eq 'U5064 ' ) {
               my $civDest = $VALS->[2];
               $DOC->append( 'civDest', $civDest );

                ############BLOC ADRESSE DESTINATAIRE##################
                my @DADDR = ();
                $NOMDEST = "$VALS->[2] $VALS->[3] $VALS->[4]";
                push( @DADDR, $NOMDEST );
                push( @DADDR, $VALS->[5] );
                push( @DADDR, "$VALS->[6] $VALS->[7]" );
                push( @DADDR, $VALS->[8] );
                push( @DADDR, "$VALS->[9] $VALS->[10]" );

                @DADDR = user_cleanup_addr(@DADDR);
                $DOC->append_table( 'DADDR', @DADDR );

                # warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";

                $xNOMDEST = "$VALS->[3] $VALS->[4]";
                $DOC->append( 'xNOMDEST', $xNOMDEST );

                ######INDEXATION GED###################
                $GED->append( 'xNOMDEST', $xNOMDEST );    #-	Nom du destinataire
                $GED->append( 'xVILDEST', $VALS->[10] )
                  ;    #-	Ville du destinataire
                $GED->append( 'xCPDEST', oe_iso_country() . $VALS->[9] );

                my $valeur = $VALS->[11];
                $valeur =~ s/^\s+//; #supprime les espaces en début de chaine
                $valeur =~ s/\s+$//; #en fin de chaine
                $valeur =~ s/\s+/ /; #remplace des séries d'espaces par un seul
                my $AffParafD = $valeur;

                if ( $CODCOUR eq 'RESDCS' && $AffParafD eq '1' ) {
                    $DOC->append( 'PrafD',   1 );
                    $DOC->append( "ALDCpdf", "ALDC2015.pdf" );
                }
                $DOC->append('COURRIER');

            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";

        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }

    # $GED->append('xSOURCE', $TYPECTR); #	Domaine métier
    $GED->append( 'xSOURCE', 'BUR' );    #	Domaine métier
    $GED->append( 'xOWNER',  'BUR' );    # Emetteur

    $DOC->append('FINDOC');

    $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $xNOMDEST, $CODCOUR, $DateRD );

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";

    emit_Doc();
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub emit_Doc() {

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    $DOC->reset();
    $GED->reset();

    oe_compo_link($OPT_GED);
    1;
}

exit main(@ARGV);
