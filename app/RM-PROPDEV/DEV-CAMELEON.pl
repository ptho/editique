use utf8;
use strict;
use warnings;

# TODO remove TPS-CAMELEON and DEV-CAMELEON during next campaign...

use File::Basename;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Cameleon qw(cameleon_open cameleon_extract);
use Data::Dumper qw( Dumper );
use Time::localtime;

use Readonly;

Readonly my $CODE_CAMP_BASE_NOPI    => "GS1";
Readonly my $CODE_CAMP_EXPERTE_NOPI => "GS2";
Readonly my $SPACE                  => "\x20";
Readonly my $NB_GRTE_MAX            => 9;

# used for "empty" LaTeX values (bugware circumventing oEdtk::TexDoc limitations)

### Variables utilisées par le tracking
my ( $cpagence, $iddevis, $nomdest, $totalcotis, $cdecpgne, $entitedoc );
my $ACS           = 0;
my $profilJeune   = 0;
my $profilFamille = 0;
my $profilSenior  = 0;
my $anneeEffet    = 0;

sub getTrackingValues {
    my ($data) = @_;

    #warn "INFO : dump data ". Dumper($data) ."\n";
    $nomdest  = $data->{'Prct_nom'} . " " . $data->{'Prct_prenom'};
    $iddevis  = $data->{'Devis_num'};
    $cdecpgne = $data->{'Devis_code_campagne'} || "";
    $cpagence = $data->{'Agence_cp'};
}

sub clean_number {
    my ( $h, $key ) = @_;
    return $SPACE unless exists $h->{$key} && defined $h->{$key};
    my $res = $h->{$key};
    return $res
      if 0
      || $res =~ m{^ [0-9]+ (?:\.[0-9]+)? $}sx    # 1.23 1
      || $res =~ m{^           \.[0-9]+   $}sx    #  .23
      || 0;
    die "Bad number '$res' for key '$key'. Dumper: "
      . Dumper($res)
      . Dumper($h);
}

sub clean_string {
    my ( $h, $key ) = @_;
    return $SPACE unless exists $h->{$key} && defined $h->{$key};
    return $h->{$key};
}

sub clean_date {
    my ($str) = @_;
    $str =~ s{^0([2-9])}{$1}sx
      ; # 03 février 2017 -> 3 février 2017; 01 mars -> 01 mars (for lack or "premier")
    return $str;
}

sub trim {
    my ($str) = @_;
    $str =~ s{\s+}{\x20}gsx;
    $str =~ s{^\s+}{}sx;
    $str =~ s{\s+$}{}sx;
    return $str;
}

sub sorted_hash_dump {
    my ($h) = @_;
    my @res = ();
    foreach my $key ( sort keys %{$h} ) {
        push @res, "$key => '$h->{$key}'";
    }
    return join( "\n", @res, '' );
}

sub verify_FormuleAndOption {
    my ( $formule, $option ) = @_;
    my %H_NOPI = ();
    $H_NOPI{"Base"}{1}    = 0;
    $H_NOPI{"Base"}{2}    = 0;
    $H_NOPI{"Experte"}{1} = 0;
    $H_NOPI{"Experte"}{2} = 0;
    $H_NOPI{"Experte"}{3} = 0;
    if ( !exists( $H_NOPI{$formule}{$option} ) ) {
        return 1;
    }
    return 0;
}

sub prep_courrier {
    my ( $data, $doc ) = @_;

    # my $nom_prospect= $data->{'Prct_nom'};

    # Infos prospect
    $doc->append( 'prctPrenom',    $data->{'Prct_prenom'}        || "" );
    $doc->append( 'prctNom',       $data->{'Prct_nom'} );
    $doc->append( 'xNOMDEST',      $data->{'Prct_nom'} );
    $doc->append( 'prctCiv',       $data->{'Prct_civ'} );
    $doc->append( 'prctAdr',       $data->{'Prct_adr'}           || "" );
    $doc->append( 'cptAdr',        $data->{'complement_adresse'} || "" );
    $doc->append( 'prctEntreeBat', $data->{'Prct_EntreeBatRes'}  || "" );
    $doc->append( 'prctLieuDit',   $data->{'Prct_LieuDit'}       || "" );
    $doc->append( 'prctCP',        $data->{'Prct_cp'}            || "" );
    $doc->append( 'xCPDEST',       $data->{'Prct_cp'}            || "" );
    $doc->append( 'prctVille',     $data->{'Prct_ville'}         || "" );
    $doc->append( 'xVILDEST',      $data->{'Prct_ville'}         || "" );

    # Creation du tableau d'adresse destinataire
    my @tADRDEST = ();
    push( @tADRDEST,
            "$data->{'Prct_civ'} "
          . ( $data->{'Prct_prenom'} || "" )
          . " $data->{'Prct_nom'}" );
    push( @tADRDEST, $data->{'Prct_adr'}           || "" );
    push( @tADRDEST, $data->{'complement_adresse'} || "" );
    push( @tADRDEST, $data->{'Prct_EntreeBatRes'}  || "" );
    push( @tADRDEST, $data->{'Prct_LieuDit'}       || "" );
    push( @tADRDEST, "$data->{'Prct_cp'} $data->{'Prct_ville'}" );
    @tADRDEST = user_cleanup_addr(@tADRDEST);
    $doc->append_table( 'xADRLN', @tADRDEST );

    # Infos Devis
    $doc->append( 'devisNum', $data->{'Devis_num'} );
    $doc->append( 'xIDDEST',  $data->{'Devis_num'} );
    if ($ACS) {
        $doc->append( 'devisCode', "ACS" );
        $doc->append( 'xCLEGEDi',  "ACS" );
    }
    else {
        $doc->append( 'devisCode', $data->{'Devis_code_campagne'} || "" );
        $doc->append( 'xCLEGEDi',  $data->{'Devis_code_campagne'} || "" );
    }
    $doc->append( 'dateDeDevis', clean_date( $data->{'Devis_dt_creation'} ) );
    $doc->append( 'xCLEGEDii',   clean_date( $data->{'Devis_dt_creation'} ) );

    my @date_d_effet;
    my $codeCampTmp = substr( $data->{'Devis_code_campagne'}, 6, 3 );
    if (   $codeCampTmp eq 'CSS'
        || $codeCampTmp eq $CODE_CAMP_BASE_NOPI
        || $codeCampTmp eq $CODE_CAMP_EXPERTE_NOPI )
    {    # Complement de salaire
        die "No 'Date_effet_cs' found:\n" . sorted_hash_dump($data)
          unless exists $data->{'Date_effet_cs'};
        @date_d_effet = split( '/', $data->{'Date_effet_cs'} );
    }
    else {
        die "No 'date_effet':\n" . sorted_hash_dump($data)
          unless exists $data->{'date_effet'};
        $doc->append( 'dateEffet', $data->{'date_effet'} || " " );
        @date_d_effet = split( '/', $data->{'date_effet'} );
    }
    die "Bad dateEffet (no annee):\n" . sorted_hash_dump($data)
      unless defined $date_d_effet[2];
    $doc->append( 'anneeEffet', $date_d_effet[2] );
    $anneeEffet = $date_d_effet[2];

    # Infos entité et agence de référence
    $doc->append( 'agenceCP',    $data->{'Agence_cp'} );
    $doc->append( 'xIDEMET',     $data->{'Agence_cp'} );
    $doc->append( 'agenceVille', ucfirst( $data->{'Agence_ville'} ) );
    $doc->append( 'xNOM_AGENCE', $data->{'Agence_etabl_mere'} );
    
    $doc->append( 'agenceEtablMere',  $data->{'Agence_etabl_mere'} );
    $doc->append( 'agenceAdr',        $data->{'Agence_adr'} );
    $doc->append( 'agenceComplement', $data->{'Agence_complement'} || " " );
    $doc->append( 'xADRESSE_AGENCE',  $data->{'Agence_adr'} . ' - '.$data->{'Agence_cp'}. ' ' . ucfirst( $data->{'Agence_ville'} ));
    
    #Infos conseiller
    $doc->append( 'consPrenom', $data->{'Cons_prenom'} || " " );
    $doc->append( 'consNom',    $data->{'Cons_nom'}    || " " );
    if ( $data->{'Devis_code_campagne'} =~ m/-00$/ ) {
        $doc->append( 'NewEntete', 1 );
        if ( $data->{'Devis_code_campagne'} =~ m/MUT-00$/ ) {
            $doc->append( 'Rupte', 0 );
        }
        else {
            $doc->append( 'Rupte', 1 );
        }
    }
    else {
        $doc->append( 'NewEntete', 0 );
    }

    my ( $temp, $codeCampagne, $moisGratuits, $phase ) =
      split( '-', $data->{'Devis_code_campagne'} );
    $codeCampagne ||= "";
    if ( !defined($phase) ) {
        $phase = $moisGratuits;
    }    ### Pour être rétro-compatible avec les anciens codes courriers

    if (   $codeCampagne ne 'CSS'
        && $codeCampagne ne $CODE_CAMP_BASE_NOPI
        && $codeCampagne ne $CODE_CAMP_EXPERTE_NOPI )
    {
        $doc->append('DEBUTDOC');

        # Inclus ou non en-tête et pied de page
        if ( $data->{'aff_entete_pdp'} ne "0" ) {
            $doc->append( $ACS ? 'includeLogoMNTACS' : 'includeLogoET' );
        }
    }

    $doc->append( 'codeCampagne', $codeCampagne );

    # Suivant le type de lettre. Fait une lettre standard par défaut.
    if ($ACS) {
        $doc->append( 'typeContrat', '123santé' );
    }
    else { $doc->append( 'typeContrat', 'OSR' ); }

    if (   $codeCampagne eq 'CSS'
        || $codeCampagne eq $CODE_CAMP_BASE_NOPI
        || $codeCampagne eq $CODE_CAMP_EXPERTE_NOPI )
    {    # Complément de salaire
        if ( $codeCampagne eq $CODE_CAMP_EXPERTE_NOPI ) {
            $doc->append( 'indemnitesJournalieres',
                clean_number( $data, 'Devis_cs_couverture_ij' ) );
        }
        else {
            $doc->append( 'indemnitesJournalieres',
                clean_number( $data, 'Devis_cs_ij' ) );
        }
        $doc->append( 'capitalInvalidite',
            clean_number( $data, 'Devis_cs_montant_inval' ) );
        $doc->append( 'cotisationMensuelleTTC',
            clean_number( $data, 'Devis_Cotis_mens' ) )
          ;    # same case as existing parameter...
        $doc->append( 'dateDAdhesionSouhaitee',
            clean_string( $data, 'Date_effet_cs' ) );

        # avoids multiple spaces if empty/space-only prenom
        $doc->append(
            'consPrenomNom',
            trim(
                    clean_string( $data, 'Cons_prenom' )
                  . clean_string( $data, 'Cons_nom' )
            )
        );
        if (   $codeCampagne eq $CODE_CAMP_BASE_NOPI
            || $codeCampagne eq $CODE_CAMP_EXPERTE_NOPI )
        {
            die "No 'Devis_cs_formule' found:\n" . sorted_hash_dump($data)
              unless ( exists $data->{'Devis_cs_formule'} );
            die "No 'Devis_cs_option' found:\n" . sorted_hash_dump($data)
              unless ( exists $data->{'Devis_cs_option'} );
            my $formule = clean_string( $data, 'Devis_cs_formule' );
            my $option = clean_number( $data, 'Devis_cs_option' );
            die "The formule <<"
              . $formule
              . ">> does not match with option <<"
              . $option . ">> \n"
              unless ( verify_FormuleAndOption( $formule, $option ) == 0 );
            $doc->append( 'FormuleNOPI', $formule );
            $doc->append( 'OptionNOPI',  $option );
            if ( $codeCampagne eq $CODE_CAMP_EXPERTE_NOPI ) {
                $doc->append( 'prctTraitement',
                    clean_number( $data, 'Prct_traitement_brut' ) );
                $doc->append( 'prctPrimes',
                    clean_number( $data, 'Prct_primes' ) );
                $doc->append( 'tauxCotisation',
                    clean_number( $data, 'Devis_cs_somme_tx_cotisation' ) );
                $doc->append( 'tauxIndemniteInvalidite',
                    clean_number( $data, 'Devis_cs_tx_indem_inval' ) );
                $doc->append( 'tauxIndemniteInvaliditeRetraite',
                    clean_number( $data, 'Devis_cs_tx_indem_inval_retraite' ) );
                $doc->append( 'prcPrimesConserve',
                    clean_string( $data, 'Prct_primes_conservees' ) );
            }
        }
    }

  SWITCH: {
        $ACS == 1 && do { $doc->append("LetterACS"); last SWITCH; };
        $data->{'Entite_Nom'} eq "MUTACITE"
          && do { $doc->append("LetterMutacite"); last SWITCH; }; # Lettre Mutacite 1 2 3 Santé
                                                                  # Courriers hors campagne
        $codeCampagne eq 'HOS'
          && do { $doc->append("LetterHospitalisation"); last SWITCH; };
        $codeCampagne eq 'STD'
          && do { $doc->append("LetterStandardClassique"); last SWITCH; };
        $codeCampagne eq 'GMS'
          && ( $phase eq '00' || $phase eq '03' )
          && do { $doc->append("LetterMMS"); last SWITCH; };
        $codeCampagne eq 'PAR'
          && do { $doc->append("LetterParrainagePI"); last SWITCH; };

        # Courriers Campagne 2018 phase 1
        $codeCampagne eq 'STDCAMP'
          && $phase eq '01'
          && do { $doc->append("LetterStandardCampagnePI"); last SWITCH; };
        $codeCampagne eq 'GMS'
          && $phase eq '01'
          && do { $doc->append("LetterGMSPI"); last SWITCH; };

        # Complément de salaire
        $codeCampagne eq 'CSS'
          && do { $doc->append("LetterComplementSalaire"); last SWITCH; };

        # Garantie de salaire Base Campagne prévoyance
        $codeCampagne eq $CODE_CAMP_BASE_NOPI
          && $moisGratuits eq '01'
          && do {
            $doc->append("LetterGarantieSalaireNOPICampagneSante");
            last SWITCH;
          };

        # Garantie de salaire Experte prévoyance
        $codeCampagne eq $CODE_CAMP_EXPERTE_NOPI
          && $moisGratuits eq '01'
          && do {
            $doc->append("LetterGarantieSalaireNOPICampagneSante");
            last SWITCH;
          };

        # Garantie de salaire Base
        $codeCampagne eq $CODE_CAMP_BASE_NOPI
          && $moisGratuits eq '00'
          && do {
            $doc->append("LetterGarantieSalaireNOPI");
            last SWITCH;
          };

        # Garantie de salaire Experte
        $codeCampagne eq $CODE_CAMP_EXPERTE_NOPI
          && $moisGratuits eq '00'
          && do {
            $doc->append("LetterGarantieSalaireNOPI");
            last SWITCH;
          };
        $doc->append("LetterStandardClassique");
    }
}

sub is_prev {
    my ($code_campagne_arg) = @_;
    if (   substr( $code_campagne_arg, 6, 3 ) ne 'CSS'
        && substr( $code_campagne_arg, 6, 3 ) ne $CODE_CAMP_BASE_NOPI
        && substr( $code_campagne_arg, 6, 3 ) ne $CODE_CAMP_EXPERTE_NOPI )
    {
        return 0;
    }
    return 1;
}

sub prep_devis {
    my ( $data, $doc ) = @_;
    $doc->append('DEBUTDEVIS');
    $doc->append( 'numProp', $data->{'Num_proposition'} || "NONUMPROP" );

    my $montant = clean_number( $data, 'Devis_Cotis_mens' );
    $totalcotis = $montant
      ;  #$cotis car $montant ressert pour le calcul des montants intermediaires
    $doc->append( 'devisCotisMensuel', $montant );
    if ( exists $data->{'taxe_cotis_mens'} ) {
        $montant = clean_number( $data, 'taxe_cotis_mens' );
    }
    else {
        $montant = undef;
    }
    $doc->append( 'taxeCotisMensuel', $montant || "NOTAXECOTISMENS" );

    my $nombrePersonnes = 0;
    my $tabDoc          = oEdtk::TexDoc->new();
    my $lastGar         = "";
    for ( my $i = 1 ; $i <= $NB_GRTE_MAX ; $i++ ) {
        if ( exists( $data->{"Pers${i}_garantie"} ) ) {
            $tabDoc->append( 'PERSNOM',    $data->{"Pers${i}_nom"}    || " " );
            $tabDoc->append( 'PERSPRENOM', $data->{"Pers${i}_prenom"} || " " );
            $tabDoc->append( 'PERSDATENAIS',
                $data->{"Pers${i}_dt_naissance"} || " " );
            if ( $lastGar ne $data->{"Pers${i}_garantie"} ) {
                $tabDoc->append( 'PERSGAR', $data->{"Pers${i}_garantie"} );
                $lastGar = $data->{"Pers${i}_garantie"};
              SWITCH: {
                    $lastGar eq "Profil Jeune"
                      && do { $profilJeune = 1; last SWITCH; };
                    $lastGar eq "Profil Sénior"
                      && do { $profilSenior = 1; last SWITCH; };
                    $lastGar eq "Profil Senior"
                      && do { $profilSenior = 1; last SWITCH; };
                    $lastGar eq "Profil Famille"
                      && do { $profilFamille = 1; last SWITCH; };
                }
            }
            else {
                $tabDoc->append( 'PERSGAR', " " );
            }
            $nombrePersonnes++;

            $montant = clean_number( $data, "Pers${i}_Cotis" );
            $tabDoc->append( 'PERSCOTIS', $montant );
            $tabDoc->append( 'PERSROLE', $data->{"Pers${i}_role"} || "" );
            if ($ACS) {
                $tabDoc->append('TABLNACS');
            }
            else {
#				if($nombrePersonnes == 1) { $tabDoc->append('FIRSTTABLN') } else { $tabDoc->append('TABLN') };
                $tabDoc->append('FIRSTTABLN');
            }
        }
    }
    $doc->append( 'NBRPERSTAB', $nombrePersonnes );
    $doc->append( 'PERSGAR',    $data->{"Pers1_garantie"} );
    $doc->append( 'tabPers',    $tabDoc );

    if ($ACS) {
        $doc->append( 'devisCotisMensuelACS',
            clean_number( $data, 'Devis_Acs_Cotis_Mens' ) );
        $doc->append( 'aideACS',
            clean_number( $data, 'Devis_Acs_Montant_Aide_Annuel' ) );
        $doc->append( 'totalCotisACS',
            clean_number( $data, 'Devis_Acs_Cotis_Mens_Aide' ) );
        $doc->append('BEGINTABACS');
    }
    else {
        $doc->append('BEGINTAB');
    }

    for ( my $i = 1 ; exists( $data->{"texte_msg_01_MKG_${i}"} ) ; $i++ )
    {    #message 2 envoyé avant message 1 suite à sesame
        if ( $i == 1 ) {
            $doc->append('IfMessUnDeb');
        }
        $doc->append( 'texteMessageUn', $data->{"texte_msg_01_MKG_${i}"} );
        $doc->append('printMessageUn');
    }
    for ( my $i = 1 ; exists( $data->{"texte_msg_02_MKG_${i}"} ) ; $i++ )
    {    #message 2 envoyé avant message 1 suite à sesame
        if ( $data->{"texte_msg_02_MKG_${i}"} ne "" ) {
            if ( $i == 1 ) {
                $doc->append('IfMessDeuxDeb');
            }
            $doc->append( 'texteMessageDeux',
                $data->{"texte_msg_02_MKG_${i}"} );
            $doc->append('printMessageDeux');
        }
    }

    $doc->append('FINDOC');
}

sub prep_tab {
    my ( $data, $doc ) = @_;

    # 	warn "INFO : dump data ". Dumper($data) ."\n";
    # 	system('pause');
    $doc->append( 'DataPBYear',   $data->{'Data_PB_year'} );
    $doc->append( 'DataPBOffre',  $data->{'Data_offre'} );
    $doc->append( 'DataColUn',    $data->{'Data_Col01'} );
    $doc->append( 'DataColDeux',  $data->{'Data_Col02'} );
    $doc->append( 'DataColTrois', $data->{'Data_Col03'} );

    # Infos entité et agence de référence
    $doc->append( 'agenceCP', $data->{'Agence_cp'} );
    $doc->append( 'xIDEMET',  $data->{'Agence_cp'} );
    $doc->append( 'agenceVille',
        ( exists $data->{'Agence_ville'} )
        ? ucfirst( $data->{'Agence_ville'} )
        : $SPACE );

    $doc->append( 'agenceEtablMere',  $data->{'Agence_etabl_mere'} );
    $doc->append( 'agenceAdr',        $data->{'Agence_adr'} );
    $doc->append( 'agenceComplement', $data->{'Agence_complement'} || " " );

    $doc->append('DebutTabPrest');
    my $i            = 1;
    my $lastTitreRub = '';
    my $currenti     = sprintf( "%02d", $i )
      ; # sprintf pour gérer les entiers avec 0 devant - parce que PB04 et pas PB4 -

    while ( exists( $data->{"Data_PB${currenti}"} ) ) {
        if ( $data->{"Data_PB${currenti}"} =~ m/Formule/ ) {
            $data->{"Data_PB${currenti}"} =~ s/Formule//;

            # 			warn "INFO Data_PB : ".$data->{"Data_PB${currenti}"}."\n";
            $doc->append( 'TitreFormule', " " . $data->{"Data_PB${currenti}"} );
            $doc->append( 'DataPBTitreRub', "Formule" );
            $doc->append('TabRub');
        }
        else {
            $doc->append( 'DataPBTitreRub', $data->{"Data_PB${currenti}"} );
        }

        #		$doc->append('TabRub');
        my $j = 1;
        my $currentj = sprintf( "%02d", $j );
        my $nextcol =
          -1;    #incrémenté s'il y a plusieurs lignes pour col1 col2 et col3
        while ( exists( $data->{"Data_PB${currenti}_${currentj}"} ) ) {
            $doc->append( 'DataPBSousRub',
                $data->{"Data_PB${currenti}_${currentj}"} );
            ###Impression de la premiere ligne
            if ( exists( $data->{"Data_PB${currenti}_${currentj}_Col01_00"} ) )
            {

                #Si Col01_00 alors il n'y aura pas de col01_01, col01_02 etc ...
                $doc->append( 'DataPBColUn',
                    $data->{"Data_PB${currenti}_${currentj}_Col01_00"} );
                $doc->append( 'DataPBColDeux',
                    $data->{"Data_PB${currenti}_${currentj}_Col02_00"} );
                $doc->append( 'DataPBColTrois',
                    $data->{"Data_PB${currenti}_${currentj}_Col03_00"} );
            }
            else {
                $doc->append( 'DataPBColUn',
                    $data->{"Data_PB${currenti}_${currentj}_Col01_01"} );
                $doc->append( 'DataPBColDeux',
                    $data->{"Data_PB${currenti}_${currentj}_Col02_01"} );
                $doc->append( 'DataPBColTrois',
                    $data->{"Data_PB${currenti}_${currentj}_Col03_01"} );
                $nextcol = 2;
            }
            ### Pour ne pas afficher le cas où dans la partie DIVERS, les trois colonnes affichent "-"
            if (
                $data->{"Data_PB${currenti}"} ne "DIVERS"
                || (   $data->{"Data_PB${currenti}_${currentj}_Col01_00"} ne "-"
                    || $data->{"Data_PB${currenti}_${currentj}_Col02_00"} ne "-"
                    || $data->{"Data_PB${currenti}_${currentj}_Col03_00"} ne
                    "-" )
              )
            {
                if ( $lastTitreRub ne $data->{"Data_PB${currenti}"} ) {
                    $lastTitreRub = $data->{"Data_PB${currenti}"};
                    $doc->append('TabRub');
                }
                $doc->append('TabLineFirst');
            }

            ###Impression éventuelle de la 2eme ligne description, eventuellement avec des infos dans les colonnes
            if ( exists( $data->{"Data_PB${currenti}_${currentj}_desc"} ) ) {
                $doc->append( 'DataPBDesc',
                    $data->{"Data_PB${currenti}_${currentj}_desc"} );
                if ( $nextcol == 2 ) {
                    $doc->append( 'DataPBColUn',
                        $data->{"Data_PB${currenti}_${currentj}_Col01_02"} );
                    $doc->append( 'DataPBColDeux',
                        $data->{"Data_PB${currenti}_${currentj}_Col02_02"} );
                    $doc->append( 'DataPBColTrois',
                        $data->{"Data_PB${currenti}_${currentj}_Col03_02"} );
                }
                else
                { # si nextcol à 0, alors il n'y a qu'une ligne de col1, col2 et col3
                        # on les met à vide pour l'impression de \TabLineDesc
                    $doc->append( 'DataPBColUn',    "" );
                    $doc->append( 'DataPBColDeux',  "" );
                    $doc->append( 'DataPBColTrois', "" );
                }
                $doc->append('TabLineDesc');
                $nextcol++;   # On incrémente parce qu'on affiche une ligne ici
            }
            ### Impression éventuelle des lignes supplémentaires
            $nextcol = sprintf( "%02d", $nextcol );
            while (
                exists(
                    $data->{"Data_PB${currenti}_${currentj}_Col01_${nextcol}"}
                )
              )
            {
                $doc->append( 'DataPBColUn',
                    $data->{"Data_PB${currenti}_${currentj}_Col01_${nextcol}"}
                );
                $doc->append( 'DataPBColDeux',
                    $data->{"Data_PB${currenti}_${currentj}_Col02_${nextcol}"}
                );
                $doc->append( 'DataPBColTrois',
                    $data->{"Data_PB${currenti}_${currentj}_Col03_${nextcol}"}
                );
                $doc->append('TabLineSimple');
                $nextcol++;
                $nextcol = sprintf( "%02d", $nextcol );
            }
            $j++;
            $currentj = sprintf( "%02d", $j );
        }
        $i++;
        $currenti = sprintf( "%02d", $i );
    }
    $doc->append('FinDuDoc');
}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    oe_new_job('--index');
    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'DEVI' );
    my $TRK;

    my $cpe =
      'CPE.wsMNT_COH/CP/cpSante.gp.wsMNT_COH/SBL/sblTotal.wsMNT_COH/SBL/';
    my $xml = cameleon_open( \*IN );

    # binmode STDIN, ':utf8';
    my $data = cameleon_extract( $xml, $cpe . "sblEdDevis1" );

    # print STDERR Dumper($data);
    unless ( keys %$data ) {

        # Devis externe (ex DEV-CAMELEON)
        warn "\n INFO : TEST PASSAGE BOUCLE DEVIS EXTERNE \n ";
        $cpe = 'CPE.wsMNT/CP/cp123Sante.gp.wsMNT/SBL/sblTotal.wsMNT/SBL/';
        my $data = cameleon_extract( $xml, $cpe . "sblEdDevis" );

        unless ( keys %$data ) {
            $cpe =
              'CPE.wsMNT/CP/cpPrevoyanceWeb.gp.wsMNT/SBL/sblTotal.wsMNT/SBL/';
            $data = cameleon_extract( $xml, $cpe . "sblEdDevis" );
        }
        $entitedoc = $data->{'Entite_Nom'};

        # my $nom_prospect= $data->{'Prct_nom'};
        if ( $entitedoc eq "MNT" ) {
            $doc->append('allMacroMNT');

        }
        else {
            $doc->append('allMacroMUTA');
        }
        $doc->append('allMacroExterne');

        ### S'il s'agit d'une offre ACS
        if ( $data->{'Type_Offre'} eq 'siAcsOffre' ) {
            $ACS = 1;
            $doc->append( 'typeContrat', 'ACS' );
        }

        $doc->append( 'xSOURCE', 'coheris' );
        $doc->append( 'xOWNER',  'coheris' );

        $TRK = oEdtk::Tracking->new(
            $argv[1],
            edmode => 'Batch',
            entity => oe_corporation_set($entitedoc),
            user   => 'Web Editiq',
            keys => [ 'IDDEVIS', 'CPAGENCE', 'NOMDEST', 'CDECPGNE', 'COTISMNT' ]
        );
        $doc->append(oe_corporation_tag);

        # XXX Create document...
        $. = 2;    # XXX OMG ITS A HACK
        getTrackingValues($data);
        my $nomScript = basename($0);
        if ( $nomScript eq "DEV-CAMELEON.pl" ) {
            prep_courrier( $data, $doc );
            if ( !is_prev( $data->{'Devis_code_campagne'} ) ) {
                prep_devis( $data, $doc );
            }
            open my $FILENAMEFILE, '>', 'filename' or die $!;
            print $FILENAMEFILE "Le devis $entitedoc de $nomdest";
            close $FILENAMEFILE;
        }
        elsif ( $0 =~ /TPS-CAMELEON/ ) {    # TPS-CAMELEON
                # Inclus ou non en-tête et pied de page

	    $doc->append('siegeEmpty');
            if ( $data->{'aff_entete_pdp'} ne "0" ) {
                $doc->append( $ACS ? 'includeLogoMNTACS' : 'includeLogoET' );
            }
            $data = cameleon_extract( $xml, $cpe . "sblEdTabPrest" );
            if   ($ACS) { $doc->append( 'typeContrat', 'ACS' ); }
            else        { $doc->append( 'typeContrat', '123santé' ); }
            if ( $data->{'Type_offre'} eq "OSR" ) {
                $doc->append( 'typeContrat', 'OSR' );
            }
            prep_tab( $data, $doc );
            open my $FILENAMEFILE, '>', 'filename' or die $!;
            print $FILENAMEFILE "Le tableau des prestations $entitedoc";
            close $FILENAMEFILE;
        }
        else { die "ERROR: bad input file:\n" . sorted_hash_dump($data); }

    }
    else {    # Devis interne
        warn "INFO : TEST PASSAGE BOUCLE DEVIS INTERNE \n ";
        $entitedoc = $data->{'Entite_Nom'};

        # my $nom_prospect= $data->{'Prct_nom'};

        if ( $entitedoc eq "MNT" ) {
            $doc->append('allMacroMNT');
        }
        else {
            $doc->append('allMacroMUTA');
        }
        $doc->append('allMacroInterne');

        ### S'il s'agit d'une offre ACS
        if ( $data->{'Type_Offre'} eq 'siAcsOffre' ) {
            $ACS = 1;
            $doc->append( 'typeContrat', 'ACS' );
        }

        $doc->append( 'xSOURCE', 'coheris' );
        $doc->append( 'xOWNER',  'coheris' );

        $TRK = oEdtk::Tracking->new(
            $argv[1],
            edmode => 'Batch',
            entity => oe_corporation_set($entitedoc),
            user   => 'Web Editiq',
            keys => [ 'IDDEVIS', 'CPAGENCE', 'NOMDEST', 'CDECPGNE', 'COTISMNT' ]
        );
        $doc->append(oe_corporation_tag);

        # XXX Create document...
        $. = 2;    # XXX OMG ITS A HACK

        getTrackingValues($data);
        prep_courrier( $data, $doc );
        if (   ( !exists $data->{'Devis_code_campagne'} )
            || ( !defined $data->{'Devis_code_campagne'} )
            || !is_prev( $data->{'Devis_code_campagne'} ) )
        {          # not complément de salaire (specific)
            warn "Devis_code_campagne: "
              . (
                ( exists $data->{'Devis_code_campagne'} )
                ? $data->{'Devis_code_campagne'}
                : "(undef)"
              );
            for ( my $compteur = 1 ; defined( $data->{'Num_proposition'} ) ; ) {
                prep_devis( $data, $doc );
                $data =
                  cameleon_extract( $xml, $cpe . "sblEdTabPrest$compteur" );
                prep_tab( $data, $doc );
                $data = cameleon_extract( $xml,
                    $cpe . "sblEdDevis" . ( ++$compteur ) );
            }
        }
    }

    $TRK->track( 'Doc', 1, $iddevis, $cpagence, $nomdest, $cdecpgne,
        $totalcotis );

    oe_print_to_output_file($doc);

    my $docFicheProfil = oEdtk::TexDoc->new();
    if ($profilJeune)   { $docFicheProfil->append("addFicheProfilJeune"); }
    if ($profilFamille) { $docFicheProfil->append("addFicheProfilFamille"); }
    if ($profilSenior)  { $docFicheProfil->append("addFicheProfilSenior"); }
    oe_print_to_output_file($docFicheProfil);

    oe_compo_link();
    $TRK->track( 'W', 1, '', '', '', '', 'Fin de traitement' );
    return 0;
}

exit main(@ARGV);
