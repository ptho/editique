#!/usr/bin/perl
#******************************************************************************
#------------------------------ LIBRAIRIES ------------------------------------
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;
use Readonly;

#******************************************************************************
#-------------------------- VARIABLES TEMPORAIRES -----------------------------
my ( $TRK, $DOC, $GED, $VALS );
my $CODCOUR;    #CODE COURRIER
my @DADDR;
my @TabPIECES;
my $AGENCE;
my $CIV;
my $CIVILITE;
my $NOMDEST;
my $NUMCTR;
my $NUMABA;
my $PIECES;
my $xNOMDEST;
my $GESTIONNAIRE;
my $EMETTEUR;
my $CODCENTRE;
my $OPT_GED;
my $NUMREF;
my $POPULATION = "";
my %H_COUR     = ();
my %H_PIECES   = ();

#******************************************************************************
#-------------------- INITIALISATION VARIABLES CONSTANTES ---------------------
Readonly my $LIB_PCMOP => 'PCMOP';
Readonly my $LIB_PCMOS => 'PCMOS';
Readonly my $LIB_PCRES => 'PCRES';
Readonly my $LIB_PCAJB => 'PCAJB';
Readonly my $LIB_PCMIC => 'PCMIC';

Readonly my $LIB_SEPA     => 'NUMSEPA';
Readonly my $LIB_QUEST    => 'NUMQUEST';
Readonly my $LIB_BULMODIF => 'NUMBULMODIF';
Readonly my $LIB_MNTRC    => 'MNT-RC';
Readonly my %H_LIBPDF =>
  ( 0 => $LIB_QUEST, 1 => $LIB_SEPA, 3 => $LIB_BULMODIF );

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMCTR', 'NUMABA', 'xIDEMET', 'CODECOURRIER', 'PIECES' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    #$E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # $E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    #$E7->debug();
    $E7->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'Z1' => $Z1
    );

    $DOC->append( 'xTYPDOC', 'CREA' );    ### Type de document
    initialiseCourrierTraite();           # ------- INITIALISATION DES COURRIERS
    my $id;
    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {

            #warn "INFO : dump E1 " . Dumper($VALS) . "\n";
            $DOC->append( 'DEBUT', '' );
            prepE1($VALS);
        }
        if ( $id eq 'E2' ) {

            # warn "INFO : dump E2 ". Dumper($VALS) ."\n";
            prepE2($VALS);
        }
        if ( $id eq 'E7' ) {

            # warn "INFO : dump E7 ". Dumper($VALS) ."\n";
            gestionAdrE7($VALS);
        }
        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }
        if ( $id eq 'OPTION' ) {

            # warn "INFO : dump OPTIONU5064 ". Dumper($VALS) ."\n";
            my $NbPIECES = recuperPieces($VALS);
            if ( $NbPIECES == 0 ) {
                die
"\nERROR:Impossible de generer un courrier si le nombre de pieces demande est 0\n\n";
            }
            $DOC->append( 'NbPIECES', $NbPIECES );
            courrier();
        }
    }

    $DOC->append('BASDEPAGE');
    $GED->append( 'xSOURCE', 'BUR' );    #-	Domaine métier
    $GED->append( 'xOWNER',  'BUR' );
    $DOC->append('FINDOC');

    $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $AGENCE, $CODCOUR, $PIECES );

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";

    emit_Doc();

    return 0;
}

###############################################################################
#-------------------------- RECUPERATION PIECES -------------------------------
sub recuperPieces {
    my ($VALS)   = @_;
    my $NbPIECES = 0;
    my $NumPIECE = 'A';
    my $iterpc   = 0;
    my $valeur   = "";

    if ( $VALS->[0] eq 'U5064 ' ) {
        $POPULATION = $VALS->[2];
        $POPULATION =~ s/^\s+//;
        $POPULATION =~ s/\s+$//;
        $NUMREF = $VALS->[3];
        $DOC->append( 'POP',    $POPULATION );
        $DOC->append( 'NUMREF', $NUMREF );       # NUMERO REFERENCE
        my $i = 4;
        while ( $VALS->[$i] ) {
            $valeur = $VALS->[$i];
            $valeur =~ s/^\s+//;    #supprime les espaces en début de chaine
            $valeur =~ s/\s+$//;    #en fin de chaine
            $valeur =~ s/\s+/ /;    #remplace des séries d'espaces par un seul
            if ( $valeur ne '' && $valeur ne ' ' ) {
                push( @TabPIECES, $valeur );
                $H_PIECES{$iterpc} = $valeur;
                if ( $valeur eq '1' || $valeur eq '0' ) {
                    $DOC->append( 'PIECE' . $NumPIECE, $valeur );
                    $PIECES .= $valeur . '/';
                }
                else {
                    $DOC->append( 'PIECE' . $NumPIECE, $VALS->[$i] );
                    $PIECES .= 'CHAMPS LIBRE';
                }

                $NumPIECE++;
                $iterpc++;
                if ( $valeur ne '0' ) {
                    $NbPIECES++;
                }
            }
            $i++;
        }
    }

    return $NbPIECES;
}

###############################################################################
#----------------------- PREPARATION COURRIER ---------------------------------
sub courrier {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT SELON LE CODE COURRIER
    # - Affichage d'un paragraphe spécifique relatif à un code courrier
    # - Affichage titre du document spécifique relatif à un code courrier

    #CODE COURRIER
    ##warn "INFO : dump TabPIECES \n". Dumper(@TabPIECES) ."\n";
    my $pageVide = 0;

    if ( exists( $H_COUR{$CODCOUR} ) ) {
        if (
            (
                !exists( $H_COUR{$CODCOUR}{$CODCENTRE} )
                && $H_COUR{$CODCOUR}{$LIB_MNTRC} == 1
            )
            || ( exists( $H_COUR{$CODCOUR}{$CODCENTRE} )
                && $H_COUR{$CODCOUR}{$CODCENTRE} == -1 )
          )
        {
            die "\nERROR:Impossible de generer un courrier "
              . $CODCOUR
              . " pour un centre de gestion "
              . $CODCENTRE . "\n\n";
        }

        foreach my $indice ( keys(%H_LIBPDF) ) {
            my $libPDF = $H_LIBPDF{$indice};
            if (   exists( $H_PIECES{$indice} )
                && $H_PIECES{$indice} == 1
                && $H_COUR{$CODCOUR}{$libPDF} == 1 )
            {
                $DOC->append( $libPDF, 1 );
                $pageVide = 1;
            }
            else {
                $DOC->append( $libPDF, 0 );
            }
        }

    }
    else {
        die "\nERROR:CODE COURRIER INCONNU\n\n";
    }
    $DOC->append( 'CODCOUR',  $CODCOUR );
    $DOC->append( 'PAGEVIDE', $pageVide );
    $DOC->append('COURRIER');

    return 0;
}

###############################################################################
#----------------------------- PREPARATION E1 ---------------------------------
sub prepE1() {
    my ($VALS) = @_;
    $NOMDEST =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
    #####GESTION DE LA CIVILITE ENTRE LE TIERS ET L'ADHERENT#########
    $EMETTEUR = $VALS->{'ODERCOD'};
    $CIVILITE = $VALS->{'LICCODC'};
    my $civDest = $VALS->{'PENOCOD'};
    $DOC->append( 'civDest', $civDest );

    if ( $EMETTEUR eq '' ) {
        $CIV = 0;
    }
    else {
        $CIV = 1;
    }

    $DOC->append( 'CIV',      $CIV );
    $DOC->append( 'CIVILITE', $CIVILITE );

    $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
    $DOC->append( 'xNOMDEST', $xNOMDEST );

    ################CENTRE DE GESTION###############################
    $CODCENTRE = $VALS->{'ODCGCOD'};
    $DOC->append( 'CODCENTRE', $CODCENTRE );

    ################NOM EMETTEUR#########################
    $AGENCE = $VALS->{'PENO027'} . " " . $VALS->{'PNTR028'};
    $AGENCE =~ s/\s+$//;
    $DOC->append( 'AGENCE',  $AGENCE );
    $DOC->append( 'xIDEMET', $AGENCE );

    ######INDEXATION GED###################
    $GED->append( 'xOWNER',   'BUR' );                 #-	Emetteur
    $GED->append( 'xNOMDEST', $xNOMDEST );             #-	Nom du destinataire
    $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} );    #-	Ville du destinataire
    $GED->append( 'xCPDEST', oe_iso_country() . $VALS->{'CODCDXC'} );

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";

    return 0;
}

###############################################################################
#----------------------------- PREPARATION E2 ---------------------------------
sub prepE2() {
    my $DATEACT =
      convertDate( $VALS->{'DATEDTx'} );    #Date du Traitement d'Edition
    $DOC->append( 'DATEACT', $DATEACT );

    #########NUMERO CONTRAT INDIVIDUEL
    $NUMCTR = $VALS->{'ACH1NUM'};
    $DOC->append( 'NUMCTR', $NUMCTR );

    ######INDEXATION GED###################
    $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
      ;                                     #-	Numéro de contrat collectif
    $GED->append( 'xCLEGEDi', $NUMCTR );    #-	Numéro de contrat individuel

    $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
    $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
         # warn "INFO GESTION : $GESTIONNAIRE";
    $GED->append( 'xCLEGEDiv', $DATEACT );    #Date du Traitement d'Edition

    #CODE COURRIER
    $CODCOUR = $VALS->{'CDCOURx'};

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
    return 0;
}

###############################################################################
#----------------------------- PREPARATION E7 ---------------------------------
sub gestionAdrE7 {
    my ($VALS) = @_;
    ############BLOC ADRESSE DESTINATAIRE##################
    my @DADDR = ();
    push( @DADDR, $NOMDEST );
    push( @DADDR, $VALS->{'PNTREMx'} );
    push( @DADDR, $VALS->{'CPLADRx'} );
    push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
    );
    push( @DADDR, $VALS->{'BOIPOSL'} );
    push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $VALS->{'LICCODP'} );
    $VALS->{'LICCODP'} =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @DADDR, $VALS->{'LICCODP'} );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'DADDR', @DADDR );

    ############BLOC ADRESSE EMETTEUR##################
    $DOC->append( 'ADRESSE',
"$VALS->{'PEAD016'} $VALS->{'PEAD017'} $VALS->{'PELI018'} $VALS->{'LIBV019'}"
    );
    $DOC->append( 'LOCALITE', $VALS->{'NOMC024'} );
    $DOC->append( 'CPLADRE',  $VALS->{'CPLA015'} );
    $DOC->append( 'CODPOS',   $VALS->{'CODC023'} );
    $DOC->append( 'BOITPOS',  $VALS->{'BOIP021'} );
    return 0;
}

###############################################################################
#----------------- INITIALISATION CODES COURRIERS TRAITES ---------------------
sub initialiseCourrierTraite {
    initHCodeCour( $LIB_PCMIC, 0, 0, 0, 0 )
      ; #----------------- TOUT CODECENTRE, PAS QUESTIONNAIRE,NI SEPAS ,NI BULLMODIF
    initHCodeCour( $LIB_PCRES, 0, 0, 0, 0 )
      ; #----------------- TOUT CODECENTRE, PAS QUESTIONNAIRE,NI SEPAS ,NI BULLMODIF
    initHCodeCour( $LIB_PCAJB, 0, 0, 0, 1 )
      ; #----------------- TOUT CODECENTRE, PAS QUESTIONNAIRE,NI SEPAS , AVEC BULLMODIF
    initHCodeCour( $LIB_PCMOS, 1, 0, 1, 0 )
      ; #----- SEULEMENT CODECENTRE MNT-RC, PAS QUESTIONNAIRE,AVEC SEPAS, PAS BULLMODIF
    initHCodeCour( $LIB_PCMOP, -1, 1, 1, 0 )
      ; #----- EXLCUSION CODECENTRE MNT-RC, AVEC QUESTIONNAIRE,AVEC SEPAS, PAS BULLMODIF
    return 0;
}

###############################################################################
#--------------------- INITIALISATION D UN COURRIER ---------------------------
sub initHCodeCour {
    my ( $LibProd, $initRc, $initQuest, $initSepa, $initBul ) = @_;
    $H_COUR{$LibProd}{$LIB_MNTRC}    = $initRc;
    $H_COUR{$LibProd}{$LIB_QUEST}    = $initQuest;
    $H_COUR{$LibProd}{$LIB_SEPA}     = $initSepa;
    $H_COUR{$LibProd}{$LIB_BULMODIF} = $initBul;
    return 0;
}

###############################################################################
#-------------------- CONVERSION DE DATE EN (JJ/MM/AAAA) ----------------------
sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

###############################################################################
#--------------------------- EMISSION DU DOCUMENT -----------------------------
sub emit_Doc {

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    $DOC->reset();
    $GED->reset();

    oe_compo_link($OPT_GED);
    return 0;
}
###############################################################################
#------------------------ APPEL FONCTION PRINCIPALE ---------------------------
exit main(@ARGV);
