#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;

my ( $TRK, $DOC, $GED, $VALS );
my $TYPECOUR;
my @DADDR;
my $CIV;
my $CIVILITE;
my $NUMCTR;
my $NUMABA;
my $NbBENEF = 0;
my $NOMDEST;
my $xNOMDEST;
my $xIDEMET;
my $GESTIONNAIRE;
my $EMETTEUR;
my $CODCENTRE;
my $OPT_GED;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMCTR', 'NUMABA', 'xIDEMET', 'TYPECOURRIER', 'NbBENEF' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # $E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # $E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    # $E7->debug();
    $E7->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();

    $DOC->append( 'xTYPDOC', 'CBDC' );
    my $LISTE_BENEF = oEdtk::TexDoc->new();

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'Z1' => $Z1
    );
    $P->set_motif_to_denormalized_split('\x{01}');

    my $id;
    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n";

            $DOC->append( 'DEBUT', '' );
            my $civDest = $VALS->{'PENOCOD'};
            $DOC->append('civDest',$civDest);
            $NOMDEST = (
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
            );
            #####GESTION DE LA CIVILITE ENTRE LA PARTIE TIERS ET LA PARTIE ADHERENT.#########
            $xIDEMET == $VALS->{'ODERCOD'};
            $EMETTEUR = $VALS->{'ODERCOD'};
            $CIVILITE = $VALS->{'LICCODC'};

            if ( $EMETTEUR eq '' ) {
                $CIV      = 0;
                $EMETTEUR = "ER";
            }
            else {
                $CIV = 1;

                #CODE ENTITE DE RATTACHEMENT
                $DOC->append( 'EMETTEUR', '- ' . $EMETTEUR . ' -' );
            }

            $DOC->append( 'CIV',      $CIV );
            $DOC->append( 'CIVILITE', $CIVILITE );

            ################CENTRE DE GESTION###############################
            $CODCENTRE = $VALS->{'ODCGCOD'};

            if ( $CODCENTRE eq 'MNT-RC' ) {

#Blocage des contrats santé (centre de gestion MNT-RC) pour les courriers BENDCC et BENDCI
                die
"\nERROR:Impossible de generer un courrier BENDCC ou BENDCI pour les contrats santé (centre de gestion "
                  . $CODCENTRE . ")\n\n";
            }

            $DOC->append('BASDEPAGE');

            ##################Nom du destinataire#############################
            $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            ######INDEXATION GED###################
            $GED->append( 'xOWNER',   'BUR' );            #-	Emetteur
            $GED->append( 'xNOMDEST', $xNOMDEST );        #-	Nom du destinataire
            $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
              ;    #-	Ville du destinataire

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";

        }

        if ( $id eq 'E2' ) {

            # warn "INFO : dump E2 ". Dumper($VALS) ."\n";

            my $DATEACT =
              convertDate( $VALS->{'DATEDTx'} );   #Date du Traitement d'Edition
            $DOC->append( 'DATEACT', $DATEACT );

            #NUMERO CONTRAT INDIVIDUEL
            $NUMCTR = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMCTR', $NUMCTR );

            #CODE COURRIER
            $TYPECOUR = $VALS->{'CDCOURx'};

            ######INDEXATION GED###################
            $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
              ;    #-	Numéro de contrat collectif
            $GED->append( 'xCLEGEDi', $NUMCTR )
              ;    #-	Numéro de contrat individuel

            $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
            $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
            $GED->append( 'xCLEGEDiv', $DATEACT ); #Date du Traitement d'Edition

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E7' ) {
            ############BLOC ADRESSE DESTINATAIRE##################
            my @DADDR = ();
            push( @DADDR, $NOMDEST );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
            );
            push( @DADDR, $VALS->{'BOIPOSL'} );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $VALS->{'LICCODP'} );
            $VALS->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $DOC->append_table( 'DADDR', @DADDR );

            # warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";
        }

        if ( $id eq 'OPTION' ) {

            # warn "INFO : dump OPTIONU5064 ". Dumper($VALS) ."\n";
            # warn "INFO : TYPECOUR - ". $TYPECOUR ."\n";

            my $VERIF = substr $VALS->[0], 0, 5;
            if ( $VERIF eq 'U5064' ) {
                my $i;
                if ( $TYPECOUR eq 'BENDCC' ) {
                    $DOC->append( 'LETTRE', {'BENDCC'} );
                    $DOC->append( 'EMPLOYEUR', $VALS->[1] );
                    $i = 2;
                }
                elsif ( $TYPECOUR eq 'BENDCI' ) {
                    $DOC->append( 'LETTRE', {'BENDCI'} );
                    $i = 1;
                }
                else {
                    die "\nERROR:CODE COURRIER INCONNU\n\n";
                }

                my ( $j, $k ) = ( $i + 1, $i + 2 );
                my $numLigne = 0;

                while ( $VALS->[$k] ) {
                    $numLigne++;

         # warn "INFO : LIGNE ".$numLigne." VALEURS ".$i." - ".$j." - ".$k."\n";

                    my $nom     = suppEspace( $VALS->[$i] );
                    my $ordre   = suppEspace( $VALS->[$j] );
                    my $capital = suppEspace( $VALS->[$k] );

                    my $vide = '';
                    if ( $nom eq '' )   { $vide .= " Bénéficiaire -"; }
                    if ( $ordre eq '' ) { $vide .= " Ordre de priorité -"; }
                    if ( $capital eq '' ) {
                        $vide .= " Répartition capital -";
                    }

                    if ( $vide eq '' ) {
                        $LISTE_BENEF->append( "NOMBENEF", $VALS->[$i] );
                        $LISTE_BENEF->append( "ORDBENEF", $VALS->[$j] );
                        $LISTE_BENEF->append( "CAPBENEF", $VALS->[$k] );
                        $LISTE_BENEF->append("EditLigneBenef");
                        $NbBENEF++;

                    }
                    elsif ( $nom eq '' && $ordre eq '' && $capital eq '' ) {
                        warn "INFO : LES VALEURS "
                          . $i . " - "
                          . $j . " - "
                          . $k
                          . " SONT VIDES. \n LA LIGNE NE SERA PAS AFFICHEE DANS LE COURRIER\n";
                    }
                    else {
                        die
"\n ERROR: vous avez aoublié de remplir les champs suivants pour le bénéficiaire numéro "
                          . $numLigne . " : "
                          . $vide . "\n\n";
                    }

                    $i += 3;
                    $j += 3;
                    $k += 3;
                }

                if ( $NbBENEF == 0 ) {
                    die
"\n ERROR: IMPOSSIBLE DE GENERER UN COURRIER SI LA LISTE DES BENEFICIAIRES EST VIDE \n\n";
                }
                else {
                    $DOC->append( 'ListeBenef', $LISTE_BENEF );
                    $DOC->append( 'NbBENEF',    $NbBENEF );
                }

                $DOC->append('COURRIER');
            }
        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }

    $GED->append( 'xSOURCE', 'BUR' );    #-	Domaine métier
    $GED->append( 'xOWNER',  'BUR' );
    $DOC->append('FINDOC');

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";
    # warn "INFO : dump GED ". Dumper($GED) ."\n";

    $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $xIDEMET, $TYPECOUR, $NbBENEF );

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    $DOC->reset();
    $GED->reset();

    oe_compo_link($OPT_GED);
    return 0;
}

sub convertDate {
    my ($refVar) = @_;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub suppEspace {
    my ($valeur) = @_;
    $valeur =~ s/^\s+//;    #supprime les espaces en début de chaine
    $valeur =~ s/\s+$//;    #supprime les espaces en fin de chaine
    $valeur =~ s/\s+//g;    #supprime des séries d'espaces
        # $valeur = substr $valeur, 0, -1; #supprime le dérnier caractère
        # warn "INFO : ". $valeur ."\n";
    return $valeur;
}

exit main(@ARGV);
