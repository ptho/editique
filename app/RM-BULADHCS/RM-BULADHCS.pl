use utf8;
use strict;
use warnings;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Data::Dumper;
use Readonly;

Readonly my $NULL_VALUE => "\x20"
  ; # bugware for TexDoc: calls a macro instead of defining it with an empty value
Readonly my $MAPPING => [

    # TeX tag base name      # XML tag name              # comment / PDF marking
    [ 'Nom',           'pers01_nom',      'A1 Nom' ],
    [ 'Prenoms',       'pers01_prenom',   'A2 Prénoms' ],
    [ 'NomNaissance',  'pers01_nom_jf',   'A3 Nom de jeune fille' ],
    [ 'DateNaissance', 'pers01_dt_naiss', 'A4 Date de naissance' ],
    [ 'NumSecuSoc',    'pers01_ss',       'A5 N. de sécurité sociale' ],
    [ 'SituFamille',  'pers01_situation_famille', 'A6 Situation de famille' ],
    [ 'AdressePerso', 'pers01_adr',               'A7 Adresse personnelle' ],
    [ 'CodePostal',   'pers01_cp',                'A8 Code postal' ],
    [ 'BureauDistri', 'pers01_ville',             'A9 Bureau distributeur' ],
    [ 'TelDomicile',  'pers01_tel_domicile',      'A10 Téléphone domicile' ],
    [ 'TelPortable',  'pers01_tel_mobile',        'A11 Téléphone portable' ],
    [ 'EMail',        'pers01_email',             'A12 E-mail' ],
    [ 'Employeur',    'employeur',                'A13 Employeur' ],
    [ 'NumEmployeur', 'num_employeur',            'A14 Nº d\'employeur' ],
    [ 'DateEmbauche', 'date_embauche',            'A15 Date d\'embauche' ],
    [ 'Profession',   'pers01_profession',        'A16 Profession' ],
    [ 'PosStatutaire', 'pers01_statut', 'A17/A18 Position statutaire' ],
    [ 'OptionChoisie', 'option',        'A19/A20 garantie souscrite (coche)' ],
    [
        'PrestaIJF',
        'montant_ij',
        'A21 Montant de la prestation: Indemnités journalières forfaitaires'
    ],
    [
        'PrestaIF', 'montant_inval',
        'A22 Montant de la prestation: Invalidité forfaitaire'
    ],
    [
        'CotisIJF', '_cotis_option1',
        'A23 Indemnités journalières forfaitaires'
    ],
    [ 'CotisIJFIJ', '_cotis_option2', 'A24 IJF et Invalidité forfaitaire' ],
    [
        'PeriodicitePaiement',
        'periode_paiement',
        'A25 Périodicité de paiement: (coche selon valeur)'
    ],
    [
        'ModalitesPaiement',
        'modalite_paiement',
        'A26 Modalités de paiement: (coche selon valeur)'
    ],
    [
        'TypeDuBulletin', 'bul_demande_adh',
        'A27 Type du bulletin: (coche selon valeur)'
    ],
];

my ($TRK);

sub has_non_empty_value {
    my ( $h, $key ) = @_;
    return
         ( exists $h->{$key} )
      && ( defined $h->{$key} )
      && ( $h->{$key} =~ m{\S}sx );
}

sub hash_value {
    my ( $h, $key ) = @_;
    return has_non_empty_value( $h, $key ) ? $h->{$key} : '?';
}

sub hv { my ( $h, $key ) = @_; return hash_value( $h, $key ); }

sub trim {
    my ($str) = @_;
    $str =~ s{\s+}{\x20}sx;
    $str =~ s{^\s+}{}sx;
    $str =~ s{\s+$}{}sx;
    return $str;
}

sub edit_bulletin {
    my ( $data, $doc ) = @_;

    # Specific case
    if (   has_non_empty_value( $data, 'option' )
        && has_non_empty_value( $data, 'cotis_option' ) )
    {
        my $val = $data->{'option'};
        my $private_tag =
            ( $val eq 'option1' ) ? '_cotis_option1'
          : ( $val eq 'option2' ) ? '_cotis_option2'
          :                         '_unused';
        die "$private_tag should not be set" if exists $data->{$private_tag};
        $data->{$private_tag} = $data->{'cotis_option'};
    }

    foreach my $s ( @{$MAPPING} ) {
        my ( $tex_tag, $xml_tag ) = @{$s};
        my $value =
          has_non_empty_value( $data, $xml_tag )
          ? $data->{$xml_tag}
          : $NULL_VALUE;
        $$doc->append( 'valeur' . $tex_tag, $value );
    }

    $$doc->append('bulletin');
    $TRK->track(
        'Doc', 1,
        trim( hv( $data, 'pers01_ss' ) ),    # NUM_SECU
        trim(
            join( "\x20",
                hv( $data, 'pers01_nom' ),
                hv( $data, 'pers01_nom_jf' ),
                hv( $data, 'pers01_prenom' ) )
        ),                                   # NOMDEST
        trim(
            join( "/",
                hv( $data, 'option' ),
                hv( $data, 'montant_ij' ),
                hv( $data, 'montant_inval' ),
                hv( $data, 'cotis_option' ) )
        ),                                   # OPT_EUR
        trim(
            join( "/",
                hv( $data, 'periode_paiement' ),
                hv( $data, 'modalite_paiement' ) )
        ),                                   # PAIEMENT
    );
}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    $TRK = oEdtk::Tracking->new(
        $ARGV[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        entity => oe_corporation_set(),
        keys   => [ 'NUM_SECU', 'NOMDEST', 'OPT_EUR', 'PAIEMENT' ]
    );
    oe_new_job('--index');
    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'BADH' );
    $doc->append( 'xSOURCE', 'coheris' );
    $doc->append( 'xOWNER',  'coheris' );
    my $data = xml_open( \*IN );
    edit_bulletin( $data, \$doc );
    oe_print_to_output_file("$doc");
    oe_compo_link();
    $TRK->track( 'W', 1, '', '', '', 'Fin de traitement' );
    return 0;
}

exit main(@ARGV);
