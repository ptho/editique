#!/usr/bin/env perl
#******************************************************************************
### Date Creation : 12/07/2017
### Date Modification : 10/11/2017
### Date Modification : 19/01/2018
### Objet :			Creation de bulletin d'adhesion pour la NOPI
#******************************************************************************
#******************************************************************************
use utf8;
use strict;
use warnings;

#******************************************************************************
#----------------------------------- LIBRAIRIES -------------------------------
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Data::Dumper;
use Readonly;

#******************************************************************************
#-------------------------- VARIABLES TEMPORAIRES -----------------------------
my ($TRK);
my $DATEEFFET  = "";
my $NUMCOLLECT = "";
my $NOMDEST    = "";
my $NOMJFDEST  = "";
my $PRENOMDEST = "";
my $ADRDEST    = "";
my $CPDEST     = "";
my $VILLEDEST  = "";
my $NOMSECTION = "";
my $MATRICULE  = "";
my $CODEOPER   = "";
my $MOISOFFERT = "";
my ( $LIB_BASE, $LIB_EXPERTE, $OPT1, $OPT2, $OPT3, $TX_INVAL_RET );
my ( $FORMULE,  $OPTION,   $MTIJF,       $MTINVAL );
my ( @BASE_IJF, @BASE_IVF, @EXPERTE_IJF, @EXPERTE_IVF );
my ( %H_FORMUL_OPT, %H_FORMUL_GTIE );

#******************************************************************************
#-------------------- INITIALISATION VARIABLES CONSTANTES ---------------------
Readonly $LIB_BASE     => "Base";
Readonly $LIB_EXPERTE  => "Experte";
Readonly $OPT1         => "option1";
Readonly $OPT2         => "option2";
Readonly $OPT3         => "option3";
Readonly $TX_INVAL_RET => 6;

#******************************************************************************
#-------------- INITIALISATION INDEM JOURNALIERE ET INVALIDE FORFAIT ----------
Readonly @BASE_IJF => ( 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60 );
Readonly @BASE_IVF => ( 1000, 2000, 3000 );
Readonly @EXPERTE_IJF => ( 90, 95 );
Readonly @EXPERTE_IVF => ( 33, 37 );

#******************************************************************************
#-------------------- INITIALISATION TABLE FORMULE ET OPTIONS -----------------
$H_FORMUL_OPT{$LIB_BASE}{$OPT1}    = 1;
$H_FORMUL_OPT{$LIB_BASE}{$OPT2}    = 2;
$H_FORMUL_OPT{$LIB_EXPERTE}{$OPT1} = 1;
$H_FORMUL_OPT{$LIB_EXPERTE}{$OPT2} = 2;
$H_FORMUL_OPT{$LIB_EXPERTE}{$OPT3} = 3;

#******************************************************************************
#-------------------- INITIALISATION TABLE FORMULE ET GARANTIE ----------------
#------------------
#------------- Base
for ( my $iter = 0 ; $iter < @BASE_IJF ; $iter++ ) {
    $H_FORMUL_GTIE{$LIB_BASE}{$OPT1}{ $BASE_IJF[$iter] } = $BASE_IJF[$iter];
    $H_FORMUL_GTIE{$LIB_BASE}{$OPT2}{ $BASE_IJF[$iter] } = $BASE_IJF[$iter];
}

for ( my $iter = 0 ; $iter < @BASE_IVF ; $iter++ ) {
    $H_FORMUL_GTIE{$LIB_BASE}{$OPT2}{ $BASE_IVF[$iter] } = $BASE_IVF[$iter];
}

#------------------
#---------- Experte
for ( my $iter = 0 ; $iter < @EXPERTE_IJF ; $iter++ ) {
    $H_FORMUL_GTIE{$LIB_EXPERTE}{$OPT1}{ $EXPERTE_IJF[$iter] } =
      $EXPERTE_IVF[$iter];
    $H_FORMUL_GTIE{$LIB_EXPERTE}{$OPT2}{ $EXPERTE_IJF[$iter] } =
      $EXPERTE_IVF[$iter];
    $H_FORMUL_GTIE{$LIB_EXPERTE}{$OPT3}{ $EXPERTE_IJF[$iter] } =
      $EXPERTE_IVF[$iter];
}
for ( my $iter = 0 ; $iter < @EXPERTE_IVF ; $iter++ ) {
    $H_FORMUL_GTIE{$LIB_EXPERTE}{$OPT1}{ $EXPERTE_IVF[$iter] } = $TX_INVAL_RET;
    $H_FORMUL_GTIE{$LIB_EXPERTE}{$OPT2}{ $EXPERTE_IVF[$iter] } = $TX_INVAL_RET;
    $H_FORMUL_GTIE{$LIB_EXPERTE}{$OPT3}{ $EXPERTE_IVF[$iter] } = $TX_INVAL_RET;
}
$H_FORMUL_GTIE{$LIB_EXPERTE}{$OPT3}{$TX_INVAL_RET} = $TX_INVAL_RET;

#******************************************************************************
#------------- INITIALISATION TABLE FORMATAGE SITUATION FAMILIALE -------------
Readonly my %H_FAM => (
    "Célibataire" => 1,
    "Marié(e)"    => 2,
    "Union"        => 3,
    "Divorcé(e)"  => 4,
    "Veuf(ve)"     => 5
);

#******************************************************************************
#-------------------- INITIALISATION TABLE FORMATAGE MOIS ---------------------
Readonly my %H_FORMAT_MOIS => (
    "00" => 0,
    "01" => 1,
    "02" => 2,
    "03" => 3,
    "04" => 4,
    "05" => 5,
    "06" => 6,
    "07" => 7,
    "08" => 8,
    "09" => 9,
    "10" => 10,
    "11" => 11,
    "21" => '2+1'
);

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------ FONCTION PRINCIPALE ---------------------------------
sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        entity => oe_corporation_set(),
        keys   => [ 'MATRICUL', 'NOMDEST', 'AGENCE' ]
    );
    oe_new_job('--index');

    my $doc  = oEdtk::TexDoc->new();
    my $data = xml_open( \*IN );

    #print Dumper($data);
    lecture_infoCom($data);
    edit_index( $data, \$doc );
    edit_bulletin( $data, \$doc );

    oe_print_to_output_file("$doc");

    oe_compo_link();
    $TRK->track( 'W', 1, '', '', '', '', 'Fin de traitement' );
    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------- FONCTION EDITION BULLETIN ADHESION --------------------
sub edit_bulletin {
    my ( $data, $doc ) = @_;

    #--------------------------------------------------------------------------
    #----------------------------- TRACKING VALUES ----------------------------
    trackingValues($data);

    #--------------------------------------------------------------------------
    #------------------ VERIFICATION  ET EDITIONDATE D'EFFET ------------------
    verifAndEdit_DateEffet( \$$doc );

    #--------------------------------------------------------------------------
    #------------------ VERIFICATION FORMULE ET OPTION DE GARANTIE ------------
    verification_Data($data);

    #--------------------------------------------------------------------------
    #--------------------- EDITION INFOS MNT ET PERSONNELLES ------------------
    edit_InfoMNT( $data, \$$doc );
    edit_InfoPerso( $data, \$$doc );
    edit_InfoProf( $data, \$$doc );

    #--------------------------------------------------------------------------
    #------------------- EDITION INFO GARANTIE ET COTISATION ------------------
    edit_InfoGarantie( $data, \$$doc );

    #--------------------------------------------------------------------------
    #------------------------------------ FIN DOCUMENT ------------------------
    $$doc->append('finbulletin');

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------- LECTURE D UNE BALISE ------------------------------
sub lire_balis {
    my ( $data, $lib, $nbrMax ) = @_;
    my $valRetour = $data->{$lib} || "";
    if ( $nbrMax > 0 ) {
        $valRetour = substr( $valRetour, 0, $nbrMax );
    }
    return $valRetour;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------------- FONCTION LECTURE INFO COMMUNES------------------------
sub lecture_infoCom {
    my ($data) = @_;
    $DATEEFFET = $data->{'date_effet'} || "";
    $MATRICULE  = lire_balis( $data, 'pers01_matricule',      20 );
    $NUMCOLLECT = lire_balis( $data, 'num_contrat_collectif', 15 );
    $NOMDEST    = lire_balis( $data, 'pers01_nom',            26 );
    $NOMJFDEST  = lire_balis( $data, 'pers_01_nom_jf',        26 );
    $PRENOMDEST = lire_balis( $data, 'pers01_prenom',         26 );
    $ADRDEST    = lire_balis( $data, 'prct_adr',              78 );
    $VILLEDEST  = lire_balis( $data, 'prct_ville',            56 );
    $CPDEST     = $data->{'prct_cp'}             || "";
    $NOMSECTION = $data->{'nom_sectionRattache'} || "";

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------------- FONCTION VERIFICATION DATE EFFET ----------------------
sub verifAndEdit_DateEffet {
    my ($doc) = @_;
    my @splitDate = split( '/', $DATEEFFET, -1 );
    my $nbr = @splitDate;
    if ( $nbr != 3 ) {
        die
"Error: La date d'effet est de format(JJ/MM/AAAA) et est obligatoire.\n";
    }
    $$doc->append( 'dateEffet', $DATEEFFET );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------------- FONCTION VERIFICATION DES DONNEEES ---------------------
sub verification_Data {
    my ($data) = @_;
    my ( $libIJF, $libInval, $mtInvalRet );

    $MTIJF      = "";
    $MTINVAL    = "";
    $mtInvalRet = "";

    #--------------------------------------------------------------------------
    #------------------ VERIFICATION FORMULE ET OPTION DE GARANTIE ------------
    if ( !exists( $data->{'devis_cs_formule'} )
        || ( $data->{'devis_cs_formule'} eq "" ) )
    {
        die
"Error: La formule de garantie doit etre renseignee obligatoirement.\n";
    }

    if ( ( !exists( $data->{'option'} ) ) || ( $data->{'option'} eq "" ) ) {
        die
          "Error: L'option de garantie doit etre renseignee obligatoirement.\n";
    }

    $FORMULE = $data->{'devis_cs_formule'};
    $OPTION  = $data->{'option'};
    if ( !exists( $H_FORMUL_OPT{$FORMULE}{$OPTION} ) ) {
        die
"Error: The formule <<$FORMULE>> does not match with option<<$OPTION>>.\n";
    }

    #--------------------------------------------------------------------------
    #------------------- VERIFICATION FORMULE ET MONTANT PRESTATION------------
    #---------------------------------------------
    #-------- Indemnites journalières forfaitaires
    if   ( $FORMULE eq $LIB_BASE ) { $libIJF = 'montant_ij'; }
    else                           { $libIJF = 'taux_couverture_ij'; }
    if ( exists( $data->{$libIJF} ) ) { $MTIJF = $data->{$libIJF}; }

    if ( !exists( $H_FORMUL_GTIE{$FORMULE}{$OPTION}{$MTIJF} ) ) {
        die
"Error: Le montant des indemnités journalières forfaitaires <<$MTIJF>> n'est pas proposé pour la formule <<$FORMULE>> de l'option <<$OPTION>>.\n";
    }

    #---------------------------------------------
    #------------------------ Invalide forfaitaire
    if   ( $FORMULE eq $LIB_BASE ) { $libInval = 'montant_inval'; }
    else                           { $libInval = 'taux_idmem_inval'; }

    if ( exists( $data->{$libInval} ) ) { $MTINVAL = $data->{$libInval}; }

    if ( ( $FORMULE eq $LIB_EXPERTE ) || ( $OPTION eq $OPT2 ) ) {
        if ( !exists( $H_FORMUL_GTIE{$FORMULE}{$OPTION}{$MTINVAL} ) ) {
            die
"Error: Le taux de l'invalidite forfaitaire <<$MTINVAL>> n'est pas proposé pour la formule <<$FORMULE>> de l'option <<$OPTION>>.\n";
        }
    }

    #---------------------------------------------
    #--------------------------- Invalide Retraite
    if ( exists( $data->{'taux_idmem_inval_ret'} ) ) {
        $mtInvalRet = $data->{'taux_idmem_inval_ret'};
    }

    #---------------------------------------------
    #------------------ IJF et Invalide forfaitaire
    if ( ( $FORMULE eq $LIB_EXPERTE ) && ( $OPTION ne $OPT1 ) ) {
        if ( $H_FORMUL_GTIE{$FORMULE}{$OPTION}{$MTIJF} != $MTINVAL ) {
            die
"Error: Le montant des indemnités journalières forfaitaires <<$MTIJF>> n'est pas proposé pour le capital invalidité <<$MTINVAL>> avec la formule <<$FORMULE>> de l'option"
              . " <<$OPTION>>.\n";
        }

        if ( $OPTION eq $OPT3 ) {
            if ( $H_FORMUL_GTIE{$FORMULE}{$OPTION}{$MTINVAL} != $mtInvalRet ) {
                die
"Error: Le montant des indemnités journalières forfaitaires <<$MTIJF>> et le capital invalidité <<$MTINVAL>> ne sont pas proposés avec l'invalidite de retraite <<$mtInvalRet>> pour la formule <<$FORMULE>> de l'option <<$OPTION>>.\n";
            }
        }
    }

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------------- FONCTION VERIFICATION CODE PROMO -----------------------
sub verif_CodePromo {
    my ($data) = @_;

    my $codePromo = $data->{'code_promo'} || "";
    my @TabPromo = split( "-", $codePromo, -1 );
    my $nbrTmp = @TabPromo;

    if ( !exists( $H_FORMAT_MOIS{ $TabPromo[2] } ) || $nbrTmp <= 2 ) {
        die "Error: Le format du code promo($codePromo) n'est pas correcte.\n";
    }
    $CODEOPER   = $TabPromo[1];
    $MOISOFFERT = $H_FORMAT_MOIS{ $TabPromo[2] };

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#----------------- FONCTION EDITION DES INFOS DE LA MNT -----------------------
sub edit_InfoMNT {
    my ( $data, $doc ) = @_;
    verif_CodePromo($data);
    $$doc->append( 'codeOperation',   $CODEOPER );
    $$doc->append( 'nbrMoisOffert',   $MOISOFFERT );
    $$doc->append( 'numCollectif',    $NUMCOLLECT );
    $$doc->append( 'numGroupeAssure', $data->{'groupe_assures'} || "" );
    $$doc->append( 'idMNT',           lire_balis( $data, 'id_mnt', 8 ) );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------- FONCTION EDITION DES INFOS PERSONNELLES ----------------------
sub edit_InfoPerso {
    my ( $data, $doc ) = @_;
    my @TabEmail  = format_eMail($data);
    my $numFam    = -1;
    my $situatFam = $data->{'pers_01_situation'} || "";

    if ( exists( $H_FAM{$situatFam} ) ) { $numFam = $H_FAM{$situatFam}; }
    $$doc->append( 'persFam',        $numFam );
    $$doc->append( 'persEmailLocal', $TabEmail[0] );
    $$doc->append( 'persEmailDNS',   $TabEmail[1] );

    $$doc->append( 'persNom',       $NOMDEST );
    $$doc->append( 'persNomJf',     $NOMJFDEST );
    $$doc->append( 'persPrenom',    $PRENOMDEST );
    $$doc->append( 'persAdr',       $ADRDEST );
    $$doc->append( 'persCp',        $CPDEST );
    $$doc->append( 'persVille',     $VILLEDEST );
    $$doc->append( 'persCiv',       $data->{'prct_civ'} || "" );
    $$doc->append( 'persDteNais',   $data->{'pers01_dt_naiss'} || "" );
    $$doc->append( 'persRegime',    $data->{'pers01_regime'} || "" );
    $$doc->append( 'persSecu',      $data->{'pers01_ss'} || "" );
    $$doc->append( 'persTelDom',    $data->{'tel_domicile'} || "" );
    $$doc->append( 'persTelMobile', $data->{'tel_mobile'} || "" );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#--------------- FONCTION EDITION DES INFOS PROFESSIONNELLES ------------------
sub edit_InfoProf {
    my ( $data, $doc ) = @_;
    $$doc->append( 'matricule',  $MATRICULE );
    $$doc->append( 'employeur',  lire_balis( $data, 'employeur_nom', 39 ) );
    $$doc->append( 'filiere',    lire_balis( $data, 'employeur_filiere', 22 ) );
    $$doc->append( 'profession', lire_balis( $data, 'pers01_profession', 24 ) );
    $$doc->append( 'dateEmbauche',  $data->{'date_embauche'}    || "" );
    $$doc->append( 'persSatut',     $data->{'pers01_statut'}    || "" );
    $$doc->append( 'persCategorie', $data->{'pers01_categorie'} || "" );
    $$doc->append( 'numSiret',      $data->{'siret'}            || "" );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------- FONCTION EDITION DES INFOS GARANTIES --------------------
sub edit_InfoGarantie {
    my ( $data, $doc ) = @_;
    $$doc->append( 'formuleGtie',      $FORMULE );
    $$doc->append( 'optionGtie',       $H_FORMUL_OPT{$FORMULE}{$OPTION} );
    $$doc->append( 'montantIJF',       $MTIJF );
    $$doc->append( 'montantInvalidF',  $MTINVAL );
    $$doc->append( 'cotisOption',      $data->{'devis_cotis_mens'} || "" );
    $$doc->append( 'periodePaie',      $data->{'periode_paiement'} || "" );
    $$doc->append( 'modalitePaie',     $data->{'modalite_paiement'} || "" );
    $$doc->append( 'traitementBrut',   $data->{'prct_traitement_brut'} || "" );
    $$doc->append( 'primesMensuelles', $data->{'prct_primes'} || "" );
    $$doc->append( 'baseTraitement',   $data->{'prct_base_traitement'} || "" );
    $$doc->append( 'txCotisGlobal',    $data->{'tx_cotisation_global'} || "" );
    $$doc->append( 'PrimesConserve', $data->{'prct_primes_conservees'} || "" );
    $$doc->append( 'modalitePaie', $data->{'modalite_paiement'}      || "" );
    $$doc->append( 'optinMNT',     $data->{'optin-email-mnt'}        || "" );
    $$doc->append( 'optinPart',    $data->{'optin-email-partenaire'} || "" );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------- FONCTION FORMATAGE ADRESSE MAIL --------------------------
sub format_eMail {
    my ($data) = @_;
    my @tab = ( "", "" );
    my $adresMail = $data->{'email'} || "";
    if ( $adresMail ne "" ) {
        @tab = split( '@', $adresMail, -1 );
        my $nbrTmp = @tab;
        die
"Error: Le format de l'adresse mail($adresMail)  n'est pas correcte.\n"
          unless ( $nbrTmp == 2 );
    }
    return @tab;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------------- FONCTION TRACKING ---------------------------------
sub trackingValues {
    my ($data) = @_;
    $TRK->track( 'Doc', 1, $MATRICULE, $NOMDEST . " " . $PRENOMDEST,
        $NOMSECTION );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#-------------------------- REMPLISSAGE FICHIER INDEX -------------------------
sub edit_index {
    my ( $data, $doc ) = @_;
    $$doc->append( 'context',  $data->{'contexte'});
    $$doc->append( 'xTYPDOC',  'BADH' );
    $$doc->append( 'xSOURCE',  'coheris' );
    $$doc->append( 'xOWNER',   'coheris' );
    $$doc->append( 'xIDDEST',  $NUMCOLLECT );
    $$doc->append( 'xNOMDEST', $NOMDEST . " " . $PRENOMDEST );
    $$doc->append( 'xVILDEST', $VILLEDEST );
    $$doc->append( 'xCPDEST',  $CPDEST );
    $$doc->append( 'xIDEMET',  $NOMSECTION );
    $$doc->append_table( 'xADRLN', $ADRDEST );

    return 0;
}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------- EXIT MAIN ---------------------------------
exit main(@ARGV);
