#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use oEUser::Lib;
use Data::Dumper;
use oEdtk::Tracking;
use oEdtk::libXls;
use oEdtk::RecordParser;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::F3;
use oEUser::Descriptor::H0;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::J2;
use oEUser::Descriptor::L2;
use oEUser::Descriptor::N1;
use oEUser::Descriptor::T1;
use oEdtk::libXls;
use strict;
use warnings;

#################################################################################
# v0.5 21/11/2006 11:56:54 du squelette d'extraction de donnees pour C7
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données suur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

# CORPS PRINCIPAL DE L'APPLICATION :

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)

my $TRK;
my %hRECORDS;
my $AGENCE;
my $NOM;
my $ADHERENT;
my $NUMIND;
my $NUMCOLL;
my $GROUPE;
my $RAPPEL = 0;
my $PERCU  = 0;
my $DATEDEB;
my $DATEFIN;
my $MONTANT = 0;
my $MTTCALC = 0;
my $CALCUL  = 0;
my $MODEREGLE;
my $FREQUENCE;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] ) || 'none';

    # INITIALISATION DU TRAKING
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMIND', 'AGENCE', 'NOM' ]
    );

    # oe_new_job(); # est remplacé par :
    oe_open_fi_IN( $argv[0] );

    # MAPPING DES RECORDS
    map_records();

    # INITIALISATION DU PROCESS PARSER (ASSIGNE LES CLEFS AUX RECORDS)
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $hRECORDS{ENTETE},
        'E1'     => $hRECORDS{E1},
        'E2'     => $hRECORDS{E2},
        'F3'     => $hRECORDS{F3},
        'H0'     => $hRECORDS{H0},
        'H1'     => $hRECORDS{H1},
        'J2'     => $hRECORDS{J2},
        'L2'     => $hRECORDS{L2},
        'N1'     => $hRECORDS{N1},
        'T1'     => $hRECORDS{T1},
    );

    initXLS();

    my ( $id, $lastid );

    while ( my ( $id, $VALS ) = $p->next() ) {
        if ( $id eq 'E1' ) {
            $AGENCE = $VALS->{'ODERCOD'};
            $NOM    = $VALS->{'PENOLIB'};
            my $Y;

            # ADRESSE DESTINATAIRE.
            oe_clean_addr_line( $VALS->{'LIBLOCL'} );
            oe_clean_addr_line( $VALS->{'NOMCDXL'} );
            $Y = $VALS->{'NOMCDXL'};
            $Y =~ s/CEDEX.*$//
              ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
            $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

            if ( $VALS->{'INDCDXx'} == 0 ) {

                # CAS 3
                $VALS->{'LIBLOCL'} = '';
            }
            elsif ( $VALS->{'INDCDXx'} == 1
                && index( $VALS->{'LIBLOCL'}, $Y ) == -1 )
            {
                # CAS 1
            }
            else {
                # CAS 2
                $VALS->{'LIBLOCL'} = '';
            }

            my @DADDR = ();
            push( @DADDR,
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
            );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
            );
            push( @DADDR, "$VALS->{'BOIPOSL'}$VALS->{'LIBLOCL'}" );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $VALS->{'LICCODP'} );
            $VALS->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $ADHERENT =
"$DADDR[0]\n $DADDR[1] $DADDR[2] $DADDR[3] $DADDR[4] $DADDR[5] $DADDR[6] $DADDR[7]";

        }
        elsif ( $id eq 'E2' ) {
            $NUMIND    = $VALS->{'ACH1NUM'};
            $MODEREGLE = $VALS->{'ACPAMOD'};
            $NUMCOLL   = $VALS->{'ACCCNUM'};
            $GROUPE    = $VALS->{'ACGACOD'};
            $FREQUENCE = $VALS->{'LICFRQA'};

        }
        elsif ( $id eq 'F3' ) {

            $DATEDEB = convertDate( $VALS->{'DDEPRD'} );
            $DATEFIN = convertDate( $VALS->{'DFEPRD'} );

        }
        elsif ( $id eq 'H0' ) {
        }
        elsif ( $id eq 'H1' ) {
        }
        elsif ( $id eq 'J2' ) {
        }
        elsif ( $id eq 'L2' ) {
        }
        elsif ( $id eq 'N1' ) {
        }
        elsif ( $id eq 'T1' ) {

            #		warn "INFO : T1 ".Dumper($VALS) ."\n";
            $MTTCALC = $VALS->{'MONTANT'};
            $PERCU   = $VALS->{'PERCUxx'};
            $RAPPEL  = $VALS->{'RESTExx'};
            $CALCUL  = $VALS->{'RESTExx'} - $VALS->{'PERCUxx'};
            $MONTANT = $MTTCALC - $CALCUL;
            if ( $VALS->{'ACPAMOD'} = 'C' && $RAPPEL > 0 || $PERCU > 0 ) {
                ligne_adh_Xls();
                $TRK->track( 'Doc', 1, $NUMIND, $AGENCE, $NOM );
            }
        }
    }
    return 0;
}

sub map_records() {
    $hRECORDS{ENTETE} = oEUser::Descriptor::ENTETE->get();
    $hRECORDS{ENTETE}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{E1} = oEUser::Descriptor::E1->get();
    $hRECORDS{E1}->bind_all_c7()
      ;    # bind les champs de données sur 7 caractères
    $hRECORDS{E2} = oEUser::Descriptor::E2->get();
    $hRECORDS{E2}->bind_all_c7()
      ;    # bind les champs de données sur 7 caractères
    $hRECORDS{F3} = oEUser::Descriptor::F3->get();
    $hRECORDS{F3}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{H0} = oEUser::Descriptor::H0->get();
    $hRECORDS{H0}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{H1} = oEUser::Descriptor::H1->get();
    $hRECORDS{H1}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{J2} = oEUser::Descriptor::J2->get();
    $hRECORDS{J2}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{L2} = oEUser::Descriptor::L2->get();
    $hRECORDS{L2}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{N1} = oEUser::Descriptor::N1->get();
    $hRECORDS{N1}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{T1} = oEUser::Descriptor::T1->get();
    $hRECORDS{T1}->bind_all_c7()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur

    return 1;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub ligne_adh_Xls {
    prod_Xls_Insert_Val($AGENCE);
    prod_Xls_Insert_Val($NUMCOLL);
    prod_Xls_Insert_Val($GROUPE);
    prod_Xls_Insert_Val($ADHERENT);
    prod_Xls_Insert_Val($NUMIND);
    prod_Xls_Insert_Val($MODEREGLE);
    prod_Xls_Insert_Val($FREQUENCE);
    prod_Xls_Insert_Val($DATEDEB);
    prod_Xls_Insert_Val($DATEFIN);
    prod_Xls_Insert_Val($MONTANT);
    prod_Xls_Insert_Val($PERCU);
    prod_Xls_Insert_Val($RAPPEL);
    prod_Xls_Insert_Val($MTTCALC);
    prod_Xls_Edit_Ligne();
}

sub initXLS () {

    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ; NR7 = Numérique Right...  )
    prod_Xls_Col_Init(
        'AC15', 'AC10', 'AC10', 'AL30', 'AC15', 'AC10', 'AC20', 'AC10',
        'AC10', 'NR20', 'NR20', 'NR20', 'NR20'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init( "", "RAPPEL SUR APPEL INDIVIDUEL CHEQUE" )
      ;    #  ,"ENTETE DROIT");

# INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
# PRÉPARATION DES TITRES DE COLONNES
# AGENCE; CONTRAT; COL; GROUPE; CIV; NOM ; DATNAI; PRéNOM ; NOV; BTQ; NTV; LBV; AD2;  LOC; CPS; BDS
    prod_Xls_Insert_Val("Entite de rattachement");
    prod_Xls_Insert_Val("Num. Contrat");
    prod_Xls_Insert_Val("Num. Groupe");
    prod_Xls_Insert_Val("Nom et Prénom");
    prod_Xls_Insert_Val("Contrat individuel");
    prod_Xls_Insert_Val("Mode Regl.");
    prod_Xls_Insert_Val("Frequence de Regl.");
    prod_Xls_Insert_Val("Début échéance");
    prod_Xls_Insert_Val("Fin échéance");
    prod_Xls_Insert_Val("Montant appelé");
    prod_Xls_Insert_Val("Trop perçu");
    prod_Xls_Insert_Val("Rappel");
    prod_Xls_Insert_Val("Montant calculé");

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );
    return 0;
}

exit main(@ARGV);
