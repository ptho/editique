#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls 0.462;
use oEdtk::Tracking;
use File::Basename;
use strict;

#################################################################################
# v0.5 01/12/2008 14:28:25 du squelette d'extraction de donnees pour XLS
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données sur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

#################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
#################################################################################

my $TRK;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    my $ctrl_idx2 = basename($0);
    $ctrl_idx2 =~ s/\.pl$/.idx2/i;

    $TRK =
      oEdtk::Tracking->new( $argv[1],
        keys => [ 'NUMRO', 'CAISSRO', 'NOMDEST', 'UG', 'MESSAGE' ] );

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );
    oe_open_fo_OUT($ctrl_idx2);

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    # avec initialisation propre au document
    &initApp();
    while ( my $ligne = <IN> ) {
        chomp($ligne);

        # APPELS DES FONCTIONS v 20090205092458
        if ( oe_process_ref_rec( 158, 2, $ligne, 168, 3034 ) ) {

# FIN traitement enregistrement(s) : LA 000/U0 100/U1 200/U2 210/U3 400/U4 410/U5 (20N/U9 ?)

        }
        else {
            # TYPE D'ENREGISTREMENT INCONNU
            warn "INFO : IGNORE REC. line $.\n";
        }

        #prod_Xls_Edit_Ligne();
    }

    &totalisation_CPAM();
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################

my $ENCRCOD_GRD_REGIME;
my $ENCR006_CAISSE_RO;
my $ENCNCOD_CTR_PAIMNT;
my $CLE_RUPTURE =
  $ENCRCOD_GRD_REGIME . $ENCR006_CAISSE_RO . $ENCNCOD_CTR_PAIMNT;
my $LAST_CLE_RUPTURE = $CLE_RUPTURE;
my @tASSEMBLAGE_LIGNE;

my $SOMME_ASS       = 0;
my $SOMME_BENEF     = 0;
my $SOMME_CONTRATS  = 0;
my $PRECEDENT_RO    = "";
my $PRECEDENT_BENEF = "";

sub initApp {

    # CARTOGRAPHIE APPLICATIVE v 20090205092458

    #	LA : Informations sur l'accord NOEMIE
    #	U0 : ENREGISTREMENT FLUX (000/000)
    #	U1 : ENREGISTREMENT LOT (100/000)
    #	U2 : ENREGISTREMENT DOSSIER BENEFICIAIRE (200/000)
    #	U3 : ENREGISTREMENT ADRESSE + RIB ASSURE (210/000)
    #	U4 : ENREGISTREMENT LIGNE DE CONTRAT (400/000)
    #	U5 : DETAIL DES GARANTIES (410/000)
    #	U9 : ENREGISTREMENT MESSAGE (xx0/nnn)

    oe_rec_motif( 'LA',
'A010 A009 A010 A015 A050 A002 A003 A004 A010 A015 A050 A009 A010 A015 A050 A*'
    );
    oe_rec_process( 'LA', \&postLA );
    oe_rec_motif( 'U2',
'A05 A8 A8 A10 A10 A15 A8 A9 A8 A8 A03 A1 A01 A30 A01 A13 A2 A01 A25 A15 A01 A25 A15 A09 A13 A2 A2 A8 A02 A02 A01 A01 A25 A15 A02 A25 A25 A01 A15 A4 A04 A02 A9 A8 A1 A8 A1 A03 A8 A9 A8 A8 A03 A8 A8 A2 A6 A2 A6 A2 A10 A10 A*'
    );
    oe_rec_process( 'U2', \&postU2 );
    oe_rec_motif( 'U4', 'A01 A08 A12 A02 A8 A8 A*' );
    oe_rec_process( 'U4', \&postU4 );

    oe_rec_motif( 'U0', 'A2 A8 A01 A10 A10 A10 A10 A10 A*' );
    oe_rec_motif( 'U1',
'A02 A02 A14 A02 A14 A02 A04 A02 A10 A10 A8 A02 A03 A04 A04 A02 A06 A06 A9 A8 A01 A03 A02 A02 A10 A10 A10 A*'
    );
    oe_rec_motif( 'U9', 'A10 A02 A02 A100 A*' );
    oe_rec_motif( 'U3',
'A20 A05 A30 A30 A05 A01 A05 A30 A30 A30 A05 A30 A03 A05 A30 A30 A05 A05 A11 A02 A*'
    );
    oe_rec_motif( 'U5', 'A01 A03 A8 A8 A*' );

    # INITIALISATION PROPRE AU DOCUMENT
    &initDoc();

    1;
}

sub initDoc {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
    #
    # 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
    # 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ;
    #	 NR7 = Numérique Right ; AL12 = Alpha Left 12 de large...  )
    #
    prod_Xls_Col_Init(
        'AC08', 'AC08', 'AC09', 'AC05', 'AC12', 'AC14', 'AL15', 'AL17',
        'AC09', 'Ac30', 'AC10', 'AC06', 'AC07', 'AC09', 'AC09'
    );

    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    #
    prod_Xls_Init( "", "Acquittements CMU et d'aide à la Mutualisation" )
      ;    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("Gd Régime");
    prod_Xls_Insert_Val("Caisse RO");
    prod_Xls_Insert_Val("CT Paiement");
    prod_Xls_Insert_Val("UG");
    prod_Xls_Insert_Val("N° Adh");
    prod_Xls_Insert_Val("N° RO");
    prod_Xls_Insert_Val("NOM Ass.");
    prod_Xls_Insert_Val("Prénom Ass.");
    prod_Xls_Insert_Val("Nature Nom Ass.");
    prod_Xls_Insert_Val("NOM Prénom Bénéf.");
    prod_Xls_Insert_Val("Naissance");
    prod_Xls_Insert_Val("Rang Laser");
    prod_Xls_Insert_Val("Type Contrat");
    prod_Xls_Insert_Val("Début Contrat");
    prod_Xls_Insert_Val("Fin Contrat");

    # ETC.

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );

    1;
}

sub postLA() {
    $CLE_RUPTURE = $DATATAB[5] . $DATATAB[6] . $DATATAB[7];

    if ( $. > 3 && $LAST_CLE_RUPTURE ne $CLE_RUPTURE ) {
        &totalisation_CPAM();
    }

    $LAST_CLE_RUPTURE   = $CLE_RUPTURE;
    $ENCRCOD_GRD_REGIME = $DATATAB[5];
    $ENCR006_CAISSE_RO  = $DATATAB[6];
    $ENCNCOD_CTR_PAIMNT = $DATATAB[7];
    1;
}

sub totalisation_CPAM() {
    prod_Xls_Insert_Val( $ENCRCOD_GRD_REGIME, $ENCR006_CAISSE_RO,
        $ENCNCOD_CTR_PAIMNT );
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val("TOTALISATION");
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val("NB Ass. : $SOMME_ASS");
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val("NB Bénéf. : $SOMME_BENEF");
    prod_Xls_Insert_Val("NB Ctrt");
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val($SOMME_CONTRATS);
    prod_Xls_Insert_Val("");
    prod_Xls_Insert_Val( "", "NEW_PAGE" );

    prod_Xls_Edit_Ligne('T3');
    $SOMME_ASS      = 0;
    $SOMME_BENEF    = 0;
    $SOMME_CONTRATS = 0;
    1;
}

sub postU2() {
    undef @tASSEMBLAGE_LIGNE;
    my $num_RO = "$DATATAB[15] $DATATAB[16]";
    my $benef  = "$DATATAB[32] $DATATAB[33]";

    if ( $PRECEDENT_RO ne $num_RO ) { $SOMME_ASS++; }
    $PRECEDENT_RO = $num_RO;

    if ( $PRECEDENT_BENEF ne $benef ) { $SOMME_BENEF++; }
    $PRECEDENT_BENEF = $benef;

    push( @tASSEMBLAGE_LIGNE,
        $ENCRCOD_GRD_REGIME, $ENCR006_CAISSE_RO, $ENCNCOD_CTR_PAIMNT );
    push( @tASSEMBLAGE_LIGNE, $DATATAB[40] );                  #"UG");
    push( @tASSEMBLAGE_LIGNE, $DATATAB[23] );                  #"N° Adh");
    push( @tASSEMBLAGE_LIGNE, "$DATATAB[15] $DATATAB[16]" );   #"N° RO");
    push( @tASSEMBLAGE_LIGNE, $DATATAB[18] );                  #"NOM Ass.");
    push( @tASSEMBLAGE_LIGNE, $DATATAB[19] );                  #"Prénom Ass.");
    push( @tASSEMBLAGE_LIGNE, $DATATAB[17] );               #"Nature Nom Ass.");
    push( @tASSEMBLAGE_LIGNE, "$DATATAB[32] $DATATAB[33]" )
      ;    #"NOM Prénom Bénéf.");
    oe_to_date( $DATATAB[27] );
    push( @tASSEMBLAGE_LIGNE, $DATATAB[27] );    #"Naissance");
    push( @tASSEMBLAGE_LIGNE, $DATATAB[30] );    #"Rang Laser");

    $TRK->track(
        'Line', 1, "$DATATAB[15] $DATATAB[16]",
        $ENCR006_CAISSE_RO, "$DATATAB[32] $DATATAB[33]",
        $DATATAB[40]
    );
    1;
}

sub postU4() {
    prod_Xls_Insert_Val(@tASSEMBLAGE_LIGNE);
    prod_Xls_Insert_Val( $DATATAB[3] );          #"Type Contrat");
    oe_to_date( $DATATAB[4] );
    prod_Xls_Insert_Val( $DATATAB[4] );          #"Début Contrat");
    oe_to_date( $DATATAB[5] );
    prod_Xls_Insert_Val( $DATATAB[5] );          #"Fin Contrat");

    prod_Xls_Edit_Ligne();
    $SOMME_CONTRATS++;
    1;
}

exit main(@ARGV);
