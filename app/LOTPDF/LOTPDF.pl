use strict;
use warnings;

use oEdtk::Main;
use oEdtk::TexDoc;
use oEdtk::Tracking;

use File::Copy;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    my $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        entity => oe_corporation_set(),
        user   => 'LOTPDF'
    );

    oe_new_job();

    $. = 2;    # XXX OMG ITS A HACK

    # There is no real .dat file to process, we just copy it.
    copy( \*IN, \*OUT );
    oe_compo_link();
    return 0;
}

exit main(@ARGV);
