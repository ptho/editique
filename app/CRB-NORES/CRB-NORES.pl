#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;

my ( $TRK, $DOC, $GED, $VALS );
my $TYPECOUR;
my @DADDR;
my $AGENCE;
my $CIV;
my $CIVILITE;
my $NOMDEST;
my $NUMCTR;
my $NUMABA;
my $xNOMDEST;
my $GESTIONNAIRE;
my $EMETTEUR;
my $CODCENTRE;
my $LIBCENTRE;
my $OPT_GED;
my $DtEDITION;
my $DtACTE;
my $DtCOUR;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys =>
          [ 'NUMCTR', 'NUMABA', 'EMETTEUR', 'CODECOURRIER', 'DATECOURRIER' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # 	$E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # 	$E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    # 	$E7->debug();
    $E7->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();
    my $LISTE_BENEF_RAD = oEdtk::TexDoc->new();
    my $DELAIS_STAGE_AJ = oEdtk::TexDoc->new();

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'Z1' => $Z1
    );

    $DOC->append( 'xTYPDOC', 'CRRE' );    ### Type de document

    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {

        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n";

            $DOC->append( 'DEBUT', '' );
            $NOMDEST =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            #####GESTION DE LA CIVILITE ENTRE TIERS ET ADHERENT#########
            my $civDest = $VALS->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );

            # $CIVILITE = $VALS->{'LICCODC'};
            $CIVILITE = $VALS->{'PENOCOD'};
            if ( $CIVILITE eq 'M' ) {
                $DOC->append( 'CIVILITE', 'Cher adhérent' );
            }
            elsif ( $CIVILITE eq 'MME' || $CIVILITE eq 'MLLE' ) {
                $DOC->append( 'CIVILITE', 'Chère adhérente' );
            }
            else {
                $DOC->append( 'CIVILITE', 'chère adhérente, cher adhérent' );
            }

            $EMETTEUR = $VALS->{'ODERCOD'};
            if ( $EMETTEUR eq '' ) {
                $CIV      = 0;
                $EMETTEUR = "ER";
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }
            else {
                $CIV = 1;
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }

            $DOC->append( 'CIV', $CIV );

            $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
            $DOC->append( 'xNOMDEST', $xNOMDEST );

            ################CENTRE DE GESTION###############################
            $CODCENTRE = $VALS->{'ODCGCOD'};
            if ( $CODCENTRE eq 'MNT-RC' ) {
                $LIBCENTRE = 'Complémentaire Santé';
            }
            else {
                $LIBCENTRE = 'Prévoyance';
            }

            $DOC->append( 'CODCENTRE', $CODCENTRE );
            $DOC->append( 'LIBCENTRE', $LIBCENTRE );

            ################ADRESSE EMETTEUR#########################
            $AGENCE = $VALS->{'PENO027'};
            $DOC->append( 'AGENCE', $VALS->{'PENO027'} );

            my $adresse =
                $VALS->{'PEAD030'} . ' '
              . $VALS->{'LICN034'} . ' '
              . $VALS->{'LIBV035'};
            $DOC->append( 'ADRESSE', $adresse );

            $DOC->append( 'BOITPOS', $VALS->{'BOIP037'} );

            $DOC->append( 'CODPOS', $VALS->{'CODC039'} );

            my $localite = ( $VALS->{'LIBL038'} );
            $DOC->append( 'LOCALITE', $localite );
            $DOC->append('BASDEPAGE');

            ######INDEXATION GED###################
            $GED->append( 'xIDEMET',  $EMETTEUR );        #-	Emetteur
            $GED->append( 'xNOMDEST', $xNOMDEST );        #-	Nom du destinataire
            $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
              ;    #-	Ville du destinataire

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E2' ) {

            # warn "INFO : dump E2 ". Dumper($VALS) ."\n";
            $DtEDITION =
              convertDate( $VALS->{'DATEDTx'} );   #Date du Traitement d'Edition
            $DOC->append( 'DATEEDT', $DtEDITION );

            #########NUMERO CONTRAT INDIVIDUEL############
            $NUMCTR = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMCTR', $NUMCTR );

            ###########CODE COURRIER###################
            $TYPECOUR = $VALS->{'CDCOURx'};                 #CODE COURRIER
            $DtACTE   = convertDate( $VALS->{'ACOPDDE'} )
              ;    #DATE D'EFFET DE L'ACTE DE GESTION
            calculAnnee();
            courrier();

            ######INDEXATION GED###################
            $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
              ;    #-	Numéro de contrat collectif
            $GED->append( 'xCLEGEDi', $VALS->{'ACH1NUM'} )
              ;    #-	Numéro de contrat individuel

            $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
            $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
            $GED->append( 'xCLEGEDiv', $DtEDITION )
              ;    #Date du Traitement d'Edition

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E7' ) {
            ############BLOC ADRESSE DESTINATAIRE##################
            my @DADDR = ();
            push( @DADDR, $NOMDEST );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
            );
            push( @DADDR, $VALS->{'BOIPOSL'} );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $VALS->{'LICCODP'} );
            $VALS->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $DOC->append_table( 'DADDR', @DADDR );

            # warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";

        }

        if ( $id eq 'OPTION' ) {

# warn "INFO : dump OPTION ". Dumper($VALS) ."\n";
# warn"  pos2=$VALS->[2]      pos3=$VALS->[3]  pos4=$VALS->[4] pos5=$VALS->[5]  \n";
            $DtCOUR = $VALS->[2];    #DATE COURRIER SAISIE PAR LE GESTIONNAIRE
            my $valeur = $DtCOUR;
            $valeur =~ s/^\s+//;     #supprime les espaces en début de chaine
            $valeur =~ s/\s+$//;     #en fin de chaine
            $valeur =~ s/\s+/ /;     #remplace des séries d'espaces par un seul

            if ( $valeur ne '' ) {
                $DOC->append( 'DATECOUR', $DtCOUR );
                ###########Affichage du 1er jour du mois suivant
                # my $dtRESILIATION = $DtCOUR;

                my $dtRESILIATION = $VALS->[3];    # date de résiliation
                     # my @tabDt = split('/',$DtCOUR);
                     # if ($tabDt[1] < 12 && $tabDt[1] > 0){
                     # $tabDt[1]++;
                     # if ($tabDt[1] < 10){
                     # $dtRESILIATION = "01/0".$tabDt[1]."/".$tabDt[2];
                     # }else{
                     # $dtRESILIATION = "01/".$tabDt[1]."/".$tabDt[2];
                     # }
                     # }elsif($tabDt[1] == 12){
                     # $tabDt[2]++;
                     # $dtRESILIATION = "01/01/".$tabDt[2];
                     # }

                $dtRESILIATION =~
                  s/^\s+//;    #supprime les espaces en début de chaine
                $dtRESILIATION =~ s/\s+$//;    #en fin de chaine
                $dtRESILIATION =~
                  s/\s+/ /;    #remplace des séries d'espaces par un seul

                #				if ($dtRESILIATION ne '') {
                $DOC->append( 'DATERESUL', $dtRESILIATION );

# 				} else {
# 					die "\nERROR:Impossible de generer un courrier ".$TYPECOUR." avec une date de resiliation vide\n\n";
# 				}
            }
            else {
                die "\nERROR:Impossible de generer un courrier "
                  . $TYPECOUR
                  . " avec une date courrier vide\n\n";
            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n"; die();
        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }

    $DOC->append('COURRIER');
    $DOC->append('FINDOC');

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";
    # warn "INFO : dump GED ". Dumper($GED) ."\n";
    # die();

    $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $EMETTEUR, $TYPECOUR, $DtCOUR );
    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    $DOC->reset();
    $GED->reset();
    oe_compo_link($OPT_GED);
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub calculAnnee() {
    my @tab = split( '/', $DtEDITION );

    my $ANNEE  = $tab[2];
    my $ANNEE1 = $ANNEE + 1;

    if (
        (
               $TYPECOUR eq 'RES01'
            || $TYPECOUR eq 'RES04'
            || $TYPECOUR eq 'RES05'
            || $TYPECOUR eq 'RES06'
            || $TYPECOUR eq 'RES07'
            || $TYPECOUR eq 'RES09'
            || $TYPECOUR eq 'RES11'
        )
        && ( $tab[1] == 12 || $tab[1] == 11 )
      )
    {
        $ANNEE  = $ANNEE + 1;
        $ANNEE1 = $ANNEE + 1;
    }

#  elsif (($TYPECOUR eq 'RES01' || $TYPECOUR eq 'RES04' || $TYPECOUR eq 'RES05' || $TYPECOUR eq 'RES06' || $TYPECOUR eq 'RES07' || $TYPECOUR eq 'RES09' || $TYPECOUR eq 'RES11') && ($tab[1] != 12 || $tab[1] != 11)){
#      $ANNEE = $ANNEE + 1;
#      $ANNEE1 = $ANNEE + 2;
#	}

    $DOC->append( 'ANNEE',   $ANNEE );
    $DOC->append( 'ANNEEUN', $ANNEE1 );

}

sub courrier() {

#Retour d'homologation sur la sésame 1129769
#Les RES 1,2,3 ne seront pas traités en production pour le moment.
#Ils sont déparamétrés par Viviane et commenté par nous en attendant une adaptation juridique.
#	if ($TYPECOUR eq 'RES01'){
#     $DOC->append ('LETTRE', {'RESI'});
#  }elsif($TYPECOUR eq 'RES02'){
#     $DOC->append ('LETTRE', {'RESII'});
#  }elsif($TYPECOUR eq 'RES03'){
#     $DOC->append ('LETTRE', {'RESIII'});
#  }els
    if ( $TYPECOUR eq 'RES04' ) {
        $DOC->append( 'LETTRE', {'RESIV'} );
    }
    elsif ( $TYPECOUR eq 'RES05' ) {
        $DOC->append( 'LETTRE', {'RESV'} );
    }
    elsif ( $TYPECOUR eq 'RES06' ) {
        $DOC->append( 'LETTRE', {'RESVI'} );
    }
    elsif ( $TYPECOUR eq 'RES07' ) {
        $DOC->append( 'LETTRE', {'RESVII'} );
    }
    elsif ( $TYPECOUR eq 'RES08' ) {
        $DOC->append( 'LETTRE', {'RESVIII'} );
    }
    elsif ( $TYPECOUR eq 'RES09' ) {
        $DOC->append( 'LETTRE', {'RESIX'} );
    }
    elsif ( $TYPECOUR eq 'RES10' ) {
        $DOC->append( 'LETTRE', {'RESX'} );
        $DOC->append( 'AfficheNote', 'Oui' );
    }
    elsif ( $TYPECOUR eq 'RES11' ) {
        $DOC->append( 'LETTRE', {'RESXI'} );
    }
    elsif ( $TYPECOUR eq 'RES12' ) {
        $DOC->append( 'LETTRE', {'RESXII'} );
    }
    else {
        die "\nERROR:CODE COURRIER INCONNU - " . $TYPECOUR . "\n\n";
    }
}

exit main(@ARGV);
