#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::PA;
use oEUser::Descriptor::PB;
use oEUser::Descriptor::PC;

#use oEUser::Descriptor::X1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use strict;
use POSIX;
use Math::Round;

my ( $TRK, $GED, $DOC, $VALS, $COURRIER, $P );
my ( $NUMCTR, $ERAFF );

#my  $CDE_COUR= 'CTA-28';
my $nomUser = "";
my $OPT_GED;

#my $aff = 1;
my $ville = "Villeurbanne";
my $Garantie_Ass;
my %Garantie;
my @tGarantie;
my $ind_tGarantie = 0;
my %CodeSS;
my @tAnalyse;
my $X = 0;
my $Y;
my $TotalDepense = 0;
my $TotalBaseSS  = 0;
my $TotalRembRO  = 0;
my $TotalRembRC  = 0;
my $TotalCharge  = 0;
my $Nomadh;
my $Prenomadh;
my $datedit;
my $RemRC;
my $Restencharge;
my @DADDR;
my @EADDR;
my $ZONEDH;
my $nomdest;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );

    #	$OPT_GED="";
    # SI ON SOUHAITE METTRE LE DOCUMENT EN GED,
    # IL SUFFIT DE PASSER L'OPRION --edms À oe_compo_link COMME CI-DESSOUS
    #  $OPT_GED='--edms';
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMADH', 'NOMDEST', 'PRENOMDEST', 'DATE', 'GARANTIE' ]
    );
    oe_new_job('--index');

    #	&init_Doc();
    my $E1 = oEUser::Descriptor::E1->get();

    # 		$E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    # 		$E2->debug();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    # 		$E7->debug();
    $E7->bind_all_c7();
    my $PA = oEUser::Descriptor::PA->get();
    $PA->bind_all_c7();
    my $PB = oEUser::Descriptor::PB->get();
    $PB->bind_all_c7();
    my $PC = oEUser::Descriptor::PC->get();

    #		$PC->debug();
    $PC->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();
    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();
    $P   = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'PA' => $PA,
        'PB' => $PB,
        'PC' => $PC,
        'Z1' => $Z1
    );
    $P->set_motif_to_denormalized_split('\x{01}');
    my $lastid;
    my $firstDoc = 0;
    my $id;

    $DOC->append( 'xTYPDOC', 'CBAD' );    ### Type de document

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {              #traitement du record X1#

            #			$TRK->track('Doc', 1, $NUMAD, '', '', '', $MONTANT);
            #			&init_Doc();
            &traite_E1();

        }
        elsif ( $id eq 'E2' ) {    #traitement du record E2#
            &traite_E2();
        }
        elsif ( $id eq 'E7' ) {    #traitement du record E2#
            &traite_E7();
        }
        elsif ( $id eq 'PA' ) {    #traitement du record PA#
            &traite_PA();
        }
        elsif ( $id eq 'PB' ) {    #record PB #
            &traite_PB();

        }
        elsif ( $id eq 'PC' ) {    #traitement du record PC#
            &traite_PC();
        }
        elsif ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            my $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMADH', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

        $lastid = $id;
    }
######### en sortant de la boucle des PC , je je n'oublie pas de remettre le total dans le tableau#####################
    if ( $X ne 0 ) { $tAnalyse[ $X - 1 ][6] = $RemRC; }
    else {
        die 'Aucune Garantie retrouvée';
    }
#######################################################################################################################

    $GED->append( 'xSOURCE', 'BUR' );
    foreach my $Garant (@tGarantie) {

        #  warn"Garant=$Garant";
        $Garantie_Ass = $Garantie_Ass . $Garant . '+';

        #  warn"Garantie_Ass=$Garantie_Ass";
    }
    $Garantie_Ass = substr( $Garantie_Ass, 0, length($Garantie_Ass) - 1 );
    $DOC->append( 'garantie', $Garantie_Ass );
    $DOC->append('LETTRE');
    my $i = 0;
    my $charge;
    while ( $i < $X ) {
        $DOC->append( 'NATURE',   $tAnalyse[$i][0] );
        $DOC->append( 'COTATION', $tAnalyse[$i][1] );
        $DOC->append( 'DEPENSE',  $tAnalyse[$i][2] );
        $TotalDepense = $TotalDepense + $tAnalyse[$i][2];
        $DOC->append( 'BASESS', $tAnalyse[$i][3] );
        $TotalBaseSS = $TotalBaseSS + $tAnalyse[$i][3];
        $DOC->append( 'TAUX', sprintf( "%1.0f", $tAnalyse[$i][4] ) );
        $DOC->append( 'REMBRO', $tAnalyse[$i][5] );
        $TotalRembRO = $TotalRembRO + $tAnalyse[$i][5];
        $DOC->append( 'REMBRC', $tAnalyse[$i][6] );
        $TotalRembRC = $TotalRembRC + $tAnalyse[$i][6];
        $charge =
          ( ( $tAnalyse[$i][2] ) * 1 ) -
          ( ( $tAnalyse[$i][5] ) * 1 ) -
          ( ( $tAnalyse[$i][6] ) * 1 );
        $charge = abs($charge);
        $DOC->append( 'CHARGE', sprintf( "%1.2f", $charge ) );
        $TotalCharge = $TotalCharge + $charge;
        $DOC->append('DEVIS');
        $i = $i + 1;
    }

    $DOC->append( 'NATURE',   'TOTAL' );
    $DOC->append( 'COTATION', '' );
    $DOC->append( 'DEPENSE',  $TotalDepense );
    $DOC->append( 'BASESS',   $TotalBaseSS );
    $DOC->append( 'TAUX',     0 );
    $DOC->append( 'REMBRO',   $TotalRembRO );
    $DOC->append( 'REMBRC',   $TotalRembRC );
    $DOC->append( 'CHARGE',   sprintf( "%1.2f", $TotalCharge ) );
    $DOC->append('TOTALDEVIS');

    $GED->append( 'xCLEGEDi', $TotalDepense );

    $DOC->append('fermetureLT');
    $TRK->track( 'Doc', 1, $NUMCTR, $Nomadh, $Prenomadh, $datedit,
        $Garantie_Ass );
    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);
    $DOC->reset();
    $GED->reset();
    oe_compo_link();
    return 0;
}

sub traite_E1() {
    $DOC->append( 'DEBUT', '' );
      my $civDest = $VALS->{'PENOCOD'};
      $DOC->append( 'civDest', $civDest );
#	warn "INFO : dump E1 ". Dumper($VALS) ."\n";
# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $VALS->{'LICCODP'} );
    $VALS->{'LICCODP'} =~ s/FRANCE//i;

    #ADRESSE DESTINATAIRE
    @DADDR = ();
    $VALS->{'ZONEDHx'} =~ s/^\s+//;
    $ZONEDH = $VALS->{'ZONEDHx'};

    #	if ($VALS->{'ZONEDHx'} eq 'RVC-DC') {
    if ( $ZONEDH eq 'RVC-DC' ) {
        my $line = '.' x 38;
        @DADDR = ($line) x 6;
    }
    else {
        push( @DADDR,
"$VALS->{'LICCODC'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
        );

#		push(@DADDR, "$VALS->{'PNTREMx'} $VALS->{'CPLADRx'}");
#		push(@DADDR, "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}");
#		push(@DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}");
#		if ($VALS->{'INDCDXx'} == 1) {
#			push(@DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}");
#		} else {
#			push(@DADDR, $VALS->{'BOIPOSL'});
#			}
#		push(@DADDR, $VALS->{'LICCODP'});
#		@DADDR = user_cleanup_addr(@DADDR);
    }

    #	$DOC->append_table('DADDR', @DADDR);
    #	$DOC->append_table('xADRLN', @DADDR);
    $nomdest = $VALS->{'PENOLIB'};

    #	my $er =$VALS->{'ODERCOD'};
    #	$ERAFF = $er;
    $ERAFF = $VALS->{'ODERCOD'};
    $DOC->append( 'ERAFF', $ERAFF );
    if ( $ERAFF eq 'D031' || $ERAFF eq 'M031' ) {
        $ville = "Toulouse";
    }
    elsif ( $ERAFF eq 'D033' || $ERAFF eq 'M033' ) {
        $ville = "Bordeaux";
    }

    #	if ($ERAFF eq 'D037' || $ERAFF eq 'M037')
    #		{$ville = "Tours";
    #	}
    elsif ( $ERAFF eq 'D044' || $ERAFF eq 'M044' ) {
        $ville = "Nantes";
    }
    elsif ( $ERAFF eq 'D066' || $ERAFF eq 'M066' ) {
        $ville = "Perpignan";
    }
    elsif ( $ERAFF eq 'D069' || $ERAFF eq 'M069' ) {
        $ville = "Lyon";
    }
    elsif ( $ERAFF eq 'D973' || $ERAFF eq 'M973' ) {
        $ville = "Remire-Montjoly";
    }

    $DOC->append( 'VILLE', $ville );
    if (   $VALS->{'LICCODC'} eq 'Madame'
        || $VALS->{'LICCODC'} eq 'Mademoiselle' )
    {
        my $civadh = "Chère Adhérente";
        $DOC->append( 'CIVADH',  $civadh );
        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
    }
    else {
        my $civadh = "Cher Adhérent";
        $DOC->append( 'CIVADH',  $civadh );
        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
    }

    #	$GED->append('xNOMDEST',$nomdest);
    #	$GED->append('xVILDEST',$VALS->{'NOMCDXL'});
    #	$GED->append('xCPDEST', oe_iso_country() . $VALS->{'CODCDXC'});
    @EADDR = ();
    push( @EADDR, "$VALS->{'PENO025'} $VALS->{'PENO027'}" );
    $DOC->append( 'AGENCE', $VALS->{'PENO027'} );

#	push(@EADDR, "$VALS->{'PNTR028'} $VALS->{'CPLA029'}");
#	my $truc = $VALS->{'PNTR028'}.' '.$VALS->{'CPLA029'};
#	$DOC->append('AGENCEDEUX',$truc);
#	push(@EADDR, "$VALS->{'PEAD030'} $VALS->{'PEAD031'} $VALS->{'LICN034'} $VALS->{'LIBV035'}");
#	push(@EADDR, "$VALS->{'CODC039'} $VALS->{'NOMC040'}");
#	if ($VALS->{'INDCDXx'} == 1) {
#		push(@EADDR, "$VALS->{'BOIP037'} $VALS->{'LIBL038'}");
#	} else {
#		push(@EADDR, $VALS->{'BOIP037'});
#		}
#	my $adresse = $VALS->{'PEAD030'}.' '.$VALS->{'LICN034'}.' '.$VALS->{'LIBV035'};
#	if ($ERAFF eq 'D973' || $ERAFF eq 'M973'){
#		$adresse = $truc.''.$adresse;
#		};

    #	$DOC->append('ADRESSE', $adresse);
    #	$DOC->append('BOITPOS', $VALS->{'BOIP037'});
    #	$DOC->append('CODPOS', $VALS->{'CODC039'});
    #	my $localite = ($VALS->{'LIBL038'});
    #	$DOC->append('LOCALITE', $localite);
    #	@EADDR = user_cleanup_addr(@EADDR);
    #	$DOC->append_table('EADDR', @EADDR);
    #AFFICHAGE NUMERO DE TELEPHONE
    my $tel = $VALS->{"PECO052"};
    $DOC->append( 'tel', $tel );

    1;
}

sub traite_E2() {

    #warn "INFO : dump E2 ". Dumper($VALS) ."\n";
    $nomUser = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
    $DOC->append( 'NOMUSER', $nomUser );
    #########NUMERO CONTRAT INDIVIDUEL############
    $NUMCTR = $VALS->{'ACH1NUM'};
    $DOC->append( 'NUMCTR', $NUMCTR );
    #############INDEXATION GED###################
    $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} ); #Numéro de contrat collectif
    $GED->append( 'xCLEGEDiv', $NUMCTR );    #Numéro de contrat individuel
####################AJOUT DU NOM-PRENOM DU GESTIONNAIRE DANS L'IDEMET POUR SIMPLICATION DE LA GESTION DANS DOCUBASE.############
    my $GEST = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
################################GEST##########################
    $GED->append( 'xIDEMET', $GEST );
    1;
}

###################################################################
# Record E7 ajouté dans le flux pour la norme 38
#
######################################################################
sub traite_E7() {

    #
    #  Transfère d'adresse d'émetteur d'E1 vers  E7
    #
    if ( $ZONEDH ne 'RVC-DC' ) {
        push( @DADDR, "$VALS->{'PNTREMx'} $VALS->{'CPLADRx'}" )
          ;    #PNTREMx,CPLADRx
        push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
        );     # PEADNUM, PEADBTQ, LIBVOIx
        push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );
        warn "DDestAdresse=" . $VALS->{'PNTREMx'} . $VALS->{'CPLADRx'};
        warn "DDestAdresse="
          . $VALS->{'PEADNUM'}
          . $VALS->{'PEADBTQ'}
          . $VALS->{'PEVONAT'}
          . $VALS->{'LIBVOIx'};
        warn "DDestAdresse=" . $VALS->{'CODCDXC'} . $VALS->{'NOMCDXL'};
        warn "DDestAdresse=" . $VALS->{'BOIPOSL'} . $VALS->{'LIBLOCL'};

        if ( $VALS->{'INDCDXx'} == 1 ) {
            push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
        }
        else {
            push( @DADDR, $VALS->{'BOIPOSL'} );
        }
        push( @DADDR, $VALS->{'PELICCO'} );    #  LICCODP
        @DADDR = user_cleanup_addr(@DADDR);
    }
    $DOC->append_table( 'DADDR',  @DADDR );
    $DOC->append_table( 'xADRLN', @DADDR );

    $GED->append( 'xNOMDEST', $nomdest );
    $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} );
    $GED->append( 'xCPDEST',  oe_iso_country() . $VALS->{'CODCDXC'} );

    #
    #  Transfère d'adresse de destinatiare d'E1 vers  E7
    #
    push( @EADDR, "$VALS->{'CPLA015'} $VALS->{'PNTR014'}" ); # PNTR028,  CPLA029
    my $truc = $VALS->{'CPLA015'} . ' ' . $VALS->{'PNTR014'}; # PNTR028, CPLA029
    $DOC->append( 'AGENCEDEUX', $truc );
    push( @EADDR, "$VALS->{'PEAD016'} $VALS->{'PEAD017'} $VALS->{'LIBV019'}" )
      ;    #PEAD030 ,PEAD031 , LICN034 (inexistant dans norme 38), LIBV035
    push( @EADDR, "$VALS->{'CODC023'} $VALS->{'NOMC024'}" );  #CODC039 , NOMC040

    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @EADDR, "$VALS->{'BOIP021'} $VALS->{'LIBV019'}" )
          ;                                                   # BOIP037, LIBL038
    }
    else {
        push( @EADDR, $VALS->{'BOIP021'} );                   #BOIP037
    }
    my $adresse = $VALS->{'PEAD016'} . ' '
      . $VALS->{'LIBV019'};    #PEAD030   , LICN034 (inexistant))   , LIBV035
    if ( $ERAFF eq 'D973' || $ERAFF eq 'M973' ) {
        $adresse = $truc . '' . $adresse;
    }

    $DOC->append( 'ADRESSE', $adresse );
    $DOC->append( 'BOITPOS', $VALS->{'BOIP021'} );    #  BOIP037
    $DOC->append( 'CODPOS',  $VALS->{'CODC023'} );    #  CODC039
    my $localite = ( $VALS->{'LIBV019'} );            #  LIBL038
    $DOC->append( 'LOCALITE', $localite );
    @EADDR = user_cleanup_addr(@EADDR);
    $DOC->append_table( 'EADDR', @EADDR );
}

sub traite_PA {
}

sub traite_PB {

    #    warn "INFO : dump PB ". Dumper($VALS) ."\n";
    $Nomadh    = $VALS->{'PSDENOM'};
    $Prenomadh = $VALS->{'PSDEPRN'};
    $DOC->append( 'NOMADH',    $Nomadh );
    $DOC->append( 'PRENOMADH', $Prenomadh );

    $datedit = $VALS->{'PSREDAT'};

    #    warn "datedit=$datedit   \n";

    $datedit =
        substr( $datedit, 6, 2 ) . '/'
      . substr( $datedit, 4, 2 ) . '/'
      . substr( $datedit, 0, 4 );

    #    warn "datedit=$datedit   \n";

    $DOC->append( 'DATEEDIT', $datedit );
}

sub traite_PC {

    #    warn "INFO : dump PC ". Dumper($VALS) ."\n";

    my $Garant = $VALS->{'PSLD021'};
    if ( exists( $Garantie{$Garant} ) ) {    #warn 'EXISTE '."\n";
    }
    else {
        $tGarantie[$ind_tGarantie] = $Garant;
        $ind_tGarantie++;
        $Garantie{$Garant} = 1;

    }

    my $Garant = $VALS->{'PSLDNUM'};

    #    warn"Garantie = $Garant \n";
    if ( exists( $CodeSS{$Garant} ) ) {      #warn 'EXISTE '."\n";
        $RemRC +=
          $VALS->{'PSLD013'};    # les lignes suivantes, je fais juste le total
    }
    else {
        $CodeSS{$Garant} = 1
          ; # la première ligne doit contenir la nature de garantie, code garantie, depense totale, la base de remb, taux de remb, montant remb,
        $tAnalyse[$X][0] = $VALS->{'PSZZLIB'};
        $tAnalyse[$X][1] = $VALS->{'PSLDCOD'};
        $tAnalyse[$X][2] = $VALS->{'PSLD009'};
        $tAnalyse[$X][3] = $VALS->{'PSLDMNT'};
        $tAnalyse[$X][4] = $VALS->{'PSLOVAL'};
        $tAnalyse[$X][5] = $VALS->{'PSLD012'};
        if ( $X ne 0 ) {
            $tAnalyse[ $X - 1 ][6] = $RemRC;
        } # quand on trouve une nouvelle garantie, on doit sauvegarder le total des remboursements mutulles de l'ancienne garantie
        $RemRC = $VALS->{'PSLD013'}
          ;    # puis remettre la nouvelle valeur pour la garantie suivante.
        $X = $X + 1;
    }

}

exit main(@ARGV);
