#! /usr/bin/perl
use warnings;
use strict;
use feature qw( switch );
use Carp qw( carp croak )
  ; # avoid Perl core bugs: Bizarre copy of hash assignment / attempt to free unfreed scalar
use oEdtk::Main qw( oe_app_usage oe_corporation_set oe_new_job oe_compo_link );
use oEUser::Lib qw( user_corp_file_prefixe user_get_aneto_user );
use oEdtk::Config qw( config_read );
use oEdtk::Tracking;
use Cwd qw ( abs_path );
use Readonly;
use Data::Dumper qw( Dumper );
use Sys::Hostname qw ( hostname );
use Text::CSV_XS qw ( csv );
use MIME::Lite;
use oEdtk::libXls
  qw( prod_Xls_Col_Init prod_Xls_Init prod_Xls_Insert_Val prod_Xls_Edit_Ligne );
use oEdtk::Infinite qw( is_flux is_entete is_ligne id_ligne record_values );
use oEdtk::LaTeX qw( make_index tag ged_tag );
use oEdtk::Restitution qw( follow_path restitution_value );
use oEdtk::Utils;

Readonly my $RESTITUTION_FILE => 'restitution.csv';
Readonly my @TRACKING_FIELDS  => (qw( ID_DEST NCONTRAT SOLDE COURRIER ));

Readonly my $ID_EMETTEUR => 'CTR';

Readonly my $INDEX_DOC_TYPE_VALUE => 'CPRE';

Readonly my $INDEX_DOC_TYPE      => 'xTYPDOC';
Readonly my $INDEX_CLEF_GED      => 'xCLEGED';
Readonly my $INDEX_ADRESSE_LISTE => 'xADRLN';
Readonly my $INDEX_ID_MNT_NUMABA => 'xIDDEST';
Readonly my $INDEX_ID_EMETTEUR   => 'xIDEMET';

Readonly my $TRACK_DOCUMENT => 'D';
Readonly my $TRACK_WARNING  => 'W';

Readonly my $DEBUG => 0 && hostname() eq 'debian';
Readonly my $VERBOSE => $DEBUG;

# What we need to extract from the flow
Readonly my $EXPECTED_VALUES => {
    flush => {
        entete => 1,
        ED     => 1,
    },
    entete => {
        type_d_imprime => [ 'X', 154, 2 ]
        ,    # or use symbolic names like LIBELONLO
        date_de_traitement => [ 'X', 158, 8 ],
    },
    ligne => {
        ED => {
            contrat_individuel => [ 'X', 240,  15 ],
            code_ordonnancier  => [ 'X', 1138, 15 ],
            code_presentation  => [ 'X', 2669, 2 ],
            civilite           => [ 'X', 429,  10 ],
            adresse_payeur     => \&oEdtk::Infinite::ED_adresse_payeur,
            index_variables    => \&oEdtk::Infinite::ED_index_variables,
            ED_restitution     => \&oEdtk::Infinite::ED_restitution,
        },
        I8 => {
            solde_comptable => [ 'S9V9', 187, 12, 2 ],
        },
        Z1 => {
            numaba => [ 'X', 183, 30 ],
        },
        EE => {},
        EF => {},
        EJ => {},
        FI => {},
        EI => {
            EI_restitution => \&oEdtk::Infinite::EI_restitution,
        },
    },
};

# 'Style': A/N Alpha/Number L/C/R Left/Center/Right; then size; Ac Alpha center wrap
# 'Path': of keys to follow in hash; code ref should be final
Readonly my @RESTITUTION_RULES => (

# 0: Title                         # 1: Style  # 2: Path  # 3: Flag_quiet_if_absent (don't warn)
    [ "Civilit\x{e9}", 'AL4',  [ "ED_restitution", "civilite" ] ],
    [ "Nom",           'AL15', [ "ED_restitution", "nom" ] ],
    [ "Pr\x{e9}nom",   'AL15', [ "ED_restitution", "prenom" ] ],
    [ "N\x{ba} adh\x{e9}sion", 'AR10', ["contrat_individuel"] ],
    [ "ID MNT",                'AR8',  ["numaba"] ],
    [ "ER",                    'AL9',  [ "ED_restitution", "ER" ] ],
    [ "centre",                'AL25', [ "ED_restitution", "centre" ] ],
    [ "Adresse",               'AL30', ["adresse_payeur"] ],
    [ "N\x{ba} tel si disponible", 'AR16', undef ],
    [ "Montant solde d\x{e9}biteur", 'NR8', ["solde_comptable"] ],
    [ "\x{c9}tape",                  'AL9', ["action"] ],
    [
        "Type de contrat",
        'AL6', [ "code_ordonnancier", \&ordonnancier_to_type_de_contrat ]
    ],
    [
        "Mode de r\x{e8}glement", 'AL10',
        [ "EI_restitution", "nouveau_mode_de_reglement" ], 'quiet'
    ],    # do not warn
);

# French strings constants
Readonly my @FRENCH_MONTHS => (
    "janvier",   "f\x{e9}vrier", "mars",     "avril",
    "mai",       "juin",         "juillet",  "ao\x{fb}t",
    "septembre", "octobre",      "novembre", "d\x{e9}cembre"
);

# First half of codes (without '2') should be deleted by 2017-07
Readonly my $ORDONNANCIER => {
    map { $_ => 1 }
      qw (

      ACS   MNTRC MUTRC
      MNTPR MUTPR CPLPR

      CC031 CC036 CC037 CC136 CC236

      CG001 CG002 CG003 CG004 CG007 CG008 CG009 CG011 CG012 CG016 CG018 CG022 CG023
      CG024 CG025 CG026 CG028 CG032 CG034 CG036 CG037 CG038 CG039 CG040 CG041 CG042
      CG046 CG047 CG048 CG053 CG054 CG055 CG058 CG059 CG060 CG062 CG063 CG064 CG065
      CG069 CG070 CG071 CG072 CG076 CG078 CG079 CG080 CG083 CG085 CG087 CG088 CG089
      CG091 CG095 CG145 CG190 CG290

      T09PR

      ACS2  MNTR2 MUTR2
      MNTP2 MUTP2 CPLP2

      C2031 C2036 C2037 C2136 C2236

      G2001 G2002 G2003 G2004 G2007 G2008 G2009 G2011 G2012 G2016 G2018 G2022 G2023
      G2024 G2025 G2026 G2028 G2032 G2034 G2036 G2037 G2038 G2039 G2040 G2041 G2042
      G2046 G2047 G2048 G2053 G2054 G2055 G2058 G2059 G2060 G2062 G2063 G2064 G2065
      G2069 G2070 G2071 G2072 G2076 G2078 G2079 G2080 G2083 G2085 G2087 G2088 G2089
      G2091 G2095 G2145 G2190 G2290

      T09P2

      )
};

Readonly my $DOMAINE_SANTE_LIST => {
    map { $_ => 1 }
      qw (
      ACS   MNTRC MUTRC
      ACS2  MNTR2 MUTR2
      )
};    # all the rest: prevoyance

Readonly my $DOMAINE_SANTE      => 'sante';
Readonly my $DOMAINE_PREVOYANCE => 'prevoyance';
Readonly my $DOMAINE_ACS        => 'ACS';

# Types d'imprimes
Readonly my $TYPE_ACTION => 'AC';    # Take an 'action'
Readonly my $TYPE_LIST =>
  'LI';    # Send a list somewhere (somehow a bit like 'action')
Readonly my $TYPE_COURRIER => 'CO';    # Send a letter

# Codes de presentation
Readonly my $PRES_APPEL        => 'AP';    # Appel telephonique
Readonly my $PRES_RADIATION    => 'RA';    # Radiation (resiliation)
Readonly my $PRES_RELANCE      => 'RE';    # Relance lettre / mandat SEPA
Readonly my $PRES_ENVOI_SMS    => 'SM';    # Envoi de SMS
Readonly my $PRES_MISE_DEMEURE => 'MD';    # Mise en demeure
Readonly my $PRES_CLOTURE      => 'CL';    # Suivi de dossier avant cloture

# Actions resultantes
Readonly my $ACTION_APPEL_TELEPHONIQUE       => 'appel telephonique';
Readonly my $ACTION_LISTE_RADIATION          => 'liste radiation';
Readonly my $ACTION_COURRIER_RADIATION       => 'courrier radiation';
Readonly my $ACTION_COURRIER_RELANCE         => 'courrier relance';
Readonly my $ACTION_LISTE_SMS                => 'envoi de SMS';
Readonly my $ACTION_LISTE_CLOTURE            => 'suivi avant cloture';
Readonly my $ACTION_COURRIER_MISE_EN_DEMEURE => 'mise en demeure';

# Actions resultantes (version courte pour le tracking)
Readonly my $ACT_APPEL_TELEPHONIQUE       => 'appel';
Readonly my $ACT_LISTE_RADIATION          => 'liste radiation';
Readonly my $ACT_COURRIER_RADIATION       => 'radiation';
Readonly my $ACT_COURRIER_RELANCE         => 'relance';
Readonly my $ACT_LISTE_SMS                => 'SMS';
Readonly my $ACT_LISTE_CLOTURE            => 'suivi avant cloture';
Readonly my $ACT_COURRIER_MISE_EN_DEMEURE => 'MED';

Readonly my $ACTION_TO_TAKE => {
    $TYPE_ACTION => {
        $PRES_APPEL     => $ACTION_APPEL_TELEPHONIQUE,
        $PRES_ENVOI_SMS => $ACTION_LISTE_SMS,
    },
    $TYPE_COURRIER => {
        $PRES_RADIATION    => $ACTION_COURRIER_RADIATION,
        $PRES_RELANCE      => $ACTION_COURRIER_RELANCE,
        $PRES_MISE_DEMEURE => $ACTION_COURRIER_MISE_EN_DEMEURE,
    },
    $TYPE_LIST => {
        $PRES_RADIATION => $ACTION_LISTE_RADIATION,
        $PRES_CLOTURE   => $ACTION_LISTE_CLOTURE,
    },
};

# Global variables
my ( $TRK, $STATS_ACTION, @ALERT );
my $PDF_HAS_CONTENT = 0;

sub flatten_hash {
    my ($h) = @_;
    my @res = ();
    foreach my $key ( sort keys %{$h} ) {
        my $val = $h->{$key};
        my $ref = ref $val;
        given ($ref) {
            when ( $ref eq "" ) { push @res, "$key: $val"; }
            when ( $ref eq "ARRAY" ) {
                push @res, map { "$key: $_" } @{$val};
            }
            when ( $ref eq "HASH" ) {
                push @res, map { "$key: $_" } flatten_hash($val);
            }
        }
    }
    return @res;
}

sub alert {
    my ( $flowFile, $lineNumber, $msg, @objs ) = @_;
    my @row = ( $flowFile, $lineNumber, $msg );
    foreach my $obj (@objs) {
        my $ref = ref $obj;
        given ($ref) {
            when ( $ref eq "" )      { push @row, $obj; }
            when ( $ref eq "ARRAY" ) { push @row, @{$obj}; }
            when ( $ref eq "HASH" )  { push @row, flatten_hash($obj); }
        }
    }
    push @ALERT, \@row;
    return ();
}

sub email_alerts {
    my $n = scalar @ALERT;
    return 0 unless $n;

    my $cfg = config_read('MAIL');
    my $to =
      exists $cfg->{'EDTK_MAIL_PRECONTENTIEUX_METIER'}
      ? $cfg->{'EDTK_MAIL_PRECONTENTIEUX_METIER'}
      : '';
    my $cc =
      exists $cfg->{'EDTK_MAIL_PRECONTENTIEUX_ANOMALIE'}
      ? $cfg->{'EDTK_MAIL_PRECONTENTIEUX_ANOMALIE'}
      : '';
    if ( ( $to !~ m{\S}sx ) && ( $cc !~ m{\S}sx ) ) {
        carp "E-mail not configured. Bailing out";
        return 1;
    }
    my $from =
      exists $cfg->{'EDTK_MAIL_SENDER'} ? $cfg->{'EDTK_MAIL_SENDER'} : '';
    my $subject =
      exists $cfg->{'EDTK_MAIL_PRECONTENTIEUX_SUBJECT'}
      ? $cfg->{'EDTK_MAIL_PRECONTENTIEUX_SUBJECT'}
      : '';
    $from    = 'nobody@localhost'          unless $from =~ m{\S}sx;
    $subject = 'precontentieux: anomalies' unless $subject =~ m{\S}sx;

    my $csv;
    unshift @ALERT, [ "File", "Line #", "Message", "Objects" ];
    csv(
        in       => \@ALERT,
        out      => \$csv,
        encoding => 'UTF-8',
        sep_char => ";",
        eol      => "\r\n"
    );

    my $msg = MIME::Lite->new(
        From => $from,
        ( $to =~ m{\S}sx ) ? ( To => $to ) : (),
        ( $cc =~ m{\S}sx ) ? ( Cc => $cc ) : (),
        Subject => $subject,
        Type    => 'multipart/mixed',
    );
    $msg->attach(
        Type     => 'text/csv',
        Data     => $csv,
        Filename => "anomalies.csv",
    );
    $msg->send();

    my $str = "to='$to' cc='$cc' from='$from' subject='$subject' n=$n";
    die "Could not send anomalies e-mail ($str)"
      unless $msg->last_send_successful();
    print STDERR "Anomalies e-mail successfully sent ($str)", "\n";
    return 0;
}

sub first_day_of_next_month {
    my ( $str, $flowFile, $lineNumber ) = @_;
    return alert( $flowFile, $lineNumber, "bad date '$str'" )
      if length($str) != 8;
    my $mm = substr( $str, 4, 2 );
    return alert( $flowFile, $lineNumber, "bad month '$mm' in date '$str'" )
      if ( $mm < 1 ) || ( $mm > 12 );
    $mm = 0 if $mm == 12;
    return "premier $FRENCH_MONTHS[$mm]";
}

sub init_XLS {
    prod_Xls_Col_Init( map { $$_[1] } @RESTITUTION_RULES ); # must be done first
    prod_Xls_Init( 'M.N.T.', "Restitution pr\x{e9}contentieux" );
    prod_Xls_Insert_Val( $$_[0] )
      foreach @RESTITUTION_RULES;                           # add column titles
    return prod_Xls_Edit_Ligne( 'T2', 'HEAD' );    #  line style / table header
}

sub edit_ligne_XLS {
    my ($h) = @_;
    prod_Xls_Insert_Val( restitution_value( $h, $$_[2], $$_[3] ) )
      foreach @RESTITUTION_RULES;
    return prod_Xls_Edit_Ligne();
}

sub init {
    my ( $flowFile, $trackingJob ) = @_;
    return alert( $flowFile, -1, 'file does not exist' )  unless -e $flowFile;
    return alert( $flowFile, -1, 'file is not readable' ) unless -r $flowFile;
    unless ( defined $trackingJob ) {
        $trackingJob = abs_path($flowFile);
        carp "Using trackingJob='$trackingJob'";
    }
    user_corp_file_prefixe( $flowFile, '_' );
    my $user = user_get_aneto_user($flowFile);

    # $DOC = oEdtk::TexDoc->new();
    $TRK = track_begin( $trackingJob, $user );
    init_XLS();
    return;
}

sub track_begin {
    my ( $trackingJob, $user ) = @_;
    return oEdtk::Tracking->new(
        $trackingJob,
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => \@TRACKING_FIELDS
    );
}

sub track {
    my ( $letter, $l ) = @_;
    carp "TRACKING: " . join( "\x20", $letter, @{$l} ) if $DEBUG;
    return $TRK->track( $letter, 1, @{$l} );
}

sub track_end {
    return $TRK->track( $TRACK_WARNING, 1, '', '', '', '',
        'Fin de traitement' );    # "W" as in "Warning"
}

sub ordonnancier_to_domaine {
    my ($str) = @_;
    return ( exists $DOMAINE_SANTE_LIST->{$str} )
      ? $DOMAINE_SANTE
      : $DOMAINE_PREVOYANCE;
}

sub ordonnancier_to_type_de_contrat {
    my ($str) = @_;
    return
        ( substr( $str, 0, 3 ) eq 'ACS' ) ? $DOMAINE_ACS
      : ( exists $DOMAINE_SANTE_LIST->{$str} ) ? $DOMAINE_SANTE
      :                                          $DOMAINE_PREVOYANCE;
}

sub common_parameters {
    my ( $h, %args ) = @_;
    return {
        Civilite            => $h->{"civilite"},
        AdresseDestinataire => $h->{"adresse_payeur"},
        SoldeDuCompte       => $h->{"solde_comptable"},
        NumeroAba           => $h->{"numaba"},
        NumeroContrat       => $h->{"contrat_individuel"},
        %args
    };
}

sub check_common_parameters {
    my ( $h, $flowFile, $lineNumber ) = @_;
    foreach my $key (
        qw(civilite adresse_payeur solde_comptable numaba contrat_individuel))
    {
        return alert( $flowFile, $lineNumber, "key '$key' missing", $h )
          if !exists $h->{$key};
        return alert( $flowFile, $lineNumber, "key '$key' undef", $h )
          if !defined $h->{$key};
    }
    return 1;
}

sub track_values {
    my ( $h, $type ) = @_;
    return [
        ( map { $h->{$_} } qw( numaba contrat_individuel solde_comptable ) ),
        $type
    ];
}

sub track_and_index {
    my ( $h, $type ) = @_;
    my $track_values = track_values( $h, $type );
    track( $TRACK_DOCUMENT, $track_values );
    $PDF_HAS_CONTENT = 1;
    return make_index(
        $h->{'index_variables'},
        $INDEX_DOC_TYPE         => $INDEX_DOC_TYPE_VALUE,
        "$INDEX_CLEF_GED#roman" => $track_values,
        $INDEX_ADRESSE_LISTE    => $h->{'adresse_payeur'},
        $INDEX_ID_MNT_NUMABA    => $h->{'numaba'},
        $INDEX_ID_EMETTEUR      => $ID_EMETTEUR,
    );
}

sub track_warning {
    my ( $h, $type ) = @_;
    return track( $TRACK_WARNING, track_values( $h, $type ) );
}

sub produce_letter_relance_cheque_mandat_SEPA {
    my ( $h, $e, $flowFile, $lineNumber ) =
      @_;    # e unused, symmetry with radiation
    return unless check_common_parameters( $h, $flowFile, $lineNumber );
    my $idx = track_and_index( $h, $ACT_COURRIER_RELANCE ),
      my $tag = tag( "relanceChequeMandatSEPA", common_parameters($h) );
    return ( ref $tag )
      ? alert( $flowFile, $lineNumber, @{$tag} )
      : $idx . $tag;
}

sub produce_letter_mise_en_demeure {
    my ( $h, $e, $flowFile, $lineNumber ) =
      @_;    # e unused, symmetry with radiation
    return unless check_common_parameters( $h, $flowFile, $lineNumber );
    my $idx = track_and_index( $h, $ACT_COURRIER_MISE_EN_DEMEURE );
    my $tag = tag( "miseEnDemeureLRAR", common_parameters($h) );
    return ( ref $tag )
      ? alert( $flowFile, $lineNumber, @{$tag} )
      : $idx . $tag;
}

sub produce_letter_radiation {
    my ( $h, $e, $flowFile, $lineNumber ) = @_;
    return unless check_common_parameters( $h, $flowFile, $lineNumber );
    my $domain = ordonnancier_to_domaine( $h->{'code_ordonnancier'} );
    return alert( $flowFile, $lineNumber,
        "No automatic radiation for domain '$domain'", $h )
      if $domain ne $DOMAINE_SANTE;
    my $idx = track_and_index( $h, $ACT_COURRIER_RADIATION );
    my $tag = tag(
        "radiationSanteACS",
        common_parameters(
            $h, PremierJourDuMoisSuivant => $e->{"premier_jour_du_mois_suivant"}
        )
    );
    return ( ref $tag )
      ? alert( $flowFile, $lineNumber, @{$tag} )
      : $idx . $tag;
}

sub action_to_take {
    my ( $h, $e, $flowFile, $lineNumber ) = @_;

    return alert( $flowFile, $lineNumber, "no 'type d'imprime'", $e )
      unless exists $e->{'type_d_imprime'};
    my $type = $e->{'type_d_imprime'};
    return alert( $flowFile, $lineNumber, "unknown type d'imprime '$type'", $e )
      unless exists $ACTION_TO_TAKE->{$type};

    return alert( $flowFile, $lineNumber, "no 'code ordonnancier'", $h )
      unless exists $h->{'code_ordonnancier'};
    my $ordo = $h->{'code_ordonnancier'};
    return alert( $flowFile, $lineNumber, "unknown code ordonnancier '$ordo'",
        $h )
      unless exists $ORDONNANCIER->{$ordo};

    return alert( $flowFile, $lineNumber, "no 'code presentation'", $h )
      unless exists $h->{'code_presentation'};
    my $pres = $h->{'code_presentation'};
    return alert( $flowFile, $lineNumber,
        "Unknown code de presentation '$pres' for type d'imprime '$type'",
        $e, $h )
      unless exists $ACTION_TO_TAKE->{$type}->{$pres};

    return $ACTION_TO_TAKE->{$type}->{$pres};
}

sub produce_letter {
    my ( $h, $e, $flowFile, $lineNumber ) = @_;
    my @a = ( $h, $e, $flowFile, $lineNumber );
    my $action = action_to_take(@a);
    return unless defined $action;
    $STATS_ACTION->{$action}++;
    $h->{'action'} = $action;
    edit_ligne_XLS($h);
    given ($action) {
        when ( $action eq $ACTION_COURRIER_RELANCE ) {
            return produce_letter_relance_cheque_mandat_SEPA(@a);
        }
        when ( $action eq $ACTION_COURRIER_RADIATION ) {
            return produce_letter_radiation(@a);
        }
        when ( $action eq $ACTION_COURRIER_MISE_EN_DEMEURE ) {
            return produce_letter_mise_en_demeure(@a);
        }

        # not implemented yet (TODO later)
        when ( $action eq $ACTION_APPEL_TELEPHONIQUE ) {
            return track_warning( $h, $ACT_APPEL_TELEPHONIQUE );
        }
        when ( $action eq $ACTION_LISTE_SMS ) {
            return track_warning( $h, $ACT_LISTE_SMS );
        }
        when ( $action eq $ACTION_LISTE_RADIATION ) {
            return track_warning( $h, $ACT_LISTE_RADIATION );
        }
        when ( $action eq $ACTION_LISTE_CLOTURE ) {
            return track_warning( $h, $ACT_LISTE_CLOTURE );
        }
        default {
            return alert( $flowFile, $lineNumber, "Unknown action '$action'",
                $h );
        }
    }
}

sub treat_entete {
    my ( $h, $e, $lineRef, $flowFile, $lineNumber ) = @_;
    $h = flush_hash( $h, $e, $flowFile, $lineNumber )
      if exists $EXPECTED_VALUES->{'flush'}->{'entete'};
    $e = {};
    return ( $h, $e ) unless exists $EXPECTED_VALUES->{'entete'};
    my @errors = record_values( $e, $EXPECTED_VALUES->{'entete'}, $lineRef );
    return alert( $flowFile, $lineNumber, join( "\n", @errors ) )
      if scalar @errors;
    $e->{"premier_jour_du_mois_suivant"} =
      first_day_of_next_month( $e->{'date_de_traitement'},
        $flowFile, $lineNumber );
    return ( $h, $e );
}

sub treat_line {
    my ( $h, $e, $lineRef, $flowFile, $lineNumber ) = @_;
    my $id = id_ligne($lineRef);
    return alert( $flowFile, $lineNumber, "Unexpected line id '$id'" )
      unless exists $EXPECTED_VALUES->{'ligne'}->{$id};
    $h = flush_hash( $h, $e, $flowFile, $lineNumber )
      if exists $EXPECTED_VALUES->{'flush'}->{$id};
    my @errors =
      record_values( $h, $EXPECTED_VALUES->{'ligne'}->{$id}, $lineRef );
    return alert( $flowFile, $lineNumber, join( "\n", @errors ) )
      if scalar @errors;
    return $h;
}

sub flush_hash {
    my ( $h, $e, $flowFile, $lineNumber ) = @_;
    if ( scalar keys %{$h} ) {
        my $letter = produce_letter( $h, $e, $flowFile, $lineNumber );

        # print oEdtk::Main::OUT $letter, "\n"
        oEdtk::Main::oe_print_to_output_file( sprintf '%s%s', $letter, "\n" )
          if defined $letter && length $letter;
    }
    return {};    # reset h
}

sub treat_input {
    my ( $h, $e, $lineRef, $flowFile, $lineNumber ) = @_;

    # Should be useless if the input received was not garbage
    oEdtk::Utils::kill_non_latin_1_9_chars_in_place( \$lineRef );
    given ($lineRef) {
        when ( is_flux($lineRef) ) { }
        when ( is_entete($lineRef) ) {
            ( $h, $e ) =
              treat_entete( $h, $e, $lineRef, $flowFile, $lineNumber );
        }
        when ( is_ligne($lineRef) ) {
            $h = treat_line( $h, $e, $lineRef, $flowFile, $lineNumber );
        }
        default {
            alert( $flowFile, $lineNumber,
                "cannot guess line type for '$$lineRef'" );
        }
    }
    return ( $h, $e );
}

sub treat_flow {
    my ($flowFile) = @_;
    my ( $h, $e ) =
      ( {}, {} ); # hashes for flow/flux / entetes (shared between several flux)
    open my $io, '<', $flowFile or die "$flowFile: $!";

    # Overwrite default xSOURCE tag (hardcoded in the LaTeX class)
    my $ged_tag = ged_tag(
        oEdtk::LaTeX::ged_tag_xSOURCE(),
        oEdtk::LaTeX::ged_value_precontentieux()
    );
    die "Bad ged_tag: " . Dumper($ged_tag) if ref $ged_tag;

    # print oEdtk::Main::OUT $ged_tag, "\n";
    oEdtk::Main::oe_print_to_output_file( sprintf '%s%s', $ged_tag, "\n" );

    ( $h, $e ) = treat_input( $h, $e, \$_, $flowFile, $. ) while <$io>;
    close($io) || 1;
    flush_hash( $h, $e, $flowFile, $. );    # do not forget the last record!
    return;
}

sub main {
    my ($flowFile) = @_;
    oe_app_usage() unless defined $flowFile;
    init($flowFile);
    oe_new_job('--index');
    treat_flow($flowFile);
    track_end();
    printf STDERR "%5d '%s'\n", $STATS_ACTION->{$_}, $_
      foreach sort keys %{$STATS_ACTION};
    oe_compo_link() if $PDF_HAS_CONTENT;
    return email_alerts();
}

exit main(@ARGV);
