#!/usr/bin/perl
use strict;
use utf8;
use feature "switch";
use warnings;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::HV;
use oEUser::Descriptor::X1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Carp qw( croak );
use Readonly;

my $INFO;
my ( $TRK, $GED, $DOC, $VALS, $COURRIER, $P, $LISTE );
my (
    @FADDR,    $NUMCTR,      $NUMABA, $MONTANT,     $CDE_COUR, $nomUser,
    $SUIVIPAR, @tab_options, $ERAFF,  $EMETTEURGED, $CIVUTIL
);
my $OPT_GED;
my ( $NOMEMETTEUR1, $NOMEMETTEUR2, $truc, $NOMDESTINATAIRE, $ZONEDH );

my $ville = "Villeurbanne";

Readonly my $xTypeDocMap => {
    "CCA-01" => "CREM",
    "CCA-02" => "CREM",
    "CCA-03" => "CREM",
    "CCA-04" => "CREM",
    "CEA-01" => "CREA",
    "CEA-02" => "CREA",
    "CMA-01" => "CREM",
    "CMA-02" => "CREM",
    "CMA-03" => "CREM",
    "CTA-01" => "CREA",
    "CTA-02" => "CRER",
    "CTA-03" => "CREA",
    "CTA-04" => "CREA",
    "CTA-05" => "CREA",
    "CTA-06" => "CREA",
    "CTA-07" => "CREA",
    "CTA-08" => "CRER",
    "CTA-09" => "CREA",
    "CTA-10" => "CREA",
    "CTA-11" => "CREA",
    "CTA-12" => "CRER",
    "CTA-13" => "CREA",
    "CTA-14" => "CRER",
    "CTA-15" => "CRER",
    "CTA-16" => "CRER",
    "CTA-17" => "CRER",
    "CTA-18" => "CRER",
    "CTA-19" => "CREA",
    "CTA-20" => "CREA",
    "CTA-21" => "CREA",
    "CTA-22" => "CRER",
    "CTA-23" => "CREA",
    "CTA-24" => "CRER",
    "CTA-25" => "CRER",
    "CTA-26" => "CREA",
    "CTA-27" => "CREA",
    "CTA-28" => "CBAD",
    "CTA-29" => "CREA",
    "CTA-30" => "CREA",
    "CTA-31" => "CREA",
    "CTA-32" => "CREA",
    "CTA-33" => "CREA",
    "CTA-34" => "CREA",
    "CTA-35" => "CREA",
    "CTA-36" => "CREA",
    "CTT-01" => "CRET",
    "CTT-02" => "CRET",
    "CTT-03" => "CRET",
    "CTT-04" => "CRET",
    "CTT-05" => "CRET",
    "CTT-06" => "CRET",
    "CTT-07" => "CRET",
    "CTT-08" => "CRET",
    "CTT-09" => "CRET",
    "CTT-10" => "CRET",
    "CTT-11" => "CRET",
    "CTT-12" => "CRET",
    "CTT-13" => "CRET",

    "ACS-01" => "CREA",
    "ACS-02" => "CREA",
    "CTA-19" => "CRER",
    "CMU-01" => "CREA",
    "CMU-02" => "CREA",
    "CMU-03" => "CREA",
    "CMU-04" => "CREA",
    "CMU-05" => "CRER",
    "CMU-06" => "CREA",
    "CTT-14" => "CRER",
    "CTT-15" => "CRER",
    "CTT-16" => "CRER",
    "CTT-17" => "CRET",
    "CTT-18" => "CRER",
    "CTT-19" => "CRET",
};

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = "";

    # SI ON SOUHAITE METTRE LE DOCUMENT EN GED,
    # IL SUFFIT DE PASSER L'OPRION --edms à oe_compo_link COMME CI-DESSOUS
    #	$OPT_GED='--edms';
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMCTR', 'NUMABA', 'GESTIONNAIRE', 'CDE_COUR', 'MONTANT' ]
    );
    oe_new_job('--index');
    &init_Doc();
    my $E1 = oEUser::Descriptor::E1->get();

    # $E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    #$E2->debug();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    # $E7->debug();
    $E7->bind_all_c7();
    my $H1 = oEUser::Descriptor::H1->get();

    #$H1->debug();
    $H1->bind_all_c7();
    my $H4 = oEUser::Descriptor::H4->get();
    $H4->bind_all_c7();
    my $X1 = oEUser::Descriptor::X1->get();

#    $X1->debug();
    $X1->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    #$Z1->debug();
    $Z1->bind_all_c7();
    $DOC   = oEdtk::TexDoc->new();
    $GED   = oEdtk::TexDoc->new();
    $LISTE = oEdtk::TexDoc->new();
    $P     = oEdtk::RecordParser->new(
        oe_get_handle_to_input_file(),
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'H1' => $H1,
        'H4' => $H4,
        'X1' => $X1,
        'Z1' => $Z1
    );
    $P->set_motif_to_denormalized_split('\x{01}');
    my $lastid;
    my $firstDoc = 0;
    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'X1' ) {

            # 			warn "INFO : dump X1 ". Dumper($VALS) ."\n";
            if ( !$firstDoc ) {
                $firstDoc = 1;
            }
            else {
                $DOC->append('FINDOC');
                $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $nomUser, $CDE_COUR,
                    $MONTANT );
                &init_Doc();
            }
            &traite_X1();    #traitement du record X1#
        }
        elsif ( $id eq 'E1' ) {
            if ( !$firstDoc ) {
                $firstDoc = 1;
            }
            else {
                $DOC->append('FINDOC');
                $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $nomUser, $CDE_COUR,
                    $MONTANT );
                &init_Doc();
            }
            &traite_E1();    #traitement du record E1#
        }
        elsif ( $id eq 'E2' ) {
            &traite_E2();    #traitement du record E2#
        }
        elsif ( $id eq 'E7' ) {
            &traite_E7();    #traitement du record E7#
        }
        elsif ( $id eq 'H1' ) {
            &traite_H1();    #traitement du record H1#
        }
        elsif ( $id eq 'H4' ) {    #record H4 non utilisé#
        }
        elsif ( $id eq 'OPTION' ) {
            &traite_option();      #traitement du record option#
        }
        elsif ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }
        $lastid = $id;
        affichage_information()
          ;        #traitement de la ligne d'information en fin de courrier#
    }
    $GED->append( 'xSOURCE', 'BUR' );
    croak "Type de document inconnu pour le courrier $CDE_COUR"
      unless defined $xTypeDocMap->{$CDE_COUR};
    $DOC->append( 'xTYPDOC', $xTypeDocMap->{$CDE_COUR} );

    $DOC->append('LETTRE');
    $DOC->append('FINDOC');
    $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $nomUser, $CDE_COUR, $MONTANT );

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    $CDE_COUR = "";
    $DOC->reset();
    $GED->reset();
    oe_compo_link($OPT_GED);
    return 0;
}

sub check_first_amount {
    my @values = @_;

    foreach my $val (@values) {
        $val =~ s/\s//g;
        if ( $val =~ /^[\d\.\,]+$/ ) {

            # warn "INFO : \$val = $val \n";
            return $val;
        }
    }

  # si on est là c'est qu'on a pas trouvé de valeur numérique de type montant
  # on retourne zéro uniquement s'il n'y a aucune valeure définie
  #    if ( !defined $val ) { return 0; }
  #    return $val;
    return 0;
}

sub init_Doc {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    undef @FADDR;
    $CDE_COUR = "";
    $nomUser  = "";
    $INFO     = "";
    return 0;
}

sub affichage_information {
    if ( $CDE_COUR =~ /CTT-05|CTT-07|CTT-09|CTT-11|CTT-13|CTT-15/ ) {
        $INFO = 'N';
    }
    else {
        $INFO = 'O';
    }
    $DOC->append( 'INFO', $INFO );
    return 0;
}

sub avec_adresse {
    $DOC->append( 'CIVDEST',  $VALS->[5] );
    $DOC->append( 'MONTANT',  check_first_amount( $VALS->[6] ) );
    $GED->append( 'xCLEGEDi', $VALS->[6] );
    push( @FADDR, $VALS->[1] );
    push( @FADDR, $VALS->[2] );
    if ( $VALS->[3] !~ /^\./ ) {
        push( @FADDR, $VALS->[3] );
    }
    push( @FADDR, $VALS->[4] );
    @FADDR = user_cleanup_addr(@FADDR);
    $DOC->append_table( 'FADDR',  @FADDR );
    $DOC->append_table( 'xADRLN', @FADDR );
    return 0;
}

sub traite_X1 {
#    warn "INFO : dump X1 ". Dumper($VALS) ."\n";
    $DOC->append( 'DEBUT', '' );
    my $civDest = $VALS->{'CODCIVx'};
    $DOC->append( 'civDest', $civDest );

    #ADRESSE DESTINATAIRE
    my @BADDR = ();
    $VALS->{'ZONEDHx'} =~ s/^\s+//;
    push( @BADDR, "$VALS->{'CODC016'}  $VALS->{'NOMxxxx'} $VALS->{'PREN021'}" );
    push( @BADDR, "$VALS->{'LIEUxxx'} $VALS->{'ADRESSx'}" );
    push( @BADDR,
"$VALS->{'VOIExxx'} $VALS->{'CODBTQx'} $VALS->{'VOIE027'} $VALS->{'VOIE029'}"
    );

    if ( $VALS->{'CEDEXxx'} == 1 ) {
        push( @BADDR, "$VALS->{'CCxxxxx'} $VALS->{'ACHxxxx'}" );
    }
    else {
        #push(@BADDR, $VALS->{'LIEU031'});
        push( @BADDR, "$VALS->{'CCxxxxx'} $VALS->{'ACHxxxx'}" );
    }
    @BADDR = user_cleanup_addr(@BADDR);
    $DOC->append_table( 'BADDR', @BADDR );
    my $nomdest = "$VALS->{'CODC016'}  $VALS->{'NOMxxxx'} $VALS->{'PREN021'}";
    $GED->append( 'xNOMDEST', $nomdest );
    $GED->append( 'xVILDEST', $VALS->{'LOCALIT'} );
    $CDE_COUR = $VALS->{'RESE065'};
    $CDE_COUR =~ s/^\s+//;

    # 	warn "INFO : CDE_COUR = $CDE_COUR \n";
    if ( $VALS->{'CODC002'} eq "Monsieur" ) {
        $DOC->append( 'CIVLONG', 'Monsieur' );
        $CIVUTIL = "M";
    }
    else {
        $DOC->append( 'CIVLONG', 'Madame' );
        $CIVUTIL = "Mme";
    }
    $SUIVIPAR    = $VALS->{'UTILxxx'} . " " . $VALS->{'PRENOMx'};
    $EMETTEURGED = $VALS->{'UTILxxx'} . " " . $VALS->{'PRENOMx'};
    $DOC->append( 'NOMUSER', $SUIVIPAR );
    $GED->append( 'xIDEMET', $VALS->{'ENFNCOD'} );
    $DOC->append( 'FINESS',  $VALS->{'ENFNCOD'} );
  SWITCH: {
        $CDE_COUR eq "CTT-01"
          && do {
            $DOC->include( "CRB-EACEX-CTTMA.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        $CDE_COUR eq "CTT-02"
          && do {
            $DOC->include( "CRB-EACEX-CTTNP.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        $CDE_COUR eq "CTT-03"
          && do {
            $DOC->include( "CRB-EACEX-CTTUP.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        $CDE_COUR eq "CTT-04"
          && do {
            $DOC->include( "CRB-EACEX-CTTDIC.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        $CDE_COUR eq "CTT-05"
          && do {
            $DOC->include( "CRB-EACEX-CTTFAER.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-06" || $CDE_COUR eq "CTT-07" )
          && do {
            if ( $CDE_COUR eq "CTT-06" ) {
                $DOC->append( 'CTT', 6 );
            }
            else {
                $DOC->append( 'CTT', 7 );
            }
            $DOC->include( "CRB-EACEX-CTTOPPE.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-08" || $CDE_COUR eq "CTT-09" )
          && do {
            if ( $CDE_COUR eq "CTT-08" ) {
                $DOC->append( 'CTT', 8 );
            }
            else {
                $DOC->append( 'CTT', 9 );
            }
            $DOC->include( "CRB-EACEX-CTTIDBE.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-10" || $CDE_COUR eq "CTT-11" )
          && do {
            if ( $CDE_COUR eq "CTT-10" ) {
                $DOC->append( 'CTT', 10 );
            }
            else {
                $DOC->append( 'CTT', 11 );
            }
            $DOC->include( "CRB-EACEX-CTTCMFP.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-12" || $CDE_COUR eq "CTT-13" )
          && do {
            if ( $CDE_COUR eq "CTT-12" ) {
                $DOC->append( 'CTT', 12 );
            }
            else {
                $DOC->append( 'CTT', 13 );
            }
            $DOC->include( "CRB-EACEX-CTTCSC.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        (        $CDE_COUR eq "CTT-14"
              || $CDE_COUR eq "CTT-15"
              || $CDE_COUR eq "CTT-16"
              || $CDE_COUR eq "CTT-17"
              || $CDE_COUR eq "CTT-18"
              || $CDE_COUR eq "CTT-19" )
          && do {
            my ($ctt) = $CDE_COUR =~ /CTT\-(\d+)/;
            $DOC->append( 'CTT', $ctt );
            $DOC->include( "CRB-EACEX-CTT-1419.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
        ( $CDE_COUR eq "CMU-03" )
          && do {
            my ($ctt) = $CDE_COUR =~ /CMU\-(\d+)/;
            $DOC->append( 'CMU', $ctt );
            $DOC->include( "CRB-EACEX-CTTLOD.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };

    }
    return 0;
}

sub traite_E1 {

 # NORME 38 : ON N'UTILISE PLUS LA RECORD E1 POUR LES ADRESSES : REMPLCEE PAR E7
 # warn "INFO : dump E1 ". Dumper($VALS) ."\n";

    $DOC->append( 'DEBUT', '' );

###############ADRESSE DESTINATAIRE###############
    my @DADDR = ();
    $ZONEDH = $VALS->{'ZONEDHx'};
    my $civDest = $VALS->{'PENOCOD'};
    $DOC->append( 'civDest', $civDest );
    my $nomdest = $VALS->{'PENOLIB'};
    $NOMDESTINATAIRE =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
###################### VILLE #######################
    my $er = $VALS->{'ODERCOD'};
    $ERAFF = $er;

    #récuperer le code courrier dans le flux
    my $CODE_COUR = $VALS->{'ZONEDHx'};
    $CODE_COUR =~ s/\s+//;    #vire tous les espaces
     #warn "\nINFO : CODE_COUR=". $CODE_COUR ." / RECH=". index($CODE_COUR,"CTA-") ." \n";

    #warn "INFO : ERAFF=". $ERAFF ."\n \n";
    $DOC->append( 'ERAFF', $ERAFF );

    if ( $ERAFF eq 'D008' || $ERAFF eq 'M008' ) {
        if ( index( $CODE_COUR, "CTA-" ) >= 0 ) {
            $ville = "Villeurbanne";
        }
        else {
            $ville = "Charleville Mézières";
        }
    }
    if ( $ERAFF eq 'D031' || $ERAFF eq 'M031' ) {
        $ville = "Toulouse";
    }
    if ( $ERAFF eq 'D033' || $ERAFF eq 'M033' ) {
        $ville = "Bordeaux";
    }
    if ( $ERAFF eq 'D037' || $ERAFF eq 'M037' ) {
        if ( index( $CODE_COUR, "CTA-" ) >= 0 ) {
            $ville = "Villeurbanne";
        }
        else {
            $ville = "Tours";
        }
    }
    if ( $ERAFF eq 'D044' || $ERAFF eq 'M044' ) {
        $ville = "Nantes";
    }
    if ( $ERAFF eq 'D066' || $ERAFF eq 'M066' ) {
        $ville = "Perpignan";
    }
    if ( $ERAFF eq 'D069' || $ERAFF eq 'M069' ) {
        $ville = "Lyon";
    }
    if ( $ERAFF eq 'D973' || $ERAFF eq 'M973' ) {
        $ville = "Remire-Montjoly";
    }

    $DOC->append( 'VILLE', $ville );

######### CIVILITE ##############
    if (   $VALS->{'LICCODC'} eq 'Madame'
        || $VALS->{'LICCODC'} eq 'Mademoiselle' )
    {
        my $civadh = "Chère Adhérente";
        $DOC->append( 'CIVADH',  $civadh );
        $DOC->append( 'CIVLONG', 'Madame' );
        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
    }
    else {
        my $civadh = "Cher Adhérent";
        $DOC->append( 'CIVLONG', 'Monsieur' );
        $DOC->append( 'CIVADH',  $civadh );
        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
    }
    $GED->append( 'xNOMDEST', $nomdest );

################ADRESSE EMETTEUR#########################
    my @EADDR = ();

    $NOMEMETTEUR1 = "$VALS->{'PENO025'} $VALS->{'PENO027'}";
    $DOC->append( 'AGENCE', $VALS->{'PENO027'} );
    $NOMEMETTEUR2 = "$VALS->{'PNTR028'} $VALS->{'CPLA029'}";
    $truc         = $VALS->{'PNTR028'} . ' ' . $VALS->{'CPLA029'};
    $DOC->append( 'AGENCEDEUX', $truc );

############AFFICHAGE NUMERO DE TELEPHONE########################
    my $tel = $VALS->{"PECO052"};
    $DOC->append( 'tel', $tel );
    return 0;
}

sub traite_E2 {

    #warn "INFO : dump E2 ". Dumper($VALS) ."\n";
    $NUMCTR = $VALS->{'ACH1NUM'};
    $DOC->append( 'NUMCTR',  $NUMCTR );
    $GED->append( 'xIDDEST', $NUMCTR );
    $nomUser = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
    $DOC->append( 'NOMUSER', $nomUser );
    $CDE_COUR = $VALS->{'CDCOURx'};
  SWITCH: {
########################################### CCA ###################################################
        $CDE_COUR eq "CCA-01"
          && do {

            #$P->add_motif_to_denormalized_split ('\x{20}');
            $DOC->include( "CRB-EACEX-CCATP.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CCA-02"
          && do {
            $DOC->include( "CRB-EACEX-CCADC.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CCA-03"
          && do {

            #$P->add_motif_to_denormalized_split ('\x{20}');
            $DOC->include( "CRB-EACEX-CCARE.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CCA-04"
          && do {

            #$P->add_motif_to_denormalized_split ('\x{20}');
            $DOC->include( "CRB-EACEX-CCAMU.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
############################################# CMA ###################################################
        ( $CDE_COUR eq "CMA-01" )
          && do {

            #$P->add_motif_to_denormalized_split ('\x{20}');
            $DOC->include( "CRB-EACEX-CMATP.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        ( $CDE_COUR eq "CMA-02" )
          && do {
            $DOC->include( "CRB-EACEX-CMADC.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        ( $CDE_COUR eq "CMA-03" )
          && do {

            #$P->add_motif_to_denormalized_split ('\x{20}');
            $DOC->include( "CRB-EACEX-CMARS.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
############################################### CEA ###################################################
        ( $CDE_COUR eq "CEA-01" )
          && do {
            $DOC->include( "CRB-EACEX-CEACH.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        ( $CDE_COUR eq "CEA-02" )
          && do {
            $DOC->include( "CRB-EACEX-CEAAR.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
############################################### CTA ###################################################
        $CDE_COUR eq "CTA-01"
          && do {
            $DOC->include( "CRB-EACEX-CTADE.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-02"
          && do {
            $DOC->include( "CRB-EACEX-CTAHN.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-03"
          && do {
            $DOC->include( "CRB-EACEX-CTAFA.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-04"
          && do {
            $DOC->include( "CRB-EACEX-CTASA.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-05"
          && do {
            $DOC->include( "CRB-EACEX-CTABH.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-06"
          && do {
            $DOC->include( "CRB-EACEX-CTAHH.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-07"
          && do {
            $DOC->include( "CRB-EACEX-CTASS.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-08"
          && do {
            $DOC->include( "CRB-EACEX-CTADH.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-09"
          && do {
            $DOC->include( "CRB-EACEX-CTATP.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-10"
          && do {
            $DOC->include( "CRB-EACEX-CTACI.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-11"
          && do {
            $DOC->include( "CRB-EACEX-CTAHP.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-12"
          && do {
            $DOC->include( "CRB-EACEX-CTAPDI.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-13"
          && do {
            $DOC->include( "CRB-EACEX-CTADA.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-14"
          && do {
            $DOC->include( "CRB-EACEX-CTAPR.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-15"
          && do {
            $DOC->include( "CRB-EACEX-CTAPNR.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-16"
          && do {
            $DOC->include( "CRB-EACEX-CTAPOR.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-17"
          && do {
            $DOC->include( "CRB-EACEX-CTAPNG.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-18"
          && do {
            $DOC->include( "CRB-EACEX-CTAFRA.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-19"
          && do {
            $DOC->include( "CRB-EACEX-CTA2EO.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-20"
          && do {
            $DOC->include( "CRB-EACEX-CTADM.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-21"
          && do {
            $DOC->include( "CRB-EACEX-CTABDS.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-22"
          && do {
            $DOC->include( "CRB-EACEX-CTAFE.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-23"
          && do {
            $DOC->include( "CRB-EACEX-CTACOLR.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-24"
          && do {
            $DOC->include( "CRB-EACEX-CTASTG.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-25"
          && do {
            $DOC->include( "CRB-EACEX-CTAVAG.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-26"
          && do {
            $DOC->include( "CRB-EACEX-CTADAV.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-27"
          && do {
            $DOC->include( "CRB-EACEX-CTAFIM.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-28"
          && do {
            $DOC->include( "CRB-EACEX-CTALAD.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-29"
          && do {
            $DOC->include( "CRB-EACEX-CTAOTA.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-30"
          && do {
            $DOC->include( "CRB-EACEX-CTAACU.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-31"
          && do {
            $DOC->include( "CRB-EACEX-CTAPDP.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-32"
          && do {
            $DOC->include( "CRB-EACEX-CTAAMV.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-33"
          && do {
            $DOC->include( "CRB-EACEX-CTACSS.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-34"
          && do {
            $DOC->include( "CRB-EACEX-CTAROO.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-35"
          && do {
            $DOC->include( "CRB-EACEX-CTAFACT.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
        $CDE_COUR eq "CTA-36"
          && do {
            $DOC->include( "CRB-EACEX-CTAPAAOC.tex", "EDTK_DIR_SCRIPT" );
            $OPT_GED = '--edms';
            last SWITCH;
          };
######################################### CMU ##############################################
        (        $CDE_COUR eq "CMU-01"
              || $CDE_COUR eq "CMU-02"
              || $CDE_COUR eq "CMU-04"
              || $CDE_COUR eq "CMU-05"
              || $CDE_COUR eq "CMU-06" )
          && do {
            my ($ctt) = $CDE_COUR =~ /CMU\-(\d+)/;
            $DOC->append( 'CMU', $ctt );
            $DOC->include( "CRB-EACEX-CTTLOD.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
######################################### ACS ##############################################
        ( $CDE_COUR eq "ACS-01" || $CDE_COUR eq "ACS-02" )
          && do {
            my ($ctt) = $CDE_COUR =~ /ACS\-(\d+)/;
            $DOC->append( 'ACS', $ctt );
            $DOC->include( "CRB-EACEX-ACS.tex", "EDTK_DIR_SCRIPT" );
            last SWITCH;
          };
#######################################################################################################
    }
####################AJOUT DU NOM-PRENOM DU GESTIONNAIRE DANS L'IDEMET POUR SIMPLICATION DE LA GESTION DANS DOCUBASE.############
    my $GEST = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
################################GEST##########################
    $GED->append( 'xIDEMET', $GEST );
    return 0;
}

sub traite_E7 {

    # NORME 38 : ON UTILISE LA RECORD E7 POUR LES ADRESSES
    # warn "INFO : dump E7 ". Dumper($VALS) ."\n";

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $VALS->{'PELICCO'} );
    $VALS->{'PELICCO'} =~ s/FRANCE//i;

###############ADRESSE DESTINATAIRE###############
    my @DADDR = ();

    $ZONEDH =~ s/^\s+//;

    if ( $ZONEDH eq 'RVC-DC' ) {
        my $line = '.' x 38;
        @DADDR = ($line) x 6;
    }
    else {
        push( @DADDR, $NOMDESTINATAIRE );
        push( @DADDR, $VALS->{'PNTREMx'} );
        push( @DADDR, $VALS->{'CPLADRx'} );
        push( @DADDR,
            "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );
        if ( $VALS->{'INDCDXx'} == 1 ) {
            push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
        }
        else {
            push( @DADDR, $VALS->{'BOIPOSL'} );
        }
        push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );
        push( @DADDR, $VALS->{'PELICCO'} );
        @DADDR = user_cleanup_addr(@DADDR);
    }
    $DOC->append_table( 'DADDR',  @DADDR );
    $DOC->append_table( 'xADRLN', @DADDR );

    $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} );
    $GED->append( 'xCPDEST',  oe_iso_country() . $VALS->{'CODCDXC'} );

################ADRESSE EMETTEUR#########################
    my @EADDR = ();

    push( @EADDR, $NOMEMETTEUR1 );
    push( @EADDR, $NOMEMETTEUR2 );
    push( @EADDR, "$VALS->{'PEAD016'} $VALS->{'PEAD017'} $VALS->{'LIBV019'}" );
    push( @EADDR, "$VALS->{'CODC023'} $VALS->{'NOMC040'}" );

    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @EADDR, "$VALS->{'BOIP021'} $VALS->{'LIBL022'}" );
    }
    else {
        push( @EADDR, $VALS->{'BOIP021'} );
    }

    my $adresse = $VALS->{'PEAD016'} . ' ' . $VALS->{'LIBV019'};

    if ( $ERAFF eq 'D973' || $ERAFF eq 'M973' ) {
        $adresse = $truc . '' . $adresse;
    }

    $DOC->append( 'ADRESSE', $adresse );
    $DOC->append( 'BOITPOS', $VALS->{'BOIP021'} );
    $DOC->append( 'CODPOS',  $VALS->{'CODC023'} );

    #my $localite = lc($VALS->{'LIBL038'});
    my $localite = " ";
    $localite = ( $VALS->{'LIBL022'} );

    #my $localite = ucfirst($localite);
    $DOC->append( 'LOCALITE', $localite );
    @EADDR = user_cleanup_addr(@EADDR);
    $DOC->append_table( 'EADDR', @EADDR );

    $DOC->append("ENDADH");
    return 0;
}

sub traite_H1 {
    $DOC->append( 'PENOCIV', $VALS->{'PENO002'} );
    $DOC->append( 'PENOAYA', $VALS->{'PENOPRN'} );
    $DOC->append( 'PENONAD', $VALS->{'PENOLIB'} );
    return 0;
}

sub traite_option {

    #     	warn "INFO : dump OPTION ". Dumper($VALS) ."\n";
  SWITCH: {
        (        $CDE_COUR eq "CMA-01"
              || $CDE_COUR eq "CMA-03"
              || $CDE_COUR eq "CTA-12" )
          && do {
            $DOC->append( 'MONTANT',  check_first_amount( $VALS->[1] ) );
            $GED->append( 'xCLEGEDi', $VALS->[1] );
            last SWITCH;
          };
        ( $CDE_COUR eq "CMA-02" )
          && do {
            avec_adresse();
            last SWITCH;
          };
        ( $CDE_COUR eq "CEA-01" )
          && do {
            my @BENEF = ();
            push( @BENEF, $VALS->[1] );
            if ( $VALS->[2] !~ /^\./ ) {
                push( @BENEF, $VALS->[2] );
            }
            if ( $VALS->[3] !~ /^\./ ) {
                push( @BENEF, $VALS->[3] );
            }
            if ( $VALS->[4] !~ /^\./ ) {
                push( @BENEF, $VALS->[4] );
            }
            $DOC->append_table( 'BENEF', @BENEF );
            last SWITCH;
          };
        ( $CDE_COUR eq "CCA-02" )
          && do {
            avec_adresse();
            last SWITCH;
          };
        (        $CDE_COUR eq "CCA-01"
              || $CDE_COUR eq "CCA-03"
              || $CDE_COUR eq "CCA-04" )
          && do {
            $MONTANT = $VALS->[1];
            $DOC->append( 'MONTANT',  check_first_amount( $VALS->[1] ) );
            $GED->append( 'xCLEGEDi', $VALS->[1] );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-01" )
          && do {
            if ( $VALS->[1] == 1 ) {
                $DOC->append( 'TYPEREG', 'partiel' );
            }
            elsif ( $VALS->[2] == 1 ) {
                $DOC->append( 'TYPEREG', 'intégral' );
            }
            $VALS->[3] =~ s/\s+//;
            $DOC->append( 'DATEREMB', $VALS->[3] );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-02" )
          && do {
            if ( $VALS->[1] == 1 ) {
                $LISTE->append( 'LIGNE',
                    'Numéro de Sécurité sociale du bénéficiaire des soins'
                );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[2] == 1 ) {
                $LISTE->append( 'LIGNE',
                    'Nom et prénom du bénéficiaire des soins' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[3] == 1 ) {
                $LISTE->append( 'LIGNE', 'Date ou période de soins' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[4] == 1 ) {
                $LISTE->append( 'LIGNE', 'Code acte et/ou coefficient' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[5] == 1 ) {
                $LISTE->append( 'LIGNE', 'Dépense engagée' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[6] == 1 ) {
                $LISTE->append( 'LIGNE',
                    'Base de remboursement et/ou taux de remboursement RO' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[7] == 1 ) {
                $LISTE->append( 'LIGNE', 'Montant RO remboursé' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[8] == 1 ) {
                $LISTE->append( 'LIGNE', 'Montant RC attendu' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[9] == 1 ) {
                $LISTE->append( 'LIGNE',
                    'Imputation des 1% selon les accords C.P.F.M' );
                $LISTE->append('EditLigneLISTE');
            }
            if ( $VALS->[10] == 1 ) {
                $LISTE->append( 'LIGNE', 'Carte de tiers payant lisible' );
                $LISTE->append('EditLigneLISTE');
            }
            $DOC->append( 'LISTE', $LISTE );

            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-03" )
          && do {
            $DOC->append( 'NUMFINESS', $VALS->[1] || '' );
            $GED->append( 'xIDDEST',   $VALS->[1] );
            $DOC->append( 'NUMFAC',    $VALS->[2] || '' );
            $DOC->append( 'DATEREG',   $VALS->[3] || '' );
            $DOC->append( 'TITREREG',  $VALS->[4] || '' );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-04" || $CDE_COUR eq "CTT-05" )
          && do {
            if ( $VALS->[1] == 1 ) {
                $DOC->append( 'ACTCON', 'Chambre particulière' );
            }
            if ( $VALS->[2] == 1 ) {
                $DOC->append( 'ACTCON', 'Box ambulatoire' );
            }
            if ( $VALS->[3] == 1 ) {
                $DOC->append( 'ACTCON', 'Taux RC erroné' );
            }
            if ( $VALS->[4] == 1 ) {
                $DOC->append( 'ACTCON', 'PH2' );
            }
            if ( $VALS->[5] == 1 ) {
                $DOC->append( 'ACTCON', 'Autre' );
            }
            if ( $VALS->[6] ne '' ) {
                $DOC->append( 'MTPRCH', $VALS->[6] );
            }
            else {
                print STDERR "ERREUR PAS DE MONTANT DE PRISE EN CHARGE\n";
                $TRK->track( 'W', 1, $NUMCTR, $NUMABA, $nomUser, $CDE_COUR,
                    "ERREUR PAS DE MONTANT DE PRISE EN CHARGE" );
                exit 1;
            }
            if ( $VALS->[7] == 1 ) {
                $DOC->append( 'MTPRCHSIG', '% de la BRSS' );
            }
            if ( $VALS->[8] == 1 ) {
                $DOC->append( 'MTPRCHSIG', '€' );
            }
            if ( $VALS->[9] == 1 ) {
                $DOC->append( 'MTPRCHSIG', 'Jour(s)' );
            }
            if (   $VALS->[1] == 0
                && $VALS->[2] == 0
                && $VALS->[3] == 0
                && $VALS->[4] == 0
                && $VALS->[5] == 0 )
            {
                print STDERR "ERREUR OPTION MANQUANTE EDITION NON POSSIBLE\n";
                $TRK->track( 'W', 1, $NUMCTR, $NUMABA, $nomUser, $CDE_COUR,
                    "ERREUR OPTION MANQUANTE EDITION NON POSSIBLE" );
                exit 1;
            }
            if ( $VALS->[7] == 0 && $VALS->[8] == 0 && $VALS->[9] == 0 ) {
                print STDERR "ERREUR OPTION MANQUANTE EDITION NON POSSIBLE\n";
                $TRK->track( 'W', 1, $NUMCTR, $NUMABA, $nomUser, $CDE_COUR,
                    "ERREUR OPTION MANQUANTE EDITION NON POSSIBLE" );
                exit 1;
            }
            last SWITCH;
          };
        (
                 $CDE_COUR eq "CTT-06"
              || $CDE_COUR eq "CTT-07"
              || $CDE_COUR eq "CTT-10"
              || $CDE_COUR eq "CTT-11"
              ||

              #$CDE_COUR eq "CTT-14" || $CDE_COUR eq "CTT-15" ||
              $CDE_COUR eq "CTT-08" || $CDE_COUR eq "CTT-09"
          )
          && do {
            $DOC->append( 'DATESOINS', $VALS->[1] );
            $DOC->append( 'MTREMB',    $VALS->[2] );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTT-12" || $CDE_COUR eq "CTT-13" )
          && do {
            $DOC->append( 'ACTECON', $VALS->[1] );
          };
        (        $CDE_COUR eq "CTT-14"
              || $CDE_COUR eq "CTT-15"
              || $CDE_COUR eq "CTT-16"
              || $CDE_COUR eq "CTT-17"
              || $CDE_COUR eq "CTT-18"
              || $CDE_COUR eq "CTT-19"
              || $CDE_COUR =~ /CMU\-0\d/ )
          && do {
            my $finCMU = ( $VALS->[1] || '' );
            $finCMU =~ s/\s+$//;
            $DOC->append( 'FINCMU', $finCMU );

            #$GED->append('xIDDEST', $VALS->[1]);
            my $QUI = $VALS->[2];
            $QUI =~ s/^\s+//;
            $QUI =~ s/\s+$//;
            $DOC->append( 'PERSONNE_CONCERNEE', $QUI || '' );
            last SWITCH;
          };

        ( $CDE_COUR eq "CTA-15" )
          && do {
            $DOC->append( 'DATEADH', $VALS->[1] || '' );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTA-16" )
          && do {
            $DOC->append( 'DATERAD', $VALS->[1] || '' );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTA-19" )
          && do {
            $VALS->[1] =~ s/^\s+//;
            $VALS->[1] =~ s/\s+$//;
            $DOC->append( 'DATE', $VALS->[1] || '' );
            $VALS->[2] =~ s/^\s+//;
            $VALS->[2] =~ s/\s+$//;
            $DOC->append( 'DATECONSO', $VALS->[2] || '' );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTA-24" )
          && do {
            $DOC->append( 'DATEDST', $VALS->[1] || '' );
            $DOC->append( 'DATEFST', $VALS->[2] || '' );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTA-28" )
          && do {
            $DOC->append( 'POURCENTAGE_SS', $VALS->[1] || '' );
            last SWITCH;
          };
        ( $CDE_COUR eq "CTA-31" )
          && do {
            $VALS->[1] =~ s/\s+$//;    #en fin de chaine
            $DOC->append( 'DATE_REMBOURSEMENT', $VALS->[1] || '' );
            last SWITCH;
          };
    }
    return 0;
}

exit main(@ARGV);
