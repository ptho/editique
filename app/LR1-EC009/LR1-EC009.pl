#!/usr/bin/perl
use utf8;

#-d:ptkdb
use oEdtk::Main 0.50;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use Data::Dumper;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::EX;
use oEUser::Descriptor::OF;
use oEUser::Descriptor::OG;
use oEUser::Descriptor::I4;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use Date::Calc qw(Today);
use Time::Local;
use Email::Sender::Simple qw(sendmail);
use Email::Sender::Transport::SMTP;
use oEdtk::Outmngr qw(omgr_stats);
use oEdtk::Config qw(config_read);
use oEdtk::DBAdmin qw(db_connect);
use strict;

my $DOC;
my @DADDR  = ();
my @EADDR  = ();
my @CTRADD = ();
my $BOOL   = 0;
my $LOCSECT;
my $NUMIND;
my $CIV;
my $CIVCOURT;
my $CODE   = 0;
my $MTTIMP = 0;
my $LIB;
my $CG;
my $CTR;
my $CPT_EX = 0;
my $DOMAINE;
my $DATECHEANCE;
my $DATECH;
my $EJ;
my $NOTEMNT;
my $DATECPL;
my $DATELT = "";
my $JOURLT;
my $MOIS;
my $JOUR_ECH;
my $MOIS_ECH;
my $ANNEE_ECH;
my $MOISLT;
my $ANNEELT;
my $DATEREJET;
my $JREJET;
my $NREJET  = "";
my $NBREJET = "";
my $ENTITE;
my $ENTSIGN;
my $PJ;
my $ER;
my $ID_MNT;
my $PRENOM;
my $CPT_PGE = 0;
my $ANO;
my $DOC;

### Dans le cadre de la norme 38 on est obligé de récupérer une partie de l'adresse en EX car l'I4 est incomplet
my $numVoieDest;
my $codePostDest;
my $numVoieEmet;
my $codePostEmet;

my $OBJETI;
my $OBJETII;
my ( $TRK, $AGENCE, $NOM, $CPDEST, $VILDEST );

use Readonly;
Readonly my $NULLNUMABA => "XXX";    # should not be a valid NUMABA value!

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'xIDEMET', 'NUMCTT', 'xNOMDEST', 'ID_MNT', 'xVILDEST' ]
    );

    my $opt_ged = '--edms';
    oe_new_job('--index');

    my $ENTETE = oEUser::Descriptor::ENTETE::get();
    $ENTETE->bind_all_c7();
    my $EX = oEUser::Descriptor::EX->get();
    $EX->bind_all_c7();
    my $OF = oEUser::Descriptor::OF->get();
    $OF->bind_all_c7();
    my $OG = oEUser::Descriptor::OG->get();
    $OG->bind_all_c7();
    my $I4 = oEUser::Descriptor::I4->get();

    #$I4->debug();
    $I4->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    #$Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $DOC->append( 'xTYPDOC', 'RJPB' );    ### Type de document
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $ENTETE,
        'EX'     => $EX,
        'OF'     => $OF,
        'OG'     => $OG,
        'I4'     => $I4,
        'Z1'     => $Z1
    );
    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'ENTETE' ) {
        }
        elsif ( $id eq 'EX' ) {
            emitDoc() if ( $. > 3 );
            @DADDR = ();
            @EADDR = ();
            $vals->{'LICCODP'} =~ s/FRANCE//i;
            $vals->{'LICC094'} =~ s/FRANCE//i;

            my $civDest = $vals->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );
            #ADRESSE DESTINATAIRE
            push( @DADDR,
"$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}"
            );
            $numVoieDest =
              "$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'}";
            $codePostDest = $vals->{'CODCDXC'};
            $numVoieEmet =
              "$vals->{'PEAD053'} $vals->{'LICBTQV'} $vals->{'PEVO056'}";
            $codePostEmet = $vals->{'CODPOSx'};
            $PRENOM       = $vals->{'PENOPRN'};
            $CG           = substr( $vals->{'ODERCOD'}, 0, 1 );

            if ( $CG =~ /J|L|M/ ) {
                $EJ = "MUTACITE";
                $CG = "MUTACITE";
                $ER = "Votre agence MUTACITE";
            }
            else {
                $EJ = "MNT";
                $CG = "la MNT";
                $ER = "Votre agence MNT";
            }

            #ADRESSE CTR
            push( @CTRADD, "$EJ" );
            push( @CTRADD, "Centre de Traitement Recouvrement" );
            push( @CTRADD, "TSA 70011" );
            push( @CTRADD, "33044 BORDEAUX Cedex" );
            @CTRADD = user_cleanup_addr(@CTRADD);

            #ADRESSE EMETTEUR
            push( @EADDR, "$vals->{'PENO078'}" );
            $DOC->append( 'PPSEC', $vals->{'PENO078'} );

            $CTR     = "Centre de Traitement Recouvrement";
            $ENTSIGN = $vals->{'ODERCOD'};
            $LOCSECT = $vals->{'LIBLOCx'};
            $NUMIND  = $vals->{'NUMCINx'};
            $ENTITE  = $vals->{'ODCGCOD'};

            #DONNEES DE GED
            $AGENCE  = $vals->{'ODERCOD'};
            $NOM     = $vals->{'PENOLIB'};
            $CPDEST  = $vals->{'CODCDXC'};
            $VILDEST = $vals->{'NOMCDXL'};
            $DOC->append( 'xIDEMET', $vals->{'ODERCOD'} );
            $DOC->append( 'xNOMDEST',
                $vals->{'PENOLIB'} . " " . $vals->{'PENOPRN'} );
            $DOC->append( 'xCPDEST',  $vals->{'CODCDXC'} );
            $DOC->append( 'xVILDEST', $vals->{'NOMCDXL'} );
            $DOC->append( 'xIDDEST',  $vals->{'NUMCINx'} );
            $DOC->append( 'xSOURCE',  'PREC' );
            $DATECHEANCE = convertDate( $vals->{'DATVALx'} );
            $JOUR_ECH    = substr( $vals->{'DATVALx'}, 6, 2 );
            $MOIS_ECH    = substr( $vals->{'DATVALx'}, 4, 2 );
            $ANNEE_ECH   = substr( $vals->{'DATVALx'}, 0, 4 );
            $DATECH =
              $JOUR_ECH . "" . month_to_name($MOIS_ECH) . "" . $ANNEE_ECH;

            #REGLE 6 DU CDC
            $CIV      = $vals->{'PENOCOD'};
            $CIVCOURT = $vals->{'PENOCOD'};
            if ( $CIV =~ /MME|MLLE/ ) {
                $CIV = "Chère Adhérente,";
            }
            else {
                $CIV = "Cher Adhérent,";
            }
            $MTTIMP = $vals->{'MTTIMPx'};
            $DOC->append( 'xCLEGEDii', $vals->{'MTTIMPx'} );
            $LIB = $vals->{'LIBMOTx'};

            $CODE = $vals->{'CMOTIFx'};
            $DOC->append( 'CODE',       $CODE );
            $DOC->append( 'xCLEGEDiii', $vals->{'CMOTIFx'} );

            #REGLE DE GESTION NUMERO 4 SUR DATE LIMITE DE REGULARISATION
            $DATECPL = localtime;
            $JOURLT  = substr( $DATECPL, 0, 3 );
            $MOISLT  = substr( $DATECPL, 4, 3 );
            $DATELT  = substr( $DATECPL, 8, 2 );
            $ANNEELT = substr( $DATECPL, 20, 4 );
            if ( $DATELT <= 14 ) {
                $JREJET = 20;
                if ( ( $JOURLT =~ /Sat/ ) || ( $JOURLT =~ /Sun/ ) ) {
                    $JREJET = 22;
                }
            }
            elsif ( $DATELT => 15 ) {
                $JREJET = 26;
                if ( ( $JOURLT =~ /Sat/ ) || ( $JOURLT =~ /Sun/ ) ) {
                    $JREJET = 28;
                }
            }
            if ( $MOISLT =~ /Jan/ ) { $MOIS = "01"; }
            if ( $MOISLT =~ /Feb/ ) { $MOIS = "02"; }
            if ( $MOISLT =~ /Mar/ ) { $MOIS = "03"; }
            if ( $MOISLT =~ /Apr/ ) { $MOIS = "04"; }
            if ( $MOISLT =~ /May/ ) { $MOIS = "05"; }
            if ( $MOISLT =~ /Jun/ ) { $MOIS = "06"; }
            if ( $MOISLT =~ /Jul/ ) { $MOIS = "07"; }
            if ( $MOISLT =~ /Aug/ ) { $MOIS = "08"; }
            if ( $MOISLT =~ /Sep/ ) { $MOIS = "09"; }
            if ( $MOISLT =~ /Oct/ ) { $MOIS = "10"; }
            if ( $MOISLT =~ /Nov/ ) { $MOIS = "11"; }
            if ( $MOISLT =~ /Dec/ ) { $MOIS = "12"; }
            $DATEREJET = $JREJET . "/" . $MOIS . "/" . $ANNEELT;
        }
        elsif ( $id eq 'I4' ) {
            adresseI4($vals);
            $NBREJET = $vals->{'REJETxx'};
            $DOC->append( 'NBREJET', $NBREJET );
            if ( $ANO ne 'ANO' && $CODE =~ /20/ ) {
                if ( $NBREJET == "001" ) {
                    $DOC->append( 'LETTRE', {'LETTREA'} );
                    $CPT_PGE++;
                }
                else {
                    $DOC->append( 'LETTRE', {'LETTREB'} );
                    $CPT_PGE++;
                }

            }
            else {
                $DOC->append( 'LETTRE', {'LETTREC'} );
                $CPT_PGE++;
            }
        }
        elsif ( $id eq 'OF' ) {
            $DOC->append( 'NUMIND',    $NUMIND );
            $DOC->append( 'JOUR_ECH',  $JOUR_ECH );
            $DOC->append( 'MOIS_ECH',  month_to_name($MOIS_ECH) );
            $DOC->append( 'ANNEE_ECH', $ANNEE_ECH );
            $DOC->append( 'CIV',       $CIV );
            $DOC->append( 'MTTIMP',    $MTTIMP );
            $DOC->append( 'LIB',       $LIB );
            $DOC->append( 'CG',        $CG );

            #$DOC->append('NOTEMNT', $NOTEMNT);
            $DOC->append( 'REGLIMITE', $DATEREJET );
            $ANO = $vals->{'ANOxxxx'};
            $DOC->append_table( 'CTRADD', @CTRADD );
            $BOOL = 1;
            $DOC->append( 'BOOL',    $BOOL );
            $DOC->append( 'CTR',     $CTR );
            $DOC->append( 'LOCSECT', 'Bordeaux' );
        }
        elsif ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $ID_MNT = $vals->{'PERF003'};
            $DOC->append( 'NUMABA',   $ID_MNT );
            $DOC->append( 'xCLEGEDi', $ID_MNT )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }
    emitDoc();
    $TRK->track( 'Doc', 1, $AGENCE, $NUMIND, $NOM, $ID_MNT, $VILDEST );
    oe_print_to_output_file($DOC);
    oe_compo_link($opt_ged);
    mail_repro();
    return 0;
}

#################################################################################
# Ajouté pour la gestion de la norme 38
#################################################################################
sub adresseI4 {
    my $vals = shift;

    my $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBLOCL'} = '';
    }

    push( @DADDR, "$vals->{'PNTREMx'} $vals->{'CPLADRx'}" );
    push( @DADDR,
        "$numVoieDest $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );
    push( @DADDR, "$vals->{'BOIPOSL'}" );
    push( @DADDR, "$codePostDest $vals->{'NOMCDXL'}" );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

    $Y = $vals->{'NOMC016'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( index( $vals->{'LIBL015'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBL015'} = '';
    }

    push( @EADDR, "$vals->{'PNTR011'} $vals->{'CPLA012'}" );
    push( @EADDR, "$numVoieEmet $vals->{'LIBV013'}" );
    push( @EADDR, "$vals->{'BOIP014'}" );
    push( @EADDR, "$codePostEmet $vals->{'LIBL015'} $vals->{'NOMC016'}" );
    push( @EADDR, "$vals->{'LICC094'}" );
    @EADDR = user_cleanup_addr(@EADDR);

    $DOC->append( 'PPCPLSEC', "$vals->{'PNTR011'} $vals->{'CPLA012'}" );
    $DOC->append( 'PPRUESEC', "$numVoieEmet $vals->{'LIBV013'}" );
    $DOC->append( 'PPBP',     $vals->{'BOIP014'} );
    $DOC->append( 'PPCODSEC',
        "$codePostEmet $vals->{'LIBL015'} $vals->{'NOMC016'}" );
}

sub emitDoc {
    $DOC->append('DEBADH');
    $DOC->append('COURRIER');
    $DOC->append('ENDADH');
    $DOC->append( 'NUMABA', $NULLNUMABA )
      ;    # or else previous NUMABA keeps active when NUMABA is undef
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

# fonction permettant la conversion du mois XX en toutes lettres.
sub month_to_name {
    my @mons =
      qw/Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre Décembre/;
    my $MOIS_ECH = shift;

    return $mons[ $MOIS_ECH - 1 ];
}

sub mail_repro {
    my @body;
    my $nbr_page;
    my $cfg = config_read('MAIL');
    my ( $sec, $min, $hour, $day, $month, $year ) = localtime();
    my $date = sprintf( "%02d/%02d/%d", $day, $month + 1, $year + 1900 );
    my $dtejrs = sprintf( "%d-%02d-%02d",   $year + 1900, $month + 1, $day );
    my $time   = sprintf( "%02d:%02d:%02d", $hour,        $min,       $sec );
    my $mailfile     = $cfg->{'EDTK_MAIL_REPRO'};
    my $edtk_prgname = $TRK->{'app'};
    my $edtk_entity  = $TRK->{'entity'};

    if ( $edtk_prgname =~ "LR2-EC009" ) {
        $nbr_page = "de trois pages";
    }
    else {
        $nbr_page = "d'une page";
    }
    open( my $fh, '<', $mailfile )
      or die "ERROR: Can't open \"$mailfile\": $!\n";
    while ( my $Lignebody = <$fh> ) {
        $Lignebody =~ s/%date/$date/;
        $Lignebody =~ s/%dtejrs/$dtejrs/;
        $Lignebody =~ s/%nbr_page/$nbr_page/;
        $Lignebody =~ s/%edtk_prgname/$edtk_prgname/;
        $Lignebody =~ s/%ed_entity/$edtk_entity/;
        push( @body, $Lignebody );
    }
    close($fh);
    my $subject .= $cfg->{'EDTK_MAIL_SUBJ_REPRO'};
    $subject =~ s/%date/$date/;
    $subject =~ s/%time/$time/;
    if ( $CPT_PGE == 1 ) {
        $CPT_PGE = $CPT_PGE . " " . "courrier";
    }
    else {
        $CPT_PGE = $CPT_PGE . " " . "courriers";
    }
    $subject =~ s/%CPT_PGE/$CPT_PGE/;
    $subject =~ s/%entity/$edtk_entity/;
    send_mail_repro(
        $cfg->{'EDTK_MAIL_LIST_REPRO'},
        $cfg->{'EDTK_MAIL_LIST_REPRO_C'},
        $subject, @body
    );
}

sub send_mail_repro {
    my ( $to, $cc, $subject, @body ) = @_;
    my $cfg = config_read('MAIL');
    $subject ||= $0;
    $subject = $cfg->{'EDTK_TYPE_ENV'} . " - $subject ";

    my $transport = Email::Sender::Transport::SMTP->new(
        {
            host => $cfg->{'EDTK_MAIL_SMTP'}
        }
    );

    my $email = Email::Simple->create(
        header => [
            To => $to || $cfg->{'EDTK_MAIL_SENDER'},
            Cc => $cc || $cfg->{'EDTK_MAIL_SENDER'},
            From    => $cfg->{'EDTK_MAIL_SENDER'},
            Subject => $subject || $0
        ],
        body => join( '', @body )
    );

    # Useful for testing.
    if ( $cfg->{'EDTK_MAIL_SMTP'} eq 'warn' ) {
        print $email->as_string() . "\n";
    }
    else {
        eval { sendmail( $email, { transport => $transport } ); };
        if ($@) {
            die "ERROR: sendmail failed. Reason is $@\n";
        }
    }
}

exit main(@ARGV);
