#!/usr/bin/perl
#use v5.14;
use feature "switch";
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::H8;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;
use Readonly;
use warnings;
my ( $TRK, $DOC, $GED, $VALS );
my $ACTEGEST;
my $dtFinSTAGE;    #DATE DE FIN DE STAGE ACCC
my $NUMCTR;
my $NUMABA;
my $xNOMDEST;
my $NOMDEST;
my $GESTIONNAIRE;
my $EMETTEUR;
my $OPT_GED;
my $dtActeGES;
my $GRPGESTbool  = 0;
my $codOFFREbool = 0;
my $CODEGTIE     = "";
my $CODEPRD      = "";
my %H_INFO_GTIE  = ();
my %H_GTIE_OPT   = ();
my %H_TAB_PRD    = ();
my %H_TAB_ACSO   = ();
my %H_TAB_ACCC   = ();
my $NUMCC;
Readonly my $LIBCODPRDGS1 => "PL-MNT-GS1";
Readonly my $LIBCODPRDGS2 => "PL-MNT-GS2";
Readonly my $LIBPRDHOSPS  => "PLU-MNT-PS";
Readonly my $LIBPRDHOSDC  => "PIDS3-MNT3";
Readonly my $LIBCODE_PDT  => "CODE_PDT_GTIE";
Readonly my $LIBLONG_PDT  => "LIB_LONG_PDT_GTIE";
Readonly my $DDEADH_GTIE  => "DATE_ADH_GTIE";

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMCTR', 'NOMDEST', 'EMETTEUR', 'ACTEGESTION', 'NUMABA' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # 	$E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # 	$E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    # 	$E7->debug();
    $E7->bind_all_c7();

    my $H4 = oEUser::Descriptor::H4->get();

    #$H4->debug();
    $H4->bind_all();

    my $H8 = oEUser::Descriptor::H8->get();

    # 	$H8->debug();
    $H8->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'H4' => $H4,
        'H8' => $H8,
        'Z1' => $Z1
    );

    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {
        given ($id) {
            when (/E1/) {

               #                   	warn "INFO : dump E1 ". Dumper($VALS) ."\n";

                $DOC->append( 'DEBUT', '' );
                $NOMDEST =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

                #####GESTION DE LA CIVILITE ENTRE TIERS ET ADHERENT#########
                # 				$CIVILITE = $VALS->{'LICCODC'};
                my $CIVILITE = $VALS->{'PENOCOD'};
                $DOC->append( 'PERSCIV', $CIVILITE );
                my $civDest = $VALS->{'PENOCOD'};
                $DOC->append( 'civDest', $civDest );

                my $CIV;
                $EMETTEUR = $VALS->{'ODERCOD'};
                if ( $EMETTEUR eq '' ) {
                    $CIV      = 0;
                    $EMETTEUR = "ER";
                }
                else {
                    $CIV = 1;
                }

                $DOC->append( 'EMETTEUR', $EMETTEUR );
                $DOC->append( 'CIV',      $CIV );

                $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

                #         $DOC->append('xNOMDEST', $xNOMDEST);
                #         $DOC->append('CIVLONG', $VALS->{'LICCODC'});

                ################CENTRE DE GESTION###############################
                my $CODCENTRE = $VALS->{'ODCGCOD'};
                my $LIBCENTRE;

                if ( $CODCENTRE eq 'MNT-RC' ) {
                    $LIBCENTRE = 'Complémentaire Santé ';
                    die "\nERROR:CENTRE DE GESTION INCONNU EN PREVOYANCE: "
                      . $CODCENTRE . " ( "
                      . $LIBCENTRE . ")\n\n";
                }
                else {
                    $LIBCENTRE = 'Prévoyance ';
                }

                #         $DOC->append('LIBCENTRE', $LIBCENTRE);

                ################ADRESSE EMETTEUR#########################
                my $AGENCE = $VALS->{'PENO027'};
                $DOC->append( 'AGENCE', $VALS->{'PENO027'} );

                my $adresse =
                    $VALS->{'PEAD030'} . ' '
                  . $VALS->{'LICN034'} . ' '
                  . $VALS->{'LIBV035'};
                $DOC->append( 'ADRESSE', $adresse );

                $DOC->append( 'BOITPOS', $VALS->{'BOIP037'} );
                $DOC->append( 'CODPOS',  $VALS->{'CODC039'} );

                my $localite = ( $VALS->{'LIBL038'} );
                $DOC->append( 'LOCALITE', $localite );
                $DOC->append('BASDEPAGE');

                ######INDEXATION GED###################
                # 				$GED->append('xOWNER', 'BUR');  #-	Domaine de métier
                $GED->append( 'xIDEMET',  $EMETTEUR );    #-	Emetteur
                $GED->append( 'xNOMDEST', $xNOMDEST );    #-	Nom du destinataire
                $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
                  ;    #-	Ville du destinataire

        #       	$GED->append('xCPDEST', oe_iso_country() . $VALS->{'CODCDXC'});

            }

            when (/E2/) {

                #            warn "INFO : dump E2 ". Dumper($VALS) ."\n";
                $NUMCC = $VALS->{'ACCCNUM'};

                #            if (!$NUMCC =~ m/-PLU/){
                #               print STDERR "INFO CONTRAT STANDART\n";
                ###########CODE COURRIER ###################
                $ACTEGEST = $VALS->{'ACOPCOD'};    #Code acte de gestion
                given ($ACTEGEST) {
                    when (/ACCC/) {
                        $DOC->append( 'xTYPDOC', 'CACO' );  ### Type de document
                        if ( !$NUMCC =~ m/-HOS/ ) {
                            $DOC->append( 'OBJET',
                                'Modification de garanties' );
                        }
                    }
                    when (/ACMS/) {
                        $DOC->append( 'xTYPDOC', 'CACO' );  ### Type de document
                        if ( !$NUMCC =~ m/-HOS/ ) {
                            $DOC->append( 'OBJET',
                                'Modification de la base de cotisations' );
                        }
                    }
                    when (/ACSO/) {
                        $DOC->append( 'xTYPDOC', 'CADC' );  ### Type de document
                        if ( !$NUMCC =~ m/-HOS/ ) {
                            $DOC->append( 'OBJET', 'Adhésion' );
                        }
                    }
                    default {
                        die "\nERROR:CODE ACTE DE GESTION INCONNU $ACTEGEST\n\n"
                    }
                }

                #            }
                $dtActeGES = $VALS->{'ACOPDDE'}
                  ; #DATE D'EFFET DE L'ACTE DE GESTION NON CONVERTI EN jj/mm/aaaa
                my $DATEACT = convertDate( $VALS->{'ACOPDDE'} )
                  ;    #DATE D'EFFET DE L'ACTE DE GESTION
                my $DATEEDT =
                  convertDate( $VALS->{'DATEDTx'} )
                  ;    #Date du Traitement d'Edition
                my $DATESOUSCRIP =
                  convertDate( $VALS->{'ACH1DDE'} );    #DATE SOUSCRIPTION

                $DOC->append( 'DATEACT', $DATEACT );
                $DOC->append( 'DATEEDT', $DATEEDT );

                #########NUMERO CONTRAT INDIVIDUEL############
                $NUMCTR = $VALS->{'ACH1NUM'};
                $DOC->append( 'NUMCTR', $NUMCTR );

                #############INDEXATION GED###################
                $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
                  ;    #-	Numéro de contrat collectif
                $GED->append( 'xCLEGEDi', $NUMCTR )
                  ;    #-	Numéro de contrat individuel

                $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
                $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
                $GED->append( 'xCLEGEDiv', $DATEEDT )
                  ;    #Date du Traitement d'Edition

                ###########GROUPE DE GESTION#######################
                my $GRPGEST = substr $VALS->{'ACH1COD'}, 0, 3;
                if ( $GRPGEST eq 'I1-' ) {
                    $GRPGESTbool = 1;
                }

                my $CodOFFRE = $VALS->{'PROFCOD'};
                if ( $CodOFFRE eq 'PCL-MNT' && $ACTEGEST eq 'ACSO' ) {
                    $codOFFREbool = 1;
                }

                #         warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
            }

            when (/E7/) {
                ############BLOC ADRESSE DESTINATAIRE##################
                my @DADDR = ();
                push( @DADDR, $NOMDEST );
                push( @DADDR, $VALS->{'PNTREMx'} );
                push( @DADDR, $VALS->{'CPLADRx'} );
                push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
                );
                push( @DADDR, $VALS->{'BOIPOSL'} );
                push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
                oe_iso_country( $VALS->{'LICCODP'} );
                $VALS->{'LICCODP'} =~
                  s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
                push( @DADDR, $VALS->{'LICCODP'} );
                @DADDR = user_cleanup_addr(@DADDR);
                $DOC->append_table( 'DADDR', @DADDR );

                # 				warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";

            }

            when (/H4/) {

                #         warn "INFO : dump H4 ". Dumper($VALS) ."\n";
                my $H4_CODEGTIE = $VALS->{'ACH4CODGAR'};
                my $H4_CODEPRD  = $VALS->{'ACH4CODPRT'};
                my $H4_LIBLONG  = $VALS->{'PRPRLIBLON'};
                $CODEPRD = $VALS->{'ACH4CODPRT'};
                $H4_LIBLONG =~ s/\/PTIA//;
                $H4_LIBLONG =~ s/ PLURELYA//;
                if ( $VALS->{'ACH4DDERAD'} eq "" ) {
                    given ($NUMCC) {
                        when (m/-HOS/) {
                            given ($ACTEGEST) {
                                when (/ACCC/) {
                                    if (
                                        ( !exists $H_TAB_ACCC{$DDEADH_GTIE} )
                                        || ( $H_TAB_ACCC{$DDEADH_GTIE} <
                                            $VALS->{'ACH4DDEADH'} )
                                      )
                                    {
                                        $H_TAB_ACCC{$DDEADH_GTIE} =
                                          $VALS->{'ACH4DDEADH'};
                                    }
                                    if ( exists $H_TAB_ACCC{$LIBLONG_PDT}
                                        && $H_TAB_ACCC{$LIBLONG_PDT} ne
                                        $H4_LIBLONG )
                                    {
                                        if (
                                            $H_TAB_ACCC{$LIBLONG_PDT} !~ m/et/ )
                                        {
                                            $H_TAB_ACCC{$LIBLONG_PDT} .=
                                              " et " . $H4_LIBLONG;
                                        }
                                    }
                                    else {
                                        $H_TAB_ACCC{$LIBLONG_PDT} = $H4_LIBLONG;
                                    }
                                }
                                when (/ACSO/) {
                                    if ( exists $H_TAB_ACSO{$LIBLONG_PDT}
                                        && $H_TAB_ACSO{$LIBLONG_PDT} ne
                                        $H4_LIBLONG )
                                    {
                                        if (
                                            $H_TAB_ACSO{$LIBLONG_PDT} !~ m/et/ )
                                        {
                                            $H_TAB_ACSO{$LIBLONG_PDT} .=
                                              " et " . $H4_LIBLONG;
                                        }
                                    }
                                    else {
                                        $H_TAB_ACSO{$LIBLONG_PDT} = $H4_LIBLONG;
                                    }
                                    if (
                                        ( !exists $H_TAB_ACSO{$DDEADH_GTIE} )
                                        || ( $H_TAB_ACSO{$DDEADH_GTIE} <
                                            $VALS->{'ACH4DDEADH'} )
                                      )
                                    {
                                        $H_TAB_ACSO{$H4_CODEPRD}{$DDEADH_GTIE}
                                          = $VALS->{'ACH4DDEADH'};
                                        $H_TAB_ACSO{$DDEADH_GTIE} =
                                          $VALS->{'ACH4DDEADH'};
                                    }
                                }
                            }
                            $H_TAB_PRD{$H4_CODEPRD}{$H4_CODEGTIE}{$LIBLONG_PDT}
                              = $VALS->{'PRPRLIBLON'};
                            if (
                                (
                                    $H_TAB_PRD{$H4_CODEPRD}{$H4_CODEGTIE}
                                    {$DDEADH_GTIE} < $VALS->{'ACH4DDEADH'}
                                )
                                || ( !exists $H_TAB_PRD{$H4_CODEPRD}
                                    {$H4_CODEGTIE}{$DDEADH_GTIE} )
                              )
                            {
                                $H_TAB_PRD{$H4_CODEPRD}{$H4_CODEGTIE}
                                  {$DDEADH_GTIE} = $VALS->{'ACH4DDEADH'};
                            }
                            if ( $H4_CODEGTIE eq 'PB1F1-DC02' ) {
                                $H_TAB_PRD{$H4_CODEPRD}{$H4_CODEGTIE}{'QM'} =
                                  'OUI';
                            }
                            elsif ( $H4_CODEGTIE eq 'PB1F1-DC03' ) {
                                $H_TAB_PRD{$H4_CODEPRD}{$H4_CODEGTIE}{'QM'} =
                                  'NON';
                            }
                            if (
                                defined $H_TAB_PRD{$H4_CODEPRD}{$H4_CODEGTIE}
                                {'QM'} )
                            {
                                $DOC->append( 'QM',
                                    $H_TAB_PRD{$H4_CODEPRD}{$H4_CODEGTIE}{'QM'}
                                );
                            }
                        }
                        default {
                            $H_GTIE_OPT{$CODEPRD} = $CODEGTIE;
                            $H_INFO_GTIE{$LIBCODE_PDT}{$CODEGTIE} = $CODEPRD;
                            $H_INFO_GTIE{$LIBLONG_PDT}{$CODEGTIE} =
                              $VALS->{'PRPRLIBLON'};
                            if ( !exists $H_INFO_GTIE{$DDEADH_GTIE}{$CODEGTIE} )
                            {
                                $H_INFO_GTIE{$DDEADH_GTIE}{$CODEGTIE} =
                                  $VALS->{'ACH4DDEADH'};
                            }
                        }
                    }
                }
            }

            when (/H8/) {

                #         warn "INFO : dump H8 ". Dumper($VALS) ."\n";
                my $H8_CODEGTIE = $VALS->{'ACH4COD'};
                my $H8_CODEPRD  = $VALS->{'ACH4002'};
                my $DureeStage  = $VALS->{'ACSTDUR'};    #DUREE DE STAGE
                my $dtSTAGE     = $VALS->{'ACSTDFE'};    #DATE DE FIN DE STAGE
                given ($NUMCC) {
                    when (m/-HOS/) {

                        #                  print STDERR "INFO CONTRAT PLU\n";
                        $H_TAB_PRD{$H8_CODEPRD}{$H8_CODEGTIE}{'DELAISTAGE'} =
                          $DureeStage;
                        $H_TAB_PRD{$H8_CODEPRD}{$H8_CODEGTIE}{'DTFINSTAGE'} =
                          $dtSTAGE;
                    }
                    default {
                        if ( $ACTEGEST eq 'ACCC' || $ACTEGEST eq 'ACSO' ) {
                            if ( ( $dtSTAGE eq '' || $dtSTAGE < $dtActeGES )
                                && $dtFinSTAGE < $dtActeGES )
                            {
                                $dtFinSTAGE = $dtActeGES;
                            }
                            else {
                                if ( $DureeStage ne '00' ) {
                                    ###########Affichage du 1er jour du mois suivant
                                    $dtSTAGE =
                                      convertDate( $VALS->{'ACSTDFE'} );
                                    my @tabDt = split( '/', $dtSTAGE );
                                    if ( $tabDt[1] < 12 && $tabDt[1] > 0 ) {
                                        $tabDt[1]++;
                                        if ( $tabDt[1] < 10 ) {
                                            $dtSTAGE = $tabDt[2] . "0"
                                              . $tabDt[1] . "01";
                                        }
                                        else {
                                            $dtSTAGE =
                                              $tabDt[2] . $tabDt[1] . "01";
                                        }
                                    }
                                    elsif ( $tabDt[1] == 12 ) {
                                        $tabDt[2]++;
                                        $dtSTAGE = $tabDt[2] . "0101";
                                    }
                                }

                                if (   $dtFinSTAGE eq ''
                                    || $dtFinSTAGE < $dtSTAGE )
                                {
                                    $dtFinSTAGE = $dtSTAGE;
                                }

                            }

                        }
                    }
                }   #warn "INFO : dtFinSTAGE = $dtFinSTAGE \n";
                    #        warn "INFO : dump DOC ". Dumper($DOC) ."\n"; die();
            }

            when (/Z1/) {
                ############BLOC ID-MNT##################
                # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
                $NUMABA = $VALS->{'PERF003'};
                $DOC->append( 'NUMABA', $VALS->{'PERF003'} );
                ######INDEXATION GED###################
                $GED->append( 'xCLEGEDiii', $VALS->{'PERF003'} )
                  ;    #- N° adhérent (ID_MNT) == N° ABA
            }
        }
    }

    #      warn "INFO : dump H_TAB_PRT ". Dumper(\%H_TAB_PRD) ."\n";
    #      warn "INFO : dump H_TAB_ACCC ". Dumper(\%H_TAB_ACCC) ."\n";
    #      warn "INFO : dump H_TAB_ACSO ". Dumper(\%H_TAB_ACSO) ."\n";
    #      foreach my $k(keys %H_GTIE_OPT){
    #         print "Clef = $k \nValeur = $H_GTIE_OPT{$k} \n";
    #      }
    #      foreach my $k(keys %H_TAB_PRD){
    #         print "Clef = $k \nValeur = $H_TAB_PRD{$k} \n";
    #      }
    #      die;
    #    if ( $ACTEGEST eq 'ACCC' || $ACTEGEST eq 'ACSO' ) {
    ## SUITE A LA DEMANDE DE VIVIANE BOST:
    ## SI LA DATE DE FIN DE STAGE EST VIDE OU INFERIEURE A LA DATE D'EFFET DE L'ACTE DE GESTION
    ## ON AFFICHE LA DATE D'EFFET DE L'ACTE DE GESTION DANS LE COURRIER

    if ( $dtFinSTAGE eq '' || $dtFinSTAGE < $dtActeGES ) {
        $dtFinSTAGE = $dtActeGES;
    }

    $dtFinSTAGE =
      convertDate($dtFinSTAGE);    #DATE DE FIN DE STAGE ASSURE PRINCIPAL
    $DOC->append( 'DtFinSTAGE', $dtFinSTAGE );
    $DOC->append( 'GRPGEST',    $GRPGESTbool );     #Echéancier de cotisation
    $DOC->append( 'CODEOFFRE',  $codOFFREbool );    #CODE OFFRE

    #----------------------- GESTION DE LA NOPI  ------------------------------
    if (   exists( $H_GTIE_OPT{$LIBCODPRDGS1} )
        || exists( $H_GTIE_OPT{$LIBCODPRDGS2} ) )
    {
        $DOC->append( 'LETTRE', {'NOPI'} );
        if ( exists( $H_GTIE_OPT{$LIBCODPRDGS1} ) ) {
            $CODEGTIE = $H_GTIE_OPT{$LIBCODPRDGS1};
        }
        else {
            $CODEGTIE = $H_GTIE_OPT{$LIBCODPRDGS2};
        }
        $DOC->append( 'LIBLONGPDT', $H_INFO_GTIE{$LIBLONG_PDT}{$CODEGTIE} );
        $DOC->append( 'DATEADH',
            convertDate( $H_INFO_GTIE{$DDEADH_GTIE}{$CODEGTIE} ) );
        $DOC->append( 'LIBCODEPDT', 'NOPI' );
    }
    elsif ( $NUMCC =~ m/-HOS/ ) {

        if ( keys %H_TAB_PRD eq '2' ) {
            $DOC->append( 'TypeAdh', '3' );
        }
        else {
            foreach my $k ( keys %H_TAB_PRD ) {
                given ($k) {
                    when (/$LIBPRDHOSPS/) { $DOC->append( 'TypeAdh', '1' ); }
                    when (/$LIBPRDHOSDC/) { $DOC->append( 'TypeAdh', '2' ); }
                }
            }
        }
        given ($ACTEGEST) {
            when (/ACSO/) {
                $DOC->append( 'DATEADH',
                    convertDate( $H_TAB_ACSO{$DDEADH_GTIE} ) );
                $DOC->append( 'LIBLONGPDT', $H_TAB_ACSO{$LIBLONG_PDT} );

     #               warn "INFO : dump H_TAB_ACSO ". Dumper(\%H_TAB_ACSO) ."\n";
            }
            when (/ACCC/) {
                $DOC->append( 'DATEADH',
                    convertDate( $H_TAB_ACCC{$DDEADH_GTIE} ) );
                $DOC->append( 'LIBLONGPDT', $H_TAB_ACCC{$LIBLONG_PDT} );
            }
        }
        $DOC->append( 'LIBCODEPDT', 'PLURELYA' );
        given ($ACTEGEST) {
            when (/ACSO/) { $DOC->append( 'ACTE', 'ACSOHOS' ); }
            when (/ACCC/) {
                $DOC->append( 'ACTE', 'ACCCHOS' );
            }
        }
        $DOC->append( 'LETTRE', {'HOSPI'} );
    }

    $DOC->append('COURRIER');
    $DOC->append('FINCOURRIER');

    $TRK->track( 'Doc', 1, $NUMCTR, $xNOMDEST, $EMETTEUR, $ACTEGEST, $NUMABA );
    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);
    $DOC->reset();
    $GED->reset();
    oe_compo_link($OPT_GED);
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

exit main(@ARGV);
