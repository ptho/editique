#! /usr/bin/perl
use warnings;
use strict;
use feature qw( say switch );
use oEdtk::Main qw( oe_app_usage oe_corporation_set oe_new_job oe_compo_link );
use oEUser::Lib qw( user_corp_file_prefixe user_cleanup_addr )
  ;    # user_get_aneto_user -- useless here
use oEdtk::Tracking;
use Cwd qw( abs_path );
use Readonly;
use Data::Dumper qw( Dumper );
use feature qw ( switch );
use oEdtk::LaTeX qw( make_index tag);
use Carp;

Readonly my $DEBUG                => 0;
Readonly my $LIBELLE_LONG_INCONNU => 'Inconnu';    # code IN
Readonly my $USER_WEB_EDITIQUE =>
  'RejVirBanc';    # Cannot be extracted from manually crafted flows
Readonly my $COUNTRY_FRANCE => 'FRANCE';    # ignored in addresses

# French strings constants
Readonly my @FRENCH_MONTHS => (
    "janvier",   "f\N{LATIN SMALL LETTER E WITH ACUTE}vrier",
    "mars",      "avril",
    "mai",       "juin",
    "juillet",   "ao\N{LATIN SMALL LETTER U WITH CIRCUMFLEX}t",
    "septembre", "octobre",
    "novembre",  "d\N{LATIN SMALL LETTER E WITH ACUTE}cembre"
);

# Flow
Readonly my $NULL_VALUE   => 'null';     # not a real string in database exports
Readonly my $CSV_EOL_CHAR => "\n";       # Input flow will always be like that
Readonly my $CSV_SEP_CHAR => ';';        # Input flow will always be like that
Readonly my $CSV_ENCODING => 'utf-8';    # Input flow will always be like that

Readonly my $TYPE_ADH => 'ADH';          # Adherent
Readonly my $TYPE_PS  => 'PS';           # Professionnel de sante

Readonly my $ID_EMETTEUR_PS => 'CTT';

# Index
Readonly my $INDEX_DOC_TYPE_VALUE => 'CRVI';

# TODO refacto with Infinite.pm (in a Constants module or something)
Readonly my $INDEX_CODE_POSTAL_DEST => 'xCPDEST';
Readonly my $INDEX_VILLE_DEST       => 'xVILDEST';
Readonly my $INDEX_NOM_DEST         => 'xNOMDEST';

# TODO refacto with PRECON-EC010.pl (in a Constants module or something)
Readonly my $INDEX_CLEF_GED      => 'xCLEGED';
Readonly my $INDEX_ADRESSE_LISTE => 'xADRLN';
Readonly my $INDEX_ID_MNT_NUMABA => 'xIDDEST';
Readonly my $INDEX_ID_EMETTEUR   => 'xIDEMET';
Readonly my $INDEX_DOC_TYPE      => 'xTYPDOC';

# Codes courts maison (table Infinite U0154, mapping done with table Infinite U0663)
Readonly my $CODE_CV => 'CV';   # passage de cheque a virement
Readonly my $CODE_DC => 'DC';   # adherent decede
Readonly my $CODE_DR => 'DR';   # demande de RIB
Readonly my $CODE_IN => 'IN';   # inconnu (always grouped with PE in treatments)
Readonly my $CODE_PE => 'PE';   # paiement errone
Readonly my $CODE_OC => 'OC';   # Ordre client
Readonly my $VALID_CODES => { map { $_ => 1 }
      ( $CODE_CV, $CODE_DC, $CODE_DR, $CODE_IN, $CODE_PE, $CODE_OC ) };

# Field names (values must not collide)
# ADH and common fields
Readonly my $FIELD_MUTUELLE                => 'Mutuelle';
Readonly my $FIELD_ISO_20022_CODE          => 'ISO20022';
Readonly my $FIELD_CODE_ERREUR_INFINITE    => 'Code erreur Infinite';
Readonly my $FIELD_LIBELLE_ERREUR_INFINITE => 'Libelle erreur Infinite';
Readonly my $FIELD_CODE_MOTIF              => 'Code motif';
Readonly my $FIELD_MOTIF_DU_REJET          => 'Motif du rejet';
Readonly my $FIELD_TYPE_ADH_OU_PS          => 'Type';
Readonly my $FIELD_NUMERO_CONTRAT          => 'Numero contrat';
Readonly my $FIELD_CIVILITE                => 'Civilite';
Readonly my $FIELD_NOM                     => 'Nom';
Readonly my $FIELD_PRENOM                  => 'Prenom';
Readonly my $FIELD_ID_MNT                  => 'Id MNT';
Readonly my $FIELD_ADDRESS                 => 'Adresse';
Readonly my $FIELD_ADDRESS_BASE            => 'Adresse ligne';
Readonly my $FIELD_ADDRESS_1               => $FIELD_ADDRESS_BASE . ' 1';
Readonly my $FIELD_ADDRESS_2               => $FIELD_ADDRESS_BASE . ' 2';
Readonly my $FIELD_ADDRESS_3               => $FIELD_ADDRESS_BASE . ' 3';
Readonly my $FIELD_ADDRESS_4               => $FIELD_ADDRESS_BASE . ' 4';
Readonly my $FIELD_ADDRESS_5               => $FIELD_ADDRESS_BASE . ' 5';
Readonly my $FIELD_ADDRESS_6               => $FIELD_ADDRESS_BASE . ' 6';
Readonly my $FIELD_ADDRESS_7               => $FIELD_ADDRESS_BASE . ' 7';
Readonly my $FIELD_ADDRESS_CODE_POSTAL     => $FIELD_ADDRESS_5;
Readonly my $FIELD_ADDRESS_VILLE_DEST      => $FIELD_ADDRESS_6;
Readonly my $ADDRESS_INDEX_CODE_POSTAL     => 5;
Readonly my $FIELD_ER_NOM                  => 'ER Nom';
Readonly my $FIELD_ER_ADDRESS_1            => 'ER Adresse 1';
Readonly my $FIELD_ER_ADDRESS_2            => 'ER Adresse 2';
Readonly my $FIELD_ER_ADDRESS_3            => 'ER Adresse 3';
Readonly my $FIELD_ER_BOITE_POSTALE        => 'ER Boite postale';
Readonly my $FIELD_ER_CODE_POSTAL          => 'ER Code postal';
Readonly my $FIELD_ER_LOCALITE             => 'ER Localite';
Readonly my $FIELD_ER_CEDEX                => 'ER Cedex';
Readonly my $FIELD_ER_ID                   => 'ER identifiant';
Readonly my $FIELD_NUM_CPT_BANQUE          => 'Numero compte bancaire';
Readonly my $FIELD_LIB_FAM_DC              => 'Famille';

# Readonly my $FIELD_ER_PAYS               => 'ER Pays'; # unused (should always be 'FRANCE')
# PS-specific fields
Readonly my $FIELD_MONTANT_REJETE_EN_CENTIMES => 'Montant rejete en centimes';
Readonly my $FIELD_NUMERO_FINESS              => 'Numero Finess';
Readonly my $FIELD_CTT_BOITE_POSTALE => 'CTTBoitePostale';    # LaTeX tagname
Readonly my $FIELD_CTT_CODE_POSTAL   => 'CTTCodePostal';      # LaTeX tagname
Readonly my $FIELD_CTT_LOCALITE      => 'CTTLocalite';        # LaTeX tagname
Readonly my $FIELD_CTT_TELEPHONE     => 'CTTTelephone';       # LaTeX tagname
Readonly my $FIELD_CTT_FAX           => 'CTTFax';             # LaTeX tagname
Readonly my $FIELD_MONTANT_REJETE    => 'Montant rejete';
Readonly my @TRACKING_FIELDS => (qw( PS_OU_ADH ID_DEST NOM ISO20022 EMETTEUR ));
Readonly my @TRACKING_ADH_FIELDS => (
    $FIELD_NUMERO_CONTRAT, $FIELD_MONTANT_REJETE, $FIELD_CODE_MOTIF,
    $FIELD_TYPE_ADH_OU_PS, $FIELD_ER_NOM
);

Readonly my $PS_EMETTEUR =>
"Centre de Traitement des Professionnels de Sant\N{LATIN SMALL LETTER E WITH ACUTE}"
  ;                                                           # constant for PS
Readonly my @TRACKING_PS_FIELDS => (
    $FIELD_NUMERO_FINESS,  $FIELD_MONTANT_REJETE, $FIELD_CODE_MOTIF,
    $FIELD_TYPE_ADH_OU_PS, $FIELD_ISO_20022_CODE
);

# This column will hold the type of the row: adherent or PS
Readonly my $FLOW_COLUMN_TYPE => "S";
Readonly my $FLOW_FIELDS      => {      # As communicated by Farid
    ADH => {                            # Structure ligne adherents
        A                 => $FIELD_MUTUELLE,
        I                 => $FIELD_ISO_20022_CODE,
        N                 => $FIELD_NUM_CPT_BANQUE,
        K                 => $FIELD_MONTANT_REJETE_EN_CENTIMES,
        O                 => $FIELD_CODE_ERREUR_INFINITE,
        P                 => $FIELD_LIBELLE_ERREUR_INFINITE,
        Q                 => $FIELD_CODE_MOTIF,
        R                 => $FIELD_MOTIF_DU_REJET,
        $FLOW_COLUMN_TYPE => $FIELD_TYPE_ADH_OU_PS,
        T                 => $FIELD_NUMERO_CONTRAT,
        U                 => $FIELD_CIVILITE,
        V                 => $FIELD_NOM,
        W                 => $FIELD_PRENOM,
        X                 => $FIELD_ID_MNT,
        Y                 => $FIELD_ADDRESS_1,
        Z                 => $FIELD_ADDRESS_2,
        AA                => $FIELD_ADDRESS_3,
        AB                => $FIELD_ADDRESS_4,
        AC                => $FIELD_ADDRESS_5,                    # code postal
        AD => $FIELD_ADDRESS_6,          # ville de destination
        AE => $FIELD_ADDRESS_7,
        AF => $FIELD_ER_ID,
        AG => $FIELD_ER_NOM,
        AH => $FIELD_ER_ADDRESS_1,
        AI => $FIELD_ER_ADDRESS_2,
        AJ => $FIELD_ER_ADDRESS_3,
        AK => $FIELD_ER_BOITE_POSTALE,
        AL => $FIELD_ER_CODE_POSTAL,
        AM => $FIELD_ER_LOCALITE,
        AN => $FIELD_ER_CEDEX,

        # AO => $FIELD_ER_PAYS, # Ignored
    },
    PS => {    # Structure ligne professionnel de sante
        A  => $FIELD_MUTUELLE,
        I  => $FIELD_ISO_20022_CODE,
        N  => $FIELD_NUM_CPT_BANQUE,
        K  => $FIELD_MONTANT_REJETE_EN_CENTIMES,
        O  => $FIELD_CODE_ERREUR_INFINITE,
        P  => $FIELD_LIBELLE_ERREUR_INFINITE,
        Q  => $FIELD_CODE_MOTIF,
        R  => $FIELD_MOTIF_DU_REJET,
        S  => $FIELD_TYPE_ADH_OU_PS,
        T  => $FIELD_NUMERO_FINESS,
        U  => $FIELD_NOM,
        V  => $FIELD_ADDRESS_1,
        W  => $FIELD_ADDRESS_2,
        X  => $FIELD_ADDRESS_3,
        Y  => $FIELD_ADDRESS_4,
        Z  => $FIELD_ADDRESS_5,                    # code postal
        AA => $FIELD_ADDRESS_6,                    # ville dest
        AB => $FIELD_ADDRESS_7,
        AC => $FIELD_CTT_BOITE_POSTALE,
        AD => $FIELD_CTT_CODE_POSTAL,
        AF => $FIELD_CTT_LOCALITE,
        AG => $FIELD_CTT_TELEPHONE,
        AH => $FIELD_CTT_FAX,
    },
};

# Global variables
my ( $TRK, $MAP, $COLUMN_TYPE_INDEX, $DOC, $DATEEDT );
my $PDF_HAS_CONTENT = 0;

# Spreadsheet numbering: A->1, B->2, ... Z->26, AA->27...
sub column_name_to_number {
    my ($str) = @_;
    die "Bad column name '$str'" unless $str =~ m{^[A-Z]+$}sx;
    my $res = 0;
    while ( $str =~ m{([A-Z])}gsx ) {
        my $char = $1;
        my $val  = ord($char) - ord('A') + 1;
        $res = $val + ( $res * 26 );
    }
    return $res;
}

sub parse_csv_fields {
    my ($h) = @_;
    my $res = {};
    foreach my $type ( keys %{$h} ) {
        die "type '$type' multiply defined" if exists $res->{$type};
        $res->{$type} = {};
        foreach my $col_name ( keys %{ $h->{$type} } ) {
            my $col_index = column_name_to_number($col_name) - 1;
            die "col index '$col_index' multiply defined"
              if exists $res->{$type}->{$col_index};
            $res->{$type}->{$col_index} = $h->{$type}->{$col_name};
        }
    }
    return $res;
}

sub today {
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
      localtime(time);
    $mday = "1er" if $mday == 1;
    return sprintf( "%s %s %d", $mday, $FRENCH_MONTHS[$mon], 1900 + $year );
}

sub track_begin {
    my ( $trackingJob, $user ) = @_;
    $TRK = oEdtk::Tracking->new(
        $trackingJob,
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => \@TRACKING_FIELDS
    );
}

sub track {
    my ($l) = @_;
    warn "TRACKING: " . join( "\x20", @{$l} ) if $DEBUG;
    $TRK->track( 'D', 1, @{$l} );    # "D" as in "Doc"
}

sub track_end {
    $TRK->track( 'W', 1, '', '', '', '', 'Fin de traitement' )
      ;                              # "W" as in "Warning"
}

sub init {
    my ( $flowFile, $trackingJob ) = (@_);
    oe_app_usage()                              unless defined $flowFile;
    die "Flow file '$flowFile' does not exist"  unless -e $flowFile;
    die "Flow file '$flowFile' is not readable" unless -r $flowFile;
    unless ( defined $trackingJob ) {
        $trackingJob = Cwd::abs_path($flowFile);
        warn "Using trackingJob='$trackingJob'";
    }
    user_corp_file_prefixe( $flowFile, '_' );
    my $user = $USER_WEB_EDITIQUE;    # user_get_aneto_user($flowFile);
    $DOC               = oEdtk::TexDoc->new();
    $COLUMN_TYPE_INDEX = column_name_to_number($FLOW_COLUMN_TYPE) - 1;
    $MAP               = parse_csv_fields($FLOW_FIELDS);
    $DATEEDT           = today();
    track_begin( $trackingJob, $user );
}

sub trim_cells {
    my ($l) = @_;
    $_ =~ s{\s+$}{}sx foreach @{$l};
    $_ = ( ( $_ eq $NULL_VALUE ) ? "" : $_ ) foreach @{$l};
    return;
}

sub interpret_row {
    my ( $l, $map ) = @_;
    my $type = $$l[$COLUMN_TYPE_INDEX];
    die "Unknown type '$type' in row: " . Dumper($l)
      unless exists $map->{$type};
    my $res = {};
    my $hh  = $map->{$type};
    my $n   = scalar @{$l};
    foreach my $col_index ( keys %{$hh} ) {
        next if ( $n <= $col_index ) || ( !defined $$l[$col_index] );
        my $val = $$l[$col_index];
        $res->{ $hh->{$col_index} } = $$l[$col_index] if $val =~ m{\S}sx;
    }
    $res->{'index_variables'} = {
        $INDEX_CODE_POSTAL_DEST => $res->{$FIELD_ADDRESS_CODE_POSTAL},
        $INDEX_VILLE_DEST       => $res->{$FIELD_ADDRESS_VILLE_DEST},
        $INDEX_NOM_DEST         => lastnameThenMaybeFirstname($res),
    };
    my $code = $res->{$FIELD_CODE_MOTIF};
    return ( $code, $type, $res );
}

sub build_address {
    my ( $h, $keyPrefix, $min_index, $max_index, $code ) = @_;

    # NB: some are empty for PS
    my $fullName = "";
    if ( $CODE_DC eq $code ) {
        $fullName = $FIELD_LIB_FAM_DC . " "
          . join( " ", map { exists $h->{$_} ? $h->{$_} : "" } ($FIELD_NOM) );
    }
    else {
        $fullName = join( " ",
            map { exists $h->{$_} ? $h->{$_} : "" }
              ( $FIELD_CIVILITE, $FIELD_NOM, $FIELD_PRENOM ) );
    }
    my @l = ($fullName);
    for ( my $i = $min_index ; $i <= $max_index ; $i++ ) {
        my $str = "$keyPrefix $i";
        if ( exists $h->{$str} ) {
            my $val = $h->{$str};
            if ( $i == $ADDRESS_INDEX_CODE_POSTAL )
            {    # get the next value on the same line
                $i++;    # intended side effect!
                my $str2 = "$keyPrefix $i";
                my $val2 = $h->{$str2};
                $val .= " $val2" if $val2 =~ m{\S}sx;
            }
            push @l, $val if $val =~ m{\S}sx;
        }
    }
    pop @l if ( scalar @l ) && uc( $l[-1] ) eq uc($COUNTRY_FRANCE);
    die "No address found" unless scalar @l;
    @l = user_cleanup_addr(@l);
    return \@l;
}

sub lastnameFirstname {
    my ($h) = @_;
    return join( "\x20", map { $h->{$_} } ( $FIELD_NOM, $FIELD_PRENOM ) );
}

sub lastnameThenMaybeFirstname {    # No first name for PS
    my ($h) = @_;
    return ( exists $h->{$FIELD_PRENOM} )
      ? "$h->{$FIELD_NOM} $h->{$FIELD_PRENOM}"
      : $h->{$FIELD_NOM};
}

sub adresse_ER {
    my ($h) = @_;
    return join( "\x20",
        map { ( exists $h->{$_} && defined $h->{$_} ) ? $h->{$_} : () }
          ( $FIELD_ER_ADDRESS_1, $FIELD_ER_ADDRESS_2, $FIELD_ER_ADDRESS_3 ) );
}

sub boite_postale_ER {
    my ($h) = @_;
    return ( exists $h->{$FIELD_ER_BOITE_POSTALE}
          && defined $h->{$FIELD_ER_BOITE_POSTALE} )
      ? $h->{$FIELD_ER_BOITE_POSTALE}
      : '';
}

sub cedex_ou_CP_et_localite {
    my ($h) = @_;
    return $h->{$FIELD_ER_CEDEX}
      if ( exists $h->{$FIELD_ER_CEDEX}
        && defined $h->{$FIELD_ER_CEDEX}
        && $h->{$FIELD_ER_CEDEX} =~ m{\S}sx );
    return join( "\x20",
        map { ( exists $h->{$_} && defined $h->{$_} ) ? $h->{$_} : () }
          ( $FIELD_ER_CODE_POSTAL, $FIELD_ER_LOCALITE ) );
}

sub params_adherent {
    my ($h) = @_;
    return {
        DateEdition         => $DATEEDT,
        Civilite            => $h->{$FIELD_CIVILITE},
        NomPrenom           => lastnameFirstname($h),
        AdresseDestinataire => $h->{$FIELD_ADDRESS},
        NumeroAba           => $h->{$FIELD_ID_MNT},
        NumeroContrat       => $h->{$FIELD_NUMERO_CONTRAT},
        SectionNom          => $h->{$FIELD_ER_NOM},
        SectionAdresse      => adresse_ER($h),
        SectionBoitePostale => boite_postale_ER($h),
        SectionCPEtLocalite => cedex_ou_CP_et_localite($h),
        NumCompteBanque     => $h->{$FIELD_NUM_CPT_BANQUE},
        MontantRejete =>
          convertCents( $h->{$FIELD_MONTANT_REJETE_EN_CENTIMES} ),
        CodeMotif => $h->{$FIELD_CODE_MOTIF},
    };
}

sub treat_adherent {
    my ( $code, $h ) = @_;
    my $track_values = [ map { $h->{$_} } @TRACKING_ADH_FIELDS ];
    given ($code) {
        when ( $code eq $CODE_DR ) {
            out_adherent( $h, $track_values, 'LettreAdherentDR' );
        }
        when ( $code eq $CODE_DC ) {
            out_adherent( $h, $track_values, 'LettreAdherentDC' );
        }
        when ( $code eq $CODE_PE ) {
            out_adherent( $h, $track_values, 'LettreAdherentDR' );
        }
        when ( $code eq $CODE_IN ) {
            out_adherent( $h, $track_values, 'LettreAdherentDR' );
        }
        when ( $code eq $CODE_OC ) {
            return 0;
        }
        default { croak "Unknown code motif '$code': " . Dumper($h); }
    }
    return $track_values;
}

sub convertCents {
    my ($eurCents) = @_;
    $eurCents = sprintf( "%03d", $eurCents );
    if ( $eurCents < 10 ) {
        $eurCents = "00$eurCents";
    }
    elsif ( $eurCents < 100 ) {
        $eurCents = "0$eurCents";
    }
    my ( $eur, $cents ) =
      ( substr( $eurCents, 0, -2 ), substr( $eurCents, -2 ) );
    my $res = "$eur,$cents";
    return $res;
}

sub treat_PS {
    my ( $code, $h ) = @_;
    my $track_values =
      [ ( map { $h->{$_} } @TRACKING_PS_FIELDS ), $PS_EMETTEUR ];
    ### On imprime pas de courrier pour l'instant pour les code OC
    if ( $code eq $CODE_OC ) {
        return 0;
    }
    out_ps(
        {
            DateEdition         => $DATEEDT,
            AdresseDestinataire => $h->{$FIELD_ADDRESS},
            MontantRejete =>
              convertCents( $h->{$FIELD_MONTANT_REJETE_EN_CENTIMES} ),
            NumFiness       => $h->{$FIELD_NUMERO_FINESS},
            CTTBoitePostale => $h->{$FIELD_CTT_BOITE_POSTALE},
            CTTCodePostal   => $h->{$FIELD_CTT_CODE_POSTAL},
            CTTLocalite     => $h->{$FIELD_CTT_LOCALITE},
            CTTTelephone    => $h->{$FIELD_CTT_TELEPHONE},
            CTTFax          => $h->{$FIELD_CTT_FAX},
            NumCompteBanque => $h->{$FIELD_NUM_CPT_BANQUE},
            CodeMotif       => $h->{$FIELD_CODE_MOTIF},
        },
        $track_values,
        'LettrePS',
        $h->{'index_variables'}
    );
    return $track_values;
}

sub out {
    my ( $index, $tag_name, $h ) = @_;
    my $latex = tag( $tag_name, $h );

    # return print oEdtk::Main::OUT $index . $latex, "\n";
    oEdtk::Main::oe_print_to_output_file( sprintf '%s%s',
        $index . $latex, "\n" );
}

sub out_ps {
    my ( $h, $track_values, $tag_name, $index_values_arg ) = @_;
    my $index = make_index(
        $index_values_arg,
        $INDEX_DOC_TYPE         => $INDEX_DOC_TYPE_VALUE,
        "$INDEX_CLEF_GED#roman" => $track_values,
        $INDEX_ID_MNT_NUMABA    => $h->{'NumFiness'},
        $INDEX_ID_EMETTEUR      => $ID_EMETTEUR_PS,
        $INDEX_ADRESSE_LISTE    => $h->{'AdresseDestinataire'},
    );
    return out( $index, $tag_name, $h );
}

sub out_adherent {
    my ( $h, $track_values, $tag_name ) = @_;
    my $index = make_index(
        $h->{'index_variables'},
        $INDEX_DOC_TYPE         => $INDEX_DOC_TYPE_VALUE,
        "$INDEX_CLEF_GED#roman" => $track_values,
        $INDEX_ID_MNT_NUMABA    => $h->{$FIELD_ID_MNT},
        $INDEX_ID_EMETTEUR      => $h->{$FIELD_ER_ID},
        $INDEX_ADRESSE_LISTE    => $h->{$FIELD_ADDRESS},
    );
    return out( $index, $tag_name, params_adherent($h) );
}

sub need_to_skip {    # XXX rename in need_to_treat (and invert values)?
    my ( $code, $type, $h ) = @_;

    # die "Unknown code motif '$code': " . Dumper($h)
    croak "Le code $code est inconnu" unless exists $VALID_CODES->{$code};

    my $infinite_ec = $h->{$FIELD_CODE_ERREUR_INFINITE};
    if ( $infinite_ec ne "0000" ) {
        my $infinite_str = $h->{$FIELD_LIBELLE_ERREUR_INFINITE};
        if ( defined $infinite_str && $infinite_str =~ m{\S}sx ) {
            warn
              "Anomalie Infinite \#$infinite_ec: '$infinite_str' -- skipping";
            return 1;
        }
    }

    return 1
      if ( $type eq $TYPE_PS )
      && ( ( $code eq $CODE_CV ) || ( $code eq $CODE_DC ) );    # ignored
    return 1 if ( $type eq $TYPE_ADH ) && ( $code eq $CODE_CV );    # ignored
    return 0;
}

sub treat_row {
    my ($row) = @_;
    my ( $code, $type, $h ) = interpret_row( $row, $MAP );
    return if need_to_skip( $code, $type, $h );
    $PDF_HAS_CONTENT = 1;
    $h->{$FIELD_MONTANT_REJETE} =
      convertCents( $h->{$FIELD_MONTANT_REJETE_EN_CENTIMES} );
    $h->{$FIELD_MOTIF_DU_REJET} = $LIBELLE_LONG_INCONNU if $code eq $CODE_IN;
    $h->{$FIELD_ADDRESS} =
      build_address( $h, $FIELD_ADDRESS_BASE, 1, 7, $code );

    my $tracking_args;
    given ($type) {
        when ( $type eq $TYPE_ADH ) {
            $tracking_args = treat_adherent( $code, $h );
        }
        when ( $type eq $TYPE_PS ) { $tracking_args = treat_PS( $code, $h ); }
        default { die "Unknown type '$type'"; }
    }
    if ($tracking_args) {
        track($tracking_args);
    }
}

sub treat_csv {
    my ($flowFile) = @_;
    my $csv = Text::CSV->new(
        { binary => 1, eol => $CSV_EOL_CHAR, sep_char => $CSV_SEP_CHAR } );
    open my $io, "<:encoding($CSV_ENCODING)", $flowFile or die "$flowFile: $!";
    while ( my $row = $csv->getline($io) ) {
        trim_cells($row);    # or use String::Util::trim? / Text::Trim::trim?
        treat_row($row);
    }
    close $io;
    track_end();
}

sub main {
    my ($flowFile) = @_;
    init($flowFile);
    oe_new_job('--index');
    treat_csv($flowFile);
    oe_compo_link() if $PDF_HAS_CONTENT;
    return 0;
}

exit main(@ARGV);
