#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::E3;
use oEUser::Descriptor::T8;
use oEUser::Descriptor::F4;
use oEUser::Descriptor::OC;
use oEUser::Descriptor::L1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use Roman;
use Benchmark;

#use Date::Calc		(Add_Delta_Days);
# S'IL FALLAIT RAJOUTER UN JOUR À LA DATE PÉRIODE DE STAGE (plus nécessaire...)
# my ($stage_year,$stage_month,$stage_day) = unpack ("A4 A2 A2", $DATATAB[88]);
# my ($next_year,$next_month,$next_day) =
#	Add_Delta_Days($stage_year,$stage_month,$stage_day,
#		1);
# $date_stage = sprintf ("%4.0f%02.0f%02.0f", $next_year,$next_month,$next_day);

use strict;
use Time::Piece;

#use warnings;

# LES VARIABLES ET BIBLIOTHEQUES POUR LE DATAMATRIX
use Barcode::DataMatrix;
use PDF::API2;
use POSIX qw(strftime);
use constant mm => 25.4 / 72;
use constant in => 1 / 72;
use constant pt => 1;

### heure de lancement de script
my $t0 = new Benchmark;

my $mon_repertoire = '';
my $cpp            = 0;    #compteur pour le nombre des adhérents
my $NewModelCTP    = 0
  ; #SELON LA DATE DE DEBUT DE VALIDITE DE LA CARTE, A PARTIR DU 01/01/2017 ON EDITE LES NOUVELLES CARTES TP AVEC LE DATAMATRIX

# CORPS PRINCIPAL DE L'APPLICATION :

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)
my $OPT_GED;
my $NUMADH;
my $NOM                 = 0;
my $PRENOM              = 1;
my $NSECU               = 2;
my $DATENAIS            = 3;
my $DATE_DEBUT_VALIDITE = '';
my $DATE_FIN_VALIDITE   = '';
my $CDE_ER_SECTION      = "";    # RÉINITIALISATION TEST DE LA SECTION
my $EntiteJuridique     = "";
my $CodeOffre           = "";
my ( $DOC, $TRK, $VALS, $GED );
my $ACTEGEST;
my $TYPCARTE;
my $CARTEACS = 0;

# RÉINITIALISATION TEST CARTE ACS (CODE PRODUIT = ACS-P-STD1 OU ACS-P-PRI1)
my $COLOPTI = 0;                 # pas de colonne OPTI
my $COLOPAU = 0;                 # pas de colonne OPAU
my $COLAUDI = 0;                 # pas de colonne AUDI
my $COLHOCF = 0;                 # pas de colonne HOCF
my $MaxRank = 0;                 # Max rang dans les records L1
my $NbL1    = 0;                 # Le nombre des records L1
my $user    = ''
  ; # Le code de traitement : AC004 (en masse) ou 'nom de l'utilisateur' (local)
my $DtNaisASS;    # La date de naissance de l'assuré principal
my $NUMABA = '';  # ID-MNT

my @EADDR          = ();    #ADRESSE EMETTEUR
my @DADDR          = ();    #ADRESSE DESITINATEUR
my @EnteteColGar   = ();
my @Garantie       = ();
my @DATE_STAGE_GAR = ();

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    user_corp_file_prefixe( $argv[0], '_' );

    # 31/08/2017 JLE recuperation prefix du flux
    my $prefix = oe_corporation_set();

    $user = user_get_aneto_user( $argv[0] );

    # warn "INFO : dump USER : ". $user ."\n";

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMADH', 'SECTION', 'ACTEGEST', 'ID-MNT', 'PERIODE' ]
    );

    if ( $user eq 'AC004' ) {
        $GED     = oEdtk::TexDoc->new();
        $OPT_GED = '--edms';
        oe_new_job('--index');
    }
    else {
        $OPT_GED = '';
        oe_new_job();
    }

    # INITIALISATION PROPRE AU DOCUMENT
    NewRept();
    initDoc();

    my $E1 = oEUser::Descriptor::E1->get();

    # $E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    # $E2->debug();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    # $E7->debug();
    $E7->bind_all_c7();
    my $E3 = oEUser::Descriptor::E3->get();
    $E3->bind_all_c7();
    my $T8 = oEUser::Descriptor::T8->get();
    $T8->bind_all_c7();
    my $F4 = oEUser::Descriptor::F4->get();
    $F4->bind_all_c7();
    my $OC = oEUser::Descriptor::OC->get();
    $OC->bind_all_c7();
    my $L1 = oEUser::Descriptor::L1->get();

    #$L1->debug();
    $L1->bind_all_c7();

#$L1->bind('CE0011C-L1-ACH4OPTMUT' => 'ACH4OPT', 'CE0011C-L1-ACH2-DATNAI' => 'DATNAI',
#	'CE0011C-L1-RO1-NUMRO' => 'NUMADH');
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $DOC->append( 'xTYPDOC', 'CTIP' );    ### Type de document

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'E3' => $E3,
        'F4' => $F4,
        'T8' => $T8,
        'L1' => $L1,
        'OC' => $OC,
        'Z1' => $Z1
    );

    my $lastid;

    # $DOC->include('toto.tex', 'EDTK_DIR_SCRIPT'); # xxxxxxxxxxxxx test Alain

    my $NumLigneFlux = 0;
    my $NumCarteFlux = 0;

    while ( ( my $id, $VALS ) = $p->next() ) {
        $NumLigneFlux++;

        # print STDERR "INFO : Num Ligne en cours : $NumLigneFlux\n";

        next if $id eq 'OC';

        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n"; die();

            $NumCarteFlux++;
            print STDERR "INFO : Num Carte TP en cours : $NumCarteFlux\n";

            finDetailAdherent( $lastid, $DOC );

# Chargement de l'adresse de destinateur et de la section selon le type de flux:
# NORME 38 : ON UTILISE LA RECORD E7
            @DADDR = ();
            push( @DADDR,
                "$VALS->{'PENOCOD'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}" )
              ;    # $VALS->{'PENOCOD'}

            @EADDR = ();

            my $debutEADDR = "$VALS->{'PENO027'} ";

            # . "-"
            # . " $VALS->{'PENO025'} " . "-"
            # . " $VALS->{'CPLA029'} " . "-"
            # . " $VALS->{'PNTR028'}";

            # print "BEFORE : $debutEADDR\n";
            $debutEADDR = suppEspace($debutEADDR);

            # print "AFTER : $debutEADDR\n";

            push( @EADDR, $debutEADDR );

            my $ENTITE = $VALS->{'ODCGCOD'};

            ############ Entité juridique ##############
                $DOC->append( 'ENTITE', 'MNT' );
                $EntiteJuridique = 'MNT';

            $CDE_ER_SECTION = $VALS->{'ODERCOD'};

            if ( $CDE_ER_SECTION =~ /971/i ) {
                $DOC->append( 'SWTCHTEL', {'NumUn'} );
            }
            elsif ( $CDE_ER_SECTION =~ /972/i ) {
                $DOC->append( 'SWTCHTEL', {'NumDeux'} );
            }
            elsif ( $CDE_ER_SECTION =~ /973/i ) {
                $DOC->append( 'SWTCHTEL', {'NumTrois'} );
            }
            elsif ( $CDE_ER_SECTION =~ /974/i ) {
                $DOC->append( 'SWTCHTEL', {'NumQuatre'} );
            }
            elsif ( $CDE_ER_SECTION =~ /N001/i ) {
                $DOC->append( 'SWTCHTEL', $VALS->{'PECOVAL'} );
            }
            else {
                $DOC->append( 'SWTCHTEL', {'Indigo'} );
            }

     # DESTINATEUR
     # my $NOMDEST = "$VALS->{'PENOCOD'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
            my $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            ################CENTRE DE GESTION###############################
            my $LIBCENTRE = '';

            if ( $ENTITE eq 'MNT-RC' ) {
                $LIBCENTRE = 'SANTE';    ### Domaine métier
            }
            else {
                $LIBCENTRE = 'PREV';     ### Domaine métier
            }

            ############### ER ::::: Emetteur###################
            if ( $CDE_ER_SECTION eq '' ) {
                $CDE_ER_SECTION = "ER";
            }

            ######INDEXATION GED###################
            if ( $user eq 'AC004' ) {
                $GED->append( 'xSOURCE', $LIBCENTRE );    #- Domaine métier
                  # $GED->append('xCORP',$VALS->{'PENO027'});		#- Entité juridique
                  # $GED->append('xOWNER', 'BUR');  				#- Domaine de métier
                $GED->append( 'xIDEMET', $CDE_ER_SECTION );    #- Emetteur
                $GED->append( 'xNOMDEST', $xNOMDEST );    #- Nom du destinataire
                $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
                  ;    #- Ville du destinataire
                $GED->append( 'xCPDEST',
                    oe_iso_country() . $VALS->{'CODCDXC'} );    #-	Code postal

# $GED->append('xCLEGEDv',"Num ABA_$CDE_ER_SECTION");  #- Code Alliage MNT : « ID_MNT _ Code ER... » //// Vu avec Frédéric pour supprimer ce champs de l'index
            }

        }
        elsif ( $id eq 'E2' ) {

           # warn "INFO : dump E2 ". Dumper($VALS) ."\n"; die();
           # warn "INFO : E2-CODE ACTE DE GESTION : ". $VALS->{'ACOPCOD'} ."\n";
           # warn "INFO : E2-TYPE CARTE : ". $VALS->{'ACGACOD'} ."\n";
            $cpp++;

            # NUM ADHERENT
            $NUMADH = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMADH', $NUMADH );

            #CODE ACTE DE GESTION
            $ACTEGEST = $VALS->{'ACOPCOD'};

            # Type de carte
            $TYPCARTE = $VALS->{'ACGACOD'};

            $CodeOffre = $VALS->{'PROFCOD'};    # code offre

            #############INDEXATION GED###################
            if ( $user eq 'AC004' ) {
                $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
                  ;                             #-	Numéro de contrat collectif

                my $DATEEDT = convertDate( $VALS->{'DATEDTx'} )
                  ;                             #Date du Traitement d'Edition
                $GED->append( 'xDTEDITION', $DATEEDT )
                  ;                             #Date du Traitement d'Edition

                $GED->append( 'xCLEGEDi', $NUMADH )
                  ;                             #-	Numéro de contrat individuel

                # my $GESTIONNAIRE = $VALS->{'PENO083'}.' '.$VALS->{'PENO084'};
                # $GED->append('xCLEGEDii',$GESTIONNAIRE);  		#-	Gestionnaire

                $GED->append( 'xCLEGEDiii', $VALS->{'ACGACOD'} )
                  ;    #- N° groupe assuré
            }

        }
        elsif ( $id eq 'E7' ) {

            # warn "INFO : dump E7 ". Dumper($VALS) ."\n";

            # Chargement de l'adresse de destinateur et de la section
            user_E7_build_addr( $DOC, $VALS );

        }
        elsif ( $id eq 'F4' ) {

            # ON REMPLACE LES EN TETE DE COLONNE VIDE PAR DES ETOILES
            my @colids =
              ( 'ACIPENT', map { sprintf( "ACIP%03d", $_ ) } ( 6 .. 15 ) );
            my $chr = ord('A');
            foreach my $colid (@colids) {
                if ( $VALS->{$colid} eq '' ) {
                    $VALS->{$colid} = '***';
                }
                elsif ( $VALS->{$colid} eq 'AUDI' ) {
                    $COLAUDI = 1;
                }
                elsif ( $VALS->{$colid} eq 'OPTI' ) {
                    $COLOPTI = 1;
                }
                elsif ( $VALS->{$colid} eq 'OPAU' ) {
                    $COLOPAU = 1;
                }
                elsif ( $VALS->{$colid} eq 'HOCF' ) {
                    $COLHOCF = 1;
                }

                $DOC->append( 'COLHEAD' . chr( $chr++ ), $VALS->{$colid} );
            }

# ON CONSERVE LA DATE DE DÉBUT DE VALIDITÉ DE LA CARTE POUR POUVOIR LA
# COMPARER À LA DATE DE DÉBUT DE VALIDITÉ DE CHACUNE DES GARANTIES POUR CHACUN DES AYANTS DROITS
            $DATE_DEBUT_VALIDITE = $VALS->{'ACIPDDV'};
            my $tp1 = Time::Piece->strptime( $DATE_DEBUT_VALIDITE, "%d/%m/%Y" );
            my $tp2 = Time::Piece->strptime( '01/01/2017',         "%d/%m/%Y" );

            # warn "INFO : DATE_DEBUT_VALIDITE = ". $DATE_DEBUT_VALIDITE ."\n";
            if ( $tp1 >= $tp2 ) {
                $NewModelCTP = 1;
                dataMatrix();    #GENERATION DATAMATRIX
            }
            else {
                $NewModelCTP = 0;
            }

            # DATE FIN DE VALIDITE
            $DATE_FIN_VALIDITE = $VALS->{'ACIPDFV'};

            $DOC->append( 'NewModelCTP', $NewModelCTP );
            $DOC->append( 'ACIPDDV',     $VALS->{'ACIPDDV'} );
            $DOC->append( 'ACIPDFV',     $VALS->{'ACIPDFV'} );

        }
        elsif ( $id eq 'L1' ) {

            # warn "INFO : dump L1 ". Dumper($VALS) ."\n"; die();
            $NbL1++;

            # LIGNE DE DÉTAIL ADHÉRENT
            # MEMORISE POUR CHAQUE RANG D'ADHERENT (DATATAB 11)
            # SES NOMS PRENOMS NUM SECU ET DATE DE NAISSANCE
            my $rank    = $VALS->{'CE00011'};
            my $LibRank = $VALS->{'LIBLONT'};

            if ( $rank > $MaxRank ) { $MaxRank = $rank; }
            if ( $rank eq '01' || $LibRank eq 'Assuré principal' ) {
                $DtNaisASS = $VALS->{'CE00018'};
            }
            $Garantie[$rank][$NOM]    = $VALS->{'CE00005'};
            $Garantie[$rank][$PRENOM] = $VALS->{'CE00006'};
            $Garantie[$rank][$NSECU] =
              $VALS->{'CE00022'} . ' ' . $VALS->{'CE00023'};
            $Garantie[$rank][$DATENAIS] = $VALS->{'CE00018'};

            # LES GARANTIES DANS $DATATAB[29] PEUVENT ÊTRE ASSEMBLÉES
            # ON EFFECTUE DONC UN DÉCOUPAGE

            # warn "INFO L1-LA GARANTIE : ".$VALS->{'ACH4OPT'}."\n";
            my @tGar = split( / /, $VALS->{'ACH4OPT'} );

            # ON TRAITE CHAQUE ÉLÉMENT TROUVÉ UN PAR UN
            while ( my $valeur = pop @tGar ) {

                warn "INFO L1-LA GARANTIE - $rank : $valeur\n";

                # POUR CHAQUE RANG D ADHERENT (DATATAB 11) ON MEMORISE
                # LA GARANTIE EN LUI DONNANT UN NUMERO D ORDRE.
                if ( $valeur =~ /SE/ ) {
                    $Garantie[$rank][23] = $valeur;
                }
                elsif (
                       ( $valeur =~ /EV/ )
                    || ( $valeur =~ /ES/ )
                    || ( $valeur =~ /HO/ )
                    || ( $valeur =~ /E1/ )
                    || ( $valeur =~ /E2/ )
                    || ( $valeur =~ /BA/ )
                    || ( $valeur =~ /CF/ )
                    || ( $valeur =~ /SA/ )
                    || ( $valeur =~ /E6/ )

                    # JLE 31/07/2017 mise a jour regle de gestion
                    || ( $valeur =~ /MNT[1-9]{1}/i )

                  )
                {

          # 31/08/2017 JLE remplacement code garantie par code garantie mutacite
          # if ($prefix eq 'MNT')
          # {
                    ##	ne rien faire
                    # }
                    # elsif ($prefix eq 'MUTACITE')
                    # {
                    # $valeur =~ s/MNT/MUT/;
                    # }

                    # ecriture test
                    # open (my $desc,'>>','c:/ctpcb-ac001.txt');
                    # print $desc $VALS->{'CE00022'}."	";
                    # print $desc 'prefix' . $prefix ."\n";
                    # print $desc Dumper($VALS) ."\n";
                    # print $desc $valeur."\n";
                    # close $desc;

                    $Garantie[$rank][10] = $valeur;
                }
                elsif ( $valeur =~ /SMP/ ) {

                    # on ne fait rien exprès pour le faire disparaitre
                }
                elsif ( $valeur =~ /SME/ ) {

                    # on ne fait rien exprès pour le faire disparaitre
                }
                elsif ( $valeur =~ /ER/ ) {
                    $Garantie[$rank][11] = $valeur;
                }
                elsif ( $valeur =~ /C1/ ) {
                    $Garantie[$rank][12] = $valeur;
                }
                elsif ( $valeur =~ /C2/ ) {
                    $Garantie[$rank][13] = $valeur;
                }
                elsif ( $valeur =~ /C3/ ) {
                    $Garantie[$rank][14] = $valeur;
                }
                elsif ( $valeur =~ /C4/ ) {
                    $Garantie[$rank][15] = $valeur;
                }
                elsif ( $valeur =~ /C5/ ) {
                    $Garantie[$rank][16] = $valeur;
                }
                elsif ( $valeur =~ /C6/ ) {
                    $Garantie[$rank][17] = $valeur;
                }
                elsif ( $valeur =~ /MJ/ ) {
                    $Garantie[$rank][18] = $valeur;
                }
                elsif ( $valeur =~ /MF/ ) {
                    $Garantie[$rank][19] = $valeur;
                }
                elsif ( $valeur =~ /MS/ ) {
                    $Garantie[$rank][20] = $valeur;
                }
                elsif ( $valeur =~ /IH1/ ) {
                    $Garantie[$rank][21] = $valeur;
                }
                elsif ( $valeur =~ /IH2/ ) {
                    $Garantie[$rank][22] = $valeur;
                }
                elsif ( $valeur =~ /RE/ ) {
                    $Garantie[$rank][24] = $valeur;
                }
                elsif ( $valeur =~ /EN/ ) {
                    $Garantie[$rank][25] = $valeur;
                }
                elsif ( $valeur =~ /MP/ ) {
                    $Garantie[$rank][26] = $valeur;
                }
                elsif ( $valeur =~ /CMU/ ) {
                    $Garantie[$rank][27] = $valeur;
                }
                elsif ( $valeur =~ /CNSD/ ) {
                    $Garantie[$rank][28] = $valeur;
                }
                elsif ( $valeur =~ /CD/ ) {    # CNSD en version courte
                    $Garantie[$rank][29] = $valeur;
                }
                elsif ( $valeur =~ /IJH/ ) {
                    $Garantie[$rank][30] = $valeur;
                }
                elsif ( $valeur =~ /PSR/ ) {
                    $Garantie[$rank][31] = $valeur;
                }
                elsif ( $valeur =~ /PFT/ ) {
                    $Garantie[$rank][32] = $valeur;
                }
                else {
# on empile les garanties "AUTRES" que le métier il aurait oublié de nous avertir
                    $Garantie[$rank][99] .= $valeur;
                }
            }

# POUR CHAQUE RANG D'ADHERENT (DATATAB 11) ON MEMORISE LES CONDITIONS D'APPLICATIONS DES
# GARANTIES CITEES EN ENTETE DE COLONNE
# (on pourrait supprimer le if qui fait doublon avec le traitement de substitution dans finDetAdh)
            $EnteteColGar[$rank][0] = $VALS->{'ACH4TYP'}
              if $VALS->{'ACH4TYP'} ne ' ';
            $EnteteColGar[$rank][1] = $VALS->{'ACH4062'}
              if $VALS->{'ACH4062'} ne ' ';
            $EnteteColGar[$rank][2] = $VALS->{'ACH4072'}
              if $VALS->{'ACH4072'} ne ' ';
            $EnteteColGar[$rank][3] = $VALS->{'ACH4082'}
              if $VALS->{'ACH4082'} ne ' ';
            $EnteteColGar[$rank][4] = $VALS->{'ACH4092'}
              if $VALS->{'ACH4092'} ne ' ';
            $EnteteColGar[$rank][5] = $VALS->{'ACH4102'}
              if $VALS->{'ACH4102'} ne ' ';
            $EnteteColGar[$rank][6] = $VALS->{'ACH4112'}
              if $VALS->{'ACH4112'} ne ' ';
            $EnteteColGar[$rank][7] = $VALS->{'ACH4122'}
              if $VALS->{'ACH4122'} ne ' ';
            $EnteteColGar[$rank][8] = $VALS->{'ACH4132'}
              if $VALS->{'ACH4132'} ne ' ';
            $EnteteColGar[$rank][9] = $VALS->{'ACH4142'}
              if $VALS->{'ACH4142'} ne ' ';

# DATE PÉRIODE DE STAGE PAR GARANTIE : ON NE GARDE QUE LE MOIS ET L'ANNÉE SANS SÉPARATEUR
            for ( my $i = 0 ; $i < 10 ; $i++ ) {
                my $tag = sprintf( 'ACH4%03d', 48 + $i * 10 );
                $DATE_STAGE_GAR[$rank][$i] = $VALS->{$tag};
            }

# si le Code produit est  ACS-P-STD alors il ne faut pas afficher l'adresse emetteur
            if (   $VALS->{'ACH4COD'} eq 'ACS-P-STD1'
                || $VALS->{'ACH4COD'} eq 'ACS-P-PRI1' )
            {
                $CARTEACS = 1;
            }

        }
        elsif ( $id eq 'T8' ) {

            #On fait rien
        }
        elsif ( $id eq 'Z1' ) {

            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $VALS->{'PERF003'} );
            ######INDEXATION GED###################
            if ( $user eq 'AC004' ) {
                $GED->append( 'xCLEGEDiv', $VALS->{'PERF003'} )
                  ;    #- N° adhérent (ID_MNT) == N° ABA
            }
        }

        $lastid = $id;
    }

    # CERTAINS FLUX SE TERMINENT PRÉMATURÉMENT, SANS T8
    finDetailAdherent( $lastid, $DOC );

    oe_compo_link($OPT_GED);

    #### HEURE DE FIN DE SCRIPT
    my $t1 = new Benchmark;

    #### LE TEMPS DE TRAITEMENT
    my $td = timediff( $t1, $t0 );
    print STDERR "INFO : LE TEMPS DE TRAITEMENT : " . timestr($td) . "\n";
    return 0;
}

sub initDoc {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    undef @EnteteColGar;    # TABLEAU DES ENTÊTES DE COLONNES
    undef @Garantie;        # TABLEAU DES GARANTIES
    undef @DATE_STAGE_GAR;
    undef @EADDR;           #ADRESSE EMETTEUR
    undef @DADDR;           #ADRESSE DESITINATEUR

    $NUMADH              = '';
    $CDE_ER_SECTION      = '';    # RÉINITIALISATION TEST DE LA SECTION
    $DATE_DEBUT_VALIDITE = '';
    $DATE_FIN_VALIDITE   = '';
    $EntiteJuridique     = "";
    $CodeOffre           = "";
    $ACTEGEST            = '';
    $TYPCARTE            = '';
    $CARTEACS            = 0
      ; # RÉINITIALISATION TEST CARTE ACS (CODE PRODUIT = ACS-P-STD1 OU ACS-P-PRI1)
    $COLOPTI   = 0;     # pas de colonne OPTI
    $COLOPAU   = 0;     # pas de colonne OPAU
    $COLAUDI   = 0;     # pas de colonne AUDI
    $COLHOCF   = 0;     # pas de colonne HOCF
    $MaxRank   = 0;     # Max rang dans les records L1
    $NbL1      = 0;     # Le nombre des records L1
    $DtNaisASS = '';    # La date de naissance de l'assuré principal
}

sub dataMatrix {

# GENERER UN DATAMATRIX FORMAT PDF
# Selon le "CDC_harmonisation des attestations AMC V2.1" nous sommes dans le deuxieme cas d'usage "Adhérent avec un seul type de convention pour tous les domaines"
# Voici la structure du DataMatrix : préfixe#n° version#n° AMC#CSR#n° adhérent#Type convention par domaine/

    my $x  = 2;
    my $y  = 44;
    my $sz = 2;

    my $NUMAMC  = "";    # N° AMC
    my $CSR     = "";    # CSR : Non utilisé
    my $TYPCONV = '';

    my $tp1 = Time::Piece->strptime( $DATE_DEBUT_VALIDITE, "%d/%m/%Y" );
    my $tp2 = Time::Piece->strptime( '01/01/2018',         "%d/%m/%Y" );

    # warn "INFO : DATE_DEBUT_VALIDITE = ". $DATE_DEBUT_VALIDITE ."\n";
    if ( $tp1 >= $tp2 ) {
        $TYPCONV = "MU*";
    }
    else {
        $TYPCONV = "HD*";
    }

    if ( $CDE_ER_SECTION eq 'N001' ) {

        # SALARIES MNT
        $NUMAMC = "444042303";
    }
    else {

        # MNT (MNT-CMU / ACS-MNT-NON CMU / ACS-MGEFI-NON CMU)
        $NUMAMC = "775678584";
    }

    my $TxtDataMatrix =
      'AMC#1#' . $NUMAMC . '#' . $CSR . '#' . $NUMADH . '#' . $TYPCONV . '/';

### À réécrire -> appeller zint pour avoir le code du datamatrix et sortir dans $DOC pour chaque ligne, une ligne du type \hhline{-~-~-~-~-~-~-~-~-~-~-~}
##  zint -o datamatrix.txt --barcode=71 --direct -d AMC#1#775678584##0003001589#HD*/

    my $mat = Barcode::DataMatrix->new()->barcode($TxtDataMatrix);

    #    my $pdf  = PDF::API2->new();
    #    my $page = $pdf->page();
    #    $page->mediabox( 17 / mm, 17 / mm );
    #    my $content = $page->gfx();
    oe_print_to_output_file("\\long\\gdef\\datamatrix{");

    for my $i ( 0 .. $#$mat ) {
        oe_print_to_output_file("\\hhline{");
        for my $j ( 0 .. $#{ $mat->[$i] } ) {
            if ( $mat->[$i][$j] ) {
                oe_print_to_output_file('-');
            }
            else {
                oe_print_to_output_file('~');
            }
        }
        oe_print_to_output_file('}');
    }
    oe_print_to_output_file('}');

    #    $content->fill();
    #    my $namePDF = $mon_repertoire . '/' . $NUMADH . $cpp . '.pdf';
    #    $DOC->append( 'namePDF', $namePDF );
    #    $pdf->saveas($namePDF);
}

sub NewRept {

    #CREATION D'UN REPERTOIRE TEMPORAIRE POUR STOCKER LES DATAMATRIX FORMAT PDF
    $mon_repertoire = strftime "%Y%m%d%H%M%S", localtime;

    print STDERR
      "INFO : Mon repertoire temporaire est : .\\$mon_repertoire\\ \n";
    mkdir($mon_repertoire) or die("Erreur creation repertoire\n");
}

sub DeleteRept {

    #SUPPRESSION DU REPERTOIRE TEMPORAIRE DU DATAMATRIX
    my $rep = $mon_repertoire . '/';
    my ( @fichiers, $fic );

    opendir( REP, $rep );
    @fichiers = readdir(REP);
    closedir(REP);

    foreach my $fic (@fichiers) {
        if ( ( $fic ne "." ) && ( $fic ne ".." ) ) {
            if ( -d "$rep/$fic" ) {
                rmtree( ["$rep/$fic"], 1, 1 );
            }
            else {
                unlink("$rep/$fic");
            }
        }
    }

    rmdir($rep);
}

sub finDetailAdherent {
    my ( $lastid, $DOC ) = @_;

    if ( $NbL1 != 0 ) {
        $DOC->append( 'CARTEACS', $CARTEACS );
        $DOC->append( 'Rappel',   '' );

        #### Pour les ACS (code produit ACS-P-STD1) et les ACS PRIM (code produit ACS-P-PRI1)
        #### il faut afficher l'adresse de Bordeaux au lieu de celle de la section de rattachement
        if ( $CARTEACS eq 1 ) {
            @EADDR = ();
            push( @EADDR, "MNT ACS - TSA 30016" );
            push( @EADDR, "33044 Bordeaux Cedex" );
            $DOC->append( 'SWTCHTEL', {'ACS'} );
        }

        @EADDR = user_cleanup(@EADDR);
        $DOC->append_table( 'EADDR', @EADDR );

    # LORSQUE L'ON A TRAITE TOUTES LES LIGNES DETAILS ADHERENT
    # ON FORMATTE UNE BALISE POUR QUE COMPUSET SORTE UNE SEULE LIGNE SYNTHETIQUE
        return unless defined $lastid;

        my $count  = 0;
        my $detail = oEdtk::TexDoc->new;

        if ( $user eq 'AC004' ) {
            $GED->append( 'xCLEGEDii', $DtNaisASS );    #- Date de naissance
        }

        my $Rappel3    = 0;
        my $RappelHOCF = 0;

        for ( my $RangAdh = 0 ; $RangAdh <= $MaxRank ; $RangAdh++ ) {
            if ( $Garantie[$RangAdh][$NOM] ) {

                # warn "Nb BENEF : ". $count ."\n";
                # MAX 5 BENEF PAR CARTE
                if ( $count > 4 ) {
                    $DOC->append( 'DetAdh', $detail );
                    if ( $user eq 'AC004' ) {

                        # warn "INFO : dump GED - NEWCTP ". Dumper($GED) ."\n";
                        oe_print_to_output_file($GED);
                        $GED->reset();
                    }
                    $DOC->append('EDITDOC');
                    $DOC->append('NEWCTP');
                    $count  = 0;
                    $detail = oEdtk::TexDoc->new();
                }

                $detail->append( 'NOMADH',
                    "$Garantie[$RangAdh][$NOM] $Garantie[$RangAdh][$PRENOM]" );
                $detail->append( 'DATENAIS', $Garantie[$RangAdh][$DATENAIS] );
                $detail->append( 'NUMSECU',  $Garantie[$RangAdh][$NSECU] );

                my @gars = ();
                for my $i ( 10 .. 99 ) {

                   # warn "GAR $RangAdh - $i : ". $Garantie[$RangAdh][$i] ."\n";
                    push( @gars, $Garantie[$RangAdh][$i] )
                      if $Garantie[$RangAdh][$i];
                }
                $detail->append( 'CODEGARS', join( ' ', @gars ) );
                $detail->append('ResetStage');

                for ( my $i = 0 ; $i <= 9 ; $i++ ) {
                    if ( $EnteteColGar[$RangAdh][$i] eq "" ) {
                        $EnteteColGar[$RangAdh][$i] = "***";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "*" ) {
                        $EnteteColGar[$RangAdh][$i] = "***";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "%" ) {
                        $EnteteColGar[$RangAdh][$i] = "100%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "5" )
                    {    # sesame 928605
                        $EnteteColGar[$RangAdh][$i] = "50%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "C" ) {
                        $EnteteColGar[$RangAdh][$i] = "CNSD";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "O" ) {
                        $EnteteColGar[$RangAdh][$i] = "OUI";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "D" ) {
                        $EnteteColGar[$RangAdh][$i] = "50%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "E" ) {
                        $EnteteColGar[$RangAdh][$i] = "85%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "F" ) {
                        $EnteteColGar[$RangAdh][$i] = "125%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "G" ) {
                        $EnteteColGar[$RangAdh][$i] = "150%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "H" ) {
                        $EnteteColGar[$RangAdh][$i] = "200%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "I" ) {
                        $EnteteColGar[$RangAdh][$i] = "25%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "J" ) {
                        $EnteteColGar[$RangAdh][$i] = "40%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "K" ) {
                        $EnteteColGar[$RangAdh][$i] = "35%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "L" ) {
                        $EnteteColGar[$RangAdh][$i] = "65%";
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "R" ) {
                        $EnteteColGar[$RangAdh][$i] =
                          "RFH";    # EN PRÉVISION DE COMPLEM'TER
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "A" )
                    {               # && ($CDE_ER_SECTION =~/971|972|973|974/)){
                        $EnteteColGar[$RangAdh][$i] = "(A)";
                        $DOC->append( 'Rappel', {'RappelA'} );
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "1" )
                    {               # && ($CDE_ER_SECTION !~/971|972|973|974/)){
                        $EnteteColGar[$RangAdh][$i] = "(1)";
                        $DOC->append( 'Rappel', {'RappelUN'} );
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "2" ) {
                        $EnteteColGar[$RangAdh][$i] = "(2)";
                        $DOC->append( 'Rappel', {'RappelDEUX'} );
                    }
                    elsif (
                        $EnteteColGar[$RangAdh][$i] eq "3"
                        && ( ( $COLOPTI eq 1 && $COLAUDI eq 1 )
                            || $COLOPAU eq 1 )
                      )
                    {
                        $EnteteColGar[$RangAdh][$i] = "(3)";
                        $DOC->append( 'Rappel', {'RappelTROIS'} );
                        $Rappel3 = 1;
                    }
                    elsif ($EnteteColGar[$RangAdh][$i] eq "3"
                        && $COLOPTI eq 1
                        && $COLAUDI eq 0 )
                    {
                        $EnteteColGar[$RangAdh][$i] = "(3)";
                        $DOC->append( 'Rappel', {'RappelTROISOPTI'} );
                    }
                    elsif ( $EnteteColGar[$RangAdh][$i] eq "4" ) {
                        $EnteteColGar[$RangAdh][$i] = "(4)";
                        $DOC->append( 'Rappel', {'RappelQUATRE'} );
                    }
                    elsif ($EnteteColGar[$RangAdh][$i] eq "6"
                        && $COLHOCF eq 1 )
                    {    ### La demande de DE-LORENZO Virginie le 08/11/2016
                        $EnteteColGar[$RangAdh][$i] = "(6)";
                        $RappelHOCF = 1;
                    }

                    my $suffix = chr( ord('A') + $i );
                    if (
                        (
                            $DATE_STAGE_GAR[$RangAdh][$i] ne
                            $DATE_DEBUT_VALIDITE
                        )
                        && ( $DATE_STAGE_GAR[$RangAdh][$i] ne "" )
                      )
                    {
    # ON ÉDITE LA DATE DE DÉBUT DE VALIDITÉ
    # UNIQUEMENT LORSQU'ELLE DIFFÈRE LA DATE DE DÉBUT DE VALIDITÉ DE LA CARTE
    # warn "Présence de période de stage pour $Garantie[$RangAdh][$NSECU]\n";

                        $DATE_STAGE_GAR[$RangAdh][$i] =~
                          /^\d{2}\/(\d{2})\/\d{2}(\d{2})/;
                        my $date = "$1$2";
                        $detail->append( 'Stage' . $suffix, $date );

                       # print OUT "<DEBUG>Stage $DATE_STAGE_GAR[$RangAdh][$i]";
                    }

                    $detail->append( 'Col' . $suffix,
                        $EnteteColGar[$RangAdh][$i] );
                }

                if ( $RappelHOCF eq 1 && $Rappel3 eq 1 ) {
                    $DOC->append( 'Rappel', {'RappelCINQ'} );
                }

                $detail->append('DetLN');
                $count++;
            }
        }

        $DOC->append( 'DetAdh', $detail );

        if ( $user eq 'AC004' ) {

            # warn "INFO : dump GED - ENDDOC ". Dumper($GED) ."\n";
            oe_print_to_output_file($GED);
            $GED->reset();
        }

        $DOC->append('EDITDOC');
        $DOC->append('ENDDOC');

        # warn "INFO : dump DOC ". Dumper($DOC) ."\n";
        oe_print_to_output_file($DOC);
        $DOC->reset();

        # TRACKING
        $TRK->track( 'Doc', 1, $NUMADH, $CDE_ER_SECTION, $ACTEGEST, $NUMABA,
            "du " . $DATE_DEBUT_VALIDITE . " au " . $DATE_FIN_VALIDITE );

        initDoc();
    }
}

sub user_E7_build_addr {
    my ( $DOC, $VALS ) = @_;
    my $Y;
    ##### NORME 38
# APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR E7) :
# SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS E7-ZZZZ-NOMCDXLIBACH
# cas 1 : E7-ZZZZ-INDCDX =1  + LIBLOCLIBLOC !C Y	: E7-ZZZZ-BOIPOSLIBLDT + b + E7-ZZZZ-LIBLOCLIBLOC
# cas 2 : E7-ZZZZ-INDCDX =1  + LIBLOCLIBLOC C  Y	: E7-ZZZZ-BOIPOSLIBLDT
# cas 3 : E7-ZZZZ-INDCDX =0 				: E7-ZZZZ-BOIPOSLIBLDT

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $VALS->{'LIBLOCL'} );
    oe_clean_addr_line( $VALS->{'NOMCDXL'} );
    $Y = $VALS->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $VALS->{'INDCDXx'} == 0 ) {

        # CAS 3
        $VALS->{'LIBLOCL'} = '';
    }
    elsif ( $VALS->{'INDCDXx'} == 1 && index( $VALS->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $VALS->{'LIBLOCL'} = '';
    }

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
# my @DADDR = ();
# push(@DADDR, "$VALS->{'PENOCOD'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"); # $VALS->{'PENOCOD'}
    push( @DADDR, $VALS->{'PNTREMx'} );
    push( @DADDR, $VALS->{'CPLADRx'} );
    push( @DADDR, "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );
    push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
    push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

    $VALS->{'PELICCO'} =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @DADDR, $VALS->{'PELICCO'} );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'DADDR', @DADDR );

    # AU TOUR DE L'ADRESSE ÉMETTEUR.

# cas 1 : E7-ZZZZ-INDCDX-2 =1  + LIBLOCLIBLOC-2 !C Y	: E7-ZZZZ-BOIPOSLIBLDT + b + E7-ZZZZ-LIBLOCLIBLOC
# cas 2 : E7-ZZZZ-INDCDX-2 =1  + LIBLOCLIBLOC-2 C  Y	: E7-ZZZZ-BOIPOSLIBLDT
# cas 3 : E7-ZZZZ-INDCDX-2 =0 				: E7-ZZZZ-BOIPOSLIBLDT

    oe_clean_addr_line( $VALS->{'LIBL022'} );
    oe_clean_addr_line( $VALS->{'NOMC024'} );

    $Y = $VALS->{'NOMC024'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $VALS->{'INDC020'} == 0 ) {

        # CAS 3
        $VALS->{'LIBL022'} = '';
    }
    elsif ( $VALS->{'INDC020'} == 1 && index( $VALS->{'LIBL022'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $VALS->{'LIBL022'} = '';
    }

    # @EADDR = ();
    push( @EADDR,
            "-"
          . " $VALS->{'PENO025'} " . "-"
          . " $VALS->{'CPLA029'} " . "-"
          . " $VALS->{'PNTR028'}" );
    push( @EADDR,
"$VALS->{'PEAD016'} $VALS->{'PEAD017'} $VALS->{'LIBV019'} $VALS->{'BOIP021'}"
    );
    push( @EADDR, "$VALS->{'CODC023'} $VALS->{'LIBL022'} $VALS->{'NOMC024'}" );
    @EADDR = user_cleanup(@EADDR);
    @EADDR = user_cleanup_addr(@EADDR);
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub suppEspace {
    my $chaine = shift;
    $chaine =~ s/^\s+//g;    #supprime les espaces en début de chaine
    $chaine =~ s/\s+$//g;    #en fin de chaine
    $chaine =~ s/\s+/ /g;    #remplace des séries d'espaces par un seul
    $chaine =~ s{ (\x20 \- \x20) (?: \- \x20)+}{$1}gsx;    #supprimer " - "
    return $chaine;
}

exit main(@ARGV);
