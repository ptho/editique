#!/usr/bin/perl  #-d:ptkdb
use oEdtk::Main 0.5074;
use File::Basename;
use oEUser::Lib;
use oEdtk::RecordParser;
use oEdtk::Config qw(config_read);
use oEdtk::Spool;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Data::Dumper;
use oEUser::Descriptor::C1;
use oEUser::Descriptor::C2;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::F1;
use oEUser::Descriptor::F2;
use oEUser::Descriptor::G1;
use oEUser::Descriptor::G2;
use oEUser::Descriptor::G4;
use oEUser::Descriptor::G6;
use oEUser::Descriptor::GG;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::H2;
use oEUser::Descriptor::H3;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::H5;
use oEUser::Descriptor::H6;
use oEUser::Descriptor::H7;
use oEUser::Descriptor::H8;
use oEUser::Descriptor::H9;
use oEUser::Descriptor::HO;
use oEUser::Descriptor::HP;
use oEUser::Descriptor::HV;
use oEUser::Descriptor::HW;
use oEUser::Descriptor::J1;
use oEUser::Descriptor::M1;
use oEUser::Descriptor::OA;
use oEUser::Descriptor::Z0;
use oEUser::Descriptor::Z1;
use oEUser::Descriptor::FE;

use strict;
use warnings;
use feature "switch";
use utf8;

#VARIABLES TRACKING ET DECLENCHEMENT DOCUMENT
my $TRK;
my $GED;
my $CFG;
my $DATAGAR;
my $DOC;
my $CTT;
my $EMET;
my $AGENCE;
my $NOM;

#VARIABLES NOTICES
my $DECIS;
my $NUMCCO;
my $CODGRP;
my $DATSO;
my $NUMGRP;
my $ANNEE;
my $NUMPRD;
my $CODPRT;
my $TAUX;
my $TOTTAUX = 0;
my $PRODOFFR;
my $NBREFPROD = 0;
my $REFPROD;
my $TYPECOUR = 0;
my ( $NUMABA, $NUMCTR );
my @DADDR;
my @TABGAR;
my %TABGAR    = ();
my %H_TAB_PRD = ();
my @TRI;
my @REFPROD;
my %TABTAUX     = ();
my %LISTTAUXGAR = ();

#VARIABLES RECEPISSES
my $PRINT = 0;
my $CPTOA;
my $NICP;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $CFG = config_read('ENVDESC');
    user_corp_file_prefixe( $argv[0], '_' );

    my $user = user_get_aneto_user( $argv[0] );

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'xIDEMET', 'xNOMDEST', 'NUMABA', 'NUMCTR', 'xVILDEST' ]
    );

    # trame keys => ['IDDEST', 'AGENCE', 'NOMDEST', 'COMPTE', 'INDUS']
    #	my $opt_ged="";
    # SI ON SOUHAITE METTRE LE DOCUMENT EN GED systématiquement,
    # IL SUFFIT DE PASSER L'OPTION --edms + oe_compo_link COMME CI-DESSOUS

    my $opt_ged = '--edms';

    oe_new_job('--index');

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    # avec initialisation propre au document

    my $E1 = oEUser::Descriptor::E1->get();

    #$E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    # 	$E2->debug();
    $E2->bind_all_c7();
    my $F1 = oEUser::Descriptor::F1->get();
    $F1->bind_all_c7();
    my $F2 = oEUser::Descriptor::F2->get();
    $F2->bind_all_c7();
    my $C1 = oEUser::Descriptor::C1->get();
    $C1->bind_all_c7();
    my $C2 = oEUser::Descriptor::C2->get();
    $C2->bind_all_c7();
    my $G1 = oEUser::Descriptor::G1->get();

    #$G1->debug();
    $G1->bind_all_c7();
    my $G2 = oEUser::Descriptor::G2->get();

    #$G2->debug();
    $G2->bind_all_c7();
    my $G4 = oEUser::Descriptor::G4->get();
    $G4->bind_all_c7();
    my $G6 = oEUser::Descriptor::G6->get();
    $G6->bind_all_c7();
    my $GG = oEUser::Descriptor::GG->get();

    #$GG->debug();
    $GG->bind_all_c7();
    my $H1 = oEUser::Descriptor::H1->get();

    #$H1->debug();
    $H1->bind_all_c7();
    my $H2 = oEUser::Descriptor::H2->get();

    #$H2->bind_all_c7();
    my $H3 = oEUser::Descriptor::H3->get();

    #$H3->debug();
    $H3->bind_all_c7();
    my $H4 = oEUser::Descriptor::H4->get();

    #$H4->debug();
    $H4->bind_all_c7();
    my $H5 = oEUser::Descriptor::H5->get();
    $H5->bind_all_c7();
    my $H6 = oEUser::Descriptor::H6->get();
    $H6->bind_all_c7();
    my $H7 = oEUser::Descriptor::H7->get();
    $H7->bind_all_c7();
    my $H8 = oEUser::Descriptor::H8->get();
    $H8->bind_all_c7();
    my $H9 = oEUser::Descriptor::H9->get();

    #$H9->debug();
    $H9->bind_all_c7();
    my $HO = oEUser::Descriptor::HO->get();
    $HO->bind_all_c7();
    my $HP = oEUser::Descriptor::HP->get();
    $HP->bind_all_c7();
    my $HV = oEUser::Descriptor::HV->get();
    $HV->bind_all_c7();
############################################
    my $HW = oEUser::Descriptor::HW->get();

    #$HW->debug();
    $HW->bind_all_c7();
############################################
    my $J1 = oEUser::Descriptor::J1->get();
    $J1->bind_all_c7();
    my $M1 = oEUser::Descriptor::M1->get();
    $M1->bind_all_c7();
    my $OA = oEUser::Descriptor::OA->get();

    #$OA->debug();
    $OA->bind_all_c7();
    my $Z0 = oEUser::Descriptor::Z0->get();
    $Z0->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();
    $Z1->bind_all_c7();
    my $FE = oEUser::Descriptor::FE->get();
    $FE->bind_all_c7();
    $DOC     = oEdtk::TexDoc->new();
    $GED     = oEdtk::TexDoc->new();
    $DATAGAR = oEdtk::TexDoc->new();
    $NICP    = oEdtk::TexDoc->new();

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'F1' => $F1,
        'F2' => $F2,
        'C1' => $C1,
        'C2' => $C2,
        'G1' => $G1,
        'G2' => $G2,
        'G4' => $G4,
        'G6' => $G6,
        'GG' => $GG,
        'H1' => $H1,
        'H2' => $H2,
        'H3' => $H3,
        'H4' => $H4,
        'H5' => $H5,
        'H6' => $H6,
        'H7' => $H7,
        'H8' => $H8,
        'H9' => $H9,
        'HO' => $HO,
        'HP' => $HP,
        'HV' => $HV,
        'HW' => $HW,
        'J1' => $J1,
        'M1' => $M1,
        'OA' => $OA,
        'Z0' => $Z0,
        'Z1' => $Z1,
        'FE' => $FE
    );
    my $CPT = 0;
    $DOC->append( 'xTYPDOC', 'NICP' );    ### Type de document

    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'E1' ) {
            if ( $. > 3 ) {               # this is a hack... à revoir

                # 			warn "INFO PASSAGE BOUCLE emitDoc $.>3 \n";
                prepDATAGAR();
                emitDoc();
            }
            $CPT++;

            #			warn "CPT ".$CPT."\n";
            $DOC->append( 'POURCENT', 0 )
              ;    # valeur par defaut pour le flux incomplets... à revoir

            # POUR LES NOUVELLES OFFRES (MISE EN FORME)
            $DOC->append( 'NOM',    $vals->{'PENOLIB'} );
            $DOC->append( 'PRENOM', $vals->{'PENOPRN'} );
            $DOC->append( 'AGENCE', $vals->{'ODERCOD'} );

            @DADDR = ();
            push( @DADDR, "$vals->{'PNTREMx'} $vals->{'CPLADRx'}" );
            push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
            );
            push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );
            if ( $vals->{'INDCDXx'} == 1 ) {
                push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );
            }
            else {
                push( @DADDR, $vals->{'BOIPOSL'} );
            }
            @DADDR = user_cleanup_addr(@DADDR);

            $DOC->append_table( 'DADDR',  @DADDR );
            $DOC->append_table( 'xADRLN', @DADDR );

            $AGENCE = $vals->{'ODERCOD'};
            $DOC->append( 'NOMDEST', $vals->{'PENOLIB'} );
            $NOM = $vals->{'PENOLIB'} . " " . $vals->{'PENOPRN'};

            #GESTION DES INFOS D'ARCHIVAGE
            $GED->append( 'xNOMDEST', $NOM );
            $GED->append( 'xCPDEST',  oe_iso_country() . $vals->{'CODCDXC'} );
            $GED->append( 'xVILDEST', $vals->{'NOMCDXL'} );

        }
        if ( $id eq 'E2' ) {

            warn "INFO $id : dump " . Dumper($vals) . "\n";
            $NUMCCO = $vals->{'ACCCNUM'};
            $NUMCTR = $vals->{'ACH1NUM'};
            $CTT    = substr $NUMCCO, 7, 3;
            $CODGRP = $vals->{'ACGACOD'};
            $NUMGRP = $NUMCCO . "/" . $CODGRP;
            $DATSO  = convertDate( $vals->{'ACOPDDE'} );
            $EMET   = oe_uc_sans_accents( $vals->{'PENO084'} );
            $EMET   = $vals->{'PENO083'} . " " . $vals->{'PENO084'};

            #INDENTIFICATION DE L'ENTITE JURIDIQUE
            #POUR LA GESTION DU FOND DE PAGE (LOGO ET PIED DE PAGE)
            #ET DE L'APPEL DE CERTAINS BLOCS DE TEXTE SPECIFIQUE A CPLTR

            if ( $CTT eq 'HOS' ) {
                $DOC->append( 'ACTIFART', {'ARTHOS'} );
                $DOC->append( 'TYPSOUSCR', 'souscripteur' );

            }
            else {
                $DOC->append( 'ACTIFART', {'ARTMNT'} );
                $DOC->append( 'TYPSOUSCR', 'souscripteur' );
            }
            $GED->append( 'xIDEMET', $EMET );
            $GED->append( 'xIDDEST', $NUMGRP );
            $DOC->append( 'DATSO',   $DATSO );
            $DOC->append( 'NUMGRP',  $NUMGRP );
            $DOC->append( 'NUMCTR',  $NUMCTR );
        }
        if ( $id eq 'F1' ) {
        }
        if ( $id eq 'F2' ) {
        }
        if ( $id eq 'H1' ) {
        }
        if ( $id eq 'H2' ) {
        }

        if ( $id eq 'H3' ) {
        }
        if ( $id eq 'H4' ) {

            # 			warn "INFO H4: dump ". Dumper($vals) ." \n";
            #la date de début de validité de la garantie
            my $DATVALGR = convertDate( $vals->{'ACH4DDE'} );

            #            warn "INFO CTT NEXT = $CTT \n";
            #Seulement les lignes H4 qui n'ont pas de date de fin de validité
            if ( ( $vals->{'ACH4010'} ) eq '' ) {

#Seulement les PLU avec la date de début de validité de la garantie est égale à la date d'effet de l'acte
                if ( $CTT eq 'HOS' ) {
                    if ( $DATSO eq $DATVALGR ) {
                        prepH4($vals);
                        prepParaf($vals);
                    }
                }
                else
                { #Pour les autres courriers: pas de contrainte sur la date de validité
                    prepH4($vals);
                    prepParaf($vals);
                }
            }
        }
        if ( $id eq 'H5' ) {
        }
        if ( $id eq 'H6' ) {
        }
        if ( $id eq 'H7' ) {
        }
        if ( $id eq 'H8' ) {

            #            warn "INFO H8: dump ". Dumper($vals) ." \n";
            my $PRD = $vals->{'ACH4002'};
            my $GAR = $vals->{'ACH4COD'};
            foreach my $k (%H_TAB_PRD) {
                if ( exists( $H_TAB_PRD{$PRD}{$GAR} ) )
                {    #&& $GAR eq 'PB1F1-DC03'){
                    $H_TAB_PRD{$PRD}{$GAR}{'DELSTA'} =
                      'Délais de stage : ' . $vals->{'ACSTDUR'} . ' mois';
                    $H_TAB_PRD{$PRD}{$GAR}{'DTFINSTA'} = $vals->{'ACSTDFE'};
                }
                else {
                    $H_TAB_PRD{$PRD}{$GAR}{'DELSTA'} =
                      'TEST DE PASSAGE DE BOUCLE DELAIS DE STAGE';
                    $H_TAB_PRD{$PRD}{$GAR}{'DTFINSTA'} =
                      'TEST DE PASSAGE DE BOUCLE DATE DE FIN DE STAGE';
                }
            }
        }

        if ( $id eq 'H9' ) {
        }
        if ( $id eq 'HO' ) {
        }
        if ( $id eq 'HP' ) {
        }
        if ( $id eq 'HV' ) {
        }

        if ( $id eq 'HW' && $CTT eq 'HOS' ) {
            my $GAR = $vals->{'ACH4COD'};
            my $TYPEHW = substr $vals->{'PRASCOD'}, 0, 4;

            #warn "INFO HW: $TYPEHW dump ". Dumper($vals) ." \n";
            if ( $TYPEHW eq "TAUX" ) {
                prepHW($vals);
            }
        }
        if ( $id eq 'J1' ) {
        }
        if ( $id eq 'M1' ) {
        }
        if ( $id eq 'C1' ) {
        }
        if ( $id eq 'C2' ) {
        }

        if ( $id eq 'G1' ) {
            $DOC->append( 'SOUSCRIP', $vals->{'PENOLIB'} );
        }
        if ( $id eq 'G2' ) {
            $PRINT = 1;
        }
        if ( $id eq 'G4' ) {
        }
        if ( $id eq 'G6' ) {
        }
        if ( $id eq 'GG' ) {

  #warn "INFO GG: dump ". Dumper($vals) ." \n";
  # pour le PLU, chaque taux est affiché en face de la garantie correspondante.
            if ( $CTT ne 'HOS' ) {

                #RECUPERATION DU TAUX MISE AU FORMAT ET CUMUL DE CE DERNIER
                $TAUX = $vals->{'ACFE011'};
                $TAUX *= 100;
                $TOTTAUX = $TAUX + $TOTTAUX;
                $DOC->append( 'POURCENT', $TOTTAUX || 0 )
                  ; # pour les numprint, toujours prévoir une valeur numérique
            }
        }
        if ( $id eq 'OA' ) {
            $CPTOA++;

            #warn "INFO: TAG ".'CTT'.": $CTT \n";
            #warn "INFO OA: dump ". Dumper($vals) ." \n";
            my $FLAGNICP = ( $vals->{'FLAGxxx'} );
            my $PRDGAR = ( $vals->{'PRODUIT'} ) . ( $vals->{'GARANTI'} );

#Lorsqu'il existe plusieurs record OA avec des références de notice différentes,
#il faut filtrer les références et ne conserver que celle(s) qui sont actives,
#c'est-à-dire dont les record H4 correspondants (mêmes codes Offre/Produit/GT) n'ont pas de date de fin de validité.
            if ( $CTT eq 'HOS' ) {
                if (   exists( $TABGAR{$PRDGAR} )
                    && !( grep { $_ eq $vals->{'REFEREN'} } @REFPROD )
                    && $NBREFPROD < 2 )
                {
                    # 						warn "TEST PASSAGE DE CONDITION \n";
                    push( @REFPROD, $vals->{'REFEREN'} );

                    # 						warn "INFO : Ref produit = @REFPROD \n";
                    $NBREFPROD++;

       #Si le flux contient deux références actives différentes,
       #les deux doivent être affichées, séparées par la mention «  et  ».
                    if ( $NBREFPROD == 1 ) {
                        $REFPROD = $vals->{'REFEREN'};
                    }
                    if ( $NBREFPROD == 2 ) {
                        $REFPROD .= " et " . $vals->{'REFEREN'};
                    }
                    $DOC->append( 'REFPRD', $REFPROD );

                    # 						warn "INFO : Ref produit = $REFPROD \n";
                    if ( $FLAGNICP == 1 ) {
                        $DOC->append( "TESTEFE", 1 );

                        # ATTENTION + LA NORME DE NOMMAGE DES FICHIERS !!!
                        $NICP->append( "encartPDF",
                            $vals->{'REFEREN'} . ".pdf" );
                        $NICP->append("CALLENCART");
                    }
                }
            }
            else {
                #warn "TEST PASSAGE DE BOUCLE 4 \n";
                $DOC->append( 'REFPRD', '' );
            }

        }
        else {
            $DOC->append( "TESTEFE", 0 );
        }
        if ( $id eq 'Z0' ) {
        }
        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $vals->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
        }
        if ( $id eq 'FE' ) {
            $DECIS = $vals->{'DECISTO'}
              ; # PRESENCE DE RESERVES MEDICALES A L'ADHESION: 0=NON / 1=NON / 2=OUI / VIDE = NON
            given ($DECIS) {
                when ( '0' || '1' ) { $DOC->append( 'RESMED', 'Non' ); }
                when ('2') { $DOC->append( 'RESMED', 'Oui' ); }
                default    { $DOC->append( 'RESMED', 'Non' ); }
            }
        }
    }
    foreach my $k (%H_TAB_PRD) {
        given ($k) {
            when ('PIDS3-MNT3') {
                if ( exists $H_TAB_PRD{'PIDS3-MNT3'}{'PB1F1-DC02'} ) {
                    if ( $DECIS eq '2' ) {
                        $H_TAB_PRD{'PIDS3-MNT3'}{'PB1F1-DC02'}{'DECIS'} =
                          'Réserve médicale : OUI';
                    }
                    else {
                        $H_TAB_PRD{'PIDS3-MNT3'}{'PB1F1-DC02'}{'DECIS'} =
                          'Réserve médicale : NON';
                    }
                }
            }
        }
    }

    #            warn "INFO H_TAB_PRD: dump ". Dumper(\%H_TAB_PRD) ." \n";
    prepDATAGAR();

    #	 die;
    emitDoc();
    oe_compo_link($opt_ged);
    return 0;
}

sub prepH4() {
    my ($vals) = @_;
    $ANNEE  = substr $vals->{'ACH4DDE'}, 2, 2;
    $NUMPRD = "NICP-$ANNEE";
    $CODPRT = ( $vals->{'ACH4006'} );
    my $PRD = $vals->{'ACH4006'};
    my $GAR = $vals->{'ACH4COD'};

    # MISE EN FORME DES LIBELLES LONGS PRODUITS SUR LE DOCUMENT
    $vals->{'PRGA003'} = user_prev_get_option( $vals->{'PRGA003'} );
    $vals->{'PRGA003'} =~ s/-.*//o;
    $vals->{'PRGA003'} =~ s/^\s+//;
    $vals->{'PRGA003'} =~
      s/C\'TER Indemnité Journalière/Indemnité Journalière/o;
    $vals->{'PRGA003'} =~ s/C\'TER Invalidité/Invalidité/o;
    $vals->{'PRGA003'} =~ s/plafonnée.*//o;
    $vals->{'PRGA003'} =~ s/proportionnelle.*//o;
    $vals->{'PRGA003'} =~ s/plafonnée proportionnelle.*//o;
    $vals->{'PRGA003'} =~ s/IJ/Indemnités Journalières/;
    $vals->{'PRGA003'} =~ s/RJ/Régime Indemnitaire IJ/;
    $vals->{'PRGA003'} =~ s/RV/Régime Indemnitaire Invalidité/;
    $vals->{'PRGA003'} =~ s/IV/Invalidité/;
    $vals->{'PRGA003'} =~ s/PR/Perte de retraite/;
    $vals->{'PRGA003'} =~ s/DC00/Décès/;
    $vals->{'PRGA003'} =~ s/DC01/Capital Décès, PTIA/;
    $vals->{'PRGA003'} =~ s/DC02/Décès accident/;
    $vals->{'PRGA003'} =~ s/DC03/Décès accident de circulation/;
    $vals->{'PRGA003'} =~ s/RE/Rente éducation/;
    $vals->{'PRGA003'} =~ s/RC/Rente de conjoint/;
    $vals->{'PRGA003'} =~ s/GO/Frais d\'obsèques/;
    $vals->{'PRGA003'} =~ s/OB/Frais d\'obsèques/;
    $vals->{'PRGA003'} =~ s/IF/Indemnité de Feu/;
    $vals->{'PRGA003'} =~ s/IR/Indemnité de Responsabilité/;
    $vals->{'PRGA003'} =~ s/IS/Indemnité de spécialité/;
    $vals->{'PRGA003'} =~ s/IT/Primes IFTS/;
    $vals->{'PRGA003'} =~ s/PC/Invalidité retraite/;
    my $size = $vals->{'PRGA003'};

    if ( $CTT eq 'HOS' )
    {  #	en cas de HOS on rempli une table de hachage dont la clé est $PRODOFFR
        $H_TAB_PRD{$PRD}{$GAR}{'LIBLGAR'} = "-" . " " . $size;
        $TABGAR{ ( $vals->{'ACH4006'} ) . ( $vals->{'ACH4COD'} ) } =
          "-" . " " . $size;
        if ( $GAR eq 'PB1F1-DC02' ) {
            $H_TAB_PRD{$PRD}{$GAR}{'QM'} = 'OUI';
        }
    }
    else {    #pour les autres courriers
        $PRODOFFR = ( ( $vals->{'ACH4006'} ) . ( $vals->{'ACH4COD'} ) );
        push( @TRI, "-" . " " . $size )
          ;    # on insère un -  devant le libéllé long
        @TABGAR = ( sort { $a cmp $b } @TRI )
          ; # on trie le tableau des garanties par ordre alphabétique croissant
    }
    return 0;

}

sub emitDoc() {
    if ( $CPTOA == 0 ) {
        die "ERROR: pas d'enregistrement OA dans le flux $ARGV[-1]\n";
    }
    if ( !( $PRINT == 1 ) ) {
        warn "INFO: ERROR CTT= $CTT et CODPRT= $CODPRT ==> PAS D EDITION \n";
        $TRK->track( 'Warn', 1, '', '', '', '' );
    }

    #TRACKING
    $TRK->track( 'Doc', 1, $AGENCE, $NOM, $NUMGRP,, );

    #SORTIE DES ELEMENT DE GED ET DU DOCUMENT
    if ( $CTT eq 'HOS' ) { $DOC->append( 'DATAGAR', $DATAGAR ) }
    $DOC->append( 'NICP', $NICP );
    $DOC->append('DEBUT');

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    resetVAR();
    return 0;
}

sub convertDate {

    #MISE EN FORME DES DATE JJ/MM/AAAA
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub prepParaf() {

#PREPARATION DES DONNEES ET APPEL DES PARAGRAPHES EN FONCTION DES ENTITES JURIDIQUES
    my ($vals) = @_;
    $DOC->append( 'NUMPRD', $NUMPRD );
    $DOC->append( 'CTT',    $CTT );

    #en cas de HOS
    if ( $CTT eq 'HOS' ) {
        @TABGAR   = sort values %TABGAR;
        $TYPECOUR = 1
          ; # 1 pour les courriers HOS, si non 0 pour les autres types de courriers
    }
    $DOC->append_table( 'GARANTIE', @TABGAR );
    $DOC->append( 'TYPECOUR', $TYPECOUR );
    return 0;
}

sub prepHW() {

    my ($vals) = @_;
    my $PRDGAR = ( $vals->{'ACH4COD'} ) . ( $vals->{'ACH4002'} );
    my $PRD    = $vals->{'ACH4COD'};
    my $GAR    = $vals->{'ACH4002'};

    #la date de validité
    my $DATVAL = convertDate( $vals->{'ACCODDE'} );
    foreach my $k (%H_TAB_PRD) {
        if ( exists( $H_TAB_PRD{$PRD}{$GAR} ) && $DATSO eq $DATVAL ) {
            given ($PRD) {
                when ('PIDS3-MNT3') {
                    $H_TAB_PRD{$PRD}{$GAR}{'TAUX'} =
                      "Taux : " . $vals->{'ACJIMAH'} . ' %';
                }
                when ('PLU-MNT-PS') {
                    $H_TAB_PRD{$PRD}{$GAR}{'TAUX'} =
                      "Montant : " . $vals->{'ACJIMAH'} . ' €';
                }
                default { print STDERR "CODE PRODUIT NON SUPPORTE $PRD \n"; }
            }
        }
    }
    if ( exists( $TABGAR{$PRDGAR} ) && $DATSO eq $DATVAL ) {

        #warn "INFO HW: dump ". Dumper($vals) ." \n";

        $TABTAUX{$PRDGAR} = ( $vals->{'ACJIMAH'} );

        #		warn "INFO TABTAUX: dump ". Dumper(\%TABTAUX) ." \n";
        #		warn "INFO TABGAR: dump ". Dumper(\%TABGAR) ." \n";
        # 		warn "INFO TABTAUX: dump \n";
        # 		foreach my $k (keys %TABTAUX) {
        # 			print "Clef=$k \nValeur=$TABTAUX{$k}\n";
        # 		}
    }
    return 0;
}

sub prepDATAGAR() {

    # 	warn "INFO CTT ". $CTT ." \n";
    if ( $CTT eq 'HOS' ) {
        my $nbr = scalar(@TABGAR);
        $DOC->append( "NBGAR", $nbr );
        foreach my $produits (%H_TAB_PRD) {
            foreach my $garanti ( keys %{ $H_TAB_PRD{$produits} } ) {
                $DATAGAR->append( "GARANTIE",
                    $H_TAB_PRD{$produits}{$garanti}{'LIBLGAR'} );
                if ( exists( $H_TAB_PRD{$produits}{$garanti}{'TAUX'} ) ) {
                    $DATAGAR->append( "TAUX",
                        $H_TAB_PRD{$produits}{$garanti}{'TAUX'} );
                }
                if ( exists( $H_TAB_PRD{$produits}{$garanti}{'MONTANT'} ) ) {
                    $DATAGAR->append( "MONTANT",
                        $H_TAB_PRD{$produits}{$garanti}{'MONTANT'} );
                }
                if ( exists( $H_TAB_PRD{$produits}{$garanti}{'DELSTA'} ) ) {
                    $DATAGAR->append( "MONTANT",
                        $H_TAB_PRD{$produits}{$garanti}{'DELSTA'} );
                }
                if ( exists( $H_TAB_PRD{$produits}{$garanti}{'DECIS'} ) ) {
                    $DATAGAR->append( "INFOGTI",
                        $H_TAB_PRD{$produits}{$garanti}{'DECIS'} );
                }
                if ( exists( $H_TAB_PRD{$produits}{$garanti}{'DELSTA'} ) ) {
                    $DATAGAR->append( "INFOGTI",
                        $H_TAB_PRD{$produits}{$garanti}{'DELSTA'} );
                }
                $DATAGAR->append("EditLigneGar");
            }
        }
    }
    return 0;
}

sub resetVAR() {
    $DOC->reset();
    $GED->reset();
    $DATAGAR->reset();
    $NICP->reset();
    $PRINT       = 0;
    $NBREFPROD   = 0;
    $TOTTAUX     = 0;
    %TABGAR      = ();
    %TABTAUX     = ();
    %LISTTAUXGAR = ();
    @REFPROD     = ();
    @TABGAR      = ();
    @TRI         = ();
    $TYPECOUR    = ();
    $NUMCCO      = ();
    $CODGRP      = ();
    $DATSO       = ();
    $NUMGRP      = ();
    $ANNEE       = ();
    $NUMPRD      = ();
    $CODPRT      = ();
    $TAUX        = ();
    $PRODOFFR    = ();
    $REFPROD     = ();
    $CPTOA       = ();
    $CTT         = ();
    $EMET        = ();
    $AGENCE      = ();
    $NOM         = ();
    $DOC         = oEdtk::TexDoc->new();
    $GED         = oEdtk::TexDoc->new();
    $DATAGAR     = oEdtk::TexDoc->new();
    $NICP        = oEdtk::TexDoc->new();
    return 0;
}

exit main(@ARGV);
