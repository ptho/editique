#!  /usr/bin/perl
use warnings;
use strict;
use feature qw( say switch );
use XML::LibXML::Reader (":types");
use Encode qw( encode );
use Data::Dumper qw( Dumper );
use Cwd qw ( abs_path );
use oEdtk::Main qw( oe_app_usage oe_corporation_set oe_new_job oe_compo_link );
use oEUser::Lib qw( user_corp_file_prefixe user_get_aneto_user );
use oEdtk::Config qw( config_read );
use oEdtk::LaTeX
  qw( make_index latex_escape_string tag_args translate_unit concat_string );
use oEdtk::Tracking;
use Carp qw( croak );

use Readonly;
Readonly my $EXPECTED_DTD_PUBLIC_ID => '-//EDTK//DTD PHX-JUSTCALC V1.0//EN';

Readonly my $DOCUMENT_TYPE => 'FCPR';

Readonly my $NODE_CARTOUCHE => "cartouche";

Readonly my $NODE_ROW           => "row";
Readonly my $NODE_CARTOUCHE_ROW => "crow";
Readonly my $NODE_DETAILS_ROW   => "drow";
Readonly my $NODE_PERIODES_ROW  => "prow";

Readonly my $NODE_DETAILS              => "details";
Readonly my $NODE_PERIODES             => "periodes";
Readonly my $NODE_BILAN_COMPARATIF     => "bilan-comparatif";
Readonly my $NODE_BILAN_COMPARATIF_ROW => "bcrow";
Readonly my $NODE_SOUS_BILAN           => "sous-bilan";
Readonly my $NODE_SOUS_BILAN_ROW       => "sbrow";
Readonly my $NODE_BILAN                => "bilan";

Readonly my $ATTR_LIBELLE                 => "libelle";
Readonly my $ATTR_VALEUR                  => "valeur";
Readonly my $ATTR_VALEUR_UNITE            => "valeur_unite";
Readonly my $ATTR_DATE                    => "date";
Readonly my $ATTR_DATE_DEBUT              => "date_debut";
Readonly my $ATTR_DATE_FIN                => "date_fin";
Readonly my $ATTR_TITLE                   => "title";
Readonly my $ATTR_BASE                    => "base";
Readonly my $ATTR_TAUX                    => "taux";
Readonly my $ATTR_TAUX_UNITE              => "taux_unite";
Readonly my $ATTR_TYPE                    => "type";
Readonly my $ATTR_CLASS                   => "class";
Readonly my $ATTR_VALEUR_DATE_DEBUT       => "valeur_date_debut";
Readonly my $ATTR_VALEUR_DATE_FIN         => "valeur_date_fin";
Readonly my $ATTR_JOURS_TOTAL             => "jours_total";
Readonly my $ATTR_JOURS_INDEMNISES        => "jours_indemnises";
Readonly my $ATTR_TRAITEMENT_THEORIQUE    => "traitement_theorique";
Readonly my $ATTR_IJSS                    => "ijss";
Readonly my $ATTR_MONTANT_INDEMNISE       => "montant_indemnise";
Readonly my $ATTR_MONTANT_INDEMNISE_UNITE => "montant_indemnise_unite";
Readonly my $ATTR_COMMENT                 => "comment";

Readonly my $DEBUG           => 1;
Readonly my @TRACKING_FIELDS => qw(AGENCE NOM COLLECTIVITE);

Readonly my $CLASS_SOUS_BILAN_TOTAL => "total";

# Index
# TODO refacto with Infinite.pm (in a Constants module or something)
Readonly my $INDEX_CODE_POSTAL_DEST => 'xCPDEST';
Readonly my $INDEX_VILLE_DEST       => 'xVILDEST';
Readonly my $INDEX_NOM_DEST         => 'xNOMDEST';

# TODO refacto with PRECON-EC010.pl (in a Constants module or something)
Readonly my $INDEX_CLEF_GED      => 'xCLEGED';
Readonly my $INDEX_ADRESSE_LISTE => 'xADRLN';
Readonly my $INDEX_ID_MNT_NUMABA => 'xIDDEST';
Readonly my $INDEX_ID_EMETTEUR   => 'xIDEMET';
Readonly my $INDEX_TYPE_DOC      => 'xTYPDOC';

my ($TRK);

sub init {
    my ( $flowFile, $trackingJob ) = @_;
    unless ( defined $trackingJob ) {
        $trackingJob = abs_path($flowFile);
        warn "Using trackingJob='$trackingJob'";
    }
    user_corp_file_prefixe( $flowFile, '_' );
    my $user = user_get_aneto_user($flowFile);
    $TRK = track_begin( $trackingJob, $user );
    return;
}

sub track_begin {
    my ( $trackingJob, $user ) = @_;
    return oEdtk::Tracking->new(
        $trackingJob,
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => \@TRACKING_FIELDS
    );
}

sub track {
    my ($l) = @_;
    warn "TRACKING: " . join( "\x20", @{$l} ) if $DEBUG;
    return $TRK->track( 'D', 1, @{$l} );    # "D" as in "Doc"
}

sub track_end {
    return $TRK->track( 'W', 1, '', '', '', '', 'Fin de traitement' )
      ;                                     # "W" as in "Warning"
}

sub latex_arguments {
    my ( $tag, @args ) = @_;
    my @res = ();
    foreach my $arg (@args) {
        croak "undef" unless defined $arg;
        my $value =
          ( ( ref $arg ) eq "ARRAY" )
          ? tag_args( @{$arg} )
          : latex_escape_string($arg);
        push @res, "{$value}";
    }
    return join( "", "\\$tag", @res );
}

sub out {
    my ($str) = @_;
    my $bytes = encode( 'iso-8859-1', $str, Encode::FB_CROAK );

    # utf8::encode $str;
    # print STDERR $str;
    return oEdtk::Main::oe_print_to_output_file( sprintf '%s%s', $bytes, "\n" );

    # return print oEdtk::Main::OUT $bytes, "\n";
}

sub parse_cartouche_row {
    my ($doc) = @_;
    my %h =
      ( map { $_ => ( exists $doc->{$_} ? $doc->{$_} : "" ) }
          ( $ATTR_LIBELLE, $ATTR_VALEUR, $ATTR_VALEUR_UNITE, $ATTR_DATE ) );
    return \%h;
}

sub parse_details_row {
    my ($doc) = @_;
    my %h = (
        map { $_ => ( exists $doc->{$_} ? $doc->{$_} : "" ) } (
            $ATTR_LIBELLE,    $ATTR_BASE,   $ATTR_TAUX,
            $ATTR_TAUX_UNITE, $ATTR_VALEUR, $ATTR_TYPE,
            $ATTR_CLASS,      $ATTR_VALEUR_UNITE
        )
    );
    return \%h;
}

sub parse_periodes_row {
    my ($doc) = @_;
    my %h = (
        map { $_ => ( exists $doc->{$_} ? $doc->{$_} : "" ) } (
            $ATTR_DATE_DEBUT,           $ATTR_DATE_FIN,
            $ATTR_JOURS_TOTAL,          $ATTR_JOURS_INDEMNISES,
            $ATTR_TRAITEMENT_THEORIQUE, $ATTR_IJSS,
            $ATTR_MONTANT_INDEMNISE,    $ATTR_MONTANT_INDEMNISE_UNITE
        )
    );
    return \%h;
}

sub parse_sous_bilan_row {
    my ($doc) = @_;
    my %h = (
        map { $_ => ( exists $doc->{$_} ? $doc->{$_} : "" ) } (
            $ATTR_LIBELLE,      $ATTR_VALEUR,
            $ATTR_VALEUR_UNITE, $ATTR_VALEUR_DATE_DEBUT,
            $ATTR_VALEUR_DATE_FIN
        )
    );
    croak "Too many values in " . Dumper( \%h )
      if ( $h{$ATTR_VALEUR_DATE_DEBUT} =~ m{\S}sx )
      && ( $h{$ATTR_VALEUR_DATE_FIN} =~ m{\S}sx )
      && ( $h{$ATTR_VALEUR} =~ m{\S}sx );
    if (   ( $h{$ATTR_VALEUR_DATE_DEBUT} =~ m{\S}sx )
        && ( $h{$ATTR_VALEUR_DATE_FIN} =~ m{\S}sx ) )
    {
        $h{$ATTR_VALEUR} = join( " ",
            "du", format_date( $h{$ATTR_VALEUR_DATE_DEBUT} ),
            "au", format_date( $h{$ATTR_VALEUR_DATE_FIN} ) );
    }
    elsif ( $h{$ATTR_VALEUR_DATE_DEBUT} =~ m{\S}sx ) {
        $h{$ATTR_VALEUR} = join( " ",
            "\x{e0} partir du",
            format_date( $h{$ATTR_VALEUR_DATE_DEBUT} ) );
    }
    elsif ( $h{$ATTR_VALEUR_DATE_FIN} =~ m{\S}sx ) {
        $h{$ATTR_VALEUR} =
          join( " ", "jusqu'au", format_date( $h{$ATTR_VALEUR_DATE_FIN} ) );
    }
    delete $h{$ATTR_VALEUR_DATE_DEBUT};
    delete $h{$ATTR_VALEUR_DATE_FIN};
    return \%h;
}

sub treat_sous_bilan {
    my ($doc) = @_;
    my $comment =
      exists $doc->{$ATTR_COMMENT} ? trim( $doc->{$ATTR_COMMENT} ) : '';
    my $has_comment = ( $comment =~ m{\S}sx );
    my $tag_name =
      $has_comment ? 'insertSousBilanWithComment' : 'insertSousBilan';
    out( latex_arguments( $tag_name, $doc->{$ATTR_TITLE} ) . '{' );
    my @rows = ();
    foreach my $node ( list_children_nodes( $doc, $NODE_SOUS_BILAN_ROW ) ) {
        push @rows, parse_sous_bilan_row($node);
    }
    my $libelle_len = longest_string( \@rows, $ATTR_LIBELLE );
    my $valeur_len = longest_number( \@rows, $ATTR_VALEUR );
    my $biggest_unit =
      biggest_unit( \@rows, $ATTR_VALEUR_UNITE );    # alignment in table
    foreach my $h (@rows) {
        my ( $libelle, $valeur, $valeur_unite ) =
          map { exists $h->{$_} ? $h->{$_} : "" }
          ( $ATTR_LIBELLE, $ATTR_VALEUR, $ATTR_VALEUR_UNITE );
        my $tag =
          ( exists $h->{$ATTR_CLASS}
              && $h->{$ATTR_CLASS} eq $CLASS_SOUS_BILAN_TOTAL )
          ? 'Sbil'
          : 'sbil';
        out(
            latex_arguments(
                $tag,
                pad_escape( $libelle, $libelle_len ),
                (
                    is_number($valeur)
                    ? format_number( $valeur, $valeur_len, $valeur_unite,
                        $biggest_unit )
                    : $valeur
                ),
            )
        );
    }
    out( '}'
          . ( $has_comment ? '{' . latex_escape_string($comment) . '}' : '' ) );
    out( latex_arguments('bigskip') );
}

sub treat_bilan_comparatif {    # XXX refacto?
    my ($doc) = @_;
    out( latex_arguments( 'insertSousSousBilan', $doc->{$ATTR_TITLE} ) . '{' );
    my @rows = ();
    foreach my $node ( list_children_nodes( $doc, $NODE_BILAN_COMPARATIF_ROW ) )
    {
        push @rows, parse_sous_bilan_row($node);
    }
    my $libelle_len = longest_string( \@rows, $ATTR_LIBELLE );
    my $valeur_len = longest_number( \@rows, $ATTR_VALEUR );
    my $biggest_unit =
      biggest_unit( \@rows, $ATTR_VALEUR_UNITE );    # alignment in table
    foreach my $h (@rows) {
        my ( $libelle, $valeur, $valeur_unite ) =
          map { exists $h->{$_} ? $h->{$_} : "" }
          ( $ATTR_LIBELLE, $ATTR_VALEUR, $ATTR_VALEUR_UNITE );
        my $tag =
          ( exists $h->{$ATTR_CLASS}
              && $h->{$ATTR_CLASS} eq $CLASS_SOUS_BILAN_TOTAL )
          ? 'Ssbil'
          : 'ssbil';
        out(
            latex_arguments(
                $tag,
                pad_escape( $libelle, $libelle_len ),
                (
                    is_number($valeur)
                    ? format_number( $valeur, $valeur_len, $valeur_unite,
                        $biggest_unit )
                    : $valeur
                ),
            )
        );
    }
    out('}');
}

sub list_children_nodes {
    my ( $doc, @names ) = @_;
    my %names = map { $_ => 1 } @names;
    my @res = ();
    foreach my $node ( $doc->childNodes() ) {
        my $type = $node->nodeType();
        given ($type) {
            when ( $type eq XML_READER_TYPE_TEXT ) {
                croak "Non-empty text in node: " . $node
                  if $node->to_literal =~ m{\S}sx;
            }
            when ( $type eq XML_READER_TYPE_ELEMENT ) {
                my $name = $node->nodeName();
                croak "Unexpected node name '$name'. Authorized: "
                  . join( ' ', @names )
                  unless exists $names{$name};
                push @res, $node;
            }
            when ( $type eq XML_READER_TYPE_COMMENT ) { }    # ignore
            default { croak "Unexpected node type '$type'" }
        }
    }
    return @res;
}

sub max {
    my (@l) = @_;
    my $res = $l[0];
    foreach my $item (@l) {
        $res = $item if $item > $res;
    }
    return $res;
}

sub pad {
    my ( $str, $pad_len ) = @_;
    my $len = ( length $str );
    return $str . ( " " x ( $pad_len - $len ) );
}

sub pad_escape {
    my ( $str, $pad_len ) = @_;
    my $Str = latex_escape_string($str);
    my $len = ( length $Str );
    return $str
      . ( " " x ( $pad_len - $len ) )
      ;    # will be escaped later in latex_arguments
}

sub pad_left {
    my ( $str, $len ) = @_;
    return ( " " x ( $len - ( length $str ) ) ) . $str;
}

sub is_number {
    my ($str) = @_;
    return $str =~ m{^\-? [0-9]*\.[0-9]+ $}sx || $str =~ m{^\-? [0-9]+ $}sx;
}

# returns an array ref which will be treated with tag_args by latex_arguments
sub format_number {
    my ( $val, $biggest_len, $unit, $biggest_unit ) = @_;
    my $biggest_unit_len =
      defined $biggest_unit ? length translate_unit($biggest_unit) : 0;
    my $extra_len =
      $biggest_unit_len ? 10 : 7;  # length of \npu{}{}{} / \nn{}{} respectively
    return "\x20" x ( $biggest_len + $biggest_unit_len + $extra_len )
      if $val eq '';
    my $sign = ( $val =~ m{^\s*\-}sx ) ? -1 : 1;
    my $is_int =
      ( $val =~ m{\.\d}sx )
      ? 0
      : 1;    # fractional numbers are supposed to have exactly two decimals
    if ($biggest_unit_len) {    # number / phantom / unit version
        my $pu =
          ( length $unit )
          ? ''
          : [ \&translate_unit, $biggest_unit ];    # phantom unit
        if ($is_int) {
            my $tag =
              ( $sign == 1 )
              ? 'rpu'
              : 'RPU';    #     integer positive : negative -- with unit
            return [
                $tag,
                pad_left( $val, $biggest_len - 3 ),
                [ \&concat_string,  ',00', $pu ],
                [ \&translate_unit, $unit ]
            ];
        }
        else {
            my $tag =
              ( $sign == 1 )
              ? 'npu'
              : 'NPU';    # fractionary positive : negative -- with unit
            return [
                $tag, pad_left( $val, $biggest_len ),
                $pu, [ \&translate_unit, $unit ]
            ];
        }
    }
    else {                # number / phantom version
        if ($is_int) {
            my $tag =
              ( $sign == 1 )
              ? 'rn'
              : 'RN';     #     integer positive : negative -- without unit
            return [
                $tag, pad_left( $val, $biggest_len + $biggest_unit_len - 3 ),
                ',00'
            ];
        }
        else {
            my $tag =
              ( $sign == 1 )
              ? 'nn'
              : 'NN';     # fractionary positive : negative -- without unit
            return [ $tag, pad_left( $val, $biggest_len + $biggest_unit_len ),
                '' ];
        }
    }
}

sub format_date {
    my ( $str, $len ) = @_;
    return pad( $str, $len ) if defined $len;

    # TODO guess / normalize / program-format?
    return pad( $str, 10 );
}

sub complete_number {
    my ($val) = @_;
    if ( $val =~ m{^\-? [0-9]+$}sx ) {    # integer
        $val .= '.00';    # every column should have space for decimal part
    }
    else {
        $val =~
          s{^(\-? [0-9]+) ([^.0-9]) $}{$1,00$2}sx;  # integer followed by a unit
    }
    return $val;
}

sub longest_string {
    my ( $l, $field, $flag_is_number ) = @_;
    $flag_is_number = 0 unless defined $flag_is_number;
    my $res = 0;
    foreach my $h ( @{$l} ) {
        if ( exists $h->{$field} ) {
            if ($flag_is_number) {
                my $val = complete_number( $h->{$field} );
                $res = max( $res, length latex_escape_string($val) );
            }
            else {
                $res = max( $res, length latex_escape_string( $h->{$field} ) );
            }
        }
    }
    return $res;
}

sub longest_number {
    my ( $l, $field ) = @_;
    my $res = 0;
    foreach my $h ( @{$l} ) {
        $res = max( $res, length latex_escape_string( $h->{$field} ) )
          if exists $h->{$field} && is_number( $h->{$field} );
    }
    return $res;
}

sub biggest_unit {
    my ( $l, $field ) = @_;
    my ( $res, $max ) = ( '', 0 );
    foreach my $h ( @{$l} ) {
        if (   ( exists $h->{$field} )
            && ( length( translate_unit( $h->{$field} ) ) > $max ) )
        {
            ( $res, $max ) =
              ( $h->{$field}, length translate_unit( $h->{$field} ) );
        }
    }
    return $res;
}

sub treat_cartouche {
    my ($doc) = @_;
    out( latex_arguments( 'insertCartouche', $doc->{$ATTR_TITLE} ) . '{' );
    my @rows = ();
    foreach my $node ( list_children_nodes( $doc, $NODE_CARTOUCHE_ROW ) ) {
        push @rows, parse_cartouche_row($node);
    }
    my $biggest_unit =
      biggest_unit( \@rows, $ATTR_VALEUR_UNITE );    # alignment in table
    my $libelle_len = longest_string( \@rows, $ATTR_LIBELLE )
      ;    # for aesthetic alignment in LaTeX code
    my $valeur_len = longest_string( \@rows, $ATTR_VALEUR, 1 );
    my $date_len = longest_string( \@rows, $ATTR_DATE );
    foreach my $h (@rows) {
        my ( $libelle, $valeur, $unite, $date ) =
          map { exists $h->{$_} ? $h->{$_} : "" }
          ( $ATTR_LIBELLE, $ATTR_VALEUR, $ATTR_VALEUR_UNITE, $ATTR_DATE );
        out(
            latex_arguments(
                "lcar",
                pad_escape( $libelle, $libelle_len ),
                format_number( $valeur, $valeur_len, $unite, $biggest_unit ),
                format_date( $date, $date_len ),
            )
        );
    }
    out('}');
    out( latex_arguments('bigskip') );
}

sub trim {
    my ($str) = @_;
    $str =~ s{\s+}{\x20}gsx;
    $str =~ s{^\s+}{}sx;
    $str =~ s{\s+$}{}sx;
    return $str;
}

sub treat_bilan {
    my ($doc) = @_;
    my ( $date_debut, $date_fin, $valeur, $valeur_unite, $comment ) = (
        map { ( exists $doc->{$_} ? $doc->{$_} : "" ) } (
            $ATTR_DATE_DEBUT,   $ATTR_DATE_FIN, $ATTR_VALEUR,
            $ATTR_VALEUR_UNITE, $ATTR_COMMENT
        )
    );
    $comment = trim($comment);
    my $has_comment = ( $comment =~ m{\S}sx );
    my $tag_name = $has_comment ? 'insertBilanWithComment' : 'insertBilan';
    my @libelles =
      ( "Soit pour la p\x{e9}riode", "un montant total \x{e0} r\x{e9}gler de" );
    my $max_len = max( map { length $_ } @libelles );
    out( latex_arguments( $tag_name, $doc->{$ATTR_TITLE} ) . '{' );
    out(
        latex_arguments(
            'lbil',
            pad_escape( $libelles[0], $max_len ),
            join( "\x20",
                format_date($date_debut),
                "au", format_date($date_fin) ),
        )
    );
    out(
        latex_arguments(
            'lbil',
            pad_escape( $libelles[1], $max_len ),
            format_number( $valeur, 0, $valeur_unite, $valeur_unite ),
        )
    );
    out( '}'
          . ( $has_comment ? '{' . latex_escape_string($comment) . '}' : '' ) );
}

sub output_details_rows {
    my ($rows) = @_;
    my $biggest_unit =
      biggest_unit( $rows, $ATTR_TAUX_UNITE );    # alignment in table
    my $biggest_v_unit = biggest_unit( $rows, $ATTR_VALEUR_UNITE );
    my $libelle_len = longest_string( $rows, $ATTR_LIBELLE )
      ;    # for aesthetic alignment in LaTeX code
    my $base_len   = longest_string( $rows, $ATTR_BASE,   1 );
    my $taux_len   = longest_string( $rows, $ATTR_TAUX,   1 );
    my $valeur_len = longest_string( $rows, $ATTR_VALEUR, 1 );
    foreach my $h ( @{$rows} ) {
        my ( $libelle, $base, $taux, $taux_unite, $valeur, $valeur_unite,
            $class, $type )
          = map { exists $h->{$_} ? $h->{$_} : "" } (
            $ATTR_LIBELLE, $ATTR_BASE, $ATTR_TAUX, $ATTR_TAUX_UNITE,
            $ATTR_VALEUR, $ATTR_VALEUR_UNITE, $ATTR_CLASS, $ATTR_TYPE
          );
        my $tag =
            ( $class eq "" )               ? "ldet"
          : ( $class eq "inner_subtotal" ) ? "idet"
          : ( $class eq "subtitle" )       ? "sdet"
          : ( $class eq "final_subtotal" ) ? "tdet"
          : ( $class eq "total" )          ? "Tdet"
          :                                  croak "unknown type '$class'";
        out(
            latex_arguments(
                $tag,
                pad_escape( $libelle, $libelle_len ),
                format_number( $base, $base_len, '',          '' ),
                format_number( $taux, $taux_len, $taux_unite, $biggest_unit ),
                format_number(
                    $valeur, $valeur_len, $valeur_unite, $biggest_v_unit
                ),
            )
        );
    }
}

sub output_periodes_rows {
    my ($rows) = @_;
    my $biggest_unit =
      biggest_unit( $rows, $ATTR_MONTANT_INDEMNISE_UNITE ); # alignment in table
    my $date_debut_len       = longest_string( $rows, $ATTR_DATE_DEBUT );
    my $date_fin_len         = longest_string( $rows, $ATTR_DATE_FIN );
    my $jours_total_len      = longest_string( $rows, $ATTR_JOURS_TOTAL );
    my $jours_indemnises_len = longest_string( $rows, $ATTR_JOURS_INDEMNISES );
    my $traitement_theorique_len =
      longest_string( $rows, $ATTR_TRAITEMENT_THEORIQUE );
    my $ijss_len = longest_string( $rows, $ATTR_IJSS );
    my $montant_indemnise_len =
      longest_string( $rows, $ATTR_MONTANT_INDEMNISE );

    foreach my $h ( @{$rows} ) {
        my (
            $date_debut,           $date_fin,
            $jours_total,          $jours_indemnises,
            $traitement_theorique, $ijss,
            $montant_indemnise,    $montant_indemnise_unite,
            $class
          )
          = map { exists $h->{$_} ? $h->{$_} : "" } (
            $ATTR_DATE_DEBUT,           $ATTR_DATE_FIN,
            $ATTR_JOURS_TOTAL,          $ATTR_JOURS_INDEMNISES,
            $ATTR_TRAITEMENT_THEORIQUE, $ATTR_IJSS,
            $ATTR_MONTANT_INDEMNISE,    $ATTR_MONTANT_INDEMNISE_UNITE,
            $ATTR_CLASS
          );
        my $tag =
            ( $class eq "" )      ? "lper"
          : ( $class eq "total" ) ? "Tper"
          :                         croak "unknown type '$class'";
        out(
            latex_arguments(
                $tag,
                format_date($date_debut),
                format_date($date_fin),
                pad( $jours_total,      $jours_total_len ),
                pad( $jours_indemnises, $jours_indemnises_len ),
                format_number(
                    $traitement_theorique, $traitement_theorique_len
                ),
                format_number( $ijss, $ijss_len ),
                format_number(
                    $montant_indemnise,       $montant_indemnise_len,
                    $montant_indemnise_unite, $biggest_unit
                ),
            )
        );
    }
}

sub treat_details {
    my ($doc) = @_;
    out( latex_arguments( 'insertDetails', $doc->{$ATTR_TITLE} ) . '{' );
    my @rows;
    foreach my $node ( list_children_nodes( $doc, $NODE_DETAILS_ROW ) ) {
        push @rows, parse_details_row($node);
    }
    output_details_rows( \@rows );
    out('}');
}

sub children_rows {
    my ( $doc, $child_row ) = @_;
    my @res = ();
    foreach my $node ( list_children_nodes( $doc, $child_row ) ) {
        my $name = $node->nodeName();
        given ($name) {
            when ( $name eq $child_row ) { push @res, $node; }
            default { croak "Unexpected node name '$name'"; }
        }
    }
    return @res;
}

sub treat_periodes {
    my ($doc) = @_;
    out( latex_arguments('insertPeriod') . '{' );
    my @periodes =
      map { parse_periodes_row($_) } children_rows( $doc, $NODE_PERIODES_ROW );
    output_periodes_rows( \@periodes );
    out('}');
}

sub treat_node {
    my ($node) = @_;
    my $name = $node->nodeName();
    given ($name) {
        when ( $name eq $NODE_CARTOUCHE ) { treat_cartouche($node); }
        when ( $name eq $NODE_DETAILS )   { treat_details($node); }
        when ( $name eq $NODE_PERIODES )  { treat_periodes($node); }
        when ( $name eq $NODE_BILAN_COMPARATIF ) {
            treat_bilan_comparatif($node);
        }
        when ( $name eq $NODE_SOUS_BILAN ) { treat_sous_bilan($node); }
        when ( $name eq $NODE_BILAN )      { treat_bilan($node); }
        default { croak "Unknown node name '$name'"; }
    }
}

sub treat_document {
    my ($doc) = @_;
    my @l = map { $doc->{$_} } qw(agence nom collectivite entite);
    track( \@l );
    my $index = make_index(
        {},
        $INDEX_TYPE_DOC         => $DOCUMENT_TYPE,
        $INDEX_CODE_POSTAL_DEST => '',
        $INDEX_VILLE_DEST       => '',
        $INDEX_NOM_DEST         => '',
        "$INDEX_CLEF_GED#roman" => \@l,
        $INDEX_ID_MNT_NUMABA    => '',
        $INDEX_ID_EMETTEUR      => '',
        $INDEX_ADRESSE_LISTE    => '',
    );
    out( $index . latex_arguments( "fiche", @l ) );
    foreach my $node (
        list_children_nodes(
            $doc,             $NODE_CARTOUCHE,        $NODE_DETAILS,
            $NODE_SOUS_BILAN, $NODE_BILAN_COMPARATIF, $NODE_BILAN,
            $NODE_PERIODES
        )
      )
    {
        treat_node($node);
    }
    return;
}

sub treat_flow {
    my ($dom) = @_;
    my $root = $dom->documentElement();
    treat_document($root);
    return 0;
}

sub load_xml_catalogs {
    my ($parser) = @_;
    my $cfg = config_read('MAIL');
    if (   ( exists $cfg->{'EDTK_XML_CATALOG_FILES'} )
        && ( defined $cfg->{'EDTK_XML_CATALOG_FILES'} ) )
    {
        my $catalog_files = $cfg->{'EDTK_XML_CATALOG_FILES'};
        warn "Using XML_CATALOG_FILES=$catalog_files";
        $parser->load_catalog($_) foreach split m/:/, $catalog_files;
    }
    else {
        warn "No XML_CATALOG_FILES found";
    }
    return;
}

sub validate_and_parse_xml {
    my ($file) = @_;
    my $parser = XML::LibXML->new( validation => 1 );
    load_xml_catalogs($parser);
    my $dom      = $parser->parse_file($file);
    my $publicId = $dom->externalSubset->publicId;
    croak "DTD public id '$publicId' is not expected"
      . " public id '$EXPECTED_DTD_PUBLIC_ID'"
      if $publicId ne $EXPECTED_DTD_PUBLIC_ID;
    die "File '$file' does not validate against its DTD: "
      . XML::LibXML->get_last_error()
      unless $dom->is_valid;
    warn "File '$file' validates against its DTD";
    return $dom;
}

sub main {
    my ($flowFile) = @_;
    oe_app_usage() unless defined $flowFile;
    my $dom = validate_and_parse_xml($flowFile);
    init($flowFile);
    oe_new_job('--index');
    treat_flow($dom);
    track_end();
    oe_compo_link();
    return 0;
}

exit main(@ARGV);
