#! /bin/sh
/bin/rm -vf .gitignore
for file in [a-z]*.xml; do
  for EJ in MNT MUTACITE CPLTR; do
    EJ_file=${EJ}_$file;
    /bin/cp -pv $file $EJ_file;
    /usr/bin/perl -i -pe "s/ENTITE_JURIDIQUE/$EJ/" $EJ_file;
    /bin/echo $EJ_file >> .gitignore
  done
done
