#! /bin/sh
for file in [a-z]*.xml; do
  for EJ in MNT MUTACITE CPLTR; do
    EJ_file=${EJ}_$file;
    /bin/rm -fv $EJ_file;
  done
done
