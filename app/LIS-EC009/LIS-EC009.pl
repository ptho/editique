#!/usr/bin/perl
#Mise à niveau pour EFE 9.
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEdtk::RecordParser;
use oEdtk::Tracking;
use oEdtk::libXls;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::EX;
use oEUser::Descriptor::OF;
use oEUser::Descriptor::OG;
use oEUser::Descriptor::I4;
use oEUser::Lib;
use strict;
use warnings;
use Date::Calc qw(Today);
use Time::Local;

my %hRECORDS;
my $TRK;
my $VALS;
my $DOC;
my $CPT = 0;
my $FREQUENCE;
my $ER;
my $DATEREJET;
my $DOMAINE;
my $NUMIND;
my $ADH;
my $MOTIF;
my $DATEFUT;
my $MODEREG;
my $CODREJ;
my $LIBELLE;
my $NBREJET;
my $DATEREG;
my $MONTANT;
my $ENTITE;
my $ANO;
my $TOP;
my $MTT;
my $BANQUE;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] ) || 'none';

    # INITIALISATION DU TRAKING
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMIND', 'AGENCE' ]
    );

    # oe_new_job(); # est remplacé par :
    oe_open_fi_IN( $argv[0] );

    # MAPPING DES RECORDS
    map_records();

    init_Doc();

    # INITIALISATION DU PROCESS PARSER (ASSIGNE LES CLEFS AUX RECORDS)
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $hRECORDS{ENTETE},
        'EX'     => $hRECORDS{EX},
        'OF'     => $hRECORDS{OF},
        'OG'     => $hRECORDS{OG},
        'I4'     => $hRECORDS{I4},
    );

    init_XLS();

    my ( $id, $lastid );

    while ( ( $id, $VALS ) = $p->next() ) {

        if ( $id eq 'EX' ) {
            process_EX();
        }
        elsif ( $id eq 'OF' ) {
            process_OF();
        }
        elsif ( $id eq 'OG' ) {
            process_OG();
        }
        elsif ( $id eq 'I4' ) {
            process_I4();
        }

    }
    return 0;
}

sub map_records() {
    $hRECORDS{ENTETE} = oEUser::Descriptor::ENTETE->get();
    $hRECORDS{ENTETE}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{EX} = oEUser::Descriptor::EX->get();
    $hRECORDS{EX}->bind_all_c7()
      ;    # bind les champs de données sur 7 caractères
    $hRECORDS{OF} = oEUser::Descriptor::OF->get();
    $hRECORDS{OF}->bind_all_c7()
      ;    # bind les champs de données sur 7 caractères
    $hRECORDS{OG} = oEUser::Descriptor::OG->get();
    $hRECORDS{OG}->bind_all_c7()
      ;    # bind les champs de données sur 7 caractères
    $hRECORDS{I4} = oEUser::Descriptor::I4->get();
    $hRECORDS{I4}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur

    return 1;
}

sub init_Doc() {

    my $FREQUENCE;
    my $ER;
    my $DATEREJET;
    my $DOMAINE;
    my $NUMIND;
    my $ADH;
    my $MOTIF;
    my $DATEFUT;
    my $CODREJ;
    my $LIBELLE;
    my $NBREJET;
    my $DATEREG;
    my $MONTANT;
    my $ENTITE;
    my $ANO;

}

sub process_EX () {

    $DOMAINE = $VALS->{'ODCGCOD'};
    if ( $DOMAINE =~ /MNT-RC|MUT-RC|MNT-RO/ ) {
        $DOMAINE = "Santé";
        if ( $VALS->{'ODCGCOD'} =~ /MNT/ ) {
            $ENTITE = "M.N.T";
        }
        else {
            $ENTITE = "MUTACITE";
        }
    }
    else {
        $DOMAINE = "Prévoyance";
        if ( $VALS->{'ODCGCOD'} =~ /MNT/ ) {
            $ENTITE = "M.N.T";
        }
        else {
            $ENTITE = "MUTACITE";
        }
    }

    $ER = $VALS->{'ODERCOD'};
    $ADH =
      $VALS->{'PENOCOD'} . " " . $VALS->{'PENOLIB'} . " " . $VALS->{'PENOPRN'};
    $CODREJ    = $VALS->{'CMOTIFx'};
    $LIBELLE   = $VALS->{'LIBMOTx'};
    $DATEREJET = convertDate( $VALS->{'DATINVx'} );
    $DATEREG   = convertDate( $VALS->{'DATVALx'} );
}

sub process_OF () {

    #warn "INFO : dump OF ". Dumper($VALS) ."\n";
    $NUMIND    = $VALS->{'NUMINDx'};
    $ANO       = $VALS->{'ANOxxxx'};
    $FREQUENCE = $VALS->{'FREQUEN'};

    # 	$NBREJET = $VALS->{'NBREJET'};
    $MODEREG = $VALS->{'MODEREG'};
    if ( $MODEREG eq 'X' ) {
        $MODEREG = 'PX';
    }
    elsif ( $MODEREG eq 'P' ) {
        $MODEREG = 'PB';
    }
    else {
        $MODEREG = $VALS->{'MODEREG'};
    }

    $DATEFUT = convertDate( $VALS->{'DATExxx'} );
    oe_num_sign_x( $VALS->{'MONTANT'}, 2 );
    $MONTANT = $VALS->{'MONTANT'};

}

sub process_OG () {

    #	warn "INFO : dump OG ". Dumper($VALS) ."\n";
    $BANQUE = $VALS->{'BANQUEx'};

    if ( $ANO eq "ANO" ) {
        $TOP = 1;
    }
    else {
        $TOP = 0;
        edit_ligne_XLS();
    }

    $TRK->track( 'Reject', 1, $NUMIND, $ER, );

}

sub process_I4 () {
    $NBREJET = $VALS->{'REJET'};
}

sub edit_ligne_XLS {

    prod_Xls_Insert_Val( $VALS->{'BANQUEx'} );
    prod_Xls_Insert_Val($FREQUENCE);
    prod_Xls_Insert_Val($ER);
    prod_Xls_Insert_Val($DATEREJET);
    prod_Xls_Insert_Val($DOMAINE);
    prod_Xls_Insert_Val($NUMIND);
    prod_Xls_Insert_Val($ADH);
    prod_Xls_Insert_Val($CODREJ);
    prod_Xls_Insert_Val($LIBELLE);
    prod_Xls_Insert_Val($NBREJET);
    prod_Xls_Insert_Val($DATEREG);
    prod_Xls_Insert_Val($MONTANT);
    prod_Xls_Insert_Val($MODEREG);

    prod_Xls_Edit_Ligne();

    #warn "INFO MONTANT: $MONTANT \n";
}

sub init_XLS {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large , Ac7 = Alpha Center Wrap... , NR7 = Numérique Right...  )
    prod_Xls_Col_Init(
        'AC10', 'AC10', 'AC10', 'AC10', 'AC10', 'AC10', 'AL35', 'AC8',
        'AL20', 'AC10', 'AC10', 'NR10', 'AC10'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init( $ENTITE, "REJETS DE PRELEVEMENTS BANCAIRES" )
      ;    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("Banque");
    prod_Xls_Insert_Val("Fréquence réglement");
    prod_Xls_Insert_Val("ER");
    prod_Xls_Insert_Val("Date du rejet");
    prod_Xls_Insert_Val("Domaine métier");
    prod_Xls_Insert_Val("N° de contrat individuel");
    prod_Xls_Insert_Val("Adhérent");
    prod_Xls_Insert_Val("Motif");
    prod_Xls_Insert_Val("Libellé");
    prod_Xls_Insert_Val("N° de rejet");
    prod_Xls_Insert_Val("Date de règlement");
    prod_Xls_Insert_Val("Montant");
    prod_Xls_Insert_Val("Mode de règlement");

    #EDITION DE LA TETE DE COLONNE
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );
    1;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

exit main(@ARGV);
