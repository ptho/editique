#!/usr/bin/perl
use utf8;
use strict;
use warnings;

use oEdtk::Main 0.50;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Lib;

use oEUser::XML::Generic;
use Data::Dumper;
use POSIX qw(strftime);

my $file_error = './PHX-MOBANQ_error.txt';
my $date_log = strftime "%d/%m/%Y %H:%M:%S", localtime;

my ( $TRK, $MATRICULE, $AGENCECP, $NOM );
my @EADDR;
my @DADDR;
my $PROFCOD;
my $MODREGL;
my @adr_CTC;
my $DOC = oEdtk::TexDoc->new();
my ( $ID_MNT, $NUM_CONTRAT, $NOMPRENOM );

sub recup_data {
    print 'sub recup_data' . "\n";

    # sleep (1);

    # save original settings. You could also use lexical typeglobs.
    *OLD_STDERR = *STDERR;

    # reassign STDOUT, STDERR
    open my $log_fh, '>>', $file_error;

    # print $log_fh '*' x 80;
    print $log_fh "\n$date_log	";
    *STDERR = $log_fh;

    my $data = xml_open( \*IN );

    # done, restore STDOUT/STDERR
    *STDERR = *OLD_STDERR;

    print 'Chargement des donnees du flux termine' . "\n";
    return $data;
}

sub edit_courrier {
    my ( $data, $type_doc ) = @_;

    print 'sub edit_courrier' . " $type_doc \n";

    # sleep (5);

    # my $cpt =0;
    # my $cpt_E1 = 0;
    my $bool_edit =
      0;    # passe a 1 si au moins un champs d'enregistrement est nul

    #dump du flux dans un fichier test
    # open (my $desc,'>','./flux_test_xml.txt');
    # $Data::Dumper::Indent = 3; # pretty print with array indices
    # print $desc Dumper($data) ."\n";
    # close $desc;

    # print $data->{'NB_SUCCESS'} ."\n";

    # tableau de hashages
    foreach my $indice ( 0 .. $#{ $data->{'MOB'} } ) {

        # print "INDICE MOB $indice\n";

        # DANS LE FLUX PHOENIX ON A QUE DE LA PREVOYANCE
        $DOC->append( 'TYPPRESTA', 'PREVOYANCE' );

        # ON EMET UN DOCUMENT SSI ON A AU MOINS UN AGENT

        # $elt ref de hash
        foreach my $elt ( $data->{'MOB'}->[$indice] ) {

            # CHAMPS EN COMMUN POUR UN OU PLUSIEURS AGENTS
            $DOC->append( 'PERIREF', $elt->{'ACCOUNTSWITCHID'} )
              ;    # reference dossier mobilite bancaire
            $DOC->append( 'PERIDAT', convertDate( $data->{'DATE'} ) )
              ;    # date de signature du mandat mobilite bancaire
            $DOC->append( 'CODBICx', $elt->{'SOURCE'}->{'BIC'} )
              ;    # BIC ancienne domiciliation
            $DOC->append( 'CODB061', $elt->{'DESTINATION'}->{'BIC'} )
              ;    # BIC nouvelle domiciliation

            my $IBANORI = $elt->{'SOURCE'}->{'IBAN'};
            $IBANORI = &format_IBAN( $IBANORI, $elt->{'ACCOUNTSWITCHID'} );

            my $IBANCIB = $elt->{'DESTINATION'}->{'IBAN'};
            $IBANCIB = &format_IBAN( $IBANCIB, $elt->{'ACCOUNTSWITCHID'} );

            $DOC->append( 'IBANORI', $IBANORI );   # IBAN ancienne domiciliation
            $DOC->append( 'IBANCIB', $IBANCIB );   # IBAN nouvelle domiciliation

            # TEST IF IS A REFERENCE TO A HASH (UN SEUL AGENT)
            if ( ref( $elt->{'AGENT'} ) eq "HASH" ) {

               # print $elt->{'AGENT'}->{'NAME'} . "\n";
               # print "WAY TYPE " . $elt->{'AGENT'}->{'WAY_TYPE_SHORT'} . "\n";

                #####################
                # Verif champs nuls #
                #####################
                # tout nouveau champ doit etre ajoute a la liste
                $bool_edit = &gestion_erreur(
                    'PERIREF',
                    $elt->{'ACCOUNTSWITCHID'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit =
                  &gestion_erreur( 'PERIDAT', $data->{'DATE'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit );
                $bool_edit = &gestion_erreur(
                    'CODBICx',
                    $elt->{'SOURCE'}->{'BIC'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'CODB061',
                    $elt->{'DESTINATION'}->{'BIC'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'IBANORI',
                    $elt->{'SOURCE'}->{'IBAN'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'IBANCIB',
                    $elt->{'DESTINATION'}->{'IBAN'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'IBANCIB',
                    $elt->{'DESTINATION'}->{'IBAN'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'CIVILITE_SHORT',
                    $elt->{'AGENT'}->{'CIVILITE_SHORT'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'NAME',
                    $elt->{'AGENT'}->{'NAME'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'FIRST_NAME',
                    $elt->{'AGENT'}->{'FIRST_NAME'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'WAY',
                    $elt->{'AGENT'}->{'WAY'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'CP',
                    $elt->{'AGENT'}->{'CP'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );
                $bool_edit = &gestion_erreur(
                    'LOCALITE',
                    $elt->{'AGENT'}->{'LOCALITE'},
                    $elt->{'AGENT'}->{'PHX_ID'}, $bool_edit
                );

                ########################

                $DOC->append( 'NUMABA', $elt->{'AGENT'}->{'MNT_ID'} );  # ID MNT
                $DOC->append( 'ACH1NUM', $elt->{'AGENT'}->{'CONTRACT_NUM'} )
                  ;    # Numero de contrat individuel
                $DOC->append( 'LICCODC', $elt->{'AGENT'}->{'CIVILITE_LONG'} )
                  ;    #civilite long

  # si on a des prelevements MAQUETTE 2,3 ou 4
  # POUR LE MOMENT ABSENT DE PHOENIX : ECC1DAT OU ECD1DAT NE SONT PAS RECUPEREES
                if ( $elt->{'AGENT'}->{'ECC1DAT'} ) {

                    $DOC->append( 'BOOLPRELEVEMENT', 1 );
                    $DOC->append( 'ECC1DAT',
                        convertDate( $elt->{'AGENT'}->{'ECC1DAT'} ) )
                      ;    # date de la dernière échéance de prélèvement
                    if ( $elt->{'AGENT'}->{'ECD1DAT'} ) {
                        $DOC->append( 'ECD1DAT',
                            convertDate( $elt->{'AGENT'}->{'ECC1DAT'} ) )
                          ; # date de la prochaine échéance théorique de prélèvement
                    }
                    else {
                        $DOC->append( 'ECD1DAT', '[date inconnnue]' );
                    }
                }

                #pour gerer laffichage de ladresse
                &gestionAdr( $DOC, $elt->{'AGENT'} );

                $bool_edit = &emitDoc( $type_doc, $bool_edit );

            }
            else {
                # DANS LE CAS DE PLUSIEURS AGENTS (REF DE TABLEAU)
                foreach my $indice_agent ( 0 .. $#{ $elt->{'AGENT'} } ) {

# print 'INDICE AGENT ' . $indice_agent . "\n";
# print "ID DE L'AGENT " . $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'} . "\n";
# print "NOM DE L'AGENT " . $elt->{'AGENT'}->[$indice_agent]->{'NAME'} . "\n";
# print "WAY TYPE " . $elt->{'AGENT'}->[$indice_agent]->{'WAY_TYPE_SHORT'} . "\n";

                    #####################
                    # Verif champs nuls #
                    #####################
                    # tout nouveau champ doit etre ajoute a la liste
                    $bool_edit = &gestion_erreur(
                        'PERIREF',
                        $elt->{'ACCOUNTSWITCHID'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'}, $bool_edit
                    );
                    $bool_edit =
                      &gestion_erreur( 'PERIDAT', $data->{'DATE'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'},
                        $bool_edit );
                    $bool_edit = &gestion_erreur(
                        'CODBICx',
                        $elt->{'SOURCE'}->{'BIC'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'}, $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'CODB061',
                        $elt->{'DESTINATION'}->{'BIC'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'}, $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'IBANORI',
                        $elt->{'SOURCE'}->{'IBAN'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'}, $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'IBANCIB',
                        $elt->{'DESTINATION'}->{'IBAN'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'}, $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'NAME',
                        $elt->{'AGENT'}->[$indice_agent]->{'NAME'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'},
                        $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'FIRST_NAME',
                        $elt->{'AGENT'}->[$indice_agent]->{'FIRST_NAME'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'},
                        $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'WAY',
                        $elt->{'AGENT'}->[$indice_agent]->{'WAY'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'}, $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'CP',
                        $elt->{'AGENT'}->[$indice_agent]->{'CP'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'}, $bool_edit
                    );
                    $bool_edit = &gestion_erreur(
                        'LOCALITE',
                        $elt->{'AGENT'}->[$indice_agent]->{'LOCALITE'},
                        $elt->{'AGENT'}->[$indice_agent]->{'PHX_ID'},
                        $bool_edit
                    );
                    ########################

                    $DOC->append( 'NUMABA',
                        $elt->{'AGENT'}->[$indice_agent]->{'MNT_ID'} ); # ID MNT
                    $DOC->append( 'ACH1NUM',
                        $elt->{'AGENT'}->[$indice_agent]->{'CONTRACT_NUM'} )
                      ;    # Numero de contrat individuel
                    $DOC->append( 'LICCODC',
                        $elt->{'AGENT'}->[$indice_agent]->{'CIVILITE_LONG'} )
                      ;    #civilite long

  # si on a des prelevements MAQUETTE 2,3 ou 4
  # POUR LE MOMENT ABSENT DE PHOENIX : ECC1DAT OU ECD1DAT NE SONT PAS RECUPEREES
                    if ( $elt->{'AGENT'}->[$indice_agent]->{'ECC1DAT'} ) {

                        $DOC->append( 'BOOLPRELEVEMENT', 1 );
                        $DOC->append(
                            'ECC1DAT',
                            convertDate(
                                $elt->{'AGENT'}->[$indice_agent]->{'ECC1DAT'}
                            )
                        );    # date de la dernière échéance de prélèvement
                        if ( $elt->{'AGENT'}->[$indice_agent]->{'ECD1DAT'} ) {
                            $DOC->append(
                                'ECD1DAT',
                                convertDate(
                                    $elt->{'AGENT'}->[$indice_agent]
                                      ->{'ECC1DAT'}
                                )
                              )
                              ; # date de la prochaine échéance théorique de prélèvement
                        }
                        else {
                            $DOC->append( 'ECD1DAT', '[date inconnnue]' );
                        }
                    }

                    &gestionAdr( $DOC, $elt->{'AGENT'}->[$indice_agent] );

                    $bool_edit = &emitDoc( $type_doc, $bool_edit );

                }

            }

        }
    }

    # print "NOM " . $data->{'MOB'}->[0]->{'AGENT'}->{'NAME'}. "\n";

    # #DOCUMENT 1

    # si on a des virements
    #variable test
    my $virement = 'ok';

    if ($virement) {
        $DOC->append( 'BOOLVIREMENT', 1 );
    }

    # my $beneficiaires = oEdtk::TexDoc->new();
    # if ( $variables->{'beneficiaires'} ne "" ) {
    # warn "tableau";
    # if ( defined( $variables->{'beneficiaires'}->{'beneficiaire'} ) ) {
    # if (
    # ref( $variables->{'beneficiaires'}->{'beneficiaire'} ) eq
    # "ARRAY" )
    # {    ### Il y a plusieurs bénéficiaires à éditer
    # my $key;
    # foreach $key (
    # @{ $variables->{'beneficiaires'}->{'beneficiaire'} } )
    # {    ### Pour chaque bénéficiaire
    # edit_beneficiaire( $key, \$beneficiaires );
    # }
    # }
    # else {    ### Il n'y a qu'un bénéficiaire
    # edit_beneficiaire( $data->{'beneficiaires'}->{'beneficiaire'},
    # \$beneficiaires );
    # }
    # }
    # }
    # else { warn "pas de tableau"; }

    # $$doc->append( 'beneficiaires', $beneficiaires );
    # $$doc->append('finbulletin');    ### Fin du contrat

    # warn "########################### $MATRICULE \n";
    # $TRK->track( 'Doc', 1, $MATRICULE, $NOM, $AGENCECP );

    return 0;
}

sub emitDoc {
    my ( $type_doc, $bool_edit ) = @_;

    print 'sub emitDoc ' . $type_doc . " avec bool_edit = " . $bool_edit . "\n";

    # sleep (1);

    if ( $bool_edit == 0 ) {
        if ( $type_doc == 1 ) {
            $DOC->append('EditDocUn');
        }
        elsif ( $type_doc == 2 ) {
            $DOC->append('EditDocDe');
        }
        elsif ( $type_doc == 3 ) {
            $DOC->append('EditDocTr');
        }
        elsif ( $type_doc == 4 ) {
            $DOC->append('EditDocQu');
        }
        elsif ( $type_doc == 5 ) {
            $DOC->append('EditDocUn');
            $DOC->append('EditDocDe');
            $DOC->append('EditDocTr');
            $DOC->append('EditDocQu');

        }

        # $DOC->append('PIEDDEPAGE');
        $DOC->append('ENDDOC');
        oe_print_to_output_file($DOC);
    }

    # on reinitialise bool_edit = 0
    $bool_edit = 0;

    # on reinitialise les identifiants
    $ID_MNT      = '';
    $NUM_CONTRAT = '';

    # reinitialise DOC pour changement de dossier
    $DOC = oEdtk::TexDoc->new();
    return $bool_edit;
}

sub convertDate {
    my ($refVar) = @_;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub gestionAdr {
    my ( $doc, $vals ) = @_;

    # CIVILITE_SHORT
    # NAME
    # FIRST_NAME
    # COMPLEMENT_ADR
    # WAY_NUM
    # BTQ
    # WAY_TYPE_SHORT
    # WAY
    # CP
    # LOCALITE
    # DISTRIB
    # COUNTRY

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
    @DADDR = ();

    my $id_phx = "$vals->{'PHX_ID'}";
    my $nom_dest =
      "$vals->{'CIVILITE_SHORT'} $vals->{'NAME'} $vals->{'FIRST_NAME'}";
    my $num_nom_rue =
"$vals->{'WAY_NUM'} $vals->{'BTQ'} $vals->{'WAY_TYPE_SHORT'} $vals->{'WAY'}";
    my $compl_adre = "$vals->{'COMPLEMENT_ADR'}";
    my $localite   = "$vals->{'CP'} $vals->{'LOCALITE'}";
    my $distrib    = "$vals->{'DISTRIB'}";
    my $pays       = "$vals->{'COUNTRY'}";

    $nom_dest =~ s/null/ /g;
    $num_nom_rue =~ s/null/ /g;

    # $compl_adre =~ s/null/ /g;
    $localite =~ s/null/ /g;
    $distrib =~ s/null/ /g;
    $pays =~ s/null/ /g;

    push( @DADDR, uc($nom_dest) );
    push( @DADDR, uc($num_nom_rue) );
    if ( $compl_adre and $compl_adre ne 'null' ) {
        push( @DADDR, uc($compl_adre) );
    }
    push( @DADDR, uc($localite) );

    # push( @DADDR,$distrib);
    # push( @DADDR,$pays);

    $DOC->append_table( 'xADRLN', @DADDR );

    # @EADDR = ();
    # push( @EADDR, $vals->{'PENO027'} );
    # push( @EADDR, $vals->{'PENO025'} );

    return 0;
}

############################################################################################################
# Suite à la norme 38: le nom et prenom de destinataire et de d'émetteur sont restés dans le record E1
# Mais les adresses se trouvent dans E7 sont de norme 38
############################################################################################################

# sub prepE7 {
# my ( $vals, $DOC ) = @_;
# my $Y;

# # ADRESSE DESTINATAIRE.
# oe_clean_addr_line( $vals->{'LIBLOCL'} );
# oe_clean_addr_line( $vals->{'NOMCDXL'} );
# $Y = $vals->{'NOMCDXL'};
# $Y =~ s/CEDEX.*$//
# ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
# $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

# if ( $vals->{'INDCDXx'} == 0 ) {

# # CAS 3
# $vals->{'LIBLOCL'} = '';
# }
# elsif ( $vals->{'INDCDXx'} == 1 && index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

# # CAS 1
# }
# else {
# # CAS 2
# $vals->{'LIBLOCL'} = '';
# }

# push( @DADDR, $vals->{'PNTREMx'} );    #PNTREMx
# push( @DADDR, $vals->{'CPLADRx'} );
# push( @DADDR,
# "$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
# );                                     #PEADNUM, PEADBTQ, LIBVOIx
# push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );  #BOIPOSL, LIBLOCL,
# push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );  #CODCDXC, NOMCDXL
# # initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
# oe_iso_country( $vals->{'PELICCO'} );   #    LICCODP
# $vals->{'LICCODP'} =~ s/FRANCE//i;      # ON AFFICHE PAS LE PAYS POUR FRANCE
# push( @DADDR, $vals->{'LICCODP'} );     # LICCODP
# @DADDR = user_cleanup_addr(@DADDR);
# $DOC->append_table( 'xADRLN', @DADDR );

# ###################################################################
# # Adresse Émetteur
# ###################################################################

# oe_clean_addr_line( $vals->{'LIBL022'} );    #  LIBL038
# oe_clean_addr_line( $vals->{'NOMC024'} );    #  NOMC040
# $Y = $vals->{'NOMC024'};                     #   NOMC040
# $Y =~ s/CEDEX.*$//
# ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
# $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

# if ( $vals->{'INDC020'} == 0 ) {    #INDC036
# # CAS 3
# $vals->{'LIBL022'} = '';        #   LIBL038
# }
# elsif ( $vals->{'INDC020'} == 1 && index( $vals->{'LIBL022'}, $Y ) == -1 )
# {                                   # INDC036,   LIBL038
# # CAS 1
# }
# else {
# # CAS 2
# $vals->{'LIBL022'} = '';        #  LIBL038
# }

# push( @EADDR, "$vals->{'CPLA015'} $vals->{'PNTR014'}" );  #PNTR028 , CPLA029
# push(
# @EADDR, "$vals->{'PEAD016'} $vals->{'PEAD017'} "
# .    #PEAD030, PEAD031, LICN034 (inexistant dans norme 38))
# "$vals->{'LIBV019'} $vals->{'BOIP021'}"
# );         #LIBV035 ,   BOIP037
# push( @EADDR, "$vals->{'CODC023'} $vals->{'LIBL022'} $vals->{'NOMC024'}" )
# ;        # CODC039 , LIBL038, NOMC040
# @EADDR = user_cleanup_addr(@EADDR);

# if ( $PROFCOD eq 'CMS-ACS' ) {
# my @ACS = (
# 'TSA 30016',
# '42-44 RUE DU GÉNÉRAL DE LARMINAT',
# '33044 BORDEAUX'
# );
# $DOC->append( 'ACS', 1 );
# $DOC->append_table( 'ADDRSECT', @ACS );
# @adr_CTC = ();
# @adr_CTC = (
# 'TSA 30016',
# '42-44 RUE DU GÉNÉRAL DE LARMINAT',
# '33044 BORDEAUX'
# );
# }
# else {
# $DOC->append( 'ACS', 0 );
# $DOC->append_table( 'ADDRSECT', @EADDR );

# #       @EADDR = ();
# }

# if ( $MODREGL eq 'Chèque' || $MODREGL eq 'PX' ) {
# if ( $adr_CTC[0] eq 'NULL' || oe_corporation_set() ne 'MNT' ) {    #
# #				$DOC->append		('EADDRFIRST',{'edTitCorp'});
# $DOC->append( 'EADDR', 'ADDRSECT' );
# $DOC->append( 'MSG',      {'MSGA'} );
# $DOC->append( 'MSGTALON', {'MSGTALONA'} );

# }
# else {
# #				$DOC->append		('EADDRFIRST',{'edTitCorp'});
# $DOC->append_table( 'ADDRCTC', @adr_CTC );
# $DOC->append( 'EADDR', 'ADDRCTC' );
# $DOC->append( 'MSG',      {'MSGB'} );
# $DOC->append( 'MSGTALON', {'MSGTALONB'} );
# }

# }
# else {
# $DOC->append( 'EADDR', 'ADDRSECT' );
# }

# }

sub gestion_erreur {
    my ( $nom_variable, $ref_variable, $ID_agent, $bool_edit ) = @_;

    # on conserve la valeur 1 si $bool_edit deja egal a 1
    if ( $bool_edit == 1 ) {
        return 1;
    }

    # si valeur de la variable est nulle
    if ( $ref_variable eq 'null' ) {
        open( my $desc_error, '>>', $file_error );

        # imprime dans la log
        print $desc_error $date_log
          . '	ERREUR: '
          . $nom_variable
          . " non defini. Document pour PX_ID = $ID_agent non genere.\n";

        close $desc_error;

        return 1;
    }

    # cas present par precaution
    unless ($ref_variable) {
        open( my $desc_error, '>>', $file_error );

        # imprime dans la log
        print $desc_error $date_log
          . '	ERREUR: '
          . $nom_variable
          . " non defini. Document pour PX_ID = $ID_agent non genere.\n";

        close $desc_error;

        return 1;
    }

    #pas d'erreur rencontree
    return 0;

}

sub format_IBAN {

    # formate IBAN et masque avec des *
    my ( $iban_arg, $accountswitchid_arg ) = @_;

    if ( $iban_arg =~
/ ( [a-z]{2} ) ( [0-9a-z]{2} ) ( [0-9a-z]{4} ) ( [0-9a-z]{4} ) ( [0-9a-z]{2} ) ( [0-9a-z]{2} ) ( [0-9a-z]{4} ) ( [0-9a-z]{4} ) ( [0-9a-z]{3} ) /xi
      )
    {
        # masquage IBAN par des *
        # $IBANORI = "$1$2 $3 $4 $5$6 $7 $8 $9";
        $iban_arg = "$1** **** **** **$6 $7 $8 ***";

    }
    else {
        open( my $desc_error, '>>', $file_error );
        print $desc_error $date_log
          . "	PB format d'IBAN dans le flux PHOENIX pour REF $accountswitchid_arg \n";
        close $desc_error;
    }

    return $iban_arg;
}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user    = user_get_aneto_user( $argv[0] );
    my $OPT_GED = '--edms';

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'ID-MNT', 'NOMDEST', 'NUMADH' ]
    );

    oe_new_job('--index');

    $DOC->append( 'xTYPDOC', 'MOBQ' );
    $DOC->append( 'xSOURCE', 'coheris' );
    $DOC->append( 'xOWNER',  'coheris' );

    my $oparser = &recup_data();

    #type de doc que l'on veut imprimer
    # my $TYPDOC = 1;
    # my $TYPDOC = 2;
    # my $TYPDOC = 3;
    # my $TYPDOC = 4;
    my $TYPDOC = 5;    # ALL DOCS

    print 'IMPRESSION DU DOC ' . $TYPDOC . " ...\n";
    sleep(5);
    edit_courrier( $oparser, $TYPDOC );

    # lance la generation du PDF
    oe_compo_link($OPT_GED);

    $TRK->track( 'Doc', 1, $ID_MNT, $NOMPRENOM, $NUM_CONTRAT );
    return 0;
}

exit main(@ARGV);
