#!/usr/bin/perl
use utf8;
use strict;

use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::P7;
use oEUser::Descriptor::P8;
use oEUser::Descriptor::P9;
use oEUser::Descriptor::OB;
use oEUser::Descriptor::OH;
use oEUser::Descriptor::PV;
use oEUser::Descriptor::Z1;
use Data::Dumper;
use oEdtk::RecordParser;
use oEdtk::Tracking;

# INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
my $CPT_ADH  = 0;
my $COUNT_P8 = 0;
my $E1_ODERCODERA =
  "";    # ODERCOD	@[24] len 9	<= (E1) E1_ODERCODERA (agence de rattachement)
my $E2_ACH1NUMCIN =
  "";    # ACH1NUM	@[36] len 15	<= (E2) E2_ACH1NUMCIN (numéro d'adhérent)
my $DCPH017 =
  "";    # DCPH017	@[17] len 8	<= (P8) PR0002MJ_DCPHDATPAI (DATE DE PAIEMENT.)
my $DCRC026_somme_indus = 0
  ; # DCRC026	@[26] len 14	<= (P8) PR0002MJ_DCRCMNTRCP (TOTAL INDUS RÉCUPÉRÉS .)
my $PSDEACA = "";    # PSDEACA	@[50] len 2	<= (P8) PR0002MJ_PSDEACASI2 (RANG.)
my $PSEP054 =
  ""; # PSEP054	@[54] len 13	<= (P8) PR0002MJ_PSEPETENRO (NUMÉRO DE RO + CLÉ.)
my $PSLDCOD =
  "0";    # PSLDCOD	@[13] len 5	<= (P9) PR0002MJ_PSLDCODARC (CODE ACTE RC.)
my $PSRLGES = ""
  ; # PSRLGES	@[56] len 1	<= (P8) PR0002MJ_PSRLGESRIN (INDICATEUR DÉCOMPTE INFORMATIF.)
my $DCRICLECPT  = "";
my $DCRICODBQE  = "";
my $DCRICODGUI  = "";
my $DCRINUMCPT  = "";
my $NOMTIERS_OK = "";
my $ADH         = "";
my $NUMABA;
my $ENTITEJUR = "";
my $AGENCE    = "";
my ( $DOC, $GED, $PREMIERE_DATE_SOINS, $DERNIERE_DATE_SOINS, $PREMIERE_DATPAI );

my %hCountRbt;
my %hCumulVirt;
my %hDesAss;            # descriptif Assuré concerné
my %hDetRbt;
my %hDetRbtPSLPDDE;     # date de début de soin : sélectionner la première
my %hDetRbtPSLPDFE;     # date de fin de soin : sélectionner la dernière
my %hDetRebtDepense;    # montant de la dépense inscrite pour le soin
my %hDetRebtMntMNT;     # montant de remboursement MNT
my %hDetRebtMntSS;      # montant de remboursement SS
my %hListDetRbt
  ; # liste des détails de remboursement pour chaque règlement (tableau anonyme de cles rembt / cles règlement)
my ( $TRK, $IDDEST, $AGENCE, $NOMDEST, $COMPTE, $INDUS );

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    my $user = user_get_aneto_user( $argv[0] );
    user_corp_file_prefixe( $argv[0], '_' );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDDEST', 'AGENCE', 'NOMDEST', 'COMPTE', 'INDUS' ]
    );

    # trame keys => ['IDDEST', 'AGENCE', 'NOMDEST', 'COMPTE', 'INDUS']

    oe_new_job('--index');

    # INITIALISATION PROPRE AU DOCUMENT
    initDoc();
    $DOC->append( 'xTYPDOC', 'RELP' );

    # XXX We're missing the 'PO' record.
    my $E1 = oEUser::Descriptor::E1::get();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2::get();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7::get();
    $E7->bind_all_c7();
    my $P7 = oEUser::Descriptor::P7::get();
    $P7->bind_all_c7();
    my $P8 = oEUser::Descriptor::P8::get();
    $P8->bind_all_c7();
    my $P9 = oEUser::Descriptor::P9::get();
    $P9->bind_all_c7();
    my $OB = oEUser::Descriptor::OB::get();
    $OB->bind( 'OB-TXT', 'MSGOBS' );
    my $OH = oEUser::Descriptor::OH::get();
    $OH->bind_all_c7();
    my $PV = oEUser::Descriptor::PV::get();
    $PV->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E7' => $E7,
        'E2' => $E2,
        'P7' => $P7,
        'P8' => $P8,
        'P9' => $P9,
        'OB' => $OB,
        'OH' => $OH,
        'PV' => $PV,
        'PO' => undef,
        'Q1' => undef,
        'Q2' => undef,
        'Z1' => $Z1
    );

    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'OB' ) {
            $DOC->append($vals);
        }
        elsif ( $id eq 'OH' ) {
            if ( $vals->{'DEMATxx'} eq "EMAIL" ) {
                $GED->append( 'xCODRUPT', 'DEMAT' )
                  ; # mise en place d'une rupture sur les relevés dématérialisés pour le lotissement
            }
        }
        elsif ( $id eq 'E1' ) {
            finAdherent_E1($DOC);
            $DOC = initDoc();

            $GED->append( 'xCLEGEDiv', substr( $vals->{'PENO027'}, 0, 37 ) )
              ; ### Met en cleged 4 le nom de l'agence - Ajouter pour qu'Actimail puisse le récupérer et le mettre sur le talon

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $vals->{'LICCODP'} );
            $vals->{'LICCODP'} =~
              s/France//i;    #SI LE CODE PAYS EST "FRANCE", ON NE L'UTILISE PAS

            $ADH =
              (     $vals->{'PENOCOD'} . ' '
                  . $vals->{'PENOPRN'} . ' '
                  . $vals->{'PENOLIB'} );
            $ENTITEJUR = $vals->{'PENO027'};
            $AGENCE    = $vals->{'ODERCOD'};
            $GED->append( 'xIDEMET', $AGENCE );
            $NOMDEST = $vals->{'PENOLIB'} . " " . $vals->{'PENOPRN'};
            $GED->append( 'xNOMDEST', $NOMDEST );

 # LES RÈGLES DE GESTION DES TÉLÉPHONES DES AGENCES VARIENT SELON LE DOCUMENT
            $DOC->append( 'LICNATC', $vals->{'LICNATC'} );

            if ( $vals->{'CODC039'} =~ /^97/ ) {
                $DOC->append( 'SWTCHTEL', $vals->{'PECOVAL'} );
            }
            elsif ($vals->{'ODERCOD'} =~ /C051/
                || $vals->{'ODERCOD'} =~ /C063/ )
            {
                $DOC->append( 'SWTCHTEL', {'Indigo'} );
            }
            else {
                $DOC->append( 'SWTCHTEL', {'Indigo'} );
            }
            $DOC->append( oe_corporation_tag() );

        }
        elsif ( $id eq 'E2' ) {
            $E2_ACH1NUMCIN = $vals->{'ACH1NUM'};
            $IDDEST        = $vals->{'ACH1NUM'};
            $GED->append( 'xIDDEST', $IDDEST );
            $DOC->append( 'NUMADH',  $vals->{'ACH1NUM'} );
        }
        elsif ( $id eq 'E7' ) {

            #warn "INFO : dump E7 ". Dumper($vals) ."\n";
            gestionAdrE7( $DOC, $vals );

        }
        elsif ( $id eq 'P8' ) {

            #warn "INFO : dump P8 ". Dumper($vals) ."\n";
            #warn "INFO : TIERS : ". $p->dumper($vals->{'PENO080'}) ."\n";
            $DCRC026_somme_indus += $vals->{'DCRCMNT'};
            $TRK->track( 'W', 1, '', '', '', '',
                $vals->{'DCRCMNT'} . " =>INDU IN P8" )
              if ( $vals->{'DCRCMNT'} > 0 );
            nouveauReglement_P8( $DOC, $vals );

        }
        elsif ( $id eq 'P9' ) {

            #warn "INFO : dump P9 ". Dumper($vals) ."\n";
            nouveauDetail_P9( $DOC, $vals );

        }
        elsif ( $id eq 'PV' ) {

            #warn "INFO : dump PV ". Dumper($vals) ."\n";

        }
        elsif ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($vals) ."\n"; die();
            $NUMABA = $vals->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
  # $GED->append('xCLEGEDiii',$NUMABA);  			#- N° adhérent (ID_MNT) == N° ABA
        }
    }
    finAdherent_E1($DOC);
    oe_compo_link( $argv[0] );
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################

sub initDoc {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    $PSRLGES = ""
      ; # PSRLGES	@[56] len 1	<= (P8) PR0002MJ_PSRLGESRIN (INDICATEUR DÉCOMPTE INFORMATIF.)
    $DCPH017 =
      "";  # DCPH017	@[17] len 8	<= (P8) PR0002MJ_DCPHDATPAI (DATE DE PAIEMENT.)
    $PSEP054 = ""
      ; # PSEP054	@[54] len 13	<= (P8) PR0002MJ_PSEPETENRO (NUMÉRO DE RO + CLÉ.)
    $PSDEACA = "";    # PSDEACA	@[50] len 2	<= (P8) PR0002MJ_PSDEACASI2 (RANG.)
    $PSLDCOD =
      "0";    # PSLDCOD	@[13] len 5	<= (P9) PR0002MJ_PSLDCODARC (CODE ACTE RC.)
    $DCRC026_somme_indus = 0
      ; # DCRC026	@[26] len 14	<= (P8) PR0002MJ_DCRCMNTRCP (TOTAL INDUS RÉCUPÉRÉS .)
    $PREMIERE_DATE_SOINS = "";
    $DERNIERE_DATE_SOINS = "";
    $PREMIERE_DATPAI     = "";

    undef %hCumulVirt;
    undef %hDetRbt;
    undef %hDetRebtDepense;
    undef %hDetRebtMntSS;
    undef %hDetRebtMntMNT;
    undef %hDetRbtPSLPDDE;
    undef %hDetRbtPSLPDFE;
    undef %hDesAss;
    undef %hListDetRbt;

    $GED = oEdtk::TexDoc->new();
    $DOC = oEdtk::TexDoc->new();
}

# FONCTIONS TRAITEMENTS DONNEES v 20070326-132516
sub nouveauReglement_P8 {
    my ( $DOC, $vals ) = @_;

#	$GED->append('xCLEGEDiv', substr($vals->{'PENO045'}, 0, 37));	### Met en cleged 4 le nom de l'agence - Ajouter pour qu'Actimail puisse le récupérer et le mettre sur le talon

# les références bancaires sont communiquées n fois et peuvent être effacées dans le P8
# il faut donc conserver les informations lorsqu'elles sont fournies
# DCRI013	@[13] len 10	<= (P8) PR0002MJ_DCRICODBQE (CODE BANQUE.)
# DCRI014	@[14] len 10	<= (P8) PR0002MJ_DCRICODGUI (CODE GUICHET.)
# DCR015a	@[15] len 20	<= (P8) PR0002MJ_DCRINUMCPT (N° COMPTE.)
# DCRI016	@[16] len 2	<= (P8) PR0002MJ_DCRICLECPT (CLÉ.)
    $DCRICODBQE = $vals->{'DCRICOD'}
      if ( $vals->{'DCRICOD'} ne "" && $DCRICODBQE eq "" );
    $DCRICODGUI = $vals->{'DCRI014'}
      if ( $vals->{'DCRI014'} ne "" && $DCRICODGUI eq "" );
    $DCRINUMCPT = $vals->{'DCRINUM'}
      if ( $vals->{'DCRINUM'} ne "" && $DCRINUMCPT eq "" );
    $DCRICLECPT = $vals->{'DCRICLE'}
      if ( $vals->{'DCRICLE'} ne "" && $DCRICLECPT eq "" );
    $COMPTE = $vals->{'DCRINUM'} . " " . $vals->{'DCRICLE'};

# remplissage d'une table de référence des remboursements par adhérent principal
# clef de tri constituée de :
#	PSRLGES	@[56]	(P8 type de remboursement PR0002MJ_PSRLGESRIN INDICATEUR DÉCOMPTE INFORMATIF)
#				= 0 alors il s'agit d'un remboursement ADH
#				= 1 alors il s'agit d'un remboursement TIER
#	DCPH017	@[17] len 8	<= (P8) PR0002MJ_DCPHDATPAI (DATE DE PAIEMENT.)
#	PSDEACA	@[50] len 2	<= (P8) PR0002MJ_PSDEACASI2 (RANG.)
#	PSEP054	@[54] len 13	<= (P8) PR0002MJ_PSEPETENRO (NUMÉRO DE RO + CLÉ.)
#	PSLDCOD	@[13] len 5	<= (P9) PR0002MJ_PSLDCODARC (CODE ACTE RC.)
    $PSRLGES = $vals->{'PSRLGES'}
      ; # P8 type de remboursement PR0002MJ_PSRLGESRIN INDICATEUR DÉCOMPTE INFORMATIF
    $DCPH017 = $vals->{'DCPHDAT'}; #(P8) PR0002MJ_DCPHDATPAI (DATE DE PAIEMENT.)

    # Reformat the date so that it is suitable as a sort key.
    if ( $DCPH017 !~ /^(\d{2})\/(\d{2})\/(\d{4})$/ ) {

# on s'est apperçu dans un cas unique en 6 ans qu'il pouvait ne pas y avoir de date de règlement pour un règlement au tiers (informatif dans le document)
        my $message =
"ERROR: Unexpected date format \'$DCPH017\' for RO $vals->{'PSEPETE'} at line $. in input file";
        warn "$message\n";

        # on laisse donc passer, mais il faudra surveiller
        $TRK->track( 'Warn', 1, '', '', '', '', $message );

# du coup on met la clef de tri à blanc pour ne pas perdre l'information et l'éditer tout de même
# => risque d'un règlement d'un acte à l'assurer sans date de règlement ????
        $DCPH017 = "";

    }
    else {
#$DCPH017 =~ /^(\d{2})\/(\d{2})\/(\d{4})$/ or warn "ERROR: Unexpected date format \'$DCPH017\' for RO $vals->{'PSEPETE'} at line $. in input file\n";
        $DCPH017 = "$3$2$1";
    }

    $PSDEACA = sprintf( "%02.0f", $vals->{'PSDEACA'} )
      ;    # (P8) PR0002MJ_PSDEACASI2 (RANG.)
    $PSEP054 =
      $vals->{'PSEPETE'};    # (P8) PR0002MJ_PSEPETENRO (NUMÉRO DE RO + CLÉ.)
    $PSLDCOD = '0';          # (P9) PR0002MJ_PSLDCODARC (CODE ACTE RC.)
    $GED->append( 'xCLEGEDi', $vals->{'PSEPETE'} );

  # 	CUMUL DES REGLEMENTS MULTIPLES POUR UNE MÊME DATE DE VIREMENT => .$PSEP054
    $hCumulVirt{ $PSRLGES . $DCPH017 . $PSDEACA . $PSEP054 } +=
      $vals->{'PSDE070'};

    # pour chaque info remboursement en P8, on conserve :
    #	PSDEPRN	@[49]	(P8 prénom de l'assuré PSDEPRNASS)
    #	PSEP054	@[54]	(P8 numéro de ro)
    #	PSEO055	@[55]	(P8 numéro de clé)
    #	DCPH025	@[25]	(P8 MONTANT DU PAIEMENT)
    #	PSDE070	@[70]	(P8 MONTANT RC DU DÉCOMPTE)

    my $regl = oEdtk::TexDoc->new();
    $regl->append( 'DATPAI', $vals->{'DCPHDAT'} );
    $regl->append( 'PSDEPRN',
        ucfirst( lc( oe_uc_sans_accents( $vals->{'PSDEPRN'} ) ) ) );
    $regl->append( 'NUMSECU', "$PSEP054 $vals->{'PSEOCLE'}" );
    $NOMTIERS_OK = $vals->{'PENO080'} || ".";
    $NOMTIERS_OK =~ s/\// \/ /g;    # on ajoute des espaces autour de '/'
    $NOMTIERS_OK =~ s/\./\. /g;     # on ajoute des espaces apres les '.'
    $regl->append( 'NOMTIERS', $NOMTIERS_OK );
    $regl->append( 'nlAdh' . chr( ord('A') + $PSRLGES ) );
    $hDesAss{ $PSRLGES . $DCPH017 . $PSDEACA . $PSEP054 } = $regl;
    $PREMIERE_DATPAI = oe_date_smallest( $PREMIERE_DATPAI, $vals->{'DCPHDAT'} );
}

sub nouveauDetail_P9 {
    my ( $DOC, $vals ) = @_;

    # pour chaque info remboursement en P9, on conserve :
    #	PSLPDDE 	@[25] 	(DATE DE DÉBUT DES SOINS PR0002MJ_PSLPDDESOI)
    #	PSLPDFE	@[26]	(PR0002MJ_PSLPDFESOI DATE DE FIN DES SOINS)
    #	PSZZLIB	@[14]	(LIBELLÉ DE L'ACTE PR0002MJ-PSZZLIBARC)
    #	PSLD019	@[19]	(PSLD019 => (P9) PR0002MJ_PSLDMNTDEP [19])
    #	PSLD022	@[22]	(PR0002MJ_PSLDMNTROR MONTANT RO RETENU)
    #	PSLD023	@[23]	(PR0002MJ_PSLDMNTRCR MONTANT RC RETENU)

# CLEFS DE TRI
#	PSRLGES	@[56] len 1	<= (P8) PR0002MJ_PSRLGESRIN (INDICATEUR DÉCOMPTE INFORMATIF.)
#	DCPH017	@[17] len 8	<= (P8) PR0002MJ_DCPHDATPAI (DATE DE PAIEMENT.)
#	PSDEACA	@[50] len 2	<= (P8) PR0002MJ_PSDEACASI2 (RANG.)
#	PSEP054	@[54] len 13	<= (P8) PR0002MJ_PSEPETENRO (NUMÉRO DE RO + CLÉ.)
#	PSLDCOD	@[13] len 5	<= (P9) PR0002MJ_PSLDCODARC (CODE ACTE RC.)

    $PSLDCOD = $vals->{'PSLDCOD'};

# if ($DATATAB[23] > 0) {  # ON AFFICHE PAS LES OPÉRATIONS DONT LE MONTANT MNT EST À ZÉRO # suspendu le 02/06/09

# 02/06/09 ON SUSPEND REGROUPEMENT DES LIGNES DE DÉTAIL PAR DATE DE PAIEMENT/BÉNÉFICIAIRE/CODE ACTE RC AVEC CUMULE DES MONTANTS
# PSLDQTE	@[16] len 4	<= (P9) PR0002MJ_PSLDQTERTU (QUANTITÉ.) ?
# my $cle=$PSRLGES.$DCPH017.$PSDEACA.$PSEP054.$PSLDCOD;
#	sinon sur .$COUNT_P8
    $COUNT_P8++;
    my $cle = $PSRLGES . $DCPH017 . $PSDEACA . $PSEP054 . $COUNT_P8;

    if ( exists $hDetRebtMntMNT{$cle} ) {

# on a déjà traité un remboursement du type $PSLDCOD pour cette date de remboursement
    }
    else {
# s'il s'agit d'un nouveau remboursement à la date de réglement pour $cle, on mémorise son descriptif
        $hDetRbtPSLPDDE{$cle} = $vals->{'PSLPDDE'};

        push(
            @{ $hListDetRbt{ $PSRLGES . $DCPH017 . $PSDEACA . $PSEP054 } },
            $cle
        );
    }
    $PREMIERE_DATE_SOINS =
      oe_date_smallest( $PREMIERE_DATE_SOINS, $vals->{'PSLPDDE'} );
    $hDetRbtPSLPDFE{$cle} = $vals->{'PSLPDFE'};
    $DERNIERE_DATE_SOINS =
      oe_date_biggest( $PREMIERE_DATE_SOINS, $vals->{'PSLPDFE'} );
    $hDetRebtDepense{$cle} += $vals->{'PSLD019'};
    $hDetRebtMntSS{$cle}   += $vals->{'PSLD022'};
    $hDetRebtMntMNT{$cle}  += $vals->{'PSLD023'};

    # on compte les détails de réglements pour la date de réglement
    $hCountRbt{ $PSRLGES . $DCPH017 . $PSDEACA . $PSEP054 }++;

    # on memorise les données a afficher pour la cle de remboursement
    # attention, on conserve la dernière information communiquée
    my $detail = oEdtk::TexDoc->new();
    $vals->{'PSZZLIB'} =
      ucfirst( lc( oe_uc_sans_accents( $vals->{'PSZZLIB'} ) ) );
    $detail->append( 'NOMTIERS', $NOMTIERS_OK );
    $detail->append( 'PSZZLIB',  $vals->{'PSZZLIB'} );
    $detail->append( 'ENGMNT',   $hDetRebtDepense{$cle} );
    $detail->append( 'SECUMNT',  $hDetRebtMntSS{$cle} );
    $detail->append( 'MNTMNT',   $hDetRebtMntMNT{$cle} );
    $detail->append( 'nlRbt' . chr( ord('A') + $PSRLGES ) );
    $hDetRbt{$cle} = $detail;
}

sub finAdherent_E1 {
    my ( $DOC, $vals ) = @_;
    my $tempPSRLGES;
    my $chaineCalculee;
    my $nbRbt        = 0;
    my $nbRbtTiers   = 0;
    my $cmptRbt      = 0;
    my $cmptRbtTiers = 0;

    #my $isFirst=1;

    $PSRLGES = 9;
    if ( $CPT_ADH++ >= 1 ) {
        my $montant_indu;

        # édition des références bancaires
        $DOC->append( 'BANQUE',  $DCRICODBQE );
        $DOC->append( 'GUICHET', $DCRICODGUI );
        $DOC->append( 'NUMCPT',  "$DCRINUMCPT $DCRICLECPT" );
        $DOC->append('finAdh');
        $INDUS = $DCRC026_somme_indus;

        foreach my $cleRbt ( sort keys %hDesAss )
        {    # premier passage pour compter les rbt tiers et les autres
            $cleRbt =~ /^(\d{1})/;
            $tempPSRLGES = $1;

            # détermimner s'il s'agit d'un remboursement ou d'un tier
            if ( $PSRLGES ne $tempPSRLGES ) {
                if ( $tempPSRLGES == 1 ) {

                    # les indus sur les règlements aux tiers sont disctincts
                    $DCRC026_somme_indus = 0;
                }
                $PSRLGES = $tempPSRLGES;
            }
            if ( $tempPSRLGES == 0 )
            {    #On n'est pas dans un remboursement à un tiers
                $nbRbt++;
            }
            else {
                $nbRbtTiers++;
            }
        }

        foreach my $cleRbt ( sort keys %hDesAss )
        {        # deuxieme passage où on fait effectivement le boulot
            $cleRbt =~ /^(\d{1})/;
            $tempPSRLGES = $1;

            # détermimner s'il s'agit d'un remboursement ou d'un tier
            if ( $PSRLGES ne $tempPSRLGES ) {
                if ( $tempPSRLGES == 1 ) {

                    # les indus sur les règlements aux tiers sont disctincts
                    $DCRC026_somme_indus = 0;
                }
                $PSRLGES = $tempPSRLGES;
            }

            my $tri = oEdtk::TexDoc->new();
            $tri->append( $hDesAss{$cleRbt} );
            foreach my $cle ( @{ $hListDetRbt{$cleRbt} } ) {
                $tri->append( 'PSLPDDE', $hDetRbtPSLPDDE{$cle} );
                $tri->append( 'PSLPFDE', $hDetRbtPSLPDFE{$cle} );
                $tri->append( $hDetRbt{$cle} );
            }

            # On calcule le montant reellement regle en fonction de l'indu
            my $total_regle = $hCumulVirt{$cleRbt} - $DCRC026_somme_indus;
            $montant_indu = $DCRC026_somme_indus;
########		#$INDUS = $DCRC026_somme_indus;
            # si la somme due ne couvre pas l'indu

            if ( $montant_indu != 0 ) {

                # mise en place d'une rupture sur les indus pour le lotissement
                $GED->append( 'xCODRUPT', 'I' );
                $DOC->append( 'totPres',  $hCumulVirt{$cleRbt} );

                # $montant_indu = -1 * $montant_indu;
                $DOC->append( 'totIndu', ( -1 * $montant_indu ) );

                #$tri->append('nlIndu');
            }

            if ( $total_regle <= 0 ) {

                # le montant versé vaut 0
                $total_regle = 0;

                # l'indu n'est pas soldé
                $DCRC026_somme_indus =
                  $DCRC026_somme_indus - $hCumulVirt{$cleRbt};
            }
            else {
                # sinon l'indu est soldé
                $DCRC026_somme_indus = 0;
            }

# Comme $total_regle est calculé en variable décimale 6.6-6.6 peut valoir 1.70e-56 par exemple
#    par conséquent on édite la variable en la formatant
# $tri->append('cumVirt', sprintf("%.2f", $total_regle));
            $DOC->append( 'cumVirt', $total_regle );
            $DOC->append( 'triRbt',  $tri );

            if ( $tempPSRLGES == 0 )
            {    # ON N'EST PAS DANS UN REMBOURSEMENT À UN TIERS (ADH)
                if ( $cmptRbt == 0 ) {    # C'est la premiere fois qu'on rentre
                    $DOC->append('debutTabRbt');
                }
                $cmptRbt++;
                $DOC->append('eTriRbt');

                if ( $montant_indu != 0 ) {    # Il y a un indu
                    if ( $cmptRbt == $nbRbt ) {
                        $DOC->append('lignesTotalInduLast');
                        $DOC->append('FinTabRbt');
                    }
                    else { $DOC->append('lignesTotalInduNotLast'); }
                }
                else {                         #Il n'y a pas d'indu
                    if ( $cmptRbt == $nbRbt ) {
                        $DOC->append('lignesTotalNormalLast');
                        $DOC->append('FinTabRbt');
                    }
                    else { $DOC->append('lignesTotalNormalNotLast'); }
                }
            }
            else {    # ON EST DANS UN REMBOURSEMENT À UN TIERS
                if ( $cmptRbtTiers == 0 )
                {     # C'est la premiere fois qu'on rentre
                    $DOC->append('debutTabTiers');
                }
                $cmptRbtTiers++;
                $DOC->append('eTriTiers');
                if ( $cmptRbtTiers == $nbRbtTiers ) {
                    $DOC->append('ligneTotalTiersLast');
                    $DOC->append('FinTabTiers');
                }
                else { $DOC->append('ligneTotalTiersNotLast'); }
            }
        }

        $cmptRbt      = 0;
        $cmptRbtTiers = 0;
    }
    $GED->append( 'xCLEGEDii',  $PREMIERE_DATE_SOINS );
    $GED->append( 'xCLEGEDiii', $DERNIERE_DATE_SOINS );

    #	$GED->append('xCLEGEDiv', $PREMIERE_DATPAI);

    $DOC->append('finDocAdh');

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    $TRK->track( 'DOC', 1, $IDDEST, $AGENCE, $NOMDEST, $COMPTE, $INDUS );

    #	$TRK->track('Doc', 1, $E2_ACH1NUMCIN, $AGENCE, 'NOM', '', $montant_indu);

    #undef %hRemboursement;
    undef %hDesAss;
    undef %hDetRbt;
    undef %hCumulVirt;
    $PSRLGES    = "";
    $PSDEACA    = "";
    $DCPH017    = "";
    $PSEP054    = "";
    $PSLDCOD    = "0";
    $DCRICODBQE = "";
    $DCRICODGUI = "";
    $DCRINUMCPT = "";
    $DCRICLECPT = "";
    $INDUS      = "";
}

sub gestionAdrE7 {
    my ( $DOC, $vals ) = @_;

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
    $vals->{'LICCODP'} =~ s/FRANCE//i;

    $vals->{'LIBLOCL'} =~ s/\s{2,}/ /g;
    my $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//;
    $Y =~ s/\s+$//;
    $Y =~ s/\s{2,}/ /g;
    if ( index( $vals->{'LIBLOCL'}, $Y ) == 0 ) {
        $vals->{'LIBLOCL'} = '';
    }

    foreach my $field (
        qw( CPLADRx PNTREMx PEADNUM PEADBTQ PEVONAT LIBVOIx BOIPOSL CODCDXC LIBLOCL NOMCDXL LICCODP)
      )
    {
        $vals->{$field} = ''
          unless exists $vals->{$field} && defined $vals->{$field};
    }
    my @DADDR = ();
    push( @DADDR, $ADH );
    push( @DADDR, join( ' ', map { $vals->{$_} } qw( CPLADRx PNTREMx ) ) );
    push( @DADDR,
        join( ' ', map { $vals->{$_} } qw( PEADNUM PEADBTQ PEVONAT LIBVOIx ) )
    );
    push( @DADDR, $vals->{'BOIPOSL'} );
    push( @DADDR,
        join( ' ', map { $vals->{$_} } qw( CODCDXC LIBLOCL NOMCDXL ) ) );
    push( @DADDR, $vals->{'LICCODP'} );

    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

    $E1_ODERCODERA = $vals->{'ODERCOD'}
      ;    # ODERCOD	@[24] len 9	<= (E1) E1_ODERCODERA (agence de rattachement)
    $vals->{'ODERCOD'} = ''
      unless exists $vals->{'ODERCOD'} && defined $vals->{'ODERCOD'};
    $AGENCE = $vals->{'ODERCOD'};

    $GED->append( 'xVILDEST', $vals->{'NOMCDXL'} );

#	$GED->append('xCPDEST', $vals->{'CODCDXC'});
# Activer la ligne suivante et commenter la précédente pour initialiser la gestion du tri par pays
    $GED->append( 'xCPDEST', oe_iso_country() . $vals->{'CODCDXC'} );

    # Au tour de l'adresse émetteur.
    $vals->{'LIBL022'} =~ s/\s{2,}/ /g;
    $Y = $vals->{'NOMC024'};
    $Y =~ s/CEDEX.*$//;
    $Y =~ s/\s+$//;
    $Y =~ s/\s{2,}/ /g;
    if ( index( $vals->{'LIBL022'}, $Y ) == 0 ) {
        $vals->{'LIBL022'} = '';
    }

    foreach my $field (
        qw( CPLA029 PNTR014 PEAD016 PEAD031 LIBV019 BOIP037 CODC023 LIBL022 NOMC024 )
      )
    {
        $vals->{$field} = ''
          unless exists $vals->{$field} && defined $vals->{$field};
    }
    my @EADDR = ();
    push( @EADDR, $ENTITEJUR );
    push( @EADDR, $AGENCE );
    push( @EADDR, join( ' ', map { $vals->{$_} } qw( CPLA029 PNTR014 ) ) );
    push( @EADDR,
        join( ' ', map { $vals->{$_} } qw( PEAD016 PEAD031 LIBV019 BOIP037 ) )
    );
    push( @EADDR,
        join( ' ', map { $vals->{$_} } qw( CODC023 LIBL022 NOMC024 ) ) );
    @EADDR = user_cleanup_addr(@EADDR);
    $DOC->append_table( 'xADREM', @EADDR );

}

exit main(@ARGV);
