#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls 0.462;
use strict;
use File::Basename;
use oEdtk::Tracking;

my $TRK;

#################################################################################
# v0.54 28/04/2009 10:35:55 du squelette d'extraction de donnees pour XLS
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données sur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

#################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
#################################################################################

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION
# my $VALEUR =0;

my $no_ro     = "";
my $dt_naiss  = "";
my $regime    = "";
my $no_ctr    = "";
my $rang      = "";
my $dt_adh    = "";
my $dt_rad    = "";
my $cod_rejet = "";
my $lib_rejet = "";
my $nom       = "";
my $prenom    = "";
my $adh       = "";
my $agence    = "";

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDDEST', 'AGENCE' ]
    );

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );
    my $ctrl_idx2 = basename($0);
    $ctrl_idx2 =~ s/\.pl$/.idx2/i;
    oe_open_fo_OUT($ctrl_idx2);

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    # avec initialisation propre au document
    &init_App();

    while ( my $ligne = <IN> ) {
        chomp($ligne);

        # APPELS DES FONCTIONS v 20090611125024
        if ( oe_process_ref_rec( 158, 2, $ligne, 168, 3034 ) ) {

            # FIN traitement enregistrement(s) : LA U2 U4 - U0 U1 U3 U5 U9

        }
        else {
            # TYPE D'ENREGISTREMENT INCONNU
            warn "INFO : IGNORE REC. line $.\n";
        }
    }
    $TRK->track( 'Doc', 1, '', 'SIEGE' );
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################

sub init_App {

    # CARTOGRAPHIE APPLICATIVE v 20090611125024
    ##	LA : Informations sur l'accord NOEMIE
    #	U0 : ENREGISTREMENT FLUX (000/000)
    #	U1 : ENREGISTREMENT LOT (100/000)
    ##	U2 : ENREGISTREMENT DOSSIER BENEFICIAIRE (200/000)
    #	U3 : ENREGISTREMENT ADRESSE + RIB ASSURE (210/000)
    ##	U4 : ENREGISTREMENT LIGNE DE CONTRAT (400/000)
    #	U5 : DETAIL DES GARANTIES (410/000)
    #	U9 : ENREGISTREMENT MESSAGE (xx0/nnn)

    # dispatcher cette liste par entité de rattachement

    oe_rec_pre_process( 'LA', \&init_donnees )
      ;    # QUEL ENREGISTREMENT REINITIALISE LE DOCUMENT ?
    oe_rec_motif( 'LA',
'A010 A009 A010 A015 A050 A002 A003 A004 A010 A015 A050 A009 A010 A015 A050 A*'
    );
    oe_rec_process( 'LA', \&process_LA );
    oe_rec_motif( 'U2',
'A05 A8 A8 A10 A10 A15 A8 A9 A8 A8 A03 A1 A01 A30 A01 A13 A2 A01 A25 A15 A01 A25 A15 A09 A13 A2 A2 A8 A02 A02 A01 A01 A25 A15 A02 A25 A25 A01 A15 A4 A04 A02 A9 A8 A1 A8 A1 A03 A8 A9 A8 A8 A03 A8 A8 A2 A6 A2 A6 A2 A10 A10 A*'
    );
    oe_rec_process( 'U2', \&process_U2 );
    oe_rec_motif( 'U4', 'A01 A08 A12 A02 A8 A8 A*' );
    oe_rec_process( 'U4', \&process_U4 );
    oe_rec_motif( 'U9', 'A10 A02 A02 A100 A*' );
    oe_rec_process( 'U9', \&process_U9 );

    oe_rec_motif( 'L9',
'A002 A002 A015 A009 A010 A015 A050 A010 A002 A003 A010 A015 A050 A009 A010 A015 A050 A010 A015 A050 A008 A013 A001 A050 A015 A008 A001 A050 A015 A015 A8 A2 A5 A010 A015 A050 A050 A008 A002 A*'
    );
    oe_rec_process( 'L9', \&process_L9 );

# oe_rec_output 	('L9', '<#ACN1COD=%s><#ACN1001=%s><#ACN1LIB=%s><#ENCTCOD=%s><#ENCT004=%s><#ENCTLIC=%s><#ENCTLIB=%s><#ENACCOD=%s><#ENCRCOD=%s><#ENCR009=%s><#ENCR010=%s><#ENCRLIC=%s><#ENCRLIB=%s><#ODERCOD=%s><#ODER014=%s><#ODERLIC=%s><#ODERLIB=%s><#RETOURx=%s><#LIBCART=%s><#LIBLART=%s><#ACN1NUM=%s><#ACN1021=%s><#ACN1NAT=%s><#ACN1NOM=%s><#ACN1PRN=%s><#ACN1DAT=%s><#ACN1RNG=%s><#ACN1027=%s><#ACN1028=%s><#ACN1029=%s><#ACN1ACO=%8.0f><#ACN1ACA=%2.0f><#ACN1032=%5.0f><#ACN1033=%s><#ACN1LIC=%s><#ACN1LIL=%s><#LIBMTFx=%s><#ACN1DEV=%s><#ACN1CLE=%s><SK>%s');

    oe_rec_motif( 'U0', 'A2 A8 A01 A10 A10 A10 A10 A10 A*' );
    oe_rec_motif( 'U1',
'A02 A02 A14 A02 A14 A02 A04 A02 A10 A10 A8 A02 A03 A04 A04 A02 A06 A06 A9 A8 A01 A03 A02 A02 A10 A10 A10 A*'
    );
    oe_rec_motif( 'U3',
'A20 A05 A30 A30 A05 A01 A05 A30 A30 A30 A05 A30 A03 A05 A30 A30 A05 A05 A11 A02 A*'
    );
    oe_rec_motif( 'U5', 'A01 A03 A8 A8 A*' );

    # INITIALISATION PROPRE AU DOCUMENT
    &init_Doc();
    1;
}

sub init_Doc {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
    #
    # 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
    # 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ;
    #	 NR7 = Numérique Right ; AL12 = Alpha Left 12 de large...  )
    #
    prod_Xls_Col_Init(
        'AC5',  'AC15', 'AC10', 'AL20', 'AC6', 'AC10',
        'AC10', 'AC12', 'AL40'
    );

    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    #
    prod_Xls_Init( "", "Liste Rejets Acquittements 'Autre'" )
      ;    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("ER");
    prod_Xls_Insert_Val("N° RO");
    prod_Xls_Insert_Val("N° Contrat");
    prod_Xls_Insert_Val("Nom Prénom Assuré");
    prod_Xls_Insert_Val("Rang");
    prod_Xls_Insert_Val("Date naiss.");
    prod_Xls_Insert_Val("Régime / Caisse RO");
    prod_Xls_Insert_Val("Code Rejet / Acquitement");
    prod_Xls_Insert_Val("Libellé rejet");

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );

    1;
}

sub init_donnees() {
    $no_ro = ""
      ; # BENINSx	200@[24] len 13	<= SCC_200_BENINS (Bénéficiaire : N° INSEE.)
     # BENCLEx	200@[25] len 2		<= SCC_200_BENCLE (Bénéficiaire : Clé du N° INSEE..)
     # ACN1021	L9@[21] len 013	<= CE006C_L9_ACN1CODMRO (MATRICULE DE L'ASSURE ( NUM INSEE ).)
     # ACN1CLE	L9@[38] len 002	<= CE006C_L9_ACN1CLEMRO (CLE N° RO DE L'ASSURE.)
    $dt_naiss = ""
      ; # BENDNAx	200@[27] len 8		<= SCC_200_BENDNA (Bénéficiaire : Date de naissance..)
     # ACN1DAT	L9@[25] len 008	<= CE006C_L9_ACN1DATNAI (DATE DE NAISSANCE DE L'ASSURE.)		 | FH-0 | PID-3320 | PRG-C:\Sources\edtk_MNT\devlib\gen_appEdtk.pl
     # ACN1RNG	L9@[26] len 001	<= CE006C_L9_ACN1RNGNAI (RANG DE NAISSANCE DE L'ASSURE.)
    $regime = ""
      ; # ENCRCOD	LA@[5] len 002		<= CE006C_LA_ENCRCODGRE (CODE GRAND REGIME (1° organisme RO DE l'ACCORD).)
        # ENCRLIC	LA@[9] len 015		<= CE006C_LA_ENCRLICTIT (LIBELLE COURT CODE TITRE CAISSE RO.)
        # ENCRCOD	L9@[8] len 002		<= CE006C_L9_ENCRCODGRE (CODE GRAND REGIME.)
        # ENCR009	L9@[9] len 003		<= CE006C_L9_ENCRCODCRO (CODE CAISSE RO.)
    $no_ctr = ""
      ; # ENCR006	LA@[6] len 003		<= CE006C_LA_ENCRCODCRO (CODE CAISSE RO (1° organisme RO DE l'ACCORD).)
        # ACN1029	L9@[29] len 015	<= CE006C_L9_ACN1NUMCIN (NUMCIN CONTRAT INDIVIDUEL.)
    $rang = ""
      ;  # BENRROx	200@[28] len 02	<= SCC_200_BENRRO (Bénéficiaire : Rang RO.)
     # BENRRCx	200@[39] len 4		<= SCC_200_BENRRC (Rang du bénéficiaire dans le contrat RC..)
     # BENRGLx	200@[30] len 01	<= SCC_200_BENRGL (Bénéficiaire : Rang laser de naissance.)
     # ACN1ACA	L9@[31] len 2		<= CE006C_L9_ACN1ACASR2 (RANG DE L ASSURE.)
     # ACN1RNG	L9@[26] len 001	<= CE006C_L9_ACN1RNGNAI (RANG DE NAISSANCE DE L'ASSURE.)
    $dt_adh =
      "";  # DDECTRx	400@[4] len 8		<= SCC_400_DDECTR (Date de début contrat..)
    $dt_rad =
      "";    # DFECTRx	400@[5] len 8		<= SCC_400_DFECTR (Date de fin contrat..)

    $cod_rejet =
      "";    # CODANOx	20N@[0] len 10		<= SCC_20N_CODANO (Code anomalie.)
     # ETADPRx	200@[14] len 01	<= SCC_200_ETADPR (Etat de la demande de prestation (Payé, Rejeté, pris en compTe).)
     # ACN1033	L9@[33] len 010	<= CE006C_L9_ACN1CODMFR (CODE MOTIF RETOUR.)
    $lib_rejet = "";    # LIBMESx	20N@[3] len 100	<= SCC_20N_LIBMES (Message.)
      # ACN1LIL	L9@[35] len 050	<= CE006C_L9_ACN1LILMFR (LIBELLE LONG MOTIF RETOUR (TABREF U0121)
      # LIBMTFx	L9@[36] len 050	<= CE006C_L9_ACZZ_LIBMTF (MOTIF RETOUR EMIS LORS DE NOEMIE 9XX.)

# REFORIx	200@[13] len 30	<= SCC_200_REFORI (Référence du décompte d'origine (date & N°).)
    $nom    = "";
    $prenom = "";
    $adh    = "";
    $agence = "";
    1;
}

sub process_LA() {

# ENCTCOD	LA@[1] len 009	<= CE006C_LA_ENCTCODCTI (CODE CENTRE DE TRAITEMENT.)
# ENCRCOD	LA@[5] len 002	<= CE006C_LA_ENCRCODGRE (CODE GRAND REGIME (1° organisme RO DE l'ACCORD).)
# ENCR006	LA@[6] len 003	<= CE006C_LA_ENCRCODCRO (CODE CAISSE RO (1° organisme RO DE l'ACCORD).)
# ENCRLIC	LA@[9] len 015	<= CE006C_LA_ENCRLICTIT (LIBELLE COURT CODE TITRE CAISSE RO.)
    $regime = "$DATATAB[5] - $DATATAB[6]";

#warn "INFO : TEST DE PASSAGE.!!!!!$DATATAB[9]!!!$DATATAB[10]!!!$DATATAB[11]!!!$DATATAB[12]!!!!!!!!!!!!!!!!\n";

    1;
}

sub process_U2 () {

    # NUMCTRx	200@[38] len 15	<= SCC_200_NUMCTR (N° de contrat RC.)
    $no_ctr = $DATATAB[38];

# BENINSx	200@[24] len 13	<= SCC_200_BENINS (Bénéficiaire : N° INSEE.)
# BENCLEx	200@[25] len 2		<= SCC_200_BENCLE (Bénéficiaire : Clé du N° INSEE..)
    $no_ro = "$DATATAB[24] $DATATAB[25]";

# BENDNAx	200@[27] len 8	<= SCC_200_BENDNA (Bénéficiaire : Date de naissance..)
    $dt_naiss = $DATATAB[27];

# BENRRCx	200@[39] len 4	<= SCC_200_BENRRC (Rang du bénéficiaire dans le contrat RC..)
# BENRROx	200@[28] len 02	<= SCC_200_BENRRO (Bénéficiaire : Rang RO.)
# BENRGLx	200@[30] len 01	<= SCC_200_BENRGL (Bénéficiaire : Rang laser de naissance.)
    $rang = "$DATATAB[39]-$DATATAB[28]";

    1;
}

sub process_U4 () {

    # DDECTRx	400@[4] len 8	<= SCC_400_DDECTR (Date de début contrat..)
    $dt_adh = $DATATAB[4];

    # DFECTRx	400@[5] len 8	<= SCC_400_DFECTR (Date de fin contrat..)
    $dt_rad = $DATATAB[5];

    1;
}

sub process_U9 () {

    # CODANOx	20N@[0] len 10	<= SCC_20N_CODANO (Code anomalie.)
    $cod_rejet = $DATATAB[0];

    # LIBMESx	20N@[3] len 100	<= SCC_20N_LIBMES (Message.)
    $lib_rejet = $DATATAB[3];

    prod_Xls_Insert_Val($agence);
    prod_Xls_Insert_Val($no_ro);
    prod_Xls_Insert_Val($no_ctr);
    prod_Xls_Insert_Val($adh);
    prod_Xls_Insert_Val($rang);
    oe_to_date($dt_naiss);
    prod_Xls_Insert_Val($dt_naiss);
    prod_Xls_Insert_Val($regime);
    prod_Xls_Insert_Val($cod_rejet);
    prod_Xls_Insert_Val($lib_rejet);
    prod_Xls_Edit_Ligne();

    1;
}

sub process_L9 () {
    &init_donnees();

    $no_ro = "$DATATAB[21] $DATATAB[38]";

# ACN1021	L9@[21] len 013	<= CE006C_L9_ACN1CODMRO (MATRICULE DE L'ASSURE ( NUM INSEE ).)
# ACN1CLE	L9@[38] len 002	<= CE006C_L9_ACN1CLEMRO (CLE N° RO DE L'ASSURE.)
    $dt_naiss = $DATATAB[25];

# ACN1DAT	L9@[25] len 008	<= CE006C_L9_ACN1DATNAI (DATE DE NAISSANCE DE L'ASSURE.)		 | FH-0 | PID-3320 | PRG-C:\Sources\edtk_MNT\devlib\gen_appEdtk.pl
# ACN1RNG	L9@[26] len 001	<= CE006C_L9_ACN1RNGNAI (RANG DE NAISSANCE DE L'ASSURE.)
    $regime = "$DATATAB[8] - $DATATAB[9]";

# ENCRCOD	L9@[8] len 002		<= CE006C_L9_ENCRCODGRE (CODE GRAND REGIME.)
# ENCRLIC	LA@[9] len 015		<= CE006C_LA_ENCRLICTIT (LIBELLE COURT CODE TITRE CAISSE RO.)
# ENCR009	L9@[9] len 003		<= CE006C_L9_ENCRCODCRO (CODE CAISSE RO.)
    $no_ctr = $DATATAB[29];

  # ACN1029	L9@[29] len 015	<= CE006C_L9_ACN1NUMCIN (NUMCIN CONTRAT INDIVIDUEL.)
    $rang = "$DATATAB[31] - $DATATAB[26]";

# ACN1ACA	L9@[31] len 2		<= CE006C_L9_ACN1ACASR2 (RANG DE L ASSURE.)
# ACN1RNG	L9@[26] len 001	<= CE006C_L9_ACN1RNGNAI (RANG DE NAISSANCE DE L'ASSURE.)
    $dt_adh = "";
    $dt_rad = "";

    $cod_rejet = $DATATAB[33];

    # ACN1033	L9@[33] len 010	<= CE006C_L9_ACN1CODMFR (CODE MOTIF RETOUR.)
    $lib_rejet = $DATATAB[36];

# ACN1LIL	L9@[35] len 050	<= CE006C_L9_ACN1LILMFR (LIBELLE LONG MOTIF RETOUR (TABREF U0121)
# LIBMTFx	L9@[36] len 050	<= CE006C_L9_ACZZ_LIBMTF (MOTIF RETOUR EMIS LORS DE NOEMIE 9XX.)
    $nom    = $DATATAB[27];
    $prenom = $DATATAB[28];
    $adh    = "$DATATAB[27] $DATATAB[28]";
    $agence = $DATATAB[13];

    if ( $no_ctr ne "" && $no_ctr ne "00000000" && $no_ctr =~ /000\d/ ) {
        prod_Xls_Insert_Val($agence);
        prod_Xls_Insert_Val($no_ro);
        prod_Xls_Insert_Val($no_ctr);
        prod_Xls_Insert_Val($adh);
        prod_Xls_Insert_Val($rang);
        oe_to_date($dt_naiss);
        prod_Xls_Insert_Val($dt_naiss);
        prod_Xls_Insert_Val($regime);
        prod_Xls_Insert_Val($cod_rejet);
        prod_Xls_Insert_Val($lib_rejet);
        prod_Xls_Edit_Ligne();
    }
    1;
}

exit main(@ARGV);
