#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls;
use Data::Dumper;
use strict;
use oEdtk::Tracking;
my $TRK;

#################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
#################################################################################
# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)
my $compteurI5 = 0;
my $total      = 0;

my (
    $EE01002_EE017C01_I5_NUMCCO,        $EE01003_EE017C01_I5_GRPASS,
    $EE01007_EE017C01_I5_DATECH_DEB,    $EE01008_EE017C01_I5_DATECH_FIN,
    $EE01011_EE017C01_I5_MODSEL_DATENC, $EE01009_EE017C01_I5_DATENC_DEB,
    $EE01010_EE017C01_I5_DATENC_FIN,    $EE0006a_EE017C01_I6_CODPART,
    $EE0002a_EE017C01_I6_NUMCIN,        $CIVI035_EE017C01_I6_CIVILITE_SOUS,
    $EE01036_EE017C01_I6_NOM_SOUS,      $EE01037_EE017C01_I6_PRENOM_SOUS,
    $EE0010a_EE017C01_I6_MTT_SOLDE,     $EE01036_EE017C01_I6_MODE_REG,
    $I6_ER
);

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDDEST', 'AGENCE' ]
    );

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    initApp();

    # INITIALISATION PROPRE AU DOCUMENT
    #initDoc();

    while ( my $ligne = <IN> ) {
        chomp($ligne);

        # APPELS DES FONCTIONS v 20070406-094002
        if ( $ligne =~ m/^.{158}(\w{2})/s
            && oe_trt_ref_rec( $1, $ligne, 168, 3034 ) )
        {
            # FIN traitement enregistrement(s) : I5 I6

        }
        else {
            # SI AUCUN TYPE D'ENREGISTREMENT N'EST RECONNU
            warn "INFO : IGNORE REC. line $.\n";

            # POUR NE PAS SURCHARGER LE FLUX DE SORTIE ON PURGE LA LIGNE
            # LES ERREURS SUR STDOUT SONT CAPTUREES PAR LA CHAINE DE PRODUCTION
        }
    }
    $TRK->track( 'Doc', 1, '', 'SIEGE' );
    &finDoc;
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################

sub initApp() {

    # CARTOGRAPHIE APPLICATIVE v 20070406-094002

    $evalSsTrt{'I5'}[0] =
      \&finDoc;    # QUEL ENREGISTREMENT REINITIALISE LE DOCUMENT ?
    $motifs{'I5'} = 'A02 A09 A15 A06 A15 A5 A03 A08 A08 A08 A08 A02 A08 A*';
    $evalSsTrt{'I5'}[1] = \&prepI5;

    $motifs{'I6'} =
'A09 A15 A15 A06 A50 A5 A03 A50 A14 A14 A14 A1 A50 A4 A03 A50 A08 A02 A08 A08 A02 A50 A02 A02 A50 A05 A09 A09 A09 A50 A8 A10 A50 A20 A8 A10 A50 A20 A*';
    $evalSsTrt{'I6'}[1] = \&prepI6;

    &initXls;
    1;
}

sub initXls {

    #	my $fichier=$ARGV[1];

    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ; NR7 = Numérique Right...  )
    prod_Xls_Col_Init(
        'AC10', 'AC10', 'AC10', 'AC5',  'AC10', 'AC10', 'AC12', 'AC10',
        'AC10', 'AC7',  'AC10', 'AL40', 'NR12'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init( "", "LISTE DES ADHERENTS NON SOLDES" );  #  ,"ENTETE DROIT");

# INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
# PRÉPARATION DES TITRES DE COLONNES
# AGENCE; CONTRAT; COL; GROUPE; CIV; NOM ; DATNAI; PRéNOM ; NOV; BTQ; NTV; LBV; AD2;  LOC; CPS; BDS
    prod_Xls_Insert_Val("Num. Contrat");
    prod_Xls_Insert_Val("Num. Groupe");
    prod_Xls_Insert_Val("Entité de rattach.");
    prod_Xls_Insert_Val("Code Part");
    prod_Xls_Insert_Val("Début échéance");
    prod_Xls_Insert_Val("Fin échéance");
    prod_Xls_Insert_Val("Mode sélect. Date Encaiss.");
    prod_Xls_Insert_Val("Début Encaiss.");
    prod_Xls_Insert_Val("Fin Encaiss.");
    prod_Xls_Insert_Val("Mode Regl.");
    prod_Xls_Insert_Val("N°Contrat ind.");
    prod_Xls_Insert_Val("Nom - Prénom");
    prod_Xls_Insert_Val("Solde Individuel");

    prod_Xls_Row_Height(32);

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );
    1;
}

sub init_I5_values() {
    $EE01002_EE017C01_I5_NUMCCO        = "";
    $EE01003_EE017C01_I5_GRPASS        = "";
    $EE01007_EE017C01_I5_DATECH_DEB    = "";
    $EE01008_EE017C01_I5_DATECH_FIN    = "";
    $EE01011_EE017C01_I5_MODSEL_DATENC = "";
    $EE01009_EE017C01_I5_DATENC_DEB    = "";
    $EE01010_EE017C01_I5_DATENC_FIN    = "";
    1;
}

sub init_I6_values() {
    $EE0006a_EE017C01_I6_CODPART       = "";
    $EE0002a_EE017C01_I6_NUMCIN        = "";
    $CIVI035_EE017C01_I6_CIVILITE_SOUS = "";
    $EE01036_EE017C01_I6_NOM_SOUS      = "";
    $EE01036_EE017C01_I6_MODE_REG      = "";
    $EE01037_EE017C01_I6_PRENOM_SOUS   = "";
    $EE0010a_EE017C01_I6_MTT_SOLDE     = "";
    $I6_ER                             = "";
    1;
}

sub finDoc() {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    if ( $compteurI5 gt 0 ) {
        prod_Xls_Insert_Val(
            $EE01002_EE017C01_I5_NUMCCO, $EE01003_EE017C01_I5_GRPASS,
            $I6_ER,                      "",
            "",                          "",
            "",                          "",
            "",                          "",
            "",                          "Montant total général",
            $total
        );
        prod_Xls_Edit_Ligne('T2');
    }
    $compteurI5++;
    $total = 0;
    1;
}

sub prepI5() {
    &init_I5_values();
    &init_I6_values();

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT I5
    oe_num_sign_x( $DATATAB[5], 0 );    #	EE01005 => EE017C01_I5_NUMPART

#FORMATAGE DES DATES DE DEBUT ET FIN D ECHEANCE ET D ENCAISSEMENT AU FORMAT (JJ/MM/AAAA)
    oe_to_date( $DATATAB[7] );
    oe_to_date( $DATATAB[8] );
    oe_to_date( $DATATAB[9] );
    oe_to_date( $DATATAB[10] );

    $EE01002_EE017C01_I5_NUMCCO        = $DATATAB[2];
    $EE01003_EE017C01_I5_GRPASS        = $DATATAB[3];
    $EE01007_EE017C01_I5_DATECH_DEB    = $DATATAB[7];
    $EE01008_EE017C01_I5_DATECH_FIN    = $DATATAB[8];
    $EE01011_EE017C01_I5_MODSEL_DATENC = $DATATAB[11];
    $EE01009_EE017C01_I5_DATENC_DEB    = $DATATAB[9];
    $EE01010_EE017C01_I5_DATENC_FIN    = $DATATAB[10];
    1;
}

sub prepI6() {
    &init_I6_values();

    # PRÉPARATION DES DONNÉES DE L'ENREGISTREMENT I6
    oe_num_sign_x( $DATATAB[8],  2 );    #	EE0008a => EE017C01_I6_MTT_DETTES
    oe_num_sign_x( $DATATAB[9],  2 );    #	EE0009a => EE017C01_I6_MTT_ENCAISS
    oe_num_sign_x( $DATATAB[10], 2 );    #	EE0010a => EE017C01_I6_MTT_SOLDE

    #SOMME DES LIGNE I6 POUR LE TOTAL GENERAL
    $total += $DATATAB[10];

    $EE0006a_EE017C01_I6_CODPART = $DATATAB[6];
    $EE0002a_EE017C01_I6_NUMCIN  = $DATATAB[2];

    #	$CIVI035_EE017C01_I6_CIVILITE_SOUS=$DATATAB[35];
    $EE01036_EE017C01_I6_MODE_REG    = $DATATAB[23];
    $EE01036_EE017C01_I6_NOM_SOUS    = $DATATAB[36];
    $EE01037_EE017C01_I6_PRENOM_SOUS = $DATATAB[37];
    $EE0010a_EE017C01_I6_MTT_SOLDE   = $DATATAB[10];
    $I6_ER                           = $DATATAB[0];

    prod_Xls_Insert_Val(
        $EE01002_EE017C01_I5_NUMCCO,
        $EE01003_EE017C01_I5_GRPASS,
        $I6_ER,
        $EE0006a_EE017C01_I6_CODPART,
        $EE01007_EE017C01_I5_DATECH_DEB,
        $EE01008_EE017C01_I5_DATECH_FIN,
        $EE01011_EE017C01_I5_MODSEL_DATENC,
        $EE01009_EE017C01_I5_DATENC_DEB,
        $EE01010_EE017C01_I5_DATENC_FIN,
        $EE01036_EE017C01_I6_MODE_REG,
        $EE0002a_EE017C01_I6_NUMCIN,
        "$EE01036_EE017C01_I6_NOM_SOUS $EE01037_EE017C01_I6_PRENOM_SOUS",
        $EE0010a_EE017C01_I6_MTT_SOLDE
    );
    prod_Xls_Edit_Ligne;

    1;
}

exit main(@ARGV);
