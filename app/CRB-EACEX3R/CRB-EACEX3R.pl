#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;

my ( $TRK, $DOC, $GED, $VALS );
my $TYPECOUR;
my @DADDR;
my $AGENCE;
my $CIV;
my $CIVILITE;
my $NOMDEST;
my $NUMCTR;
my $NUMABA;
my $xNOMDEST;
my $GESTIONNAIRE;
my $EMETTEUR;
my $CODCENTRE;
my $LIBCENTRE;
my $OPT_GED;
my $DtEDITION;
my $DtACTE;
my $DtCOUR;
my $stop = 0;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys =>
          [ 'NUM_CTR', 'NOM_DEST', 'EMETTEUR', 'CODE_COURRIER', 'NUM_ABA' ]
    );
    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # 	$E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # 	$E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    # 	$E7->debug();
    $E7->bind_all_c7();

    my $H4 = oEUser::Descriptor::H4->get();

    # 	$H4->debug();
    $H4->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();
    my $LISTE_BENEF_RAD = oEdtk::TexDoc->new();
    my $DELAIS_STAGE_AJ = oEdtk::TexDoc->new();

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'H4' => $H4,
        'Z1' => $Z1
    );
    $P->set_motif_to_denormalized_split('\x{01}');

    my $id;

    #   warn"id record = $id     \n";
    while ( ( $id, $VALS ) = $P->next() ) {

        #      warn"id record = $id     \n";
        if ( $id eq 'E1' ) {

            #       	warn "INFO : dump E1 ". Dumper($VALS) ."\n";

            $DOC->append( 'DEBUT', '' );
            $NOMDEST =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            #####GESTION DE LA CIVILITE ENTRE TIERS ET ADHERENT#########

            # 				$CIVILITE = $VALS->{'LICCODC'};
            my $civDest = $VALS->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );
            $CIVILITE = $VALS->{'PENOCOD'};
            if ( $CIVILITE eq 'M' ) {
                $DOC->append( 'CIVILITE', 'Cher adhérent' );
            }
            elsif ( $CIVILITE eq 'MME' || $CIVILITE eq 'MLLE' ) {
                $DOC->append( 'CIVILITE', 'Chère adhérente' );
            }
            else {
                $DOC->append( 'CIVILITE', 'Chère adhérente, Cher adhérent' );
            }

            $EMETTEUR = $VALS->{'ODERCOD'};
            if ( $EMETTEUR eq '' ) {
                $CIV      = 0;
                $EMETTEUR = "ER";
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }
            else {
                $CIV = 1;
                $DOC->append( 'EMETTEUR', $EMETTEUR );
            }

            $DOC->append( 'CIV', $CIV );

            $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";
            $DOC->append( 'xNOMDEST', $xNOMDEST );

            ################CENTRE DE GESTION###############################
            $CODCENTRE = $VALS->{'ODCGCOD'};

            ################ADRESSE EMETTEUR#########################
            $AGENCE = $VALS->{'PENO027'};
            $DOC->append( 'AGENCE', $VALS->{'PENO027'} );

            my $adresse =
                $VALS->{'PEAD030'} . ' '
              . $VALS->{'LICN034'} . ' '
              . $VALS->{'LIBV035'};
            $DOC->append( 'ADRESSE', $adresse );

            $DOC->append( 'BOITPOS', $VALS->{'BOIP037'} );

            $DOC->append( 'CODPOS', $VALS->{'CODC039'} );

            my $localite = ( $VALS->{'LIBL038'} );
            $DOC->append( 'LOCALITE', $localite );
            $DOC->append('BASDEPAGE');

            ######INDEXATION GED###################
            #				$GED->append('xIDEMET', $EMETTEUR);  #-	Emetteur
            $GED->append( 'xNOMDEST', $xNOMDEST );        #-	Nom du destinataire
            $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
              ;    #-	Ville du destinataire

            #         warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E2' ) {

            #        warn "INFO : dump E2 ". Dumper($VALS) ."\n";
            $DtEDITION =
              convertDate( $VALS->{'DATEDTx'} );   #Date du Traitement d'Edition
            $DOC->append( 'DATEEDT', $DtEDITION );

            #########NUMERO CONTRAT INDIVIDUEL############
            $NUMCTR = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMCTR', $NUMCTR );

            ###########CODE COURRIER###################
            $TYPECOUR = $VALS->{'CDCOURx'};        #CODE COURRIER
            if ( $TYPECOUR eq 'ANI-1' ) {
                $DOC->append( 'OBJETUN',
                    'Réponse à votre demande de radiation' );
                $DOC->append( 'OBJETDEUX', 'de bénéficiaire(s)' );
            }
            elsif ( $TYPECOUR eq 'ANI-2' ) {
                $DOC->append( 'OBJETUN',
                    'Réponse à votre demande de résiliation' );
                $DOC->append( 'OBJETDEUX', '' );
            }
            elsif ( $TYPECOUR eq 'RES13' ) {
                $DOC->append( 'OBJETUN',
                    'Réponse à votre demande de résiliation' );
                $DOC->append( 'OBJETDEUX', '' );
            }
            else {

            }

            $DtACTE = convertDate( $VALS->{'ACOPDDE'} )
              ;    #DATE D'EFFET DE L'ACTE DE GESTION
            calculAnnee();
            courrier();

            ######INDEXATION GED###################
            $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
              ;    #-	Numéro de contrat collectif
            $GED->append( 'xCLEGEDi', $NUMCTR )
              ;    #-	Numéro de contrat individuel

            $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
            $GED->append( 'xIDEMET', $GESTIONNAIRE )
              ; #-	edms.pm prends le champs xIDEMET si xIDEMET= numero agence sinon il prends xSOURCE
            $GED->append( 'xSOURCE', 'BUR' )
              ; #l'exploitation prends cette valeur pour mettre dans le file gestionnaire de DOCUBASE
            $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #-	Gestionnaire
            $GED->append( 'xCLEGEDiv', $DtEDITION )
              ;    #Date du Traitement d'Edition

            #         warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E7' ) {
            ############BLOC ADRESSE DESTINATAIRE##################
            my @DADDR = ();
            push( @DADDR, $NOMDEST );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
            );
            push( @DADDR, $VALS->{'BOIPOSL'} );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $VALS->{'LICCODP'} );
            $VALS->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $DOC->append_table( 'DADDR', @DADDR );

            # 				warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";

        }

        if ( $id eq 'OPTION' ) {
            if ( $stop eq 0 ) { $DOC->append( 'LABEL', 0 ); }

            #         warn "INFO : dump OPTION ". Dumper($VALS) ."\n";

            $DtCOUR = $VALS->[1]
              ;    #DATE réception du courrier de résiliation de l'adhérent
            $DtCOUR =~ s/^\s+//;   #supprime les espaces en début de chaine
            $DtCOUR =~ s/\s+$//;   #en fin de chaine
            $DtCOUR =~ s/\s+/ /;   #remplac²e des séries d'espaces par un seul

            if ( $DtCOUR eq '' ) {
                die
"\nERROR:Date de réception (position 1) non trouvé un courrier "
                  . $TYPECOUR
                  . " avec une date courrier vide\n\n";
            }
            else {
                my $dtRESULIATION =
                  $VALS->[2];      #DATE COURRIER SAISIE PAR LE GESTIONNAIRE
                $dtRESULIATION =~
                  s/^\s+//;        #supprime les espaces en début de chaine
                $dtRESULIATION =~ s/\s+$//;    #en fin de chaine
                $dtRESULIATION =~
                  s/\s+/ /;    #remplace des séries d'espaces par un seul
                if ( ( $TYPECOUR eq 'ANI-1' ) or ( $TYPECOUR eq 'ANI-2' ) ) {
                    $DOC->append( 'DATECOUR', $DtCOUR );
                }
                elsif ( ( $TYPECOUR eq 'RES13' ) and ( $dtRESULIATION ne '' ) )
                {
                    $DOC->append( 'DATECOUR',  $DtCOUR );
                    $DOC->append( 'DATERESUL', $dtRESULIATION );
                }
                else {
                    die
"\nERROR:Date de radiation (position 2) non trouvé un courrier "
                      . $TYPECOUR
                      . " avec une date courrier vide\n\n";
                }

            }

            if ( $CODCENTRE =~ /MNT/ ) {
                $DOC->append( 'ER', 'MNT' );
            }
            else {
                $DOC->append( 'ER', 'Mutacité' );
            }

        }
        if ( $id eq 'H4' ) {

#          warn "INFO : dump H4 ". Dumper($VALS) ."\n";
# contrat labellisée uniquement pour MNT si au moins une garantie EV ou ES dont la date de radiation vide
            if (
                ( $CODCENTRE =~ /MNT/ )
                and (
                       ( $VALS->{'ACH4COD'} eq 'CBNEV' )
                    or ( $VALS->{'ACH4COD'} eq 'CBNES' )

                    # JLE 28/07/2017 mise a jour regle de gestion
                    or ( $VALS->{'ACH4COD'} =~ /MNT[1-9]{1}/i )
                )
                and ( $VALS->{'ACH4010'} eq '' )
                and ( $stop eq 0 )
              )
            {
                $DOC->append( 'LABEL', 1 );
                $stop = 1;

                # ecriture test
                # open (my $desc,'>>','c:/ACH4COD.txt');
                # print $desc $VALS->{'ACH4COD'}."\n";
                # close $desc;
            }

        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $VALS->{'PERF003'} );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $VALS->{'PERF003'} )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

    }

    $DOC->append('COURRIER');
    $DOC->append('FINDOC');

    #   warn "INFO : dump DOC ". Dumper($DOC) ."\n";
    #   warn "INFO : dump GED ". Dumper($GED) ."\n";
    #   die();

    $TRK->track( 'Doc', 1, $NUMCTR, $xNOMDEST, $EMETTEUR, $TYPECOUR, $NUMABA );
    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);
    $DOC->reset();
    $GED->reset();
    oe_compo_link($OPT_GED);
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub calculAnnee {
    my @tab = split( '/', $DtEDITION );

    # warn "date edition = $DtEDITION";
    my $ANNEE  = $tab[2];
    my $ANNEE1 = $ANNEE + 1;

#  si date d'émission = novembre ou decembre alors annee = annee +1 et annee1 = annee +2
    if (
        (
               $TYPECOUR eq 'RES13'
            || $TYPECOUR eq 'ANI-1'
            || $TYPECOUR eq 'ANI-2'
        )
        && ( $tab[1] == 12 || $tab[1] == 11 )
      )
    {
        $ANNEE  = $ANNEE + 1;
        $ANNEE1 = $ANNEE + 1;
    }

    $DOC->append( 'ANNEE',   $ANNEE );
    $DOC->append( 'ANNEEUN', $ANNEE1 );
}

sub courrier {

#Retour d'homologation sur la sésame 1129769
#Les RES 1,2,3 ne seront pas traités en production pour le moment.
#Ils sont déparamétrés par Viviane et commenté par nous en attendant une adaptation juridique.
    if ( $TYPECOUR eq 'RES04' ) {

        #     $DOC->append ('LETTRE', {'RESIV'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'RES05' ) {

        #     $DOC->append ('LETTRE', {'RESV'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'RES06' ) {

        #     $DOC->append ('LETTRE', {'RESVI'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'RES07' ) {

        #     $DOC->append ('LETTRE', {'RESVII'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'RES08' ) {

        #     $DOC->append ('LETTRE', {'RESVIII'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'RES09' ) {

        #     $DOC->append ('LETTRE', {'RESIX'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'RES10' ) {

        #     $DOC->append ('LETTRE', {'RESX'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";

        #     $DOC->append ('AfficheNote','Oui');
    }
    elsif ( $TYPECOUR eq 'RES11' ) {

        #     $DOC->append ('LETTRE', {'RESXI'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'RES12' ) {

        #     $DOC->append ('LETTRE', {'RESXII'});
        die "\nERROR:CODE COURRIER INCONNU POUR CETTE APPLICATION - "
          . $TYPECOUR . "\n\n";
    }
    elsif ( $TYPECOUR eq 'ANI-1' ) {
        $DOC->append( 'xTYPDOC', 'CREA' );
        $DOC->append( 'LETTRE', {'ANIUN'} );
    }
    elsif ( $TYPECOUR eq 'ANI-2' ) {
        $DOC->append( 'xTYPDOC', 'CRRE' );
        $DOC->append( 'LETTRE', {'ANIDEUX'} );
    }
    elsif ( $TYPECOUR eq 'RES13' ) {
        $DOC->append( 'xTYPDOC', 'CRRE' );
        $DOC->append( 'LETTRE', {'RESTREIZE'} );
    }
    else {
        die "\nERROR:CODE COURRIER INCONNU - " . $TYPECOUR . "\n\n";
    }
}

exit main(@ARGV);
