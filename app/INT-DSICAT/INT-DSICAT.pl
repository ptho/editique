use utf8;
use strict;
use warnings;

use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use oEUser::XML::Generic;
use Data::Dumper;
use Encode;

my $TRK;

sub edit_contrat {
    my ( $data, $doc ) = @_;

    my ( $numContrat, $nomSociete, $numAppelOffre );
    if ( defined( $data->{'variables'} ) ) {
        my $variables = $data->{'variables'};

        $$doc->append( 'NUMCONTRAT', $variables->{'num_contrat'} );
        $numContrat = $variables->{'num_contrat'};
        $$doc->append( 'NOMSOCIETE', $variables->{'nom_societe'} );
        $nomSociete = $variables->{'nom_societe'};
        $$doc->append( 'NOMVOIESOCIETE', $variables->{'nom_voie_societe'} );
        $$doc->append( 'NUMVOIESOCIETE', $variables->{'num_voie_societe'} );
        $$doc->append( 'CAPITALSOCIETE', $variables->{'capital_societe'} );
        $$doc->append( 'NOMRESPSOCIETE',
            $variables->{'nom_responsable_societe'} );
        $$doc->append( 'QUALITERESPSOCIETE',
            $variables->{'qualite_responsable_societe'} );
        $$doc->append( 'PRENOMRESPSOCIETE',
            $variables->{'prenom_responsable_societe'} );
        $$doc->append( 'CODEPOSTSOCIETE', $variables->{'code_postal_societe'} );
        $$doc->append( 'VILLESOCIETE', $variables->{'ville_societe'} || "" );
        $$doc->append( 'STATUTJURSOCIETE',
            $variables->{'statut_juridique_societe'} );
        $$doc->append( 'NUMRCS',          $variables->{'num_rcs'} );
        $$doc->append( 'PRENOMDIRECTEUR', $variables->{'prenom_directeur'} );
        $$doc->append( 'NOMDIRECTEUR',    $variables->{'nom_directeur'} );
        $$doc->append( 'NUMAPPELOFFRE',   $variables->{'num_appel_offre'} );

        if ( $numAppelOffre == " " ) {
            $numAppelOffre = '';
        }
        else { $numAppelOffre = $variables->{'num_appel_offre'}; }
        $$doc->append( 'FONCTIONDIRECTEUR',
            $variables->{'fonction_directeur'} );
        $$doc->append( 'LIBELLEDIRECTION', $variables->{'libelle_direction'} );
        $$doc->append( 'VILLERCS',         $variables->{'ville_rcs'} );

        $$doc->append('fincontrat');    ### Fin du contrat
    }

    my $numAvenant;
    my $numContratAvenant;
    my $nomSocieteAvenant;
    if ( $data->{'avenants'} ne "" ) {
        if ( defined( $data->{'avenants'}->{'avenant'} ) ) {
            if ( ref( $data->{'avenants'}->{'avenant'} ) eq "ARRAY" )
            {                           ### Il y a plusieurs avenants à éditer
                foreach my $key ( @{ $data->{'avenants'}->{'avenant'} } )
                {                       ### Pour chaque avenant
                    ( $numAvenant, $numContratAvenant, $nomSocieteAvenant ) =
                      edit_avenant( $key, $doc );
                }
            }
            else {                      ### Il n'y a qu'un avenant
                ( $numAvenant, $numContratAvenant, $nomSocieteAvenant ) =
                  edit_avenant( $data->{'avenants'}->{'avenant'}, $doc );
            }
        }
    }

    if ( !defined($numContrat) ) {
        $numContrat    = $numContratAvenant;
        $nomSociete    = $nomSocieteAvenant;
        $numAppelOffre = '';
    }
    if ( !defined($numAvenant) ) {
        $numAvenant = '';
    }

    #'NUMCONTRAT','MISSION','NOMDEST','NUMAVENANT','NUMOFFRE'
    $TRK->track( 'Doc', 1, $numContrat, $nomSociete, $numAvenant,
        $numAppelOffre );
}

sub edit_avenant {
    my ( $data, $doc ) = @_;
    my $variables = $data->{'variables'};
    $$doc->append( 'PRENOMRESPMNT', $variables->{'prenom_responsable_mnt'} );
    $$doc->append( 'NOMRESPMNT',    $variables->{'nom_responsable_mnt'} );

    $$doc->append( 'QUALIFPRESTA',
        $variables->{'qualification_presta'} || " " );
    $$doc->append( 'PRENOMDIRECTEUR', $variables->{'prenom_directeur'} );
    $$doc->append( 'NOMDIRECTEUR',    $variables->{'nom_directeur'} );
    $$doc->append( 'FONCTIONDIRECTEUR',
        $variables->{'fonction_directeur'} || " " );
    $$doc->append( 'OBJETMISSION', $variables->{'objet_mission'} );
    $$doc->append( 'NUMAVENANT',   $variables->{'num_avenant'} );
    my $numAvenant = $variables->{'num_avenant'};
    my $numContrat = $variables->{'num_contrat'};
    $$doc->append( 'NBRJOURSMISSION', $variables->{'nbre_jour_mission'} );
    $$doc->append( 'NBRJOURSSEMAINE', $variables->{'nbre_jour_semaine'} );
    $$doc->append( 'FONCTIONRESPMNT',
        $variables->{'fonction_responsable_mnt'} || " " );
    $$doc->append( 'LIEUMISSION', $variables->{'lieu_mission'} || " " );
    $$doc->append( 'NOMSOCIETE',  $variables->{'nom_societe'}  || " " );
    $$doc->append( 'NOMRESPSOCIETE',
        $variables->{'nom_responsable_societe'} || " " );
    $$doc->append( 'QUALITERESPSOCIETE',
        $variables->{'qualite_responsable_societe'} || " " );
    $$doc->append( 'PRENOMRESPSOCIETE',
        $variables->{'prenom_responsable_societe'} || " " );
    $$doc->append( 'NOMSOCIETE',    $variables->{'nom_societe'}    || " " );
    $$doc->append( 'NOMCOMMERCIAL', $variables->{'nom_commercial'} || " " );
    $$doc->append( 'PRENOMCOMMERCIAL',
        $variables->{'prenom_commercial'} || " " );
    my $nomSociete = $variables->{'nom_societe'};

    my $conditionDoc = oEdtk::TexDoc->new();
    if ( $data->{'conditionspecs'} ne "" )
    {    ### Il y a un ou des avenants à éditer
        if ( defined( $data->{'conditionspecs'}->{'conditionspec'} ) ) {
            if (
                ref( $data->{'conditionspecs'}->{'conditionspec'} ) eq "ARRAY" )
            {    ### Il y a plusieurs avenants à éditer
                foreach
                  my $key ( @{ $data->{'conditionspecs'}->{'conditionspec'} } )
                {    ### Pour chaque avenant
                    edit_conditionsspec( $key, \$conditionDoc );
                }
            }
            else {    ### Il n'y a qu'un avenant
                edit_conditionsspec(
                    $data->{'conditionspecs'}->{'conditionspec'},
                    \$conditionDoc );
            }
        }
    }
    $$doc->append( 'conditions', $conditionDoc );

    my $tarifs = oEdtk::TexDoc->new();
    if ( $data->{'tarifs'} ne "" ) {    ### Il y a un ou des avenants à éditer
        if ( defined( $data->{'tarifs'}->{'tarif'} ) ) {
            if ( ref( $data->{'tarifs'}->{'tarif'} ) eq "ARRAY" )
            {                           ### Il y a plusieurs avenants à éditer
                foreach my $key ( @{ $data->{'tarifs'}->{'tarif'} } )
                {                       ### Pour chaque avenant
                    edit_tarif( $key, \$tarifs );
                }
            }
            else {                      ### Il n'y a qu'un avenant
                edit_tarif( $data->{'tarifs'}->{'tarif'}, \$tarifs );
            }
        }
    }
    $$doc->append( 'tarifs', $tarifs );
    $$doc->append('finavenant');        ### Fin de l'avenant
    return ( $numAvenant, $numContrat, $nomSociete );
}

sub edit_tarif {
    my ( $data, $doc ) = @_;
    $$doc->append( 'DEBPERIODE',    $data->{'debut_periode'} );
    $$doc->append( 'FINPERIODE',    $data->{'fin_periode'} );
    $$doc->append( 'TARIFCHIFFRES', $data->{'prix_ht'} );
    $$doc->append( 'TARIFLETTRES',  $data->{'prix_ht_lettre'} );
    $$doc->append('editTarif');
}

sub edit_conditionsspec {
    my ( $data, $doc ) = @_;

    # Suivant la condition.
  SWITCH: {
        $data eq '1' && do { $$doc->append('conditionI'); last SWITCH; };
    }
}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'Web Editiq',
        keys   => [ 'NUMCONTRAT', 'NOMDEST', 'NUMAVENANT', 'NUMOFFRE' ]
    );
    oe_new_job('--index');

    #	oe_new_job("--input_code=utf8", "--index");

    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'COPR' );    ### Type de document
    my $data = xml_open( \*IN );
    print Dumper($data);

    if ( ref( $data->{'contrat'} ) eq "ARRAY" )
    {    ### Il y a plusieurs contrats à éditer
        foreach my $key ( @{ $data->{'contrat'} } ) {    ### Pour chaque contrat
            edit_contrat( $key, \$doc );
        }
    }
    else {    ### Il n'y a qu'un contrat
        edit_contrat( $data->{'contrat'}, \$doc );
    }

    $doc->append('findoc');

    oe_print_to_output_file($doc);

    oe_compo_link();
    return 0;
}

exit main(@ARGV);
