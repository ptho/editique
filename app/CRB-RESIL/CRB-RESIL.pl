#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;

my ( $TRK, $DOC, $GED, $VALS );
my $SOURCE;
my $ACTEGEST;
my $CODECONTEXTE;
my $NOMDEST;
my @DADDR;
my $NUMCTR;
my $NUMABA;
my $xNOMDEST;
my $EMETTEUR;
my $OPT_GED;
my $LabelBool = 0;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';
    $TRK     = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMCTR', 'NUMABA', 'NOMDEST', 'EMETTEUR', 'ACTEGESTION' ]
    );

    oe_new_job('--index');

    my $E1 = oEUser::Descriptor::E1->get();

    # $E1->debug();
    $E1->bind_all_c7();

    my $E2 = oEUser::Descriptor::E2->get();

    # $E2->debug();
    $E2->bind_all_c7();

    my $E7 = oEUser::Descriptor::E7->get();

    # $E7->debug();
    $E7->bind_all_c7();

    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();

    my $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'Z1' => $Z1
    );

    $DOC->append( 'xTYPDOC', 'CRCI' );    ### Type de document

    my $id;

    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {

            # warn "INFO : dump E1 ". Dumper($VALS) ."\n"; die();
            my $civDest = $VALS->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );
            $DOC->append( 'DEBUT', '' );
            $NOMDEST =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            #####GESTION DE LA CIVILITE ENTRE TIERS ET ADHERENT#########
            my $CIVILITE = $VALS->{'LICCODC'};
            $DOC->append( 'CIVILITE', $CIVILITE );

            my $CIV;
            $EMETTEUR = $VALS->{'ODERCOD'};
            if ( $EMETTEUR eq '' ) {
                $CIV      = 0;
                $EMETTEUR = "ER";
            }
            else {
                $CIV = 1;
            }

            $DOC->append( 'CIV', $CIV );

            $xNOMDEST = "$VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

            ################CENTRE DE GESTION###############################
            my $CODCENTRE = $VALS->{'ODCGCOD'};
            my $LIBCENTRE;

            if ( $CODCENTRE eq 'MNT-RC' ) {
                $LIBCENTRE = 'Complémentaire Santé';
                $SOURCE    = 'SANTE';
            }
            else {
                $LIBCENTRE = 'Prévoyance';
                $SOURCE    = 'PREV';
            }
            $DOC->append( 'LIBCENTRE', $LIBCENTRE );
            $DOC->append( 'xSOURCE',   $SOURCE );      #-	Domaine métier

            ################ADRESSE EMETTEUR#########################
            my $AGENCE = $VALS->{'PENO027'};
            $DOC->append( 'AGENCE', $VALS->{'PENO027'} );
	    $DOC->append( 'xNOM_AGENCE', $VALS->{'PENO027'} );
	    
            my $adresse =
                $VALS->{'PEAD030'} . ' '
              . $VALS->{'LICN034'} . ' '
              . $VALS->{'LIBV035'};
            $DOC->append( 'ADRESSE', $adresse );
	    $DOC->append( 'xADRESSE_AGENCE', $adresse . ' - ' . $VALS->{'CODC039'} . ' ' . $VALS->{'LIBL038'});

            $DOC->append( 'BOITPOS', $VALS->{'BOIP037'} );
            $DOC->append( 'CODPOS',  $VALS->{'CODC039'} );

            my $localite = ( $VALS->{'LIBL038'} );
            $DOC->append( 'LOCALITE', $localite );
            $DOC->append('BASDEPAGE');

            ######INDEXATION GED###################
            $GED->append( 'xSOURCE',  $SOURCE );          #-	Domaine métier
            $GED->append( 'xIDEMET',  $EMETTEUR );        #-	Emetteur
            $GED->append( 'xNOMDEST', $xNOMDEST );        #-	Nom du destinataire
            $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} )
              ;    #-	Ville du destinataire
               # $GED->append('xCPDEST', oe_iso_country() . $VALS->{'CODCDXC'});
               # $GED->append('xOWNER', 'BUR');  #-	Domaine de métier

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E2' ) {

            # warn "INFO : dump E2 ". Dumper($VALS) ."\n"; die();

            my $DATEACT = convertDate( $VALS->{'ACOPDDE'} )
              ;    #DATE D'EFFET DE L'ACTE DE GESTION
            my $DATEEDT =
              convertDate( $VALS->{'DATEDTx'} );   #Date du Traitement d'Edition

            $DOC->append( 'DATEACT', $DATEACT )
              ;    #DATE D'EFFET DE L'ACTE DE GESTION
            $DOC->append( 'DATEEDT', $DATEEDT );   #Date du Traitement d'Edition

            #########NUMERO CONTRAT INDIVIDUEL############
            $NUMCTR = $VALS->{'ACH1NUM'};
            $DOC->append( 'NUMCTR', $NUMCTR );

            #############INDEXATION GED###################
            $GED->append( 'xIDDEST', $VALS->{'ACCCNUM'} )
              ;                                    #Numéro de contrat collectif
            $GED->append( 'xCLEGEDi', $NUMCTR );  #Numéro de contrat individuel

            my $GESTIONNAIRE = $VALS->{'PENO083'} . ' ' . $VALS->{'PENO084'};
            $GED->append( 'xCLEGEDii', $GESTIONNAIRE );    #Gestionnaire
              # $GED->append('xCLEGEDiii',$DATEEDT); #Date du Traitement d'Edition
            $GED->append( 'xCLEGEDiv', $DATEACT )
              ;    #DATE D'EFFET DE L'ACTE DE GESTION

            ###########CODE COURRIER ###################
            $ACTEGEST     = $VALS->{'ACOPCOD'};    #Code acte de gestion
            $CODECONTEXTE = $VALS->{'ACOP130'};    #Code contexte

            if ( $ACTEGEST eq 'ACRE' ) {
                if ( $CODECONTEXTE eq 'P-DL' && $SOURCE eq 'PREV' ) {
                    $DOC->append( 'LETTRE', {'PDL'} );    #PREV-Sans motif

                }
                elsif ( $CODECONTEXTE eq 'P-CO' && $SOURCE eq 'PREV' ) {
                    $DOC->append( 'LETTRE', {'PCO'} ); #PREV-Départ concurrence

                }
                elsif ( $CODECONTEXTE eq 'P-ME' && $SOURCE eq 'PREV' ) {
                    $DOC->append( 'LETTRE', {'PME'} );    #PREV-Mécontentement

                }
                elsif ( $CODECONTEXTE eq 'P-VP' && $SOURCE eq 'PREV' ) {
                    $DOC->append( 'LETTRE', {'PVP'} )
                      ;    #PREV-Modification de vie professionnelle

                }
                elsif ( $CODECONTEXTE eq 'S-DL' && $SOURCE eq 'SANTE' ) {
                    $DOC->append( 'LETTRE', {'SDL'} );    #SANTE-Sans motif

                }
                elsif ( $CODECONTEXTE eq 'S-CO' && $SOURCE eq 'SANTE' ) {
                    $DOC->append( 'LETTRE', {'SCO'} )
                      ;    #SANTE-Départ concurrence

                }
                elsif ( $CODECONTEXTE eq 'S-ME' && $SOURCE eq 'SANTE' ) {
                    $DOC->append( 'LETTRE', {'SME'} );    #SANTE-Mécontentement

                }
                elsif ( $CODECONTEXTE eq 'S-VP' && $SOURCE eq 'SANTE' ) {
                    $DOC->append( 'LETTRE', {'SVP'} )
                      ;    #SANTE-Modification de vie professionnelle

                }
                elsif ( $CODECONTEXTE eq 'S-CM' && $SOURCE eq 'SANTE' ) {
                    $DOC->append( 'LETTRE', {'SCM'} );  #SANTE-Fin de droits CMU

                }
                else {
                    die
"\nERROR : CODE CONTEXTE -$CODECONTEXTE- POUR LE DOMAINE METIER -$SOURCE-\n\n";
                }

            }
            else {
                die "\nERROR - CODE ACTE DE GESTION INCONNU : $ACTEGEST\n\n";
            }

            ########CODE CONTRAT##########
            my $CODECTR = substr $VALS->{'ACCCNUM'}, -3;

            # warn "INFO : CODECTR - ". $VALS->{'ACCCNUM'} ."\n";
            # warn "INFO : CODECTR - ". $CODECTR ."\n";
            if ( $CODECTR eq 'PCL' ) {
                $LabelBool = 1;
            }

            # warn "INFO : dump DOC ". Dumper($DOC) ."\n";   die();
        }

        if ( $id eq 'E7' ) {
            ############BLOC ADRESSE DESTINATAIRE##################
            my @DADDR = ();
            push( @DADDR, $NOMDEST );
            push( @DADDR, $VALS->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}"
            );
            push( @DADDR, $VALS->{'BOIPOSL'} );
            push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $VALS->{'LICCODP'} );
            $VALS->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $VALS->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $DOC->append_table( 'DADDR', @DADDR );

            # warn "INFO : dump DADDR ". Dumper(@DADDR) ."\n";
        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }
    }

    $DOC->append( 'CODECTR', $LabelBool )
      ;            #Labellisation - CODE CONTRAT SE TERMINE PAR PCL

    $DOC->append('COURRIER');
    $DOC->append('FINCOURRIER');

    # $GED->append('xSOURCE', 'SANTE'); #-	Domaine métier
    # $GED->append('xOWNER', 'BUR');    #-	Domaine de métier

    # warn "\n\nINFO : CODECONTEXTE ". $CODECONTEXTE ."\n\n\n";
    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";
    # warn "INFO : dump GED ". Dumper($GED) ."\n";
    # die();

    $TRK->track( 'Doc', 1, $NUMCTR, $NUMABA, $xNOMDEST, $EMETTEUR,
        $ACTEGEST . "/" . $CODECONTEXTE );

    oe_print_to_output_file($GED);
    oe_print_to_output_file($DOC);

    $DOC->reset();
    $GED->reset();
    oe_compo_link($OPT_GED);
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

exit main(@ARGV);
