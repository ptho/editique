#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls 0.46;
use oEdtk::Tracking;
use strict;

# DECLARATION DES VARIALBES GLOBALES DE L'APPLICATION :
my ($TRK);

# CORPS PRINCIPAL DE L'APPLICATION :

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'IDDEST', 'AGENCE' ]
    );
    oe_open_fi_IN( $argv[0] );

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    initApp();

    # INITIALISATION PROPRE AU DOCUMENT
    initXls();

    while ( my $ligne = <IN> ) {
        chomp($ligne);

        # APPELS DES FONCTIONS v 20061128-161731
        if ( $ligne =~ m/^.{158}(\w{2})/s
            && oe_trt_ref_rec( $1, $ligne, 168, 3034 ) )
        {
            # FIN traitement enregistrement(s) : L9

        }
        else {
            # SI AUCUN TYPE D'ENREGISTREMENT N'EST RECONNU
            warn "INFO : IGNORE REC. line $.\n";
        }
    }
    $TRK->track( 'Doc', 1, '', 'SIEGE' );
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################
sub initApp() {
    oe_rec_motif( 'L9',
'A002 A002 A015 A009 A010 A015 A050 A010 A002 A003 A010 A015 A050 A009 A010 A015 A050 A010 A015 A050 A008 A013 A001 A050 A015 A008 A001 A050 A015 A015 A8 A2 A5 A010 A015 A050 A050 A008 A002 A100 A300 A009 A100 A002 A*'
    );
    oe_rec_process( 'L9', \&prepL9 );
    1;
}

sub prepL9() {
    prod_Xls_Insert_Val( $DATATAB[12] );
    prod_Xls_Insert_Val( $DATATAB[13] );
    prod_Xls_Insert_Val( $DATATAB[16] );
    prod_Xls_Insert_Val( $DATATAB[23] );
    prod_Xls_Insert_Val( $DATATAB[28] );
    prod_Xls_Insert_Val( $DATATAB[29] );
    prod_Xls_Insert_Val(
        "$DATATAB[31] $DATATAB[32] $DATATAB[33] $DATATAB[34] $DATATAB[35]");
    prod_Xls_Insert_Val( $DATATAB[36] );
    prod_Xls_Insert_Val( $DATATAB[40] );
    prod_Xls_Insert_Val( $DATATAB[41] );
    prod_Xls_Insert_Val( $DATATAB[42] );
    prod_Xls_Insert_Val( $DATATAB[43] );
    prod_Xls_Edit_Ligne();

    1;
}

# LES UTILISATEURS N'ONT PAS ENCORE DEMANDÉ À INTÉGRER LE FORMAT EXCEL
sub initXls() {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
#
# 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
# 	(AC7 = Alpha Center 7 de large , Ac7 = Alpha Center Wrap... , NR7 = Numérique Right...  )
#	(AL7 = Alpha Left 7, AR7 = Alpha Right 7)
    prod_Xls_Col_Init(
        'AL15', 'AC05', 'AL20', 'AL20', 'AL15', 'AC15',
        'AL35', 'Ac20', 'AL20', 'AC05', 'AL30', 'AC06'
    );
    #
    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    prod_Xls_Init( &oe_corporation_set(), "ETAT DES FUSIONS" )
      ;    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("CPAM");
    prod_Xls_Insert_Val("ER");
    prod_Xls_Insert_Val("AGENCE");
    prod_Xls_Insert_Val("NOM");
    prod_Xls_Insert_Val("PRENOM");
    prod_Xls_Insert_Val("No CONTRAT");
    prod_Xls_Insert_Val("38S");
    prod_Xls_Insert_Val("MOTIF");
    prod_Xls_Insert_Val("TRAITEMENT");
    prod_Xls_Insert_Val("COM.");
    prod_Xls_Insert_Val("REMARQUE");
    prod_Xls_Insert_Val("Type Remarque");

    # ETC.

    #EDITION DE LA TETE DE COLONNE
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );
    1;
}

exit main(@ARGV);
