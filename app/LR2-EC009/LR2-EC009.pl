#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use Data::Dumper;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::EX;
use oEUser::Descriptor::OF;
use oEUser::Descriptor::OG;
use oEUser::Descriptor::I4;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use Date::Calc qw(Today);
use Time::Local;
use Email::Sender::Simple qw(sendmail);
use Email::Sender::Transport::SMTP;
use oEdtk::Outmngr qw(omgr_stats);
use oEdtk::Config qw(config_read);
use oEdtk::DBAdmin qw(db_connect);
use strict;

my $DOC;
my @DADDR  = ();
my @EADDR  = ();
my @CTRADD = ();
my $BOOL   = 0;
my $LOCSECT;
my $NUMIND;
my $CIV;
my $CIVCOURT;
my $CODE   = 0;
my $MTTIMP = 0;
my $LIB;
my $CG;
my $CGI;
my $CTR;
my $CPT_EX = 0;
my $DOMAINE;
my $DATECHEANCE;
my $DATECH;
my $EJ;
my $NOTEMNT;
my $DATECPL;
my $DATELT = "";
my $JOURLT;
my $MOIS;
my $JOUR_ECH;
my $MOIS_ECH;
my $ANNEE_ECH;
my $MOISLT;
my $ANNEELT;
my $DATEREJET;
my $JREJET;
my $NREJET  = "";
my $NBREJET = "";
my $ENTITE;
my $ENTSIGN;
my $NUMABA;
my $PJ;
my $ER;
my $PRENOM;
my $CPT_PGE;
my $ANO;
my $Y;
my $DOC;

### Dans le cadre de la norme 38 on est obligé de récupérer une partie de l'adresse en EX car l'I4 est incomplet
my $numVoieDest;
my $codePostDest;
my $numVoieEmet;
my $codePostEmet;

my $OBJETI;
my $OBJETII;
my ( $TRK, $AGENCE, $NOM, $CPDEST, $VILDEST );

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'xIDEMET', 'NUMADH', 'xNOMDEST', 'xCPDEST', 'xVILDEST' ]
    );

    # trame keys => ['IDDEST', 'AGENCE', 'NOMDEST', 'COMPTE', 'INDUS']

    my $opt_ged = '--edms';
    oe_new_job('--index');

    my $ENTETE = oEUser::Descriptor::ENTETE::get();
    $ENTETE->bind_all_c7();
    my $EX = oEUser::Descriptor::EX->get();

    #	$EX->debug();
    $EX->bind_all_c7();
    my $OF = oEUser::Descriptor::OF->get();

    #	$OF->debug();
    $OF->bind_all_c7();
    my $OG = oEUser::Descriptor::OG->get();

    #	$OF->debug();
    $OG->bind_all_c7();
    my $I4 = oEUser::Descriptor::I4->get();

    #	$I4->debug();
    $I4->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    #	$Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $DOC->append( 'xTYPDOC', 'RJPB' );    ### Type de document
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $ENTETE,
        'EX'     => $EX,
        'OF'     => $OF,
        'OG'     => $OG,
        'I4'     => $I4,
        'Z1'     => $Z1
    );

    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'ENTETE' ) {

        }
        elsif ( $id eq 'EX' ) {

            #warn "INFO EX: dump ". Dumper($vals) ."\n";
            @DADDR = ();
            @EADDR = ();
            $vals->{'LICCODP'} =~ s/FRANCE//i;
            $vals->{'LICC094'} =~ s/FRANCE//i;

            my $civDest = $vals->{'PENOCOD'};
            $DOC->append( 'civDest', $civDest );
            #ADRESSE DESTINATAIRE
            push( @DADDR,
"$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}"
            );
            $numVoieDest =
              "$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'}";
            $codePostDest = $vals->{'CODCDXC'};

            $numVoieEmet =
              "$vals->{'PEAD053'} $vals->{'LICBTQV'} $vals->{'PEVO056'}";
            $codePostEmet = $vals->{'CODPOSx'};

            $PRENOM = $vals->{'PENOPRN'};
            $CG = substr( $vals->{'ODERCOD'}, 0, 1 );
            if ( $CG =~ /I|D|P/ ) {
                $EJ  = "MNT";
                $CGI = "la MNT";
                $ER  = "Votre agence MNT";
            }
            elsif ( $CG =~ /J|L|M/ ) {
                $EJ  = "MUTACITE";
                $CGI = "MUTACITE";
                $ER  = "Votre agence MUTACITE";
            }
            else {
                $ER = "INFORMATION : ATTENTION CETTE ER N'EST PAS REFERENCEE";
            }

            #ADRESSE CTR
            push( @CTRADD, "$EJ" );
            push( @CTRADD, "Centre de Traitement Recouvrement" );
            push( @CTRADD, "TSA 70011" );
            push( @CTRADD, "33044 BORDEAUX Cedex" );

            @CTRADD = user_cleanup_addr(@CTRADD);

            #ADRESSE EMETTEUR
            push( @EADDR, "$vals->{'PENO078'}" );
            $DOC->append( 'PPSEC', $vals->{'PENO078'} );

            @EADDR = user_cleanup_addr(@EADDR);
            $DOC->append( 'ASSUREUR', $EJ );
            $DOC->append( 'PPSEC',    $vals->{'PENO078'} );
            $DOC->append( 'PPCPLSEC', "$vals->{'PNTR051'} $vals->{'CPLA052'}" );
            $DOC->append( 'PPRUESEC',
                    $vals->{'PEAD053'} . " "
                  . $vals->{'LICBTQV'} . " "
                  . $vals->{'PEVO056'} . " "
                  . $vals->{'LIBV058'} );
            $DOC->append( 'PPBP', $vals->{'LIBLDTx'} );
            $DOC->append( 'PPCODSEC',
                $vals->{'CODPOSx'} . " " . $vals->{'LIBLOCx'} );

            $CTR     = "Centre de Traitement Recouvrement";
            $ENTSIGN = $vals->{'ODERCOD'};
            $LOCSECT = ucfirst( lc( $vals->{'LIBLOCx'} ) );
            $NUMIND  = $vals->{'NUMCINx'};
            $ENTITE  = $vals->{'ODCGCOD'};
            my @liste;
            my $i;
            @liste = split( //, $NUMIND );
            $DOC->append_table( 'NUMADHL', @liste );

            #DONNEES DE GED
            $AGENCE  = $vals->{'ODERCOD'};
            $NOM     = $vals->{'PENOLIB'};
            $CPDEST  = $vals->{'CODCDXC'};
            $VILDEST = $vals->{'NOMCDXL'};
            $DOC->append( 'xIDEMET',  $vals->{'ODERCOD'} );
            $DOC->append( 'xNOMDEST', $vals->{'PENOLIB'} );
            $DOC->append( 'xCPDEST',  $vals->{'CODCDXC'} );
            $DOC->append( 'xVILDEST', $vals->{'NOMCDXL'} );
            $DOC->append( 'xIDDEST',  $vals->{'NUMCINx'} );
            $DOC->append( 'xSOURCE',  'PREC' );

            $DATECHEANCE = convertDate( $vals->{'DATVALx'} );
            $JOUR_ECH    = substr( $vals->{'DATVALx'}, 6, 2 );
            $MOIS_ECH    = substr( $vals->{'DATVALx'}, 4, 2 );
            $ANNEE_ECH   = substr( $vals->{'DATVALx'}, 0, 4 );
            $DATECH =
              $JOUR_ECH . "" . month_to_name($MOIS_ECH) . "" . $ANNEE_ECH;

            #REGLE 6 DU CDC
            $CIV      = $vals->{'PENOCOD'};
            $CIVCOURT = $vals->{'PENOCOD'};
            if ( $CIV =~ /MME|MLLE/ ) {
                $CIV = "Chère Adhérente,";
            }
            else {
                $CIV = "Cher Adhérent,";
            }

            $MTTIMP = $vals->{'MTTIMPx'};
            $DOC->append( 'xCLEGEDi', $vals->{'MTTIMPx'} );
            $LIB = $vals->{'LIBMOTx'};

            #REGLE 7 DU CDC
            $DOMAINE = $vals->{'ODCGCOD'};
            if ( $DOMAINE =~ /MNT-RC|MUT-RC|MNT-RO/ ) {
                $DOMAINE = "santé";
            }
            else {
                $DOMAINE = "prévoyance";
            }

            #REGLE 12 DU CDC
            if ( $vals->{'ODCGCOD'} =~ /MNT-RC|MNT-RO/ ) {
                $NOTEMNT = "OK";
            }
            else {
                $NOTEMNT = "KO";
            }
            $CODE = $vals->{'CMOTIFx'};
            $DOC->append( 'CODE',       $CODE );
            $DOC->append( 'xCLEGEDiii', $vals->{'CMOTIFx'} );

            #REGLE DE GESTION NUMERO 4 SUR DATE LIMITE DE REGULARISATION
            $DATECPL = localtime;
            $JOURLT  = substr( $DATECPL, 0, 3 );
            $MOISLT  = substr( $DATECPL, 4, 3 );
            $DATELT  = substr( $DATECPL, 8, 2 );
            $ANNEELT = substr( $DATECPL, 20, 4 );

            if ( $DATELT <= 14 ) {
                $JREJET = 20;
                if ( ( $JOURLT =~ /Sat/ ) || ( $JOURLT =~ /Sun/ ) ) {
                    $JREJET = 22;
                }

            }
            elsif ( $DATELT => 15 ) {
                $JREJET = 26;
                if ( ( $JOURLT =~ /Sat/ ) || ( $JOURLT =~ /Sun/ ) ) {
                    $JREJET = 28;
                }
            }

            if ( $MOISLT =~ /Jan/ ) { $MOIS = "01"; }
            if ( $MOISLT =~ /Feb/ ) { $MOIS = "02"; }
            if ( $MOISLT =~ /Mar/ ) { $MOIS = "03"; }
            if ( $MOISLT =~ /Apr/ ) { $MOIS = "04"; }
            if ( $MOISLT =~ /May/ ) { $MOIS = "05"; }
            if ( $MOISLT =~ /Jun/ ) { $MOIS = "06"; }
            if ( $MOISLT =~ /Jul/ ) { $MOIS = "07"; }
            if ( $MOISLT =~ /Aug/ ) { $MOIS = "08"; }
            if ( $MOISLT =~ /Sep/ ) { $MOIS = "09"; }
            if ( $MOISLT =~ /Oct/ ) { $MOIS = "10"; }
            if ( $MOISLT =~ /Nov/ ) { $MOIS = "11"; }
            if ( $MOISLT =~ /Dec/ ) { $MOIS = "12"; }

            $DATEREJET = $JREJET . "/" . $MOIS . "/" . $ANNEELT;
        }
        elsif ( $id eq 'OF' ) {

            #warn "INFO : dump OF ". Dumper($vals) ."\n";

            $DOC->append( 'NUMIND',    $NUMIND );
            $DOC->append( 'JOUR_ECH',  $JOUR_ECH );
            $DOC->append( 'MOIS_ECH',  month_to_name($MOIS_ECH) );
            $DOC->append( 'ANNEE_ECH', $ANNEE_ECH );
            $DOC->append( 'CIV',       $CIV );
            $DOC->append( 'MTTIMP',    $MTTIMP );
            $DOC->append( 'LIB',       $LIB );
            $DOC->append( 'CG',        $CGI );
            $DOC->append( 'NOTEMNT',   $NOTEMNT );
            $ANO = $vals->{'ANOxxxx'};

            #				$DOC->append('ANO', $ANO);
            $NBREJET = $vals->{'NBREJET'};
            $DOC->append( 'NBREJET', $NBREJET );

            if ( $ENTSIGN =~
/D001|M001|D008|M008|D009|M009|M012|D012|D013|M013|P006|D018|M018|M019|D019|D021|M021|D022|M022|D023|D024|M024|M029|D028|D029|I029|D031|M031|M033|D033|M034|D034|D036|M040|D040|M041|D041|D044|M044|M049|I049|D051|M051|D052|M052|M059|D059|D063|M063|M064|D064|M066|D066|D071|M071|D072|M072|D073|M073|D075|M075|I074|J074|J081|M081|I081|D082|M082|D089|M089|D091|M091|I092|J092|D093|M093|I094|J094|M095|D095|D971|M971/
              )
            {
                $DOC->append_table( 'CTRADD', @CTRADD );
                $BOOL = 1;
                $DOC->append( 'BOOL', $BOOL );
                $DOC->append( 'CTR',  $CTR );
                @EADDR  = ();
                @CTRADD = ();
                $DOC->append( 'LOCSECT', 'Bordeaux' );
            }
            else {
                $DOC->append_table( 'CTRADD', @CTRADD );
                $BOOL = 1;
                $DOC->append( 'BOOL', $BOOL );
                $DOC->append( 'CTR',  $CTR );
                @EADDR  = ();
                @CTRADD = ();
                $DOC->append( 'LOCSECT', 'Bordeaux' );

                #					$DOC->append('LOCSECT',ucfirst(lc($LOCSECT)));
            }

            $NREJET = $vals->{'REJETxx'};

            #REGLE 8 SUR PIECE JOINTE
            if ( $EJ =~ /MNT/ ) {
                $DOC->append( "encartPDF", "MANSEP-MNT.pdf" );
            }
            else {
                $DOC->append( "encartPDF", "MANSEP-MUT.pdf" );
            }
        }
        elsif ( $id eq 'I4' ) {
            adresseI4($vals);
            if ( $NBREJET == "001" && $CG =~ /I|D|P|J|L|M/ ) {

    #Ne génère un etat que si différent d'ANO et des code listés ci-dessous.
                if ( $ANO =~ /ANO/ || $CODE =~ /20|75|35|80/ ) {

                    #LES REJETS 35 ET 80 SONT GERES PAR LA BUREAUTIQUE
                    next;
                }
                else {
                    #CAS AUTRES QUE 20,75,35 ET 80
                    $DOC->append( 'LETTRE', {'LETTREB'} );
                    $CPT_PGE++;
                }
                $DOC->append('DEBADH');

            }
            else {
              #On ne traite pas les seconds rejets donc nous passons au suivant.
                next;
            }
            $DOC->append('COURRIER');
            $DOC->append('ENDADH');
        }
        elsif ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $vals->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
#			$GED->append('xCLEGEDiii',$NUMABA);  			#- N° adhérent (ID_MNT) == N° ABA
        }

    }

    $TRK->track( 'Doc', 1, $AGENCE, $NUMIND, $NOM, $CPDEST, $VILDEST );

    oe_print_to_output_file($DOC);

    oe_compo_link($opt_ged);
    mail_repro();
    return 0;
}

#################################################################################
# Ajouté pour la gestion de la norme 38
#################################################################################
sub adresseI4 {
    my $vals = shift;

    my $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBLOCL'} = '';
    }

    push( @DADDR, "$vals->{'PNTREMx'} $vals->{'CPLADRx'}" );
    push( @DADDR,
        "$numVoieDest $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );
    push( @DADDR, "$vals->{'BOIPOSL'}" );
    push( @DADDR, "$codePostDest $vals->{'NOMCDXL'}" );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );

    $Y = $vals->{'NOMC016'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( index( $vals->{'LIBL015'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBL015'} = '';
    }

    push( @EADDR, "$vals->{'PNTR011'} $vals->{'CPLA012'}" );
    push( @EADDR, "$numVoieEmet $vals->{'LIBV013'}" );
    push( @EADDR, "$vals->{'BOIP014'}" );
    push( @EADDR, "$codePostEmet $vals->{'LIBL015'} $vals->{'NOMC016'}" );
    push( @EADDR, "$vals->{'LICC094'}" );
    @EADDR = user_cleanup_addr(@EADDR);

    $DOC->append( 'PPCPLSEC', "$vals->{'PNTR011'} $vals->{'CPLA012'}" );
    $DOC->append( 'PPRUESEC', "$numVoieEmet $vals->{'LIBV013'}" );
    $DOC->append( 'PPBP',     $vals->{'BOIP014'} );
    $DOC->append( 'PPCODSEC',
        "$codePostEmet $vals->{'LIBL015'} $vals->{'NOMC016'}" );
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub month_to_name {
    my @mons =
      qw/Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre Décembre/;
    my $MOIS_ECH = shift;

    return $mons[ $MOIS_ECH - 1 ];
}

sub mail_repro {
    my @body;
    my $nbr_page;
    my $cfg = config_read('MAIL');
    my ( $sec, $min, $hour, $day, $month, $year ) = localtime();
    my $date = sprintf( "%02d/%02d/%d", $day, $month + 1, $year + 1900 );
    my $dtejrs = sprintf( "%d-%02d-%02d",   $year + 1900, $month + 1, $day );
    my $time   = sprintf( "%02d:%02d:%02d", $hour,        $min,       $sec );
    my $mailfile     = $cfg->{'EDTK_MAIL_REPRO'};
    my $edtk_prgname = $TRK->{'app'};
    my $edtk_entity  = $TRK->{'entity'};

    if ( $edtk_prgname =~ "LR2-EC009" ) {
        $nbr_page = "de trois pages";
    }
    else {
        $nbr_page = "d'une page";
    }
    open( my $fh, '<', $mailfile )
      or die "ERROR: Can't open \"$mailfile\": $!\n";
    while ( my $Lignebody = <$fh> ) {
        $Lignebody =~ s/%date/$date/;
        $Lignebody =~ s/%dtejrs/$dtejrs/;
        $Lignebody =~ s/%nbr_page/$nbr_page/;
        $Lignebody =~ s/%edtk_prgname/$edtk_prgname/;
        $Lignebody =~ s/%ed_entity/$edtk_entity/;
        push( @body, $Lignebody );
    }
    close($fh);
    my $subject .= $cfg->{'EDTK_MAIL_SUBJ_REPRO'};
    $subject =~ s/%date/$date/;
    $subject =~ s/%time/$time/;
    if ( $CPT_PGE == 1 ) {
        $CPT_PGE = $CPT_PGE . " " . "courrier";
    }
    else {
        $CPT_PGE = $CPT_PGE . " " . "courriers";
    }
    $subject =~ s/%CPT_PGE/$CPT_PGE/;
    $subject =~ s/%entity/$edtk_entity/;
    send_mail_repro(
        $cfg->{'EDTK_MAIL_LIST_REPRO'},
        $cfg->{'EDTK_MAIL_LIST_REPRO_C'},
        $subject, @body
    );
}

sub send_mail_repro {
    my ( $to, $cc, $subject, @body ) = @_;
    my $cfg = config_read('MAIL');
    $subject ||= $0;
    $subject = $cfg->{'EDTK_TYPE_ENV'} . " - $subject ";

    my $transport = Email::Sender::Transport::SMTP->new(
        {
            host => $cfg->{'EDTK_MAIL_SMTP'}
        }
    );

    my $email = Email::Simple->create(
        header => [
            To => $to || $cfg->{'EDTK_MAIL_SENDER'},
            Cc => $cc || $cfg->{'EDTK_MAIL_SENDER'},
            From    => $cfg->{'EDTK_MAIL_SENDER'},
            Subject => $subject || $0
        ],
        body => join( '', @body )
    );

    # Useful for testing.
    if ( $cfg->{'EDTK_MAIL_SMTP'} eq 'warn' ) {
        print $email->as_string() . "\n";
    }
    else {
        eval { sendmail( $email, { transport => $transport } ); };
        if ($@) {
            die "ERROR: sendmail failed. Reason is $@\n";
        }
    }
}

exit main(@ARGV);
