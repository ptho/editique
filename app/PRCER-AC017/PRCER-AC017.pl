#!/usr/bin/perl #-d:ptkdb
use strict;
use utf8;
use oEdtk::Main 0.5074;
use oEUser::Lib;
use oEdtk::RecordParser;
use oEdtk::Spool;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Data::Dumper;
use Readonly;
use warnings;
use feature "switch";
use Date::Calc qw(Today
  Gmtime
  Week_of_Year
  Delta_Days
  Add_Delta_Days
  Delta_YMD);    # http://search.cpan.org/~stbey/Date-Calc-6.3/lib/Date/Calc.pod

use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::C1;
use oEUser::Descriptor::C2;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::F1;
use oEUser::Descriptor::F2;
use oEUser::Descriptor::FE;
use oEUser::Descriptor::G1;
use oEUser::Descriptor::G2;
use oEUser::Descriptor::G4;
use oEUser::Descriptor::G6;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::H2;
use oEUser::Descriptor::H3;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::H5;
use oEUser::Descriptor::H6;
use oEUser::Descriptor::H7;
use oEUser::Descriptor::H8;
use oEUser::Descriptor::H9;
use oEUser::Descriptor::HO;
use oEUser::Descriptor::HP;
use oEUser::Descriptor::HV;
use oEUser::Descriptor::HW;
use oEUser::Descriptor::J1;
use oEUser::Descriptor::M1;
use oEUser::Descriptor::OA;
use oEUser::Descriptor::Z0;
use oEUser::Descriptor::Z1;

# INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
# avec initialisation propre au document
my $ENTETE = oEUser::Descriptor::ENTETE::get();

#$ENTETE->debug();
$ENTETE->bind_all_c7();

my $E1 = oEUser::Descriptor::E1->get();

#$E1->debug();
$E1->bind_all_c7();
my $E2 = oEUser::Descriptor::E2->get();

#$E2->debug();
$E2->bind_all_c7();
my $E7 = oEUser::Descriptor::E7->get();

# $E7->debug();
$E7->bind_all_c7();
my $F1 = oEUser::Descriptor::F1->get();
$F1->bind_all_c7();
my $F2 = oEUser::Descriptor::F2->get();
$F2->bind_all_c7();
my $FE = oEUser::Descriptor::FE->get();

# $FE->debug();
$FE->bind_all_c7();
my $C1 = oEUser::Descriptor::C1->get();
$C1->bind_all_c7();
my $C2 = oEUser::Descriptor::C2->get();
$C2->bind_all_c7();

#$C2->debug();
my $G1 = oEUser::Descriptor::G1->get();

#$G1->debug();
$G1->bind_all_c7();
my $G2 = oEUser::Descriptor::G2->get();

#$G2->debug();
$G2->bind_all_c7();
my $G4 = oEUser::Descriptor::G4->get();
$G4->bind_all_c7();
my $G6 = oEUser::Descriptor::G6->get();
$G6->bind_all_c7();
my $H1 = oEUser::Descriptor::H1->get();

#$H1->debug();
$H1->bind_all_c7();
my $H2 = oEUser::Descriptor::H2->get();
$H2->bind_all_c7();
my $H3 = oEUser::Descriptor::H3->get();

#$H3->debug();
$H3->bind_all_c7();
my $H4 = oEUser::Descriptor::H4->get();

#$H4->debug();
$H4->bind_all_c7();
my $H5 = oEUser::Descriptor::H5->get();

#$H5->debug();
$H5->bind_all_c7();
my $H6 = oEUser::Descriptor::H6->get();
$H6->bind_all_c7();
my $H7 = oEUser::Descriptor::H7->get();
$H7->bind_all_c7();
my $H8 = oEUser::Descriptor::H8->get();

#$H8->debug();
$H8->bind_all_c7();
my $H9 = oEUser::Descriptor::H9->get();

#$H9->debug();
$H9->bind_all_c7();
my $HO = oEUser::Descriptor::HO->get();
$HO->bind_all_c7();
my $HP = oEUser::Descriptor::HP->get();
$HP->bind_all_c7();
my $HV = oEUser::Descriptor::HV->get();
$HV->bind_all_c7();
my $HW = oEUser::Descriptor::HW->get();

#$HW->debug();
$HW->bind_all_c7();
my $J1 = oEUser::Descriptor::J1->get();
$J1->bind_all_c7();
my $M1 = oEUser::Descriptor::M1->get();
$M1->bind_all_c7();

my $OA = oEUser::Descriptor::OA->get();

#$OA->debug();
$OA->bind_all_c7();
my $Z0 = oEUser::Descriptor::Z0->get();
$Z0->bind_all_c7();
my $Z1 = oEUser::Descriptor::Z1->get();
$Z1->bind_all_c7();

my $CODPRD_HW;
my $CODH9;
my $TYPE_HW;
my %VALCO;
my %LISTE_MODELE_DOC
  ; # un même modèle peut être appelé plusieurs fois (variantes d'option dans les garanties)

my $CPTHW = 0;
my %CPT;
my $DT_DEBUT;
my $DT_FIN;
my %CODPROD_BY_DT_EFFET = oEdtk::TexDoc->new();
my $DATA                = oEdtk::TexDoc->new();
my $DOC                 = oEdtk::TexDoc->new();
my %DATA                = oEdtk::TexDoc->new();
my $DELAIS_STAGE_PLM    = oEdtk::TexDoc->new();
my $DELAIS_STAGE_PIM    = oEdtk::TexDoc->new();
my $DELAIS_STAGE_PLD    = oEdtk::TexDoc->new();
my $DELAIS_STAGE_PIO    = oEdtk::TexDoc->new();
my $DELAIS_STAGE_PIR    = oEdtk::TexDoc->new();
my $DELAIS_STAGE_PLNOPI = oEdtk::TexDoc->new();
my $DELAIS_STAGE_PID    = oEdtk::TexDoc->new();
my $DT_TODAY            = sprintf( "%4.0f%02.0f%02.0f", Today() );
my $EMET;
my $MONTANT = 0;
my $MTTANNUEL;
my $MTTDC = 0;
my $NUMSS;
my %OPTIONS;
my $RESERVE = "";
my $SOUSCRIP;
my %SOMME;
my %SOMMEGAR;
my $INDEMNJ  = "";
my $DATARRET = "";
my $NUMCCO;
my $DECMEDTOP;
my $DECMEDLIB;
my $DTECHINI;
my $MTECHINI;
my $NUMAD;
my $DATEEFFET;
my $COTTOTAL;
my $civadh;
my ( $TRK, $AGENCE, $NOM, $CPDEST, $VILDEST, $NUMCTR, $NUMABA );
my %DT_OPTGAR1_PIR;
my %DT_OPTGAR2_PIR;
my @xADRLN;
my @DADDR = ();
my %TABGAR;
my %TABCERT;
my %TABDELAIS;
my %LISTDELAISGAR;
my @LISTDELAISGAR;
my $CPTFE = 0;
my $TOP_LAB;
my $TOP_TEXT;
my $DELAISTAGE;
my $DELAISPIO;
my $STAGE         = 0;
my $nbStagePLM    = 0;
my $nbStagePLD    = 0;
my $nbStagePIM    = 0;
my $nbStagePIR    = 0;
my $nbStagePID    = 0;
my $nbStagePIO    = 0;
my $nbStagePLNOPI = 0;
my $COTANNU;
my $TOP_OA;
my $ERREUR;
my $CODOFR;
my $SOMMEATTEST;
my ( $DT_GTIE, $CODEPRT, $CODGTIE, $MTCOT, $IndFirstDte, $nbrOpt );
my @TABCODGTIE     = ();
my @TABCODGTIENOPI = ();
my @TABDTEDEB      = ();
my %H_CODPRD       = ();
my %H_INFO_GTIE    = ();
my %H_BASETAUXCO   = ();
my %H_GTIE_OPT     = ();
Readonly my $LIBCODNOPI   => "PLNOPI";
Readonly my $LIBCODPRDGS1 => "PL-MNT-GS1";
Readonly my $LIBCODPRDGS2 => "PL-MNT-GS2";
Readonly my $LIBTOP_TXT   => "TOPTXT_GTIE";
Readonly my $LIBOPT_TXT   => "OPTIONTXT_GTIE";
Readonly my $LIBCODE_PRT  => "CODE_PDT_GTIE";
Readonly my $LIBCODE_GTIE => "CODE_GTIE_COURT";
Readonly my $LIBLONG_PDT  => "LIB_LONG_PDT_GTIE";
Readonly my $LIBTXETBASE  => "TAUXETBASE_GTIE";
Readonly my $LIBTAUXCO    => "TAUXCO";
Readonly my $LIBBASECO    => "BASECO";
Readonly my $LIBNIVOCO    => "NIVOCO";
Readonly my $LIBMTCOT     => "MONTANT_COT_AN";

my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
  localtime(time);
my $dateJour = sprintf( "%04d%02d%02d", 1900 + $year, $mon + 1, $mday );

my %JoursSem = (
    1 => "lundi",
    2 => "mardi",
    3 => "mercredi",
    4 => "jeudi",
    5 => "vendredi",
    6 => "samedi",
    7 => "dimanche",
);

my %Mois = (
    1  => "janvier",
    2  => "février",
    3  => "mars",
    4  => "avril",
    5  => "mai",
    6  => "juin",
    7  => "juillet",
    8  => "août",
    9  => "septembre",
    10 => "octobre",
    11 => "novembre",
    12 => "décembre",
);

my $DateEdit =
    $JoursSem{$wday} . ' '
  . $mday . ' '
  . $Mois{ $mon + 1 } . ' '
  . ( 1900 + $year );
my $annee = 1900 + $year;
my %hData;
my $MontGlobPeriode = 0;
my $DateFinMax;
my $COTMENS;
my $ANNEE_RET;
my $ANNEE_RET1;
my $DTEFFACT;
my $ANNUEL_HT  = 0;
my $ANNUEL_TX  = 0;
my $ANNUEL_TTC = 0;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );

    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMABA', 'IDEMET', 'NOMDEST', 'CPDEST', 'VILDEST' ]
    );

    # SI ON SOUHAITE METTRE LE DOCUMENT EN GED,
    # IL SUFFIT DE PASSER L'OPRION --edms à oe_compo_link COMME CI-DESSOUS
    my $opt_ged = '--edms';    #'--edms';

    oe_new_job('--index');

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $ENTETE,
        'E1'     => $E1,
        'E2'     => $E2,
        'E7'     => $E7,
        'F1'     => $F1,
        'F2'     => $F2,
        'FE'     => $FE,
        'C1'     => $C1,
        'C2'     => $C2,
        'G1'     => $G1,
        'G2'     => $G2,
        'G4'     => $G4,
        'G6'     => $G6,
        'H1'     => $H1,
        'H2'     => $H2,
        'H3'     => $H3,
        'H4'     => $H4,
        'H5'     => $H5,
        'H6'     => $H6,
        'H7'     => $H7,
        'H8'     => $H8,
        'H9'     => $H9,
        'HO'     => $HO,
        'HP'     => $HP,
        'HV'     => $HV,
        'HW'     => $HW,
        'J1'     => $J1,
        'OA'     => $OA,
        'M1'     => $M1,
        'Z0'     => $Z0,
        'Z1'     => $Z1
    );

    while ( my ( $id, $VALS ) = $p->next() ) {
        if ( $id eq 'E1' ) {

            #warn "INFO : dump E1 ". Dumper($VALS) ."\n";
            $AGENCE = $VALS->{'ODERCOD'};
            $NOM = $VALS->{'PENOLIB'} . " " . ucfirst( lc $VALS->{'PENOPRN'} );
            $DOC->append( 'NOM', $NOM );
            $DATA->append( 'NOM', $NOM );
            $CPDEST  = $VALS->{'CODCDXC'};
            $VILDEST = $VALS->{'NOMCDXL'};
            prepE1($VALS);
            gestionAdrE1($VALS);
        }
        if ( $id eq 'E2' ) {

            #warn "INFO : dump e2 ". Dumper($VALS) ."\n";
            $EMET       = oe_uc_sans_accents( $VALS->{'PENO084'} );
            $EMET       = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
            $NUMCCO     = $VALS->{'ACCCNUM'};
            $NUMCTR     = ( $VALS->{'ACH1NUM'} );
            $DTEFFACT   = convertDate( $VALS->{'ACOPDDE'} );
            $ANNEE_RET1 = substr( $VALS->{'ACOPDDE'}, 0, 4 );
            $DATA->append( 'DTEFFACT', $DTEFFACT );
            $DOC->append( 'NUMCTR', $NUMCTR );
            $DATA->append( 'NUMCTR',   $NUMCTR );
            $DATA->append( 'ACCCNUM',  $VALS->{'ACCCNUM'} );
            $DATA->append( 'FREQPAIE', $VALS->{'ACFQLIB'} || 'MENSUEL' );
        }
        if ( $id eq 'E7' ) {
            gestionAdrE7($VALS);
        }
        if ( $id eq 'F1' ) {
        }
        if ( $id eq 'F2' ) {
        }
        if ( $id eq 'FE' ) {

            #            warn "INFO : dump FE ". Dumper($VALS) ."\n";
            $CPTFE++;
            if ( $VALS->{'DECISTO'} eq 2 ) {
                $DECMEDTOP = 'OUI';
            }
            else {
                $DECMEDTOP = 'NON';
            }
            $DECMEDLIB = $VALS->{'DECISME'};
            $DTECHINI  = $VALS->{'DTECHIN'};
            $MTECHINI  = $VALS->{'MTECHIN'};

            #warn "info TOP : $DECMEDTOP \n";
        }
        if ( $id eq 'H1' ) {

            #warn "INFO : dump H1 ". Dumper($VALS) ."\n";
            $NUMSS = $VALS->{'PEAFNUM'} . " " . $VALS->{'PEAFCLE'};
            $DATA->append( 'NUMSS',   $NUMSS );
            $DATA->append( 'DATNAIS', oe_to_date( $VALS->{'PECPDAT'} ) );
            $DATEEFFET = convertDate( $VALS->{'ACH2DDE'} );
            $ANNEE_RET = substr( $VALS->{'ACH2DDE'}, 0, 4 );
        }
        if ( $id eq 'H2' ) {
        }
        if ( $id eq 'H3' ) {
        }
        if ( $id eq 'H4' ) {

            #            warn "INFO : dump H4 ". Dumper($VALS) ."\n";
            prepH4($VALS);
        }
        if ( $id eq 'H5' ) {

            #warn "INFO : dump H5 ". Dumper($VALS) ."\n";
        }
        if ( $id eq 'H6' ) {
        }
        if ( $id eq 'H7' ) {
        }
        if ( $id eq 'H8' ) {

            #warn "INFO : dump H8 ". Dumper($VALS) ."\n";
            prepH8($VALS);
        }
        if ( $id eq 'H9' ) {

            #warn "INFO : dump H9 ". Dumper($VALS) ."\n";
            prepH9($VALS);
        }
        if ( $id eq 'HO' ) {
        }
        if ( $id eq 'HP' ) {
        }
        if ( $id eq 'HV' ) {
        }
        if ( $id eq 'HW' ) {
            prepHW($VALS);
        }
        if ( $id eq 'J1' ) {
        }
        if ( $id eq 'M1' ) {
        }
        if ( $id eq 'OA' ) {
            $TOP_OA++;
            appel_EFE($VALS);
        }
        if ( $id eq 'C1' ) {
        }
        if ( $id eq 'C2' ) {

            #warn "INFO : dump C2 ". Dumper($VALS) ."\n";
            #warn "INFO : TOP_LAB record C2 ". $VALS->{'PROFCOD'} ."\n";
            prepC2($VALS);
        }
        if ( $id eq 'G1' ) {
            $SOUSCRIP = ( $VALS->{'PENOLIB'} );
            $DATA->append( 'SOUSCRIP', $SOUSCRIP );
        }
        if ( $id eq 'G2' ) {
        }
        if ( $id eq 'G4' ) {
        }
        if ( $id eq 'G6' ) {
        }
        if ( $id eq 'Z0' ) {
        }

        if ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DATA->append( 'NUMABA', $NUMABA );
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
#			$GED->append('xCLEGEDiii',$NUMABA);  			#- N° adhérent (ID_MNT) == N° ABA
        }
        if ( $id eq 'OPTION' ) {
        }
    }

    #	while ( my ($key, $value) = each(%OPTIONS) ) {
    #		 print "OPTIONS - $key => $value\n";
    #	}

    finFlux();

    #    warn "INFO DATA: dump ". Dumper($DATA) ." \n";
    #    warn "INFO DOC: dump ". Dumper($DOC) ." \n";
    #die;
    $TRK->track( 'Doc', 1, $AGENCE, $NOM, $CPDEST, $VILDEST, $NUMABA );
    oe_compo_link($opt_ged);
    return 0;
}

sub prepGED {
    my $cod_produit = shift;

    $DATA{$cod_produit}->append( 'xTYPDOC', 'CINS' );    ### Type de document
    $DATA{$cod_produit}->append( 'xIDEMET', 'TEST' );
    $DATA{$cod_produit}->append_table( 'DADDR', @xADRLN );
    $DATA{$cod_produit}->append( 'xIDEMET',    $EMET );
    $DATA{$cod_produit}->append( 'xIDDEST',    $NUMCCO );
    $DATA{$cod_produit}->append( 'xNOMDEST',   $NOM );
    $DATA{$cod_produit}->append( 'xCPDEST',    $CPDEST );
    $DATA{$cod_produit}->append( 'xVILDEST',   $VILDEST );
    $DATA{$cod_produit}->append( 'xCLEGEDi',   $NUMSS );
    $DATA{$cod_produit}->append( 'xCLEGEDii',  oe_to_date($DT_DEBUT) );
    $DATA{$cod_produit}->append( 'xCLEGEDiii', oe_to_date($DT_FIN) );

    #warn "INFO cod_produit :". $cod_produit ."\n";
    $DATA{$cod_produit}->append( 'xCLEGEDiv', $OPTIONS{$cod_produit} );
    return 0;
}

sub gestionAdrE1 {
    my $VALS = shift;
    $civadh = ( $VALS->{'LICCODC'} );
    push( @DADDR,
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
    );
    return 0;
}

sub gestionAdrE7 {
    my ($VALS) = @_;
    my $Y;

# APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR E1) :
# SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS E1-ZZZZ-NOMCDXLIBACH
# cas 1 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC !C Y	: E1-ZZZZ-BOIPOSLIBLDT + b + E1-ZZZZ-LIBLOCLIBLOC
# cas 2 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC C  Y	: E1-ZZZZ-BOIPOSLIBLDT
# cas 3 : E1-ZZZZ-INDCDX =0 				: E1-ZZZZ-BOIPOSLIBLDT

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $VALS->{'LIBLOCL'} );
    oe_clean_addr_line( $VALS->{'NOMCDXL'} );
    $Y = $VALS->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $VALS->{'INDCDXx'} == 0 ) {

        # CAS 3
        $VALS->{'LIBLOCL'} = '';
    }
    elsif ( $VALS->{'INDCDXx'} == 1 && index( $VALS->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $VALS->{'LIBLOCL'} = '';
    }

    push( @xADRLN, $VALS->{'PNTREMx'} );
    push( @xADRLN, $VALS->{'CPLADRx'} );
    push( @xADRLN, "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );
    push( @xADRLN, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
    push( @xADRLN, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );

    $VALS->{'PELICCO'} =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @xADRLN, $VALS->{'PELICCO'} );
    @xADRLN = user_cleanup_addr(@xADRLN);
    $DOC->append_table( 'DADDR', @xADRLN );

    push( @DADDR, $VALS->{'PNTREMx'} );
    push( @DADDR, $VALS->{'CPLADRx'} );
    push( @DADDR, "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );
    push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
    push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );
    $VALS->{'PELICCO'} =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @DADDR, $VALS->{'PELICCO'} );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'xADRLN', @DADDR );
    return 0;
}

sub prepE1() {
    my ($VALS) = @_;

    @xADRLN = ();
    push( @xADRLN, "$VALS->{'PNTREMx'} $VALS->{'CPLADRx'}" );

    $DATA->append( 'AGENCE', $AGENCE );
    return 0;
}

sub prepC2() {
    my ($VALS) = @_;
    if ( $VALS->{'PROFCOD'} =~ m/PCL/ ) {
        $TOP_LAB = 1;
    }
    return 0;
}

sub prepH4() {
    my ($VALS) = @_;
    my $DateConsid;
    my $opt_txt;
    my $DATERAD = $VALS->{'ACH4010'};
    $CODGTIE = $VALS->{'ACH4COD'};
    $CODEPRT = $VALS->{'ACH4006'};

#    print STDERR "CODGTIE = $VALS->{'ACH4COD'} et CODEPRT = $VALS->{'ACH4006'} \n";
    my $flag_text = ( substr( $CODEPRT, 0, 4 ) );

    if ( $flag_text =~ /(PLM\d|PLS\d|PLC\d)/ ) {
        $TOP_TEXT = 1;
        if ( $flag_text =~ /(PLM\d|PLS\d)/ ) {
            $opt_txt = 1;
        }
        elsif ( $flag_text =~ /(PLC\d)/ ) {
            $opt_txt = 2;
        }
    }
    elsif ( $flag_text =~ /(PLM\D|PLSP|PLCP)/ ) {
        $TOP_TEXT = 2;
        if ( $flag_text =~ /(PLM\D|PLSP)/ ) {
            $opt_txt = 1;
        }
        elsif ( $flag_text =~ /(PLCP)/ ) {
            $opt_txt = 2;
        }
    }
    elsif (( $CODEPRT =~ m/$LIBCODPRDGS1/ )
        || ( $CODEPRT =~ m/$LIBCODPRDGS2/ ) )
    {
        $TOP_TEXT = 3;
        $opt_txt  = 4;
    }
    else { }

    if ( defined $opt_txt ) { $DOC->append( 'OPTTXT', $opt_txt ); }

    #------------------------------------ On prend que les garanties non radiees
    if ( $DATERAD eq "" ) {
        $H_INFO_GTIE{$LIBCODE_PRT}{$CODGTIE} = $CODEPRT;
        $H_INFO_GTIE{$LIBLONG_PDT}{$CODGTIE} = $VALS->{'PRPR008'};
        if ( exists( $H_INFO_GTIE{$LIBMTCOT}{$CODEPRT} ) ) {
            $H_INFO_GTIE{$LIBMTCOT}{$CODEPRT} += $VALS->{'ACCOMAO'};
        }
        else {
            $H_INFO_GTIE{$LIBMTCOT}{$CODEPRT} = $VALS->{'ACCOMAO'};
        }
        if (   ( $CODEPRT =~ m/$LIBCODPRDGS1/ )
            || ( $CODEPRT =~ m/$LIBCODPRDGS2/ ) )
        {
            push( @TABCODGTIENOPI, $CODGTIE );
        }
        {
            push( @TABCODGTIE, $CODGTIE );
        }
        if ( !exists( $H_CODPRD{$CODEPRT} ) ) {
            $H_CODPRD{$CODEPRT} = 0;
        }
    }

    my $code_prd_H4 = set_apptex_from_code_produit($CODEPRT);
    my $cod_tex     = set_apptex_from_code_produit($CODEPRT);

    $CPT{$code_prd_H4}++;

   # il faut commencer par créer l'objet même si la garantie n'est plus valide
   # car on peut avoir besoin de l'objet dans les lignes de détail
    $DATA{$code_prd_H4} = oEdtk::TexDoc->new()
      if ( !exists( $DATA{$code_prd_H4} ) );

    if ( !check_dt_fin_validite( $VALS->{'ACH4010'}, $CODEPRT ) ) {

        $ERREUR .=
"ERROR : DATE DE RESILIATION PRESENT DANS LE RECORD H4 code produit $code_prd_H4 Date de résiliation : $DATERAD \n";
    }

    #warn "INFO COD PRODUIT: ".user_prev_get_option($VALS->{'PRPR008'})."\n";
    my $COD_PRD = user_prev_get_option( $VALS->{'PRPR008'} );
    $COD_PRD =~ s/{/-/g;
    $COD_PRD =~ s/}/+/g;
    $OPTIONS{$code_prd_H4} = $COD_PRD;
    $DATA{$code_prd_H4}->append( 'LIBCODEPRT', $CODEPRT );
    if ( $cod_tex =~ /PID/ ) {
        if ( $VALS->{'ACH4006'} =~ /PIDP-CNP1/ ) {
            $DATA{$code_prd_H4}
              ->append( 'typgarantie', "GARANTIE ACCIDENT - SAPEURS POMPIERS" );
            $DATA{$code_prd_H4}->append( 'ctrfootnote', {"editfootnote"} );
            $DATA{$code_prd_H4}->append( 'numcontrat', "5725 Y" );
        }
        elsif (( $VALS->{'ACH4006'} =~ /PID..-MNT3/ )
            && ( $VALS->{'ACH4006'} ne 'PIDS3-MNT3' ) )
        {
            $DATA{$code_prd_H4}->append( 'typgarantie',
                "REGLEMENT MUTUALISTE PREVOYANCE\nGARANTIE DÉCÈS PTIA\n" );
            $DATA{$code_prd_H4}->append( 'ctrfootnote', {"null"} );
            $DATA{$code_prd_H4}->append( 'numcontrat', "" );
        }
        elsif ( $VALS->{'ACH4006'} eq 'PIDS3-MNT3' ) {
            $DATA{$code_prd_H4}->append( 'typgarantie',
                "REGLEMENT MUTUALISTE PREVOYANCE\nGARANTIE DÉCÈS PTIA\n" );
            $DATA{$code_prd_H4}->append( 'ctrfootnote', {"null"} );
            $DATA{$code_prd_H4}->append( 'numcontrat', "" );
        }
        else {
            $DATA{$code_prd_H4}->append( 'typgarantie',
"GARANTIE DÉCÈS - PERTE TOTALE\n ET IRRÉVERSIBLE D'AUTONOMIE\n"
            );
            $DATA{$code_prd_H4}->append( 'ctrfootnote', {"editfootnote"} );
            $DATA{$code_prd_H4}->append( 'numcontrat', "8969 Z" );
        }

    }
    elsif ( $cod_tex =~ /PIO/ ) {
        $MTTDC = $VALS->{'ACKGVAL'};
        $DATA->append( 'MTTDC', $MTTDC || 0 );

    }
    elsif ( $cod_tex =~ /PIR/ ) {

        $INDEMNJ = $VALS->{'ACIJVAL'}
          if ( $INDEMNJ eq "" && $VALS->{'ACH4006'} =~ /^PIR.1/ );
        $DATARRET = $VALS->{'PRFRLIB'} if ( $DATARRET eq "" );
    }

    warn "H4-cod_tex	= $VALS->{'ACH4006'} \n";

    #    warn "H4-CODE_PRODUIT = $VALS->{'ACH4COD'} \n";

    # MISE EN FORME DES LIBELLES LONGS PRODUITS SUR LE DOCUMENT
    my $libgarantie = "";
    if ( ( $cod_tex =~ /PIR/ ) || ( $cod_tex =~ /$LIBCODNOPI/ ) ) {
        $libgarantie = $VALS->{'PRGA003'};
    }
    else {
        $libgarantie = $VALS->{'PRGALIB'};
    }
    $libgarantie = user_prev_get_option($libgarantie);
    $libgarantie =~ s/-.*//o;
    $libgarantie =~ s/^\s+//;
    $libgarantie =~ s/{/-/g;
    $libgarantie =~ s/}/+/g;
    $libgarantie =~ s/=//;
    $libgarantie =~ s/IJ/Indemnités Journalières/;
    $libgarantie =~ s/PC/Invalidité retraite/;
    $libgarantie =~ s/IV/Invalidité/;
    $libgarantie =~ s/PR/Perte de retraite/;
    $libgarantie =~ s/DC01/Décès, PTIA/;
    $libgarantie =~ s/Capital DC ssqm/Décès, PTIA/;

    if ( $CODGTIE eq 'PCMF1-RI00' ) {
        $libgarantie =~ s/RI/Rente Invalidité/;
    }
    my $CODE_PRODUIT = $CODGTIE;
    $TABGAR{ $code_prd_H4 . "-" . $CODE_PRODUIT } = "Garantie " . $libgarantie;
    $TABCERT{$code_prd_H4} = $code_prd_H4;
    if ( $DATERAD eq "" ) {
        $H_INFO_GTIE{$LIBCODE_GTIE}{$CODGTIE} = $libgarantie;
    }

# DANS UN MONDE MERVEILLEUX AVEC UN FLUX CONFORME AUX SPECIFICATIONS NOUS DEVRIONS AVOIR UN RECORD H3
# MAIS VOILA NOUS DEVRONS NOUS CONTENTER DE RECUPERER l'INFORMATION DE RESERVE SUR LE RECORD OPTION.

    $MONTANT = $VALS->{'ACCOMAO'};
    $SOMME{$code_prd_H4} += $VALS->{'ACCOMAO'}
      if ( $DATERAD eq '' || $DTEFFACT > $DATERAD );
    $SOMMEATTEST += $VALS->{'ACCOMAO'}
      if ( $DATERAD eq '' || $DTEFFACT > $DATERAD );

    if ( $CPT{$code_prd_H4} > 1 ) {
        $DATA{$code_prd_H4}
          ->append( "CUMUL$cod_tex", $SOMME{$code_prd_H4} || 0 );
    }
    else {
        $DATA{$code_prd_H4}->append( "CUMUL$cod_tex", $MONTANT || 0 );
    }
    if ( ( substr( $VALS->{'PRGALIB'}, 0, 2 ) eq 'IJ' ) ) {
        $DateConsid = $ANNEE_RET1 . '0101';
        $hData{'DATEDEB'} =
          Select2Date( $DateConsid, $VALS->{'ACH4DDE'}, 'MAX' );
        $DateConsid = $ANNEE_RET . '1231';
        if ( $DATERAD ne '' )
        { # si la date de de radiation est vide alors la date de fin période est 31/12/année considérée
            $hData{'DATEFIN'} =
              Select2Date( $DATERAD, $hData{'DATEFIN'}, 'MAX' );
            $hData{'DATEFIN'} =
              Select2Date( $DateConsid, $hData{'DATEFIN'}, 'MIN' );
        }
        else {
            $hData{'DATEFIN'} = $DateConsid;
        }
    }
    if (   $VALS->{'ACH4006'} =~ /^PIR.2/
        || $cod_tex =~ /PLD/
        || $cod_tex =~ /PID/ )
    {
        $DATA{$code_prd_H4}->append( 'MTTRENTE', $VALS->{'ACKGVAL'} || 0 );
    }
    $DATA{$code_prd_H4}->append( 'CUMULMTT', $VALS->{'ACCOMAO'} || 0 );
    $DATA{$code_prd_H4}->append( 'INDOPT', $OPTIONS{$code_prd_H4} );
    return (0);
}

sub prepH8() {
    my ($VALS)       = @_;
    my $cod_tex      = $VALS->{'ACH4002'};
    my $code_prd_H8  = set_apptex_from_code_produit( $VALS->{'ACH4002'} );
    my $CODE_PRODUIT = $VALS->{'ACH4COD'};

    #warn "INFO :". Dumper(@_) ."\n";
    if ( exists( $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } ) ) {
        $DELAISPIO = $VALS->{'ACSTDUR'} . " mois";
        $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } = $DELAISPIO;

        my $cod_prod =
          set_apptex_from_code_produit( $code_prd_H8 . "-" . $CODE_PRODUIT );
        if ( $cod_prod eq 'PLM' ) {
            $DELAIS_STAGE_PLM->append( "PRD", $cod_prod );
            $DELAIS_STAGE_PLM->append( "GARANTIE",
                $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PLM->append( "DELAIS",
                $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PLM->append("EditLigne");
            $nbStagePLM++;
        }
        elsif ( $cod_prod eq 'PLD' ) {
            $DELAIS_STAGE_PLD->append( "PRD", $cod_prod );
            $DELAIS_STAGE_PLD->append( "GARANTIE",
                $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PLD->append( "DELAIS",
                $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PLD->append("EditLigne");
            $nbStagePLD++;
        }
        elsif ( $cod_prod eq 'PIM' ) {
            $DELAIS_STAGE_PIM->append( "PRD", $cod_prod );
            $DELAIS_STAGE_PIM->append( "GARANTIE",
                $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PIM->append( "DELAIS",
                $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PIM->append("EditLigne");
            $nbStagePIM++;
        }
        elsif ( $cod_prod eq 'PIO' ) {
            $DELAIS_STAGE_PIO->append( "PRD", $cod_prod );
            $DELAIS_STAGE_PIO->append( "GARANTIE",
                $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PIO->append( "DELAIS",
                $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PIO->append("EditLigne");
            $nbStagePIO++;
        }
        elsif ( $cod_prod eq 'PIR' ) {
            $DELAIS_STAGE_PIR->append( "PRD", $cod_prod );
            $DELAIS_STAGE_PIR->append( "GARANTIE",
                $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PIR->append( "DELAIS",
                $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PIR->append("EditLigne");
            $nbStagePIR++;
        }
        elsif ( $cod_prod eq $LIBCODNOPI ) {
            $DELAIS_STAGE_PLNOPI->append( "PRD", $cod_prod );
            $DELAIS_STAGE_PLNOPI->append( "GARANTIE",
                $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PLNOPI->append( "DELAIS",
                $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PLNOPI->append("EditLigne");
            $nbStagePLNOPI++;
        }
        elsif ( $cod_prod eq 'PID' ) {
            $DELAIS_STAGE_PID->append( "PRD", $cod_prod );
            $DELAIS_STAGE_PID->append( "GARANTIE",
                $TABGAR{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PID->append( "DELAIS",
                $TABDELAIS{ $code_prd_H8 . "-" . $CODE_PRODUIT } );
            $DELAIS_STAGE_PID->append("EditLigne");
            $nbStagePID++;
        }
    }

    return (0);
}

sub prepH9() {
    my ($VALS) = @_;
    my $DateConsid;

    #warn "INFO :". Dumper(@_) ."\n";
    my $nbmois;
    my $moisf;
    my $moisd;

    $CODH9 = $VALS->{'ACH4002'};
    $ANNUEL_HT += $VALS->{'ACCOMPH'};
    $ANNUEL_TX += $VALS->{'ACT1MTP'};
    $MTTANNUEL += $VALS->{'ACCOMAO'};

    if ( ( substr( $hData{'DATEFIN'}, 4, 2 ) != '12' ) ) {
        if ( ( substr( $hData{'DATEFIN'}, 6, 2 ) ) != '31' ) {
            $moisf = ( substr( $hData{'DATEFIN'}, 4, 2 ) - 1 );
        }
        else {
            $moisf = ( substr( $hData{'DATEFIN'}, 4, 2 ) );
        }

        $moisd = ( substr( $hData{'DATEDEB'}, 4, 2 ) ) || 0;
        $nbmois = ( ( $moisf - $moisd ) + 1 );

        if ( $nbmois == undef ) { ( $nbmois = 1 ) }

    }
    else {
        $moisf = ( substr( $hData{'DATEFIN'}, 4, 2 ) );
        $moisd = ( substr( $hData{'DATEDEB'}, 4, 2 ) );
        $nbmois = ( $moisf - $moisd ) + 1;

        if ( $nbmois == undef ) { ( $nbmois = 1 ) }

    }

    $ANNUEL_TTC = ( $ANNUEL_HT + $ANNUEL_TX );

    $COTMENS = ( $SOMMEATTEST / 12 );
    $COTANNU = $ANNUEL_TTC;
    return (0);
}

sub prepHW() {
    my ($VALS) = @_;

# on ne check pas la date de validité car les dates ne sont pas gérées de la même façon dans le H4 et le HW
    $CPTHW++;
    $TYPE_HW   = $VALS->{'PRASCOD'};
    $CODPRD_HW = set_apptex_from_code_produit( $VALS->{'ACH4COD'} );

    #warn "INFO : type HW ($CODPRD_HW) = $TYPE_HW\n";
    #warn "INFO : dump HW ". Dumper($VALS) ."\n";

    $DT_DEBUT = $VALS->{'ACCODDE'};
    $DT_FIN   = $VALS->{'ACCODFE'};

# ON ÉDITE UNIQUEMENT SI LA DATE D'EFFET DE L'OPTION EST LA PLUS RÉCENTE DES DATES D'EFFET DU FLUX
# (Infinite passe tout l'historique dans le flux, mais on ne veut pas tout rééditer à chaque fois)
# on peut éditer plusieurs certificats, mais uniquement pour la dernière date de début d'effet indiquée dans le flux
    $CODPROD_BY_DT_EFFET{ $VALS->{'ACCODDE'} }{$CODPRD_HW} = 1;    #$DT_DEBUT
      # dans finFlux on va reconsiderer les dates de début d'effet pour choisir les produits à éditer
    if ( $TYPE_HW eq $LIBBASECO ) {
        $VALCO{$TYPE_HW}{$CODPRD_HW}{ $VALS->{'ACCODDE'} } =
          $VALS->{'ACJIMAH'};    # cumul par type HW / PRASLIB
        prepGED($CODPRD_HW)
          ; # on produit les données de GED uniqument pour la première itération du record HW (BASECO)
        if ( $CODPRD_HW eq $LIBCODNOPI ) {
            $H_BASETAUXCO{$TYPE_HW}{ $VALS->{'ACH4002'} }{ $VALS->{'ACCODDE'} }
              = $VALS->{'ACJIMAH'};
        }
    }
    elsif ( $TYPE_HW eq $LIBNIVOCO ) {
        $H_BASETAUXCO{$TYPE_HW}{ $VALS->{'ACH4002'} }{ $VALS->{'ACCODDE'} } =
          $VALS->{'ACJIMAH'};
    }
    elsif ( $TYPE_HW eq $LIBTAUXCO ) {
        if (   ( ( $CODPRD_HW eq 'PLM' ) && ( $VALS->{'ACH4002'} eq $CODOFR ) )
            || ( ( $CODPRD_HW eq 'PLS' ) && ( $VALS->{'ACH4002'} eq $CODOFR ) )
          )
        {
            $VALCO{$TYPE_HW}{$CODPRD_HW}{ $VALS->{'ACCODDE'} } =
              $VALS->{'ACJIMAH'};    # cumul par type HW / PRASLIB
                                     #warn "INFO =". Dumper(%VALCO)." \n";
            if ( $VALS->{'ACH4COD'} =~ /^PIR.1/ ) {
                $DATA->append( 'OPTGARp', {"OPTUN"} );
            }

            if ( $VALS->{'ACH4COD'} =~ /^PIR.2/ ) {
                $DATA->append( 'OPTGARs', {"OPTDEUX"} );
            }

            $DATA->append( 'INDEMNJ',  $INDEMNJ );
            $DATA->append( 'DATARRET', $DATARRET );
            $CODOFR = $VALS->{'ACH4002'};
        }
        elsif ( $CODPRD_HW eq $LIBCODNOPI ) {
            $H_BASETAUXCO{$TYPE_HW}{ $VALS->{'ACH4002'} }{ $VALS->{'ACCODDE'} }
              += $VALS->{'ACJIMAH'};

        }
        else {
            $VALCO{$TYPE_HW}{$CODPRD_HW}{ $VALS->{'ACCODDE'} } +=
              $VALS->{'ACJIMAH'};    # cumul par type HW / PRASLIB

            if ( $VALS->{'ACH4COD'} =~ /^PIR.1/ ) {
                $DATA->append( 'OPTGARp', {"OPTUN"} );
            }

            if ( $VALS->{'ACH4COD'} =~ /^PIR.2/ ) {
                $DATA->append( 'OPTGARs', {"OPTDEUX"} );
            }

            $DATA->append( 'INDEMNJ',  $INDEMNJ );
            $DATA->append( 'DATARRET', $DATARRET );
            $CODOFR = $VALS->{'ACH4002'};
        }
    }
    else {

    }

# PIR* : gestion des options : les options sont gérées ici pour disposer de la date de début de
# validité de l'option dans la garantie => lorsque un adhérent à les 2 options à des dates de
# début de validité différentes, si on modifie les conditions d'une des options, seule la date
# de début de validité de l'option modifiée change et on a besoin de refaire un certificat avec
# les nouvelles conditions de cette option
#

    return (0);
}

sub appel_EFE() {
    my ($VALS) = @_;

    # ATTENTION À LA NORME DE NOMMAGE DES FICHIERS !!!
    my $FLAGNICP     = ( $VALS->{'FLAGxxx'} );
    my $appel_encart = oEdtk::TexDoc->new();
    my $CODPRD       = set_apptex_from_code_produit( $VALS->{'PRODUIT'} );

    if ( exists $TABCERT{$CODPRD} ) {
        $TABCERT{$CODPRD} = 1;
        if ( $FLAGNICP == 1 ) {
            $DATA->append( "flagNICP$CODPRD", 1 );
            $appel_encart->append( "encartPDF", $VALS->{'REFEREN'} . ".pdf" );
            $DATA->append( "ruptEFE",       1 );
            $DATA->append( "Notice$CODPRD", $appel_encart );
        }
        else {
            $DATA->append( "flagNICP$CODPRD", 0 );
        }
    }
    foreach my $key ( keys %TABCERT ) {

        if ( $TABCERT{$key} ne '1' ) { $DATA->append( "flagNICP$key", 0 ); }
    }
    return (0);
}

sub set_apptex_from_code_produit {
    my $code = shift;
    if ( $code =~ m/PLS/ ) {
        $code = substr( $code, 0, 4 );
        $code =~ s/PLS\d|PLSP/PLM/;
        $code =~ s/PLSD/PLD/;
    }
    elsif ( $code =~ m/PLC/ ) {
        $code = substr( $code, 0, 4 );
        $code =~ s/PLC\d|PLCP/PLM/;
        $code =~ s/PLCD/PLD/;
    }
    elsif (( $code =~ m/$LIBCODPRDGS1/ )
        || ( $code =~ m/$LIBCODPRDGS2/ )
        || ( $code =~ m/$LIBCODNOPI/ ) )
    {
        $code = $LIBCODNOPI;
    }
    else {
        $code = substr( $code, 0, 3 );
        $code =~ s/PI3/PIMbret/
          ;    # LaTeX ne supporte pas les valeurs numériques dans les tag
        $code =~ s/PIS/PIM/;
    }
    return $code;
}

sub finFlux() {
    if ( $DECMEDTOP eq "NON" )
    {          #TRAITEMENT DE LA RESERVE MEDICALE, SOLUTION PROVISOIRE.
        $DATA->append( 'RESERVE', "NON" );
    }
    else {
        $DATA->append( 'RESERVE', "OUI" );
    }

    #    warn "INFO TABCODGTIE =". Dumper(\@TABCODGTIE)." \n";
    #    warn "INFO H_INFO_GTIE =". Dumper(\%H_INFO_GTIE)." \n";
    foreach my $itercprd ( keys %H_CODPRD ) {
        my $codePrtCou = set_apptex_from_code_produit($itercprd);
        $DATA->append( 'LIBCODEPRTCOU', $codePrtCou );
        if ( $itercprd eq $LIBCODPRDGS1 || $itercprd eq $LIBCODPRDGS2 ) {
            $nbrOpt = @TABCODGTIENOPI;
            $DATA->append( 'NBROPTGTIE', $nbrOpt );
            $CODGTIE = code_gtie_edit( $itercprd, $nbrOpt );
            @TABDTEDEB = sort keys( %{ $H_BASETAUXCO{$LIBBASECO}{$CODGTIE} } )
              ; #--- Trie du tableau contenant les dates  pour prendre la plus recente
            $IndFirstDte = @TABDTEDEB - 1;
            $DATA->append(
                'LIBLONGTIE',
                recupere_libLongNOPI(
                    $itercprd, $nbrOpt, $TABDTEDEB[$IndFirstDte]
                )
            );
            $DATA->append( 'LIBLONPRT', $H_INFO_GTIE{$LIBLONG_PDT}{$CODGTIE} );
            $DATA->append( "CUMUL$LIBCODNOPI",
                $H_INFO_GTIE{$LIBMTCOT}{ $H_INFO_GTIE{$LIBCODE_PRT}{$CODGTIE} }
            );

            if ( $itercprd eq $LIBCODPRDGS2 ) {
                my $txGl = 0;
                foreach my $magtie (@TABCODGTIENOPI) {
                    $txGl += $H_BASETAUXCO{$LIBTAUXCO}{$magtie}
                      { $TABDTEDEB[$IndFirstDte] };
                }
                $DATA->append( "$LIBTAUXCO$LIBCODNOPI", $txGl );

                $DATA->append( "$LIBBASECO$LIBCODNOPI",
                    $H_BASETAUXCO{$LIBBASECO}{$CODGTIE}
                      { $TABDTEDEB[$IndFirstDte] } );
            }
            else {
                for ( my $iter = 0 ; $iter < $nbrOpt ; $iter++ ) {
                    my @SPLITGTIE = split( "-", $TABCODGTIENOPI[$iter], -1 );
                    my $libBase = "BASECO$LIBCODNOPI" . $SPLITGTIE[2];
                    $DATA->append( $libBase,
                        $H_BASETAUXCO{$LIBBASECO}{ $TABCODGTIENOPI[$iter] }
                          { $TABDTEDEB[$IndFirstDte] } );
                }
            }
        }
        else {
            foreach my $k ( keys %{ $H_INFO_GTIE{$LIBCODE_GTIE} } ) {
                given ($k) {
                    when (/PCMF1-RI00/) {
                        $H_INFO_GTIE{$LIBCODE_GTIE}{$k} =~
                          s/Rente Invalidité/Invalidité/;
                        $DATA->append( 'LIBLONGTIEDeux',
                            $H_INFO_GTIE{$LIBCODE_GTIE}{$k} );
                    }
                    when (/PBMF1-RI00/) {
                        $DATA->append( 'LIBLONGTIE',
                            $H_INFO_GTIE{$LIBCODE_GTIE}{$k} );
                    }
                }
            }
            $nbrOpt = @TABCODGTIE;
            $DATA->append( 'NBROPTGTIE', $nbrOpt );
        }
    }

    if ( $CPTHW == 0 ) {
        die "ERROR: pas d'enregistrement HW dans le flux $ARGV[-1]\n";
    }

    if ( $TOP_LAB == 1 ) {
        $DOC->append( 'DATEEDI',   $DateEdit );
        $DOC->append( 'DATEEFFET', $DATEEFFET );
        $DOC->append( 'ANNEE',     substr( $DTEFFACT, 6, 4 ) );
        $DOC->append( 'CIVIL',     $civadh );
        $DOC->append( 'COTTOTAL',  $COTANNU );
        $DOC->append( 'COTMENS',   $COTMENS );

        if ( ( $TOP_TEXT == 1 ) || ( $TOP_TEXT == 2 ) || ( $TOP_TEXT == 3 ) ) {
            $DOC->append('STARTATTEST');
            $DOC->append('EditENTETE');
            $DOC->append('EditPeriode');
            if ( $TOP_TEXT == 2 ) {
                $DOC->append('TEXTGMSPOMP');
            }
            else { $DOC->append('TEXTSTANDARD'); }
        }
    }
    if ( $nbStagePLM > 0 ) {
        $DATA->append( 'DelaisStagePLM', $DELAIS_STAGE_PLM );
        $DATA->append( 'STAGEPLM',       1 );
    }
    else {
        $DATA->append( 'STAGEPLM', 0 );
    }
    if ( $nbStagePLD > 0 ) {
        $DATA->append( 'DelaisStagePLD', $DELAIS_STAGE_PLD );
        $DATA->append( 'STAGEPLD',       1 );
    }
    else {
        $DATA->append( 'STAGEPLD', 0 );
    }
    if ( $nbStagePIM > 0 ) {
        $DATA->append( 'DelaisStagePIM', $DELAIS_STAGE_PIM );
        $DATA->append( 'STAGEPIM',       1 );
    }
    else {
        $DATA->append( 'STAGEPIM', 0 );
    }
    if ( $nbStagePIO > 0 ) {
        $DATA->append( 'DelaisStagePIO', $DELAIS_STAGE_PIO );
        $DATA->append( 'STAGEPIO',       1 );
    }
    else {
        $DATA->append( 'STAGEPIO', 0 );
    }
    if ( $nbStagePIR > 0 ) {
        $DATA->append( 'DelaisStagePIR', $DELAIS_STAGE_PIR );
        $DATA->append( 'STAGEPIR',       1 );
    }
    else {
        $DATA->append( 'STAGEPIR', 0 );
    }

    if ( $nbStagePLNOPI > 0 ) {
        $DATA->append( 'DelaisStagePLNOPI', $DELAIS_STAGE_PLNOPI );
        $DATA->append( 'STAGEPLNOPI',       1 );
    }
    else {
        $DATA->append( 'STAGEPLNOPI', 0 );
    }
    if ( $nbStagePID > 0 ) {
        $DATA->append( 'DelaisStagePID', $DELAIS_STAGE_PID );
        $DATA->append( 'STAGEPID',       1 );
    }
    else {
        $DATA->append( 'STAGEPID', 0 );
    }

    #VERFICATION DE LA PRESENCE DU RECORD FE OBLIGATOIRE (LAB)
    if ( $CPTFE == 0 ) {
        die "ERROR: pas d'enregistrement FE dans le flux $ARGV[-1]\n";
    }

    oe_print_to_output_file($DATA);

    my $last_date_effet
      ;    # cette variable va servir à conserver la dernière date du tri

    foreach my $date ( sort keys %CODPROD_BY_DT_EFFET ) {
        $last_date_effet = $date;
    }

# maintenant on va chercher le ou les produits à éditer pour la dernière date
    foreach my $cod_produit ( keys %{ $CODPROD_BY_DT_EFFET{$last_date_effet} } )
    {
        my $cod_tex = set_apptex_from_code_produit($cod_produit);
        $cod_tex = $cod_produit;
        if ( $cod_produit ne $LIBCODNOPI ) {
            $DATA{$cod_produit}->append( "$LIBBASECO$cod_tex",
                $VALCO{$LIBBASECO}{$cod_produit}{$last_date_effet} || 0 );
            $DATA{$cod_produit}->append( "$LIBTAUXCO$cod_tex",
                $VALCO{$LIBTAUXCO}{$cod_produit}{$last_date_effet} || 0 );
        }

        my $date_effet = $last_date_effet;
        $DATA{$cod_produit}->append( 'DATEFFET', oe_to_date($date_effet) );
        $DATA{$cod_produit}->append_table( 'xADRLN', @DADDR );
        $DATA{$cod_produit}->append( 'STARTDOC' . $cod_tex );
        if ( ( $TOP_OA == undef ) || ( $TOP_OA == 0 ) ) {
            $DATA{$cod_produit}->append('newpage');
        }

        oe_print_to_output_file( $DATA{$cod_produit} );

    }

    if ( $TOP_LAB == 1 ) {
        oe_print_to_output_file($DOC);
    }

    return (0);
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub check_dt_fin_validite {

# SI LA DATE DE FIN DE VALIDITÉ DE L'OPTION SOUSCRITE EST PLUS ANCIENNE
# que la date d'édition (date courante) on édite ni le H4 ni le HW de l'option concerne
# ie : Infinite passe tout l'historique du contrat dans le flux, on édite pas ce qui est obsolète
    my $date = shift
      || $DT_TODAY
      ;    # pour H4 la date n'est fournie que lorsque le produit est échu
    my $produit = shift;
    if ( $date lt $DT_TODAY ) {

#warn "INFO : date de fin de validite ($date) depassee ligne $. pour $produit\n";
#warn "INFO : ligne $. ignoree\n";
        return 0;
    }

    return (0);
}

sub code_gtie_edit {
    my ( $libprt, $nbrOpt ) = @_;
    if ( $libprt eq $LIBCODPRDGS1 || $libprt eq $LIBCODPRDGS2 ) {
        $H_GTIE_OPT{$LIBCODPRDGS1}{1} = "PL-GS1-IJ";
        $H_GTIE_OPT{$LIBCODPRDGS1}{2} = "PL-GS1-INV";
        $H_GTIE_OPT{$LIBCODPRDGS2}{1} = "PL-GS2-IJ";
        $H_GTIE_OPT{$LIBCODPRDGS2}{2} = "PL-GS2-INV";
        $H_GTIE_OPT{$LIBCODPRDGS2}{3} = "PL-GS2-RET";
        if ( exists( $H_GTIE_OPT{$libprt}{$nbrOpt} ) ) {
            return $H_GTIE_OPT{$libprt}{$nbrOpt};
        }
        else {
            die "ERROR: Le produit NOPI($libprt) n a pas cette garantie.\n";
        }

    }
    else {
        return $TABCODGTIE[0];
    }
}

sub recupere_libLongNOPI {
    my ( $libprt, $nbrOpt, $dategtie ) = @_;
    my $magtie  = "";
    my $liblong = "";
    my $iterOp  = 1;
    while ( $iterOp <= $nbrOpt ) {

#         print STDERR "info nombre option = $nbrOpt \n  info magtie = $H_GTIE_OPT{$libprt}{$iterOp} \n  info liblong = $H_GTIE_OPT{$libprt}{$iterOp} \n info taugar = $H_BASETAUXCO{$LIBNIVOCO}{$magtie}{$dategtie} \n";
        $magtie = $H_GTIE_OPT{$libprt}{$iterOp};
        $liblong .= " $H_INFO_GTIE{$LIBCODE_GTIE}{$magtie}";
        if ( $magtie =~ m/\D*\WGS2\W\D*/ ) {
            my @splitTX =
              split( '\.', $H_BASETAUXCO{$LIBNIVOCO}{$magtie}{$dategtie}, -1 );
            $liblong .= " $splitTX[0] %";
        }
        $iterOp++;
    }
    return $liblong;
}

#################### On peut aussi le formaliser comme ça :
#################### - Date de début de période = Max (01/01/année en cours ; Date de souscription)
#################### - Date de fin de période = Min (Date de résiliation ; 31/12/année en cours).

##############################################################################################################################
# sous programme qui compare les deux dates aux formats AAAAMMJJ et renvoie la date max ou min de format AAAAMMJJ
# si une date est vide en réception il retournera la date non vide , si les deux dates sont vides , il retournera la chaine "VIDE"
###############################################################################################################################
sub Select2Date {

    my $Date1    = shift;
    my $Date2    = shift;
    my $MAXouMIN = shift;
    my $DateRet;
    my $Dd;

    if ( ( $Date1 eq '' ) and ( $Date2 eq '' ) ) {
        return $Date1;
    }
    elsif ( ( $Date1 eq '' ) and ( $Date2 ne '' ) ) {
        return $Date2;
    }
    elsif ( ( $Date1 ne '' ) and ( $Date2 eq '' ) ) {
        return $Date1;
    }
    else {
        $Dd = Delta_Days(
            substr( $Date1, 0, 4 ),
            substr( $Date1, 4, 2 ),
            substr( $Date1, 6, 2 ),
            substr( $Date2, 0, 4 ),
            substr( $Date2, 4, 2 ),
            substr( $Date2, 6, 2 )
        );
        if ( $MAXouMIN eq 'MAX' ) {
            if ( $Dd > 0 ) {
                $DateRet = $Date2;
            }
            else {
                $DateRet = $Date1;
            }
        }
        elsif ( $MAXouMIN eq 'MIN' ) {
            if ( $Dd > 0 ) {
                $DateRet = $Date1;
            }
            else {
                $DateRet = $Date2;
            }
        }

        return $DateRet;
    }
}

exit main(@ARGV);
