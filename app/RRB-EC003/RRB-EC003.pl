#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls 0.4625;
use Data::Dumper;
use oEdtk::RecordParser;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::EY;
use oEUser::Descriptor::EW;
use oEdtk::Tracking;
use strict;

use warnings;

#################################################################################
# v0.5 21/11/2006 11:56:54 du squelette d'extraction de donnees pour C7
#################################################################################
# METHODOLOGIE DE DEVELOPPEMENT :
#
# 1- préparation DET (description des enregistrements techniques)
# 2- génération de l'application Perl (récupération des noms de balises)
# 3- description de la cinématique des données
# 4- report de la cinématique des données sur la maquette
# 5- positionnement des balises de données suur la maquette
# 6- description résumée des règles fonctionnelles (qq phrases)
# 7- création de la maquette
# 8- mise à jour de la structure de document dans la feuille de style (balises de structure)
# 9- désignation des clefs de rupture
#10- description de l'application d'extraction sous forme d'arbre algorythmique
#11- développement et mise à jour de la feuille de style et de l'application d'extraction de données
#12- validation - recette
#

# CORPS PRINCIPAL DE L'APPLICATION :

# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)

my $TRK;
my $NUMCHQ;
my $BANQUE;
my $NUMADH;
my $ADH;
my $MONTANT;

my %hRECORDS;
my $VALS;
my $numrows = 0;
my $TOTAL   = 0;
my $FIRST   = 0;

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }

    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] ) || 'none';

    # INITIALISATION DU TRAKING
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMIND', 'ADHERENT' ]
    );

    # oe_new_job(); # est remplacé par :
    oe_open_fi_IN( $argv[0] );

    # MAPPING DES RECORDS
    map_records();

    init_Doc();

    # INITIALISATION DU PROCESS PARSER (ASSIGNE LES CLEFS AUX RECORDS)
    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => $hRECORDS{ENTETE},
        'EY'     => $hRECORDS{EY},
        'EW'     => $hRECORDS{EW},
    );

    init_XLS();

    my ( $id, $lastid );

    while ( ( $id, $VALS ) = $p->next() ) {

        if ( $id eq 'EY' ) {
            process_EY();
        }
        elsif ( $id eq 'EW' ) {
            process_EW();
        }
        $TRK->track( 'Reject', 1, $NUMADH, $ADH, );

    }

    # Génération des deux lignes des totaux
    prod_Xls_Insert_Val( undef, "NOMBRE DE PIECES: $numrows",
        "TOTAL REMISE", undef, $TOTAL );
    prod_Xls_Edit_Ligne( { bold => 1 } );

    prod_Xls_Insert_Val( undef, undef, "TOTAL GENERAL", undef, $TOTAL );
    prod_Xls_Edit_Ligne( { bold => 1 } );
    return 0;
}

sub map_records() {
    $hRECORDS{ENTETE} = oEUser::Descriptor::ENTETE->get();
    $hRECORDS{ENTETE}->bind_all()
      ; # bind les champs de données en reprenant l'étiquette définie dans le descripteur
    $hRECORDS{EY} = oEUser::Descriptor::EY->get();

    #$hRECORDS{EY}->debug();
    $hRECORDS{EY}->bind_all_c7()
      ;    # bind les champs de données sur 7 caractères
    $hRECORDS{EW} = oEUser::Descriptor::EW->get();

    #$hRECORDS{EW}->debug();
    $hRECORDS{EW}->bind_all_c7()
      ;    # bind les champs de données sur 7 caractères

    return 1;
}

sub init_Doc() {

    my $NUMCHQ;
    my $BANQUE;
    my $NUMADH;
    my $ADH;
    my $MONTANT;

}

sub process_EY () {

    #    warn "INFO : dump EY ".Dumper($VALS)."\n";
    #    'ET00014' => '009876582',
    #    'ET0002C' => '200.00',
    #    'ET00015' => 'BNP'
    $NUMCHQ  = $VALS->{'ET00014'};
    $MONTANT = $VALS->{'ET0002C'};
    $BANQUE  = $VALS->{'ET00015'};

}

sub process_EW () {

    #	warn "INFO : dump EW ". Dumper($VALS) ."\n";

    $NUMADH = $VALS->{'NUMCONT'};

    $ADH =
      $VALS->{'CIVILxx'} . " " . $VALS->{'NOMPAYx'} . " " . $VALS->{'PRENPAY'};
    $ADH =~ s/^ *//;
    edit_ligne_XLS();
}

sub edit_ligne_XLS {

    prod_Xls_Insert_Val($NUMADH);
    prod_Xls_Insert_Val($ADH);
    prod_Xls_Insert_Val($BANQUE);
    prod_Xls_Insert_Val($NUMCHQ);
    prod_Xls_Insert_Val($MONTANT);
    $TOTAL += $MONTANT;
    $numrows++;
    prod_Xls_Edit_Ligne();

    #warn "INFO MONTANT: $MONTANT \n";
}

sub init_XLS {
    my $remise_en_banque = shift;
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
    #
    # 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
    # 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ;
    #	 NR7 = Numérique Right ; AL12 = Alpha Left 12 de large...  )
    #
    prod_Xls_Col_Init( 'AC15', 'AL40', 'AL20', 'AC20', 'NR15' );

    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    #
    if ( $FIRST == 0 ) {
        prod_Xls_Init( "M.N.T.",
            "REEDITION DE LA REMISE EN BANQUE DU $remise_en_banque" );
        $FIRST = 1;
    }
    else {
        prod_Xls_New();
    }

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("ADHERENT");
    prod_Xls_Insert_Val("NOM & PRENOM");
    prod_Xls_Insert_Val("BANQUE");
    prod_Xls_Insert_Val("N° DE CHEQUE");
    prod_Xls_Insert_Val("MONTANT");

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );
    return 0;
}

exit main(@ARGV);
