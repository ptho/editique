# Statistiques semestrielles éditiques v. 2.0a du 20/12/2012
use utf8;

use strict;
use warnings;

use Data::Dumper;
use Date::Calc qw(Today Gmtime Week_of_Year)
  ;    # http://search.cpan.org/~stbey/Date-Calc-6.3/lib/Date/Calc.pod
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Config qw(config_read);
use oEdtk::DBAdmin qw(db_connect);
use oEdtk::Outmngr 0.07 qw(omgr_stats);

our ( $dico_app, $dico_ej, $dico_dm );

my $edtktracking    = 'EDTK_TRACKING';
my $edtk_trackingy1 = 'EDTK_TRACKING_20170712_BAK';
my $edtkindex       = 'EDTK_INDEX';
my $edtkindexy1     = 'EDTK_INDEX';

sub main {
    oe_new_job('--noinputfile');

    my $doc = oEdtk::TexDoc->new();

    my $cfg = config_read('EDTK_STATS');
    my $dbh = db_connect( $cfg, 'EDTK_DBI_STATS',
        { AutoCommit => 1, RaiseError => 1 } );
    my $pdbh = db_connect( $cfg, 'EDTK_DBI_STATS' );
    my $rows;
    my $rowsy1;
    my $nbrdocy1;
    my $evolution;

    my (
        $sql,                 $key,               $tstamp_from,
        $tstamp_to,           $tstamp_fromy1,     $tstamp_toy1,
        $tstampActimail_from, $tstampActimail_to, $totalptcs,
        $ptcs,                $pc,                $camembertDocGe,
        $tableauDocGe
    );
    my ( $tstampActimail_toy1, $tstampActimail_fromy1 );
    my $time = time;
    my ( $year, $month, $day, $hour, $min, $sec, $doy, $dow, $dst ) =
      Gmtime($time);

    my ( $colorR, $colorV, $colorB );

    my ( $week, ) = Week_of_Year( $year, $month, $day );

    ### Calcul des date de début et de fin de période
    $tstamp_from = ( $year - 1 ) * 10000000000;
    $tstamp_to   = $year * 10000000000;
    $doc->append( 'yearStat', $year - 1 );
    $tstamp_fromy1 = ( $year - 2 ) * 10000000000;
    $tstamp_toy1   = ( $year - 1 ) * 10000000000;
    $doc->append( 'yearIStat', $year - 2 );
    $tstampActimail_from   = ( $year - 1 ) * 10000;
    $tstampActimail_to     = $year * 10000;
    $tstampActimail_fromy1 = ( $year - 2 ) * 10000;
    $tstampActimail_toy1   = ( $year - 1 ) * 10000;
    $doc->append('beginDoc');

    ### Récupération du nombre de documents générés la dernière année sur la base de tracknig
    $sql =
"select count(*) from $edtktracking where ED_JOB_EVT = \'D\' AND ED_TSTAMP > $tstamp_from AND ED_TSTAMP < $tstamp_to AND ED_APP NOT LIKE \'TST\%\' AND ED_APP not like \'\%H9_ged_insert\' AND \(ED_K1_VAL NOT LIKE \'TEST \%\' OR ED_K1_VAL is null\) AND ED_USER not like \'DocsRenouv\'";
    $rows = executeSQLcommand( $dbh, $sql );
    print
"Récupération du nombre de documents générés la dernière année sur la base de tracknig : $sql\n";
    my $totalDocumentsGenere = $$rows[0][0];
    $doc->append( 'comptDocumentsGeneres', $totalDocumentsGenere );

    ### Récupération du nombre de pages générés la dernière année sur la base de lotissement
### $sql = "SELECT SUM(NbrPages) FROM ( SELECT ED_CORP, ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM EDTK_INDEX where ED_DTLOT > \'$tstampActimail_from\' and ED_DTLOT < \'$tstampActimail_to\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_IDLDOC, ED_SEQDOC )";
    $sql =
"SELECT SUM(NbrPages), COUNT(*) NbrDocs FROM ( SELECT ED_SOURCE, ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM $edtkindex where ED_DTLOT > \'$tstampActimail_from\' and ED_DTLOT < \'$tstampActimail_to\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_SOURCE, ED_IDLDOC, ED_SEQDOC )";
    $rows = executeSQLcommand( $dbh, $sql );
    print
"Récupération du nombre de pages générés la dernière année sur la base de lotissement : $sql\n";
    my $totalPagesEnvoyes     = $$rows[0][0];
    my $totalDocumentsEnvoyes = $$rows[0][1];
    $doc->append( 'comptPagesEnvoyes',     $totalPagesEnvoyes );
    $doc->append( 'comptDocumentsEnvoyes', $totalDocumentsEnvoyes );

    ### Récupération du nombre de pages envoyées l'année précédente sur la base de lotissement
    $sql =
"SELECT SUM(NbrPages), COUNT(*) NbrDocs FROM ( SELECT ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM $edtkindexy1 where ED_DTLOT > \'$tstampActimail_fromy1\' and ED_DTLOT < \'$tstampActimail_toy1\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_IDLDOC, ED_SEQDOC )";
    $rows = executeSQLcommand( $dbh, $sql );
    print
"Récupération du nombre de pages générés l'année précédente sur la base de lotissement : $sql\n";
    my $totalPagesEnvoyesy1     = $$rows[0][0];
    my $totalDocumentsEnvoyesy1 = $$rows[0][1];
    $doc->append( 'comptPagesEnvoyesyI',     $totalPagesEnvoyesy1 );
    $doc->append( 'comptDocumentsEnvoyesyI', $totalDocumentsEnvoyesy1 );

    ### Calcule évolution totale nombre docs envoyes
    $evolution = ( $totalDocumentsEnvoyes - $totalDocumentsEnvoyesy1 ) * 100 /
      $totalDocumentsEnvoyesy1;
    $doc->append(
        'evoTotalActimailDocsPourcent',
        sprintf( "%0.2f", ($evolution) ) . "\%"
    );
    if ( $evolution > 0 ) {
        $doc->append( 'signeEvoTotalActimailDocsPourcent', "+" );
    }
    else {
        $doc->append( 'signeEvoTotalActimailDocsPourcent', "" );
    }

    ### Calcule évolution totale nombre de pages envoyees
    $evolution = ( $totalPagesEnvoyes - $totalPagesEnvoyesy1 ) * 100 /
      $totalPagesEnvoyesy1;
    $doc->append(
        'evoActimailTotalPagesPourcent',
        sprintf( "%0.2f", ($evolution) ) . "\%"
    );
    if ( $evolution > 0 ) {
        $doc->append( 'signeEvoTotalActimailPagesPourcent', "+" );
    }
    else {
        $doc->append( 'signeEvoTotalActimailPagesPourcent', "" );
    }

    ### Récupération du nombre de documents générés l'année dernière sur la même période sur la base de tracknig
    $sql =
"select count(*) from $edtk_trackingy1 where ED_JOB_EVT = \'D\' AND ED_TSTAMP > $tstamp_fromy1 AND ED_TSTAMP < $tstamp_toy1 AND ED_APP NOT LIKE \'TST\%\' AND ED_APP not like \'\%H9_ged_insert\' AND \(ED_K1_VAL NOT LIKE \'TEST \%\' OR ED_K1_VAL is null\) AND ED_USER not like \'DocsRenouv\'";
    print
"Récupération du nombre de documents générés l'année dernière sur la même période sur la base de h : $sql\n";
    $rows = executeSQLcommand( $dbh, $sql );
    my $totalDocumentsGenereY1 = $$rows[0][0];
    my $augment                = 0;
    if ( $totalDocumentsGenere > $totalDocumentsGenereY1 ) {
        $doc->append( 'evolutionGen', 'augmentation' );
        $doc->append( 'signeevo',     '+' );
        $augment = sprintf( "%0.2f",
            ( $totalDocumentsGenere * 100 / $totalDocumentsGenereY1 ) - 100 );
    }
    else {
        $doc->append( 'evolutionGen', 'diminution' );
        $doc->append( 'signeevo',     '-' );
        $augment = sprintf( "%0.2f",
            ( $totalDocumentsGenereY1 * 100 / $totalDocumentsGenere ) - 100 );
    }
    $doc->append( 'augment',                 $augment );
    $doc->append( 'comptDocumentsGeneresyI', $totalDocumentsGenereY1 );
    $doc->append('editComptageDocGeneres');

    ### Récupération du nombre de documents générés la dernière année sur la base de tracknig par entité juridique
    $sql =
"select ED_CORP, count (*) from $edtktracking where ED_JOB_EVT = \'D\' AND ED_TSTAMP > $tstamp_from AND ED_TSTAMP < $tstamp_to AND ED_APP NOT LIKE \'TST\%\' AND ED_APP not like \'\%H9_ged_insert\' AND \(ED_K1_VAL NOT LIKE \'TEST \%\' OR ED_K1_VAL is null\)  AND ED_USER not like \'DocsRenouv\' group by ED_CORP order by COUNT(*) desc";
    print
"Récupération du nombre de documents générés la dernière année sur la base de tracknig par entité juridique : $sql\n";
    $rows = executeSQLcommand( $dbh, $sql );
    ### Même chose un an avant afin d'avoir l'évolution
    $sql =
"select ED_CORP, count (*) from $edtk_trackingy1 where ED_JOB_EVT = \'D\' AND ED_TSTAMP > $tstamp_fromy1 AND ED_TSTAMP < $tstamp_toy1 AND ED_APP NOT LIKE \'TST\%\' AND ED_APP not like \'\%H9_ged_insert\' AND \(ED_K1_VAL NOT LIKE \'TEST \%\' OR ED_K1_VAL is null\) AND ED_USER not like \'DocsRenouv\' group by ED_CORP order by COUNT(*) desc";
    $rowsy1 = executeSQLcommand( $dbh, $sql );

    ### Récupération du nombre de pages et de documents envoyés à Actimail la dernière année sur la base de lotissement par entité juridique
    $sql =
"SELECT ED_CORP, SUM(NbrPages), COUNT(*) NbrDocs FROM ( SELECT ED_CORP, ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM $edtkindex where ED_DTLOT > \'$tstampActimail_from\' and ED_DTLOT < \'$tstampActimail_to\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_CORP, ED_IDLDOC, ED_SEQDOC ) group by ED_CORP order by NbrDocs desc";
    print
"Récupération du nombre de pages et de documents envoyés à Actimail la dernière année sur la base de lotissement par entité juridique : $sql\n";
    my $rowsLotis = executeSQLcommand( $dbh, $sql );

    ### Récupération du nombre de pages et de documents envoyés à Actimail la dernière année sur la base de lotissement par entité juridique l'année précédente
    $sql =
"SELECT ED_CORP, SUM(NbrPages), COUNT(*) NbrDocs FROM ( SELECT ED_CORP, ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM $edtkindexy1 where ED_DTLOT > \'$tstampActimail_fromy1\' and ED_DTLOT < \'$tstampActimail_toy1\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_CORP, ED_IDLDOC, ED_SEQDOC ) group by ED_CORP order by NbrDocs desc";
    print
"Récupération du nombre de pages et de documents envoyés à Actimail la dernière année sur la base de lotissement par entité juridique : $sql\n";
    my $rowsLotisy1 = executeSQLcommand( $dbh, $sql );

    $doc->append( 'principalEntiteNom',   $$rows[0][0] );
    $doc->append( 'principalEntiteCompt', $$rows[0][1] );
    $doc->append( 'principalEntitePC',
        sprintf( "%0.2f", ( $$rows[0][1] * 100 / $totalDocumentsGenere ) ) );
    $tableauDocGe = oEdtk::TexDoc->new();
    foreach ( 0 ... scalar($#$rows) ) {
        my $row = $$rows[$_];

        # Obtention du nombre de documents générés il y a un an
        $nbrdocy1 = 0;

        # Obtention du nombre de documents envoyés à Actimail
        my $nbrdocActimail   = 0;
        my $nbrdocActimaily1 = 0;

        # Obtention du nombre de pages envoyées à Actimail
        my $nbrpagesActimail   = 0;
        my $nbrpagesActimaily1 = 0;
        foreach ( 0 ... scalar($#$rowsy1) ) {
            my $rowy1 = $$rowsy1[$_];
            if ( $$rowy1[0] eq $$row[0] ) {
                $nbrdocy1 = $$rowy1[1];
                last;
            }
        }

        ### Recherche les infos dans les résultats du lotissement sur l'entité juridique du tracking
        foreach ( 0 ... scalar($#$rowsLotis) ) {
            my $rowLotis = $$rowsLotis[$_];
            if ( $$rowLotis[0] eq $$row[0] ) {
                $nbrdocActimail   = $$rowLotis[2];
                $nbrpagesActimail = $$rowLotis[1];
                last;
            }
        }
        ### Recherche les infos dans les résultats du lotissement sur l'entité juridique du tracking pour l'année -1
        foreach ( 0 ... scalar($#$rowsLotisy1) ) {
            my $rowLotisy1 = $$rowsLotisy1[$_];
            if ( $$rowLotisy1[0] eq $$row[0] ) {
                $nbrdocActimaily1   = $$rowLotisy1[2];
                $nbrpagesActimaily1 = $$rowLotisy1[1];
                last;
            }
        }
        $tableauDocGe->append( 'totalDocsEntActimail',      $nbrdocActimail );
        $tableauDocGe->append( 'totalDocsEntPagesActimail', $nbrpagesActimail );
        $tableauDocGe->append( 'totalDocsEntActimailyI',    $nbrdocActimaily1 );
        $tableauDocGe->append( 'totalDocsEntPagesActimailyI',
            $nbrpagesActimaily1 );
        $evolution =
          ( $nbrdocActimail - $nbrdocActimaily1 ) * 100 / $nbrdocActimaily1;
        $tableauDocGe->append( 'evoActimailDocsPourcent',
            sprintf( "%0.2f", ($evolution) ) . "\%" );
        if ( $evolution > 0 ) {
            $tableauDocGe->append( 'signeEvoActimailDocsPourcent', "+" );
        }
        else {
            $tableauDocGe->append( 'signeEvoActimailDocsPourcent', "" );
        }

        $evolution = ( $nbrpagesActimail - $nbrpagesActimaily1 ) * 100 /
          $nbrpagesActimaily1;
        $tableauDocGe->append( 'evoActimailPagesPourcent',
            sprintf( "%0.2f", ($evolution) ) . "\%" );
        if ( $evolution > 0 ) {
            $tableauDocGe->append( 'signeEvoActimailPagesPourcent', "+" );
        }
        else {
            $tableauDocGe->append( 'signeEvoActimailPagesPourcent', "" );
        }

        $tableauDocGe->append( 'totalDocsEntNbryI', $nbrdocy1 );
        $evolution = ( $$row[1] - $nbrdocy1 ) * 100 / $nbrdocy1;
        $tableauDocGe->append( 'evoPourcent',
            sprintf( "%0.2f", ($evolution) ) . "\%" );
        if ( $evolution > 0 ) {
            $tableauDocGe->append( 'signeEvoPourcent', "+" );
        }
        else {
            $tableauDocGe->append( 'signeEvoPourcent', "" );
        }
        $colorR += 20;
        $colorV += 120;
        $colorB += 180;
        $pc = ( $$row[1] * 100 ) / $totalDocumentsGenere;
        $tableauDocGe->append( 'nomEnt',       convert_EJ( $$row[0] ) );
        $tableauDocGe->append( 'totalDocsEnt', $$row[1] );
        $tableauDocGe->append( 'pourCent',     sprintf( "%0.2f", ($pc) ) );
        $tableauDocGe->append('editRowTableauDocGeEnt');
    }

    ### Pour afficher les entitées juridiques qui ne sont plus d'actualité le dernière année
    foreach ( 0 ... scalar($#$rowsy1) ) {
        my $rowy1 = $$rowsy1[$_];
        foreach ( 0 ... scalar($#$rows) ) {
            my $row = $$rows[$_];
            if ( $$rowy1[0] eq $$row[0] ) {
                goto FOUND_ENTITE;
            }
        }
        my $nomEnt             = $$rowy1[0];
        my $nbrdocy1           = $$rowy1[1];
        my $nbrdocActimaily1   = 0;
        my $nbrpagesActimaily1 = 0;

        ### Recherche les infos dans les résultats du lotissement sur l'entité juridique du tracking pour l'année -1
        foreach ( 0 ... scalar($#$rowsLotisy1) ) {
            my $rowLotisy1 = $$rowsLotisy1[$_];
            if ( $$rowLotisy1[0] eq $$rowy1[0] ) {
                $nbrdocActimaily1   = $$rowLotisy1[2];
                $nbrpagesActimaily1 = $$rowLotisy1[1];
                last;
            }
        }
        ### L'entité n'existe pas, on va donc l'afficher
        $tableauDocGe->append( 'totalDocsEntActimail',      0 );
        $tableauDocGe->append( 'totalDocsEntPagesActimail', 0 );
        $tableauDocGe->append( 'totalDocsEntActimailyI',    $nbrdocActimaily1 );
        $tableauDocGe->append( 'totalDocsEntPagesActimailyI',
            $nbrpagesActimaily1 );
        $tableauDocGe->append( 'evoActimailDocsPourcent',       "" );
        $tableauDocGe->append( 'signeEvoActimailDocsPourcent',  "" );
        $tableauDocGe->append( 'evoActimailPagesPourcent',      "" );
        $tableauDocGe->append( 'signeEvoActimailPagesPourcent', "" );
        $tableauDocGe->append( 'totalDocsEntNbryI',             $nbrdocy1 );
        $tableauDocGe->append( 'evoPourcent',                   "" );
        $tableauDocGe->append( 'signeEvoPourcent',              "" );
        $tableauDocGe->append( 'nomEnt',       convert_EJ($nomEnt) );
        $tableauDocGe->append( 'totalDocsEnt', "0" );
        $tableauDocGe->append( 'pourCent',     "0" );
        $tableauDocGe->append('editRowTableauDocGeEnt');

      FOUND_ENTITE:
    }

    $doc->append( 'tableauDocGeEnt', $tableauDocGe );
    $doc->append('editComptageDocGeneresParEntite');

    ### Récupération du nombre de pages envoyées à Actimail la dernière année sur la base de lotissement par domaine métier
    $sql =
"SELECT ED_SOURCE, SUM(NbrPages), COUNT(*) NbrDocs FROM ( SELECT ED_SOURCE, ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM $edtkindex where ED_DTLOT > \'$tstampActimail_from\' and ED_DTLOT < \'$tstampActimail_to\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_SOURCE, ED_IDLDOC, ED_SEQDOC ) group by ED_SOURCE order by NbrDocs desc";
    print
"Récupération du nombre de pages envoyées à Actimail la dernière année sur la base de lotissement par domaine métier : $sql\n";
    $rows = executeSQLcommand( $dbh, $sql );
    $tableauDocGe = oEdtk::TexDoc->new();
    foreach ( 0 ... scalar($#$rows) ) {
        my $row = $$rows[$_];
        $tableauDocGe->append( 'nomDom', convert_domaine_metier( $$row[0] ) );
        $tableauDocGe->append( 'totalPagesDom', $$row[1] );
        $tableauDocGe->append( 'totalDocsDom',  $$row[2] );
        $pc = ( $$row[1] * 100 ) / $totalDocumentsEnvoyes;
        $tableauDocGe->append( 'pourCent', sprintf( "%0.2f", ($pc) ) );
        $tableauDocGe->append('editRowTableauDocEnvDom');
    }
    $doc->append( 'tableauDocGeEnvDom', $tableauDocGe );
    $doc->append('editComptageDocEnvoyesParDomaine');

    ### Récupération du nombre de documents générés la dernière année sur la base de tracknig par application et création d'un camembert
    $sql =
"select ED_APP, count(*) from $edtktracking where ED_JOB_EVT = \'D\' AND ED_TSTAMP > $tstamp_from AND ED_TSTAMP < $tstamp_to AND ED_APP NOT LIKE \'TST\%\' AND ED_APP not like \'\%H9_ged_insert\' AND \(ED_K1_VAL NOT LIKE \'TEST \%\' OR ED_K1_VAL is null\) AND ED_USER not like \'DocsRenouv\' GROUP BY ED_APP order by COUNT(*) desc";
    print
"Récupération du nombre de documents générés la dernière année sur la base de tracknig par application et création d'un camembert : $sql\n";
    $rows = executeSQLcommand( $dbh, $sql );
    ### Même chose un an avant afin d'avoir l'évolution
    $sql =
"select ED_APP, count(*) from $edtk_trackingy1 where ED_JOB_EVT = \'D\' AND ED_TSTAMP > $tstamp_fromy1 AND ED_TSTAMP < $tstamp_toy1 AND ED_APP NOT LIKE \'TST\%\' AND ED_APP not like \'\%H9_ged_insert\' AND \(ED_K1_VAL NOT LIKE \'TEST \%\' OR ED_K1_VAL is null\) AND ED_USER not like \'DocsRenouv\' GROUP BY ED_APP order by COUNT(*) desc";
    $rowsy1 = executeSQLcommand( $dbh, $sql );

    ### Récupération du nombre de pages envoyées à Actimail la dernière année sur la base de lotissement par application
    $sql =
"SELECT ED_REFIDDOC, SUM(NbrPages), COUNT(*) NbrDocs FROM ( SELECT ED_REFIDDOC, ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM $edtkindex where ED_DTLOT > \'$tstampActimail_from\' and ED_DTLOT < \'$tstampActimail_to\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_REFIDDOC, ED_IDLDOC, ED_SEQDOC ) group by ED_REFIDDOC order by NbrDocs desc";
    print
"Récupération du nombre de pages envoyées à Actimail la dernière année sur la base de lotissement par application : $sql\n";
    $rowsLotis = executeSQLcommand( $dbh, $sql );
    ### Même chose un an avant afin d'avoir l'évolution
    $sql =
"SELECT ED_REFIDDOC, SUM(NbrPages), COUNT(*) NbrDocs FROM ( SELECT ED_REFIDDOC, ED_IDLDOC, ED_SEQDOC, COUNT(*) NbrPages FROM $edtkindexy1 where ED_DTLOT > \'$tstampActimail_fromy1\' and ED_DTLOT < \'$tstampActimail_toy1\' and ED_REFIDDOC not like \'TST\%\' and ED_IDLOT not like \'\%_d\%mat\%\' GROUP BY ED_REFIDDOC, ED_IDLDOC, ED_SEQDOC ) group by ED_REFIDDOC order by NbrDocs desc";
    print
"Récupération du nombre de pages envoyées à Actimail la dernière année sur la base de lotissement par application : $sql\n";
    $rowsLotisy1 = executeSQLcommand( $dbh, $sql );

    $camembertDocGe = oEdtk::TexDoc->new();
    $tableauDocGe   = oEdtk::TexDoc->new();
    $totalptcs      = 0;
    $ptcs           = 0;
    $pc             = 0;
    $colorR         = 0;
    $colorV         = 0;
    $colorB         = 0;
    foreach ( 0 ... $#$rows ) {
        my $row = $$rows[$_];
        my $evolution;
        my $nbrdocActimaily1   = 0;
        my $nbrpagesActimaily1 = 0;

        # Obtention du nombre de documents générés il y a un an
        $nbrdocy1 = 0;

        # Obtention du nombre de documents envoyés à Actimail
        my $nbrdocActimail = 0;

        # Obtention du nombre de pages envoyées à Actimail
        my $nbrpagesActimail = 0;
        foreach ( 0 ... scalar($#$rowsy1) ) {
            my $rowy1 = $$rowsy1[$_];
            if ( $$rowy1[0] eq $$row[0] ) {
                $nbrdocy1 = $$rowy1[1];
                last;
            }
        }

        foreach ( 0 ... scalar($#$rowsLotis) ) {
            my $rowLotis = $$rowsLotis[$_];
            if ( $$rowLotis[0] eq $$row[0] ) {
                $nbrdocActimail   = $$rowLotis[2];
                $nbrpagesActimail = $$rowLotis[1];
                last;
            }
        }
        foreach ( 0 ... scalar($#$rowsLotisy1) ) {
            my $rowLotisy1 = $$rowsLotisy1[$_];
            if ( $$rowLotisy1[0] eq $$row[0] ) {
                $nbrdocActimaily1   = $$rowLotisy1[2];
                $nbrpagesActimaily1 = $$rowLotisy1[1];
                last;
            }
        }
        $tableauDocGe->append( 'totalDocsAppActimail',      $nbrdocActimail );
        $tableauDocGe->append( 'totalDocsAppPagesActimail', $nbrpagesActimail );
        $tableauDocGe->append( 'totalDocsAppActimailyI',    $nbrdocActimaily1 );
        $tableauDocGe->append( 'totalDocsAppPagesActimailyI',
            $nbrpagesActimaily1 );

        ### Calcul de l'évolution des docs et pages au façonnier
        if ( $nbrdocActimaily1 != 0 ) {
            my $evolution =
              ( $nbrdocActimail - $nbrdocActimaily1 ) * 100 / $nbrdocActimaily1;
            $tableauDocGe->append( 'evoDocsFacoPourcent',
                sprintf( "%0.2f", ($evolution) ) . "\%" );
            if ( $evolution > 0 ) {
                $tableauDocGe->append( 'signeEvoDocsFacoPourcent', "+" );
            }
            else {
                $tableauDocGe->append( 'signeEvoDocsFacoPourcent', "" );
            }
            $evolution = ( $nbrpagesActimail - $nbrpagesActimaily1 ) * 100 /
              $nbrpagesActimaily1;
            $tableauDocGe->append( 'evoPagesFacoPourcent',
                sprintf( "%0.2f", ($evolution) ) . "\%" );
            if ( $evolution > 0 ) {
                $tableauDocGe->append( 'signeEvoPagesFacoPourcent', "+" );
            }
            else {
                $tableauDocGe->append( 'signeEvoPagesFacoPourcent', "" );
            }
        }
        else {
            $tableauDocGe->append( 'evoDocsFacoPourcent',       " " );
            $tableauDocGe->append( 'signeEvoDocsFacoPourcent',  " " );
            $tableauDocGe->append( 'evoPagesFacoPourcent',      " " );
            $tableauDocGe->append( 'signeEvoPagesFacoPourcent', " " );
        }

        $tableauDocGe->append( 'totalDocsAppNbryI', $nbrdocy1 );
        if ( $nbrdocy1 != 0 ) {
            my $evolution = ( $$row[1] - $nbrdocy1 ) * 100 / $nbrdocy1;
            $tableauDocGe->append( 'evoPourcent',
                sprintf( "%0.2f", ($evolution) ) . "\%" );
            if ( $evolution > 0 ) {
                $tableauDocGe->append( 'signeEvoPourcent', "+" );
            }
            else {
                $tableauDocGe->append( 'signeEvoPourcent', "" );
            }
        }
        else {
            $tableauDocGe->append( 'evoPourcent',      " " );
            $tableauDocGe->append( 'signeEvoPourcent', " " );
        }
        $colorR += 20;
        $colorV += 120;
        $colorB += 180;
        $pc = ( $$row[1] * 100 ) / $totalDocumentsGenere;
        $tableauDocGe->append( 'nomApp',       $$row[0] );
        $tableauDocGe->append( 'libelleApp',   convert_app_name( $$row[0] ) );
        $tableauDocGe->append( 'totalDocsApp', $$row[1] );
        $tableauDocGe->append( 'pourCent',     sprintf( "%0.2f", ($pc) ) )
          ;    #+0.005)));
        $tableauDocGe->append('editRowTableauDocGeApp');
        $ptcs = ( $$row[1] * 360 ) / $totalDocumentsGenere;

        if ( $ptcs > 0.5 ) {
            $camembertDocGe->append( 'pourTroisCentSoixante',
                sprintf( "%1.0f", ( $ptcs + 0.5 ) ) );
            $camembertDocGe->append(
                'pourTroisCentSoixanteSurDeux',
                sprintf( "%1.0f", ( $ptcs + 0.5 ) / 2 )
            );
            $camembertDocGe->append( 'pourCent',
                sprintf( "%0.2f", ( $pc + 0.005 ) ) );
            $camembertDocGe->append( 'libellePTCS', $$row[0] );
            $camembertDocGe->append( 'colorR',      $colorR % 255 );
            $camembertDocGe->append( 'colorV',      $colorV % 255 );
            $camembertDocGe->append( 'colorB',      $colorB % 255 );
            $camembertDocGe->append('editPortionCamembertApp');
        }
    }
    $doc->append( 'tableauDocGeApp',   $tableauDocGe );
    $doc->append( 'camembertDocGeApp', $camembertDocGe );
    $doc->append('editComptageDocGeneresParApp');

    oe_print_to_output_file("$doc");

    oe_compo_link();

}

sub executeSQLcommand {
    my ( $base, $requete ) = @_;
    my $sth = $base->prepare($requete);
    $sth->execute();    # Execute la requete

    my $rows = $sth->fetchall_arrayref()
      ;                 # Va récupérer les résultats renvoyés par la base
    return $rows;
}

sub convert_app_name() {
    my $scalar = shift;

    if ( !defined($dico_app) ) {
        my $section = 'APPS';
        my $cfg     = config_read();
        $dico_app =
          oEdtk::Dict->new( $cfg->{'EDTK_DICO_APPS'},,
            { section => $section } );
    }
    $scalar = $dico_app->translate($scalar);

    return $scalar;
}

sub convert_EJ() {
    my $scalar = shift;

    if ( !defined($dico_ej) ) {
        my $section = 'ENTITE_JURIDIQUE';
        my $cfg     = config_read();
        $dico_ej =
          oEdtk::Dict->new( $cfg->{'EDTK_DICO_APPS'},,
            { section => $section } );
    }
    $scalar = $dico_ej->translate($scalar);

    return $scalar;
}

sub convert_domaine_metier() {
    my $scalar = shift;

    if ( !defined($dico_dm) ) {
        my $section = 'DOMAINE_METIER';
        my $cfg     = config_read();
        $dico_dm =
          oEdtk::Dict->new( $cfg->{'EDTK_DICO_APPS'},,
            { section => $section } );
    }
    $scalar = $dico_dm->translate($scalar);

    return $scalar;
}

main();
