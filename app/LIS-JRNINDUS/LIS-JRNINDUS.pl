#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::libXls 0.46;
use strict;
use oEdtk::Tracking;

my $TRK;

#################################################################################
# CORPS PRINCIPAL DE L'APPLICATION :
#################################################################################

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => 'CSV',
        entity => oe_corporation_set(),
        keys   => [ 'IDDEST', 'AGENCE' ]
    );

    # OUVERTURE DES FLUX
    oe_open_fi_IN( $argv[0] );

    # INITIALISATION ET CARTOGRAPHIE DE L'APPLICATION
    # avec initialisation propre au document
    &initApp();

    while ( my $ligne = <IN> ) {
        chomp($ligne);
        oe_trimp_space($ligne);
        @DATATAB = split( /\;/, $ligne );
        &trt_ENR();

        # prod_Xls_Edit_Ligne();
    }

    $TRK->track( 'Doc', 1, '', 'SIEGE' );
    &sortie_triee();
    return 0;
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################
# DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION
my %h_Tri_Ligne;
my $sequence = 0;

sub initApp {

    # intialisation des définitions d'enregistrements

    # INITIALISATION PROPRE AU DOCUMENT
    &initDoc();
    1;
}

sub initDoc {
    ###########################################################################
    # CONFIGURATION DU DOCUMENT EXCEL
    ###########################################################################
    #
    # 	OPTIONNEL : FORMATAGE PAR DEFAUT DES COLONNES DU TABLEAU EXCEL
    # 	(AC7 = Alpha Center 7 de large ; Ac7 = Alpha Center Wrap... ;
    #	 NR7 = Numérique Right ; AL12 = Alpha Left 12 de large...  )
    #
    prod_Xls_Col_Init(
        'AC12', 'AL20', 'AC12', 'AC10', 'AL40', 'AC12',
        'AC5',  'NR10', 'AC10'
    );

    ###########################################################################
    # 	REQUIS !
    # 	OUVERTURE ET CONFIGURATION DU DOCUMENT
    #	prod_Xls_Init permet d'ouvrir un fichier XLS
    # 		le paramètre 1 est obligatoire (nom fichier)
    #		les paramètres suivants sont optionnels
    ###########################################################################
    #
    prod_Xls_Init( "", "Journal des Indus" );    #  ,"ENTETE DROIT");

    # INITIALISATIONS PROPRES A LA MISE EN FORME DU DOCUMENT
    # PRÉPARATION DES TITRES DE COLONNES
    prod_Xls_Insert_Val("N° ADH");
    prod_Xls_Insert_Val("NOM");
    prod_Xls_Insert_Val("ENTITÉ RATT.");
    prod_Xls_Insert_Val("N° PRATICIEN");
    prod_Xls_Insert_Val("NOM PRATICIEN");
    prod_Xls_Insert_Val("CONSTATA TION INDU");
    prod_Xls_Insert_Val("Type");
    prod_Xls_Insert_Val("SOLDE");
    prod_Xls_Insert_Val("DOMAINE");

    # ETC.

#EDITION DE LA TETE DE COLONNE (les paramètres sont optionnels)
# 	le paramètre 1 est le style pour la ligne, 'HEAD' déclare la ligne en tête de tableau
    prod_Xls_Edit_Ligne( 'T2', 'HEAD' );

    1;
}

sub trt_ENR() {
    my $entite = $DATATAB[2] . $DATATAB[5];
    my $nom    = $DATATAB[1];

# LES MONTANTS DANS LE FICHIER DE DONNÉES ONT DES VIRGULES COMME SÉPARATEUR DE DÉCIMAL,
#  EXCEL ATTEND DES POINTS
    oe_num2txt_us( $DATATAB[6] );
    push(
        @{ $h_Tri_Ligne{ $entite . $nom . $DATATAB[0] . $sequence++ } },
        $DATATAB[0], $nom,        $entite,     $DATATAB[3], $DATATAB[4],
        $DATATAB[7], $DATATAB[8], $DATATAB[6], $DATATAB[10]
    );

    1;
}

sub sortie_triee() {

    foreach my $membre ( sort keys %h_Tri_Ligne ) {
        prod_Xls_Insert_Val( @{ $h_Tri_Ligne{$membre} } );
        prod_Xls_Edit_Ligne();
    }
    1;
}

exit main(@ARGV);
