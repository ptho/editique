#!/usr/bin/perl
use utf8;

###################################################################################################################
# Date : 27/07/2012
# Projet : PREVOYANCE CIBLE /LOT 1.2
# Nom Application : PRACD-AC082
# Flux entrée : AC082/AC084
# En sorite : AVIS D'APPEL DE COTISATIONS PREVOYANCE COLLECTIVE
#
# Description de fonctionnement :
#	- Logo afiiché en fonction d'entité juridique
#	- Adresse paiement : Mutacite ou Mérignac ou Villeneuve d'asc ou siege pour les mutuelles partenaires
#	- regroupement des groupes d'assués de même n°employeur et N° contrat (sur l'avis, cela correspond à réf sans le numéro de groupe d'assuré, exemple : 174004-PMS)
#	- les taux sont cumulés pour le même groupe mais si la garantie PDC ou POB alors laisser vide (blanc)
###################################################################################################################
# 19/02/2013   Sésame évolution N° 1003490
# 	Evolution demande séparer les produits sur les lignes différentes.
##########################################################################################################################
# Evolution Date : 15/04/2013
# Demande sésame Interne DSI: 1010018  (projet piloté par l'équipe "Développement spécifique" DS)
# Description : L'équipe DS ajoute deux enregistrements OD, OE dans le flux qui apporte des informations sur l'adresse de CTC et de relevé indentité bancaire (RIB)
#			L'état éditique est évolué pour prendre en compte l'adresse CTC à partir de cette enregistrement OD et non pas de son fichier paramètre.
#			et aussi pour afficher le RIB à la fin de de bordereau
##########################################################################################################################
#

use oEdtk::Main;
use oEdtk::Run;
use oEUser::Lib;

#use oEUser::Lib2;
use oEdtk::RecordParser;
use oEdtk::Spool;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Data::Dumper;
use File::Basename;
use oEUser::Descriptor::ENTETE;
use oEUser::Descriptor::C1;
use oEUser::Descriptor::C2;
use oEUser::Descriptor::G9;
use oEUser::Descriptor::GA;
use oEUser::Descriptor::GB;
use oEUser::Descriptor::GC;
use oEUser::Descriptor::GD;
use oEUser::Descriptor::GE;
use oEUser::Descriptor::GF;
use oEUser::Descriptor::OD;
use oEUser::Descriptor::OE;
use strict;
use oEdtk::Config qw(config_read);
my ( $TRK, $AGENCE );

my $NUMEMP;
my $OPTION;
my $TAUX;
my $CUMTAUX = 0;
my $NUMCCO;
my $CODGRP;
my $NUMGRP;
my $NOM;
my $CODPOS;
my $DOC;
my $INDEX;
my $ECHEANCE;
my $DEBPER;
my $FINPER;
my $DEBUT_GB;
my @DADDR;
my @EADDR;
my $EDITION_OK = 0;
my $EDITION_KO = 0;
my $FLAG_DOC   = 1;
my $CPTC1      = 0;
my $CGEST;
my $CPTGB = 0;
my %T_GA
  ; # tableau qui sauvegarde et regroupe les groupes d'assuré de même contrat et même employeur mais de num de groupes d'assuré différents

#my $INDICE_T_GA=0; # indice du tableau T_GA
my %RIB;
my @EADDR = ();

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );

    #warn"\n argv[0]=$argv[0]; argv[1]=$argv[1] \n";
    my $user    = user_get_aneto_user( $argv[0] );
    my $opt_ged = "";
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'xIDEMET', 'NUMGRP', 'xIDDEST', 'xCPDEST' ]
    );

    # trame keys => ['IDDEST', 'AGENCE', 'NOMDEST', 'COMPTE', 'INDUS']

    oe_new_job('--index');

    my $C1 = oEUser::Descriptor::C1->get();

    #$C1->debug();
    $C1->bind_all_c7();
    my $C2 = oEUser::Descriptor::C2->get();

    #$C2->debug();
    $C2->bind_all_c7();
    my $OD = oEUser::Descriptor::OD::get();

    #	$OD->debug();
    $OD->bind_all_c7();
    my $OE = oEUser::Descriptor::OE::get();

    #	$OE->debug();
    $OE->bind_all_c7();
    my $GA = oEUser::Descriptor::GA->get();

    #	$GA->debug();
    $GA->bind_all_c7();
    my $GB = oEUser::Descriptor::GB->get();

    #$GB->debug();
    $GB->bind_all_c7();
    my $GC = oEUser::Descriptor::GC->get();
    $GC->bind_all_c7();
    my $GD = oEUser::Descriptor::GD->get();
    $GD->bind_all_c7();
    my $GE = oEUser::Descriptor::GE->get();
    $GE->bind_all_c7();
    my $GF = oEUser::Descriptor::GF->get();
    $GF->bind_all_c7();
    my $G9 = oEUser::Descriptor::G9->get();

    #$G9->debug();
    $G9->bind_all_c7();

    $DOC   = oEdtk::TexDoc->new();
    $INDEX = oEdtk::TexDoc->new();
    $DOC->append( 'xTYPDOC', 'ACOT' );    ### Type de document

    my $p = oEdtk::RecordParser->new(
        \*IN,
        'ENTETE' => undef,
        'C1'     => $C1,
        'C2'     => $C2,
        'GA'     => $GA,
        'GB'     => $GB,
        'GC'     => $GC,
        'GD'     => $GD,
        'GE'     => $GE,
        'GF'     => $GF,
        'G9'     => $G9,
        'OD'     => $OD,
        'OE'     => $OE
    );
    my $firstGA = 0;

    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'C1' ) {
            $firstGA = 0;
            if ( $CPTC1 > 0 ) {
                detail();
                finDoc();
            }

            $CPTC1++;
            prepC1( $vals, $DOC );

        }
        elsif ( $id eq 'C2' ) {
            prepC2( $vals, $DOC );

        }
        elsif ( $id eq 'GA' ) {
            if ( $firstGA == 0 ) {    # si le premier GA
                if ( $vals->{'LIBLONC'} eq "Adhérent" ) {
                    $vals->{'LIBLONC'} = "Membre participant";
                }
                $DOC->append( 'PAYEUR', $vals->{'LIBLONC'} );
            }
            prepGA( $vals, $DOC );
            $firstGA++;

        }
        elsif ( $id eq 'GB' ) {

            # 			warn "INFO : dump GB ". Dumper($vals) ."\n";
            prepGB( $vals, $DOC );

        }
        elsif ( $id eq 'GC' ) {
        }
        elsif ( $id eq 'GD' ) {
        }
        elsif ( $id eq 'GE' ) {
        }
        elsif ( $id eq 'GF' ) {
        }
        elsif ( $id eq 'G9' ) {

            #warn "INFO : dump E1 ". Dumper($vals) ."\n";
            prepG9( $vals, $DOC );
            $DOC->append( 'ECHEANCE', $ECHEANCE );

            #			if ($firstGA == 1){
            $DOC->append('EditCHQ');

            #			}
        }
        elsif ( $id eq 'OD' ) {
            prepOD($vals);
        }
        elsif ( $id eq 'OE' ) {
            prepOE($vals);
        }

    }
    detail();
    finDoc();
    oe_compo_link();
    return 0;
}

sub convertDate {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;
    return $refVar;
}

sub detail() {
    my $y = 0;
    my ( $codPro, $Codcontrat, $codGrAss, $coddebper, $codfinper );
    $DOC->append('DETADH');

    foreach my $k ( sort ( { $T_GA{$a} <=> $T_GA{$b} } keys %T_GA ) ) {

        # # 		warn"clé = $cle \n";
        ( $codPro, $Codcontrat, $codGrAss, $coddebper, $codfinper ) =
          split( /\|/, $k );

        # 		warn"codPro,codGrAss =$codPro,$codGrAss   $val  \n";
        $NUMGRP = $NUMCCO . "/" . $codGrAss;    #   $CODGRP;
        $DOC->append( 'REF',    $NUMGRP );
        $DOC->append( 'OPTION', $codPro );
        if ( ( $NUMGRP =~ /POB/ ) || ( $NUMGRP =~ /PDC/ ) ) {
            $DOC->append( 'TAUX', ' ' );
        }
        else {
            $DOC->append( 'TAUX', $T_GA{$k} || 0 );
        }

        $DOC->append( 'PERIODEa', $coddebper );
        $DOC->append( 'PERIODEb', $codfinper );

        if ( ( $NUMGRP =~ /POB/ ) || ( $NUMGRP =~ /PDC/ ) ) {
            $DOC->append('TABGATAUX');
        }
        else {
            $DOC->append('TABGA');
        }
        $y++;
    }
    $DOC->append('FERMER');
    $DOC->append( 'BANQUE',        $RIB{'BANQUE'} );
    $DOC->append( 'GUICHET',       $RIB{'GUICHET'} );
    $DOC->append( 'COMPTE',        $RIB{'COMPTE'} );
    $DOC->append( 'CLE',           $RIB{'CLE'} );
    $DOC->append( 'DEVISE',        $RIB{'DEVISE'} );
    $DOC->append( 'DOMICILIATION', $RIB{'DOMICILIATION'} );
    $DOC->append( 'IBAN',          $RIB{'IBAN'} );
    $DOC->append( 'BIC',           $RIB{'BIC'} );
    $DOC->append('FINADH');
    undef %T_GA;
}

sub finDoc() {
    $TRK->track( 'Doc', 1, $AGENCE, $NUMGRP, $NUMEMP, $CODPOS );
    oe_print_to_output_file($INDEX);
    oe_print_to_output_file($DOC);

    $DOC   = oEdtk::TexDoc->new();
    $INDEX = oEdtk::TexDoc->new();
}

sub prepC1 {
    my ( $vals, $DOC ) = @_;
    $AGENCE = $vals->{'ODERCOD'};

    #warn "INFO C1: dump ". Dumper($vals) ."\n";
    #ADRESSE EMETTEUR
    #	my @EADDR = ();
    push( @EADDR, "$vals->{'PENO025'}" );
    push( @EADDR, $vals->{'PNTR026'} );
    push( @EADDR, $vals->{'CPLA027'} );
    push( @EADDR,
"$vals->{'PEAD028'} $vals->{'PEAD029'} $vals->{'PEVO031'} $vals->{'LIBV033'}"
    );
    push( @EADDR, "$vals->{'BOIP035'}" );
    push( @EADDR, "$vals->{'CODC037'} $vals->{'NOMC038'}" );
    @EADDR   = user_cleanup_addr(@EADDR);
    $CUMTAUX = 0;

    #	my @adr_CTC = user_get_adr_ret('PREVOYANCE',$AGENCE);
    #	if (join (' ', @adr_CTC)=~/\s+33\d{3}\s+/){
    #warn "DEBUG: mise en place code rupture CTC33 \n";
    #		$DOC->append('xCODRUPT', 'CTC33');
    #	}

#	if ($adr_CTC[0] eq 'NULL' || oe_corporation_set() eq 'MUTACITE' ) { #
#		$DOC->append_table	('ADDRSECT',	@EADDR);
#		$DOC->append		('EADDR',		'ADDRSECT');
#		$DOC->append('retours', "Un exemple de ce document est à renvoyer à l'agence après validation.");
#		$DOC->append_table	('AdrCTC',	@EADDR);
#	} else {
#		$DOC->append_table	('ADDRCTC',	@adr_CTC);
#		$DOC->append		('EADDR',		'ADDRCTC');
#		$DOC->append('retours', "Un exemplaire de ce document est à renvoyer à l'adresse suivante :");
#		$DOC->append_table	('AdrCTC',	@adr_CTC);
#	}

    #warn "INFO : $AGENCE => @adr_CTC \n";
    oe_iso_country( $vals->{'PEPALIC'} );
    $vals->{'PEPALIC'} =~ s/FRANCE//i;

    #ADRESSE DESTINATAIRE
    my @DADDR = ();

    push( @DADDR, "$vals->{'PENOCOD'} $vals->{'PENOLIB'}" );
    push( @DADDR, $vals->{'PNTREMx'} );
    push( @DADDR, $vals->{'CPLADRx'} );
    push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );
    push( @DADDR, "$vals->{'BOIPOSL'}" );
    push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'DADDR', @DADDR );
    $INDEX->append_table( 'xADRLN', @DADDR );

    $CGEST  = $vals->{'ODCGCOD'};
    $NUMGRP = $vals->{'PENO116'};
    $NUMEMP = $vals->{'PENONUM'};

    if ( $CGEST =~ /-PREV/ ) {
        $DOC->append( 'NUMGRP', $NUMEMP );
    }
    else {
        $DOC->append( 'NUMGRP', $CGEST );
    }

    $DOC->append( 'NUMEMP', $NUMEMP );
    $NOM    = $vals->{'PENOLIB'};
    $CODPOS = $vals->{'CODCDXC'};

    $INDEX->append( 'xIDDEST',  $NUMEMP );
    $INDEX->append( 'xNOMDEST', $NOM );
    $INDEX->append( 'xIDEMET',  $vals->{'ODERCOD'} );
    $INDEX->append( 'xCPDEST',  $vals->{'CODCDXC'} );
    $INDEX->append( 'xVILDEST', $vals->{'NOMCDXL'} );

    #warn "INFO : TEST DE PASSAGE.!!!!!!$CGEST!!!!!!!!!!!!!!!!!!!!!!!!!\n";
}

sub prepC2 {
    my ( $vals, $DOC ) = @_;
    $NUMCCO = $vals->{'ACCCNUM'};
}

############################################################################################
# L'enregistrement nous apporte 8 champs et afficher sur 7 champs sur notre pavé d'adresse
# on affiche sur le bordereau de l'adresse de CTC et d'émetteur
###############################################################################################
sub prepOD {
    my ($vals) = @_;
    my @adr_CTC;

    #warn "INFO OD: dump ". Dumper($vals) ."\n";

    if ( $vals->{'LIGNExx'} ne '' ) { push( @adr_CTC, $vals->{'LIGNExx'} ); }
    if ( $vals->{'LIGN003'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN003'} );
    }
    if ( $vals->{'LIGN004'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN004'} );
    }
    if ( $vals->{'LIGN005'} ne '' ) {
        push( @adr_CTC, $vals->{'LIGN005'} );
    }
    if ( $vals->{'LIGN006'} ne '' ) {    #$adr_CTC[4] = 'NULL';}
        push( @adr_CTC, $vals->{'LIGN006'} );
    }
    $vals->{'CODEPOS'} =~ s/^\s+//;
    $vals->{'CODEPOS'} =~ s/\s+$//;
    $vals->{'VILLExx'} =~ s/^\s+//;
    $vals->{'VILLExx'} =~ s/\s+$//;
    my $CP = $vals->{'CODEPOS'} . ' ' . $vals->{'VILLExx'};
    if ( $CP ne '' ) {
        push( @adr_CTC, $CP );
    }
    $vals->{'PAYSxxx'} =~ s/^\s+//;
    if ( $vals->{'PAYSxxx'} ne '' ) {
        push( @adr_CTC, $vals->{'PAYSxxx'} );
    }
    if ( join( ' ', @adr_CTC ) =~ /\s+33\d{3}\s+/ ) {

        #warn "DEBUG: mise en place code rupture CTC33 \n";
        $DOC->append( 'xCODRUPT', 'CTC33' );
    }

    if ( $adr_CTC[0] eq 'NULL' || oe_corporation_set() eq 'MUTACITE' ) {    #
        $DOC->append_table( 'ADDRSECT', @EADDR );
        $DOC->append( 'EADDR', 'ADDRSECT' );
        $DOC->append( 'retours',
"Un exemple de ce document est à renvoyer à l'agence après validation."
        );
        $DOC->append_table( 'AdrCTC', @EADDR );
    }
    else {
        $DOC->append_table( 'ADDRCTC', @adr_CTC );
        $DOC->append( 'EADDR', 'ADDRCTC' );
        $DOC->append( 'retours',
"Un exemplaire de ce document est à renvoyer à l'adresse suivante :"
        );
        $DOC->append_table( 'AdrCTC', @adr_CTC );
    }

}

sub prepOE {
    my ($vals) = @_;

    #		warn "INFO OE: dump ". Dumper($vals) ."\n";
    $RIB{'BANQUE'}        = $vals->{'BANQUEx'};
    $RIB{'GUICHET'}       = $vals->{'GUICHET'};
    $RIB{'COMPTE'}        = $vals->{'COMPTEx'};
    $RIB{'CLE'}           = $vals->{'CLExxxx'};
    $RIB{'DEVISE'}        = $vals->{'DEVISEx'};
    $RIB{'DOMICILIATION'} = $vals->{'Domicil'};
    $RIB{'IBAN'}          = $vals->{'IBANxxx'};
    $RIB{'BIC'}           = $vals->{'BICxxxx'};
}

sub prepGA {
    my ( $vals, $DOC ) = @_;
    $CODGRP = $vals->{'CODGRAx'};
}

sub prepG9 {
    my ( $vals, $DOC ) = @_;
    if ( $vals->{'ETATxxx'} eq 'GE' ) {    # on édite les états générés
        $ECHEANCE = $vals->{'ECHBORx'};
        $ECHEANCE =~ s/(\d{4})(\d{2})(.*)/$2\/$1/o;

        # 		$DEBPER =convertDate $vals->{'DEBPERx'};
        # 		$FINPER =convertDate $vals->{'FINPERx'};
        $EDITION_OK++;

    }
    else {    # on édite pas les valorisés.
              # check des mises en écart PB (seule les chèques sont édites) :
         #  lorsqu'il y a beaucoup d'EcartPB, Compuset tombe en erreur car il y a trop
         #  d'initialisation de balises sans traitement
         # A priori, les AC014 destinés au traitement ACI ne contiendraient pas de PB (à valider avec PGT)
         #$DOC->append('xIDEMET', '');
         #$DOC->append('xIDDEST', '');

        $DOC->append('EcartPB');
        $EDITION_KO++;
        $FLAG_DOC = 0;
    }
}

sub prepGB {
    my ( $vals, $DOC ) = @_;
    my $Regroupe;
    $CPTGB++;
    $OPTION = $vals->{'LIBC016'};
    $DEBPER = convertDate $vals->{'DEBPRDx'};
    $FINPER = convertDate $vals->{'FINPRDx'};
    $Regroupe =
        $OPTION . '|'
      . $NUMCCO . '|'
      . $CODGRP . '|'
      . $DEBPER . '|'
      . $FINPER
      ; # on associe les trois valeur  pour faire la cle du tableau T_GA : code de produit+ Numero contrat + Code groupe d'assuré. ==> Taux
    warn "INFO: regroupe = $Regroupe \n";

    if ( exists( $T_GA{$Regroupe} ) ) {
        $T_GA{$Regroupe} += $vals->{'VALTXxx'};    # Dans GB
    }
    else {
        $T_GA{$Regroupe} = $vals->{'VALTXxx'};     # Dans GB
    }

}

exit main(@ARGV);
