#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::Tracking;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::D1;
use Data::Dumper;
use oEdtk::TexDoc;

my $TRK;

#my $TYPE="";
my $doc;

# Variables pour le tracking
my $numad;
my $agence;
my $nomdest;
my $NOMPREN;
my $montant;

# intégré dans oEUSer::Lib
#sub user_cleanup_addr {
#	my @lines = @_;
#
#	@lines = grep(!/^\s*$/, @lines);
#	@lines = map { s/^\s+//; s/\s+/ /; $_ } @lines;
#	return @lines;
#}

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    $TRK =
      oEdtk::Tracking->new( $argv[1],
        keys => [ 'NUMADH', 'AGENCE', 'NOMDEST', 'CONTRAT', 'MONTANT' ] );
    $doc = initDoc();
    oe_new_job();

    my $E1 = oEUser::Descriptor::E1->get();
    $E1->bind_all_c7();
    my $D1 = oEUser::Descriptor::D1->get();

    #$D1->debug();
    $D1->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();
    $E7->debug();
    $E7->bind_all_c7();
    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'LCHD' );    ### Type de document
    my $p =
      oEdtk::RecordParser->new( \*IN, 'E1' => $E1, 'E7' => $E7, 'D1' => $D1 );

    my $lastid;
    while ( my ( $id, $vals ) = $p->next() ) {
        if ( $id eq 'E1' ) {
            $doc->append('DEBUT');
            $NOMPREN = (
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
            );

#ADRESSE DESTINATAIRE
# 		$vals->{'LICCODP'} =~ s/FRANCE//i;
# 		my @DADDR = ();
# 			push(@DADDR, "$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}");
# 			push(@DADDR, $vals->{'PNTREMx'});
# 			push(@DADDR, $vals->{'CPLADRx'});
# 			push(@DADDR, "$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}");
# 			push(@DADDR, "$vals->{'CODCDXC'} $vals->{'LIBLOCL'}");
# 			if ($vals->{'INDCDXx'} == 1) {
# 				push(@DADDR, "$vals->{'BOIPOSL'} $vals->{'NOMCDXL'}");
# 			} else {
# 				push(@DADDR, $vals->{'BOIPOSL'});
# 			}
# 			push(@DADDR, $vals->{'LICCODP'});
# 			@DADDR = user_cleanup_addr(@DADDR);
# 			$doc->append_table('DADDR', @DADDR);
# 			$doc->append('VILLE', $vals->{'LIBL068'});
# 			$doc->append($vals);
# 			$doc->append('ENDADH');
        }
        elsif ( $id eq 'E7' ) {
            ############BLOC ADRESSE DESTINATAIRE##################
            my @DADDR = ();
            push( @DADDR, $NOMPREN );
            push( @DADDR, $vals->{'PNTREMx'} );
            push( @DADDR, $VALS->{'CPLADRx'} );
            push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
            );
            push( @DADDR, $vals->{'BOIPOSL'} );
            push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
            oe_iso_country( $vals->{'LICCODP'} );
            $vals->{'LICCODP'} =~
              s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
            push( @DADDR, $vals->{'LICCODP'} );
            @DADDR = user_cleanup_addr(@DADDR);
            $doc->append_table( 'DADDR', @DADDR );
            $doc->append( 'VILLE', $vals->{'LIBL036'} );
            $doc->append('ENDADH');
            warn "INFO : dump DADDR " . Dumper(@DADDR) . "\n";
        }
        elsif ( $id eq 'D1' ) {

            #warn "INFO : D1 dump ". Dumper($vals) ."\n";
            $numad   = $vals->{'ACCINUM'};
            $agence  = $vals->{'CODERAx'};
            $nomdest = $vals->{'DCADNOM'} . ' ' . $vals->{'DCADPRN'};
            $montant = $vals->{'DCPHMNT'};
            $doc->append('DEBCPT');

            $doc->append( 'NUMADH',  $vals->{'ACCI046'} );
            $doc->append( 'DCADNOM', $vals->{'DCAD036'} );

            if ( $vals->{'DCCDTYP'} !~ s/FR/T/ ) { $vals->{'DCCDTYP'} = 'A'; }
            my $ligneTexteCheque = $vals->{'LETTREx'};

#			if(length($ligneTexteCheque) < 30){
#				my $my_star = ' '.'*'x20;
#				$ligneTexteCheque .= $my_star;
#			}
#			$ligneTexteCheque.=' ';
# on ne passe pas par la commande ligne de suite de LaTeX %\hspace{200pt}\LETTREx\starsfill
# car il la gère sur la ligne de base courante et nous la voulons sur 2 lignes
            for ( my $it = length($ligneTexteCheque) ; $it < 90 ; $it += 2 ) {
                $ligneTexteCheque .= ' *';
            }
            $doc->append( 'ligneCheq', $ligneTexteCheque );
            $doc->append($vals);
            $doc->append('ENDCPT');
            $TRK->track( 'Doc', 1, $numad, $agence, $nomdest, '', $montant );
        }

        $lastid = $id;
    }
    $doc->append('DOC');
    oe_print_to_output_file($doc);
    $doc->reset();
    oe_compo_link();
    return 0;
}

sub initDoc {
    my $doc = oEdtk::TexDoc->new;
    $doc->append('DEBUT');
    return $doc;
}

exit main(@ARGV);
