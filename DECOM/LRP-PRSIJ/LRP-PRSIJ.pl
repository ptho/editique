#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use POSIX qw(strftime);
use Date::Calc qw(Today Gmtime Week_of_Year)
  ;    # http://search.cpan.org/~stbey/Date-Calc-6.3/lib/Date/Calc.pod
use strict;

my ( $TRK, $IDDEST, $AGENCE, $NOMDEST, $MONTANTci, $MONTANTcii, $ANNEE );

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Local',
        entity => oe_corporation_set(),
        keys   => [ 'xIDDEST', 'AGENCE', 'xNOMDEST', 'MONTANT' ]
    );

    oe_new_job('--index');
    my $doc = oEdtk::TexDoc->new();
    $doc->append( 'xTYPDOC', 'RELP' );    ### Type de document

    my $time = time;
    my ( $year, $month, $day, $hour, $min, $sec, $doy, $dow, $dst ) =
      Gmtime($time);

# SOLUTION POUR RÉCUPÉRER L'ANNÉE DU RELEVÉ DE PRESTATIONS SI LE CHAMPS DATE/ANNEE N'EST PAS ALIMENTÉ
    $ANNEE = $year - 1;

    #warn "INFO ANNEE = $ANNEE \n";

    while ( my $ligne = <IN> ) {
        chomp($ligne);

        my @values = unpack(
            'A4 A3 A15 A45 A45 A45 A45 A45 A45 A45 A45 A18 A1 A2 A12 A12 A6 A*',
            $ligne
        );
        $doc->append( 'DEBUT', '' );

        $doc->append( 'ANNEE', $values[0] || $ANNEE );
        $doc->append( 'CAISSE',    $values[1] );
        $doc->append( 'TEL',       $values[2] );
        $doc->append( 'ETA',       $values[3] );
        $doc->append( 'ADRETA',    $values[4] );
        $doc->append( 'ADRETB',    $values[5] );
        $doc->append( 'xNOMDEST',  $values[6] );
        $doc->append( 'ADRDEST',   $values[7] );
        $doc->append( 'ADRDESTI',  $values[8] );
        $doc->append( 'ADRDESTII', $values[9] );
        $doc->append( 'ADRDESIII', $values[10] );
        $doc->append( 'INSEE',     $values[11] );
        $doc->append( 'IGNORE',    $values[12] );
        $doc->append( 'CLESS',     $values[13] );
        $doc->append( 'MTTCI',     $values[14] || 0 );
        $doc->append( 'MTTPF',     $values[15] || 0 );
        $doc->append( 'NBIJ',      $values[16] );

        my $telInd = $values[2] . "(0,09 € TTC/mn)";

        if ( $values[2] != "0 820 201 202" ) {
            $doc->append( 'TEL', $values[2] );
        }
        else {
            $doc->append( 'TEL', $telInd );
        }

        if ( $values[5] =~ /^(\d{5}) *(.+)/ ) {
            $doc->append( 'xCPDEST', $1 );
            my $villdest = $2;
            $doc->append( 'xVILDEST', $villdest );
        }

        $IDDEST = $values[11] . ' ' . $values[13];
        $doc->append( 'NUMSS',   $IDDEST );
        $doc->append( 'xIDDEST', $IDDEST );
        $AGENCE = 'SIEGE';
        $doc->append( 'AGENCE', $AGENCE );
        $NOMDEST = $values[6];
        $doc->append( 'xNOMDEST', $NOMDEST );
        $values[15] =~ s/\,/\./g;
        $values[14] =~ s/\,/\./g;
        $MONTANTci = $values[14];
        $doc->append( 'MTTUN',   $MONTANTci || 0 );
        $doc->append( 'MONTANT', $MONTANTci || 0 );
        $MONTANTcii = $values[15];
        $doc->append( 'MTTDEUX', $MONTANTcii || 0 );

        my @DADDR = ();
        push( @DADDR, $values[6] );
        push( @DADDR, $values[7] );
        push( @DADDR, $values[8] );
        push( @DADDR, $values[9] );
        push( @DADDR, $values[10] );

        @DADDR = user_cleanup_addr(@DADDR);
        $doc->append_table( 'xADRLN', @DADDR );

        my @EADDR = ();

        #		push(@EADDR, $values[1]);
        push( @EADDR, $values[3] );
        push( @EADDR, $values[4] );
        push( @EADDR, $values[5] );

        @EADDR = user_cleanup_addr(@EADDR);

        #Look for the zip code in the last two lines of the address.
        #		foreach my $i ($#EADDR, $#EADDR - 1) {
        #			if ($EADDR[$i] =~ m/^(\d{5})/) {
        #				$doc->append('xIDEMET', $1);
        #			}
        #		}
        $doc->append_table( 'xADREM', @EADDR );
        $doc->append( 'xIDEMET',  $AGENCE );
        $doc->append( 'xNOMDEST', $NOMDEST );

        $TRK->track( 'Doc', 1, $IDDEST, $AGENCE, $NOMDEST, $MONTANTci );
        $doc->append('COURRIER');

        oe_print_to_output_file($doc);
        $doc->reset();
    }

    oe_compo_link();
    return 0;
}

exit main(@ARGV);
