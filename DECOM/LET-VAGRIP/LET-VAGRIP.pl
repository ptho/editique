#!/usr/bin/perl
use utf8;
use oEdtk::Main 0.50;
use oEUser::Lib;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;

#######################################################################################################################################
# Date : 12/09/2012
# Projet : EVOLUTION VACI-GRIPPE
# Nom Application : LET-VAGRIP
# Flux entrée : sorie de esquif avec 32 fichiers pour 32 ventilations (1 fichier/ventilation mais certains peut-être vide)
# En sorite : format PDF Lettre d'invitation + annexe de pris en charge
# Version V2 en 2012 développé par AN
# sésame : 970243 (EB + Excel tableau de ventilation )
# Description de fonctionnement de la version V2 2012:
#	A. Création des deux courriers 02L pour les 65 ans + primo déjà invité et le courrier 07L les 18-64 ans primo déjà invité.
#	B. Modification des courriers existants 01L, 06L, 04L, 05L.
#	C. Correction des dates en 2013 dans les courriers
#	D. Correction des dates dans les prise en charges ( primo et non primo) : 2012-2013
###################################################################################################################

my ( $TRK, $numro, $agence, $nomdest, $offre, $numlot );

# my @tADREMET;
# my @tADRDEST;
my $doc;

# intégré dans oEUser::Lib
#sub user_cleanup_addr {
#	my @lines = @_;
#
#	@lines = grep(!/^\s*$/, @lines);
#	@lines = map { s/^\s+//; s/\s+/ /; $_ } @lines;
#	return @lines;
#}

sub main() {
    if ( $#ARGV == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $ARGV[0], '_' );

    $TRK = oEdtk::Tracking->new(
        $ARGV[1],
        edmode => 'Batch',
        entity => oe_corporation_set(),
        keys   => [ 'NUMRO', 'AGENCE', 'NOMDEST', 'OFFRE', 'NUMLOT' ]
    );

    oe_new_job('--index');

    my $doc = oEdtk::TexDoc->new();
    initApp();
    $doc->append( 'xTYPDOC', 'GRIP' );

    while ( my $ligne = <IN> ) {
        chomp($ligne);

        my @values = unpack(
'A2 A9 A13 A2 A20 A16 A20 A16 A5 A3 A4 A27 A25 A32 A5 A26 A8 A2 A3 A1 A1 A1 A2 A3 A1 A3 A1 A1 A1 A8 A1 A2 A9 A50 A10 A32 A32 A32 A32 A32 A32 A1 A13 A2 A*',
            $ligne
        );
        $doc->append( 'DEBUT', '' );

        $doc->append( 'SIT',      $values[0] );
        $doc->append( 'MUT',      $values[1] );
        $doc->append( 'NSS',      $values[2] );
        $doc->append( 'CSS',      $values[3] );
        $doc->append( 'xNOMDEST', $values[4] );
        $doc->append( 'PREADH',   $values[5] );
        $doc->append( 'NOMBEN',   $values[6] );
        $doc->append( 'PREBEN',   $values[7] );
        $doc->append( 'NUMVOI',   $values[8] );
        $doc->append( 'PEADBTQ',  $values[9] );
        $doc->append( 'NATVOI',   $values[10] );
        $doc->append( 'LIBVOI',   $values[11] );
        $doc->append( 'CPLADR',   $values[12] );
        $doc->append( 'PNTREM',   $values[13] );
        $doc->append( 'xCPDEST',  $values[14] );
        $doc->append( 'xVILDEST', $values[15] );
        $doc->append( 'DATNAI',   $values[16] );
        $doc->append( 'RANG',     $values[17] );
        $doc->append( 'CA1',      $values[18] );
        $doc->append( 'TOPRNG',   $values[19] );
        $doc->append( 'NBEXP',    $values[20] );
        $doc->append( 'INDVAG',   $values[21] );
        $doc->append( 'VENVAG',   $values[22] );
        $doc->append( 'CIVVAG',   $values[23] );
        $doc->append( 'SEX',      $values[24] );
        $doc->append( 'AGE',      $values[25] );
        $doc->append( 'INDALD',   $values[26] );
        $doc->append( 'INDEXT',   $values[27] );
        $doc->append( 'PRIVAC',   $values[28] );
        $doc->append( 'DATVAC',   $values[29] );
        $doc->append( 'INDMETR',  $values[30] );
        $doc->append( 'CLI',      $values[31] );
        $doc->append( 'ORGANIS',  $values[32] );
        $doc->append( 'MUTNOM',   $values[33] );
        $doc->append( 'MUTNOA',   $values[34] );
        $doc->append( 'ADR1',     $values[35] );
        $doc->append( 'ADR2',     $values[36] );
        $doc->append( 'ADR3',     $values[37] );
        $doc->append( 'ADR4',     $values[38] );
        $doc->append( 'ADR5',     $values[39] );
        $doc->append( 'ADR6',     $values[40] );
        $doc->append( 'RGLS',     $values[41] );
        $doc->append( 'ADH',      $values[42] );
        $doc->append( 'RNG',      $values[43] );

        my $numro = $values[2] . " " . $values[3];
        $doc->append( 'xIDDEST', $numro );

        my $agence = $values[1];

        my $nomdest = $values[4] . " " . $values[5];

        my $offre = "";

        my $civil = "";

        my $vide = "";

        if ( $values[5] eq $values[7] ) {
            if ( $values[23] eq "MME" ) {
                $civil = "Chère Madame";
                $doc->append( 'CIV', $civil );
            }
            elsif ( $values[23] eq "MLE" ) {
                $civil = "Chère Mademoiselle";
                $doc->append( 'CIV', $civil );
            }
            elsif ( $values[23] eq "MR" ) {
                $civil = "Cher Monsieur";
                $doc->append( 'CIV', $civil );
            }
            else {
                $civil = "Chère Madame, Cher Monsieur";
                $doc->append( 'CIV', $civil );
            }
        }
        else {
            $civil = "Chère Madame, Cher Monsieur";
            $doc->append( 'CIV', $civil );
        }

        my @DADDR = ();
        if ( $values[5] eq $values[7] ) {
            push( @DADDR, "$values[23] $values[6] $values[7]" );
        }
        else {
            push( @DADDR, "$values[4] $values[5]" );
        }

        my $CDE_ER_AGENCE;
        my $NUMTELE;
        my $CDE_ER_AGENCE = $values[1];

        my $numero  = "";
        my $couttel = "";
        if ( $CDE_ER_AGENCE =~ /971|972|973|974/i ) {
            $numero = $values[36];
            $doc->append( 'SWITCHTEL', $numero );

            #		        		$couttel = "";
            #		        		$doc->append('COUTTEL',$couttel) ;
        }
        else {
            $numero = "TEL 09 72 72 02 02";
            $doc->append( 'SWITCHTEL', $numero );
        }
        $couttel = "(prix d'un appel local)";
        $doc->append( 'COUTTEL',  $couttel );
        $doc->append( 'COUTVIDE', $vide );

        push( @DADDR, $values[12] );
        push( @DADDR, $values[13] );
        push( @DADDR, "$values[8] $values[9] $values[10] $values[11]" );
        push( @DADDR, "$values[14] $values[15]" );

        @DADDR = user_cleanup_addr(@DADDR);
        $doc->append_table( 'xADRLN', @DADDR );

        if ( $values[1] = '973' ) {
            $doc->append( 'xCODRUPT', 'VAGDOM' );
        }

        my @EADDR = ();
        my $tailletab;
        my $ville = lc( $values[38] );
        $ville =~ s/^(\d+\s)//;
        $ville =~ s/(\scedex\s*\d*)$//;
        $ville = ucfirst($ville);
        $doc->append( 'IDEMET', $ville );
        push( @EADDR, $values[37] );
        push( @EADDR, $values[38] );
        push( @EADDR, $values[39] );

        # 			warn "INFO VILLE = $ville \n";
        #push(@EADDR, $values[36]);
        push( @EADDR, $numero );

        #		push(@EADDR, $couttel);
        @EADDR = user_cleanup_addr(@EADDR);

        # Look for the zip code in the last two lines of the address.
        foreach my $i ( $#EADDR, $#EADDR - 1 ) {
            if ( $EADDR[$i] =~ m/^(\d{5})/ ) {
                $doc->append( 'xIDEMET', $1 );
            }
        }
        $doc->append_table( 'xADREM', @EADDR );
        my $tailletab = @EADDR;
        $doc->append( 'TAILLETAB', $tailletab );

        my $i = $values[22];
        $numlot = $values[22];

        #	print "valeur = $values[22]  \n";
        if ( $values[22] eq "01" || $values[22] eq "02" ) {    #  lettre 01
            $doc->append( 'TYPCR', {'LETUN'} );
            $doc->append( 'CRACC', {'PEC'} );
            $offre = "P+65";
        }
        elsif ( $values[22] eq "03" || $values[22] eq "04" ) {    # lettre 03
            $doc->append( 'TYPCR', {'LETTROIS'} );
            $doc->append( 'CRACC', {'PECNP'} );
            $offre = "NP+65";
        }
        elsif ($values[22] eq "05"
            || $values[22] eq "06"
            || $values[22] eq "07"
            || $values[22] eq "08"
            || $values[22] eq "17"
            || $values[22] eq "18"
            || $values[22] eq "19"
            || $values[22] eq "20" )
        {    # lettre 04
            $doc->append( 'TYPCR', {'LETQUATRE'} );
            $doc->append( 'CRACC', {'PECPLUS'} );
            $offre = "-9";
        }
        elsif ($values[22] eq "09"
            || $values[22] eq "10"
            || $values[22] eq "11"
            || $values[22] eq "12"
            || $values[22] eq "21"
            || $values[22] eq "22"
            || $values[22] eq "23"
            || $values[22] eq "24" )
        {    # lettre 05
            $doc->append( 'TYPCR', {'LETCINQ'} );
            $doc->append( 'CRACC', {'PEC'} );
            $offre = "9-17";
        }
        elsif ($values[22] eq "13"
            || $values[22] eq "14"
            || $values[22] eq "25"
            || $values[22] eq "26" )
        {    # lettre 06
            $doc->append( 'TYPCR', {'LETSIX'} );
            $doc->append( 'CRACC', {'PEC'} );
            $offre = "PALDEXT18-64";
        }
        elsif ( $values[22] eq "15" || $values[22] eq "16" ) {    #  lettre 08L
            $doc->append( 'TYPCR', {'LETHUIT'} );
            $doc->append( 'CRACC', {'PECNP'} );
            $offre = "NPALD18-64";
        }
        elsif ( $values[22] eq "27" || $values[22] eq "28" ) {    # lettre 09L
            $doc->append( 'TYPCR', {'LETNEUF'} );
            $doc->append( 'CRACC', {'PECNP'} );
            $offre = "NPEXT18-64";
        }
        elsif ( $values[22] eq "31" || $values[22] eq "32" ) {    # lettre 02L
            $doc->append( 'TYPCR', {'LETDEUX'} );
            $doc->append( 'CRACC', {'PEC'} );
            $offre = "P+65DEJA";
        }
        elsif ( $values[22] eq "33" || $values[22] eq "34" ) {    #  lettre 07L
            $doc->append( 'TYPCR', {'LETSEPT'} );
            $doc->append( 'CRACC', {'PEC'} );
            $offre = "PALDEXT18-64DEJA";
        }
        else {
            die "unexpected value for ventilation: $values[22]\n";
        }

        $doc->append( "OFFRE", $offre );

        my $i        = "";
        my $civil2   = "";
        my $test_civ = "";
        $doc->append("ENDADH");
        my $numSS = $values[2] . "/" . $values[3];
        $doc->append( 'NUMSS', $numSS );

   #if ($test_civ=($values[2] =~ m/^1/))  {$test_civ="1"} else {$test_civ="2"} ;
        $test_civ = substr $values[2], 0, 1;

        if ( $test_civ eq "1" ) {
            $civil2 = "Cher Monsieur";
            $doc->append( 'CIVDEUX', $civil2 );

        }
        else {
            $civil2 = "Chère Madame";
            $doc->append( 'CIVDEUX', $civil2 );
        }

        my $benef = $values[6] . " " . $values[7];
        $doc->append( 'BENEF', $benef );
        my $datrang = convertDate( $values[16] ) . " " . "-" . $values[17];
        $doc->append( 'DATRG', $datrang );
        my $codOrg = $values[32] . "-" . $values[33] . " " . $values[34];
        $doc->append( 'CODORG', $codOrg );

        my $RSI = $values[35];
        $doc->append( 'RS', $RSI );
        my $RSII = $values[36];
        $doc->append( 'RSD', $RSII );
        my $RSIII = $values[37];
        $doc->append( 'RST', $RSIII );
        my $RSIV = $values[38];
        $doc->append( 'RSQ', $RSIV );
        my $RSV = $values[39];
        $doc->append( 'RSC', $RSV );
        my $RSVI = $values[40];
        $doc->append( 'RSS', $RSVI );
        $doc->append("COURRIER");

        $TRK->track( 'Doc', 1, $numro, $agence, $nomdest, $offre, $numlot );

        oe_print_to_output_file($doc);
        $doc->reset();
    }

    oe_compo_link();
}

#################################################################################
# FONCTIONS SPECIFIQUES A L'APPLICATION
#################################################################################
sub initApp() {

    # DECLARATIONS DES VARIABLES PROPRES A L'APPLICATION (use strict)
    # our $typEnr=0;

}

sub convertDate($) {
    my $refVar = shift;
    $refVar =~ s/(\d{4})(\d{2})(\d{2})/$3\/$2\/$1/o;
    return $refVar;
}

main;
