#!/usr/bin/perl
use utf8;
use oEdtk::Main;
use Data::Dumper;
use oEUser::Lib;
use File::Basename;
use oEdtk::Spool;
use oEdtk::RecordParser;
use oEUser::Descriptor::E1;
use oEUser::Descriptor::E2;
use oEUser::Descriptor::E7;
use oEUser::Descriptor::H1;
use oEUser::Descriptor::H4;
use oEUser::Descriptor::HV;
use oEUser::Descriptor::X1;
use oEUser::Descriptor::Z1;
use oEdtk::TexDoc;
use oEdtk::Tracking;
use Date::Calc qw(Mktime Gmtime);
use strict;
my $INFO;
my ( $TRK, $GED, $DOC, $VALS, $COURRIER, $P );
my (
    @FADDR,   $NUMCTR,   $NUMABA,      $MONTANT, $CDE_COUR,
    $nomUser, $SUIVIPAR, @tab_options, $ERAFF,   $EMETTEURGED,
    $CIVIL,   $CTR,      $EJ
);
my $OPT_GED;
my $COMMUNE_ER;
my ( $ENTITE, $NOMEMETTEUR1, $NOMEMETTEUR2, $NOMDESTINATAIRE );

sub main {
    my @argv = @_;
    if ( $#argv == -1 ) { oe_app_usage(); }
    user_corp_file_prefixe( $argv[0], '_' );
    my $user = user_get_aneto_user( $argv[0] );
    $OPT_GED = '--edms';

    # SI ON SOUHAITE METTRE LE DOCUMENT EN GED,
    # IL SUFFIT DE PASSER L'OPTION --edms À oe_compo_link COMME CI-DESSOUS
    #	$OPT_GED='--edms';
    $TRK = oEdtk::Tracking->new(
        $argv[1],
        edmode => 'Batch',
        user   => $user,
        entity => oe_corporation_set(),
        keys   => [ 'NUMCTR', 'xIDEMET', 'NUMABA', 'CONTRAT', 'MONTANT' ]
    );
    oe_new_job('--index');
    init_Doc();

    my $E1 = oEUser::Descriptor::E1->get();

    #$E1->debug();
    $E1->bind_all_c7();
    my $E2 = oEUser::Descriptor::E2->get();

    #$E2->debug();
    $E2->bind_all_c7();
    my $E7 = oEUser::Descriptor::E7->get();

    # $E7->debug();
    $E7->bind_all_c7();
    my $H1 = oEUser::Descriptor::H1->get();
    $H1->bind_all_c7();
    my $H4 = oEUser::Descriptor::H4->get();
    $H4->bind_all_c7();
    my $Z1 = oEUser::Descriptor::Z1->get();

    # $Z1->debug();
    $Z1->bind_all_c7();

    $DOC = oEdtk::TexDoc->new();
    $GED = oEdtk::TexDoc->new();

    $P = oEdtk::RecordParser->new(
        \*IN,
        'E1' => $E1,
        'E2' => $E2,
        'E7' => $E7,
        'H1' => $H1,
        'H4' => $H4,
        'Z1' => $Z1
    );
    $DOC->append( 'xTYPDOC', 'CRPR' );
    $P->set_motif_to_denormalized_split('\x{01}');

    my $lastid;
    my $firstDoc = 0;
    my $id;
    while ( ( $id, $VALS ) = $P->next() ) {
        if ( $id eq 'E1' ) {
            if ( !$firstDoc ) {
                $firstDoc = 1;
            }
            else {
                $DOC->append('FINDOC');
                $TRK->track( 'Doc', 1, $NUMCTR, $ERAFF, $NUMABA, '', $MONTANT );
                &init_Doc();
            }
            &traite_E1();    #traitement du record E1#
        }
        elsif ( $id eq 'E2' ) {
            &traite_E2();    #traitement du record E2#
        }
        elsif ( $id eq 'E7' ) {
            &traite_E7();    #traitement du record E7#
        }
        elsif ( $id eq 'H1' ) {
            &traite_H1();    #traitement du record H1#
        }
        elsif ( $id eq 'H4' ) {    #record H4 non utilisé#
        }
        elsif ( $id eq 'OPTION' ) {
            &traite_option();      #traitement du record option#
        }
        elsif ( $id eq 'Z1' ) {
            ############BLOC ID-MNT##################
            # warn "INFO : dump Z1 ". Dumper($VALS) ."\n"; die();
            $NUMABA = $VALS->{'PERF003'};
            $DOC->append( 'NUMABA', $NUMABA );
            ######INDEXATION GED###################
            $GED->append( 'xCLEGEDiii', $NUMABA )
              ;    #- N° adhérent (ID_MNT) == N° ABA
        }

        $lastid = $id;

#affichage_information(); #traitement de la ligne d'information en fin de courrier#
    }

    if ( $EJ =~ /MNT/ ) {
        $DOC->append( "encartPDF", "MANSEP-MNT.pdf" );

        #$DOC	-> append("encartPDF","LRUN-MNT.pdf"); #LRUNMNT.pdf
    }
    else {
        $DOC->append( "encartPDF", "MANSEP-MUT.pdf" );

        #$DOC	-> append("encartPDF","LRUN-MUT.pdf");
    }

    $GED->append( 'xSOURCE', 'PREC' );
    $DOC->append('LETTRE');
    $DOC->append('FINDOC');

    $TRK->track( 'Doc', 1, $NUMCTR, $ERAFF, $NUMABA, '', $MONTANT );

    # warn "INFO : dump DOC ". Dumper($DOC) ."\n";

    print OUT $GED;
    print OUT $DOC;

    $CDE_COUR = "";
    $DOC->reset();
    $GED->reset();

    oe_compo_link($OPT_GED);
    return 0;
}

sub init_Doc {

    # INITIALISATION DES VARIABLES PROPRES AU DOCUMENT
    undef @FADDR;
    $CDE_COUR = "";
    $nomUser  = "";
    $INFO     = "";
    return 0;
}

sub check_first_amount {
    my @values = @_;

    foreach my $val (@values) {
        $val =~ s/\s//g;
        if ( $val =~ /^[\d\.\,]+$/ ) {

            # warn "INFO : \$val = $val \n";
            return $val;
        }
    }
    return 0;
}

sub traite_E1 {

# NORME 38 : ON UTILISE PLUS LA RECORD E1 POUR LES ADRESSES ===> REPLACEE PAR LA RECORD E7
#warn "INFO : dump E1 ". Dumper($VALS) ."\n";
    my $assureur;
    $DOC->append( 'DEBUT', '' );

# # initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
# oe_iso_country($VALS->{'LICCODP'});
# $VALS->{'LICCODP'} =~ s/FRANCE//i;

    $ENTITE = $VALS->{'ODERCOD'};
    if ( $VALS->{'ODERCOD'} =~
/M012|D012|P006|M019|D019|M029|D029|I029|M033|D033|M034|D034|M040|D040|M073|D073|M041|D041|M049|I049|M059|D059|M064|D064|M066|D066|J081|M081|I081|M095|D095|I074|J074/
      )
    {
        $COMMUNE_ER = "Bordeaux";
        $CTR        = 1;
    }
    else {
        $COMMUNE_ER = ucfirst( lc( $VALS->{'LIBL038'} ) );
        $CTR        = 0;
    }

    $DOC->append( 'COMMUNE_ER', $COMMUNE_ER );
    $DOC->append( 'CTR',        $CTR );

    $DOC->append( 'RAISOC', $VALS->{'PENO027'} );

    # warn"info entité = $VALS->{'ODERCOD'} \n";

    if ( $VALS->{'ODERCOD'} =~ /M|J|R/ ) {
        $assureur = "MUTACITE";    #MUTACITE
    }
    else {
        $assureur = "MNT";         #MNT
    }

    $EJ = $assureur;

    # #############ADRESSE DESTINATAIRE
    # my @DADDR = ();

    # $VALS->{'ZONEDHx'} =~ s/^\s+//;
    $NOMDESTINATAIRE =
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}";

# push(@DADDR, "$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}");
# push(@DADDR, "$VALS->{'PNTREMx'} $VALS->{'CPLADRx'}");
# push(@DADDR, "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'PEVONAT'} $VALS->{'LIBVOIx'}");
# push(@DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}");
# if ($VALS->{'INDCDXx'} == 1) {
# push(@DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}");
# }
# else {
# push(@DADDR, $VALS->{'BOIPOSL'});
# }
# push(@DADDR, $VALS->{'LICCODP'});
# @DADDR = user_cleanup_addr(@DADDR);
# $DOC->append_table('DADDR', @DADDR);
# $DOC->append_table('xADRLN', @DADDR);

    my $nomdest = ucfirst(
        lc(
"$VALS->{'PENOCOD'} $VALS->{'PENO003'} $VALS->{'PENOLIB'} $VALS->{'PENOPRN'}"
        )
    );
    my $er = $VALS->{'ODERCOD'};
    $ERAFF = $er;
    $nomdest =~ s/(\w+)/\u$1/g;
    $DOC->append( 'ERAFF', $ERAFF );

    # $DOC->append('ASSUREUR', $assureur);

    if (   $VALS->{'LICCODC'} eq 'Madame'
        || $VALS->{'LICCODC'} eq 'Mademoiselle' )
    {
        my $civadh = "Chère adhérente";
        $DOC->append( 'CIVADH',  $civadh );
        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
    }
    else {
        my $civadh = "Cher adhérent";
        $DOC->append( 'CIVADH',  $civadh );
        $DOC->append( 'LICCODC', $VALS->{'LICCODC'} );
    }
    $DOC->append( 'NOMDEST',  $nomdest );
    $GED->append( 'xNOMDEST', $nomdest );

    # $GED->append('xVILDEST',$VALS->{'NOMCDXL'});
    # $GED->append('xCPDEST', oe_iso_country() . $VALS->{'CODCDXC'});

    ########## ADRESSE EMETTEUR.
# my $EADDR = ("$VALS->{'PNTR028'} $VALS->{'CPLA029'} $VALS->{'PEAD030'} $VALS->{'PEAD031'} $VALS->{'LICN034'} $VALS->{'LIBV035'} $VALS->{'CODC039'} $VALS->{'NOMC040'}");
# $DOC->append('EADDR', $EADDR);
# # ADRESSE EMETTEUR.PDF
# my @EADDR = ();
    $NOMEMETTEUR1 = "$VALS->{'PENO025'} $VALS->{'PENO027'}";

    # push(@EADDR, "$VALS->{'PENO025'} $VALS->{'PENO027'}");
    $NOMEMETTEUR2 = "$VALS->{'PNTR028'} $VALS->{'CPLA029'}";

# push(@EADDR, "$VALS->{'PNTR028'} $VALS->{'CPLA029'}");
# push(@EADDR, "$VALS->{'PEAD030'} $VALS->{'PEAD031'} $VALS->{'LICN034'} $VALS->{'LIBV035'}");
# push(@EADDR, "$VALS->{'CODC039'} $VALS->{'NOMC040'}");
# if ($VALS->{'INDCDXx'} == 1) {
# push(@EADDR, "$VALS->{'BOIP037'} $VALS->{'LIBL038'}");
# }
# else {
# push(@EADDR, $VALS->{'BOIP037'});
# }
# @EADDR = user_cleanup_addr(@EADDR);

    # my @CADDR = ();
    # push(@CADDR, "$assureur");
    # push(@CADDR, 'Centre de Traitement Recouvrement');
    # push (@CADDR, 'TSA 70011');
    # push (@CADDR, '33044 BORDEAUX Cedex');
    # @CADDR = user_cleanup_addr(@CADDR);
    # $DOC->append_table('EMADDR', @EADDR);
    # $DOC->append_table('CTRADDR', @CADDR);

    #########AFFICHAGE NUMERO DE TELEPHONE
    my $tel = $VALS->{"PECO052"};
    $DOC->append( 'tel', $tel );

    # $DOC->append("ENDADH");
    return 0;
}

sub traite_E2 {
    my @liste;
    my $i;
    $NUMCTR = $VALS->{'ACH1NUM'};
    @liste = split( //, $NUMCTR );
    $DOC->append_table( 'NUMADHL', @liste );
    $DOC->append( 'NUMCTR',  $NUMCTR );
    $GED->append( 'xIDDEST', $NUMCTR );
    $nomUser = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
    $DOC->append( 'NOMUSER', $nomUser );
    $CDE_COUR = $VALS->{'CDCOURx'};
    $GED->append( 'xCLEGEDiv', $CDE_COUR );
#######################################################################################################

####################AJOUT DU NOM-PRENOM DU GESTIONNAIRE DANS L'IDEMET POUR SIMPLICATION DE LA GESTION DANS DOCUBASE.############
    my $GEST = $VALS->{'PENO083'} . " " . $VALS->{'PENO084'};
################################GEST##########################
    # warn "INFO: ODERCOD = $VALS->{'ODERCOD'} \n";
    $VALS->{'ODERCOD'} =~ s/\s+//g;
    if ( $VALS->{'ODERCOD'} ne '' or $VALS->{'ODERCOD'} ne defined ) {
        $GED->append( 'xIDEMET', $VALS->{'ODERCOD'} );
    }
    else {
        $GED->append( 'xIDEMET', $GEST );
    }

    $GED->append( 'xCLEGEDii', $GEST );
    return 0;
}

sub traite_E7 {

    # NORME 38 : ON UTILISE LA RECORD E7 POUR LES ADRESSES
    #warn "INFO : dump E7 ". Dumper($VALS) ."\n";

    # 	my $assureur;

# initialiser oe_iso_country pour activer la gestion du tri des codes postaux par pays
    oe_iso_country( $VALS->{'PELICCO'} );
    $VALS->{'PELICCO'} =~ s/FRANCE//i;

# 	if ($ENTITE =~/M012|D012|P006|M019|D019|M029|D029|I029|M033|D033|M034|D034|M040|D040|M073|D073|M041|D041|M049|I049|M059|D059|M064|D064|M066|D066|J081|M081|I081|M095|D095|I074|J074/){
# 		$COMMUNE_ER = "Bordeaux";
# 		$CTR=1;
# 	}
# 	else {
# 		$COMMUNE_ER = ucfirst (lc($VALS->{'LIBL038'}));
# 		$CTR=0;
# 	}
# 	$DOC->append('COMMUNE_ER',$COMMUNE_ER);
# 	$DOC->append('CTR',$CTR);

    # 	warn"info entité = $ENTITE \n";
    # 	if ($ENTITE =~/M|J|R/) {
    # 		$assureur = "MUTACITE"; #MUTACITE
    # 	} else {
    # 		$assureur = "MNT"; #MNT
    # 	}
    # 	$EJ=$assureur;

    #############ADRESSE DESTINATAIRE
    my @DADDR = ();

    push( @DADDR, $NOMDESTINATAIRE );
    push( @DADDR, $VALS->{'PNTREMx'} );
    push( @DADDR, $VALS->{'CPLADRx'} );
    push( @DADDR, "$VALS->{'PEADNUM'} $VALS->{'PEADBTQ'} $VALS->{'LIBVOIx'}" );

    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @DADDR, "$VALS->{'BOIPOSL'} $VALS->{'LIBLOCL'}" );
    }
    else {
        push( @DADDR, $VALS->{'BOIPOSL'} );
    }

    push( @DADDR, "$VALS->{'CODCDXC'} $VALS->{'NOMCDXL'}" );
    push( @DADDR, $VALS->{'PELICCO'} );
    @DADDR = user_cleanup_addr(@DADDR);
    $DOC->append_table( 'DADDR',  @DADDR );
    $DOC->append_table( 'xADRLN', @DADDR );
    $DOC->append( 'ASSUREUR', $EJ );

    $GED->append( 'xVILDEST', $VALS->{'NOMCDXL'} );
    $GED->append( 'xCPDEST',  oe_iso_country() . $VALS->{'CODCDXC'} );

    ########## ADRESSE EMETTEUR.
    my $EADDR = (
"$NOMEMETTEUR2 $VALS->{'PEAD016'} $VALS->{'PEAD017'} $VALS->{'LIBV019'} $VALS->{'CODC023'} $VALS->{'NOMC040'}"
    );
    $DOC->append( 'EADDR', $EADDR );

    # ADRESSE EMETTEUR.PDF
    my @EADDR = ();
    push( @EADDR, $NOMEMETTEUR1 );
    push( @EADDR, $NOMEMETTEUR2 );
    push( @EADDR, "$VALS->{'PEAD016'} $VALS->{'PEAD017'} $VALS->{'LIBV019'}" );
    push( @EADDR, "$VALS->{'CODC023'} $VALS->{'NOMC040'}" );

    if ( $VALS->{'INDCDXx'} == 1 ) {
        push( @EADDR, "$VALS->{'BOIP021'} $VALS->{'LIBL038'}" );
    }
    else {
        push( @EADDR, $VALS->{'BOIP021'} );
    }

    @EADDR = user_cleanup_addr(@EADDR);

    my @CADDR = ();
    push( @CADDR, "$EJ" );
    push( @CADDR, 'Centre de Traitement Recouvrement' );
    push( @CADDR, 'TSA 70011' );
    push( @CADDR, '33044 BORDEAUX Cedex' );
    @CADDR = user_cleanup_addr(@CADDR);

    $DOC->append_table( 'EMADDR',  @EADDR );
    $DOC->append_table( 'CTRADDR', @CADDR );

    $DOC->append("ENDADH");
    return 0;
}

sub traite_H1 {
    return 0;
}

# fonction permettant la conversion du mois XX en toutes lettres.
sub month_to_name {
    my ($mons) = @_;
    my @mons =
      qw/Janvier Février Mars Avril Mai Juin Juillet Août Septembre Octobre Novembre Décembre/;
    return $mons[ $mons - 1 ];
}

sub traite_option {

    #warn "INFO : dump OPTION ". Dumper($VALS) ."\n";
  SWITCH: {
        ( $CDE_COUR eq "CTR-4" )
          && do {
            if ( $VALS->[2] !~ /(\d{2})\/(\d{2})\/(\d{4})/ ) {
                die
"ERROR: in date décès Unexpected date format \'$VALS->[1]\' at line $. in input file\n";
            }

            ( $VALS->[2] =~ /(\d{2})\/(\d{2})\/(\d{4})/ );

            my $joursrg = $1;
            my $moisrg  = $2;
            my $anneerg = $3;
            $moisrg  = month_to_name($moisrg);
            $MONTANT = ( $VALS->[1] );
            $MONTANT =~ s/\s+//;
            $MONTANT = check_first_amount($MONTANT);
            $DOC->append( 'MONTANT', $MONTANT );
            $DOC->append( 'DATERG', $joursrg . ' ' . $moisrg . ' ' . $anneerg );
            last SWITCH;
          };
    }
    return 0;
}

exit main(@ARGV);
