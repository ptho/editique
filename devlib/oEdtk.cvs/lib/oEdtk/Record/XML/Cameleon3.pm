package oEdtk::XML::Cameleon3;

use strict;
use warnings;

use Encode qw(encode);
use Fcntl qw(:seek);
use XML::LibXML;

# This is so we can refer to Unicode characters by their name.
use charnames ':full';

use Exporter;
our $VERSION	= 0.01;
our @ISA	= qw(Exporter);
our @EXPORT_OK	= qw(cameleon_extract);

# Wrapper for Encode::encode that warns about characters that cannot be
# converted, but doesn't insert any replacement text.
sub my_encode {
	my ($enc, $data) = @_;
	my $check = sub {
		warn sprintf("<U+%04X> does not map to $enc", shift);
		return '';
	};
	encode($enc, $data, $check);
}

# Extract the key/value pairs from the Cameleon XML file and
# return them in a hash.
sub cameleon_extract {
	my ($fh, $cpeid) = @_;

	seek($fh, 0, SEEK_SET);

	binmode($fh);	# Make sure to drop any PerlIO layer.
	my $xml = XML::LibXML->load_xml(IO => $fh);
	my $xpc = XML::LibXML::XPathContext->new($xml);

	my $cpe = 'CPE.wsMNT_COH/CP/cpSante.gp.wsMNT_COH/SBL/sblTotal.wsMNT_COH/SBL/';
	$cpe .= $cpeid;

	my $cond = "[\@cpe=\"$cpe\"]";
	my @nodes = $xpc->findnodes('//conf:GenerativeProcess//' .
	    'conf:ItemSalesBreakdownLine' .  $cond .
	    '/conf:Sig//conf:BusinessProperties');

	my $data = {};
	foreach my $line (@nodes) {
		my ($knode) = $xpc->findnodes('./conf:SingleBusinessProperty[@name="bpTypeDonnee"]/conf:Value/conf:textValue', $line);
		my ($vnode) = $xpc->findnodes('./conf:SingleBusinessProperty[@name="bpValeur"]/conf:Value/conf:longTextValue', $line);
		if (!defined($vnode) || !defined($knode) ||
		    $vnode->nodeType != XML_ELEMENT_NODE ||
		    $knode->nodeType != XML_ELEMENT_NODE) {
			return undef;
		};
		my $key = $knode->textContent();
		my $val = $vnode->textContent();
		# We substitute the unicode character "RIGHT SINGLE
		# QUOTATION MARK" by the common apostrophe character,
		# because there is no other corresponding character
		# when encoding in ISO-8859-15.
		# http://ascii-table.com/unicode-index-e.php
		$val =~ s/\N{RIGHT SINGLE QUOTATION MARK}/'/g;
		$val =~ s/\N{HORIZONTAL ELLIPSIS}/.../g;
		$val = my_encode('latin9', $val);
		$data->{$key} = $val;
	}
	return $data;
}

1;
