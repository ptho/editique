package oEdtk::XML::Cameleon2;

use strict;
use warnings;

use Encode qw(encode);
use Fcntl qw(:seek);
use XML::LibXML;

# This is so we can refer to Unicode characters by their name.
use charnames ':full';

use Exporter;
our $VERSION	= 0.01;
our @ISA	= qw(Exporter);
our @EXPORT_OK	= qw(cameleon_extract);

# Wrapper for Encode::encode that warns about characters that cannot be
# converted, but doesn't insert any replacement text.
sub my_encode {
	my ($enc, $data) = @_;
	my $check = sub {
		warn sprintf("<U+%04X> does not map to $enc", shift);
		return '';
	};
	encode($enc, $data, $check);
}

# Extract the key/value pairs from the Cameleon XML file and
# return them in a hash.
sub cameleon_extract {
	my ($fh, $cpe) = @_;

	seek($fh, 0, SEEK_SET);

	binmode($fh);	# Make sure to drop any PerlIO layer.
	my $xml = XML::LibXML->load_xml(IO => $fh);
	my $xpc = XML::LibXML::XPathContext->new($xml);

#	$cpe |= "CPE.wsMNT_COH/CP/cpSante.gp.wsMNT_COH/SBL/sblTotal";

	my $cond = "[\@cpe=\"$cpe\"]";
	my @nodes = $xpc->findnodes('//conf:GenerativeProcess//conf:ItemSalesBreakdownLine' . $cond . '/conf:Children/conf:ItemSalesBreakdownLine');

	my $data = {};
	foreach my $line1 (@nodes) {
		my $nameProp = $xpc->findvalue('./@name', $line1);
		my @nodes2 = $xpc->findnodes('./conf:Sig//conf:BusinessPropertySets/conf:BusinessPropertySet[@name="bpsEdition"]', $line1);
		foreach my $line2 (@nodes2) {
			my ($knode) = $xpc->findnodes('./conf:BusinessProperties/conf:SingleBusinessProperty[@name="bpTypeDonnee"]/conf:Value/conf:textValue', $line2);
			my ($vnode) = $xpc->findnodes('./conf:BusinessProperties/conf:SingleBusinessProperty[@name="bpValeur"]/conf:Value/conf:longTextValue', $line2);
			if (!defined($vnode) || !defined($knode) ||
			    $vnode->nodeType != XML_ELEMENT_NODE ||
			    $knode->nodeType != XML_ELEMENT_NODE) {
				return undef;
			};
			my $key = $knode->textContent();
			my $val = $vnode->textContent();
			# We substitute the unicode character "RIGHT SINGLE
			# QUOTATION MARK" by the common apostrophe character,
			# because there is no other corresponding character
			# when encoding in ISO-8859-15.
			# http://ascii-table.com/unicode-index-e.php
			$val =~ s/\N{RIGHT SINGLE QUOTATION MARK}/'/g;
			$val =~ s/\N{HORIZONTAL ELLIPSIS}/.../g;
			$data->{$nameProp}->{$key} = my_encode('iso-8859-15', $val);
		}
	}
	return $data;
}

1;
