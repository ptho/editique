package oEUser::Lib;
use utf8;

use strict;
use warnings;

use Text::Unaccent::PurePerl qw( unac_string );
use File::Basename;
use oEdtk::Main;
use oEdtk::Dict;
use oEdtk::Config qw(config_read);
use Carp qw( carp croak );
use Data::Dumper qw( Dumper );

use Exporter;
our $VERSION = 0.09;
our @ISA     = qw(Exporter);
our @EXPORT  = qw(
  user_cleanup
  user_cleanup_addr
  user_corp_file_prefixe
  user_get_adr_ret
  user_get_aneto_user
  user_get_tag_adr_CTC
  user_prev_get_option
  normalize_address_AFNOR_NZ_Z_10_011
  norme_38
);

# USER MODULE FOR CORPORATE SPECIFIC FUNCTIONS

=pod
# Unused sub?
sub user_corp {
    my $value = shift;

    return "INFO : this is $value in \n";
}
=cut

sub user_corp_file_prefixe {
    my ( $filename, $directories, $suffix ) = fileparse(shift);
    my $sep = shift || '.';
    my @prefix = split( /$sep/, $filename );
    oe_corporation_set( $prefix[0] );

    # warn "$filename \$prefix[0] $prefix[0] -> ". oe_corporation_set()."\n";
    1;
}

sub user_get_aneto_user {    # migrer oe_aneto_get_user
    my $file = shift;

    open( my $fh, '<', $file ) or die "ERROR: impossible d'ouvrir $!\n";
    my $line = <$fh>;

    # FLUX  1169733711000000000001EDITBA    EDITAC014 AC014                D018
    my ( $user, $type_edition, $flux ) = unpack( 'x28 A10 A10 A10', $line );

    if ($user)
    {    # dans les batch le user est sur la première ligne de données (FLUX)
        close($fh);
        return ( $user, $type_edition, $flux );
    }

    $line = <$fh>;    # dans les TP le user est sur la seconde ligne (ENTETE)
    close($fh);
    return unpack( 'x30 A8', $line );   # en 28 on 'UT' pour les TP utilisateurs
}

=pod
# Unused sub?
sub user_get_sycomore_user {         # migrer oe_sycomore_get_user
    my $file = shift;

    $file =~ s/^.*[\/\\]([^\/\\]+)$/$1/;
    $file =~ s/\.[^.]*$//;
    my @parts = split( '_', $file );
    return $parts[$#parts];
}
=cut

# See
# http://srvetuoutilsprd:81/redmine/projects/editique/wiki/Comment_r%C3%A9diger_une_adresse_postale
# AFNOR NF Z 10-011
sub normalize_address_AFNOR_NZ_Z_10_011 {
    my ($l) = @_;
    my @res;
    foreach my $line ( @{$l} ) {
        croak "list ref has undef elements: " . Dumper($l) unless defined $line;
        if ( $line =~ m{\S}sox ) {
            my $ascii = unac_string( 'UTF-8', $line );
            push @res, oe_clean_addr_line($ascii);
        }
    }
    return \@res;
}

sub norme_38 {
    my (@l) = @_;
    return normalize_address_AFNOR_NZ_Z_10_011( \@l );
}

sub user_cleanup_addr {
    my @lines = @_;

=pod
# Activate to see where numerous warnings come from
use Data::Dumper qw( Dumper );
use Carp qw( croak );
foreach my $line (@lines) {
    croak Dumper(\@lines) unless defined $line;
}
=cut

    # Remove empty lines.
    @lines = grep( !/^\s*$/, @lines );

    # Strip leading/trailing whitespace, and concatenate spaces.
    @lines = map { oe_clean_addr_line($_); oe_uc_sans_accents($_); } @lines;
    return @lines;
}

sub user_cleanup {
    my @lines = @_;

    # Remove empty lines.
    @lines = grep( !/^\s*$/, @lines );

    # Strip leading/trailing whitespace, and concatenate spaces.
    @lines = map { oe_clean_addr($_); oe_uc_sans_accents($_); } @lines;
    return @lines;
}

sub user_prev_get_option {

# Fonction demandée par la Prévoyance pour coupeer les libellés long afin de récupérer l'OPTION de la garantie
# en entrée il faut passer le libellé long
# en sortie on récupère le libellé court
# exemple d'appel :
# $OPTION = user_prev_get_option($vals->{'PRPR008'});

    my $libelle = shift;
    $libelle =~
      s/^(.+)(\!{1}.*)$/$1/g;    # ex. : GMS op1 ! Prév Ind MS Std Opt 1 (TU)

    return $libelle;
}

=pod
# Unused sub?
# puis nouvelle méthode de gestion des adresses E1...
sub user_E1_build_addr {    # CTP-AC001
    my ( $doc, $vals ) = @_;
    my $Y;

# APPLICATION DE LA RÈGLE DE GESTION DES ADRESSES ANETO (ENR E1) :
# SOIT 'Y' EGALE LE NOM DE LA VILLE AVANT 'CEDEX' DANS E1-ZZZZ-NOMCDXLIBACH
# cas 1 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC !C Y	: E1-ZZZZ-BOIPOSLIBLDT + b + E1-ZZZZ-LIBLOCLIBLOC
# cas 2 : E1-ZZZZ-INDCDX =1  + LIBLOCLIBLOC C  Y	: E1-ZZZZ-BOIPOSLIBLDT
# cas 3 : E1-ZZZZ-INDCDX =0 				: E1-ZZZZ-BOIPOSLIBLDT

    # ADRESSE DESTINATAIRE.
    oe_clean_addr_line( $vals->{'LIBLOCL'} );
    oe_clean_addr_line( $vals->{'NOMCDXL'} );
    $Y = $vals->{'NOMCDXL'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDCDXx'} == 0 ) {

        # CAS 3
        $vals->{'LIBLOCL'} = '';
    }
    elsif ( $vals->{'INDCDXx'} == 1 && index( $vals->{'LIBLOCL'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBLOCL'} = '';
    }

# Gestion de l'adresse (should probably be moved somewhere else so that it can be reused).
    my @DADDR = ();

#	push(@DADDR, "$vals->{'PENOCOD'} $vals->{'PENO003'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}"); # ACi-AC014 RPA-PSRE1
    push( @DADDR, "$vals->{'PENOCOD'} $vals->{'PENOLIB'} $vals->{'PENOPRN'}" );
    push( @DADDR, $vals->{'PNTREMx'} );
    push( @DADDR, $vals->{'CPLADRx'} );
    push( @DADDR,
"$vals->{'PEADNUM'} $vals->{'PEADBTQ'} $vals->{'PEVONAT'} $vals->{'LIBVOIx'}"
    );
    push( @DADDR, "$vals->{'BOIPOSL'} $vals->{'LIBLOCL'}" );
    push( @DADDR, "$vals->{'CODCDXC'} $vals->{'NOMCDXL'}" );

    $vals->{'LICCODP'} =~ s/FRANCE//i;    # ON AFFICHE PAS LE PAYS POUR FRANCE
    push( @DADDR, $vals->{'LICCODP'} );
    @DADDR = user_cleanup_addr(@DADDR);
    $doc->append_table( 'DADDR', @DADDR );

# AU TOUR DE L'ADRESSE ÉMETTEUR.
# cas 1 : E1-ZZZZ-INDCDX-2 =1  + LIBLOCLIBLOC-2 !C Y	: E1-ZZZZ-BOIPOSLIBLDT + b + E1-ZZZZ-LIBLOCLIBLOC
# cas 2 : E1-ZZZZ-INDCDX-2 =1  + LIBLOCLIBLOC-2 C  Y	: E1-ZZZZ-BOIPOSLIBLDT
# cas 3 : E1-ZZZZ-INDCDX-2 =0 				: E1-ZZZZ-BOIPOSLIBLDT

    oe_clean_addr_line( $vals->{'LIBL038'} );
    oe_clean_addr_line( $vals->{'NOMC040'} );
    $Y = $vals->{'NOMC040'};
    $Y =~ s/CEDEX.*$//
      ; # ON RETIRE TOUT CE QU'IL Y A AVANT 'CEDEX', SACHANT QU'ON PEUT AVOIR CEDEX TOUT SEUL...
    $Y =~ s/\s+$//;    # ON RETIRE LE(S) BLANC(S) DE FIN DE CHAINE

    if ( $vals->{'INDC036'} == 0 ) {

        # CAS 3
        $vals->{'LIBL038'} = '';
    }
    elsif ( $vals->{'INDC036'} == 1 && index( $vals->{'LIBL038'}, $Y ) == -1 ) {

        # CAS 1
    }
    else {
        # CAS 2
        $vals->{'LIBL038'} = '';
    }

    my @EADDR = ();
    push( @EADDR, $vals->{'PENO027'} );
    push( @EADDR, $vals->{'PENO025'} );
    push( @EADDR, "$vals->{'CPLA029'} $vals->{'PNTR028'}" );
    push( @EADDR,
            "$vals->{'PEAD030'} $vals->{'PEAD031'} $vals->{'LICN034'} "
          . "$vals->{'LIBV035'} $vals->{'BOIP037'}" );
    push( @EADDR, "$vals->{'CODC039'} $vals->{'LIBL038'} $vals->{'NOMC040'}" );
    @EADDR = user_cleanup_addr(@EADDR);
    $doc->append_table( 'EADDR', @EADDR );

    #	$doc->append_table('ADDRA', @EADDR); # ACi-AC014 RPA-PSRE1
}
=cut

sub user_get_tag_adr_CTC {

# CETTE FONCTION EST REMPLACÉE PAR LA FONCTION user_get_adr_ret() !
# recoit un nom de section (ie : D059) et retourne  un tableau comportant l'adresse à  utiliser ou NULL
    my $section = shift;
    my @ADR;

    return user_get_adr_ret( 'SANTE', $section );
}

my $_ADR_RET_DIC;

sub user_get_adr_ret {

# UTILISATION DICTIONNAIRE :
#	- si paramètre connu dans le dictionnaire =>	valeur du dictionnaire
#	- sinon retourne =>	NULL (on mettra l'adresse section du flux)
#
# pour tester la configuration on peut utiliser le script lib/check_adr_ret.pl ID-SECTION [BRANCHE]
#
# EXEMPLE D'UTILISATION :
#my @adr_CTC = user_get_adr_ret($SECTION, 'PREV');
#
#if ($adr_CTC[0] eq 'NULL') { #
#	Code spécifique, soit on affiche rien :
#	$adr_CTC[0] = "~";
#	soit on redéfini l'adresse avec celle de la section contenue dans le flux
#	@adr_CTC = split (',', $vals->{'xxxxx'});
#		ou
#	@adr_CTC = ($vals->{'xxxxx'}, $vals->{'xxxxx'}, $vals->{'xxxxx'}, $vals->{'xxxxx'}, $vals->{'xxxxx'});
#}
#$DOC->append_table('AdrRET', @adr_CTC);

    my ( $branche, $code_section ) = @_;
    if ( $branche =~ /SANTE/i ) {
        $branche = "SANTE";
    }
    elsif ( $branche =~ /PREV/i ) {
        $branche = "PREVOYANCE";
    }
    else {
        die
"ERROR: branche '$branche' demandée dans user_get_adr_ret inconnue\n";
    }

# l'option check permet au dictionnaire de vérifier que la variable est présente dans le dictionnaire (retourne 'undef' s'il ne trouve rien)
    my $check = 1;

    if ( !defined($_ADR_RET_DIC) ) {
        my $cfg = config_read();

        #my $ini_file = $cfg->{'EDTK_DICO'};
        my $ini_file = $cfg->{'EDTK_DIR_CONFIG'} . "/adr.ret.ini";

#warn "INFO: chargement config pour $branche cible = ". $cfg->{'EDTK_DICO'} ."\n";
        $_ADR_RET_DIC =
          oEdtk::Dict->new( $ini_file, { invert => 1, section => $branche } );
    }

    my ( $value, @ADR_RET );
    $value = $_ADR_RET_DIC->translate( $code_section, $check );

    if ( defined($value) ) {

        # si la valeur a été trouvée dans le dictionnaire
        @ADR_RET = split( ',', $value );
    }
    else {
        push( @ADR_RET, 'NULL' );
    }

    return @ADR_RET;
}

1;
