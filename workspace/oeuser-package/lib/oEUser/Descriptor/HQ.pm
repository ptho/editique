package oEUser::Descriptor::HQ;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CZ007CHQ-HQ-LGNDAT
        # HQ-INFO-AIDE-ACS
        oEdtk::Field->new( 'HQ-ACAAACASR2',        2 ),
        oEdtk::Field->new( 'HQ-ACAANUMACS',        5 ),
        oEdtk::Field->new( 'HQ-ACAADDEPRD',        8 ),
        oEdtk::Field->new( 'HQ-ACAADFEPRD',        8 ),
        oEdtk::Field->new( 'HQ-ACAADFFPRD',        8 ),
        oEdtk::Field->new( 'HQ-ACAATYPACS',        5 ),
        oEdtk::Field->new( 'HQ-LIBCOU-ACAATYPACS', 15 ),
        oEdtk::Field->new( 'HQ-LIBLON-ACAATYPACS', 50 ),
        oEdtk::SignedField->new( 'HQ-ACAAMNTACS', 12, 2 ),
        oEdtk::SignedField->new( 'HQ-ACZZMTACSU', 12, 2 ),
        oEdtk::SignedField->new( 'HQ-ACZZMTACSR', 12, 2 ),
        oEdtk::Field->new( 'HQ-ACAADATEDT', 8 ),
        oEdtk::Field->new( 'HQ-ACAAINDVAL', 1 ),
        oEdtk::Field->new( 'HQ-RESERVE-HQ', 2348 ),
    );
}

1;
