package oEUser::Descriptor::PA;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # PR0012MJ-PA
        oEdtk::Field->new( 'PA-PSDEIDFDOS', 10 ),
        oEdtk::Field->new( 'PA-PSDENUMDCP', 2 ),
        oEdtk::Field->new( 'PA-PSREDATEDT', 8 ),
        oEdtk::Field->new( 'PA-ENFNACPPEC', 1 ),
        oEdtk::Field->new( 'PA-ENFNCODRGF', 4 ),
        oEdtk::Field->new( 'PA-ENFNCODACC', 10 ),
        oEdtk::Field->new( 'PA-CODFRS-DST', 9 ),
        oEdtk::Field->new( 'PA-TYPFRS-DST', 2 ),
        oEdtk::Field->new( 'PA-PSEPNUMETB', 9 ),
        oEdtk::Field->new( 'PA-NOMETB',     50 ),
    );
}

1;
