package oEUser::Descriptor::R325;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R325-LGNDAT
        oEdtk::Field->new( 'R325-ACCLACASR2',    2 ),
        oEdtk::Field->new( 'R325-ACH4CODGAR',    10 ),
        oEdtk::Field->new( 'R325-ACH4CODPRT',    10 ),
        oEdtk::Field->new( 'R325-ACCLNUMCAU',    5 ),
        oEdtk::Field->new( 'R325-ACCLDDEPRD',    8 ),
        oEdtk::Field->new( 'R325-ACCLDFEPRD',    8 ),
        oEdtk::Field->new( 'R325-ACCLTYPTXT',    1 ),
        oEdtk::Field->new( 'R325-LL-ACCLTYPTXT', 50 ),
        oEdtk::Field->new( 'R325-LC-ACCLTYPTXT', 15 ),

        # R325-TEXTE-CLAUSE
        oEdtk::Field->new( 'R325-TEXCL1', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4', 50 ),

        # R325-TEXTE-CLAUSE-2
        oEdtk::Field->new( 'R325-TEXCL1-2', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-2', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-2', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-2', 50 ),

        # R325-TEXTE-CLAUSE-3
        oEdtk::Field->new( 'R325-TEXCL1-3', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-3', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-3', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-3', 50 ),

        # R325-TEXTE-CLAUSE-4
        oEdtk::Field->new( 'R325-TEXCL1-4', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-4', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-4', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-4', 50 ),

        # R325-TEXTE-CLAUSE-5
        oEdtk::Field->new( 'R325-TEXCL1-5', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-5', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-5', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-5', 50 ),

        # R325-TEXTE-CLAUSE-6
        oEdtk::Field->new( 'R325-TEXCL1-6', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-6', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-6', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-6', 50 ),

        # R325-TEXTE-CLAUSE-7
        oEdtk::Field->new( 'R325-TEXCL1-7', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-7', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-7', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-7', 50 ),

        # R325-TEXTE-CLAUSE-8
        oEdtk::Field->new( 'R325-TEXCL1-8', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-8', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-8', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-8', 50 ),

        # R325-TEXTE-CLAUSE-9
        oEdtk::Field->new( 'R325-TEXCL1-9', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-9', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-9', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-9', 50 ),

        # R325-TEXTE-CLAUSE-10
        oEdtk::Field->new( 'R325-TEXCL1-10', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-10', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-10', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-10', 50 ),

        # R325-TEXTE-CLAUSE-11
        oEdtk::Field->new( 'R325-TEXCL1-11', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-11', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-11', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-11', 50 ),

        # R325-TEXTE-CLAUSE-12
        oEdtk::Field->new( 'R325-TEXCL1-12', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-12', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-12', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-12', 50 ),

        # R325-TEXTE-CLAUSE-13
        oEdtk::Field->new( 'R325-TEXCL1-13', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-13', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-13', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-13', 50 ),

        # R325-TEXTE-CLAUSE-14
        oEdtk::Field->new( 'R325-TEXCL1-14', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-14', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-14', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-14', 50 ),

        # R325-TEXTE-CLAUSE-15
        oEdtk::Field->new( 'R325-TEXCL1-15', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-15', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-15', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-15', 50 ),

        # R325-TEXTE-CLAUSE-16
        oEdtk::Field->new( 'R325-TEXCL1-16', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-16', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-16', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-16', 50 ),

        # R325-TEXTE-CLAUSE-17
        oEdtk::Field->new( 'R325-TEXCL1-17', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-17', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-17', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-17', 50 ),

        # R325-TEXTE-CLAUSE-18
        oEdtk::Field->new( 'R325-TEXCL1-18', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-18', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-18', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-18', 50 ),

        # R325-TEXTE-CLAUSE-19
        oEdtk::Field->new( 'R325-TEXCL1-19', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-19', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-19', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-19', 50 ),

        # R325-TEXTE-CLAUSE-20
        oEdtk::Field->new( 'R325-TEXCL1-20', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-20', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-20', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-20', 50 ),

        # R325-TEXTE-CLAUSE-21
        oEdtk::Field->new( 'R325-TEXCL1-21', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-21', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-21', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-21', 50 ),

        # R325-TEXTE-CLAUSE-22
        oEdtk::Field->new( 'R325-TEXCL1-22', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-22', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-22', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-22', 50 ),

        # R325-TEXTE-CLAUSE-23
        oEdtk::Field->new( 'R325-TEXCL1-23', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-23', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-23', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-23', 50 ),

        # R325-TEXTE-CLAUSE-24
        oEdtk::Field->new( 'R325-TEXCL1-24', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-24', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-24', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-24', 50 ),

        # R325-TEXTE-CLAUSE-25
        oEdtk::Field->new( 'R325-TEXCL1-25', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-25', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-25', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-25', 50 ),

        # R325-TEXTE-CLAUSE-26
        oEdtk::Field->new( 'R325-TEXCL1-26', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-26', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-26', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-26', 50 ),

        # R325-TEXTE-CLAUSE-27
        oEdtk::Field->new( 'R325-TEXCL1-27', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-27', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-27', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-27', 50 ),

        # R325-TEXTE-CLAUSE-28
        oEdtk::Field->new( 'R325-TEXCL1-28', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-28', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-28', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-28', 50 ),

        # R325-TEXTE-CLAUSE-29
        oEdtk::Field->new( 'R325-TEXCL1-29', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-29', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-29', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-29', 50 ),

        # R325-TEXTE-CLAUSE-30
        oEdtk::Field->new( 'R325-TEXCL1-30', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-30', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-30', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-30', 50 ),

        # R325-TEXTE-CLAUSE-31
        oEdtk::Field->new( 'R325-TEXCL1-31', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-31', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-31', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-31', 50 ),

        # R325-TEXTE-CLAUSE-32
        oEdtk::Field->new( 'R325-TEXCL1-32', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-32', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-32', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-32', 50 ),

        # R325-TEXTE-CLAUSE-33
        oEdtk::Field->new( 'R325-TEXCL1-33', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-33', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-33', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-33', 50 ),

        # R325-TEXTE-CLAUSE-34
        oEdtk::Field->new( 'R325-TEXCL1-34', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-34', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-34', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-34', 50 ),

        # R325-TEXTE-CLAUSE-35
        oEdtk::Field->new( 'R325-TEXCL1-35', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-35', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-35', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-35', 50 ),

        # R325-TEXTE-CLAUSE-36
        oEdtk::Field->new( 'R325-TEXCL1-36', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-36', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-36', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-36', 50 ),

        # R325-TEXTE-CLAUSE-37
        oEdtk::Field->new( 'R325-TEXCL1-37', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-37', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-37', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-37', 50 ),

        # R325-TEXTE-CLAUSE-38
        oEdtk::Field->new( 'R325-TEXCL1-38', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-38', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-38', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-38', 50 ),

        # R325-TEXTE-CLAUSE-39
        oEdtk::Field->new( 'R325-TEXCL1-39', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-39', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-39', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-39', 50 ),

        # R325-TEXTE-CLAUSE-40
        oEdtk::Field->new( 'R325-TEXCL1-40', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-40', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-40', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-40', 50 ),

        # R325-TEXTE-CLAUSE-41
        oEdtk::Field->new( 'R325-TEXCL1-41', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-41', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-41', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-41', 50 ),

        # R325-TEXTE-CLAUSE-42
        oEdtk::Field->new( 'R325-TEXCL1-42', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-42', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-42', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-42', 50 ),

        # R325-TEXTE-CLAUSE-43
        oEdtk::Field->new( 'R325-TEXCL1-43', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-43', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-43', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-43', 50 ),

        # R325-TEXTE-CLAUSE-44
        oEdtk::Field->new( 'R325-TEXCL1-44', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-44', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-44', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-44', 50 ),

        # R325-TEXTE-CLAUSE-45
        oEdtk::Field->new( 'R325-TEXCL1-45', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-45', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-45', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-45', 50 ),

        # R325-TEXTE-CLAUSE-46
        oEdtk::Field->new( 'R325-TEXCL1-46', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-46', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-46', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-46', 50 ),

        # R325-TEXTE-CLAUSE-47
        oEdtk::Field->new( 'R325-TEXCL1-47', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-47', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-47', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-47', 50 ),

        # R325-TEXTE-CLAUSE-48
        oEdtk::Field->new( 'R325-TEXCL1-48', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-48', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-48', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-48', 50 ),

        # R325-TEXTE-CLAUSE-49
        oEdtk::Field->new( 'R325-TEXCL1-49', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-49', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-49', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-49', 50 ),

        # R325-TEXTE-CLAUSE-50
        oEdtk::Field->new( 'R325-TEXCL1-50', 50 ),
        oEdtk::Field->new( 'R325-TEXCL2-50', 50 ),
        oEdtk::Field->new( 'R325-TEXCL3-50', 50 ),
        oEdtk::Field->new( 'R325-TEXCL4-50', 50 ),
    );
}

1;
