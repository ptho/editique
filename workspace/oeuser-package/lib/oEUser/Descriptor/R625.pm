package oEUser::Descriptor::R625;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R625-LGNDAT
        oEdtk::Field->new( 'R625-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R625-PECODDE',       8 ),
        oEdtk::Field->new( 'R625-PECODFE',       8 ),
        oEdtk::Field->new( 'R625-PEOPTYPCOM',    5 ),
        oEdtk::Field->new( 'R625-LL-PEOPTYPCOM', 50 ),
        oEdtk::Field->new( 'R625-LC-PEOPTYPCOM', 15 ),
        oEdtk::Field->new( 'R625-PECONATCOM',    5 ),
        oEdtk::Field->new( 'R625-LL-PECONATCOM', 50 ),
        oEdtk::Field->new( 'R625-LC-PECONATCOM', 15 ),
        oEdtk::Field->new( 'R625-PECOVALCOM',    150 ),
        oEdtk::Field->new( 'R625-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R625-ACOPNUMOPE',    5 ),
    );
}

1;
