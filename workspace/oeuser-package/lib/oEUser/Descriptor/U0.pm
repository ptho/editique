package oEUser::Descriptor::U0;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # SCC-U0-ENRFLX
        # SCC-000-IDFFLX
        oEdtk::Field->new( 'SCC-000-NUMFLX', 20 ),
        oEdtk::Field->new( 'SCC-000-NUMLOT', 6 ),
        oEdtk::Field->new( 'SCC-000-NUMDOS', 6 ),
        oEdtk::Field->new( 'SCC-000-NUMLCO', 6 ),
        oEdtk::Field->new( 'SCC-000-NUMLGA', 6 ),
        oEdtk::Field->new( 'SCC-000-TYPENR', 3 ),
        oEdtk::Field->new( 'SCC-000-STYENR', 3 ),

        # SCC-000-DONFLX
        # SCC-000-NUMVERX
        oEdtk::Field->new( 'SCC-000-NUMVER', 2 ),

        # SCC-000-DATCREX
        oEdtk::Field->new( 'SCC-000-DATCRE', 8 ),
        oEdtk::Field->new( 'SCC-000-SNSFLX', 1 ),

        # SCC-000-DENOMB
        # SCC-000-NBRTOTX
        oEdtk::Field->new( 'SCC-000-NBRTOT', 10 ),

        # SCC-000-NBRLOTX
        oEdtk::Field->new( 'SCC-000-NBRLOT', 10 ),

        # SCC-000-NBRBENX
        oEdtk::Field->new( 'SCC-000-NBRBEN', 10 ),

        # SCC-000-NBRLCOX
        oEdtk::Field->new( 'SCC-000-NBRLCO', 10 ),

        # SCC-000-NBRLGAX
        oEdtk::Field->new( 'SCC-000-NBRLGA', 10 ),
        oEdtk::Field->new( 'SCC-000-SCC01',  5 ),
    );
}

1;
