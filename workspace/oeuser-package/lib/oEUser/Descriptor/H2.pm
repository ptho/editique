package oEUser::Descriptor::H2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # -H2-LGNDAT
        oEdtk::Field->new( 'H2-ACCEACASR2',        2 ),
        oEdtk::Field->new( 'H2-ACCEDDEPRD',        8 ),
        oEdtk::Field->new( 'H2-ACCEDFEPRD',        8 ),
        oEdtk::Field->new( 'H2-ACCECODGEL',        5 ),
        oEdtk::Field->new( 'H2-LIBCOU-ACCECODGEL', 15 ),
        oEdtk::Field->new( 'H2-LIBLON-ACCECODGEL', 50 ),
        oEdtk::Field->new( 'H2-ACCEINDEXL',        1 ),
        oEdtk::Field->new( 'H2-PSARCODARC',        5 ),
        oEdtk::Field->new( 'H2-PSARLIBCOU',        15 ),
        oEdtk::Field->new( 'H2-PSARLIBLON',        50 ),
        oEdtk::Field->new( 'H2-RESERVE-H2',        200 ),
    );
}

1;
