package oEUser::Descriptor::C2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # C2-LGNDAT
        # C2-IDENT-SO
        oEdtk::Field->new( 'C2-TYPBLC-SO', 2 ),
        oEdtk::Field->new( 'C2-IDFREL-SO', 8 ),
        oEdtk::Field->new( 'C2-CODGRA-SO', 6 ),
        oEdtk::Field->new( 'C2-CODSIR-SO', 14 ),

        # C2-NOM-SO
        oEdtk::Field->new( 'C2-PENOCODTIT-SO', 10 ),
        oEdtk::Field->new( 'C2-PENOLICTIT-SO', 15 ),
        oEdtk::Field->new( 'C2-PENOLIBLON-SO', 50 ),

        # C2-ADRESSE-SO
        oEdtk::Field->new( 'C2-PNTREM-SO',       32 ),
        oEdtk::Field->new( 'C2-CPLADR-SO',       32 ),
        oEdtk::Field->new( 'C2-PEADNUMVOI-SO',   9 ),
        oEdtk::Field->new( 'C2-PEADBTQVOI-SO',   2 ),
        oEdtk::Field->new( 'C2-PEADLICBTQ-SO',   15 ),
        oEdtk::Field->new( 'C2-PEVONATVOI-SO',   5 ),
        oEdtk::Field->new( 'C2-PEADLICNTV-SO',   15 ),
        oEdtk::Field->new( 'C2-LIBVOI-SO',       32 ),
        oEdtk::Field->new( 'C2-INDCDX-SO',       1 ),
        oEdtk::Field->new( 'C2-BOIPOSLIBLDT-SO', 32 ),
        oEdtk::Field->new( 'C2-LIBLOCLIBLOC-SO', 32 ),
        oEdtk::Field->new( 'C2-CODCDXCODPOS-SO', 10 ),
        oEdtk::Field->new( 'C2-NOMCDXLIBACH-SO', 32 ),
        oEdtk::Field->new( 'C2-PEPACODPAY-SO',   3 ),
        oEdtk::Field->new( 'C2-PEPALICPAY-SO',   15 ),
        oEdtk::Field->new( 'C2-TRNFAC-SO',       4 ),

        # C2-ZZEDIT-ADM-CT
        oEdtk::Field->new( 'C2-IDFSYS-SO',  8 ),
        oEdtk::Field->new( 'C2-LIBCTYPCCO', 15 ),
        oEdtk::Field->new( 'C2-LIBTYPCCO',  50 ),
        oEdtk::Field->new( 'C2-ACCCNUMCCO', 15 ),
        oEdtk::Field->new( 'C2-ACH6NUECCO', 15 ),
        oEdtk::Field->new( 'C2-LICORICTT',  15 ),
        oEdtk::Field->new( 'C2-LIBORICTT',  50 ),
        oEdtk::Field->new( 'C2-DATEDT',     8 ),
        oEdtk::Field->new( 'C2-ODGGCODGGS', 9 ),
        oEdtk::Field->new( 'C2-ACH6DDESOU', 8 ),
        oEdtk::Field->new( 'C2-ACH6DDERES', 8 ),
        oEdtk::Field->new( 'C2-ACH6ECHPAL', 4 ),
        oEdtk::Field->new( 'C2-ACH6CODDVG', 3 ),
        oEdtk::Field->new( 'C2-LICCODDVG',  15 ),
        oEdtk::Field->new( 'C2-LIBCODDVG',  50 ),
        oEdtk::Field->new( 'C2-ACH6NOMCNT', 50 ),

        # C2-ZZEDIT-PROF
        oEdtk::Field->new( 'C2-PROFCODOFF', 10 ),
        oEdtk::Field->new( 'C2-PROFLIBCOU', 15 ),
        oEdtk::Field->new( 'C2-PROFLIBLON', 50 ),

        # C2-ACEN
        oEdtk::Field->new( 'C2-ACENCODEVL', 15 ),

        # C2-ZZEDIT-ENUT
        oEdtk::Field->new( 'C2-ENUTCODUTI', 8 ),

        # C2-NOM-UT
        oEdtk::Field->new( 'C2-PENOCODCIV-UT', 10 ),
        oEdtk::Field->new( 'C2-PENOLICCIV-UT', 15 ),
        oEdtk::Field->new( 'C2-PENOCODTIT-UT', 10 ),
        oEdtk::Field->new( 'C2-PENOLICTIT-UT', 15 ),
        oEdtk::Field->new( 'C2-PENOLIBLON-UT', 50 ),
        oEdtk::Field->new( 'C2-PENOPRN01-UT',  20 ),

        # C2-IDENT-AGC
        oEdtk::Field->new( 'C2-TYPBLC-AG',  2 ),
        oEdtk::Field->new( 'C2-ODAGCODAGC', 9 ),

        # C2-NOM-AG
        oEdtk::Field->new( 'C2-PENOCODTIT-AG', 10 ),
        oEdtk::Field->new( 'C2-PENOLICTIT-AG', 15 ),
        oEdtk::Field->new( 'C2-PENOLIBLON-AG', 50 ),

        # C2-ADRESSE-AG
        oEdtk::Field->new( 'C2-PNTREM-AG',       32 ),
        oEdtk::Field->new( 'C2-CPLADR-AG',       32 ),
        oEdtk::Field->new( 'C2-PEADNUMVOI-AG',   9 ),
        oEdtk::Field->new( 'C2-PEADBTQVOI-AG',   2 ),
        oEdtk::Field->new( 'C2-PEADLICBTQ-AG',   15 ),
        oEdtk::Field->new( 'C2-PEVONATVOI-AG',   5 ),
        oEdtk::Field->new( 'C2-PEADLICNTV-AG',   15 ),
        oEdtk::Field->new( 'C2-LIBVOI-AG',       32 ),
        oEdtk::Field->new( 'C2-INDCDX-AG',       1 ),
        oEdtk::Field->new( 'C2-BOIPOSLIBLDT-AG', 32 ),
        oEdtk::Field->new( 'C2-LIBLOCLIBLOC-AG', 32 ),
        oEdtk::Field->new( 'C2-CODCDXCODPOS-AG', 10 ),
        oEdtk::Field->new( 'C2-NOMCDXLIBACH-AG', 32 ),
        oEdtk::Field->new( 'C2-PEPACODPAY-AG',   3 ),
        oEdtk::Field->new( 'C2-PEPALICPAY-AG',   15 ),
        oEdtk::Field->new( 'C2-TRNFAC-AG',       4 ),

        # C2-COMM-AG
        oEdtk::Field->new( 'C2-PECONATCOM-AG', 5 ),
        oEdtk::Field->new( 'C2-PENOLICCOM-AG', 15 ),
        oEdtk::Field->new( 'C2-PECOVALCOM-AG', 50 ),

        # C2-COMM-AG-2
        oEdtk::Field->new( 'C2-PECONATCOM-AG-2', 5 ),
        oEdtk::Field->new( 'C2-PENOLICCOM-AG-2', 15 ),
        oEdtk::Field->new( 'C2-PECOVALCOM-AG-2', 50 ),

        # C2-COMM-AG-3
        oEdtk::Field->new( 'C2-PECONATCOM-AG-3', 5 ),
        oEdtk::Field->new( 'C2-PENOLICCOM-AG-3', 15 ),
        oEdtk::Field->new( 'C2-PECOVALCOM-AG-3', 50 ),

        # C2-ZZEDIT-ODRS
        oEdtk::Field->new( 'C2-TYPBLC-RS',  2 ),
        oEdtk::Field->new( 'C2-ODRSCODRDI', 9 ),
        oEdtk::Field->new( 'C2-ODRSLIBLON', 50 ),

        # C2-ZZEDIT-CTX-EDITION
        oEdtk::Field->new( 'C2-ACOPCODAGC',     4 ),
        oEdtk::Field->new( 'C2-ACZZ-LICCODAGC', 15 ),
        oEdtk::Field->new( 'C2-ACZZ-LIBCODAGC', 50 ),
        oEdtk::Field->new( 'C2-ACOPCODCTX',     4 ),
        oEdtk::Field->new( 'C2-ACZZ-LICCODCTX', 15 ),
        oEdtk::Field->new( 'C2-ACZZ-LIBCODCTX', 50 ),
        oEdtk::Field->new( 'C2-ACOPDDEOPE',     8 ),
        oEdtk::Field->new( 'C2-ACOPDDECOT',     8 ),
        oEdtk::Field->new( 'C2-ACOPDATDEM',     8 ),
        oEdtk::Field->new( 'C2-ACOPDATVTE',     8 ),
        oEdtk::Field->new( 'C2-ZZZZ-NUMAVT',    3 ),

        # C2-NUMERO-PERSONNES
        oEdtk::Field->new( 'C2-PENONUMPER-SO', 8 ),
        oEdtk::Field->new( 'C2-PENONUMPER-GA', 8 ),
        oEdtk::Field->new( 'C2-PENONUMPER-PA', 8 ),
        oEdtk::Field->new( 'C2-PENONUMPER-UT', 8 ),
        oEdtk::Field->new( 'C2-PENONUMPER-AG', 8 ),
    );
}

1;
