package oEUser::Descriptor::T3;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # T3-LGNDAT
        # T3-IDENT-PART
        oEdtk::Field->new( 'T3-NUMPART', 5 ),
        oEdtk::Field->new( 'T3-FILLER',  15 ),

        # T3-SOLDE-PART
        oEdtk::SignedField->new( 'T3-DEJA-PERCU',         12, 2 ),
        oEdtk::SignedField->new( 'T3-RESTE-DU-ANTERIEUR', 12, 2 ),

        # T3-HORS-PERIODE-DIFF-C-I
        oEdtk::Field->new( 'T3-FILLER1', 1 ),
        oEdtk::SignedField->new( 'T3-TOT-CONSO-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T3-FILLER2', 1 ),
        oEdtk::SignedField->new( 'T3-TOT-IND-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T3-FILLER3', 1 ),
        oEdtk::SignedField->new( 'T3-DIFF-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T3-FILLER4',              1 ),
        oEdtk::Field->new( 'T3-TEST-YA-DIF-HORS-PER', 2 ),

        # T3-YA-DIFF-HORS-PER
        # T3-MT-SOLDE-EC
        oEdtk::Field->new( 'T3-FILLER5', 1 ),
        oEdtk::SignedField->new( 'T3-MTTOT-DETTE', 12, 2 ),
        oEdtk::SignedField->new( 'T3-MTTOT-ENC',   12, 2 ),

        # T3-MT-PERIODE-AC019
        oEdtk::Field->new( 'T3-FILLER6', 1 ),
        oEdtk::SignedField->new( 'T3-HT-DEJA-APP-SUR-PER',  12, 2 ),
        oEdtk::SignedField->new( 'T3-TTC-DEJA-APP-SUR-PER', 12, 2 ),
    );
}

1;
