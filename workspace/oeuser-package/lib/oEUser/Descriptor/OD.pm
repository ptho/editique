package oEUser::Descriptor::OD;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # OD-ENR
        oEdtk::Field->new( 'OD-CODE-CTC',   50 ),
        oEdtk::Field->new( 'OD-LIBELLE-CT', 50 ),
        oEdtk::Field->new( 'OD-LIGNE-ADR1', 50 ),
        oEdtk::Field->new( 'OD-LIGNE-ADR2', 50 ),
        oEdtk::Field->new( 'OD-LIGNE-ADR3', 50 ),
        oEdtk::Field->new( 'OD-LIGNE-ADR4', 50 ),
        oEdtk::Field->new( 'OD-LIGNE-ADR5', 50 ),
        oEdtk::Field->new( 'OD-CODEPOSTAL', 50 ),
        oEdtk::Field->new( 'OD-VILLE',      50 ),
        oEdtk::Field->new( 'OD-PAYS',       50 ),
    );
}

1;
