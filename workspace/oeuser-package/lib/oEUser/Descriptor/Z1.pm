package oEUser::Descriptor::Z1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # Z1-LGNDAT
        oEdtk::Field->new( 'Z1-NBRREF', 2 ),

        # Z1-TABLEAU-REXEXT
        # Z1-TABREF
        oEdtk::Field->new( 'Z1-PERFCODSYS', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF', 30 ),

        # Z1-TABREF-2
        oEdtk::Field->new( 'Z1-PERFCODSYS-2', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-2',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-2', 30 ),

        # Z1-TABREF-3
        oEdtk::Field->new( 'Z1-PERFCODSYS-3', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-3',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-3', 30 ),

        # Z1-TABREF-4
        oEdtk::Field->new( 'Z1-PERFCODSYS-4', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-4',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-4', 30 ),

        # Z1-TABREF-5
        oEdtk::Field->new( 'Z1-PERFCODSYS-5', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-5',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-5', 30 ),

        # Z1-TABREF-6
        oEdtk::Field->new( 'Z1-PERFCODSYS-6', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-6',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-6', 30 ),

        # Z1-TABREF-7
        oEdtk::Field->new( 'Z1-PERFCODSYS-7', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-7',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-7', 30 ),

        # Z1-TABREF-8
        oEdtk::Field->new( 'Z1-PERFCODSYS-8', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-8',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-8', 30 ),

        # Z1-TABREF-9
        oEdtk::Field->new( 'Z1-PERFCODSYS-9', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-9',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-9', 30 ),

        # Z1-TABREF-10
        oEdtk::Field->new( 'Z1-PERFCODSYS-10', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-10',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-10', 30 ),

        # Z1-TABREF-11
        oEdtk::Field->new( 'Z1-PERFCODSYS-11', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-11',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-11', 30 ),

        # Z1-TABREF-12
        oEdtk::Field->new( 'Z1-PERFCODSYS-12', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-12',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-12', 30 ),

        # Z1-TABREF-13
        oEdtk::Field->new( 'Z1-PERFCODSYS-13', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-13',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-13', 30 ),

        # Z1-TABREF-14
        oEdtk::Field->new( 'Z1-PERFCODSYS-14', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-14',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-14', 30 ),

        # Z1-TABREF-15
        oEdtk::Field->new( 'Z1-PERFCODSYS-15', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-15',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-15', 30 ),

        # Z1-TABREF-16
        oEdtk::Field->new( 'Z1-PERFCODSYS-16', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-16',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-16', 30 ),

        # Z1-TABREF-17
        oEdtk::Field->new( 'Z1-PERFCODSYS-17', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-17',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-17', 30 ),

        # Z1-TABREF-18
        oEdtk::Field->new( 'Z1-PERFCODSYS-18', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-18',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-18', 30 ),

        # Z1-TABREF-19
        oEdtk::Field->new( 'Z1-PERFCODSYS-19', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-19',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-19', 30 ),

        # Z1-TABREF-20
        oEdtk::Field->new( 'Z1-PERFCODSYS-20', 10 ),
        oEdtk::Field->new( 'Z1-CODREFEXT-20',  2 ),
        oEdtk::Field->new( 'Z1-PERFCODREF-20', 30 ),
        oEdtk::Field->new( 'FILLER',           1658 ),
    );
}

1;
