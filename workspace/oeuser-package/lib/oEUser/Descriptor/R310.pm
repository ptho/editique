package oEUser::Descriptor::R310;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R310-LGNDAT
        oEdtk::Field->new( 'R310-ODGGCODGGS', 9 ),
        oEdtk::Field->new( 'R310-ACGGDDEPRD', 8 ),
        oEdtk::Field->new( 'R310-ACGGDFEPRD', 8 ),
        oEdtk::Field->new( 'R310-ODCGCODCGS', 9 ),
        oEdtk::Field->new( 'R310-ACCGDDEPRD', 8 ),
        oEdtk::Field->new( 'R310-ACCGDFEPRD', 8 ),
    );
}

1;
