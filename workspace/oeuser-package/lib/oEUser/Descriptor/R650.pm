package oEUser::Descriptor::R650;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R650-LGNDAT
        oEdtk::Field->new( 'R650-PEPENUMPER', 8 ),
        oEdtk::Field->new( 'R650-PERFDDE',    8 ),
        oEdtk::Field->new( 'R650-PERFDFE',    8 ),
        oEdtk::Field->new( 'R650-PERFCODSYS', 10 ),
        oEdtk::Field->new( 'R650-PERFVAL001', 30 ),
        oEdtk::Field->new( 'R650-PERFVAL002', 30 ),
        oEdtk::Field->new( 'R650-PERFVAL003', 30 ),
        oEdtk::Field->new( 'R650-PERFVAL004', 30 ),
        oEdtk::Field->new( 'R650-PERFVAL005', 30 ),
        oEdtk::Field->new( 'R650-ACOPNUMCIN', 15 ),
        oEdtk::Field->new( 'R650-ACOPNUMOPE', 5 ),
    );
}

1;
