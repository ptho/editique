package oEUser::Descriptor::NR;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CE031C-NR-LGNDAT
        oEdtk::Field->new( 'CE031C-NR-ACNDCODSIT', 2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNDCODACD', 10 ),
        oEdtk::Field->new( 'CE031C-NR-ACNETYPECH', 2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEETAECH', 2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNENUMEMI', 8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNESTAINT', 2 ),
        oEdtk::Field->new( 'CE031C-NR-ENCTCODCTI', 9 ),
        oEdtk::Field->new( 'CE031C-NR-ENCTCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NR-ENCTLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NR-ENCTLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEDEVNMI', 8 ),
        oEdtk::Field->new( 'CE031C-NR-ENCRCODGRE', 2 ),
        oEdtk::Field->new( 'CE031C-NR-ENCRCODCRO', 3 ),
        oEdtk::Field->new( 'CE031C-NR-ENCRCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NR-ENCRLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NR-ENCRLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NR-ENACNUMORC', 10 ),
        oEdtk::Field->new( 'CE031C-NR-ENCNCODCPA', 4 ),
        oEdtk::Field->new( 'CE031C-NR-ENCNCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NR-ENCNLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NR-ENCNLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NR-ODERCODERA', 9 ),

        # CE031C-NR-ODERNOMERA
        oEdtk::Field->new( 'CE031C-NR-ODERCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NR-ODERLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NR-ODERLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NR-ODCGCODCGS', 9 ),

        # CE031C-NR-ODCGNOMCGS
        oEdtk::Field->new( 'CE031C-NR-ODCGCODTIT',    10 ),
        oEdtk::Field->new( 'CE031C-NR-ODCGLICTIT',    15 ),
        oEdtk::Field->new( 'CE031C-NR-ODCGLIBLON',    50 ),
        oEdtk::Field->new( 'CE031C-NR-ACNDCODMRO',    13 ),
        oEdtk::Field->new( 'CE031C-NR-ACNDCLEMRO',    2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNENATNOM',    1 ),
        oEdtk::Field->new( 'CE031C-NR-ACNENOMASU',    50 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEPRNASU',    15 ),
        oEdtk::Field->new( 'CE031C-NR-ACNDDATNAI',    8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNDRNGNAI',    1 ),
        oEdtk::Field->new( 'CE031C-NR-ACNENUMTRS',    8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNECODMVT',    1 ),
        oEdtk::Field->new( 'CE031C-NR-ACNETYPCTT1',   2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEDDECTT1',   8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEDFECTT1',   8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNETYPCTT2',   2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEDDECTT2',   8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEDFECTT2',   8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNETYPCTT3',   2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEDDECTT3',   8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEDFECTT3',   8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNENOMAYD',    50 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEPRNAYD',    15 ),
        oEdtk::Field->new( 'CE031C-NR-ACNECODUGR',    4 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEACOPR1',    8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNEACOPR2',    8 ),
        oEdtk::Field->new( 'CE031C-NR-ACCINUMCIN',    15 ),
        oEdtk::Field->new( 'CE031C-NR-ACNECODMFE',    8 ),
        oEdtk::Field->new( 'CE031C-NR-LILMFE-E0142',  8 ),
        oEdtk::Field->new( 'CE031C-NR-ACNECODMFR',    10 ),
        oEdtk::Field->new( 'CE031C-NR-LILDMFR-U0121', 10 ),
        oEdtk::Field->new( 'CE031C-NR-ACNELIBRET',    100 ),
        oEdtk::Field->new( 'CE031C-NR-ACNAIDFSYS',    10 ),
        oEdtk::Field->new( 'CE031C-NR-ACNACODACN',    4 ),
        oEdtk::Field->new( 'CE031C-NR-ACNACMRUTI',    100 ),
        oEdtk::Field->new( 'CE031C-NR-ACNACODCRR',    6 ),
        oEdtk::Field->new( 'CE031C-NR-ACNANOUSTA',    2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNAANCSTA',    2 ),
        oEdtk::Field->new( 'CE031C-NR-ACNADELREE',    3 ),
        oEdtk::Field->new( 'CE031C-NR-ACNACODETA',    2 ),
    );
}

1;
