package oEUser::Descriptor::Z0;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # Z0-LGNDAT
        oEdtk::Field->new( 'Z0-ENPEIDFSYS', 10 ),
        oEdtk::Field->new( 'FILLER',        2490 ),
    );
}

1;
