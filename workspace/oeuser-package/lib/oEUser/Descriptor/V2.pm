package oEUser::Descriptor::V2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # V2-ENREG
        oEdtk::Field->new( 'V2-ACH5CODPARX', 3 ),
        oEdtk::Field->new( 'V2-CODICS',      13 ),
        oEdtk::Field->new( 'V2-ACMDREFRUM',  35 ),
        oEdtk::Field->new( 'V2-ACMDREFRUI',  35 ),
        oEdtk::Field->new( 'V2-ACMDDATSIG',  8 ),
        oEdtk::Field->new( 'V2-ECC1DATECH',  8 ),
        oEdtk::Field->new( 'V2-ECC1DATVAL',  8 ),
        oEdtk::Field->new( 'V2-ECD1DATECH',  8 ),
    );
}

1;
