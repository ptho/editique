package oEUser::Descriptor::U1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # SCC-U1-ENRLOT
        # SCC-100-IDFLOT
        oEdtk::Field->new( 'SCC-100-NUMFLX', 20 ),
        oEdtk::Field->new( 'SCC-100-NUMLOT', 6 ),
        oEdtk::Field->new( 'SCC-100-NUMDOS', 6 ),
        oEdtk::Field->new( 'SCC-100-NUMLCO', 6 ),
        oEdtk::Field->new( 'SCC-100-NUMLGA', 6 ),
        oEdtk::Field->new( 'SCC-100-TYPENR', 3 ),
        oEdtk::Field->new( 'SCC-100-STYENR', 3 ),

        # SCC-100-DONLOT
        oEdtk::Field->new( 'SCC-100-CODSIT', 2 ),

        # SCC-100-DONFIC
        oEdtk::Field->new( 'SCC-100-TYPEME',  2 ),
        oEdtk::Field->new( 'SCC-100-CODEME',  14 ),
        oEdtk::Field->new( 'SCC-100-TYPDST',  2 ),
        oEdtk::Field->new( 'SCC-100-CODDST',  14 ),
        oEdtk::Field->new( 'SCC-100-TYPNME',  2 ),
        oEdtk::Field->new( 'SCC-100-REFNME',  4 ),
        oEdtk::Field->new( 'SCC-100-VERNME',  2 ),
        oEdtk::Field->new( 'SCC-100-NUMORC',  10 ),
        oEdtk::Field->new( 'SCC-100-FICORIG', 10 ),

        # SCC-100-DATCREX
        oEdtk::Field->new( 'SCC-100-DATCRE', 8 ),
        oEdtk::Field->new( 'SCC-100-CODGRE', 2 ),
        oEdtk::Field->new( 'SCC-100-NUMCAI', 3 ),
        oEdtk::Field->new( 'SCC-100-NUMCGS', 4 ),
        oEdtk::Field->new( 'SCC-100-MODRTO', 4 ),
        oEdtk::Field->new( 'SCC-100-MODLOT', 2 ),

        # SCC-100-DONLOX
        oEdtk::Field->new( 'SCC-100-NUMLOX', 6 ),
        oEdtk::Field->new( 'SCC-100-ORIFIC', 6 ),

        # SCC-100-NUMPSAX
        oEdtk::Field->new( 'SCC-100-NUMPSA', 9 ),

        # SCC-100-DCRLOTX
        oEdtk::Field->new( 'SCC-100-DCRLOT', 8 ),
        oEdtk::Field->new( 'SCC-100-INDCRT', 1 ),
        oEdtk::Field->new( 'SCC-100-CODCAT', 3 ),
        oEdtk::Field->new( 'SCC-100-STAJUR', 2 ),
        oEdtk::Field->new( 'SCC-100-MODTRF', 2 ),

        # SCC-100-DENOMB
        # SCC-100-NBRBENX
        oEdtk::Field->new( 'SCC-100-NBRBEN', 10 ),

        # SCC-100-NBRLCOX
        oEdtk::Field->new( 'SCC-100-NBRLCO', 10 ),

        # SCC-100-NBRLGAX
        oEdtk::Field->new( 'SCC-100-NBRLGA', 10 ),
    );
}

1;
