package oEUser::Descriptor::D3;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # D3-INDU
        # D3-INFOS-INDU
        oEdtk::Field->new( 'D3-DCINIDFSYS', 10 ),
        oEdtk::Field->new( 'D3-DCINTYPINU', 2 ),
        oEdtk::Field->new( 'D3-LIBETYPINU', 50 ),
        oEdtk::Field->new( 'D3-DCINCODFRI', 6 ),
        oEdtk::Field->new( 'D3-LIBECODFRI', 50 ),
        oEdtk::Field->new( 'D3-DCINCODIPA', 1 ),
        oEdtk::Field->new( 'D3-LIBECODIPA', 50 ),
        oEdtk::Field->new( 'D3-DCINMOTINU', 2 ),
        oEdtk::Field->new( 'D3-LIBEMOTINU', 50 ),
        oEdtk::SignedField->new( 'D3-DCINMNTINU', 12, 2 ),
        oEdtk::Field->new( 'D3-DCINDATINU', 19 ),
        oEdtk::Field->new( 'D3-DCINDLPINU', 19 ),
        oEdtk::Field->new( 'D3-DCINCODMOG', 3 ),
        oEdtk::Field->new( 'D3-LIBECODMOG', 50 ),
        oEdtk::Field->new( 'D3-DCINREFLB1', 30 ),
        oEdtk::Field->new( 'D3-DCINREFLB2', 30 ),
        oEdtk::Field->new( 'D3-DCINREFLB3', 30 ),
        oEdtk::Field->new( 'D3-DCINTYPEVT', 10 ),
        oEdtk::Field->new( 'D3-DCINDCCDR1', 10 ),
        oEdtk::Field->new( 'D3-DCINODCGI1', 8 ),
        oEdtk::Field->new( 'D3-DCINPSODI1', 10 ),

        # D3-INFOS-HISTO-INDU
        oEdtk::Field->new( 'D3-DCIHDCINR1', 10 ),
        oEdtk::Field->new( 'D3-DCIHDCOCR2', 8 ),
        oEdtk::SignedField->new( 'D3-DCIHSLDINU', 12, 2 ),
        oEdtk::Field->new( 'D3-DCIHCODETA', 2 ),
        oEdtk::Field->new( 'D3-DCIHINDCPS', 1 ),
        oEdtk::Field->new( 'D3-DCIHINDRTO', 1 ),
        oEdtk::Field->new( 'D3-DCIHDCOCR1', 10 ),
        oEdtk::Field->new( 'D3-DCIHINDCPB', 1 ),

        # D3-COMPTE-DECAIS
        oEdtk::Field->new( 'D3-CODTYPDST',  2 ),
        oEdtk::Field->new( 'D3-DCCDACDPI1', 10 ),
        oEdtk::Field->new( 'D3-DCCDACDPI2', 10 ),
        oEdtk::Field->new( 'D3-DCCDENFNI1', 10 ),
        oEdtk::Field->new( 'D3-ACCINUMCIN', 15 ),
        oEdtk::Field->new( 'D3-ACCCNUMCCO', 15 ),

        # D3-DC-IDENT-FRN
        oEdtk::Field->new( 'D3-ENFNTYPFRN', 2 ),
        oEdtk::Field->new( 'D3-ENFNCODFRN', 9 ),
        oEdtk::Field->new( 'D3-DCINCODCIV', 6 ),
        oEdtk::Field->new( 'D3-DCINLIBLON', 15 ),
        oEdtk::Field->new( 'D3-DCINNOMASS', 50 ),
        oEdtk::Field->new( 'D3-DCINPRNASS', 20 ),
        oEdtk::Field->new( 'D3-DCINDDESOI', 8 ),
        oEdtk::Field->new( 'D3-DCINDFESOI', 8 ),
        oEdtk::Field->new( 'D3-DCINDATFCU', 8 ),
        oEdtk::Field->new( 'D3-DCINDATSUS', 8 ),
        oEdtk::Field->new( 'D3-DCINDATRAD', 8 ),
        oEdtk::Field->new( 'D3-DCINTYPFRN', 2 ),
        oEdtk::Field->new( 'D3-DCINNOMFRN', 50 ),
    );
}

1;
