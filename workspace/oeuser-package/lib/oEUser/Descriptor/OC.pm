package oEUser::Descriptor::OC;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new( oEdtk::Field->new( 'OC-MSG', 12 ) );
}

1;
