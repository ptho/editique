package oEUser::Descriptor::L2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # L2-LGNDAT
        # L2-COTIS-PARTI
        oEdtk::Field->new( 'L2-CODE-CODCEC',   9 ),
        oEdtk::Field->new( 'L2-LIBCOU-CODCEC', 15 ),
        oEdtk::Field->new( 'L2-LIBLON-CODCEC', 50 ),
        oEdtk::Field->new( 'L2-TYPCEC',        6 ),
        oEdtk::Field->new( 'L2-LIBCOU-TYPCEC', 15 ),
        oEdtk::Field->new( 'L2-LIBLON-TYPCEC', 50 ),
        oEdtk::Field->new( 'L2-TYPCNP',        1 ),
        oEdtk::Field->new( 'L2-LIBCOU-TYPCNP', 15 ),
        oEdtk::Field->new( 'L2-LIBLON-TYPCNP', 50 ),
        oEdtk::Field->new( 'L2-MTFCNP',        5 ),
        oEdtk::Field->new( 'L2-LIBCOU-MTFCNP', 15 ),
        oEdtk::Field->new( 'L2-LIBLON-MTFCNP', 50 ),
        oEdtk::SignedField->new( 'L2-MONTANT-HT', 12, 2 ),
        oEdtk::Field->new( 'L2-ECHIPU', 8 ),

        # L2-INFO-TAXES
        oEdtk::Field->new( 'L2-CODTAX1', 6 ),
        oEdtk::SignedField->new( 'L2-TAUX-TAX1',    4,  5 ),
        oEdtk::SignedField->new( 'L2-MONTANT-TAX1', 12, 2 ),
        oEdtk::Field->new( 'L2-IDFSYS-TAX1', 8 ),
        oEdtk::Field->new( 'L2-LIBLON-TAX1', 50 ),
        oEdtk::Field->new( 'L2-CODTAX2',     6 ),
        oEdtk::SignedField->new( 'L2-TAUX-TAX2',    4,  5 ),
        oEdtk::SignedField->new( 'L2-MONTANT-TAX2', 12, 2 ),
        oEdtk::Field->new( 'L2-IDFSYS-TAX2', 8 ),
        oEdtk::Field->new( 'L2-LIBLON-TAX2', 50 ),
        oEdtk::Field->new( 'L2-CODTAX3',     6 ),
        oEdtk::SignedField->new( 'L2-TAUX-TAX3',    4,  5 ),
        oEdtk::SignedField->new( 'L2-MONTANT-TAX3', 12, 2 ),
        oEdtk::Field->new( 'L2-IDFSYS-TAX3', 8 ),
        oEdtk::Field->new( 'L2-LIBLON-TAX3', 50 ),
        oEdtk::Field->new( 'L2-CODTAX4',     6 ),
        oEdtk::SignedField->new( 'L2-TAUX-TAX4',    4,  5 ),
        oEdtk::SignedField->new( 'L2-MONTANT-TAX4', 12, 2 ),
        oEdtk::Field->new( 'L2-IDFSYS-TAX4', 8 ),
        oEdtk::Field->new( 'L2-LIBLON-TAX4', 50 ),
        oEdtk::Field->new( 'L2-CODTAX5',     6 ),
        oEdtk::SignedField->new( 'L2-TAUX-TAX5',    4,  5 ),
        oEdtk::SignedField->new( 'L2-MONTANT-TAX5', 12, 2 ),
        oEdtk::Field->new( 'L2-IDFSYS-TAX5', 8 ),
        oEdtk::Field->new( 'L2-LIBLON-TAX5', 50 ),
        oEdtk::Field->new( 'L2-DATEXI',      8 ),
        oEdtk::Field->new( 'L2-FILLER',      242 ),
    );
}

1;
