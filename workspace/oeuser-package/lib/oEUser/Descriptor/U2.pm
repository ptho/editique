package oEUser::Descriptor::U2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # SCC-U2-ENRDOS
        # SCC-200-IDFDOS
        oEdtk::Field->new( 'SCC-200-NUMFLX', 20 ),
        oEdtk::Field->new( 'SCC-200-NUMLOT', 6 ),
        oEdtk::Field->new( 'SCC-200-NUMDOS', 6 ),
        oEdtk::Field->new( 'SCC-200-NUMLCO', 6 ),
        oEdtk::Field->new( 'SCC-200-NUMLGA', 6 ),
        oEdtk::Field->new( 'SCC-200-TYPENR', 3 ),
        oEdtk::Field->new( 'SCC-200-STYENR', 3 ),

        # SCC-200-DONDOS
        oEdtk::Field->new( 'SCC-200-CODORI', 5 ),

        # SCC-200-DDSOINX
        oEdtk::Field->new( 'SCC-200-DDSOIN', 8 ),

        # SCC-200-DFSOINX
        oEdtk::Field->new( 'SCC-200-DFSOIN', 8 ),
        oEdtk::Field->new( 'SCC-200-NUMORC', 10 ),
        oEdtk::Field->new( 'SCC-200-ORCSUB', 10 ),

        # SCC-200-DONFAC
        oEdtk::Field->new( 'SCC-200-NUMLOX', 15 ),

        # SCC-200-DCRLOTX
        oEdtk::Field->new( 'SCC-200-DCRLOT', 8 ),

        # SCC-200-NUMFACX
        oEdtk::Field->new( 'SCC-200-NUMFAC', 9 ),

        # SCC-200-DATFACX
        oEdtk::Field->new( 'SCC-200-DATFAC', 8 ),

        # SCC-200-DATCPTX
        oEdtk::Field->new( 'SCC-200-DATCPT', 8 ),
        oEdtk::Field->new( 'SCC-200-DEVISE', 3 ),

        # SCC-200-NBRDCMX
        oEdtk::Field->new( 'SCC-200-NBRDCM', 1 ),
        oEdtk::Field->new( 'SCC-200-NATOPE', 1 ),
        oEdtk::Field->new( 'SCC-200-REFORI', 30 ),
        oEdtk::Field->new( 'SCC-200-ETADPR', 1 ),

        # SCC-200-DONASS
        oEdtk::Field->new( 'SCC-200-ASSINS', 13 ),

        # SCC-200-ASSCLEX
        oEdtk::Field->new( 'SCC-200-ASSCLE',  2 ),
        oEdtk::Field->new( 'SCC-200-ASSNTN1', 1 ),
        oEdtk::Field->new( 'SCC-200-ASSNOM1', 25 ),
        oEdtk::Field->new( 'SCC-200-ASSPRN1', 15 ),
        oEdtk::Field->new( 'SCC-200-ASSNTN2', 1 ),
        oEdtk::Field->new( 'SCC-200-ASSNOM2', 25 ),
        oEdtk::Field->new( 'SCC-200-ASSPRN2', 15 ),
        oEdtk::Field->new( 'SCC-200-ASSNMU',  9 ),

        # SCC-200-DONBEN
        oEdtk::Field->new( 'SCC-200-BENINS', 13 ),

        # SCC-200-BENCLEX
        oEdtk::Field->new( 'SCC-200-BENCLE', 2 ),

        # SCC-200-BENTYCX
        oEdtk::Field->new( 'SCC-200-BENTYC', 2 ),

        # SCC-200-BENDNAX
        oEdtk::Field->new( 'SCC-200-BENDNA',  8 ),
        oEdtk::Field->new( 'SCC-200-BENRRO',  2 ),
        oEdtk::Field->new( 'SCC-200-BENQUA',  2 ),
        oEdtk::Field->new( 'SCC-200-BENRGL',  1 ),
        oEdtk::Field->new( 'SCC-200-BENNTN1', 1 ),
        oEdtk::Field->new( 'SCC-200-BENNOM1', 25 ),
        oEdtk::Field->new( 'SCC-200-BENPRN1', 15 ),
        oEdtk::Field->new( 'SCC-200-BENNTN2', 2 ),
        oEdtk::Field->new( 'SCC-200-BENNOM2', 25 ),
        oEdtk::Field->new( 'SCC-200-BENPRN2', 25 ),
        oEdtk::Field->new( 'SCC-200-BENCMU',  1 ),
        oEdtk::Field->new( 'SCC-200-NUMCTR',  15 ),

        # SCC-200-BENRRCX
        oEdtk::Field->new( 'SCC-200-BENRRC', 4 ),
        oEdtk::Field->new( 'SCC-200-BENUGE', 4 ),

        # SCC-200-DONRSQ
        oEdtk::Field->new( 'SCC-200-NATASS', 2 ),

        # SCC-200-NUMSINX
        oEdtk::Field->new( 'SCC-200-NUMSIN', 9 ),

        # SCC-200-DATSINX
        oEdtk::Field->new( 'SCC-200-DATSIN', 8 ),

        # SCC-200-NATPCEX
        oEdtk::Field->new( 'SCC-200-NATPCE', 1 ),

        # SCC-200-DATPECX
        oEdtk::Field->new( 'SCC-200-DATPEC', 8 ),

        # SCC-200-JUSEXOX
        oEdtk::Field->new( 'SCC-200-JUSEXO', 1 ),
        oEdtk::Field->new( 'SCC-200-REGPRS', 3 ),

        # SCC-200-DATMATX
        oEdtk::Field->new( 'SCC-200-DATMAT', 8 ),

        # SCC-200-NUMENTX
        oEdtk::Field->new( 'SCC-200-NUMENT', 9 ),

        # SCC-200-DATHOSX
        oEdtk::Field->new( 'SCC-200-DATHOS', 8 ),

        # SCC-200-CRTDDVX
        oEdtk::Field->new( 'SCC-200-CRTDDV', 8 ),
        oEdtk::Field->new( 'SCC-200-CRTORG', 3 ),

        # SCC-200-DENHOSX
        oEdtk::Field->new( 'SCC-200-DENHOS', 8 ),

        # SCC-200-DSOHOSX
        oEdtk::Field->new( 'SCC-200-DSOHOS', 8 ),

        # SCC-200-HSOHOSX
        oEdtk::Field->new( 'SCC-200-HSOHOS', 2 ),

        # SCC-200-DONTPG
        # SCC-200-NUMTPGX
        oEdtk::Field->new( 'SCC-200-NUMTPG', 6 ),

        # SCC-200-CODCHX
        oEdtk::Field->new( 'SCC-200-CODCH', 2 ),

        # SCC-200-POSCPTX
        oEdtk::Field->new( 'SCC-200-POSCPT', 6 ),

        # SCC-200-EXECPTX
        oEdtk::Field->new( 'SCC-200-EXECPT', 2 ),

        # SCC-200-DENOMB
        # SCC-200-NBRLCOX
        oEdtk::Field->new( 'SCC-200-NBRLCO', 10 ),

        # SCC-200-NBRLGAX
        oEdtk::Field->new( 'SCC-200-NBRLGA', 10 ),
    );
}

1;
