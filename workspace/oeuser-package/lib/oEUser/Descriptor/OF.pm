package oEUser::Descriptor::OF;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # OF-ENR
        oEdtk::Field->new( 'OF-NUMIND',      15 ),
        oEdtk::Field->new( 'OF-ANO',         3 ),
        oEdtk::Field->new( 'OF-DATE',        8 ),
        oEdtk::Field->new( 'OF-SIGNCOMPTA',  1 ),
        oEdtk::Field->new( 'OF-SOLDECOMPT',  14 ),
        oEdtk::Field->new( 'OF-SIGNCOMPTB',  1 ),
        oEdtk::Field->new( 'OF-SOLDEGLOBAL', 14 ),
        oEdtk::Field->new( 'OF-SIGNCOMPTC',  1 ),
        oEdtk::Field->new( 'OF-MONTANT',     14 ),
        oEdtk::Field->new( 'OF-FREQUENCE',   2 ),
        oEdtk::Field->new( 'OF-NBREJET',     3 ),
        oEdtk::Field->new( 'OF-SIGNCOMPTD',  1 ),
        oEdtk::Field->new( 'OF-SOLDECOMPT',  14 ),
        oEdtk::Field->new( 'OF-MODEREGLE',   1 ),
    );
}

1;
