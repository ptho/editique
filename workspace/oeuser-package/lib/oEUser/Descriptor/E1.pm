package oEUser::Descriptor::E1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::Field;
use oEdtk::AddrField;

sub get {
    return oEdtk::Record->new(

        # E1-ZZEDIT-E1-LGNDAT
        # E1-ZZEDIT-DST-DOC
        oEdtk::Field->new( 'E1-ZZZZ-GESTION-DST-DOC', 2 ),

        # E1-ZZEDIT-NOM-P-PH
        oEdtk::AddrField->new( 'E1-PENOCODCIV', 10 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODCIV', 15 ),
        oEdtk::AddrField->new( 'E1-PENOCODTIT', 10 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODTIT', 15 ),
        oEdtk::AddrField->new( 'E1-PENOLIBLON', 50 ),
        oEdtk::AddrField->new( 'E1-PENOPRN01',  20 ),

        # E1-ZZEDIT-ADRESSE
        oEdtk::AddrField->new( 'E1-ZZZZ-PNTREM', 32 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-CPLADR', 32 ),
        oEdtk::AddrField->new( 'E1-PEADNUMVOI',  9 ),
        oEdtk::AddrField->new( 'E1-PEADBTQVOI',  2 ),

        oEdtk::Field->new( 'E1-PEZZ-LICBTQVOI', 15 ),
        oEdtk::AddrField->new( 'E1-PEVONATVOI', 5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATVOI', 15 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-LIBVOI', 32 ),
        oEdtk::Field->new( 'E1-ZZZZ-INDCDX', 1 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-BOIPOSLIBLDT', 32 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-LIBLOCLIBLOC', 32 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-CODCDXCODPOS', 10 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-NOMCDXLIBACH', 32 ),
        oEdtk::Field->new( 'E1-PEPACODPAY', 3 ),
        oEdtk::AddrField->new( 'E1-PEZZ-LICCODPAY', 15 ),
        oEdtk::Field->new( 'E1-ZZZZ-TRNFAC', 4 ),

        # E1-ZZEDIT-ODER
        oEdtk::Field->new( 'E1-ZZ-EDIT-TYPBLC', 2 ),
        oEdtk::Field->new( 'E1-ODERCODERA',     9 ),

        # E1-ZZEDIT-NOM-P-MO
        oEdtk::AddrField->new( 'E1-PENOCODTIT-2', 10 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODTIT-2', 15 ),
        oEdtk::AddrField->new( 'E1-PENOLIBLON-2', 50 ),

        # E1-ZZEDIT-ADRESSE
        oEdtk::AddrField->new( 'E1-ZZZZ-PNTREM-2', 32 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-CPLADR-2', 32 ),
        oEdtk::AddrField->new( 'E1-PEADNUMVOI-2',  9 ),
        oEdtk::AddrField->new( 'E1-PEADBTQVOI-2',  2 ),
        oEdtk::Field->new( 'E1-PEZZ-LICBTQVOI-2', 15 ),
        oEdtk::Field->new( 'E1-PEVONATVOI-2',     5 ),
        oEdtk::AddrField->new( 'E1-PEZZ-LICNATVOI-2', 15 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-LIBVOI-2',    32 ),
        oEdtk::Field->new( 'E1-ZZZZ-INDCDX-2', 1 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-BOIPOSLIBLDT-2', 32 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-LIBLOCLIBLOC-2', 32 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-CODCDXCODPOS-2', 10 ),
        oEdtk::AddrField->new( 'E1-ZZZZ-NOMCDXLIBACH-2', 32 ),
        oEdtk::Field->new( 'E1-PEPACODPAY-2',     3 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODPAY-2', 15 ),
        oEdtk::Field->new( 'E1-ZZZZ-TRNFAC-2',    4 ),

        # E1-ZZEDIT-COMM-1
        oEdtk::Field->new( 'E1-PECONATCOM',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM',     50 ),

        # E1-ZZEDIT-COMM-2
        oEdtk::Field->new( 'E1-PECONATCOM',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM',     50 ),

        # E1-ZZEDIT-COMM-3
        oEdtk::Field->new( 'E1-PECONATCOM',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM',     50 ),

        # E1-ZZEDIT-ODCG
        oEdtk::Field->new( 'E1-ZZ-EDIT-TYPBLC-2', 2 ),
        oEdtk::Field->new( 'E1-ODCGCODCGS',       9 ),

        # E1-ZZEDIT-NOM-P-MO
        oEdtk::Field->new( 'E1-PENOCODTIT-3',     10 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODTIT-3', 15 ),
        oEdtk::Field->new( 'E1-PENOLIBLON-3',     50 ),

        # E1-ZZEDIT-ADRESSE
        oEdtk::Field->new( 'E1-ZZZZ-PNTREM-3',       32 ),
        oEdtk::Field->new( 'E1-ZZZZ-CPLADR-3',       32 ),
        oEdtk::Field->new( 'E1-PEADNUMVOI-3',        9 ),
        oEdtk::Field->new( 'E1-PEADBTQVOI-3',        2 ),
        oEdtk::Field->new( 'E1-PEZZ-LICBTQVOI-3',    15 ),
        oEdtk::Field->new( 'E1-PEVONATVOI-3',        5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATVOI-3',    15 ),
        oEdtk::Field->new( 'E1-ZZZZ-LIBVOI-3',       32 ),
        oEdtk::Field->new( 'E1-ZZZZ-INDCDX-3',       1 ),
        oEdtk::Field->new( 'E1-ZZZZ-BOIPOSLIBLDT-3', 32 ),
        oEdtk::Field->new( 'E1-ZZZZ-LIBLOCLIBLOC-3', 32 ),
        oEdtk::Field->new( 'E1-ZZZZ-CODCDXCODPOS-3', 10 ),
        oEdtk::Field->new( 'E1-ZZZZ-NOMCDXLIBACH-3', 32 ),
        oEdtk::Field->new( 'E1-PEPACODPAY-3',        3 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODPAY-3',    15 ),
        oEdtk::Field->new( 'E1-ZZZZ-TRNFAC-3',       4 ),

        # E1-ZZEDIT-COMM-1
        oEdtk::Field->new( 'E1-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM-2',     50 ),

        # E1-ZZEDIT-COMM-2
        oEdtk::Field->new( 'E1-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM-2',     50 ),

        # E1-ZZEDIT-COMM-3
        oEdtk::Field->new( 'E1-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM-2',     50 ),

        # E1-ZZEDIT-ODPU
        oEdtk::Field->new( 'E1-ZZ-EDIT-TYPBLC-3', 2 ),
        oEdtk::Field->new( 'E1-ODPUCODPUC',       9 ),

        # E1-ZZEDIT-NOM-P-PH
        oEdtk::Field->new( 'E1-PENOCODCIV-2',     10 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODCIV-2', 15 ),
        oEdtk::Field->new( 'E1-PENOCODTIT-4',     10 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODTIT-4', 15 ),
        oEdtk::Field->new( 'E1-PENOLIBLON-4',     50 ),
        oEdtk::Field->new( 'E1-PENOPRN01-2',      20 ),

        # E1-ZZEDIT-ADRESSE
        oEdtk::Field->new( 'E1-ZZZZ-PNTREM-4',       32 ),
        oEdtk::Field->new( 'E1-ZZZZ-CPLADR-4',       32 ),
        oEdtk::Field->new( 'E1-PEADNUMVOI-4',        9 ),
        oEdtk::Field->new( 'E1-PEADBTQVOI-4',        2 ),
        oEdtk::Field->new( 'E1-PEZZ-LICBTQVOI-4',    15 ),
        oEdtk::Field->new( 'E1-PEVONATVOI-4',        5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATVOI-4',    15 ),
        oEdtk::Field->new( 'E1-ZZZZ-LIBVOI-4',       32 ),
        oEdtk::Field->new( 'E1-ZZZZ-INDCDX-4',       1 ),
        oEdtk::Field->new( 'E1-ZZZZ-BOIPOSLIBLDT-4', 32 ),
        oEdtk::Field->new( 'E1-ZZZZ-LIBLOCLIBLOC-4', 32 ),
        oEdtk::Field->new( 'E1-ZZZZ-CODCDXCODPOS-4', 10 ),
        oEdtk::Field->new( 'E1-ZZZZ-NOMCDXLIBACH-4', 32 ),
        oEdtk::Field->new( 'E1-PEPACODPAY-4',        3 ),
        oEdtk::Field->new( 'E1-PEZZ-LICCODPAY-4',    15 ),
        oEdtk::Field->new( 'E1-ZZZZ-TRNFAC-4',       4 ),

        # E1-ZZEDIT-COMM-1
        oEdtk::Field->new( 'E1-PECONATCOM-3',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM-3', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM-3',     50 ),

        # E1-ZZEDIT-COMM-2
        oEdtk::Field->new( 'E1-PECONATCOM-3',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM-3', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM-3',     50 ),

        # E1-ZZEDIT-COMM-3
        oEdtk::Field->new( 'E1-PECONATCOM-3',     5 ),
        oEdtk::Field->new( 'E1-PEZZ-LICNATCOM-3', 15 ),
        oEdtk::Field->new( 'E1-PECOVALCOM-3',     50 ),

        # E1-NUMERO-PERSONNES
        oEdtk::Field->new( 'E1-PENONUMPER-DD', 8 ),
        oEdtk::Field->new( 'E1-PENONUMPER-ER', 8 ),
        oEdtk::Field->new( 'E1-PENONUMPER-CG', 8 ),
        oEdtk::Field->new( 'E1-PENONUMPER-PU', 8 ),
        oEdtk::Field->new( 'E1-ZZZZ-ZONEDH',   400 ),
        oEdtk::Field->new( 'E1-ZZZZ-CODCOUR',  7 )
    );
}

1;
