package oEUser::Descriptor::R635;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R635-LGNDAT
        oEdtk::Field->new( 'R635-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R635-PECPDDE',       8 ),
        oEdtk::Field->new( 'R635-PECPDFE',       8 ),
        oEdtk::Field->new( 'R635-PECPCODSEX',    1 ),
        oEdtk::Field->new( 'R635-PECPDATNAI',    8 ),
        oEdtk::Field->new( 'R635-PECPLOCNAI',    50 ),
        oEdtk::Field->new( 'R635-PECPDEPNAI',    3 ),
        oEdtk::Field->new( 'R635-LL-PECPDEPNAI', 50 ),
        oEdtk::Field->new( 'R635-LC-PECPDEPNAI', 15 ),
        oEdtk::Field->new( 'R635-PECPPAYNAI',    3 ),
        oEdtk::Field->new( 'R635-LL-PECPPAYNAI', 50 ),
        oEdtk::Field->new( 'R635-LC-PECPPAYNAI', 15 ),
        oEdtk::Field->new( 'R635-PECPSTUFAM',    2 ),
        oEdtk::Field->new( 'R635-LL-PECPSTUFAM', 50 ),
        oEdtk::Field->new( 'R635-LC-PECPSTUFAM', 15 ),
        oEdtk::Field->new( 'R635-PECPSEEPER',    13 ),
        oEdtk::Field->new( 'R635-PECPCLESEE',    2 ),
        oEdtk::Field->new( 'R635-PECPGESDEC',    1 ),
        oEdtk::Field->new( 'R635-PECPDATDEC',    8 ),
        oEdtk::Field->new( 'R635-PECPVAL01',     6 ),
        oEdtk::Field->new( 'R635-LL-PECPVAL01',  50 ),
        oEdtk::Field->new( 'R635-LC-PECPVAL01',  15 ),
        oEdtk::Field->new( 'R635-PECPVAL02',     6 ),
        oEdtk::Field->new( 'R635-LL-PECPVAL02',  50 ),
        oEdtk::Field->new( 'R635-LC-PECPVAL02',  15 ),
        oEdtk::Field->new( 'R635-PECPVAL03',     6 ),
        oEdtk::Field->new( 'R635-LL-PECPVAL03',  50 ),
        oEdtk::Field->new( 'R635-LC-PECPVAL03',  15 ),
        oEdtk::Field->new( 'R635-PECPVAL04',     6 ),
        oEdtk::Field->new( 'R635-LL-PECPVAL04',  50 ),
        oEdtk::Field->new( 'R635-LC-PECPVAL04',  15 ),
        oEdtk::Field->new( 'R635-PECPVAL05',     6 ),
        oEdtk::Field->new( 'R635-LL-PECPVAL05',  50 ),
        oEdtk::Field->new( 'R635-LC-PECPVAL05',  15 ),
        oEdtk::Field->new( 'R635-PECPCODSIF',    2 ),
        oEdtk::Field->new( 'R635-LL-PECPCODSIF', 50 ),
        oEdtk::Field->new( 'R635-LC-PECPCODSIF', 15 ),
        oEdtk::Field->new( 'R635-PECPTYPHAB',    2 ),
        oEdtk::Field->new( 'R635-LL-PECPTYPHAB', 50 ),
        oEdtk::Field->new( 'R635-LC-PECPTYPHAB', 15 ),
        oEdtk::Field->new( 'R635-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R635-ACOPNUMOPE',    5 ),
    );
}

1;
