package oEUser::Descriptor::Q2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # Q2-ENREG
        oEdtk::Field->new( 'Q2-NUM-SEQ-RELEVE',  10 ),
        oEdtk::Field->new( 'Q2-DATE-TRAITEMENT', 8 ),
        oEdtk::Field->new( 'Q2-CRITERE-01',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-02',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-04',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-05',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-06',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-07',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-08',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-09',      50 ),
        oEdtk::Field->new( 'Q2-CRITERE-10',      50 ),
        oEdtk::Field->new( 'Q1-CRITERE-11',      50 ),
        oEdtk::Field->new( 'Q2-CODE-PAYS',       2 ),
        oEdtk::Field->new( 'Q2-CLE-IBAN',        4 ),
        oEdtk::Field->new( 'Q2-ID-NATIONAL',     30 ),
        oEdtk::Field->new( 'Q2-CODE-BIC',        14 ),
    );
}

1;
