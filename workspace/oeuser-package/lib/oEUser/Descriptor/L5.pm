package oEUser::Descriptor::L5;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # L5-LGNDAT
        # L5-IDENT-CI
        oEdtk::Field->new( 'L5-NUMPART',    5 ),
        oEdtk::Field->new( 'L5-ACH1NUMCIN', 15 ),

        # L5-INFO-FRAIS
        oEdtk::SignedField->new( 'L5-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'L5-MONTANT-TTC', 12, 2 ),
        oEdtk::Field->new( 'L5-ECHIPU',       8 ),
        oEdtk::Field->new( 'L5-CODFRS',       2 ),
        oEdtk::Field->new( 'L5-LIBCOU-FRAIS', 15 ),
        oEdtk::Field->new( 'L5-LIBLON-FRAIS', 50 ),
        oEdtk::Field->new( 'L5-ACFSDDEPRD',   8 ),
        oEdtk::Field->new( 'L5-ACFSDFEPRD',   8 ),

        # L5-INFO-OPE
        oEdtk::Field->new( 'L5-ACOPNUMOPE', 5 ),
        oEdtk::Field->new( 'L5-ACOPDDEOPE', 8 ),
        oEdtk::Field->new( 'L5-ACOPDDECOT', 8 ),
        oEdtk::Field->new( 'L5-ACOPNUMAVT', 3 ),
        oEdtk::Field->new( 'L5-ACOPCODAGC', 4 ),
        oEdtk::Field->new( 'L5-ACOPLICAGC', 15 ),
        oEdtk::Field->new( 'L5-ACOPLIBAGC', 50 ),
        oEdtk::Field->new( 'L5-ACOPCODCTX', 4 ),
        oEdtk::Field->new( 'L5-ACOPLICCTX', 15 ),
        oEdtk::Field->new( 'L5-ACOPLIBCTX', 50 ),
        oEdtk::Field->new( 'L5-ACOPCODTRT', 5 ),

        # L5-DATE-EXIGIBILITE
        oEdtk::Field->new( 'L5-DATEXI', 8 ),
    );
}

1;
