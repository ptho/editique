package oEUser::Descriptor::GA;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # GA-LGNDAT
        # CO62-GA-GROUPE-ASS
        oEdtk::Field->new( 'CO62-GA-CODGRA', 6 ),

        # CO62-GA-NOM-GA
        oEdtk::Field->new( 'CO62-GA-PENOCODTIT-GA', 10 ),
        oEdtk::Field->new( 'CO62-GA-PENOLICTIT-GA', 15 ),
        oEdtk::Field->new( 'CO62-GA-PENOLIBLON-GA', 50 ),
        oEdtk::Field->new( 'CO62-GA-CODSIR-GA',     14 ),
        oEdtk::Field->new( 'CO62-GA-ACH7DDEADH',    8 ),
        oEdtk::Field->new( 'CO62-GA-ACH7DDERAD',    8 ),
        oEdtk::Field->new( 'CO62-GA-ACH7LIBATT',    30 ),
        oEdtk::Field->new( 'CO62-GA-ACH7INDABR',    1 ),
        oEdtk::Field->new( 'CO62-GA-ACH7ACGAR3',    6 ),

        # CO62-GA-ADRESSE-GA
        oEdtk::Field->new( 'CO62-GA-PNTREM-GA',       32 ),
        oEdtk::Field->new( 'CO62-GA-CPLADR-GA',       32 ),
        oEdtk::Field->new( 'CO62-GA-PEADNUMVOI-GA',   9 ),
        oEdtk::Field->new( 'CO62-GA-PEADBTQVOI-GA',   2 ),
        oEdtk::Field->new( 'CO62-GA-PEADLICBTQ-GA',   15 ),
        oEdtk::Field->new( 'CO62-GA-PEVONATVOI-GA',   5 ),
        oEdtk::Field->new( 'CO62-GA-PEADLICNTV-GA',   15 ),
        oEdtk::Field->new( 'CO62-GA-LIBVOI-GA',       32 ),
        oEdtk::Field->new( 'CO62-GA-INDCDX-GA',       1 ),
        oEdtk::Field->new( 'CO62-GA-BOIPOSLIBLDT-GA', 32 ),
        oEdtk::Field->new( 'CO62-GA-LIBLOCLIBLOC-GA', 32 ),
        oEdtk::Field->new( 'CO62-GA-CODCDXCODPOS-GA', 10 ),
        oEdtk::Field->new( 'CO62-GA-NOMCDXLIBACH-GA', 32 ),
        oEdtk::Field->new( 'CO62-GA-PEPACODPAY-GA',   3 ),
        oEdtk::Field->new( 'CO62-GA-PEPALICPAY-GA',   15 ),
        oEdtk::Field->new( 'CO62-GA-TRNFAC-GA',       4 ),

        # CO62-GA-IDENT-PART
        oEdtk::Field->new( 'CO62-GA-NUMPART',      5 ),
        oEdtk::Field->new( 'CO62-GA-CODPAR',       3 ),
        oEdtk::Field->new( 'CO62-GA-LIBCOUCODPAR', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBLONCODPAR', 50 ),
        oEdtk::Field->new( 'CO62-GA-ACPAACGAR2',   6 ),
        oEdtk::Field->new( 'CO62-GA-ACPTMODGPA',   1 ),
        oEdtk::Field->new( 'CO62-GA-LIBCOUMODGPA', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBLONMODGPA', 50 ),
        oEdtk::SignedField->new( 'CO62-GA-ACPTRSTDUANT', 12, 2 ),
        oEdtk::SignedField->new( 'CO62-GA-ACPTTRPPERCU', 12, 2 ),

        # CO62-GA-ZZEDIT-PAYEUR
        oEdtk::Field->new( 'CO62-GA-ACPACODQUA', 1 ),
        oEdtk::Field->new( 'CO62-GA-LICCODQUA',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODQUA',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACPACODDVR', 3 ),
        oEdtk::Field->new( 'CO62-GA-LICCODDVR',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODDVR',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACPAMODRGC', 2 ),
        oEdtk::Field->new( 'CO62-GA-LICMODRGC',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBMODRGC',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACFQFRQRGC', 2 ),
        oEdtk::Field->new( 'CO62-GA-ACFQLIBCOU', 15 ),
        oEdtk::Field->new( 'CO62-GA-ACFQLIBLON', 50 ),
        oEdtk::Field->new( 'CO62-GA-ACPAFRQAVI', 2 ),
        oEdtk::Field->new( 'CO62-GA-LICFRQAVI',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBFRQAVI',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACPATYPTRM', 2 ),
        oEdtk::Field->new( 'CO62-GA-LICTYPTRM',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBTYPTRM',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACPAECHPBA', 2 ),
        oEdtk::Field->new( 'CO62-GA-LICECHPBA',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBECHPBA',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACPADDEPBA', 8 ),
        oEdtk::Field->new( 'CO62-GA-ACPADFEPBA', 8 ),
        oEdtk::Field->new( 'CO62-GA-ACPAMTFPBA', 2 ),
        oEdtk::Field->new( 'CO62-GA-LICMTFPBA',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBMTFPBA',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACPALIBATT', 50 ),
        oEdtk::Field->new( 'CO62-GA-TYPPAYE',    1 ),
        oEdtk::Field->new( 'CO62-GA-LICTYPPAY',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBTYPPAY',  50 ),
        oEdtk::Field->new( 'CO62-GA-ACPAINDDAV', 1 ),

        # CO62-GA-NOM-PAYEUR
        oEdtk::Field->new( 'CO62-GA-PENOCODTIT-PA', 10 ),
        oEdtk::Field->new( 'CO62-GA-PENOLICTIT-PA', 15 ),
        oEdtk::Field->new( 'CO62-GA-PENOLIBLON-PA', 50 ),

        # CO62-GA-ADRESSE-PA
        oEdtk::Field->new( 'CO62-GA-PNTREM-PA',       32 ),
        oEdtk::Field->new( 'CO62-GA-CPLADR-PA',       32 ),
        oEdtk::Field->new( 'CO62-GA-PEADNUMVOI-PA',   9 ),
        oEdtk::Field->new( 'CO62-GA-PEADBTQVOI-PA',   2 ),
        oEdtk::Field->new( 'CO62-GA-PEADLICBTQ-PA',   15 ),
        oEdtk::Field->new( 'CO62-GA-PEVONATVOI-PA',   5 ),
        oEdtk::Field->new( 'CO62-GA-PEADLICNTV-PA',   15 ),
        oEdtk::Field->new( 'CO62-GA-LIBVOI-PA',       32 ),
        oEdtk::Field->new( 'CO62-GA-INDCDX-PA',       1 ),
        oEdtk::Field->new( 'CO62-GA-BOIPOSLIBLDT-PA', 32 ),
        oEdtk::Field->new( 'CO62-GA-LIBLOCLIBLOC-PA', 32 ),
        oEdtk::Field->new( 'CO62-GA-CODCDXCODPOS-PA', 10 ),
        oEdtk::Field->new( 'CO62-GA-NOMCDXLIBACH-PA', 32 ),
        oEdtk::Field->new( 'CO62-GA-PEPACODPAY-PA',   3 ),
        oEdtk::Field->new( 'CO62-GA-PEPALICPAY-PA',   15 ),
        oEdtk::Field->new( 'CO62-GA-TRNFAC-PA',       4 ),

        # CO62-GA-RIB-PY
        oEdtk::Field->new( 'CO62-GA-PERICPTTIT', 50 ),
        oEdtk::Field->new( 'CO62-GA-PERICODBQE', 10 ),
        oEdtk::Field->new( 'CO62-GA-PERICODGUI', 10 ),
        oEdtk::Field->new( 'CO62-GA-PERINUMCPT', 20 ),
        oEdtk::Field->new( 'CO62-GA-PERICLECPT', 2 ),
        oEdtk::Field->new( 'CO62-GA-PERICLEIBA', 4 ),
        oEdtk::Field->new( 'CO62-GA-PEPACODPAY', 3 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPAY',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPAY',  50 ),
        oEdtk::Field->new( 'CO62-GA-PERICODDEV', 3 ),
        oEdtk::Field->new( 'CO62-GA-LICCODDEV',  15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODDEV',  50 ),

        # CO62-GA-CONVENTIONS
        # CO62-GA-LISTE-PRODUIT
        oEdtk::Field->new( 'CO62-GA-CODPRT1',   10 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPRT', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPRT', 50 ),

        # CO62-GA-LISTE-PRODUIT-2
        oEdtk::Field->new( 'CO62-GA-CODPRT1-2',   10 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPRT-2', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPRT-2', 50 ),

        # CO62-GA-LISTE-PRODUIT-3
        oEdtk::Field->new( 'CO62-GA-CODPRT1-3',   10 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPRT-3', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPRT-3', 50 ),

        # CO62-GA-LISTE-PRODUIT-4
        oEdtk::Field->new( 'CO62-GA-CODPRT1-4',   10 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPRT-4', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPRT-4', 50 ),

        # CO62-GA-LISTE-PRODUIT-5
        oEdtk::Field->new( 'CO62-GA-CODPRT1-5',   10 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPRT-5', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPRT-5', 50 ),

        # CO62-GA-LISTE-PRODUIT-6
        oEdtk::Field->new( 'CO62-GA-CODPRT1-6',   10 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPRT-6', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPRT-6', 50 ),

        # CO62-GA-LISTE-PRODUIT-7
        oEdtk::Field->new( 'CO62-GA-CODPRT1-7',   10 ),
        oEdtk::Field->new( 'CO62-GA-LICCODPRT-7', 15 ),
        oEdtk::Field->new( 'CO62-GA-LIBCODPRT-7', 50 ),
    );
}

1;
