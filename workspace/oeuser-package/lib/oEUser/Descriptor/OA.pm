package oEUser::Descriptor::OA;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::Field;

# ENREGISTREMENT CRÉÉ PAR LA MNT POUR TRANSMETTRE LES APPELS D'ENCART PRÉVOYANCE

sub get {
    return oEdtk::Record->new(
        oEdtk::Field->new( 'OA-CODE-FLAG',     1 ),
        oEdtk::Field->new( 'OA-CODE-GARANTIE', 10 ),
        oEdtk::Field->new( 'OA-CODE-OFFRE',    10 ),
        oEdtk::Field->new( 'OA-CODE-PRODUIT',  10 ),
        oEdtk::Field->new( 'OA-REFERENCE-NI',  '*' )
    );
}

1;
