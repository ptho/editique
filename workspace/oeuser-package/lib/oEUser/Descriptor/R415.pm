package oEUser::Descriptor::R415;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R415-LGNDAT
        oEdtk::Field->new( 'R415-ACMRACASR2', 2 ),
        oEdtk::Field->new( 'R415-ACMRDDEPRD', 8 ),
        oEdtk::Field->new( 'R415-ACMRDFEPRD', 8 ),
        oEdtk::Field->new( 'R415-ENFNCODFRN', 9 ),
        oEdtk::Field->new( 'R415-PENOLIBLON', 50 ),
        oEdtk::Field->new( 'R415-ACMRRGARAT', 2 ),
    );
}

1;
