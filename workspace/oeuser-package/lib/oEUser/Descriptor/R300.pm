package oEUser::Descriptor::R300;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R300-LGNDAT
        oEdtk::Field->new( 'R300-ACH1CODETA',    2 ),
        oEdtk::Field->new( 'R300-ACH1TYPCCI',    1 ),
        oEdtk::Field->new( 'R300-ACH1DDEPRD',    8 ),
        oEdtk::Field->new( 'R300-ACH1DFEPRD',    8 ),
        oEdtk::Field->new( 'R300-ACH1DATANT',    8 ),
        oEdtk::Field->new( 'R300-ACH1DDE1SO',    8 ),
        oEdtk::Field->new( 'R300-ACH1DDEZRE',    8 ),
        oEdtk::Field->new( 'R300-ACH1CODDVG',    3 ),
        oEdtk::Field->new( 'R300-LL-ACH1CODDVG', 50 ),
        oEdtk::Field->new( 'R300-LC-ACH1CODDVG', 15 ),
        oEdtk::Field->new( 'R300-ODPUCODPUC',    9 ),
        oEdtk::Field->new( 'R300-ACH1INDCMU',    1 ),
        oEdtk::Field->new( 'R300-ACH1DURCIN',    4 ),
        oEdtk::Field->new( 'R300-ACH1INDLOM',    1 ),
        oEdtk::Field->new( 'R300-ACH1DEBEXE',    4 ),
        oEdtk::Field->new( 'R300-ACH1ECHPAL',    4 ),
        oEdtk::Field->new( 'R300-ACH1INDTMP',    1 ),
        oEdtk::Field->new( 'R300-ACH1MTFSVC',    5 ),
        oEdtk::Field->new( 'R300-LL-ACH1MTFSVC', 50 ),
        oEdtk::Field->new( 'R300-LC-ACH1MTFSVC', 15 ),
        oEdtk::Field->new( 'R300-ACH1DDESVC',    8 ),
        oEdtk::Field->new( 'R300-ACH1ORICTT',    2 ),
        oEdtk::Field->new( 'R300-LL-ACH1ORICTT', 50 ),
        oEdtk::Field->new( 'R300-LC-ACH1ORICTT', 15 ),
        oEdtk::Field->new( 'R300-ACH1INDABS',    1 ),
        oEdtk::Field->new( 'R300-ACH1INDASC',    1 ),
        oEdtk::Field->new( 'R300-CODE-ASSO',     6 ),
        oEdtk::Field->new( 'R300-LL-CODE-ASSO',  50 ),
        oEdtk::Field->new( 'R300-LC-CODE-ASSO',  15 ),
        oEdtk::Field->new( 'R300-ACH1INDQCO',    1 ),
        oEdtk::Field->new( 'R300-ACH1TYPQCO',    1 ),
        oEdtk::Field->new( 'R300-LL-ACH1TYPQCO', 50 ),
        oEdtk::Field->new( 'R300-LC-ACH1TYPQCO', 15 ),
        oEdtk::Field->new( 'R300-ACH1TYPCTP',    4 ),
        oEdtk::Field->new( 'R300-LL-ACH1TYPCTP', 50 ),
        oEdtk::Field->new( 'R300-LC-ACH1TYPCTP', 15 ),
        oEdtk::Field->new( 'R300-ACH1INDRSM',    1 ),
        oEdtk::Field->new( 'R300-ACH1INDRSA',    1 ),
        oEdtk::Field->new( 'R300-ACH1NUECIN',    15 ),
        oEdtk::Field->new( 'R300-ACH1INDNTI',    1 ),
        oEdtk::Field->new( 'R300-ACH1INDNCO',    1 ),
        oEdtk::Field->new( 'R300-ACH7ACGAR2',    6 ),
        oEdtk::Field->new( 'R300-ACH7DDEPRD',    8 ),
        oEdtk::Field->new( 'R300-ACH7DFEPRD',    8 ),
    );
}

1;
