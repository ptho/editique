package oEUser::Descriptor::G2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # G2-LGNDAT
        oEdtk::Field->new( 'G2-ACH5CODPAR',        3 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACH5CODPAR', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACH5CODPAR', 50 ),
        oEdtk::Field->new( 'G2-ACPTACGAR2',        6 ),
        oEdtk::Field->new( 'G2-ACH5MODGPA',        1 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACH5MODGPA', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACH5MODGPA', 50 ),
        oEdtk::Field->new( 'G2-ACPAQUAPAY',        1 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPAQUAPAY', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPAQUAPAY', 50 ),
        oEdtk::Field->new( 'G2-ACPACODDVR',        3 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPACODDVR', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPACODDVR', 50 ),
        oEdtk::Field->new( 'G2-ACPAMODRGC',        2 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPAMODRGC', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPAMODRGC', 50 ),
        oEdtk::Field->new( 'G2-ACPAFRQAVI',        2 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPAFRQAVI', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPAFRQAVI', 50 ),
        oEdtk::Field->new( 'G2-ACPATYPTRM',        2 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPATYPTRM', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPATYPTRM', 50 ),
        oEdtk::Field->new( 'G2-ACPAECHPBA',        2 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPAECHPBA', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPAECHPBA', 50 ),
        oEdtk::Field->new( 'G2-ACPADDEPBA',        8 ),
        oEdtk::Field->new( 'G2-ACPADFEPBA',        8 ),
        oEdtk::Field->new( 'G2-ACPAMTFPBA',        2 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPAMTFPBA', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPAMTFPBA', 50 ),
        oEdtk::Field->new( 'G2-ACPALIBATT',        30 ),
        oEdtk::Field->new( 'G2-ACPATYPPAY',        1 ),
        oEdtk::Field->new( 'G2-LIBCOU-ACPATYPPAY', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-ACPATYPPAY', 50 ),
        oEdtk::Field->new( 'G2-ACPAINDDAV',        1 ),
        oEdtk::Field->new( 'G2-PERICPTTIT',        50 ),
        oEdtk::Field->new( 'G2-PERICODBQE',        10 ),
        oEdtk::Field->new( 'G2-PERICODGUI',        10 ),
        oEdtk::Field->new( 'G2-PERINUMCPT',        20 ),
        oEdtk::Field->new( 'G2-PERICLECPT',        2 ),
        oEdtk::Field->new( 'G2-PERICLEIBA',        4 ),
        oEdtk::Field->new( 'G2-PEPACODPAY',        3 ),
        oEdtk::Field->new( 'G2-LIBCOU-PEPACODPAY', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-PEPACODPAY', 50 ),
        oEdtk::Field->new( 'G2-PERICODDEV',        3 ),
        oEdtk::Field->new( 'G2-LIBCOU-PERICODDEV', 15 ),
        oEdtk::Field->new( 'G2-LIBLON-PERICODDEV', 50 ),
        oEdtk::Field->new( 'G2-ACPADLPNBM',        2 ),
        oEdtk::Field->new( 'G2-ACPADLPQUAN',       2 ),
        oEdtk::Field->new( 'G2-RESERVE-G2',        196 ),
    );
}

1;
