package oEUser::Descriptor::R605;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R605-LGNDAT
        oEdtk::Field->new( 'R605-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R605-PENPTYPLIE',    1 ),
        oEdtk::Field->new( 'R605-PENRROLNOM',    5 ),
        oEdtk::Field->new( 'R605-LL-PENRROLNOM', 50 ),
        oEdtk::Field->new( 'R605-LC-PENRROLNOM', 15 ),
        oEdtk::Field->new( 'R605-PEZZDDE',       8 ),
        oEdtk::Field->new( 'R605-PEZZDFE',       8 ),
        oEdtk::Field->new( 'R605-PENRDDE',       8 ),
        oEdtk::Field->new( 'R605-PENRDFE',       8 ),
        oEdtk::Field->new( 'R605-PENOLIBLON',    50 ),
        oEdtk::Field->new( 'R605-PENOMAJLIB',    50 ),
        oEdtk::Field->new( 'R605-NOMJEUNEFILLE', 50 ),
        oEdtk::Field->new( 'R605-PENOPRN01',     20 ),
        oEdtk::Field->new( 'R605-PENOMAJPRN',    20 ),
        oEdtk::Field->new( 'R605-PENOPRN02',     20 ),
        oEdtk::Field->new( 'R605-PENOPRN03',     20 ),
        oEdtk::Field->new( 'R605-PENOPRN04',     20 ),
        oEdtk::Field->new( 'R605-PENOTYPNOM',    5 ),
        oEdtk::Field->new( 'R605-LL-PENOTYPNOM', 50 ),
        oEdtk::Field->new( 'R605-LC-PENOTYPNOM', 15 ),
        oEdtk::Field->new( 'R605-PENOCODCIV',    10 ),
        oEdtk::Field->new( 'R605-LL-PENOCODCIV', 50 ),
        oEdtk::Field->new( 'R605-LC-PENOCODCIV', 15 ),
        oEdtk::Field->new( 'R605-PENOCODTIT',    10 ),
        oEdtk::Field->new( 'R605-LL-PENOCODTIT', 50 ),
        oEdtk::Field->new( 'R605-LC-PENOCODTIT', 15 ),
        oEdtk::Field->new( 'R605-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R605-ACOPNUMOPE',    5 ),
    );
}

1;
