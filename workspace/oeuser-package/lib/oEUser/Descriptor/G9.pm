package oEUser::Descriptor::G9;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # G9-LGNDAT
        # CO62-G9-INFO-BORDEREAU
        oEdtk::Field->new( 'CO62-G9-ACBONUMBOR',  6 ),
        oEdtk::Field->new( 'CO62-G9-DEBPER',      8 ),
        oEdtk::Field->new( 'CO62-G9-FINPER',      8 ),
        oEdtk::Field->new( 'CO62-G9-NUMOPE-BOR',  5 ),
        oEdtk::Field->new( 'CO62-G9-ECHBOR',      6 ),
        oEdtk::Field->new( 'CO62-G9-COD-DADS',    8 ),
        oEdtk::Field->new( 'CO62-G9-TYPE-BOR',    1 ),
        oEdtk::Field->new( 'CO62-G9-LIBCTYPBOR',  15 ),
        oEdtk::Field->new( 'CO62-G9-LIBLTYPBOR',  50 ),
        oEdtk::Field->new( 'CO62-G9-TYPE-ED-BOR', 2 ),
        oEdtk::Field->new( 'CO62-G9-LIBCTYPEDB',  15 ),
        oEdtk::Field->new( 'CO62-G9-LIBLTYPEDB',  50 ),
        oEdtk::Field->new( 'CO62-G9-ETAT-BOR',    2 ),
        oEdtk::Field->new( 'CO62-G9-LIBCETATBOR', 15 ),
        oEdtk::Field->new( 'CO62-G9-LIBLETATBOR', 50 ),
        oEdtk::Field->new( 'CO62-G9-TYPERGU',     4 ),
        oEdtk::Field->new( 'CO62-G9-LIBCTYPRGU',  15 ),
        oEdtk::Field->new( 'CO62-G9-LIBLTYPRGU',  50 ),
        oEdtk::SignedField->new( 'CO62-G9-MTTRPPERCU',      12, 2 ),
        oEdtk::SignedField->new( 'CO62-G9-MTRSTDU',         12, 2 ),
        oEdtk::SignedField->new( 'CO62-G9-MTTOTPRECAL-BOR', 12, 2 ),
        oEdtk::SignedField->new( 'CO62-G9-MTTOTPRDBOR',     12, 2 ),
        oEdtk::SignedField->new( 'CO62-G9-MTTOTBOR',        12, 2 ),
        oEdtk::SignedField->new( 'CO62-G9-MTNETAPAYER',     12, 2 ),
    );
}

1;
