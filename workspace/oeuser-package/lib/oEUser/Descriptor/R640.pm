package oEUser::Descriptor::R640;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R640-LGNDAT
        oEdtk::Field->new( 'R640-PEPENUMPER', 8 ),
        oEdtk::Field->new( 'R640-PEPMNBRSAL', 5 ),
        oEdtk::SignedField->new( 'R640-PEPMCAFMNT', 12, 2 ),
        oEdtk::Field->new( 'R640-PEPMTRCEFF', 2 ),
        oEdtk::Field->new( 'R640-PEPMTRCCAF', 2 ),
        oEdtk::Field->new( 'R640-PEPMNBRETB', 2 ),
        oEdtk::SignedField->new( 'R640-PEPMMASSL1', 12, 2 ),
        oEdtk::SignedField->new( 'R640-PEPMMASSL2', 12, 2 ),
        oEdtk::SignedField->new( 'R640-PEPMMASSL3', 12, 2 ),
        oEdtk::Field->new( 'R640-PEPMDDE',    8 ),
        oEdtk::Field->new( 'R640-ACOPNUMCIN', 15 ),
        oEdtk::Field->new( 'R640-ACOPNUMOPE', 5 ),
    );
}

1;
