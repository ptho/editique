package oEUser::Descriptor::H0;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CC71-H0-LGNDAT
        # CC71-H0-INFO-AS
        oEdtk::Field->new( 'CC71-H0-ACASRNGASU', 2 ),

        # CC71-H0-INFO-GS
        oEdtk::Field->new( 'CC71-H0-CODE-GAR-TECH',       10 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOU-GAR-TECH',     15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-GAR-TECH',     50 ),
        oEdtk::Field->new( 'CC71-H0-TYP-GAR-TECH',        1 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOU-TYP-GAR-TECH', 15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-TYP-GAR-TECH', 50 ),

        # CC71-H0-INFO-PS
        oEdtk::Field->new( 'CC71-H0-CODE-PDT',   10 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOU-PDT', 15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-PDT', 50 ),

        # CC71-H0-INFO-PERIODE
        oEdtk::Field->new( 'CC71-H0-DDE-ADMIN',  8 ),
        oEdtk::Field->new( 'CC71-H0-DFE-ADMIN',  8 ),
        oEdtk::Field->new( 'CC71-H0-ACCODDEPRD', 8 ),
        oEdtk::Field->new( 'CC71-H0-ACCODFEPRD', 8 ),
        oEdtk::Field->new( 'CC71-H0-ACH4DATVTE', 8 ),

        # CC71-H0-INFO-ELT-PRIME
        oEdtk::Field->new( 'CC71-H0-TYP-ELT-PRIME',    1 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOU-ELT-PRIME', 15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-ELT-PRIME', 50 ),

        # CC71-H0-INFO-ASSURE-DIVERS
        oEdtk::Field->new( 'CC71-H0-ACH4DURGAR',         4 ),
        oEdtk::Field->new( 'CC71-H0-ACH4DFEEFC',         8 ),
        oEdtk::Field->new( 'CC71-H0-ACH4DATNAI',         8 ),
        oEdtk::Field->new( 'CC71-H0-ACH4CODSEX',         1 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOU-SEX',         15 ),
        oEdtk::Field->new( 'CC71-H0-ACH4CODCSP',         4 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOUCSP-ASSUR',    15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLONCSP-ASSUR',    50 ),
        oEdtk::Field->new( 'CC71-H0-ACH4CODCSO',         12 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOUCATSOC-ASSUR', 15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLONCATSOC-ASSUR', 50 ),
        oEdtk::Field->new( 'CC71-H0-ACH4CODCLR',         2 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOUCLR',          15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLONCLR',          50 ),
        oEdtk::Field->new( 'CC71-H0-ACH4CODFRI',         6 ),
        oEdtk::Field->new( 'CC71-H0-LIBCOUFRI',          15 ),
        oEdtk::Field->new( 'CC71-H0-LIBLONFRI',          50 ),
        oEdtk::Field->new( 'CC71-H0-PRTPNATPRE',         1 ),

        # CC71-H0-INFO-MONTANT
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-HT',      12, 2 ),
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-HT-EXE',  12, 2 ),
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-TTC-EXE', 12, 2 ),
        oEdtk::Field->new( 'CC71-H0-NUM-PRD', 5 ),
        oEdtk::Field->new( 'CC71-H0-NUM-GAR', 5 ),

        # CC71-H0-INFO-TAXES
        oEdtk::Field->new( 'CC71-H0-CODTAX1', 6 ),
        oEdtk::SignedField->new( 'CC71-H0-TAUX-TAX1',    4,  5 ),
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-TAX1', 12, 2 ),
        oEdtk::Field->new( 'CC71-H0-IDFSYS-TAX1', 8 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-TAX1', 50 ),
        oEdtk::Field->new( 'CC71-H0-CODTAX2',     6 ),
        oEdtk::SignedField->new( 'CC71-H0-TAUX-TAX2',    4,  5 ),
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-TAX2', 12, 2 ),
        oEdtk::Field->new( 'CC71-H0-IDFSYS-TAX2', 8 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-TAX2', 50 ),
        oEdtk::Field->new( 'CC71-H0-CODTAX3',     6 ),
        oEdtk::SignedField->new( 'CC71-H0-TAUX-TAX3',    4,  5 ),
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-TAX3', 12, 2 ),
        oEdtk::Field->new( 'CC71-H0-IDFSYS-TAX3', 8 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-TAX3', 50 ),
        oEdtk::Field->new( 'CC71-H0-CODTAX4',     6 ),
        oEdtk::SignedField->new( 'CC71-H0-TAUX-TAX4',    4,  5 ),
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-TAX4', 12, 2 ),
        oEdtk::Field->new( 'CC71-H0-IDFSYS-TAX4', 8 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-TAX4', 50 ),
        oEdtk::Field->new( 'CC71-H0-CODTAX5',     6 ),
        oEdtk::SignedField->new( 'CC71-H0-TAUX-TAX5',    4,  5 ),
        oEdtk::SignedField->new( 'CC71-H0-MONTANT-TAX5', 12, 2 ),
        oEdtk::Field->new( 'CC71-H0-IDFSYS-TAX5', 8 ),
        oEdtk::Field->new( 'CC71-H0-LIBLON-TAX5', 50 ),

        # CC71-H0-SPECIF-COMMERCIALES
        # CC71-H0-MODULATION-COM
        oEdtk::Field->new( 'CC71-H0-CODE-OPE-COM', 6 ),
        oEdtk::Field->new( 'CC71-H0-LIB-MOD-COM',  50 ),
        oEdtk::Field->new( 'CC71-H0-TYPE-VALEUR',  1 ),
        oEdtk::Field->new( 'CC71-H0-UNITE-VALEUR', 5 ),

        # CC71-H0-VALEUR1
        oEdtk::SignedField->new( 'CC71-H0-ACVMCA1-MNT',  12, 2 ),
        oEdtk::SignedField->new( 'CC71-H0-ACVMCA1-TAUX', 4,  5 ),
        oEdtk::Field->new( 'CC71-H0-ACVMCA1-MOIS', 4 ),

        # CC71-H0-VALEUR2
        oEdtk::SignedField->new( 'CC71-H0-ACVMCA2-MNT',  12, 2 ),
        oEdtk::SignedField->new( 'CC71-H0-ACVMCA2-TAUX', 4,  5 ),
        oEdtk::Field->new( 'CC71-H0-ACVMCA2-MOIS', 4 ),

        # CC71-H0-VALEUR3
        oEdtk::SignedField->new( 'CC71-H0-ACVMCA3-MNT',  12, 2 ),
        oEdtk::SignedField->new( 'CC71-H0-ACVMCA3-TAUX', 4,  5 ),
        oEdtk::Field->new( 'CC71-H0-ACVMCA3-MOIS',      4 ),
        oEdtk::Field->new( 'CC71-H0-RECURENCE-MOD',     1 ),
        oEdtk::Field->new( 'CC71-H0-IND-TYPE-ECHEANCE', 1 ),
        oEdtk::Field->new( 'CC71-H0-IND-PRIME-MOD',     1 ),

        # CC71-H0-DEROGATION-COM
        oEdtk::Field->new( 'CC71-H0-CODE-MOTIF-DC', 5 ),
        oEdtk::Field->new( 'CC71-H0-LIB-COURT-DC',  15 ),
        oEdtk::Field->new( 'CC71-H0-LIB-DC',        50 ),

        # CC71-H0-VALEUR-DC
        oEdtk::SignedField->new( 'CC71-H0-ACVDC-MNT',  8, 2 ),
        oEdtk::SignedField->new( 'CC71-H0-ACVDC-TAUX', 4, 5 ),
        oEdtk::Field->new( 'CC71-H0-UNITE',         2 ),
        oEdtk::Field->new( 'CC71-H0-DATE-DEBUT-DC', 8 ),
        oEdtk::Field->new( 'CC71-H0-DATE-FIN-DC',   8 ),

        # CC71-H0-SURPIME-RA
        oEdtk::Field->new( 'CC71-H0-CODE-MOTIF-SRA', 5 ),
        oEdtk::Field->new( 'CC71-H0-LIB-COURT-SRA',  15 ),
        oEdtk::Field->new( 'CC71-H0-LIB-SRA',        50 ),

        # CC71-H0-VAL-SRA
        oEdtk::SignedField->new( 'CC71-H0-ACVSR-MNT',  8, 2 ),
        oEdtk::SignedField->new( 'CC71-H0-ACVSR-TAUX', 4, 5 ),
        oEdtk::Field->new( 'CC71-H0-UNITE-SRA',      2 ),
        oEdtk::Field->new( 'CC71-H0-DATE-DEBUT-SRA', 8 ),
        oEdtk::Field->new( 'CC71-H0-DATE-FIN-SRA',   8 ),
        oEdtk::Field->new( 'CC71-H0-IND-PRIME-SRA',  1 ),
        oEdtk::Field->new( 'CC71-H0-NBR-LGN',        5 ),
        oEdtk::Field->new( 'CC71-H0-FILLER',         134 ),
    );
}

1;
