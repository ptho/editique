package oEUser::Descriptor::H1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H1-LGNDAT
        oEdtk::Field->new( 'H1-ACH2PEREI1',        8 ),
        oEdtk::Field->new( 'H1-PENOCODCIV',        10 ),
        oEdtk::Field->new( 'H1-LIBCOU-PENOCODCIV', 15 ),
        oEdtk::Field->new( 'H1-PENOCODTIT',        10 ),
        oEdtk::Field->new( 'H1-LIBCOU-PENOCODTIT', 15 ),
        oEdtk::Field->new( 'H1-PENOLIBLON',        50 ),
        oEdtk::Field->new( 'H1-PENOPRN01',         20 ),
        oEdtk::Field->new( 'H1-ACH2CODTAS',        10 ),
        oEdtk::Field->new( 'H1-LIBCOU-ACH2CODTAS', 15 ),
        oEdtk::Field->new( 'H1-LIBLON-ACH2CODTAS', 50 ),
        oEdtk::Field->new( 'H1-ACH2CODAXT',        10 ),
        oEdtk::Field->new( 'H1-ACH2ACASR2',        2 ),
        oEdtk::Field->new( 'H1-PEPPCODCSO',        12 ),
        oEdtk::Field->new( 'H1-LIBCOU-PEPPCODCSO', 15 ),
        oEdtk::Field->new( 'H1-LIBLON-PEPPCODCSO', 50 ),
        oEdtk::Field->new( 'H1-PEPPCODCSP',        4 ),
        oEdtk::Field->new( 'H1-LIBCOU-PEPPCODCSP', 15 ),
        oEdtk::Field->new( 'H1-LIBLON-PEPPCODCSP', 50 ),
        oEdtk::Field->new( 'H1-PEPPCODPRF',        5 ),
        oEdtk::Field->new( 'H1-LIBCOU-PEPPCODPRF', 15 ),
        oEdtk::Field->new( 'H1-LIBLON-PEPPCODPRF', 50 ),
        oEdtk::Field->new( 'H1-PECPDATNAI',        8 ),
        oEdtk::Field->new( 'H1-ACH2DDE1AD',        8 ),
        oEdtk::Field->new( 'H1-ACH2DDEZRA',        8 ),
        oEdtk::Field->new( 'H1-PEROTYPDNA',        2 ),
        oEdtk::Field->new( 'H1-PEAFNUMRO',         13 ),
        oEdtk::Field->new( 'H1-PEAFCLERO',         2 ),
        oEdtk::Field->new( 'H1-PERODDEDRO',        8 ),
        oEdtk::Field->new( 'H1-PERODFEDRO',        8 ),
        oEdtk::Field->new( 'H1-PEAFCODGRE',        2 ),
        oEdtk::Field->new( 'H1-PEAFCODCRO',        3 ),
        oEdtk::Field->new( 'H1-PEAFCODCPA',        4 ),
        oEdtk::Field->new( 'H1-ACH2INDTIP',        1 ),
        oEdtk::Field->new( 'H1-ACH2DDETIP',        8 ),
        oEdtk::Field->new( 'H1-ACH2DFETIP',        8 ),
        oEdtk::Field->new( 'H1-ACH2INDCMU',        1 ),
        oEdtk::Field->new( 'H1-ACH2INDTLT',        1 ),
        oEdtk::Field->new( 'H1-ACH2DDETLT',        8 ),
        oEdtk::Field->new( 'H1-ACH2DFETLT',        8 ),
        oEdtk::Field->new( 'H1-PEPPLIBMAT',        15 ),
        oEdtk::Field->new( 'H1-PEPPCODUSI',        10 ),
        oEdtk::Field->new( 'H1-PEPPCODATE',        10 ),
        oEdtk::Field->new( 'H1-PEPPCODEXP',        2 ),
        oEdtk::Field->new( 'H1-PEZZLICEXP',        15 ),
        oEdtk::Field->new( 'H1-PEPPETBPCM',        20 ),
        oEdtk::Field->new( 'H1-PEPPCOEGRD',        5 ),
        oEdtk::Field->new( 'H1-PEPPCODFCT',        5 ),
        oEdtk::Field->new( 'H1-PEZZLICFCT',        15 ),
        oEdtk::Field->new( 'H1-PEPPTYPCNG',        2 ),
        oEdtk::Field->new( 'H1-PEZZLICCNG',        15 ),
        oEdtk::Field->new( 'H1-PEPPTYPCTT',        2 ),
        oEdtk::Field->new( 'H1-PEZZLICCTT',        15 ),
        oEdtk::Field->new( 'H1-PEPPCODPSU',        2 ),
        oEdtk::Field->new( 'H1-PEZZLICPSU',        15 ),
        oEdtk::Field->new( 'H1-PEPPDDEACT',        8 ),
        oEdtk::Field->new( 'H1-PEPPDFEACT',        8 ),
        oEdtk::Field->new( 'H1-RESERVE-H1',        250 ),
    );
}

1;
