package oEUser::Descriptor::R660;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R660-LGNDAT
        oEdtk::Field->new( 'R660-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R660-PERSCODREF',    10 ),
        oEdtk::Field->new( 'R660-LC-PERSCODREF', 15 ),
        oEdtk::Field->new( 'R660-LL-PERSCODREF', 50 ),
        oEdtk::Field->new( 'R660-PERSDDEREF',    10 ),
        oEdtk::Field->new( 'R660-PERSDFEREF',    10 ),
        oEdtk::Field->new( 'R660-PERSNUMODR',    4 ),
        oEdtk::Field->new( 'R660-PERSTYPIND',    1 ),
        oEdtk::Field->new( 'R660-LC-PERSTYPIND', 15 ),
        oEdtk::Field->new( 'R660-LL-PERSTYPIND', 50 ),
        oEdtk::SignedField->new( 'R660-PERSVALSAL', 12, 2 ),
        oEdtk::Field->new( 'R660-PERSINDSAL',    8 ),
        oEdtk::Field->new( 'R660-PERSTYPSAL',    1 ),
        oEdtk::Field->new( 'R660-LC-PERSTYPSAL', 15 ),
        oEdtk::Field->new( 'R660-LL-PERSTYPSAL', 50 ),
        oEdtk::SignedField->new( 'R660-PERSSALAVT', 12, 2 ),
        oEdtk::SignedField->new( 'R660-PERSTAXSAL', 4,  5 ),
        oEdtk::Field->new( 'R660-PERSELTSAL', 6 ),
        oEdtk::SignedField->new( 'R660-PERSVALPRM', 12, 2 ),
        oEdtk::Field->new( 'R660-PERSINDPRM',    8 ),
        oEdtk::Field->new( 'R660-PERSTYPPRM',    1 ),
        oEdtk::Field->new( 'R660-LC-PERSTYPPRM', 15 ),
        oEdtk::Field->new( 'R660-LL-PERSTYPPRM', 50 ),
        oEdtk::SignedField->new( 'R660-PERSPRMAVT', 12, 2 ),
        oEdtk::SignedField->new( 'R660-PERSTAXPRM', 4,  5 ),
        oEdtk::Field->new( 'R660-PERSELTPRM', 6 ),
        oEdtk::Field->new( 'R660-PERSINDICE', 8 ),
        oEdtk::SignedField->new( 'R660-PERSTPSPAR', 4, 5 ),
    );
}

1;
