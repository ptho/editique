package oEUser::Descriptor::L7;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # L7-LGNDAT
        # L7-IDENT-CI
        oEdtk::Field->new( 'L7-NUMPART',    5 ),
        oEdtk::Field->new( 'L7-ACH1NUMCIN', 15 ),

        # L7-ARRONDI
        oEdtk::SignedField->new( 'L7-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'L7-MONTANT-TTC', 12, 2 ),
        oEdtk::Field->new( 'L7-ECHIPU',     8 ),
        oEdtk::Field->new( 'L7-TYPCNP',     1 ),
        oEdtk::Field->new( 'L7-LIC-TYPCNP', 15 ),
        oEdtk::Field->new( 'L7-LIB-TYPCNP', 50 ),

        # L7-INFO-OPE
        oEdtk::Field->new( 'L7-ACOPNUMOPE', 5 ),
        oEdtk::Field->new( 'L7-ACOPDDEOPE', 8 ),
        oEdtk::Field->new( 'L7-ACOPDDECOT', 8 ),
        oEdtk::Field->new( 'L7-ACOPNUMAVT', 3 ),
        oEdtk::Field->new( 'L7-ACOPCODAGC', 4 ),
        oEdtk::Field->new( 'L7-ACOPLICAGC', 15 ),
        oEdtk::Field->new( 'L7-ACOPLIBAGC', 50 ),
        oEdtk::Field->new( 'L7-ACOPCODCTX', 4 ),
        oEdtk::Field->new( 'L7-ACOPLICCTX', 15 ),
        oEdtk::Field->new( 'L7-ACOPLIBCTX', 50 ),
        oEdtk::Field->new( 'L7-ACOPCODTRT', 5 ),

        # L7-DATE-EXIGIBILITE
        oEdtk::Field->new( 'L7-DATEXI', 8 ),
    );
}

1;
