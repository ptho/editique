package oEUser::Descriptor::R100;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R100-LGNDAT
        oEdtk::Field->new( 'R100-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R100-ACOPACOBR1',    8 ),
        oEdtk::Field->new( 'R100-ACOPNUMCCO',    15 ),
        oEdtk::Field->new( 'R100-ACOPNUMOPE',    5 ),
        oEdtk::Field->new( 'R100-ACOPDDEOPE',    8 ),
        oEdtk::Field->new( 'R100-ACOPDDECOT',    8 ),
        oEdtk::Field->new( 'R100-ACOPCODAGC',    4 ),
        oEdtk::Field->new( 'R100-LL-ACOPCODAGC', 50 ),
        oEdtk::Field->new( 'R100-LC-ACOPCODAGC', 15 ),
        oEdtk::Field->new( 'R100-ACTION-ACTE',   2 ),
    );
}

1;
