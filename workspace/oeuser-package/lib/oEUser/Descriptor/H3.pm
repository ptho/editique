package oEUser::Descriptor::H3;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H3-LGNDAT
        oEdtk::Field->new( 'H3-ACCLACASR2',        2 ),
        oEdtk::Field->new( 'H3-ACCLNUMCAU',        5 ),
        oEdtk::Field->new( 'H3-ACCLDDEPRD',        8 ),
        oEdtk::Field->new( 'H3-ACCLDFEPRD',        8 ),
        oEdtk::Field->new( 'H3-ACCLTYPTXT',        1 ),
        oEdtk::Field->new( 'H3-LIBCOU-ACCLTYPTXT', 15 ),
        oEdtk::Field->new( 'H3-ACCLPRCLI1',        8 ),

        # H3-TEXTE-CLAUSE
        oEdtk::Field->new( 'H3-TEXCL1',     250 ),
        oEdtk::Field->new( 'H3-TEXCL2',     250 ),
        oEdtk::Field->new( 'H3-TEXCL3',     250 ),
        oEdtk::Field->new( 'H3-TEXCL4',     250 ),
        oEdtk::Field->new( 'H3-RESERVE-H3', 200 ),
    );
}

1;
