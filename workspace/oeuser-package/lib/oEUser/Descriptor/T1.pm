package oEUser::Descriptor::T1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # T1-LGNDAT
        # T1-INFOS-ECHEANCE
        oEdtk::SignedField->new( 'T1-DEJA-PERCU', 12, 2 ),
        oEdtk::SignedField->new( 'T1-RESTE-DU',   12, 2 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-01', 8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-01', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-01', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-02',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-02', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-02', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-03',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-03', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-03', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-04',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-04', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-04', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-05',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-05', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-05', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-06',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-06', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-06', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-07',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-07', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-07', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-08',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-08', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-08', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-09',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-09', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-09', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-10',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-10', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-10', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-11',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-11', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-11', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-12',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-12', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-12', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-13',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-13', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-13', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-14',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-14', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-14', 8 ),
        oEdtk::Field->new( 'T1-ECHE-IMPU-15',  8 ),
        oEdtk::SignedField->new( 'T1-MONTANT-TTC-ECHE-15', 12, 2 ),
        oEdtk::Field->new( 'T1-DAT-PRELEV-15', 8 ),
        oEdtk::SignedField->new( 'T1-TOTAL-PRE-ENCAISS', 12, 2 ),
        oEdtk::Field->new( 'T1-FILLER', 286 ),
    );
}

1;
