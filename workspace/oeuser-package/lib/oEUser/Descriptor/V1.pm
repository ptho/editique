package oEUser::Descriptor::V1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # V1-ENREG
        oEdtk::Field->new( 'V1-PEPENUMPER',       8 ),
        oEdtk::Field->new( 'V1-ROLPER-ACPA',      1 ),
        oEdtk::Field->new( 'V1-ROLPER-ACDP',      1 ),
        oEdtk::Field->new( 'V1-ROLPER-ACDP-ASS',  1 ),
        oEdtk::Field->new( 'PNTREM-PA',           50 ),
        oEdtk::Field->new( 'V1-CPLADR-PA',        50 ),
        oEdtk::Field->new( 'V1-PEADNUMVOI-PA',    9 ),
        oEdtk::Field->new( 'V1-PEADBTQVOI-PA',    2 ),
        oEdtk::Field->new( 'V1-PELICBTQVOI-PA',   15 ),
        oEdtk::Field->new( 'V1-PEVONATVOI-PA',    5 ),
        oEdtk::Field->new( 'V1-PELICNATVOI-PA',   15 ),
        oEdtk::Field->new( 'V1-LIBVOI-PA',        50 ),
        oEdtk::Field->new( 'V1-INDCDX-PA',        1 ),
        oEdtk::Field->new( 'V1-BOIPOSLIBLDT-PA',  50 ),
        oEdtk::Field->new( 'V1-LIBLOCLIBLOC-PA',  50 ),
        oEdtk::Field->new( 'V1-CODCDXCODPOS-PA',  10 ),
        oEdtk::Field->new( 'V1-NOMCDXLIBACH-PA',  50 ),
        oEdtk::Field->new( 'V1-PEPACODPAY-PA',    3 ),
        oEdtk::Field->new( 'V1-PELICCODPAY-PA',   15 ),
        oEdtk::Field->new( 'V1-TRNFAC-PA',        4 ),
        oEdtk::Field->new( 'V1-PNTREM-DP',        50 ),
        oEdtk::Field->new( 'V1-CPLADR-DP',        50 ),
        oEdtk::Field->new( 'V1-PEADNUMVOI-DP',    9 ),
        oEdtk::Field->new( 'V1-PEADBTQVOI-DP',    2 ),
        oEdtk::Field->new( 'V1-PELICBTQVOI-DP',   15 ),
        oEdtk::Field->new( 'V1-PEVONATVOI-DP',    5 ),
        oEdtk::Field->new( 'V1-PELICNATVOI-DP',   15 ),
        oEdtk::Field->new( 'V1-LIBVOI-DP',        50 ),
        oEdtk::Field->new( 'V1-INDCDX-DP',        1 ),
        oEdtk::Field->new( 'V1-BOIPOSLIBLDT-DP',  50 ),
        oEdtk::Field->new( 'V1-LIBLOCLIBLOC-DP',  50 ),
        oEdtk::Field->new( 'V1-CODCDXCODPOS-DP',  10 ),
        oEdtk::Field->new( 'V1-NOMCDXLIBACH-DP',  50 ),
        oEdtk::Field->new( 'V1-PEPACODPAY-DP',    3 ),
        oEdtk::Field->new( 'V1-PELICCODPAY-DP',   15 ),
        oEdtk::Field->new( 'V1-TRNFAC-DP',        4 ),
        oEdtk::Field->new( 'V1-CPTTIT-AN',        50 ),
        oEdtk::Field->new( 'V1-CODBQE-AN',        10 ),
        oEdtk::Field->new( 'V1-CODGUI-AN',        10 ),
        oEdtk::Field->new( 'V1-NUMCPT-AN',        20 ),
        oEdtk::Field->new( 'V1-CLECPT-AN',        2 ),
        oEdtk::Field->new( 'V1-CODPAY-RIB-AN',    3 ),
        oEdtk::Field->new( 'V1-LICCODPAY-RIB-AN', 15 ),
        oEdtk::Field->new( 'V1-CODDEV-RIB-AN',    3 ),
        oEdtk::Field->new( 'V1-LICDEV--RIB-AN',   15 ),
        oEdtk::Field->new( 'V1-CODPA2-AN',        2 ),
        oEdtk::Field->new( 'V1-CLEIBA-AN',        2 ),
        oEdtk::Field->new( 'V1-IDTNAT-AN',        30 ),
        oEdtk::Field->new( 'V1-CODBIC-AN',        14 ),
        oEdtk::Field->new( 'V1-CPTTIT-NX',        50 ),
        oEdtk::Field->new( 'V1-CODBQE-NX',        10 ),
        oEdtk::Field->new( 'V1-CODGUI-NX',        10 ),
        oEdtk::Field->new( 'V1-NUMCPT-NX',        20 ),
        oEdtk::Field->new( 'V1-CLECPT-NX',        2 ),
        oEdtk::Field->new( 'V1-CODPAY-RIB-NX',    3 ),
        oEdtk::Field->new( 'V1-LICCODPAY-RIB-NX', 15 ),
        oEdtk::Field->new( 'V1-CODDEV-RIB-NX',    3 ),
        oEdtk::Field->new( 'V1-LICDEV--RIB-NX',   15 ),
        oEdtk::Field->new( 'V1-CODPA2-NX',        2 ),
        oEdtk::Field->new( 'V1-CLEIBA-NX',        2 ),
        oEdtk::Field->new( 'V1-IDTNAT-NX',        30 ),
        oEdtk::Field->new( 'V1-CODBIC-NX',        14 ),
        oEdtk::Field->new( 'V1-PERIREFMOB',       35 ),
        oEdtk::Field->new( 'V1-PERIDATMOB',       8 ),
        oEdtk::Field->new( 'V1-DATTRT',           14 ),
    );
}

1;
