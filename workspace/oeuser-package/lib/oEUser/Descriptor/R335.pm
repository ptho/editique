package oEUser::Descriptor::R335;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R335-LGNDAT
        oEdtk::Field->new( 'R335-ACFPDDEPRD', 8 ),
        oEdtk::Field->new( 'R335-ACFPDFEPRD', 8 ),
        oEdtk::Field->new( 'R335-ACFQLIBCOU', 15 ),
        oEdtk::Field->new( 'R335-ACFQLIBLON', 50 ),
        oEdtk::Field->new( 'R335-ACFQFRQRGC', 2 ),
        oEdtk::Field->new( 'R335-ACFQNBRMOI', 2 ),
        oEdtk::Field->new( 'R335-ACFQINDEGA', 1 ),
    );
}

1;
