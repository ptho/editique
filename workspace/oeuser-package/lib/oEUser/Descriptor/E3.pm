package oEUser::Descriptor::E3;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # E3-ZZEDIT-E3-LGNDAT
        # E3-ZZEDIT-MUT-DOC
        oEdtk::Field->new( 'E3-ODGGLIBLON', 50 ),
        oEdtk::Field->new( 'E3-ODGGLIBCOU', 15 ),
        oEdtk::Field->new( 'E3-ODGGRMQ',    250 ),

        # E3-ZZEDIT-CORR-DOC
        oEdtk::Field->new( 'E3-ODCOLIBQUA', 50 ),

        # E3-ZZEDIT-NOM-P-PH
        oEdtk::Field->new( 'E3-PENOCODCIV',     10 ),
        oEdtk::Field->new( 'E3-PEZZ-LICCODCIV', 15 ),
        oEdtk::Field->new( 'E3-PENOCODTIT',     10 ),
        oEdtk::Field->new( 'E3-PEZZ-LICCODTIT', 15 ),
        oEdtk::Field->new( 'E3-PENOLIBLON',     50 ),
        oEdtk::Field->new( 'E3-PENOPRN01',      20 ),

        # E3-ZZEDIT-ADRESSE
        oEdtk::Field->new( 'E3-ZZZZ-PNTREM',       32 ),
        oEdtk::Field->new( 'E3-ZZZZ-CPLADR',       32 ),
        oEdtk::Field->new( 'E3-PEADNUMVOI',        9 ),
        oEdtk::Field->new( 'E3-PEADBTQVOI',        2 ),
        oEdtk::Field->new( 'E3-PEZZ-LICBTQVOI',    15 ),
        oEdtk::Field->new( 'E3-PEVONATVOI',        5 ),
        oEdtk::Field->new( 'E3-PEZZ-LICNATVOI',    15 ),
        oEdtk::Field->new( 'E3-ZZZZ-LIBVOI',       32 ),
        oEdtk::Field->new( 'E3-ZZZZ-INDCDX',       1 ),
        oEdtk::Field->new( 'E3-ZZZZ-BOIPOSLIBLDT', 32 ),
        oEdtk::Field->new( 'E3-ZZZZ-LIBLOCLIBLOC', 32 ),
        oEdtk::Field->new( 'E3-ZZZZ-CODCDXCODPOS', 10 ),
        oEdtk::Field->new( 'E3-ZZZZ-NOMCDXLIBACH', 32 ),
        oEdtk::Field->new( 'E3-PEPACODPAY',        3 ),
        oEdtk::Field->new( 'E3-PEZZ-LICCODPAY',    15 ),
        oEdtk::Field->new( 'E3-ZZZZ-TRNFAC',       4 )
    );
}

1;
