package oEUser::Descriptor::R600;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R600-LGNDAT
        oEdtk::Field->new( 'R600-PEPENUMPER', 8 ),
        oEdtk::Field->new( 'R600-PEREGESPHY', 1 ),
        oEdtk::Field->new( 'R600-PEREGESMOR', 1 ),
        oEdtk::Field->new( 'R600-PEPEENSOI1', 9 ),

        # R600-ROLE-PERS
        oEdtk::Field->new( 'R600-PEPRROLPER',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE',       8 ),

        # R600-ROLE-PERS-2
        oEdtk::Field->new( 'R600-PEPRROLPER-2',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-2', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-2', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-2',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-2',       8 ),

        # R600-ROLE-PERS-3
        oEdtk::Field->new( 'R600-PEPRROLPER-3',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-3', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-3', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-3',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-3',       8 ),

        # R600-ROLE-PERS-4
        oEdtk::Field->new( 'R600-PEPRROLPER-4',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-4', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-4', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-4',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-4',       8 ),

        # R600-ROLE-PERS-5
        oEdtk::Field->new( 'R600-PEPRROLPER-5',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-5', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-5', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-5',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-5',       8 ),

        # R600-ROLE-PERS-6
        oEdtk::Field->new( 'R600-PEPRROLPER-6',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-6', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-6', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-6',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-6',       8 ),

        # R600-ROLE-PERS-7
        oEdtk::Field->new( 'R600-PEPRROLPER-7',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-7', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-7', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-7',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-7',       8 ),

        # R600-ROLE-PERS-8
        oEdtk::Field->new( 'R600-PEPRROLPER-8',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-8', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-8', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-8',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-8',       8 ),

        # R600-ROLE-PERS-9
        oEdtk::Field->new( 'R600-PEPRROLPER-9',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-9', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-9', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-9',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-9',       8 ),

        # R600-ROLE-PERS-10
        oEdtk::Field->new( 'R600-PEPRROLPER-10',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-10', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-10', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-10',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-10',       8 ),

        # R600-ROLE-PERS-11
        oEdtk::Field->new( 'R600-PEPRROLPER-11',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-11', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-11', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-11',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-11',       8 ),

        # R600-ROLE-PERS-12
        oEdtk::Field->new( 'R600-PEPRROLPER-12',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-12', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-12', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-12',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-12',       8 ),

        # R600-ROLE-PERS-13
        oEdtk::Field->new( 'R600-PEPRROLPER-13',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-13', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-13', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-13',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-13',       8 ),

        # R600-ROLE-PERS-14
        oEdtk::Field->new( 'R600-PEPRROLPER-14',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-14', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-14', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-14',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-14',       8 ),

        # R600-ROLE-PERS-15
        oEdtk::Field->new( 'R600-PEPRROLPER-15',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-15', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-15', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-15',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-15',       8 ),

        # R600-ROLE-PERS-16
        oEdtk::Field->new( 'R600-PEPRROLPER-16',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-16', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-16', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-16',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-16',       8 ),

        # R600-ROLE-PERS-17
        oEdtk::Field->new( 'R600-PEPRROLPER-17',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-17', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-17', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-17',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-17',       8 ),

        # R600-ROLE-PERS-18
        oEdtk::Field->new( 'R600-PEPRROLPER-18',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-18', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-18', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-18',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-18',       8 ),

        # R600-ROLE-PERS-19
        oEdtk::Field->new( 'R600-PEPRROLPER-19',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-19', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-19', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-19',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-19',       8 ),

        # R600-ROLE-PERS-20
        oEdtk::Field->new( 'R600-PEPRROLPER-20',    5 ),
        oEdtk::Field->new( 'R600-LL-PEPRROLPER-20', 50 ),
        oEdtk::Field->new( 'R600-LC-PEPRROLPER-20', 15 ),
        oEdtk::Field->new( 'R600-PEPRDDE-20',       8 ),
        oEdtk::Field->new( 'R600-PEPRDFE-20',       8 ),
    );
}

1;
