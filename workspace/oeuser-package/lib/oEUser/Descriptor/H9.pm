package oEUser::Descriptor::H9;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H9-LGNDAT
        oEdtk::Field->new( 'H9-ACELACASR2',        2 ),
        oEdtk::Field->new( 'H9-ACH4CODGAR',        10 ),
        oEdtk::Field->new( 'H9-ACH4CODPRT',        10 ),
        oEdtk::Field->new( 'H9-ACELACGSR3',        5 ),
        oEdtk::Field->new( 'H9-ACELACGSR4',        5 ),
        oEdtk::Field->new( 'H9-ACELCODTEC',        1 ),
        oEdtk::Field->new( 'H9-LIBCOU-ACELCODTEC', 15 ),
        oEdtk::Field->new( 'H9-LIBLON-ACELCODTEC', 50 ),
        oEdtk::Field->new( 'H9-ACCODDEPRD',        8 ),
        oEdtk::Field->new( 'H9-ACCODFIPRD',        8 ),
        oEdtk::SignedField->new( 'H9-ACT1MTPTAX',         12, 2 ),
        oEdtk::SignedField->new( 'H9-ACCOMAOCOT',         12, 2 ),
        oEdtk::SignedField->new( 'H9-ACCOMPHCOT',         12, 2 ),
        oEdtk::SignedField->new( 'H9-ACCOMAHCOT',         12, 2 ),
        oEdtk::SignedField->new( 'H9-ACCO-REDUCTION-TTC', 12, 2 ),
        oEdtk::SignedField->new( 'H9-ACCO-REDUCTION-HT',  12, 2 ),

        # H9-TAB-TAXES
        # H9-ELT-TAXES
        oEdtk::Field->new( 'H9-PRTXCODTAX', 6 ),
        oEdtk::Field->new( 'H9-ACT1DDEPRD', 8 ),
        oEdtk::Field->new( 'H9-ACT1DFIPRD', 8 ),
        oEdtk::SignedField->new( 'H9-PRVLVALTAU', 4,  5 ),
        oEdtk::SignedField->new( 'H9-ACEAMNTECH', 12, 2 ),
        oEdtk::Field->new( 'H9-PRTXIDFSYS', 8 ),
        oEdtk::Field->new( 'H9-PRTXLIBLON', 50 ),

        # H9-TAXE-1
        oEdtk::Field->new( 'H9-PRTXCODTAX-1', 6 ),
        oEdtk::Field->new( 'H9-ACT1DDEPRD-1', 8 ),
        oEdtk::Field->new( 'H9-ACT1DFIPRD-1', 8 ),
        oEdtk::SignedField->new( 'H9-PRVLVALTAU-1', 4,  5 ),
        oEdtk::SignedField->new( 'H9-ACEAMNTECH-1', 12, 2 ),
        oEdtk::Field->new( 'H9-PRTXIDFSYS-1', 8 ),
        oEdtk::Field->new( 'H9-PRTXLIBLON-1', 50 ),

        # H9-TAXE-2
        oEdtk::Field->new( 'H9-PRTXCODTAX-2', 6 ),
        oEdtk::Field->new( 'H9-ACT1DDEPRD-2', 8 ),
        oEdtk::Field->new( 'H9-ACT1DFIPRD-2', 8 ),
        oEdtk::SignedField->new( 'H9-PRVLVALTAU-2', 4,  5 ),
        oEdtk::SignedField->new( 'H9-ACEAMNTECH-2', 12, 2 ),
        oEdtk::Field->new( 'H9-PRTXIDFSYS-2', 8 ),
        oEdtk::Field->new( 'H9-PRTXLIBLON-2', 50 ),

        # H9-TAXE-3
        oEdtk::Field->new( 'H9-PRTXCODTAX-3', 6 ),
        oEdtk::Field->new( 'H9-ACT1DDEPRD-3', 8 ),
        oEdtk::Field->new( 'H9-ACT1DFIPRD-3', 8 ),
        oEdtk::SignedField->new( 'H9-PRVLVALTAU-3', 4,  5 ),
        oEdtk::SignedField->new( 'H9-ACEAMNTECH-3', 12, 2 ),
        oEdtk::Field->new( 'H9-PRTXIDFSYS-3', 8 ),
        oEdtk::Field->new( 'H9-PRTXLIBLON-3', 50 ),

        # H9-TAXE-4
        oEdtk::Field->new( 'H9-PRTXCODTAX-4', 6 ),
        oEdtk::Field->new( 'H9-ACT1DDEPRD-4', 8 ),
        oEdtk::Field->new( 'H9-ACT1DFIPRD-4', 8 ),
        oEdtk::SignedField->new( 'H9-PRVLVALTAU-4', 4,  5 ),
        oEdtk::SignedField->new( 'H9-ACEAMNTECH-4', 12, 2 ),
        oEdtk::Field->new( 'H9-PRTXIDFSYS-4', 8 ),
        oEdtk::Field->new( 'H9-PRTXLIBLON-4', 50 ),

        # H9-TAXE-5
        oEdtk::Field->new( 'H9-PRTXCODTAX-5', 6 ),
        oEdtk::Field->new( 'H9-ACT1DDEPRD-5', 8 ),
        oEdtk::Field->new( 'H9-ACT1DFIPRD-5', 8 ),
        oEdtk::SignedField->new( 'H9-PRVLVALTAU-5', 4,  5 ),
        oEdtk::SignedField->new( 'H9-ACEAMNTECH-5', 12, 2 ),
        oEdtk::Field->new( 'H9-PRTXIDFSYS-5', 8 ),
        oEdtk::Field->new( 'H9-PRTXLIBLON-5', 50 ),
        oEdtk::Field->new( 'H9-RESERVE-H9',   200 ),
    );
}

1;
