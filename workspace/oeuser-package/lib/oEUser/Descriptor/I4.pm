package oEUser::Descriptor::I4;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # I4-LGNDAT
        # I4-STATS-REJETS
        oEdtk::Field->new( 'I4-DATREJET-DEB', 8 ),
        oEdtk::Field->new( 'I4-DATREJET-FIN', 8 ),
        oEdtk::Field->new( 'I4-NB-REJET',     3 ),
        oEdtk::Field->new( 'I4-DATEXI',       8 ),
        oEdtk::Field->new( 'I4-LIBCODPAY',    50 ),

        # I4-CPL-ADR-PAY				Complément adresse du payeur
        oEdtk::Field->new( 'I4-PNTREM-CP',       50 ),
        oEdtk::Field->new( 'I4-CPLADR-CP',       50 ),
        oEdtk::Field->new( 'I4-LIBVOI-CP',       50 ),
        oEdtk::Field->new( 'I4-BOIPOSLIBLDT-CP', 50 ),
        oEdtk::Field->new( 'I4-LIBLOCLIBLOC-CP', 50 ),
        oEdtk::Field->new( 'I4-NOMCDXLIBACH-CP', 50 ),

        # I4-CPL-ADR-ER				Complément adresse de l'entité de rattachement
        oEdtk::Field->new( 'I4-PNTREM-CER',       50 ),
        oEdtk::Field->new( 'I4-CPLADR-CER',       50 ),
        oEdtk::Field->new( 'I4-LIBVOI-CER',       50 ),
        oEdtk::Field->new( 'I4-BOIPOSLIBLDT-CER', 50 ),
        oEdtk::Field->new( 'I4-LIBLOCLIBLOC-CER', 50 ),
        oEdtk::Field->new( 'I4-NOMCDXLIBACH-CER', 50 ),

        # I4-CPL-ADR-CG				Complément adresse du centre de gestion
        oEdtk::Field->new( 'I4-PNTREM-CCGS',       50 ),
        oEdtk::Field->new( 'I4-CPLADR-CCGS',       50 ),
        oEdtk::Field->new( 'I4-LIBVOI-CCGS',       50 ),
        oEdtk::Field->new( 'I4-BOIPOSLIBLDT-CCGS', 50 ),
        oEdtk::Field->new( 'I4-LIBLOCLIBLOC-CCGS', 50 ),
        oEdtk::Field->new( 'I4-NOMCDXLIBACH-CCGS', 50 ),

        # I4-INFO-AGENCE			Informations sur l'agence
        oEdtk::Field->new( 'I4-ODAGCODAGC-AG',   9 ),
        oEdtk::Field->new( 'I4-PENOCODTIT-AG',   10 ),
        oEdtk::Field->new( 'I4-LICCODTIT-AG',    15 ),
        oEdtk::Field->new( 'I4-PENOLIBLON-AG',   50 ),
        oEdtk::Field->new( 'I4-PNTREM-AG',       50 ),
        oEdtk::Field->new( 'I4-CPLADR-AG',       50 ),
        oEdtk::Field->new( 'I4-PEADNUMVOI-AG',   9 ),
        oEdtk::Field->new( 'I4-PEADBTQVOI-AG',   2 ),
        oEdtk::Field->new( 'I4-LICBTQVOI-AG',    15 ),
        oEdtk::Field->new( 'I4-PEVONATVOI-AG',   5 ),
        oEdtk::Field->new( 'I4-LICNATVOI-AG',    15 ),
        oEdtk::Field->new( 'I4-LIBVOI-AG',       50 ),
        oEdtk::Field->new( 'I4-INDCDX-AG',       1 ),
        oEdtk::Field->new( 'I4-BOIPOSLIBLDT-AG', 50 ),
        oEdtk::Field->new( 'I4-LIBLOCLIBLOC-AG', 50 ),
        oEdtk::Field->new( 'I4-CODCDXCODPOS-AG', 10 ),
        oEdtk::Field->new( 'I4-NOMCDXLIBACH-AG', 50 ),
        oEdtk::Field->new( 'I4-PEPACODPAY-AG',   3 ),
        oEdtk::Field->new( 'I4-LICCODPAY-AG',    15 ),
        oEdtk::Field->new( 'I4-TRNFAC-AG',       4 ),

        # I4-ZZEDIT-COMM-AG1 // Communication 1/3 de l'agence
        oEdtk::Field->new( 'I4-PECONATCOM-AG1',  5 ),
        oEdtk::Field->new( 'I4-PELICNATCOM-AG1', 15 ),
        oEdtk::Field->new( 'I4-PECOVALCOM-AG1',  50 ),

        # I4-ZZEDIT-COMM-AG2 // Communication 2/3 de l'agence
        oEdtk::Field->new( 'I4-PECONATCOM-AG2',  5 ),
        oEdtk::Field->new( 'I4-PELICNATCOM-AG2', 15 ),
        oEdtk::Field->new( 'I4-PECOVALCOM-AG2',  50 ),

        # I4-ZZEDIT-COMM-AG3 // Communication 3/3 de l'agence
        oEdtk::Field->new( 'I4-PECONATCOM-AG3',  5 ),
        oEdtk::Field->new( 'I4-PELICNATCOM-AG3', 15 ),
        oEdtk::Field->new( 'I4-PECOVALCOM-AG3',  50 ),
        oEdtk::Field->new( 'I4-PENONUMPER-AG',   8 ),
    );
}

1;
