package oEUser::Descriptor::E7;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # E7-ZZEDIT-E7
        # E7-ZZEDIT-ADRESSE-DEST // Adresse du destinataire du document
        oEdtk::Field->new( 'E7-PNTREM-DD',       50 ),
        oEdtk::Field->new( 'E7-CPLADR-DD',       50 ),
        oEdtk::Field->new( 'E7-PEADNUMVO-DD',    9 ),
        oEdtk::Field->new( 'E7-PEADBTQVOI-DD',   2 ),
        oEdtk::Field->new( 'E7-PELICBTQVOI-DD',  15 ),
        oEdtk::Field->new( 'E7-LIBVOI-DD',       50 ),
        oEdtk::Field->new( 'E7-INDCDX-DD',       1 ),
        oEdtk::Field->new( 'E7-BOIPOSLIBLDT-DD', 50 ),
        oEdtk::Field->new( 'E7-LIBLOCLIBLOC-DD', 50 ),
        oEdtk::Field->new( 'E7-CODCDXCODPOS-DD', 10 ),
        oEdtk::Field->new( 'E7-NOMCDXLIBACH-DD', 50 ),
        oEdtk::Field->new( 'E7-PEPACODPAY-DD',   3 ),
        oEdtk::Field->new( 'E7-PELICCODPAY-DD',  15 ),
        oEdtk::Field->new( 'E7-TRNFAC-DD',       4 ),

        # E7-ZZEDIT-ADRESSE-ER // Adresse de l'entité de rattachement
        oEdtk::Field->new( 'E7-PNTREM-ER',       50 ),
        oEdtk::Field->new( 'E7-CPLADR-ER',       50 ),
        oEdtk::Field->new( 'E7-PEADNUMVOI-ER',   9 ),
        oEdtk::Field->new( 'E7-PEADBTQVOI-ER',   2 ),
        oEdtk::Field->new( 'E7-PELICBTQVOI-ER',  15 ),
        oEdtk::Field->new( 'E7-LIBVOI-ER',       50 ),
        oEdtk::Field->new( 'E7-INDCDX-ER',       1 ),
        oEdtk::Field->new( 'E7-BOIPOSLIBLDT-ER', 50 ),
        oEdtk::Field->new( 'E7-LIBLOCLIBLOC-ER', 50 ),
        oEdtk::Field->new( 'E7-CODCDXCODPOS-ER', 10 ),
        oEdtk::Field->new( 'E7-NOMCDXLIBACH-ER', 50 ),
        oEdtk::Field->new( 'E7-PEPACODPAY-ER',   3 ),
        oEdtk::Field->new( 'E7-PELICCODPAY-ER',  15 ),
        oEdtk::Field->new( 'E7-TRNFAC-ER',       4 ),

        # E7-ZZEDIT-ADRESSE-CG // Adresse du centre de gestion
        oEdtk::Field->new( 'E7-PNTREM-CG',       50 ),
        oEdtk::Field->new( 'E7-CPLADR-CG',       50 ),
        oEdtk::Field->new( 'E7-PEADNUMVOI-CG',   9 ),
        oEdtk::Field->new( 'E7-PEADBTQVOI-CG',   2 ),
        oEdtk::Field->new( 'E7-PELICBTQVOI-CG',  15 ),
        oEdtk::Field->new( 'E7-LIBVOI-CG',       50 ),
        oEdtk::Field->new( 'E7-INDCDX-CG',       1 ),
        oEdtk::Field->new( 'E7-BOIPOSLIBLDT-CG', 50 ),
        oEdtk::Field->new( 'E7-LIBLOCLIBLOC-CG', 50 ),
        oEdtk::Field->new( 'E7-CODCDXCODPOS-CG', 10 ),
        oEdtk::Field->new( 'E7-NOMCDXLIBACH-CG', 50 ),
        oEdtk::Field->new( 'E7-PEPACODPAY-CG',   3 ),
        oEdtk::Field->new( 'E7-PELICCODPAY-CG',  15 ),
        oEdtk::Field->new( 'E7-TRNFAC-CG',       4 ),

        # E7-ZZEDIT-ADRESSE-PU // Adresse du producteur
        oEdtk::Field->new( 'E7-PNTREM-PU',       50 ),
        oEdtk::Field->new( 'E7-CPLADR-PU',       50 ),
        oEdtk::Field->new( 'E7-PEADNUMVOI-PU',   9 ),
        oEdtk::Field->new( 'E7-PEADBTQVOI-PU',   2 ),
        oEdtk::Field->new( 'E7-PELICBTQVOI-PU',  15 ),
        oEdtk::Field->new( 'E7-LIBVOI-PU',       50 ),
        oEdtk::Field->new( 'E7-INDCDX-PU',       1 ),
        oEdtk::Field->new( 'E7-BOIPOSLIBLDT-PU', 50 ),
        oEdtk::Field->new( 'E7-LIBLOCLIBLOC-PU', 50 ),
        oEdtk::Field->new( 'E7-CODCDXCODPOS-PU', 10 ),
        oEdtk::Field->new( 'E7-NOMCDXLIBACH-PU', 50 ),
        oEdtk::Field->new( 'E7-PEPACODPAY-PU',   3 ),
        oEdtk::Field->new( 'E7-PELICCODPAY-PU',  15 ),
        oEdtk::Field->new( 'E7-TRNFAC-PU',       4 ),

        # E7-ZZEDIT-ADRESSE-SO // Adresse du souscripteur
        oEdtk::Field->new( 'E7-PNTREM-SO',       50 ),
        oEdtk::Field->new( 'E7-CPLADR-SO',       50 ),
        oEdtk::Field->new( 'E7-PEADNUMVOI-SO',   9 ),
        oEdtk::Field->new( 'E7-PEADBTQVOI-SO',   2 ),
        oEdtk::Field->new( 'E7-PELICBTQVOI-SO',  15 ),
        oEdtk::Field->new( 'E7-LIBVOI-SO',       50 ),
        oEdtk::Field->new( 'E7-INDCDX-SO',       1 ),
        oEdtk::Field->new( 'E7-BOIPOSLIBLDT-SO', 50 ),
        oEdtk::Field->new( 'E7-LIBLOCLIBLOC-SO', 50 ),
        oEdtk::Field->new( 'E7-CODCDXCODPOS-SO', 10 ),
        oEdtk::Field->new( 'E7-NOMCDXLIBACH-SO', 50 ),
        oEdtk::Field->new( 'E7-PEPACODPAY-SO',   3 ),
        oEdtk::Field->new( 'E7-PELICCODPAY-SO',  15 ),
        oEdtk::Field->new( 'E7-TRNFAC-SO',       4 ),

        # E7-ZZEDIT-ADRESSE-AG // Adresse de l'agence
        oEdtk::Field->new( 'E7-PNTREM-AG',       50 ),
        oEdtk::Field->new( 'E7-CPLADR-AG',       50 ),
        oEdtk::Field->new( 'E7-PEADNUMVOI-AG',   9 ),
        oEdtk::Field->new( 'E7-PEADBTQVOI-AG',   2 ),
        oEdtk::Field->new( 'E7-PELICBTQVOI-AG',  15 ),
        oEdtk::Field->new( 'E7-LIBVOI-AG',       50 ),
        oEdtk::Field->new( 'E7-INDCDX-AG',       1 ),
        oEdtk::Field->new( 'E7-BOIPOSLIBLDT-AG', 50 ),
        oEdtk::Field->new( 'E7-LIBLOCLIBLOC-AG', 50 ),
        oEdtk::Field->new( 'E7-CODCDXCODPOS-AG', 10 ),
        oEdtk::Field->new( 'E7-NOMCDXLIBACH-AG', 50 ),
        oEdtk::Field->new( 'E7-PEPACODPAY-AG',   3 ),
        oEdtk::Field->new( 'E7-PELICCODPAY-AG',  15 ),
        oEdtk::Field->new( 'E7-TRNFAC-AG',       4 ),
    );
}

1;
