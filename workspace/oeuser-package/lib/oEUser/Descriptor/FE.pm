package oEUser::Descriptor::FE;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::SignedField;
use oEdtk::DateField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # FE-LGNDAT
        # FE-DEC-MED
        oEdtk::SignedField->new( 'FE-DECISTOP', 1 )
        ,    #FE-DECISION-MEDICALE-TOP totootot
        oEdtk::Field->new( 'FE-DECISMED', 50 ),    #FE-DECISION-MEDICALE
                                                   # FE-ECH-INI
        oEdtk::DateField->new( 'FE-DTECHINI', 8 ), #FE-DATE-ECH-INIT
        oEdtk::SignedField->new( 'FE-MTECHINI', 12, 2 ),    #FE-MT-ECH-INIT
    );
}

1;
