package oEUser::Descriptor::HW;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # HW-HW-LGNDAT
        # HW-INFO-CALC-INTER
        oEdtk::Field->new( 'HW-ACH4ACGSR2', 2 ),
        oEdtk::Field->new( 'HW-ACH4CODPRT', 10 ),
        oEdtk::Field->new( 'HW-ACH4CODGAR', 10 ),
        oEdtk::Field->new( 'HW-ACCODDEPRD', 8 ),
        oEdtk::Field->new( 'HW-ACCODFEPRD', 8 ),
        oEdtk::Field->new( 'HW-PRASCODASF', 6 ),
        oEdtk::Field->new( 'HW-PRASLIBCOU', 15 ),
        oEdtk::Field->new( 'HW-PRASLIBLON', 50 ),
        oEdtk::SignedField->new( 'HW-ACJIMPHCOT', 12, 2 ),
        oEdtk::SignedField->new( 'HW-ACJIMAHCOT', 12, 2 ),
        oEdtk::Field->new( 'HW-RESERVE-HW', 2363 ),
    );
}

1;
