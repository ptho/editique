package oEUser::Descriptor::PB;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # PB-ENREG
        oEdtk::Field->new( 'PB-PSDEIDFDOS', 10 ),
        oEdtk::Field->new( 'PB-PSDENUMDCP', 2 ),
        oEdtk::Field->new( 'PB-PSREDATEDT', 8 ),

        # PB-INF-CONTRAT
        oEdtk::Field->new( 'PB-ASPRCODCIV',   6 ),
        oEdtk::Field->new( 'PB-ASPRLIBCIV',   15 ),
        oEdtk::Field->new( 'PB-ASPRNOMADR',   50 ),
        oEdtk::Field->new( 'PB-ASPRPRNADR',   20 ),
        oEdtk::Field->new( 'PB-PSDPNUMCCO',   20 ),
        oEdtk::Field->new( 'PB-ACDPACGAR2',   8 ),
        oEdtk::Field->new( 'PB-PSZZ-NOM-GRA', 120 ),
        oEdtk::Field->new( 'PB-PSDENUMCIN',   20 ),
        oEdtk::Field->new( 'PB-ODGGCODGGS',   9 ),
        oEdtk::Field->new( 'PB-ODCGCODCGS',   9 ),

        # PB-ODCGLIBCGS
        oEdtk::Field->new( 'PB-PENOCODTIT',     10 ),
        oEdtk::Field->new( 'PB-PEZZ-LICCODTIT', 15 ),
        oEdtk::Field->new( 'PB-PENOLIBLON',     50 ),
        oEdtk::Field->new( 'PB-ODERCODERA',     9 ),

        # PB-ODERLIBERA
        oEdtk::Field->new( 'PB-PENOCODTIT-2',     10 ),
        oEdtk::Field->new( 'PB-PEZZ-LICCODTIT-2', 15 ),
        oEdtk::Field->new( 'PB-PENOLIBLON-2',     50 ),

        # PB-PSZZ-IDENT-ASS
        oEdtk::Field->new( 'PB-ASS-CODCIV', 6 ),
        oEdtk::Field->new( 'PB-ASS-LIBCIV', 15 ),
        oEdtk::Field->new( 'PB-PSDENOMASS', 50 ),
        oEdtk::Field->new( 'PB-PSDEPRNASS', 20 ),
        oEdtk::Field->new( 'PB-PSDEACASI2', 2 ),
        oEdtk::Field->new( 'PB-ASS-CODTAS', 6 ),
        oEdtk::Field->new( 'PB-ASS-LIBTAS', 50 ),
        oEdtk::Field->new( 'PB-PSDEDATNAI', 8 ),
        oEdtk::Field->new( 'PB-PSEPETENRO', 13 ),
        oEdtk::Field->new( 'PB-PSEOCLENRO', 2 ),
        oEdtk::Field->new( 'PB-CODTRT',     4 ),
        oEdtk::Field->new( 'PB-PSRLGESRIN', 1 ),
        oEdtk::Field->new( 'PB-PSDETYPDEM', 1 ),
        oEdtk::Field->new( 'PB-PSDEDATARR', 8 ),
        oEdtk::Field->new( 'PB-PSDENUMDEM', 8 ),
        oEdtk::Field->new( 'PB-PSDENUMDOS', 2 ),
        oEdtk::Field->new( 'PB-PSDENUMDC',  2 ),
        oEdtk::Field->new( 'PB-PSDENLTPSA', 8 ),
        oEdtk::Field->new( 'PB-PSDENUMFCU', 9 ),
        oEdtk::Field->new( 'PB-PSDEDATFCU', 8 ),
        oEdtk::Field->new( 'PB-PSDMREFEXT', 30 ),
        oEdtk::Field->new( 'PB-PSDEAUDUCR', 8 ),
        oEdtk::Field->new( 'PB-PSEPCODORI', 5 ),
        oEdtk::Field->new( 'PB-PSEPLIBORI', 50 ),
        oEdtk::SignedField->new( 'PB-PSDEMNTROT', 12, 2 ),
        oEdtk::SignedField->new( 'PB-PSDEMNTRCT', 12, 2 ),
        oEdtk::SignedField->new( 'PB-PSDECUMREM', 12, 2 ),
        oEdtk::Field->new( 'PB-PSEPDDESOI',     8 ),
        oEdtk::Field->new( 'PB-PSEPDFESOI',     8 ),
        oEdtk::Field->new( 'PB-PSZZMSG-DRC',    250 ),
        oEdtk::Field->new( 'PB-PSZZMSG-LIA',    250 ),
        oEdtk::Field->new( 'PB-PSEPCODFRN',     9 ),
        oEdtk::Field->new( 'PB-PSEPTYPFRN',     2 ),
        oEdtk::Field->new( 'PB-PENOCODCIV',     10 ),
        oEdtk::Field->new( 'PB-PENOLIBCIV',     15 ),
        oEdtk::Field->new( 'PB-PENOLIBLON-3',   50 ),
        oEdtk::Field->new( 'PB-PENOPRN01',      20 ),
        oEdtk::Field->new( 'PB-PSEPNUMETB',     9 ),
        oEdtk::Field->new( 'PB-PSEPN0METB',     50 ),
        oEdtk::Field->new( 'PB-PSLNCODDMT',     3 ),
        oEdtk::Field->new( 'PB-PSLNCODMTR',     2 ),
        oEdtk::Field->new( 'PB-PSEPCODNSS',     2 ),
        oEdtk::Field->new( 'PB-PSEPDATENT',     8 ),
        oEdtk::Field->new( 'PB-PSEPDATADT',     8 ),
        oEdtk::Field->new( 'PB-DECISIONPEC',    1 ),
        oEdtk::Field->new( 'PB-PSLNLIBDMT',     50 ),
        oEdtk::Field->new( 'PB-PSLNLIBMTR',     50 ),
        oEdtk::Field->new( 'PB-PSEPLIBNSS',     50 ),
        oEdtk::Field->new( 'PB-PSDENUMFCU-CPL', 6 ),
    );
}

1;
