package oEUser::Descriptor::P8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::DateField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # P8-EDEC
        oEdtk::Field->new( 'P8-PSRLIDFREL', 10 ),
        oEdtk::Field->new( 'P8-PSREDATEDT', 8 ),

        # P8-PSZZ-LST-CRIT-DRC
        oEdtk::DateField->new( 'P8-PSZZ-CONST-CRIT-DRC', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-2
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-2', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-3
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-3', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-4
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-4', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-5
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-5', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-6
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-6', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-7
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-7', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-8
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-8', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-9
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-9', 50 ),

        # P8-PSZZ-LST-CRIT-DRC-10
        oEdtk::Field->new( 'P8-PSZZ-CONST-CRIT-DRC-10', 50 ),

        # P8MJ-INF-RIB
        oEdtk::Field->new( 'P8-DCRICPTTIT', 50 ),
        oEdtk::Field->new( 'P8-DCRICODBQE', 10 ),
        oEdtk::Field->new( 'P8-DCRICODGUI', 10 ),
        oEdtk::Field->new( 'P8-DCRINUMCPT', 20 ),
        oEdtk::Field->new( 'P8-DCRICLECPT', 2 ),

        # P8MJ-INF-PAIEMENT
        oEdtk::DateField->new( 'P8-DCPHDATPAI', 8 ),
        oEdtk::Field->new( 'P8-DCPHNUMPIE',       8 ),
        oEdtk::Field->new( 'P8-DCADCODCIV',       6 ),
        oEdtk::Field->new( 'P8-DCADLIBCIV',       15 ),
        oEdtk::Field->new( 'P8-DCADNOMADR',       50 ),
        oEdtk::Field->new( 'P8-DCADPRNADR',       20 ),
        oEdtk::Field->new( 'P8-PSZZ-CODFRS-PAIE', 9 ),
        oEdtk::Field->new( 'P8-PSZZ-TYPFRS-PAI',  2 ),
        oEdtk::SignedField->new( 'P8-DCPHMNTPAI', 12, 2 ),
        oEdtk::SignedField->new( 'P8-DCRCMNTRCP', 12, 2 ),
        oEdtk::Field->new( 'P8-DCZZ-NBRECUP', 4 ),
        oEdtk::Field->new( 'P8-DCPHNUMCHQ',   8 ),

        # P8MJ-INF-CONTRAT
        oEdtk::Field->new( 'P8-ASPRCODCIV',   6 ),
        oEdtk::Field->new( 'P8-ASPRLIBCIV',   15 ),
        oEdtk::Field->new( 'P8-ASPRNOMADR',   50 ),
        oEdtk::Field->new( 'P8-ASPRPRNADR',   20 ),
        oEdtk::Field->new( 'P8-PSDPNUMCCO',   20 ),
        oEdtk::Field->new( 'P8-ACDPACGAR2',   8 ),
        oEdtk::Field->new( 'P8-PSZZ-NOM-GRA', 120 ),
        oEdtk::Field->new( 'P8-PSDENUMCIN',   20 ),
        oEdtk::Field->new( 'P8-PSRLCODRGD',   5 ),
        oEdtk::Field->new( 'P8-ODCGCODCGS',   9 ),

        # P8-ODCGLIBCGS
        oEdtk::Field->new( 'P8-PENOCODTIT',     10 ),
        oEdtk::Field->new( 'P8-PEZZ-LICCODTIT', 15 ),
        oEdtk::Field->new( 'P8-PENOLIBLON',     50 ),
        oEdtk::Field->new( 'P8-ODERCODERA',     9 ),

        # P8-ODERLIBERA
        oEdtk::Field->new( 'P8-PENOCODTIT-2',     10 ),
        oEdtk::Field->new( 'P8-PEZZ-LICCODTIT-2', 15 ),
        oEdtk::Field->new( 'P8-PENOLIBLON-2',     50 ),

        # P8MJ-PSZZ-IDENT-ASS
        oEdtk::Field->new( 'P8-ASS-CODCIV', 6 ),
        oEdtk::Field->new( 'P8-ASS-LIBCIV', 15 ),
        oEdtk::Field->new( 'P8-PSDENOMASS', 50 ),
        oEdtk::Field->new( 'P8-PSDEPRNASS', 20 ),
        oEdtk::Field->new( 'P8-PSDEACASI2', 2 ),
        oEdtk::Field->new( 'P8-ASS-CODTAS', 6 ),
        oEdtk::Field->new( 'P8-ASS-LIBTAS', 50 ),
        oEdtk::DateField->new( 'P8-PSDEDATNAI', 8 ),
        oEdtk::Field->new( 'P8-PSEPETENRO', 13 ),
        oEdtk::Field->new( 'P8-PSEOCLENRO', 2 ),

        # P8MJ-INF-DRC
        oEdtk::Field->new( 'P8-PSRLGESRIN', 1 ),
        oEdtk::Field->new( 'P8-PSDETYPDEM', 1 ),
        oEdtk::Field->new( 'P8-PSDEDATARR', 8 ),
        oEdtk::Field->new( 'P8-PSDENUMDEM', 8 ),
        oEdtk::Field->new( 'P8-PSDENUMDOS', 2 ),
        oEdtk::Field->new( 'P8-PSDENUMDCP', 2 ),
        oEdtk::Field->new( 'P8-PSDENLTPSA', 8 ),
        oEdtk::Field->new( 'P8-PSDENUMFCU', 9 ),
        oEdtk::Field->new( 'P8-PSDEDATFCU', 8 ),
        oEdtk::Field->new( 'P8-PSDMREFEXT', 30 ),
        oEdtk::Field->new( 'P8-PSDEAUDUCR', 8 ),
        oEdtk::Field->new( 'P8-PSEPCODORI', 5 ),
        oEdtk::Field->new( 'P8-PSEPLIBORI', 50 ),
        oEdtk::SignedField->new( 'P8-PSDEMNTROT', 12, 2 ),
        oEdtk::SignedField->new( 'P8-PSDEMNTRCT', 12, 2 ),
        oEdtk::SignedField->new( 'P8-PSDECUMREM', 12, 2 ),
        oEdtk::Field->new( 'P8-PSEPDDESOI', 8 ),
        oEdtk::DateField->new( 'P8-PSEPDFESOI', 8 ),
        oEdtk::Field->new( 'P8-PSZZMSG-DRC',  250 ),
        oEdtk::Field->new( 'P8-PSZZMSG-LIA',  250 ),
        oEdtk::Field->new( 'P8-PSEPCODFRN',   9 ),
        oEdtk::Field->new( 'P8-PSEPTYPFRN',   2 ),
        oEdtk::Field->new( 'P8-PENOCODCIV',   10 ),
        oEdtk::Field->new( 'P8-PENOLIBCIV',   15 ),
        oEdtk::Field->new( 'P8-PENOLIBLON-3', 50 ),
        oEdtk::Field->new( 'P8-PENOPRN01',    20 ),
        oEdtk::Field->new( 'P8-PSEPNUMETB',   9 ),
        oEdtk::Field->new( 'P8-PSEPN0METB',   50 ),

        # P8MJ-INF-DRC-RO
        oEdtk::Field->new( 'P8-PSRLCODGRE',     2 ),
        oEdtk::Field->new( 'P8-PSRLCODCRO',     3 ),
        oEdtk::Field->new( 'P8-PSRLCODCPA',     4 ),
        oEdtk::Field->new( 'P8-PSEODATCPB',     8 ),
        oEdtk::Field->new( 'P8-PSRLCODGGS',     9 ),
        oEdtk::Field->new( 'P8-CODNSS',         2 ),
        oEdtk::Field->new( 'P8-LIBCOU-CODNSS',  15 ),
        oEdtk::Field->new( 'P8-LIBLON-CODNSS',  50 ),
        oEdtk::Field->new( 'P8-AVANCE-RO',      1 ),
        oEdtk::Field->new( 'P8-PSDENUMFCU-CPL', 6 ),
        oEdtk::Field->new( 'P8-FILLER-P8',      165 ),
    );
}

1;
