package oEUser::Descriptor::C3;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # C3-ZZEDIT-C3
        # C3-ZZEDIT-ADRESSE-DEST // Adresse du destinataire du document
        oEdtk::Field->new( 'C3-PNTREM-DD',       50 ),
        oEdtk::Field->new( 'C3-CPLADR-DD',       50 ),
        oEdtk::Field->new( 'C3-PEADNUMVO-DD',    9 ),
        oEdtk::Field->new( 'C3-PEADBTQVOI-DD',   2 ),
        oEdtk::Field->new( 'C3-PELICBTQVOI-DD',  15 ),
        oEdtk::Field->new( 'C3-LIBVOI-DD',       50 ),
        oEdtk::Field->new( 'C3-INDCDX-DD',       1 ),
        oEdtk::Field->new( 'C3-BOIPOSLIBLDT-DD', 50 ),
        oEdtk::Field->new( 'C3-LIBLOCLIBLOC-DD', 50 ),
        oEdtk::Field->new( 'C3-CODCDXCODPOS-DD', 10 ),
        oEdtk::Field->new( 'C3-NOMCDXLIBACH-DD', 50 ),
        oEdtk::Field->new( 'C3-PEPACODPAY-DD',   3 ),
        oEdtk::Field->new( 'C3-PELICCODPAY-DD',  15 ),
        oEdtk::Field->new( 'C3-TRNFAC-DD',       4 ),

        # C3-ZZEDIT-ADRESSE-ER // Adresse de l'entité de rattachement
        oEdtk::Field->new( 'C3-PNTREM-ER',       50 ),
        oEdtk::Field->new( 'C3-CPLADR-ER',       50 ),
        oEdtk::Field->new( 'C3-PEADNUMVOI-ER',   9 ),
        oEdtk::Field->new( 'C3-PEADBTQVOI-ER',   2 ),
        oEdtk::Field->new( 'C3-PELICBTQVOI-ER',  15 ),
        oEdtk::Field->new( 'C3-LIBVOI-ER',       50 ),
        oEdtk::Field->new( 'C3-INDCDX-ER',       1 ),
        oEdtk::Field->new( 'C3-BOIPOSLIBLDT-ER', 50 ),
        oEdtk::Field->new( 'C3-LIBLOCLIBLOC-ER', 50 ),
        oEdtk::Field->new( 'C3-CODCDXCODPOS-ER', 10 ),
        oEdtk::Field->new( 'C3-NOMCDXLIBACH-ER', 50 ),
        oEdtk::Field->new( 'C3-PEPACODPAY-ER',   3 ),
        oEdtk::Field->new( 'C3-PELICCODPAY-ER',  15 ),
        oEdtk::Field->new( 'C3-TRNFAC-ER',       4 ),

        # C3-ZZEDIT-ADRESSE-CG // Adresse du centre de gestion
        oEdtk::Field->new( 'C3-PNTREM-CG',       50 ),
        oEdtk::Field->new( 'C3-CPLADR-CG',       50 ),
        oEdtk::Field->new( 'C3-PEADNUMVOI-CG',   9 ),
        oEdtk::Field->new( 'C3-PEADBTQVOI-CG',   2 ),
        oEdtk::Field->new( 'C3-PELICBTQVOI-CG',  15 ),
        oEdtk::Field->new( 'C3-LIBVOI-CG',       50 ),
        oEdtk::Field->new( 'C3-INDCDX-CG',       1 ),
        oEdtk::Field->new( 'C3-BOIPOSLIBLDT-CG', 50 ),
        oEdtk::Field->new( 'C3-LIBLOCLIBLOC-CG', 50 ),
        oEdtk::Field->new( 'C3-CODCDXCODPOS-CG', 10 ),
        oEdtk::Field->new( 'C3-NOMCDXLIBACH-CG', 50 ),
        oEdtk::Field->new( 'C3-PEPACODPAY-CG',   3 ),
        oEdtk::Field->new( 'C3-PELICCODPAY-CG',  15 ),
        oEdtk::Field->new( 'C3-TRNFAC-CG',       4 ),

        # C3-ZZEDIT-ADRESSE-PU // Adresse du producteur
        oEdtk::Field->new( 'C3-PNTREM-PU',       50 ),
        oEdtk::Field->new( 'C3-CPLADR-PU',       50 ),
        oEdtk::Field->new( 'C3-PEADNUMVOI-PU',   9 ),
        oEdtk::Field->new( 'C3-PEADBTQVOI-PU',   2 ),
        oEdtk::Field->new( 'C3-PELICBTQVOI-PU',  15 ),
        oEdtk::Field->new( 'C3-LIBVOI-PU',       50 ),
        oEdtk::Field->new( 'C3-INDCDX-PU',       1 ),
        oEdtk::Field->new( 'C3-BOIPOSLIBLDT-PU', 50 ),
        oEdtk::Field->new( 'C3-LIBLOCLIBLOC-PU', 50 ),
        oEdtk::Field->new( 'C3-CODCDXCODPOS-PU', 10 ),
        oEdtk::Field->new( 'C3-NOMCDXLIBACH-PU', 50 ),
        oEdtk::Field->new( 'C3-PEPACODPAY-PU',   3 ),
        oEdtk::Field->new( 'C3-PELICCODPAY-PU',  15 ),
        oEdtk::Field->new( 'C3-TRNFAC-PU',       4 ),

        # C3-ZZEDIT-ADRESSE-SO // Adresse du souscripteur
        oEdtk::Field->new( 'C3-PNTREM-SO',       50 ),
        oEdtk::Field->new( 'C3-CPLADR-SO',       50 ),
        oEdtk::Field->new( 'C3-PEADNUMVOI-SO',   9 ),
        oEdtk::Field->new( 'C3-PEADBTQVOI-SO',   2 ),
        oEdtk::Field->new( 'C3-PELICBTQVOI-SO',  15 ),
        oEdtk::Field->new( 'C3-LIBVOI-SO',       50 ),
        oEdtk::Field->new( 'C3-INDCDX-SO',       1 ),
        oEdtk::Field->new( 'C3-BOIPOSLIBLDT-SO', 50 ),
        oEdtk::Field->new( 'C3-LIBLOCLIBLOC-SO', 50 ),
        oEdtk::Field->new( 'C3-CODCDXCODPOS-SO', 10 ),
        oEdtk::Field->new( 'C3-NOMCDXLIBACH-SO', 50 ),
        oEdtk::Field->new( 'C3-PEPACODPAY-SO',   3 ),
        oEdtk::Field->new( 'C3-PELICCODPAY-SO',  15 ),
        oEdtk::Field->new( 'C3-TRNFAC-SO',       4 ),

        # C3-ZZEDIT-ADRESSE-AG // Adresse de l'agence
        oEdtk::Field->new( 'C3-PNTREM-AG',       50 ),
        oEdtk::Field->new( 'C3-CPLADR-AG',       50 ),
        oEdtk::Field->new( 'C3-PEADNUMVOI-AG',   9 ),
        oEdtk::Field->new( 'C3-PEADBTQVOI-AG',   2 ),
        oEdtk::Field->new( 'C3-PELICBTQVOI-AG',  15 ),
        oEdtk::Field->new( 'C3-LIBVOI-AG',       50 ),
        oEdtk::Field->new( 'C3-INDCDX-AG',       1 ),
        oEdtk::Field->new( 'C3-BOIPOSLIBLDT-AG', 50 ),
        oEdtk::Field->new( 'C3-LIBLOCLIBLOC-AG', 50 ),
        oEdtk::Field->new( 'C3-CODCDXCODPOS-AG', 10 ),
        oEdtk::Field->new( 'C3-NOMCDXLIBACH-AG', 50 ),
        oEdtk::Field->new( 'C3-PEPACODPAY-AG',   3 ),
        oEdtk::Field->new( 'C3-PELICCODPAY-AG',  15 ),
        oEdtk::Field->new( 'C3-TRNFAC-AG',       4 ),
    );
}

1;
