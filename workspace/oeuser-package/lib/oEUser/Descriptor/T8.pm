package oEUser::Descriptor::T8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CE0011C-T8-LGNDAT
        # CE0011C-T8-IDENT-PART
        oEdtk::Field->new( 'CE0011C-T8-NUMPART',         5 ),
        oEdtk::Field->new( 'CE0011C-T8-CODPART',         3 ),
        oEdtk::Field->new( 'CE0011C-T8-ECHE-PRINCIPALE', 4 ),

        # CE0011C-T8-ECHEANCIER-CALCULE
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-01', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-01',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-01', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-02', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-02',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-02', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-03', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-03',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-03', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-04', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-04',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-04', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-05', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-05',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-05', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-06', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-06',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-06', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-07', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-07',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-07', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-08', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-08',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-08', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-09', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-09',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-09', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-10', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-10',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-10', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-11', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-11',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-11', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-12', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-12',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-12', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-13', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-13',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-13', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-14', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-14',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-14', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-CALC-15', 6 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-CALC-15',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-CALC-15', 12, 2 ),

        # CE0011C-T8-ECHEANCIER-THEO
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-01', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-01',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-01', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-02', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-02',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-02', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-03', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-03',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-03', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-04', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-04',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-04', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-05', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-05',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-05', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-06', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-06',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-06', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-07', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-07',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-07', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-08', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-08',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-08', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-09', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-09',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-09', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-10', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-10',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-10', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-11', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-11',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-11', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-12', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-12',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-12', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-13', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-13',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-13', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-14', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-14',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-14', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-MOIS-ECH-THEO-15', 8 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-HT-THEO-15',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TTC-THEO-15', 12, 2 ),

        # CE0011C-T8-MNT-TOTAL
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TOTAL-HT',  12, 2 ),
        oEdtk::SignedField->new( 'CE0011C-T8-MNT-TOTAL-TTC', 12, 2 ),
        oEdtk::Field->new( 'CE0011C-T8-FRQCE-REGLT', 2 ),
    );
}

1;
