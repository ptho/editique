package oEUser::Descriptor::EC;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::DateField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # EC-LGNDAT
        oEdtk::DateField->new( 'EC-ECD1DATECH', 8 ),
        oEdtk::Field->new( 'EC-ECD1CODNFI', 8 ),
        oEdtk::Field->new( 'EC-LIB-NATFIN', 50 ),
        oEdtk::SignedField->new( 'EC-ECD1MTTECH', 12, 2 ),
        oEdtk::SignedField->new( 'EC-ECD1MTTTAX', 12, 2 ),
        oEdtk::SignedField->new( 'EC-ECD1MTTENC', 12, 2 ),
        oEdtk::SignedField->new( 'EC-ECZZ-SOLDE', 12, 2 ),
    );
}

1;
