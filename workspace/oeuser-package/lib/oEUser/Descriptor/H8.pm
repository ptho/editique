package oEUser::Descriptor::H8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H8-LGNDAT
        oEdtk::Field->new( 'H8-ACSTACASR2', 2 ),
        oEdtk::Field->new( 'H8-ACH4CODGAR', 10 ),
        oEdtk::Field->new( 'H8-ACH4CODPRT', 10 ),
        oEdtk::Field->new( 'H8-ACSTACGSR3', 5 ),
        oEdtk::Field->new( 'H8-ACSTACGSR4', 5 ),

        # H8-TAB-STAGES
        # H8-ELT-STAGES
        oEdtk::Field->new( 'H8-ACSTCODSTA',        6 ),
        oEdtk::Field->new( 'H8-LIBCOU-ACSTCODSTA', 15 ),
        oEdtk::Field->new( 'H8-ACSTDURDRG',        2 ),
        oEdtk::Field->new( 'H8-ACSTDFESTA',        8 ),

        # H8-TAB-STAGES-1
        # H8-STAGE-1
        oEdtk::Field->new( 'H8-ACSTCODSTA-1',        6 ),
        oEdtk::Field->new( 'H8-LIBCOU-ACSTCODSTA-1', 15 ),
        oEdtk::Field->new( 'H8-ACSTDURDRG-1',        2 ),
        oEdtk::Field->new( 'H8-ACSTDFESTA-1',        8 ),

        # H8-STAGE-2
        oEdtk::Field->new( 'H8-ACSTCODSTA-2',        6 ),
        oEdtk::Field->new( 'H8-LIBCOU-ACSTCODSTA-2', 15 ),
        oEdtk::Field->new( 'H8-ACSTDURDRG-2',        2 ),
        oEdtk::Field->new( 'H8-ACSTDFESTA-2',        8 ),

        # H8-STAGE-3
        oEdtk::Field->new( 'H8-ACSTCODSTA-3',        6 ),
        oEdtk::Field->new( 'H8-LIBCOU-ACSTCODSTA-3', 15 ),
        oEdtk::Field->new( 'H8-ACSTDURDRG-3',        2 ),
        oEdtk::Field->new( 'H8-ACSTDFESTA-3',        8 ),

        # H8-STAGE-4
        oEdtk::Field->new( 'H8-ACSTCODSTA-4',        6 ),
        oEdtk::Field->new( 'H8-LIBCOU-ACSTCODSTA-4', 15 ),
        oEdtk::Field->new( 'H8-ACSTDURDRG-4',        2 ),
        oEdtk::Field->new( 'H8-ACSTDFESTA-4',        8 ),

        # H8-STAGE-5
        oEdtk::Field->new( 'H8-ACSTCODSTA-5',        6 ),
        oEdtk::Field->new( 'H8-LIBCOU-ACSTCODSTA-5', 15 ),
        oEdtk::Field->new( 'H8-ACSTDURDRG-5',        2 ),
        oEdtk::Field->new( 'H8-ACSTDFESTA-5',        8 ),
        oEdtk::Field->new( 'H8-RESERVE-H8',          200 ),
    );
}

1;
