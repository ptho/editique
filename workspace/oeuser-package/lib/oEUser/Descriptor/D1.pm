package oEUser::Descriptor::D1;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::Field;
use oEdtk::SignedField;
use oEdtk::SignedField;

sub get {
    return oEdtk::Record->new(

        # D1-DPLGNDAT-ENREG
        # D1-LGNDAT
        # D1-RELEVE
        oEdtk::SignedField->new( 'D1-NUMRLV', 9 ),
        oEdtk::Field->new( 'D1-DATE-TRT', 8 ),
        oEdtk::SignedField->new( 'D1-NBPAGE', 9 ),
        oEdtk::Field->new( 'D1-TYPIMP', 2 ),

        # D1-DEST
        oEdtk::Field->new( 'D1-DCCDTYPDST', 2 ),
        oEdtk::Field->new( 'D1-TYPCCI',     1 ),
        oEdtk::Field->new( 'D1-ACGACODGRA', 6 ),
        oEdtk::Field->new( 'D1-ACCINUMCIN', 15 ),
        oEdtk::Field->new( 'D1-ODGGCODGGS', 9 ),
        oEdtk::Field->new( 'D1-CODSIT',     2 ),
        oEdtk::Field->new( 'D1-NUMRO-CLE',  15 ),
        oEdtk::Field->new( 'D1-ENFNTYPFRN', 2 ),
        oEdtk::Field->new( 'D1-ENFNCODFRN', 9 ),
        oEdtk::Field->new( 'D1-ENFNNOMFRN', 50 ),
        oEdtk::Field->new( 'D1-ENFNCODRGF', 4 ),
        oEdtk::Field->new( 'D1-ENFNLIBRGF', 50 ),

        # D1-RIB
        oEdtk::Field->new( 'D1-PERICODBQE', 10 ),
        oEdtk::Field->new( 'D1-PERICODGUI', 10 ),
        oEdtk::Field->new( 'D1-ENBANOMEBC', 40 ),
        oEdtk::Field->new( 'D1-PERINUMCPT', 20 ),
        oEdtk::Field->new( 'D1-PERICLECPT', 2 ),

        # D1-PAIEMENT
        oEdtk::Field->new( 'D1-DCPHDATPAI', 8 ),
        oEdtk::SignedField->new( 'D1-DCPAIDFSYS', 10 ),
        oEdtk::SignedField->new( 'D1-DCPHMNTPAI', 12, 2 ),
        oEdtk::Field->new( 'D1-MNT-LETTRE', 300 ),

        # D1-ADR-DEST
        oEdtk::Field->new( 'D1-DCADCODCIV', 6 ),
        oEdtk::Field->new( 'D1-LIBCODCIV',  50 ),
        oEdtk::Field->new( 'D1-DCADNOMADR', 50 ),
        oEdtk::Field->new( 'D1-DCADPRNADR', 20 ),
        oEdtk::Field->new( 'D1-DCADNUMVOI', 9 ),
        oEdtk::Field->new( 'D1-DCADBTQVOI', 1 ),
        oEdtk::Field->new( 'D1-LIBBTQVOI',  50 ),
        oEdtk::Field->new( 'D1-DCADNATVOI', 4 ),
        oEdtk::Field->new( 'D1-LIBNATVOI',  50 ),
        oEdtk::Field->new( 'D1-DCADLIBVOI', 50 ),
        oEdtk::Field->new( 'D1-DCADLIBLOC', 50 ),
        oEdtk::Field->new( 'D1-DCADCODPOS', 10 ),

        # D1-CONTRAT
        oEdtk::Field->new( 'D1-PENOCODCIV',     6 ),
        oEdtk::Field->new( 'D1-CNT-LIBCODCIV',  50 ),
        oEdtk::Field->new( 'D1-PENOLIBLON',     50 ),
        oEdtk::Field->new( 'D1-PENOPRN01',      20 ),
        oEdtk::Field->new( 'D1-PECPSEEPER',     13 ),
        oEdtk::Field->new( 'D1-PECPCLESSE',     2 ),
        oEdtk::Field->new( 'D1-CNT-TYPCCI',     1 ),
        oEdtk::Field->new( 'D1-CNT-CODGRA',     6 ),
        oEdtk::Field->new( 'D1-ACGALIBGRA',     50 ),
        oEdtk::Field->new( 'D1-CNT-ACCINUMCIN', 15 ),
        oEdtk::Field->new( 'D1-CNT-CODGGS',     9 ),
        oEdtk::Field->new( 'D1-ODGGLIBLON',     50 ),
        oEdtk::Field->new( 'D1-CNT-CODCGS',     9 ),
        oEdtk::Field->new( 'D1-ODCGLIBLON',     50 ),
        oEdtk::Field->new( 'D1-CNT-CODERA',     9 ),
        oEdtk::Field->new( 'D1-LIBERA',         50 ),

        # D1-RIB de paiement en SEPA
        oEdtk::Field->new( 'D1-DCRICODPA2', 2 ),
        oEdtk::Field->new( 'D1-DCRICLEIBA', 4 ),
        oEdtk::Field->new( 'D1-DCRIIDTNAT', 30 ),
        oEdtk::Field->new( 'D1-DCRICODBIC', 14 ),

        # D1-ADRESSE-NORMALISE-DEST 			Adresse normalisée du destinataire
        oEdtk::Field->new( 'D1-PNTREM',       50 ),
        oEdtk::Field->new( 'D1-CPLADR',       50 ),
        oEdtk::Field->new( 'D1-PEADNUMVO',    9 ),
        oEdtk::Field->new( 'D1-PEADBTQVOI',   2 ),
        oEdtk::Field->new( 'D1-PELICBTQVOI',  15 ),
        oEdtk::Field->new( 'D1-LIBVOI',       50 ),
        oEdtk::Field->new( 'D1-INDCDX',       1 ),
        oEdtk::Field->new( 'D1-BOIPOSLIBLDT', 50 ),
        oEdtk::Field->new( 'D1-LIBLOCLIBLOC', 50 ),
        oEdtk::Field->new( 'D1-CODCDXCODPOS', 10 ),
        oEdtk::Field->new( 'D1-NOMCDXLIBACH', 50 ),
        oEdtk::Field->new( 'D1-PEPACODPAY',   3 ),
        oEdtk::Field->new( 'D1-PELICCODPAY',  15 ),
        oEdtk::Field->new( 'D1-TRNFAC',       4 ),
    );
}

1;
