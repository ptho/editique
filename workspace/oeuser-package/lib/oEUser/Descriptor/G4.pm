package oEUser::Descriptor::G4;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # G4-LGNDAT
        # G4-INFO-GARANTIE-COLLECTIVE
        oEdtk::Field->new( 'G4-ACH9ACGCR2',        6 ),
        oEdtk::Field->new( 'G4-ACH9CODGAR',        10 ),
        oEdtk::Field->new( 'G4-PRGALIBCOU',        15 ),
        oEdtk::Field->new( 'G4-PRGALIBLON',        50 ),
        oEdtk::Field->new( 'G4-PRGPTYPGAR',        1 ),
        oEdtk::Field->new( 'G4-LIBCOU-PRGPTYPGAR', 15 ),
        oEdtk::Field->new( 'G4-ACH9CODPRT',        10 ),
        oEdtk::Field->new( 'G4-PRPRLIBCOU',        15 ),
        oEdtk::Field->new( 'G4-PRPRLIBLON',        50 ),
        oEdtk::Field->new( 'G4-ACH9DDEADH',        8 ),
        oEdtk::Field->new( 'G4-ACH9DDERAD',        8 ),
        oEdtk::Field->new( 'G4-ACOPDDEOPE',        8 ),
        oEdtk::Field->new( 'G4-ACH9DDECOT',        8 ),
        oEdtk::Field->new( 'G4-ACH9DFECOT',        8 ),
        oEdtk::Field->new( 'G4-ACH9DATVTE',        8 ),
        oEdtk::Field->new( 'G4-ACH9CODFRI',        6 ),
        oEdtk::Field->new( 'G4-LIBCOU-ACH9CODFRI', 15 ),
        oEdtk::Field->new( 'G4-LIBLON-ACH9CODFRI', 50 ),
        oEdtk::Field->new( 'G4-ACH9ACGCR3',        5 ),
        oEdtk::Field->new( 'G4-ACH9ACGCR4',        5 ),
        oEdtk::Field->new( 'G4-CODE-NAT-PREST',    1 ),
        oEdtk::Field->new( 'G4-LIBCOU-NAT-PREST',  15 ),
        oEdtk::Field->new( 'G4-LIBLON-NAT-PREST',  50 ),
        oEdtk::Field->new( 'G4-RESERVE-G4',        200 ),
    );
}

1;
