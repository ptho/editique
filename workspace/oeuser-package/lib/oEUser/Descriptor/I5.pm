package oEUser::Descriptor::I5;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # I5-ENREG
        # I5-LIGNE-CRIT-SELECT
        oEdtk::Field->new( 'I5-SITE',          2 ),
        oEdtk::Field->new( 'I5-CODERA',        9 ),
        oEdtk::Field->new( 'I5-NUMCCO',        15 ),
        oEdtk::Field->new( 'I5-GRPASS',        6 ),
        oEdtk::Field->new( 'I5-NUMCIN',        15 ),
        oEdtk::Field->new( 'I5-NUMPART',       5 ),
        oEdtk::Field->new( 'I5-CODPART',       3 ),
        oEdtk::Field->new( 'I5-DATECH-DEB',    8 ),
        oEdtk::Field->new( 'I5-DATECH-FIN',    8 ),
        oEdtk::Field->new( 'I5-DATENC-DEB',    8 ),
        oEdtk::Field->new( 'I5-DATENC-FIN',    8 ),
        oEdtk::Field->new( 'I5-MODSEL-DATENC', 2 ),
        oEdtk::Field->new( 'I5-CODUTIL',       8 ),
    );
}

1;
