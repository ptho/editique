package oEUser::Descriptor::HP;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # HP-LGNDAT
        # HP-INFO-GESTCPL
        oEdtk::Field->new( 'HP-ACH2ACASR2',        2 ),
        oEdtk::Field->new( 'HP-ACH2CODSPR',        10 ),
        oEdtk::Field->new( 'HP-LIBCOU-ACH2CODSPR', 15 ),
        oEdtk::Field->new( 'HP-LIBLON-ACH2CODSPR', 50 ),
        oEdtk::Field->new( 'HP-ACH2CODPAR',        3 ),
        oEdtk::Field->new( 'HP-LIBCOU-ACH2CODPAR', 15 ),
        oEdtk::Field->new( 'HP-LIBLON-ACH2CODPAR', 50 ),
        oEdtk::Field->new( 'HP-ACH2CODPRR',        6 ),
        oEdtk::Field->new( 'HP-LIBCOU-ACH2CODPRR', 15 ),
        oEdtk::Field->new( 'HP-LIBLON-ACH2CODPRR', 50 ),
        oEdtk::SignedField->new( 'HP-MNT-GESTCPL1', 12, 2 ),
        oEdtk::SignedField->new( 'HP-KC-GESTCPL1',  12, 2 ),
        oEdtk::SignedField->new( 'HP-MNT-GESTCPL2', 12, 2 ),
        oEdtk::SignedField->new( 'HP-KC-GESTCPL2',  12, 2 ),
        oEdtk::SignedField->new( 'HP-MNT-GESTCPL3', 12, 2 ),
        oEdtk::SignedField->new( 'HP-KC-GESTCPL3',  12, 2 ),
        oEdtk::SignedField->new( 'HP-MNT-GESTCPL4', 12, 2 ),
        oEdtk::SignedField->new( 'HP-KC-GESTCPL4',  12, 2 ),
        oEdtk::SignedField->new( 'HP-MNT-GESTCPL5', 12, 2 ),
        oEdtk::SignedField->new( 'HP-KC-GESTCPL5',  12, 2 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC1',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC1',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC1',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC1',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC2',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC2',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC2',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC2',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC3',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC3',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC3',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC3',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC4',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC4',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC4',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC4',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC5',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC5',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC5',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC5',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC6',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC6',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC6',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC6',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC7',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC7',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC7',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC7',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC8',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC8',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC8',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC8',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC9',    2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC9',    15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC9',    50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC9',    1 ),
        oEdtk::Field->new( 'HP-ACDSCODDEC10',   2 ),
        oEdtk::Field->new( 'HP-ACDSLBCDEC10',   15 ),
        oEdtk::Field->new( 'HP-ACDSLIBDEC10',   50 ),
        oEdtk::Field->new( 'HP-ACDSVALDEC10',   1 ),
        oEdtk::Field->new( 'HP-COD-DOCJUSTIF1', 6 ),
        oEdtk::Field->new( 'HP-LBC-DOCJUSTIF1', 15 ),
        oEdtk::Field->new( 'HP-LIB-DOCJUSTIF1', 50 ),
        oEdtk::Field->new( 'HP-COD-DOCJUSTIF2', 6 ),
        oEdtk::Field->new( 'HP-LBC-DOCJUSTIF2', 15 ),
        oEdtk::Field->new( 'HP-LIB-DOCJUSTIF2', 50 ),
        oEdtk::Field->new( 'HP-COD-DOCJUSTIF3', 6 ),
        oEdtk::Field->new( 'HP-LBC-DOCJUSTIF3', 15 ),
        oEdtk::Field->new( 'HP-LIB-DOCJUSTIF3', 50 ),
        oEdtk::Field->new( 'HP-COD-DOCJUSTIF4', 6 ),
        oEdtk::Field->new( 'HP-LBC-DOCJUSTIF4', 15 ),
        oEdtk::Field->new( 'HP-LIB-DOCJUSTIF4', 50 ),
        oEdtk::Field->new( 'HP-COD-DOCJUSTIF5', 6 ),
        oEdtk::Field->new( 'HP-LBC-DOCJUSTIF5', 15 ),
        oEdtk::Field->new( 'HP-LIB-DOCJUSTIF5', 50 ),
    );
}

1;
