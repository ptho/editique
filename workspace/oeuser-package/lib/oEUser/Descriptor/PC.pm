package oEUser::Descriptor::PC;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # PR0012MJ-PC
        oEdtk::Field->new( 'PC-PSDEIDFDOS', 10 ),
        oEdtk::Field->new( 'PC-PSDENUMDCP', 2 ),
        oEdtk::Field->new( 'PC-PSREDATEDT', 8 ),

        # PC-INF-LIGDRC
        oEdtk::Field->new( 'PC-PSLDNUMLGP', 5 ),
        oEdtk::Field->new( 'PC-PSLDCODARC', 5 ),
        oEdtk::Field->new( 'PC-PSZZLIBARC', 50 ),
        oEdtk::Field->new( 'PC-PSLDQTERTU', 4 ),
        oEdtk::SignedField->new( 'PC-PSLDCOERTU', 4,  5 ),
        oEdtk::SignedField->new( 'PC-PSLDMNTPRU', 12, 2 ),
        oEdtk::SignedField->new( 'PC-PSLDMNTDEP', 12, 2 ),
        oEdtk::SignedField->new( 'PC-PSLOVALTXO', 4,  5 ),
        oEdtk::SignedField->new( 'PC-PSLOVALTXC', 4,  5 ),
        oEdtk::SignedField->new( 'PC-PSLDMNTROR', 12, 2 ),
        oEdtk::SignedField->new( 'PC-PSLDMNTRCR', 12, 2 ),
        oEdtk::Field->new( 'PC-PSLPDATPCP', 8 ),
        oEdtk::Field->new( 'PC-PSLPDDESOI', 8 ),
        oEdtk::Field->new( 'PC-PSLPDFESOI', 8 ),
        oEdtk::Field->new( 'PC-PSZZCODGAR', 10 ),
        oEdtk::Field->new( 'PC-PSZZLIBGAR', 50 ),
        oEdtk::Field->new( 'PC-PSLDCODGEL', 5 ),
        oEdtk::Field->new( 'PC-PSZZLIBGEL', 50 ),
        oEdtk::Field->new( 'PC-PSLDCODGAE', 2 ),
        oEdtk::SignedField->new( 'PC-PSLORRUMGE', 12, 2 ),
        oEdtk::SignedField->new( 'PC-PSLORCXMGE', 12, 2 ),
        oEdtk::SignedField->new( 'PC-PSLDMNTRCD', 12, 2 ),
        oEdtk::Field->new( 'PC-PSZZMSG-LD', 250 ),
        oEdtk::Field->new( 'PC-PSLNNUMEXA', 9 ),
        oEdtk::Field->new( 'PC-PSLNNOMEXA', 50 ),
        oEdtk::Field->new( 'PC-PSEVCODELI', 2 ),
        oEdtk::SignedField->new( 'PC-PSZZMNTUNI', 12, 2 ),
        oEdtk::Field->new( 'PR00012MJ-PSLNLCAD01', 2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD02',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD03',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD04',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD05',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD06',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD07',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD08',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD09',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD10',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD11',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD12',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD13',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD14',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD15',        2 ),
        oEdtk::Field->new( 'PC-PSLNLCAD16',        2 ),
        oEdtk::Field->new( 'PC-ACH4CODPRT',        10 ),
        oEdtk::Field->new( 'PC-PRPRLIBLON',        50 ),
        oEdtk::Field->new( 'PC-PSLNMATERI',        5 ),
        oEdtk::Field->new( 'PC-MATR-COLO1',        15 ),
        oEdtk::Field->new( 'PC-MATR-COLO2',        50 ),
        oEdtk::Field->new( 'PC-PSLNTYPEPS',        5 ),
        oEdtk::Field->new( 'PC-TYPP-COLO1',        15 ),
        oEdtk::Field->new( 'PC-TYPP-COLO2',        50 ),
        oEdtk::Field->new( 'PC-PSLNCODEPS',        2 ),
        oEdtk::Field->new( 'PC-CODP-COLO1',        15 ),
        oEdtk::Field->new( 'PC-CODP-COLO2',        50 ),
        oEdtk::Field->new( 'PC-PSLNCODLPP',        13 ),
        oEdtk::Field->new( 'PC-CODL-COLO1',        15 ),
        oEdtk::Field->new( 'PC-CODL-COLO2',        50 ),
        oEdtk::SignedField->new( 'PC-PSLNSPHERE', 2, 2 ),
        oEdtk::Field->new( 'PC-PSZZ-IND-SPHERE', 1 ),
        oEdtk::SignedField->new( 'PC-PSLNCYLIND', 2, 2 ),
        oEdtk::Field->new( 'PC-PSZZ-IND-CYLIND', 1 ),
    );
}

1;
