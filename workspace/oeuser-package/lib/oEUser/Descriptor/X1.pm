package oEUser::Descriptor::X1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # X1
        oEdtk::Field->new( 'X1-CODUTIL',           8 ),
        oEdtk::Field->new( 'X1-UTIL-CODCIV',       10 ),
        oEdtk::Field->new( 'X1-UTIL-LIBC-CODCIV',  15 ),
        oEdtk::Field->new( 'X1-UTIL-CODTIT',       10 ),
        oEdtk::Field->new( 'X1-UTIL-LIBC-CODTIT',  15 ),
        oEdtk::Field->new( 'X1-UTIL-NOM',          50 ),
        oEdtk::Field->new( 'X1-UTIL-PRENOM-1',     20 ),
        oEdtk::Field->new( 'X1-NAT-COMM-1',        5 ),
        oEdtk::Field->new( 'X1-LIBCOUR-NAT-COMM1', 15 ),
        oEdtk::Field->new( 'X1-VAL-COMM-1',        50 ),
        oEdtk::Field->new( 'X1-NAT-COMM-2',        5 ),
        oEdtk::Field->new( 'X1-LIBCOUR-NAT-COMM2', 15 ),
        oEdtk::Field->new( 'X1-VAL-COMM-2',        50 ),
        oEdtk::Field->new( 'X1-NAT-COMM-3',        5 ),
        oEdtk::Field->new( 'X1-LIBCOUR-NAT-COMM3', 15 ),
        oEdtk::Field->new( 'X1-VAL-COMM-3',        50 ),
        oEdtk::Field->new( 'X1-CODCIV',            10 ),
        oEdtk::Field->new( 'X1-LIBC-CODCIV',       15 ),
        oEdtk::Field->new( 'X1-CODTIT',            10 ),
        oEdtk::Field->new( 'X1-LIBC-CODTIT',       15 ),
        oEdtk::Field->new( 'X1-NOM',               50 ),
        oEdtk::Field->new( 'X1-PRENOM-1',          20 ),
        oEdtk::Field->new( 'X1-LIEU-REM-COU',      32 ),
        oEdtk::Field->new( 'X1-ADRESS-COMPL',      32 ),
        oEdtk::Field->new( 'X1-NUM-VOIE',          9 ),
        oEdtk::Field->new( 'X1-CODBTQ',            2 ),
        oEdtk::Field->new( 'X1-LIBC-BTQ',          15 ),
        oEdtk::Field->new( 'X1-CODE-NAT-VOIE',     5 ),
        oEdtk::Field->new( 'X1-LIBC-NAT-VOIE',     15 ),
        oEdtk::Field->new( 'X1-LIB-VOIE',          32 ),
        oEdtk::Field->new( 'X1-INDIC-CEDEX',       1 ),
        oEdtk::Field->new( 'X1-BP-LIEU',           32 ),
        oEdtk::Field->new( 'X1-LOCALITE',          32 ),
        oEdtk::Field->new( 'X1-CP-CC',             10 ),
        oEdtk::Field->new( 'X1-LIG-ACH',           32 ),
        oEdtk::Field->new( 'X1-CODPAYS',           3 ),
        oEdtk::Field->new( 'X1-LIBC-CODPAY',       15 ),
        oEdtk::Field->new( 'X1-CODE-TRNFAC',       4 ),
        oEdtk::Field->new( 'X1-TITUL-COMPTE',      50 ),
        oEdtk::Field->new( 'X1-CODBQE',            10 ),
        oEdtk::Field->new( 'X1-CODGUI',            10 ),
        oEdtk::Field->new( 'X1-NUMCPT',            20 ),
        oEdtk::Field->new( 'X1-CLE-RIB',           2 ),
        oEdtk::Field->new( 'X1-CLE-IBAN',          4 ),
        oEdtk::Field->new( 'X1-CODPAY-RIB',        3 ),
        oEdtk::Field->new( 'X1-LICCODPAY-RIB',     15 ),
        oEdtk::Field->new( 'X1-CODDEV-RIB',        3 ),
        oEdtk::Field->new( 'X1-LICDEV-RIB',        15 ),
        oEdtk::Field->new( 'X1-CODPA2',            2 ),
        oEdtk::Field->new( 'X1-CODBIC',            14 ),
        oEdtk::Field->new( 'X1-IDTNAT',            30 ),
        oEdtk::Field->new( 'X1-PNTREM-CPE',        50 ),
        oEdtk::Field->new( 'X1-CPLADR-CPE',        50 ),
        oEdtk::Field->new( 'X1-LIBVOI-CPE',        50 ),
        oEdtk::Field->new( 'X1-BOIPOSLIBLDT-CPE',  50 ),
        oEdtk::Field->new( 'X1-LIBLOCLIBLOC-CPE',  50 ),
        oEdtk::Field->new( 'X1-NOMCDXLIBACH-CPE',  50 ),
        oEdtk::Field->new( 'X1-ENFNCODFRN',        9 ),
        oEdtk::Field->new( 'X1-PEPENUMPER',        8 ),
        oEdtk::Field->new( 'X1-RESERVE',           113 ),

        #            oEdtk::Field->new('X1-RESERVE1', 250),
        oEdtk::Field->new( 'X1-RESERVE2', 250 ),
        oEdtk::Field->new( 'X1-RESERVE3', 250 ),
        oEdtk::Field->new( 'X1-RESERVE4', 250 ),
        oEdtk::Field->new( 'X1-RESERVE5', 389 ),
        oEdtk::Field->new( 'X1-RESERVE6', 10 ),
        oEdtk::Field->new( 'X1-RESERVE7', 50 ),
    );
}

1;
