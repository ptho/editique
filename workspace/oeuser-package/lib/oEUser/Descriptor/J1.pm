package oEUser::Descriptor::J1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # J1-LGNDAT
        oEdtk::Field->new( 'J1-TYPE-FRAIS',        2 ),
        oEdtk::Field->new( 'J1-LIBLON-TYPE-FRAIS', 50 ),
        oEdtk::Field->new( 'J1-ACFRCODFRS',        2 ),
        oEdtk::Field->new( 'J1-ACFRLIBCOU',        15 ),
        oEdtk::Field->new( 'J1-ACFRLIBLON',        50 ),
        oEdtk::Field->new( 'J1-ACFSDDEPRD',        8 ),
        oEdtk::Field->new( 'J1-ACFSDFIPRD',        8 ),
        oEdtk::SignedField->new( 'J1-ACFSMAOFRS', 12, 2 ),
        oEdtk::SignedField->new( 'J1-ACFSMPHFRS', 12, 2 ),
        oEdtk::SignedField->new( 'J1-ACFSMAHFRS', 12, 2 ),

        # J1-TAB-TAXES
        # J1-ELT-TAXES
        oEdtk::Field->new( 'J1-PRTXCODTAX', 6 ),
        oEdtk::Field->new( 'J1-ACT2DDEPRD', 8 ),
        oEdtk::Field->new( 'J1-ACT2DFIPRD', 8 ),
        oEdtk::SignedField->new( 'J1-PRVLVALTAU', 4,  5 ),
        oEdtk::SignedField->new( 'J1-ACEAMNTECH', 12, 2 ),
        oEdtk::Field->new( 'J1-PRTXIDFSYS', 8 ),
        oEdtk::Field->new( 'J1-PRTXLIBLON', 50 ),

        # J1-TAXE-1
        oEdtk::Field->new( 'J1-PRTXCODTAX-1', 6 ),
        oEdtk::Field->new( 'J1-ACT2DDEPRD-1', 8 ),
        oEdtk::Field->new( 'J1-ACT2DFIPRD-1', 8 ),
        oEdtk::SignedField->new( 'J1-PRVLVALTAU-1', 4,  5 ),
        oEdtk::SignedField->new( 'J1-ACEAMNTECH-1', 12, 2 ),
        oEdtk::Field->new( 'J1-PRTXIDFSYS-1', 8 ),
        oEdtk::Field->new( 'J1-PRTXLIBLON-1', 50 ),

        # J1-TAXE-2
        oEdtk::Field->new( 'J1-PRTXCODTAX-2', 6 ),
        oEdtk::Field->new( 'J1-ACT2DDEPRD-2', 8 ),
        oEdtk::Field->new( 'J1-ACT2DFIPRD-2', 8 ),
        oEdtk::SignedField->new( 'J1-PRVLVALTAU-2', 4,  5 ),
        oEdtk::SignedField->new( 'J1-ACEAMNTECH-2', 12, 2 ),
        oEdtk::Field->new( 'J1-PRTXIDFSYS-2', 8 ),
        oEdtk::Field->new( 'J1-PRTXLIBLON-2', 50 ),

        # J1-TAXE-3
        oEdtk::Field->new( 'J1-PRTXCODTAX-3', 6 ),
        oEdtk::Field->new( 'J1-ACT2DDEPRD-3', 8 ),
        oEdtk::Field->new( 'J1-ACT2DFIPRD-3', 8 ),
        oEdtk::SignedField->new( 'J1-PRVLVALTAU-3', 4,  5 ),
        oEdtk::SignedField->new( 'J1-ACEAMNTECH-3', 12, 2 ),
        oEdtk::Field->new( 'J1-PRTXIDFSYS-3', 8 ),
        oEdtk::Field->new( 'J1-PRTXLIBLON-3', 50 ),

        # J1-TAXE-4
        oEdtk::Field->new( 'J1-PRTXCODTAX-4', 6 ),
        oEdtk::Field->new( 'J1-ACT2DDEPRD-4', 8 ),
        oEdtk::Field->new( 'J1-ACT2DFIPRD-4', 8 ),
        oEdtk::SignedField->new( 'J1-PRVLVALTAU-4', 4,  5 ),
        oEdtk::SignedField->new( 'J1-ACEAMNTECH-4', 12, 2 ),
        oEdtk::Field->new( 'J1-PRTXIDFSYS-4', 8 ),
        oEdtk::Field->new( 'J1-PRTXLIBLON-4', 50 ),

        # J1-TAXE-5
        oEdtk::Field->new( 'J1-PRTXCODTAX-5', 6 ),
        oEdtk::Field->new( 'J1-ACT2DDEPRD-5', 8 ),
        oEdtk::Field->new( 'J1-ACT2DFIPRD-5', 8 ),
        oEdtk::SignedField->new( 'J1-PRVLVALTAU-5', 4,  5 ),
        oEdtk::SignedField->new( 'J1-ACEAMNTECH-5', 12, 2 ),
        oEdtk::Field->new( 'J1-PRTXIDFSYS-5', 8 ),
        oEdtk::Field->new( 'J1-PRTXLIBLON-5', 50 ),
        oEdtk::Field->new( 'J1-ACFSACFCR3',   8 ),
        oEdtk::Field->new( 'J1-RESERVE-J1',   200 ),
    );
}

1;
