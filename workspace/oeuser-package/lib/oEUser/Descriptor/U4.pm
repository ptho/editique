package oEUser::Descriptor::U4;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # SCC-U4-ENRLCO
        # SCC-400-IDFLCO
        oEdtk::Field->new( 'SCC-400-NUMFLX', 20 ),
        oEdtk::Field->new( 'SCC-400-NUMLOT', 6 ),
        oEdtk::Field->new( 'SCC-400-NUMDOS', 6 ),
        oEdtk::Field->new( 'SCC-400-NUMLCO', 6 ),
        oEdtk::Field->new( 'SCC-400-NUMLGA', 6 ),
        oEdtk::Field->new( 'SCC-400-TYPENR', 3 ),
        oEdtk::Field->new( 'SCC-400-STYENR', 3 ),

        # SCC-400-DONLCO
        oEdtk::Field->new( 'SCC-400-TYPOPE', 1 ),

        # SCC-400-NUMCTR
        oEdtk::Field->new( 'SCC-400-NUMADH', 8 ),
        oEdtk::Field->new( 'SCC-400-NUMCPL', 12 ),
        oEdtk::Field->new( 'SCC-400-TYPCTR', 2 ),

        # SCC-400-DDECTRX
        oEdtk::Field->new( 'SCC-400-DDECTR', 8 ),

        # SCC-400-DFECTRX
        oEdtk::Field->new( 'SCC-400-DFECTR', 8 ),
        oEdtk::Field->new( 'SCC-400-INDCTR', 1 ),
    );
}

1;
