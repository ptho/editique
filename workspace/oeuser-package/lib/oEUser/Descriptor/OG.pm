package oEUser::Descriptor::OG;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # OG-ZZEDIT-OG-LGNDAT
        # OG-ZZEDIT-BANQUE-DOC
        oEdtk::Field->new( 'OG-ZZZZ-BANQUE-DOC', 50 ),
    );
}

1;
