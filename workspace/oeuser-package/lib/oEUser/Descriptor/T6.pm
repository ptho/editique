package oEUser::Descriptor::T6;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # T6-LGNDAT
        # T6-IDENT-PART
        oEdtk::Field->new( 'T6-NUMPART', 5 ),
        oEdtk::Field->new( 'T6-FILLER',  15 ),

        # T6-COTIS-PARTI
        oEdtk::SignedField->new( 'T6-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'T6-MONTANT-TTC', 12, 2 ),
        oEdtk::Field->new( 'T6-ECHIPU',     8 ),
        oEdtk::Field->new( 'T6-CODCEC',     9 ),
        oEdtk::Field->new( 'T6-LIC-CODCEC', 15 ),
        oEdtk::Field->new( 'T6-LIB-CODCEC', 50 ),
        oEdtk::Field->new( 'T6-TYPCEC',     6 ),
        oEdtk::Field->new( 'T6-LIC-TYPCEC', 15 ),
        oEdtk::Field->new( 'T6-LIB-TYPCEC', 50 ),
        oEdtk::Field->new( 'T6-TYPCNP',     1 ),
        oEdtk::Field->new( 'T6-LIC-TYPCNP', 15 ),
        oEdtk::Field->new( 'T6-LIB-TYPCNP', 50 ),
        oEdtk::Field->new( 'T6-MTFCNP',     5 ),
        oEdtk::Field->new( 'T6-LIC-MTFCNP', 15 ),
        oEdtk::Field->new( 'T6-LIB-MTFCNP', 50 ),

        # T6-HORS-PERIODE-DIFF-C-I
        oEdtk::Field->new( 'T6-FILLER1', 1 ),
        oEdtk::SignedField->new( 'T6-TOT-CONSO-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T6-FILLER2', 1 ),
        oEdtk::SignedField->new( 'T6-TOT-IND-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T6-FILLER3', 1 ),
        oEdtk::SignedField->new( 'T6-DIFF-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T6-FILLER4',              1 ),
        oEdtk::Field->new( 'T6-TEST-YA-DIF-HORS-PER', 2 ),

        # T6-YA-DIFF-HORS-PER
        oEdtk::Field->new( 'T6-DATEXI', 8 ),
    );
}

1;
