package oEUser::Descriptor::U5;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # SCC-U5-ENRLGA
        # SCC-410-IDFLGA
        oEdtk::Field->new( 'SCC-410-NUMFLX', 20 ),
        oEdtk::Field->new( 'SCC-410-NUMLOT', 6 ),
        oEdtk::Field->new( 'SCC-410-NUMDOS', 6 ),
        oEdtk::Field->new( 'SCC-410-NUMLCO', 6 ),
        oEdtk::Field->new( 'SCC-410-NUMLGA', 6 ),
        oEdtk::Field->new( 'SCC-410-TYPENR', 3 ),
        oEdtk::Field->new( 'SCC-410-STYENR', 3 ),

        # SCC-410-DONLGA
        oEdtk::Field->new( 'SCC-410-TYPOPE', 1 ),
        oEdtk::Field->new( 'SCC-410-CODGAR', 3 ),

        # SCC-410-DDECTRX
        oEdtk::Field->new( 'SCC-410-DDECTR', 8 ),

        # SCC-410-DFECTRX
        oEdtk::Field->new( 'SCC-410-DFECTR', 8 ),
    );
}

1;
