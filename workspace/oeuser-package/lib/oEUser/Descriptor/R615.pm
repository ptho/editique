package oEUser::Descriptor::R615;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R615-LGNDAT
        oEdtk::Field->new( 'R615-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R615-PERRROLRIB',    5 ),
        oEdtk::Field->new( 'R615-LL-PERRROLRIB', 50 ),
        oEdtk::Field->new( 'R615-LC-PERRROLRIB', 15 ),
        oEdtk::Field->new( 'R615-PERPDDE',       8 ),
        oEdtk::Field->new( 'R615-PERPDFE',       8 ),
        oEdtk::Field->new( 'R615-PERICODBQE',    10 ),
        oEdtk::Field->new( 'R615-ENBANOMEBC',    40 ),
        oEdtk::Field->new( 'R615-PERICODGUI',    10 ),
        oEdtk::Field->new( 'R615-PERINUMCPT',    20 ),
        oEdtk::Field->new( 'R615-PERICLECPT',    2 ),
        oEdtk::Field->new( 'R615-PERICLEIBA',    4 ),
        oEdtk::Field->new( 'R615-PERICPTTIT',    50 ),
        oEdtk::Field->new( 'R615-PERICODDEV',    3 ),
        oEdtk::Field->new( 'R615-LL-PERICODDEV', 50 ),
        oEdtk::Field->new( 'R615-LC-PERICODDEV', 15 ),
        oEdtk::Field->new( 'R615-PERPTYPLIE',    1 ),
        oEdtk::Field->new( 'R615-LL-PERPTYPLIE', 50 ),
        oEdtk::Field->new( 'R615-LC-PERPTYPLIE', 15 ),
        oEdtk::Field->new( 'R615-PEPACODPAY',    3 ),
        oEdtk::Field->new( 'R615-LL-PEPACODPAY', 50 ),
        oEdtk::Field->new( 'R615-LC-PEPACODPAY', 15 ),
        oEdtk::Field->new( 'R615-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R615-ACOPNUMOPE',    5 ),
    );
}

1;
