package oEUser::Descriptor::R610;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R610-LGNDAT
        oEdtk::Field->new( 'R610-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R610-PEAPTYPLIE',    1 ),
        oEdtk::Field->new( 'R610-PEARROLADR',    5 ),
        oEdtk::Field->new( 'R610-LL-PEARROLADR', 50 ),
        oEdtk::Field->new( 'R610-LC-PEARROLADR', 15 ),
        oEdtk::Field->new( 'R610-PEAPDDE',       8 ),
        oEdtk::Field->new( 'R610-PEAPDFE',       8 ),
        oEdtk::Field->new( 'R610-PEAPTYPADR',    5 ),
        oEdtk::Field->new( 'R610-LL-PEAPTYPADR', 50 ),
        oEdtk::Field->new( 'R610-LC-PEAPTYPADR', 15 ),
        oEdtk::Field->new( 'R610-PEAPNBRNPA',    2 ),
        oEdtk::Field->new( 'R610-PEAPGESROB',    1 ),
        oEdtk::Field->new( 'R610-PEAPENVPUB',    1 ),
        oEdtk::Field->new( 'R610-PEAPTYPLIE-A',  1 ),
        oEdtk::Field->new( 'R610-LL-PEAPTYPLIE', 50 ),
        oEdtk::Field->new( 'R610-LC-PEAPTYPLIE', 15 ),
        oEdtk::Field->new( 'R610-PEADNUMVOI',    9 ),
        oEdtk::Field->new( 'R610-PEADBTQVOI',    2 ),
        oEdtk::Field->new( 'R610-LL-PEADBTQVOI', 50 ),
        oEdtk::Field->new( 'R610-LC-PEADBTQVOI', 15 ),
        oEdtk::Field->new( 'R610-PEADNATVOI',    5 ),
        oEdtk::Field->new( 'R610-LL-PEADNATVOI', 50 ),
        oEdtk::Field->new( 'R610-LC-PEADNATVOI', 15 ),
        oEdtk::Field->new( 'R610-PEADLIBVOI',    50 ),
        oEdtk::Field->new( 'R610-PEADREFVOI',    10 ),
        oEdtk::Field->new( 'R610-PEADNUMCPV',    2 ),
        oEdtk::Field->new( 'R610-PEADCPLADR',    50 ),
        oEdtk::Field->new( 'R610-PEADPNTREM',    50 ),
        oEdtk::Field->new( 'R610-PEADLIBLIE',    50 ),
        oEdtk::Field->new( 'R610-PEADREFLIE',    10 ),
        oEdtk::Field->new( 'R610-PEADBOIPOS',    50 ),
        oEdtk::Field->new( 'R610-PEADCODPOS',    10 ),
        oEdtk::Field->new( 'R610-PEADLIBLOC',    50 ),
        oEdtk::Field->new( 'R610-PEADREFLOC',    10 ),
        oEdtk::Field->new( 'R610-PEADLIBACH',    50 ),
        oEdtk::Field->new( 'R610-PEADTRNFAC',    4 ),
        oEdtk::Field->new( 'R610-PEADCODCDX',    5 ),
        oEdtk::Field->new( 'R610-PEADNOMCDX',    26 ),
        oEdtk::Field->new( 'R610-PEADTITCDX',    100 ),
        oEdtk::Field->new( 'R610-PEADCODDEP',    3 ),
        oEdtk::Field->new( 'R610-LL-PEADCODDEP', 50 ),
        oEdtk::Field->new( 'R610-LC-PEADCODDEP', 15 ),
        oEdtk::Field->new( 'R610-PEPACODPAY',    3 ),
        oEdtk::Field->new( 'R610-LL-PEPACODPAY', 50 ),
        oEdtk::Field->new( 'R610-LC-PEPACODPAY', 15 ),
        oEdtk::Field->new( 'R610-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R610-ACOPNUMOPE',    5 ),
    );
}

1;
