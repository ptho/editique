package oEUser::Descriptor::OB;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new( oEdtk::Field->new( 'OB-TXT', '*' ) );
}

1;
