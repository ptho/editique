package oEUser::Descriptor::P7;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # P7-ENTETE
        oEdtk::Field->new( 'P7-PSRLIDFREL', 10 ),
        oEdtk::Field->new( 'P7-PSREDATEDT', 8 ),

        # P7-PSZZ-LST-CRIT-DRC
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-2
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-2', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-3
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-3', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-4
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-4', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-5
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-5', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-6
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-6', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-7
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-7', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-8
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-8', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-9
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-9', 50 ),

        # P7-PSZZ-LST-CRIT-DRC-10
        oEdtk::Field->new( 'P7-PSZZ-CONST-CRIT-DRC-10', 50 ),
        oEdtk::Field->new( 'P7-LMODPAI',                50 ),

        # P7-DC-IDENT-RIB
        oEdtk::Field->new( 'P7-DCRICPTTIT', 50 ),
        oEdtk::Field->new( 'P7-DCRICODBQE', 10 ),
        oEdtk::Field->new( 'P7-DCRICODGUI', 10 ),
        oEdtk::Field->new( 'P7-DCRINUMCPT', 20 ),
        oEdtk::Field->new( 'P7-DCRICLECPT', 2 ),

        # P7-DC-IDENT-CHQ
        oEdtk::Field->new( 'P7-DCPHNUMCHQ',      8 ),
        oEdtk::Field->new( 'P7-MONTANT-LETTREE', 300 ),

        # P7-DC-IDENT-PAIE
        oEdtk::Field->new( 'P7-NBR-DRC',          12 ),
        oEdtk::Field->new( 'P7-NBR-LDRC',         12 ),
        oEdtk::Field->new( 'P7-DCPHDATPAI',       8 ),
        oEdtk::Field->new( 'P7-DCPHNUMPIE',       8 ),
        oEdtk::Field->new( 'P7-DCADCODCIV',       6 ),
        oEdtk::Field->new( 'P7-DCADLIBCIV',       15 ),
        oEdtk::Field->new( 'P7-DCADNOMADR',       50 ),
        oEdtk::Field->new( 'P7-DCADPRNADR',       20 ),
        oEdtk::Field->new( 'P7-PSZZ-CODFRS-PAIE', 9 ),
        oEdtk::Field->new( 'P7-PSZZ-TYPFRS-PAI',  2 ),
        oEdtk::SignedField->new( 'P7-DCPHMNTPAI', 12, 2 ),
        oEdtk::SignedField->new( 'P7-DCRCMNTRCP', 12, 2 ),
        oEdtk::Field->new( 'P7-DCZZ-NBRECUP', 4 ),

        # P7-INFO-FRS
        oEdtk::Field->new( 'P7-ENFNACPPEC',      1 ),
        oEdtk::Field->new( 'P7-ENFNCODRGF',      4 ),
        oEdtk::Field->new( 'P7-ENFNCODACC',      10 ),
        oEdtk::Field->new( 'P7-PSZZ-CODFRS-DST', 9 ),
        oEdtk::Field->new( 'P7-PSZZ-TYPFRS-DST', 2 ),
        oEdtk::Field->new( 'P7-PSEPNUMETB',      9 ),
        oEdtk::Field->new( 'P7-PSZZ-NOMETB',     50 ),
        oEdtk::Field->new( 'P7-PSRLCODSIT',      2 ),

        # P7-RL-AD-RO
        oEdtk::Field->new( 'P7-PSRLCODGRE', 2 ),
        oEdtk::Field->new( 'P7-PSRLCODCRO', 3 ),
        oEdtk::Field->new( 'P7-PSRLCODCPA', 4 ),
        oEdtk::Field->new( 'P7-PSEPETENRO', 13 ),
        oEdtk::Field->new( 'P7-PSEOCLENRO', 2 ),

        # P7-RL-DC-RO
        oEdtk::Field->new( 'P7-PSEODATCPB', 8 ),
        oEdtk::Field->new( 'P7-PSRLCODGGS', 9 ),

        # P7-NOM-CODCRO
        oEdtk::Field->new( 'P7-PENOLIBLON', 50 ),

        # P7-NOM-CODCPA
        oEdtk::Field->new( 'P7-PENOLIBLON-2', 50 ),

        # P7-ADRESSE-CODCRO
        oEdtk::Field->new( 'P7-PELOLIBLOC', 50 ),
        oEdtk::Field->new( 'P7-PELOREFLOC', 10 ),
        oEdtk::Field->new( 'P7-PEZZLIBLDT', 50 ),
        oEdtk::Field->new( 'P7-PEZZREFLDT', 10 ),
        oEdtk::Field->new( 'P7-PEADBOIPOS', 50 ),
        oEdtk::Field->new( 'P7-PEADCPLADR', 50 ),
        oEdtk::Field->new( 'P7-PEADPNTREM', 50 ),
        oEdtk::Field->new( 'P7-PEADNUMVOI', 9 ),
        oEdtk::Field->new( 'P7-PEADBTQVOI', 2 ),
        oEdtk::Field->new( 'P7-PEVONATVOI', 5 ),
        oEdtk::Field->new( 'P7-PEVOLIBVOI', 50 ),
        oEdtk::Field->new( 'P7-PEVOREFVOI', 10 ),
        oEdtk::Field->new( 'P7-PECDCODPOS', 10 ),
        oEdtk::Field->new( 'P7-PECDLIBACH', 50 ),
        oEdtk::Field->new( 'P7-PECXCODCDX', 5 ),
        oEdtk::Field->new( 'P7-PECXNOMCDX', 26 ),
        oEdtk::Field->new( 'P7-PECXTITCDX', 100 ),
        oEdtk::Field->new( 'P7-PELOCODDEP', 3 ),
        oEdtk::Field->new( 'P7-PEPACODPAY', 3 ),

        # P7-ADRESSE-CODCPA
        oEdtk::Field->new( 'P7-PELOLIBLOC-2', 50 ),
        oEdtk::Field->new( 'P7-PELOREFLOC-2', 10 ),
        oEdtk::Field->new( 'P7-PEZZLIBLDT-2', 50 ),
        oEdtk::Field->new( 'P7-PEZZREFLDT-2', 10 ),
        oEdtk::Field->new( 'P7-PEADBOIPOS-2', 50 ),
        oEdtk::Field->new( 'P7-PEADCPLADR-2', 50 ),
        oEdtk::Field->new( 'P7-PEADPNTREM-2', 50 ),
        oEdtk::Field->new( 'P7-PEADNUMVOI-2', 9 ),
        oEdtk::Field->new( 'P7-PEADBTQVOI-2', 2 ),
        oEdtk::Field->new( 'P7-PEVONATVOI-2', 5 ),
        oEdtk::Field->new( 'P7-PEVOLIBVOI-2', 50 ),
        oEdtk::Field->new( 'P7-PEVOREFVOI-2', 10 ),
        oEdtk::Field->new( 'P7-PECDCODPOS-2', 10 ),
        oEdtk::Field->new( 'P7-PECDLIBACH-2', 50 ),
        oEdtk::Field->new( 'P7-PECXCODCDX-2', 5 ),
        oEdtk::Field->new( 'P7-PECXNOMCDX-2', 26 ),
        oEdtk::Field->new( 'P7-PECXTITCDX-2', 100 ),
        oEdtk::Field->new( 'P7-PELOCODDEP-2', 3 ),
        oEdtk::Field->new( 'P7-PEPACODPAY-2', 3 ),

        # P7-DONNEES-RO-ASSI
        oEdtk::Field->new( 'P7-PERONUMSEE-ASSI', 13 ),
        oEdtk::Field->new( 'P7-PEROCLESEE-ASSI', 2 ),
        oEdtk::Field->new( 'P7-FILLER-P7',       29 ),
    );
}

1;
