package oEUser::Descriptor::GB;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # GB-LGNDAT
        # CO62-GB-GRP-ASS-PART
        oEdtk::Field->new( 'CO62-GB-CODGRA', 6 ),
        oEdtk::Field->new( 'CO62-GB-NUMPAR', 5 ),
        oEdtk::Field->new( 'CO62-GB-CODPAR', 3 ),
        oEdtk::Field->new( 'CO62-GB-DEBPRD', 8 ),
        oEdtk::Field->new( 'CO62-GB-FINPRD', 8 ),

        # CO62-GB-REGROUPEMENT
        # CO62-GB-CODE-REGROUPEMENT
        oEdtk::Field->new( 'CO62-GB-CODRGM',       2 ),
        oEdtk::Field->new( 'CO62-GB-CODRGMLIC',    15 ),
        oEdtk::Field->new( 'CO62-GB-CODRGMLIBLON', 50 ),

        # CO62-GB-FRI
        oEdtk::Field->new( 'CO62-GB-CODFRI',     10 ),
        oEdtk::Field->new( 'CO62-GB-LIBCCODFRI', 15 ),
        oEdtk::Field->new( 'CO62-GB-LIBLCODFRI', 50 ),

        # CO62-GB-ASSUREUR
        oEdtk::Field->new( 'CO62-GB-CODASS',    10 ),
        oEdtk::Field->new( 'CO62-GB-CODTITASS', 10 ),
        oEdtk::Field->new( 'CO62-GB-TITASSLIC', 15 ),
        oEdtk::Field->new( 'CO62-GB-NOMTITASS', 50 ),

        # CO62-GB-PRODUIT
        oEdtk::Field->new( 'CO62-GB-CODPRT',     10 ),
        oEdtk::Field->new( 'CO62-GB-LIBCCODPRT', 15 ),
        oEdtk::Field->new( 'CO62-GB-LIBLCODPRT', 50 ),

        # CO62-GB-GARANTIE
        oEdtk::Field->new( 'CO62-GB-CODGAR',     10 ),
        oEdtk::Field->new( 'CO62-GB-LIBCCODGAR', 15 ),
        oEdtk::Field->new( 'CO62-GB-LIBLCODGAR', 50 ),

        # CO62-GB-VAR-DECLA
        oEdtk::Field->new( 'CO62-GB-IDFASF',       8 ),
        oEdtk::Field->new( 'CO62-GB-CODASFLIC',    15 ),
        oEdtk::Field->new( 'CO62-GB-CODASFLIBLON', 50 ),
        oEdtk::Field->new( 'CO62-GB-UNICAL',       4 ),
        oEdtk::Field->new( 'CO62-GB-LIBCUNICAL',   15 ),
        oEdtk::Field->new( 'CO62-GB-LIBLUNICAL',   50 ),
        oEdtk::SignedField->new( 'CO62-GB-VALTX', 4,  5 ),
        oEdtk::SignedField->new( 'CO62-GB-VALMT', 12, 2 ),
        oEdtk::Field->new( 'CO62-GB-VALENT', 9 ),

        # CO62-GB-PRE-CALCULEES
        oEdtk::SignedField->new( 'CO62-GB-VALMTTCTPC', 12, 2 ),
        oEdtk::SignedField->new( 'CO62-GB-VALMTHCPC',  12, 2 ),

        # CO62-GB-CRITERES-RGP
        oEdtk::Field->new( 'CO62-GB-CRT1', 10 ),
        oEdtk::Field->new( 'CO62-GB-CRT2', 10 ),
        oEdtk::Field->new( 'CO62-GB-CRT3', 10 ),
        oEdtk::Field->new( 'CO62-GB-CRT4', 10 ),
    );
}

1;
