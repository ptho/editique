package oEUser::Descriptor::U9;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # SCC-U9-ENRMES
        # SCC-590-IDFMES
        oEdtk::Field->new( 'SCC-590-NUMFLX', 20 ),
        oEdtk::Field->new( 'SCC-590-NUMLOT', 6 ),
        oEdtk::Field->new( 'SCC-590-NUMDOS', 6 ),
        oEdtk::Field->new( 'SCC-590-NUMLCO', 6 ),
        oEdtk::Field->new( 'SCC-590-NUMLGA', 6 ),
        oEdtk::Field->new( 'SCC-590-TYPENR', 3 ),
        oEdtk::Field->new( 'SCC-590-STYENR', 3 ),

        # SCC-590-DONMES
        oEdtk::Field->new( 'SCC-590-CODANO', 10 ),
        oEdtk::Field->new( 'SCC-590-LIBMES', 100 ),
    );
}

1;
