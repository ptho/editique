package oEUser::Descriptor::R420;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R420-LGNDAT
        oEdtk::Field->new( 'R420-ACVCACASR2',    2 ),
        oEdtk::Field->new( 'R420-PRCBCODCRI',    6 ),
        oEdtk::Field->new( 'R420-PRCBLIBLON',    50 ),
        oEdtk::Field->new( 'R420-ACVCDDEPRD',    8 ),
        oEdtk::Field->new( 'R420-ACVCDFEPRD',    8 ),
        oEdtk::Field->new( 'R420-ACVCPRPCRI',    1 ),
        oEdtk::Field->new( 'R420-LC-ACVCPRPCRI', 15 ),
        oEdtk::Field->new( 'R420-LL-ACVCPRPCRI', 50 ),
        oEdtk::Field->new( 'R420-PRVCCODVCR',    10 ),
    );
}

1;
