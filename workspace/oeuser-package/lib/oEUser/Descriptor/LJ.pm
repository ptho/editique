package oEUser::Descriptor::LJ;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # LJ-ENREG
        oEdtk::Field->new( 'CC20-LJ-NUMPART',    5 ),
        oEdtk::Field->new( 'CC20-LJ-ACH1NUMCIN', 15 ),
        oEdtk::Field->new( 'CC20-LJ-RANG-ASSUR', 2 ),
        oEdtk::Field->new( 'CC20-LJ-PRPRCODPDT', 10 ),
        oEdtk::Field->new( 'CC20-LJ-PRGACODGAR', 10 ),
        oEdtk::Field->new( 'CC20-LJ-ACCODDEPRD', 8 ),
        oEdtk::Field->new( 'CC20-LJ-ACCODFEPRD', 8 ),
        oEdtk::Field->new( 'CC20-LJ-PRASCODASF', 6 ),
        oEdtk::Field->new( 'CC20-LJ-PRASLIBCOU', 15 ),
        oEdtk::Field->new( 'CC20-LJ-PRASLIBLON', 50 ),
        oEdtk::SignedField->new( 'CC20-LJ-ACJIMPHCOT', 12, 2 ),
        oEdtk::SignedField->new( 'CC20-LJ-ACJIMAHCOT', 12, 2 ),
    );
}

1;
