package oEUser::Descriptor::R345;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R345-LGNDAT
        oEdtk::Field->new( 'R345-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R345-PENOLIBLON',    50 ),
        oEdtk::Field->new( 'R345-PENOPRN01',     20 ),
        oEdtk::Field->new( 'R345-PEADCODPOS',    5 ),
        oEdtk::Field->new( 'R345-ACH5CODPAR',    3 ),
        oEdtk::Field->new( 'R345-LC-ACH5CODPAR', 15 ),
        oEdtk::Field->new( 'R345-LL-ACH5CODPAR', 50 ),
        oEdtk::Field->new( 'R345-ACH5MODGPA',    1 ),
        oEdtk::Field->new( 'R345-LC-ACH5MODGPA', 15 ),
        oEdtk::Field->new( 'R345-LL-ACH5MODGPA', 50 ),
        oEdtk::Field->new( 'R345-ACPADDEPRD',    8 ),
        oEdtk::Field->new( 'R345-ACPADFEPRD',    8 ),
        oEdtk::Field->new( 'R345-ACPAQUAPAY',    1 ),
        oEdtk::Field->new( 'R345-LC-ACPAQUAPAY', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPAQUAPAY', 50 ),
        oEdtk::Field->new( 'R345-ACPACODDVR',    3 ),
        oEdtk::Field->new( 'R345-LC-ACPACODDVR', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPACODDVR', 50 ),
        oEdtk::Field->new( 'R345-ACPAMODRGC',    2 ),
        oEdtk::Field->new( 'R345-LC-ACPAMODRGC', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPAMODRGC', 50 ),
        oEdtk::Field->new( 'R345-ACPAFRQAVI',    2 ),
        oEdtk::Field->new( 'R345-LC-ACPAFRQAVI', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPAFRQAVI', 50 ),
        oEdtk::Field->new( 'R345-ACPATYPTRM',    2 ),
        oEdtk::Field->new( 'R345-LC-ACPATYPTRM', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPATYPTRM', 50 ),
        oEdtk::Field->new( 'R345-ACPAECHPBA',    2 ),
        oEdtk::Field->new( 'R345-LC-ACPAECHPBA', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPAECHPBA', 50 ),
        oEdtk::Field->new( 'R345-ACPADDEPBA',    8 ),
        oEdtk::Field->new( 'R345-ACPADFEPBA',    8 ),
        oEdtk::Field->new( 'R345-ACPAMTFPBA',    2 ),
        oEdtk::Field->new( 'R345-LC-ACPAMTFPBA', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPAMTFPBA', 50 ),
        oEdtk::Field->new( 'R345-ACPALIBATT',    30 ),
        oEdtk::Field->new( 'R345-ACPATYPPAY',    1 ),
        oEdtk::Field->new( 'R345-LC-ACPATYPPAY', 15 ),
        oEdtk::Field->new( 'R345-LL-ACPATYPPAY', 50 ),
        oEdtk::Field->new( 'R345-ACPAINDDAV',    1 ),
    );
}

1;
