package oEUser::Descriptor::R505;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R505-LGNDAT
        # R505-TBL-STAGE
        oEdtk::Field->new( 'R505-ACSTACASR2',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA',        8 ),

        # R505-TBL-STAGE-2
        oEdtk::Field->new( 'R505-ACSTACASR2-2',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-2',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-2',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-2',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-2', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-2',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-2',        8 ),

        # R505-TBL-STAGE-3
        oEdtk::Field->new( 'R505-ACSTACASR2-3',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-3',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-3',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-3',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-3', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-3',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-3',        8 ),

        # R505-TBL-STAGE-4
        oEdtk::Field->new( 'R505-ACSTACASR2-4',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-4',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-4',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-4',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-4', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-4',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-4',        8 ),

        # R505-TBL-STAGE-5
        oEdtk::Field->new( 'R505-ACSTACASR2-5',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-5',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-5',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-5',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-5', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-5',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-5',        8 ),

        # R505-TBL-STAGE-6
        oEdtk::Field->new( 'R505-ACSTACASR2-6',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-6',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-6',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-6',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-6', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-6',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-6',        8 ),

        # R505-TBL-STAGE-7
        oEdtk::Field->new( 'R505-ACSTACASR2-7',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-7',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-7',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-7',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-7', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-7',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-7',        8 ),

        # R505-TBL-STAGE-8
        oEdtk::Field->new( 'R505-ACSTACASR2-8',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-8',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-8',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-8',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-8', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-8',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-8',        8 ),

        # R505-TBL-STAGE-9
        oEdtk::Field->new( 'R505-ACSTACASR2-9',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-9',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-9',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-9',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-9', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-9',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-9',        8 ),

        # R505-TBL-STAGE-10
        oEdtk::Field->new( 'R505-ACSTACASR2-10',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-10',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-10',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-10',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-10', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-10',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-10',        8 ),

        # R505-TBL-STAGE-11
        oEdtk::Field->new( 'R505-ACSTACASR2-11',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-11',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-11',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-11',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-11', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-11',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-11',        8 ),

        # R505-TBL-STAGE-12
        oEdtk::Field->new( 'R505-ACSTACASR2-12',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-12',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-12',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-12',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-12', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-12',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-12',        8 ),

        # R505-TBL-STAGE-13
        oEdtk::Field->new( 'R505-ACSTACASR2-13',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-13',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-13',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-13',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-13', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-13',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-13',        8 ),

        # R505-TBL-STAGE-14
        oEdtk::Field->new( 'R505-ACSTACASR2-14',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-14',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-14',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-14',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-14', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-14',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-14',        8 ),

        # R505-TBL-STAGE-15
        oEdtk::Field->new( 'R505-ACSTACASR2-15',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-15',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-15',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-15',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-15', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-15',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-15',        8 ),

        # R505-TBL-STAGE-16
        oEdtk::Field->new( 'R505-ACSTACASR2-16',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-16',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-16',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-16',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-16', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-16',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-16',        8 ),

        # R505-TBL-STAGE-17
        oEdtk::Field->new( 'R505-ACSTACASR2-17',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-17',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-17',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-17',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-17', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-17',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-17',        8 ),

        # R505-TBL-STAGE-18
        oEdtk::Field->new( 'R505-ACSTACASR2-18',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-18',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-18',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-18',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-18', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-18',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-18',        8 ),

        # R505-TBL-STAGE-19
        oEdtk::Field->new( 'R505-ACSTACASR2-19',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-19',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-19',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-19',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-19', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-19',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-19',        8 ),

        # R505-TBL-STAGE-20
        oEdtk::Field->new( 'R505-ACSTACASR2-20',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-20',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-20',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-20',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-20', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-20',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-20',        8 ),

        # R505-TBL-STAGE-21
        oEdtk::Field->new( 'R505-ACSTACASR2-21',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-21',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-21',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-21',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-21', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-21',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-21',        8 ),

        # R505-TBL-STAGE-22
        oEdtk::Field->new( 'R505-ACSTACASR2-22',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-22',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-22',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-22',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-22', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-22',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-22',        8 ),

        # R505-TBL-STAGE-23
        oEdtk::Field->new( 'R505-ACSTACASR2-23',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-23',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-23',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-23',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-23', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-23',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-23',        8 ),

        # R505-TBL-STAGE-24
        oEdtk::Field->new( 'R505-ACSTACASR2-24',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-24',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-24',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-24',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-24', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-24',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-24',        8 ),

        # R505-TBL-STAGE-25
        oEdtk::Field->new( 'R505-ACSTACASR2-25',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-25',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-25',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-25',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-25', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-25',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-25',        8 ),

        # R505-TBL-STAGE-26
        oEdtk::Field->new( 'R505-ACSTACASR2-26',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-26',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-26',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-26',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-26', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-26',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-26',        8 ),

        # R505-TBL-STAGE-27
        oEdtk::Field->new( 'R505-ACSTACASR2-27',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-27',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-27',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-27',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-27', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-27',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-27',        8 ),

        # R505-TBL-STAGE-28
        oEdtk::Field->new( 'R505-ACSTACASR2-28',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-28',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-28',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-28',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-28', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-28',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-28',        8 ),

        # R505-TBL-STAGE-29
        oEdtk::Field->new( 'R505-ACSTACASR2-29',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-29',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-29',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-29',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-29', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-29',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-29',        8 ),

        # R505-TBL-STAGE-30
        oEdtk::Field->new( 'R505-ACSTACASR2-30',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-30',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-30',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-30',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-30', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-30',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-30',        8 ),

        # R505-TBL-STAGE-31
        oEdtk::Field->new( 'R505-ACSTACASR2-31',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-31',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-31',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-31',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-31', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-31',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-31',        8 ),

        # R505-TBL-STAGE-32
        oEdtk::Field->new( 'R505-ACSTACASR2-32',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-32',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-32',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-32',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-32', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-32',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-32',        8 ),

        # R505-TBL-STAGE-33
        oEdtk::Field->new( 'R505-ACSTACASR2-33',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-33',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-33',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-33',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-33', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-33',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-33',        8 ),

        # R505-TBL-STAGE-34
        oEdtk::Field->new( 'R505-ACSTACASR2-34',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-34',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-34',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-34',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-34', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-34',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-34',        8 ),

        # R505-TBL-STAGE-35
        oEdtk::Field->new( 'R505-ACSTACASR2-35',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-35',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-35',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-35',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-35', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-35',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-35',        8 ),

        # R505-TBL-STAGE-36
        oEdtk::Field->new( 'R505-ACSTACASR2-36',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-36',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-36',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-36',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-36', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-36',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-36',        8 ),

        # R505-TBL-STAGE-37
        oEdtk::Field->new( 'R505-ACSTACASR2-37',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-37',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-37',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-37',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-37', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-37',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-37',        8 ),

        # R505-TBL-STAGE-38
        oEdtk::Field->new( 'R505-ACSTACASR2-38',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-38',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-38',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-38',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-38', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-38',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-38',        8 ),

        # R505-TBL-STAGE-39
        oEdtk::Field->new( 'R505-ACSTACASR2-39',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-39',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-39',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-39',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-39', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-39',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-39',        8 ),

        # R505-TBL-STAGE-40
        oEdtk::Field->new( 'R505-ACSTACASR2-40',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-40',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-40',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-40',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-40', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-40',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-40',        8 ),

        # R505-TBL-STAGE-41
        oEdtk::Field->new( 'R505-ACSTACASR2-41',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-41',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-41',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-41',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-41', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-41',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-41',        8 ),

        # R505-TBL-STAGE-42
        oEdtk::Field->new( 'R505-ACSTACASR2-42',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-42',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-42',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-42',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-42', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-42',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-42',        8 ),

        # R505-TBL-STAGE-43
        oEdtk::Field->new( 'R505-ACSTACASR2-43',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-43',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-43',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-43',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-43', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-43',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-43',        8 ),

        # R505-TBL-STAGE-44
        oEdtk::Field->new( 'R505-ACSTACASR2-44',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-44',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-44',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-44',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-44', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-44',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-44',        8 ),

        # R505-TBL-STAGE-45
        oEdtk::Field->new( 'R505-ACSTACASR2-45',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-45',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-45',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-45',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-45', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-45',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-45',        8 ),

        # R505-TBL-STAGE-46
        oEdtk::Field->new( 'R505-ACSTACASR2-46',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-46',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-46',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-46',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-46', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-46',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-46',        8 ),

        # R505-TBL-STAGE-47
        oEdtk::Field->new( 'R505-ACSTACASR2-47',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-47',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-47',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-47',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-47', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-47',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-47',        8 ),

        # R505-TBL-STAGE-48
        oEdtk::Field->new( 'R505-ACSTACASR2-48',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-48',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-48',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-48',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-48', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-48',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-48',        8 ),

        # R505-TBL-STAGE-49
        oEdtk::Field->new( 'R505-ACSTACASR2-49',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-49',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-49',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-49',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-49', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-49',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-49',        8 ),

        # R505-TBL-STAGE-50
        oEdtk::Field->new( 'R505-ACSTACASR2-50',        2 ),
        oEdtk::Field->new( 'R505-ACH4CODGAR-50',        10 ),
        oEdtk::Field->new( 'R505-ACH4CODPRT-50',        10 ),
        oEdtk::Field->new( 'R505-ACSTCODSTA-50',        6 ),
        oEdtk::Field->new( 'R505-LIBCOU-ACSTCODSTA-50', 15 ),
        oEdtk::Field->new( 'R505-ACSTDURDRG-50',        2 ),
        oEdtk::Field->new( 'R505-ACSTDFESTA-50',        8 ),
    );
}

1;
