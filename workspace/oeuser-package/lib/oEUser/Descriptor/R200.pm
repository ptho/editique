package oEUser::Descriptor::R200;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R200-LGNDAT
        oEdtk::Field->new( 'R200-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R200-ACOPNUMOPE',    5 ),
        oEdtk::Field->new( 'R200-ACOPCODETA',    2 ),
        oEdtk::Field->new( 'R200-ACOPDDEOPE',    8 ),
        oEdtk::Field->new( 'R200-ACOPDDECOT',    8 ),
        oEdtk::Field->new( 'R200-ACOPNUMAVT',    3 ),
        oEdtk::Field->new( 'R200-ACOPCODAGC',    4 ),
        oEdtk::Field->new( 'R200-LL-ACOPCODAGC', 50 ),
        oEdtk::Field->new( 'R200-LC-ACOPCODAGC', 15 ),
        oEdtk::Field->new( 'R200-ACOPCODCTX',    4 ),
        oEdtk::Field->new( 'R200-LL-ACOPCODCTX', 50 ),
        oEdtk::Field->new( 'R200-LC-ACOPCODCTX', 15 ),
        oEdtk::Field->new( 'R200-ACOPCODSIT',    2 ),
        oEdtk::Field->new( 'R200-ACOPNUMCCO',    15 ),
        oEdtk::Field->new( 'R200-SOUSC-CC',      8 ),
        oEdtk::Field->new( 'R200-ACOPDATCTA',    8 ),
        oEdtk::Field->new( 'R200-ACOPDATVTE',    8 ),
        oEdtk::Field->new( 'R200-ACOPDATDEM',    8 ),
        oEdtk::Field->new( 'R200-ACOPDEREXU',    4 ),
        oEdtk::Field->new( 'R200-ACOPAUDTRT',    10 ),
        oEdtk::Field->new( 'R200-NUMCIN-ASS',    15 ),
        oEdtk::Field->new( 'R200-NUMCIN-SSE',    15 ),
    );
}

1;
