package oEUser::Descriptor::F5;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::DateField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # F5-LGNDAT
        # F5-GROUPE-ASS
        oEdtk::Field->new( 'F5-CODGRA', 6 ),

        # F5-NOM-GA
        oEdtk::Field->new( 'F5-PENOCODTIT-GA', 10 ),
        oEdtk::Field->new( 'F5-PENOLICTIT-GA', 15 ),
        oEdtk::Field->new( 'F5-PENOLIBLON-GA', 50 ),
        oEdtk::Field->new( 'F5-CODSIR-GA',     14 ),
        oEdtk::Field->new( 'F5-ACH7DDEADH',    8 ),
        oEdtk::Field->new( 'F5-ACH7DDERAD',    8 ),
        oEdtk::Field->new( 'F5-ACH7LIBATT',    30 ),
        oEdtk::Field->new( 'F5-ACH7INDABR',    1 ),
        oEdtk::Field->new( 'F5-ACH7ACGAR3',    6 ),

        # F5-NOM-GR
        oEdtk::Field->new( 'F5-PENOCODTIT-GR', 10 ),
        oEdtk::Field->new( 'F5-PENOLICTIT-GR', 15 ),
        oEdtk::Field->new( 'F5-PENOLIBLON-GR', 50 ),

        # F5-IDENT-PART
        oEdtk::Field->new( 'F5-NUMPART',      5 ),
        oEdtk::Field->new( 'F5-CODPAR',       3 ),
        oEdtk::Field->new( 'F5-LIBCOUCODPAR', 15 ),
        oEdtk::Field->new( 'F5-LIBLONCODPAR', 50 ),

        # F5-ZZEDIT-PAYEUR
        oEdtk::Field->new( 'F5-ACPAMODRGC', 2 ),
        oEdtk::Field->new( 'F5-LICMODRGC',  15 ),
        oEdtk::Field->new( 'F5-LIBMODRGC',  50 ),
        oEdtk::Field->new( 'F5-ACFQFRQRGC', 2 ),
        oEdtk::Field->new( 'F5-ACFQLIBCOU', 15 ),
        oEdtk::Field->new( 'F5-ACFQLIBLON', 50 ),
        oEdtk::Field->new( 'F5-ACPAFRQAVI', 2 ),
        oEdtk::Field->new( 'F5-LICFRQAVI',  15 ),
        oEdtk::Field->new( 'F5-LIBFRQAVI',  50 ),
        oEdtk::Field->new( 'F5-ACPAECHPBA', 2 ),
        oEdtk::Field->new( 'F5-LICECHPBA',  15 ),
        oEdtk::Field->new( 'F5-LIBECHPBA',  50 ),
        oEdtk::Field->new( 'F5-ACPADDEPBA', 8 ),
        oEdtk::Field->new( 'F5-ACPADFEPBA', 8 ),
        oEdtk::Field->new( 'F5-ACPAMTFPBA', 2 ),
        oEdtk::Field->new( 'F5-LICMTFPBA',  15 ),
        oEdtk::Field->new( 'F5-LIBMTFPBA',  50 ),

        # F5-IDENT-PY
        oEdtk::Field->new( 'F5-ACPAACGAR2', 6 ),

        # F5-NOM-PY
        oEdtk::Field->new( 'F5-PENOCODTIT-PY', 10 ),
        oEdtk::Field->new( 'F5-PENOLICTIT-PY', 15 ),
        oEdtk::Field->new( 'F5-PENOLIBLON-PY', 50 ),

        # F5-ADRESSE-PY
        oEdtk::Field->new( 'F5-PNTREM-PY',       32 ),
        oEdtk::Field->new( 'F5-CPLADR-PY',       32 ),
        oEdtk::Field->new( 'F5-PEADNUMVOI-PY',   9 ),
        oEdtk::Field->new( 'F5-PEADBTQVOI-PY',   2 ),
        oEdtk::Field->new( 'F5-PEADLICBTQ-PY',   15 ),
        oEdtk::Field->new( 'F5-PEVONATVOI-PY',   5 ),
        oEdtk::Field->new( 'F5-PEADLICNTV-PY',   15 ),
        oEdtk::Field->new( 'F5-LIBVOI-PY',       32 ),
        oEdtk::Field->new( 'F5-INDCDX-PY',       1 ),
        oEdtk::Field->new( 'F5-BOIPOSLIBLDT-PY', 32 ),
        oEdtk::Field->new( 'F5-LIBLOCLIBLOC-PY', 32 ),
        oEdtk::Field->new( 'F5-CODCDXCODPOS-PY', 10 ),
        oEdtk::Field->new( 'F5-NOMCDXLIBACH-PY', 32 ),
        oEdtk::Field->new( 'F5-PEPACODPAY-PY',   3 ),
        oEdtk::Field->new( 'F5-PEPALICPAY-PY',   15 ),
        oEdtk::Field->new( 'F5-TRNFAC-PY',       4 ),

        # F5-RIB-PY
        oEdtk::Field->new( 'F5-PERICPTTIT', 50 ),
        oEdtk::Field->new( 'F5-PERICODBQE', 10 ),
        oEdtk::Field->new( 'F5-PERICODGUI', 10 ),
        oEdtk::Field->new( 'F5-PERINUMCPT', 20 ),
        oEdtk::Field->new( 'F5-PERICLECPT', 2 ),
        oEdtk::Field->new( 'F5-PERICLEIBA', 4 ),
        oEdtk::Field->new( 'F5-PEPACODPAY', 3 ),
        oEdtk::Field->new( 'F5-LICCODPAY',  15 ),
        oEdtk::Field->new( 'F5-PERICODDEV', 3 ),
        oEdtk::Field->new( 'F5-LICCODDEV',  15 ),

        # F5-AUTRE-INFO
        oEdtk::DateField->new( 'F5-DEBPER', 8 ),
        oEdtk::DateField->new( 'F5-FINPER', 8 ),
        oEdtk::Field->new( 'CC71-F5-NUMOPE-AVIS', 5 ),
        oEdtk::Field->new( 'F5-NUM-BOR',          6 ),
        oEdtk::Field->new( 'F5-TYPTRM',           2 ),

        # F5-ZZEDIT-COMM
        oEdtk::Field->new( 'F5-PECONATCOM',     5 ),
        oEdtk::Field->new( 'F5-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'F5-PECOVALCOMM',    50 ),

        # F5-ZZEDIT-COMM-2
        oEdtk::Field->new( 'F5-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'F5-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'F5-PECOVALCOMM-2',    50 ),

        # F5-ZZEDIT-COMM-3
        oEdtk::Field->new( 'F5-PECONATCOM-3',     5 ),
        oEdtk::Field->new( 'F5-PEZZ-LICNATCOM-3', 15 ),
        oEdtk::Field->new( 'F5-PECOVALCOMM-3',    50 ),

        # F5-ZZEDIT-COMM-4
        oEdtk::Field->new( 'F5-PECONATCOM-4',     5 ),
        oEdtk::Field->new( 'F5-PEZZ-LICNATCOM-4', 15 ),
        oEdtk::Field->new( 'F5-PECOVALCOMM-4',    50 ),

        # F5-ZZEDIT-COMM-5
        oEdtk::Field->new( 'F5-PECONATCOM-5',     5 ),
        oEdtk::Field->new( 'F5-PEZZ-LICNATCOM-5', 15 ),
        oEdtk::Field->new( 'F5-PECOVALCOMM-5',    50 ),

        # F5-ADRESSE-PAYEUR    Complément adresse du payeur
        oEdtk::Field->new( 'F5-PNTREM-CPA',       50 ),
        oEdtk::Field->new( 'F5-CPLADR-CPA',       50 ),
        oEdtk::Field->new( 'F5-LIBVOI-CPA',       50 ),
        oEdtk::Field->new( 'F5-BOIPOSLIBLDT-CPA', 50 ),
        oEdtk::Field->new( 'F5-LIBLOCLIBLOC-CPA', 50 ),
        oEdtk::Field->new( 'F5-NOMCDXLIBACH-CPA', 50 ),

    );
}

1;
