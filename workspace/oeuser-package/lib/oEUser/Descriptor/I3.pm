package oEUser::Descriptor::I3;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # I3-LGNDAT
        oEdtk::Field->new( 'I3-ECH2DATTRT', 8 ),
        oEdtk::Field->new( 'I3-ECETCODETP', 5 ),
        oEdtk::Field->new( 'I3-ECEPLIBCOU', 15 ),
        oEdtk::Field->new( 'I3-ECAPCODCOU', 5 ),
        oEdtk::Field->new( 'I3-ECEPLIBCRR', 50 ),
    );
}

1;
