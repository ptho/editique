package oEUser::Descriptor::PV;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # DEFINTION-RECORD-PV-TEST
        oEdtk::Field->new( 'NUM-SEQ-RELEVE', 10 ),
        oEdtk::DateField->new( 'DATE-TRAITEMENT-EDITION', 8 ),
        oEdtk::Field->new( 'C01-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C02-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C03-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C04-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C05-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C06-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C07-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C08-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C09-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'C10-TRI-INTERNE',          50 ),
        oEdtk::Field->new( 'ID-SYST-CPT-DECAISS',      10 ),
        oEdtk::Field->new( 'TYPE-DESTINATAIRE',        2 ),
        oEdtk::Field->new( 'LIB-LONG-TYPE-DEST',       50 ),
        oEdtk::Field->new( 'NUM-CTR-INDIVIDUEL',       15 ),
        oEdtk::Field->new( 'NUM-DESTINATAIRE-CONTRAT', 8 ),
        oEdtk::Field->new( 'TYP-FOURNISSEUR',          2 ),
        oEdtk::Field->new( 'LIB-LONG-TYP-FOURNISSEUR', 50 ),
        oEdtk::Field->new( 'CODE-FOURNISSEUR',         9 ),
        oEdtk::Field->new( 'ID-SYST-PAIEMENT',         10 ),
        oEdtk::Field->new( 'ID-SYST-INDU',             10 ),
        oEdtk::Field->new( 'NUM-OPE-RECUPERATION',     8 ),
        oEdtk::SignedField->new( 'MONTANT-RECUPERE', 12, 2 ),
        oEdtk::Field->new( 'CODE-ETAT',                     2 ),
        oEdtk::Field->new( 'LIB-COURT-CODE-ETAT',           15 ),
        oEdtk::Field->new( 'NUM-LOT',                       10 ),
        oEdtk::Field->new( 'NUM-PIECE',                     8 ),
        oEdtk::Field->new( 'TYP-INDU',                      2 ),
        oEdtk::Field->new( 'LIB-LONG-TYP-INDU',             50 ),
        oEdtk::Field->new( 'FAMILLE-RISQUES',               6 ),
        oEdtk::Field->new( 'LIB-COURT-FAMILLE-RIQUES',      15 ),
        oEdtk::Field->new( 'MOTIF-INDU',                    2 ),
        oEdtk::Field->new( 'LIB-LONG-MOTIF-INDU',           50 ),
        oEdtk::Field->new( 'CODE-INDICATEUR-PAIEMENT',      1 ),
        oEdtk::Field->new( 'LIB-LONG-CODE-INDICATEUR',      50 ),
        oEdtk::Field->new( 'CODE-MONNAIE-GESTION',          3 ),
        oEdtk::Field->new( 'LIB-COURT-MONNAIE-GESTION',     15 ),
        oEdtk::Field->new( 'REFERENCE-1',                   30 ),
        oEdtk::Field->new( 'REFERENCE-2',                   30 ),
        oEdtk::Field->new( 'REFERENCE-3',                   30 ),
        oEdtk::Field->new( 'TYPE-EVT-JOURNAL',              10 ),
        oEdtk::Field->new( 'CODE-REGROUP-DESTINATAIRE',     5 ),
        oEdtk::Field->new( 'LIB-LONG-REGROUP-DESTINATAIRE', 50 ),
        oEdtk::DateField->new( 'DATE-CONSTAT-INDU',  8 ),
        oEdtk::DateField->new( 'DATE-CREATION-INDU', 8 ),
        oEdtk::Field->new( 'CODE-UTILISATEUR-CREATION-INDU', 8 ),
        oEdtk::DateField->new( 'DATE-DEBUT-INDU',   8 ),
        oEdtk::DateField->new( 'DATE-FIN-INDU',     8 ),
        oEdtk::DateField->new( 'DATE-CLOTURE-INDU', 8 ),
        oEdtk::Field->new( 'INDICATEUR-COMPENSATION-INDU', 1 ),
        oEdtk::Field->new( 'ETAT-INDU',                    2 ),
        oEdtk::Field->new( 'LIB-COURT-ETAT',               15 ),
        oEdtk::SignedField->new( 'MONTANT-ORIGINE-INDU',   12, 2 ),
        oEdtk::SignedField->new( 'SOLDE-INDU-DATE-RELEVE', 12, 2 ),
        oEdtk::Field->new( 'CODE-CENTRE-GESTION',           9 ),
        oEdtk::Field->new( 'CODE-TITRE',                    10 ),
        oEdtk::Field->new( 'LIB-COURT-CODE-TITRE',          15 ),
        oEdtk::Field->new( 'NOM-RAISON-SOCIALE',            50 ),
        oEdtk::Field->new( 'CODE-ENTITE-RATTACHEMENT-INDU', 9 ),
        oEdtk::Field->new( 'CODE-TITRE-2',                  10 ),
        oEdtk::Field->new( 'LIB-COURT-CODE-TITRE-2',        15 ),
        oEdtk::Field->new( 'NOM-RAISON-SOCIALE-2',          50 ),
        oEdtk::Field->new( 'ID-SYSTEM-ODP',                 10 ),
        oEdtk::DateField->new( 'DATE-ARRIVEE', 8 ),
        oEdtk::Field->new( 'NUM-DEMANDE',         8 ),
        oEdtk::Field->new( 'NUM-DOSSIER',         2 ),
        oEdtk::Field->new( 'NUM-DECOMPTE',        2 ),
        oEdtk::Field->new( 'NOM-BENEFICIAIRE',    50 ),
        oEdtk::Field->new( 'PRENOM-BENEFICIAIRE', 20 ),
        oEdtk::DateField->new( 'DATE-DEBUT-SOINS', 8 ),
        oEdtk::DateField->new( 'DATE-FIN-SOINS',   8 ),
    );
}

1;
