package oEUser::Descriptor::GD;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # GD-LGNDAT
        oEdtk::Field->new( 'GD-ACH1NUMCIN',        15 ),
        oEdtk::Field->new( 'GD-ACH1ACOPR1',        8 ),
        oEdtk::Field->new( 'GD-ACH2PEREI1',        8 ),
        oEdtk::Field->new( 'GD-PENOCODCIV',        10 ),
        oEdtk::Field->new( 'GD-LIBCOU-PENOCODCIV', 15 ),
        oEdtk::Field->new( 'GD-PENOCODTIT',        10 ),
        oEdtk::Field->new( 'GD-LIBCOU-PENOCODTIT', 15 ),
        oEdtk::Field->new( 'GD-PENOLIBLON',        50 ),
        oEdtk::Field->new( 'GD-PENOPRN01',         20 ),
        oEdtk::Field->new( 'GD-ACH2CODTAS',        10 ),
        oEdtk::Field->new( 'GD-LIBCOU-ACH2CODTAS', 15 ),
        oEdtk::Field->new( 'GD-LIBLON-ACH2CODTAS', 50 ),
        oEdtk::Field->new( 'GD-ACH2CODAXT',        10 ),
        oEdtk::Field->new( 'GD-ACH2ACASR2',        2 ),
        oEdtk::Field->new( 'GD-PEPPCODCSO',        12 ),
        oEdtk::Field->new( 'GD-LIBCOU-PEPPCODCSO', 15 ),
        oEdtk::Field->new( 'GD-LIBLON-PEPPCODCSO', 50 ),
        oEdtk::Field->new( 'GD-PEPPCODCSP',        4 ),
        oEdtk::Field->new( 'GD-LIBCOU-PEPPCODCSP', 15 ),
        oEdtk::Field->new( 'GD-LIBLON-PEPPCODCSP', 50 ),
        oEdtk::Field->new( 'GD-PEPPCODPRF',        5 ),
        oEdtk::Field->new( 'GD-LIBCOU-PEPPCODPRF', 15 ),
        oEdtk::Field->new( 'GD-LIBLON-PEPPCODPRF', 50 ),
        oEdtk::Field->new( 'GD-PECPDATNAI',        8 ),
        oEdtk::Field->new( 'GD-ACH2DDE1AD',        8 ),
        oEdtk::Field->new( 'GD-ACH2DDEZRA',        8 ),
        oEdtk::Field->new( 'GD-PEROTYPDNA',        2 ),
        oEdtk::Field->new( 'GD-PEAFNUMRO',         13 ),
        oEdtk::Field->new( 'GD-PEAFCLERO',         2 ),
        oEdtk::Field->new( 'GD-PERODDEDRO',        8 ),
        oEdtk::Field->new( 'GD-PERODFEDRO',        8 ),
        oEdtk::Field->new( 'GD-PEAFCODGRE',        2 ),
        oEdtk::Field->new( 'GD-PEAFCODCRO',        3 ),
        oEdtk::Field->new( 'GD-PEAFCODCPA',        4 ),
        oEdtk::Field->new( 'GD-ACH2INDTIP',        1 ),
        oEdtk::Field->new( 'GD-ACH2DDETIP',        8 ),
        oEdtk::Field->new( 'GD-ACH2DFETIP',        8 ),
        oEdtk::Field->new( 'GD-ACH2INDCMU',        1 ),
        oEdtk::Field->new( 'GD-ACH2INDTLT',        1 ),
        oEdtk::Field->new( 'GD-ACH2DDETLT',        8 ),
        oEdtk::Field->new( 'GD-ACH2DFETLT',        8 ),
        oEdtk::Field->new( 'GD-PEPPLIBMAT',        15 ),
        oEdtk::Field->new( 'GD-PEPPCODUSI',        10 ),
        oEdtk::Field->new( 'GD-PEPPCODATE',        10 ),
        oEdtk::Field->new( 'GD-RESERVE-GD',        227 ),
    );
}

1;
