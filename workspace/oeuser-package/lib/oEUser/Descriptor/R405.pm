package oEUser::Descriptor::R405;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R405-LGNDAT
        oEdtk::Field->new( 'R405-ACCEACASR2',    2 ),
        oEdtk::Field->new( 'R405-ACCEDDEPRD',    8 ),
        oEdtk::Field->new( 'R405-ACCEDFEPRD',    8 ),
        oEdtk::Field->new( 'R405-ACCECODGEL',    5 ),
        oEdtk::Field->new( 'R405-LC-ACCECODGEL', 15 ),
        oEdtk::Field->new( 'R405-LL-ACCECODGEL', 50 ),
        oEdtk::Field->new( 'R405-ACCEINDEXL',    1 ),
        oEdtk::Field->new( 'R405-PSARCODARC',    5 ),
        oEdtk::Field->new( 'R405-LC-PSARCODARC', 15 ),
        oEdtk::Field->new( 'R405-LL-PSARCODARC', 50 ),
    );
}

1;
