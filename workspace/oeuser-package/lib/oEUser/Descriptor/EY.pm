package oEUser::Descriptor::EY;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # ENREG-EY
        oEdtk::SignedField->new( 'ET0002C4-MTRGDEV', 12, 2 ),
        oEdtk::Field->new( 'ET0002C4-CODDEV',      3 ),
        oEdtk::Field->new( 'ET0002C4-LIBDEV',      50 ),
        oEdtk::Field->new( 'ET0002C4-DAT-SAIS',    8 ),
        oEdtk::Field->new( 'ET0002C4-CODUTIL',     8 ),
        oEdtk::Field->new( 'ET0002C4-CODCIV-UTIL', 8 ),
        oEdtk::Field->new( 'ET0002C4-LIBCIV-UTIL', 15 ),
        oEdtk::Field->new( 'ET0002C4-CODTIT-UTIL', 8 ),
        oEdtk::Field->new( 'ET0002C4-LIBTIT-UTIL', 15 ),
        oEdtk::Field->new( 'ET0002C4-NOM-UTIL',    30 ),
        oEdtk::Field->new( 'ET0002C4-PRENOM-UTIL', 30 ),
        oEdtk::Field->new( 'ET0002C4-CODMYENC',    2 ),
        oEdtk::Field->new( 'ET0002C4-LIBMYE',      50 ),
        oEdtk::Field->new( 'ET0002C4-NUMLOT',      5 ),
        oEdtk::Field->new( 'ET0002C4-NUMCHQ',      9 ),
        oEdtk::Field->new( 'ET0002C4-NOMBQE',      20 ),
        oEdtk::Field->new( 'ET0002C4-CODCHQ',      1 ),
        oEdtk::Field->new( 'ET0002C4-LIBCHQ',      50 ),
        oEdtk::Field->new( 'ET0002C4-TITCPT',      50 ),
        oEdtk::Field->new( 'ET0002C4-NOREG',       9 ),
        oEdtk::Field->new( 'ET0002C4-NUMOPE-REGL', 5 ),
        oEdtk::Field->new( 'ET0002C4-REMISE-BQUE', 8 ),
    );
}

1;
