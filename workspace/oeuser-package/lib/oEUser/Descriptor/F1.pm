package oEUser::Descriptor::F1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # F1-LGNDAT
        # F1-DONNEES-CONTRAT
        oEdtk::SignedField->new( 'F1-SOLDE-GLOBAL',    12, 2 ),
        oEdtk::SignedField->new( 'F1-SOLDE-COMPTABLE', 12, 2 ),
        oEdtk::Field->new( 'F1-DATE-EFFET-SUSP', 8 ),

        # F1-RIB-PAYEUR
        oEdtk::Field->new( 'F1-PERICPTTIT',        50 ),
        oEdtk::Field->new( 'F1-PERICODBQE',        10 ),
        oEdtk::Field->new( 'F1-PERICODGUI',        10 ),
        oEdtk::Field->new( 'F1-PERINUMCPT',        20 ),
        oEdtk::Field->new( 'F1-PERICLECPT',        2 ),
        oEdtk::Field->new( 'F1-PERICLEIBA',        4 ),
        oEdtk::Field->new( 'F1-PEPACODPAY',        3 ),
        oEdtk::Field->new( 'F1-LIBCOU-PEPACODPAY', 15 ),
        oEdtk::Field->new( 'F1-PERICODDEV',        3 ),
        oEdtk::Field->new( 'F1-LIBCOU-PERICODDEV', 15 ),

        # F1-DONNEES-COTISATIONS
        oEdtk::SignedField->new( 'F1-MT-ANNUEL-HT',    12, 2 ),
        oEdtk::SignedField->new( 'F1-MT-ANNUEL-TTC',   12, 2 ),
        oEdtk::SignedField->new( 'F1-PRIME-A-EMETTRE', 12, 2 ),
    );
}

1;
