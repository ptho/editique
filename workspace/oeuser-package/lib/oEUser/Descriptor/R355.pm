package oEUser::Descriptor::R355;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R355-LGNDAT
        oEdtk::Field->new( 'R355-ACDTCODETA',    8 ),
        oEdtk::Field->new( 'R355-ACDTDDEPRD',    8 ),
        oEdtk::Field->new( 'R355-ACDTMTFFDC',    5 ),
        oEdtk::Field->new( 'R355-LC-ACDTMTFFDC', 15 ),
        oEdtk::Field->new( 'R355-LL-ACDTMTFFDC', 50 ),
    );
}

1;
