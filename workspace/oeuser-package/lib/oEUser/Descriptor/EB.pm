package oEUser::Descriptor::EB;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # EB-LGNDAT
        oEdtk::Field->new( 'EB-ECCICODPAR',    3 ),
        oEdtk::Field->new( 'EB-LIB-PART',      50 ),
        oEdtk::Field->new( 'EB-ECCINUMCCO',    15 ),
        oEdtk::Field->new( 'EB-ECCICODGRA',    6 ),
        oEdtk::Field->new( 'EB-ACH7LIBAUT',    50 ),
        oEdtk::Field->new( 'EB-GAADPNTREM',    32 ),
        oEdtk::Field->new( 'EB-GAADCPLADR',    32 ),
        oEdtk::Field->new( 'EB-GAADNUMVOI',    9 ),
        oEdtk::Field->new( 'EB-GAADBTQVOI',    2 ),
        oEdtk::Field->new( 'EB-GAVONATVOI',    5 ),
        oEdtk::Field->new( 'EB-GAVOLIBVOI',    32 ),
        oEdtk::Field->new( 'EBG-INDCDX',       1 ),
        oEdtk::Field->new( 'EBG-BOIPOSLIBLDT', 32 ),
        oEdtk::Field->new( 'EBG-LIBLOCLIBLOC', 32 ),
        oEdtk::Field->new( 'EBG-CODCDXCODPOS', 10 ),
        oEdtk::Field->new( 'EBG-NOMCDXLIBACH', 32 ),
        oEdtk::Field->new( 'EBG-PEPACODPAY',   3 ),
        oEdtk::Field->new( 'EBG-TRNFAC',       4 ),
        oEdtk::Field->new( 'EB-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'EB-PENOCODCIV',    10 ),
        oEdtk::Field->new( 'EB-LIB-CIVILITE',  15 ),
        oEdtk::Field->new( 'EB-PENOCODTIT',    10 ),
        oEdtk::Field->new( 'EB-LIB-CODTIT',    15 ),
        oEdtk::Field->new( 'EB-ECDONOMSOU',    50 ),
        oEdtk::Field->new( 'EB-ECDOPRNSOU',    20 ),
        oEdtk::Field->new( 'EB-PEADPNTREM',    32 ),
        oEdtk::Field->new( 'EB-PEADCPLADR',    32 ),
        oEdtk::Field->new( 'EB-PEADNUMVOI',    9 ),
        oEdtk::Field->new( 'EB-PEADBTQVOI',    2 ),
        oEdtk::Field->new( 'EB-PEVONATVOI',    5 ),
        oEdtk::Field->new( 'EB-PEVOLIBVOI',    32 ),
        oEdtk::Field->new( 'EB-INDCDX',        1 ),
        oEdtk::Field->new( 'EB-BOIPOSLIBLDT',  32 ),
        oEdtk::Field->new( 'EB-LIBLOCLIBLOC',  32 ),
        oEdtk::Field->new( 'EB-CODCDXCODPOS',  10 ),
        oEdtk::Field->new( 'EB-NOMCDXLIBACH',  32 ),
        oEdtk::Field->new( 'EB-PEPACODPAY',    3 ),
        oEdtk::Field->new( 'EB-TRNFAC',        4 ),
        oEdtk::Field->new( 'EB-ECDOMODRGC',    2 ),
        oEdtk::Field->new( 'EB-LIB-MODRGC',    50 ),
        oEdtk::Field->new( 'EB-ECDOCODDVG',    3 ),
        oEdtk::Field->new( 'EB-LIB-DEVISE',    50 ),
        oEdtk::Field->new( 'EB-ECCINUMCIN',    15 ),
        oEdtk::Field->new( 'EB-ECDOQUAPAY',    1 ),
        oEdtk::Field->new( 'EB-LIB-QUAPAY',    50 ),
        oEdtk::Field->new( 'EB-DATE-ARRETE',   8 ),
        oEdtk::Field->new( 'EB-ECDOETACTR',    2 ),
        oEdtk::Field->new( 'EB-ECDODDEZRE',    8 ),
        oEdtk::SignedField->new( 'EB-ECD1MTTECH', 12, 2 ),
        oEdtk::SignedField->new( 'EB-ECD1MTTENC', 12, 2 ),
        oEdtk::SignedField->new( 'EB-ECZZ-SOLDE', 12, 2 ),
        oEdtk::Field->new( 'EB-INDIC-ECHPAI',       1 ),
        oEdtk::Field->new( 'EB-INDIC-ECHPAI-REGUL', 1 ),
        oEdtk::Field->new( 'EB-ECDOCODCGS',         9 ),
        oEdtk::Field->new( 'EB-LIBLON-CGS',         50 ),
        oEdtk::Field->new( 'EB-ECCICODERA',         9 ),
        oEdtk::Field->new( 'EB-ODRSCODRDI',         9 ),
        oEdtk::Field->new( 'EB-ACOFCODOFF',         10 ),
        oEdtk::Field->new( 'EB-ACH6NUECCO',         15 ),
        oEdtk::Field->new( 'EB-ECDOMODGPA',         1 ),
        oEdtk::Field->new( 'EB-ACTTIDFETB',         20 ),
        oEdtk::Field->new( 'EB-ACTTCODDPT',         3 ),
        oEdtk::Field->new( 'EB-ACTTLIBMAT',         20 ),

        # EB-CPL-ADR-CG				Complément adresse du groupe d'assuré
        oEdtk::Field->new( 'EB-PNTREM-GA',       50 ),
        oEdtk::Field->new( 'EB-CPLADR-GA',       50 ),
        oEdtk::Field->new( 'EB-LIBVOI-GA',       50 ),
        oEdtk::Field->new( 'EB-BOIPOSLIBLDT-GA', 50 ),
        oEdtk::Field->new( 'EB-LIBLOCLIBLOC-GA', 50 ),
        oEdtk::Field->new( 'EB-NOMCDXLIBACH-GA', 50 ),

        # EB-ADRESSE-SOUSCRIPTEUR    Complément adresse du souscripteur
        oEdtk::Field->new( 'EB-PNTREM-CSO',       50 ),
        oEdtk::Field->new( 'EB-CPLADR-CSO',       50 ),
        oEdtk::Field->new( 'EB-LIBVOI-CSO',       50 ),
        oEdtk::Field->new( 'EB-BOIPOSLIBLDT-CSO', 50 ),
        oEdtk::Field->new( 'EB-LIBLOCLIBLOC-CSO', 50 ),
        oEdtk::Field->new( 'EB-NOMCDXLIBACH-CSO', 50 ),
    );
}

1;
