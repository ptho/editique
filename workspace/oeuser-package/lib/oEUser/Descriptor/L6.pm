package oEUser::Descriptor::L6;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # L6-LGNDAT
        # L6-IDENT-CI
        oEdtk::Field->new( 'L6-NUMPART',    5 ),
        oEdtk::Field->new( 'L6-ACH1NUMCIN', 15 ),

        # L6-COTIS-PARTI
        oEdtk::SignedField->new( 'L6-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'L6-MONTANT-TTC', 12, 2 ),
        oEdtk::Field->new( 'L6-ECHIPU',     8 ),
        oEdtk::Field->new( 'L6-CODCEC',     9 ),
        oEdtk::Field->new( 'L6-LIC-CODCEC', 15 ),
        oEdtk::Field->new( 'L6-LIB-CODCEC', 50 ),
        oEdtk::Field->new( 'L6-TYPCEC',     6 ),
        oEdtk::Field->new( 'L6-LIC-TYPCEC', 15 ),
        oEdtk::Field->new( 'L6-LIB-TYPCEC', 50 ),
        oEdtk::Field->new( 'L6-TYPCNP',     1 ),
        oEdtk::Field->new( 'L6-LIC-TYPCNP', 15 ),
        oEdtk::Field->new( 'L6-LIB-TYPCNP', 50 ),
        oEdtk::Field->new( 'L6-MTFCNP',     5 ),
        oEdtk::Field->new( 'L6-LIC-MTFCNP', 15 ),
        oEdtk::Field->new( 'L6-LIB-MTFCNP', 50 ),

        # L6-INFO-OPE
        oEdtk::Field->new( 'L6-ACOPNUMOPE', 5 ),
        oEdtk::Field->new( 'L6-ACOPDDEOPE', 8 ),
        oEdtk::Field->new( 'L6-ACOPDDECOT', 8 ),
        oEdtk::Field->new( 'L6-ACOPNUMAVT', 3 ),
        oEdtk::Field->new( 'L6-ACOPCODAGC', 4 ),
        oEdtk::Field->new( 'L6-ACOPLICAGC', 15 ),
        oEdtk::Field->new( 'L6-ACOPLIBAGC', 50 ),
        oEdtk::Field->new( 'L6-ACOPCODCTX', 4 ),
        oEdtk::Field->new( 'L6-ACOPLICCTX', 15 ),
        oEdtk::Field->new( 'L6-ACOPLIBCTX', 50 ),
        oEdtk::Field->new( 'L6-ACOPCODTRT', 5 ),

        # L6-DATE-EXIGIBILITE
        oEdtk::Field->new( 'L6-DATEXI', 8 ),
    );
}

1;
