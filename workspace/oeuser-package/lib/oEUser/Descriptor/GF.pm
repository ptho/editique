package oEUser::Descriptor::GF;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # GF-LGNDAT
        oEdtk::Field->new( 'CO62-GF-ACBONUMBOR',     6 ),
        oEdtk::Field->new( 'CO62-GF-ECC1DATECH-SAI', 8 ),
        oEdtk::Field->new( 'CO62-GF-ECC1DATVAL',     8 ),
        oEdtk::SignedField->new( 'CO62-GF-MTENCBORD',  12, 2 ),
        oEdtk::SignedField->new( 'CO62-GF-MTRGLTRECU', 12, 2 ),
        oEdtk::Field->new( 'CO62-GF-ECRGCODMYE', 2 ),
        oEdtk::Field->new( 'CO62-GF-LIBCCODMYE', 15 ),
        oEdtk::Field->new( 'CO62-GF-LIBLCODMYE', 50 ),
        oEdtk::Field->new( 'CO62-GF-ECRGNUMCHQ', 9 ),
        oEdtk::Field->new( 'CO62-GF-DATECH',     8 ),

        # CO62-GF-RIB
        oEdtk::Field->new( 'CO62-GF-CODEBANQUE',    10 ),
        oEdtk::Field->new( 'CO62-GF-CODGUICHET',    10 ),
        oEdtk::Field->new( 'CO62-GF-NUMCPT',        20 ),
        oEdtk::Field->new( 'CO62-GF-CLERIB',        2 ),
        oEdtk::Field->new( 'CO62-GF-NOM-BANQUE',    20 ),
        oEdtk::Field->new( 'CO62-GF-TITULAIRE-CPT', 50 ),
    );
}

1;
