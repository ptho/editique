package oEUser::Descriptor::I6;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # I6-ENREG
        # I6-LIGNE-ADHESIONS
        oEdtk::Field->new( 'I6-CODERA',  9 ),
        oEdtk::Field->new( 'I6-NUMCCO',  15 ),
        oEdtk::Field->new( 'I6-NUMCIN',  15 ),
        oEdtk::Field->new( 'I6-GRPASS',  6 ),
        oEdtk::Field->new( 'I6-LIBASS',  50 ),
        oEdtk::Field->new( 'I6-NUMPART', 5 ),
        oEdtk::Field->new( 'I6-CODPART', 3 ),
        oEdtk::Field->new( 'I6-LIBPART', 50 ),
        oEdtk::SignedField->new( 'I6-MTT-DETTES',  12, 2 ),
        oEdtk::SignedField->new( 'I6-MTT-ENCAISS', 12, 2 ),
        oEdtk::SignedField->new( 'I6-MTT-SOLDE',   12, 2 ),
        oEdtk::Field->new( 'I6-MODGPA',        1 ),
        oEdtk::Field->new( 'I6-LIBMODGPA',     50 ),
        oEdtk::Field->new( 'I6-ECHPAL',        4 ),
        oEdtk::Field->new( 'I6-CODDVG',        3 ),
        oEdtk::Field->new( 'I6-LIBDVG',        50 ),
        oEdtk::Field->new( 'I6-DDEZRE',        8 ),
        oEdtk::Field->new( 'I6-ETACTR',        2 ),
        oEdtk::Field->new( 'I6-DDEPBA',        8 ),
        oEdtk::Field->new( 'I6-DFEPBA',        8 ),
        oEdtk::Field->new( 'I6-CODECHPBA',     2 ),
        oEdtk::Field->new( 'I6-LIB-ECHPBA',    50 ),
        oEdtk::Field->new( 'I6-FRQRGC',        2 ),
        oEdtk::Field->new( 'I6-MODRGC',        2 ),
        oEdtk::Field->new( 'I6-LIB-MODRGC',    50 ),
        oEdtk::Field->new( 'I6-CODODN',        5 ),
        oEdtk::Field->new( 'I6-CODCGS',        9 ),
        oEdtk::Field->new( 'I6-CODGGS',        9 ),
        oEdtk::Field->new( 'I6-CODRDI',        9 ),
        oEdtk::Field->new( 'I6-LIBRDI',        50 ),
        oEdtk::Field->new( 'I6-PEREI2',        8 ),
        oEdtk::Field->new( 'I6-CIVILITE',      10 ),
        oEdtk::Field->new( 'I6-NOM-PAY',       50 ),
        oEdtk::Field->new( 'I6-PRENOM',        20 ),
        oEdtk::Field->new( 'I6-PEREI1',        8 ),
        oEdtk::Field->new( 'I6-CIVILITE-SOUS', 10 ),
        oEdtk::Field->new( 'I6-NOM-SOUS',      50 ),
        oEdtk::Field->new( 'I6-PRENOM-SOUS',   20 ),
    );
}

1;
