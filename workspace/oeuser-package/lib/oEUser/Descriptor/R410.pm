package oEUser::Descriptor::R410;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R410-LGNDAT
        oEdtk::Field->new( 'R410-ACDSACASR2',     2 ),
        oEdtk::Field->new( 'R410-ACDSCODDECL',    2 ),
        oEdtk::Field->new( 'R410-LC-ACDSCODDECL', 15 ),
        oEdtk::Field->new( 'R410-LL-ACDSCODDECL', 50 ),
        oEdtk::Field->new( 'R410-ACDSDDEPRD',     8 ),
        oEdtk::Field->new( 'R410-ACDSDFEPRD',     8 ),
        oEdtk::Field->new( 'R410-ACDSINDDCL',     1 ),
    );
}

1;
