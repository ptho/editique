package oEUser::Descriptor::R330;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R330-LGNDAT
        oEdtk::Field->new( 'R330-ACBNNUMBLN', 5 ),
        oEdtk::Field->new( 'R330-ACBNDDEPRD', 8 ),
        oEdtk::Field->new( 'R330-ACBNDFEPRD', 8 ),
        oEdtk::Field->new( 'R330-ACBNTEXCL1', 250 ),
        oEdtk::Field->new( 'R330-ACBNTEXCL2', 250 ),
        oEdtk::Field->new( 'R330-ACBNTEXCL3', 250 ),
        oEdtk::Field->new( 'R330-ACBNTEXCL4', 250 ),
        oEdtk::Field->new( 'R330-ACBNACASR2', 2 ),
    );
}

1;
