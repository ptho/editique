package oEUser::Descriptor::J2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # J2-LGNDAT
        # J2-INFO-FRAIS
        oEdtk::Field->new( 'J2-CODE-FRAIS',   2 ),
        oEdtk::Field->new( 'J2-LIBCOU-FRAIS', 15 ),
        oEdtk::Field->new( 'J2-LIBLON-FRAIS', 50 ),
        oEdtk::SignedField->new( 'J2-MONTANT-HT', 12, 2 ),
        oEdtk::Field->new( 'J2-ACFSDDEPRD',   8 ),
        oEdtk::Field->new( 'J2-ACFSDFEPRD',   8 ),
        oEdtk::Field->new( 'J2-IDFSYS-FRAIS', 5 ),

        # J2-INFO-TAXES
        oEdtk::Field->new( 'J2-CODTAX1', 6 ),
        oEdtk::SignedField->new( 'J2-TAUX-TAX1',    4,  5 ),
        oEdtk::SignedField->new( 'J2-MONTANT-TAX1', 12, 2 ),
        oEdtk::Field->new( 'J2-IDFSYS-TAX1', 8 ),
        oEdtk::Field->new( 'J2-LIBLON-TAX1', 50 ),
        oEdtk::Field->new( 'J2-CODTAX2',     6 ),
        oEdtk::SignedField->new( 'J2-TAUX-TAX2',    4,  5 ),
        oEdtk::SignedField->new( 'J2-MONTANT-TAX2', 12, 2 ),
        oEdtk::Field->new( 'J2-IDFSYS-TAX2', 8 ),
        oEdtk::Field->new( 'J2-LIBLON-TAX2', 50 ),
        oEdtk::Field->new( 'J2-CODTAX3',     6 ),
        oEdtk::SignedField->new( 'J2-TAUX-TAX3',    4,  5 ),
        oEdtk::SignedField->new( 'J2-MONTANT-TAX3', 12, 2 ),
        oEdtk::Field->new( 'J2-IDFSYS-TAX3', 8 ),
        oEdtk::Field->new( 'J2-LIBLON-TAX3', 50 ),
        oEdtk::Field->new( 'J2-CODTAX4',     6 ),
        oEdtk::SignedField->new( 'J2-TAUX-TAX4',    4,  5 ),
        oEdtk::SignedField->new( 'J2-MONTANT-TAX4', 12, 2 ),
        oEdtk::Field->new( 'J2-IDFSYS-TAX4', 8 ),
        oEdtk::Field->new( 'J2-LIBLON-TAX4', 50 ),
        oEdtk::Field->new( 'J2-CODTAX5',     6 ),
        oEdtk::SignedField->new( 'J2-TAUX-TAX5',    4,  5 ),
        oEdtk::SignedField->new( 'J2-MONTANT-TAX5', 12, 2 ),
        oEdtk::Field->new( 'J2-IDFSYS-TAX5', 8 ),
        oEdtk::Field->new( 'J2-LIBLON-TAX5', 50 ),
        oEdtk::Field->new( 'J2-FILLER',      250 ),
    );
}

1;
