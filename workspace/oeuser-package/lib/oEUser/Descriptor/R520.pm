package oEUser::Descriptor::R520;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R520-LGNDAT
        # R520-LST-INDEMNITES
        oEdtk::Field->new( 'R520-ACIJACASR2', 2 ),
        oEdtk::Field->new( 'R520-ACIJCODGAR', 10 ),
        oEdtk::Field->new( 'R520-ACIJCODPRT', 10 ),
        oEdtk::Field->new( 'R520-ACIJUNICAL', 5 ),
        oEdtk::SignedField->new( 'R520-ACIJVALMNT', 12, 2 ),
        oEdtk::SignedField->new( 'R520-ACIJVALTAU', 4,  5 ),
        oEdtk::Field->new( 'R520-ACIJINDINX', 1 ),
        oEdtk::SignedField->new( 'R520-ACEMSLRREF', 12, 2 ),
        oEdtk::Field->new( 'R520-PRFRCODDVE',    4 ),
        oEdtk::Field->new( 'R520-LC-PRFRCODDVE', 15 ),
        oEdtk::Field->new( 'R520-LL-PRFRCODDVE', 50 ),
        oEdtk::Field->new( 'R520-PRFRCODFRD',    6 ),
        oEdtk::Field->new( 'R520-PRFRLIBLON',    50 ),
        oEdtk::Field->new( 'R520-PRFRRIS01',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA01',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN1', 50 ),
        oEdtk::Field->new( 'R520-PRFRRIS02',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA02',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN2', 50 ),
        oEdtk::Field->new( 'R520-PRFRRIS03',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA03',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN3', 50 ),

        # R520-LST-INDEMNITES-2
        oEdtk::Field->new( 'R520-ACIJACASR2-2', 2 ),
        oEdtk::Field->new( 'R520-ACIJCODGAR-2', 10 ),
        oEdtk::Field->new( 'R520-ACIJCODPRT-2', 10 ),
        oEdtk::Field->new( 'R520-ACIJUNICAL-2', 5 ),
        oEdtk::SignedField->new( 'R520-ACIJVALMNT-2', 12, 2 ),
        oEdtk::SignedField->new( 'R520-ACIJVALTAU-2', 4,  5 ),
        oEdtk::Field->new( 'R520-ACIJINDINX-2', 1 ),
        oEdtk::SignedField->new( 'R520-ACEMSLRREF-2', 12, 2 ),
        oEdtk::Field->new( 'R520-PRFRCODDVE-2',    4 ),
        oEdtk::Field->new( 'R520-LC-PRFRCODDVE-2', 15 ),
        oEdtk::Field->new( 'R520-LL-PRFRCODDVE-2', 50 ),
        oEdtk::Field->new( 'R520-PRFRCODFRD-2',    6 ),
        oEdtk::Field->new( 'R520-PRFRLIBLON-2',    50 ),
        oEdtk::Field->new( 'R520-PRFRRIS01-2',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA01-2',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN1-2', 50 ),
        oEdtk::Field->new( 'R520-PRFRRIS02-2',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA02-2',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN2-2', 50 ),
        oEdtk::Field->new( 'R520-PRFRRIS03-2',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA03-2',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN3-2', 50 ),

        # R520-LST-INDEMNITES-3
        oEdtk::Field->new( 'R520-ACIJACASR2-3', 2 ),
        oEdtk::Field->new( 'R520-ACIJCODGAR-3', 10 ),
        oEdtk::Field->new( 'R520-ACIJCODPRT-3', 10 ),
        oEdtk::Field->new( 'R520-ACIJUNICAL-3', 5 ),
        oEdtk::SignedField->new( 'R520-ACIJVALMNT-3', 12, 2 ),
        oEdtk::SignedField->new( 'R520-ACIJVALTAU-3', 4,  5 ),
        oEdtk::Field->new( 'R520-ACIJINDINX-3', 1 ),
        oEdtk::SignedField->new( 'R520-ACEMSLRREF-3', 12, 2 ),
        oEdtk::Field->new( 'R520-PRFRCODDVE-3',    4 ),
        oEdtk::Field->new( 'R520-LC-PRFRCODDVE-3', 15 ),
        oEdtk::Field->new( 'R520-LL-PRFRCODDVE-3', 50 ),
        oEdtk::Field->new( 'R520-PRFRCODFRD-3',    6 ),
        oEdtk::Field->new( 'R520-PRFRLIBLON-3',    50 ),
        oEdtk::Field->new( 'R520-PRFRRIS01-3',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA01-3',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN1-3', 50 ),
        oEdtk::Field->new( 'R520-PRFRRIS02-3',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA02-3',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN2-3', 50 ),
        oEdtk::Field->new( 'R520-PRFRRIS03-3',     6 ),
        oEdtk::Field->new( 'R520-PRFRFRA03-3',     4 ),
        oEdtk::Field->new( 'R520-PRFR-LC-FRAN3-3', 50 ),
    );
}

1;
