package oEUser::Descriptor::F2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # F2-LGNDAT
        oEdtk::Field->new( 'F2-ACCLNUMCAU',        5 ),
        oEdtk::Field->new( 'F2-ACCLDDEPRD',        8 ),
        oEdtk::Field->new( 'F2-ACCLDFEPRD',        8 ),
        oEdtk::Field->new( 'F2-ACCLTYPTXT',        1 ),
        oEdtk::Field->new( 'F2-LIBCOU-ACCLTYPTXT', 15 ),
        oEdtk::Field->new( 'F2-ACCLPRCLI1',        8 ),

        # F2-TEXTE-CLAUSE
        oEdtk::Field->new( 'F2-TEXCL1',     250 ),
        oEdtk::Field->new( 'F2-TEXCL2',     250 ),
        oEdtk::Field->new( 'F2-TEXCL3',     250 ),
        oEdtk::Field->new( 'F2-TEXCL4',     250 ),
        oEdtk::Field->new( 'F2-RESERVE-F2', 200 ),
    );
}

1;
