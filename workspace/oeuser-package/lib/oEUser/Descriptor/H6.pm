package oEUser::Descriptor::H6;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H6-LGNDAT
        oEdtk::Field->new( 'H6-ACBEACASR2', 2 ),
        oEdtk::Field->new( 'H6-ACH4CODGAR', 10 ),
        oEdtk::Field->new( 'H6-ACH4CODPRT', 10 ),
        oEdtk::Field->new( 'H6-ACBEACGSR3', 5 ),
        oEdtk::Field->new( 'H6-ACBEACGSR4', 5 ),
        oEdtk::Field->new( 'H6-ACBENUMBEN', 5 ),
        oEdtk::Field->new( 'H6-ACBEPEREI1', 8 ),
        oEdtk::Field->new( 'H6-ACBEDDEPRD', 8 ),
        oEdtk::Field->new( 'H6-ACBEDFEPRD', 8 ),
        oEdtk::Field->new( 'H6-ACBERNGBEN', 2 ),
        oEdtk::SignedField->new( 'H6-ACBEPRCBEN', 4, 5 ),
        oEdtk::Field->new( 'H6-ACBEINDBFA',        1 ),
        oEdtk::Field->new( 'H6-PENOCODCIV',        10 ),
        oEdtk::Field->new( 'H6-LIBCOU-PENOCODCIV', 15 ),
        oEdtk::Field->new( 'H6-PENOCODTIT',        10 ),
        oEdtk::Field->new( 'H6-LIBCOU-PENOCODTIT', 15 ),
        oEdtk::Field->new( 'H6-PENOLIBLON',        50 ),
        oEdtk::Field->new( 'H6-PENOPRN01',         20 ),
        oEdtk::Field->new( 'H6-ACBEDATNAI',        8 ),
        oEdtk::Field->new( 'H6-RESERVE-H6',        200 ),
    );
}

1;
