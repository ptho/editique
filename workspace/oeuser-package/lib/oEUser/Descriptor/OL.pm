package oEUser::Descriptor::OL;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # HO-LGNDAT
        # HO-INFO-OPERATION
        oEdtk::Field->new( 'OL-ACH2NUMCIN', 15 ),
        oEdtk::Field->new( 'OL-ACH3CODPRT', 10 ),
        oEdtk::Field->new( 'OL-PRPRLIBCOU', 15 ),
        oEdtk::Field->new( 'OL-PRPRLIBLON', 50 ),
        oEdtk::Field->new( 'OL-ACH4CODGAR', 10 ),
        oEdtk::Field->new( 'OL-PRGALIBCOU', 15 ),
        oEdtk::Field->new( 'OL-PRGALIBLON', 50 ),
        oEdtk::Field->new( 'OL-ACIJUNICAL', 5 ),
        oEdtk::SignedField->new( 'OL-ACIJVALMNT', 12, 2 ),
        oEdtk::SignedField->new( 'OL-ACIJVALTAU', 4,  5 ),
        oEdtk::Field->new( 'OL-ACKGUNICAL', 5 ),
        oEdtk::SignedField->new( 'OL-ACKGVALMNT', 12, 2 ),
        oEdtk::SignedField->new( 'OL-ACKGVALTAU', 4,  5 ),
    );
}

1;
