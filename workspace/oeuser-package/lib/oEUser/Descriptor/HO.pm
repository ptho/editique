package oEUser::Descriptor::HO;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # HO-LGNDAT
        # HO-INFO-OPERATION
        oEdtk::Field->new( 'HO-ACEOCODETA', 2 ),
        oEdtk::Field->new( 'HO-ACEOMTFINT', 5 ),
        oEdtk::Field->new( 'HO-LIBCMTFINT', 15 ),
        oEdtk::Field->new( 'HO-LIBLMTFINT', 50 ),
        oEdtk::Field->new( 'HO-ACEOMTFRFU', 5 ),
        oEdtk::Field->new( 'HO-LIBCMTFRFU', 15 ),
        oEdtk::Field->new( 'HO-LIBLMTFRFU', 50 ),
    );
}

1;
