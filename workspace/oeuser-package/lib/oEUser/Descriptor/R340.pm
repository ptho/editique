package oEUser::Descriptor::R340;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R340-LGNDAT
        # R340-OPE-COMM
        oEdtk::Field->new( 'R340-PROCCODOPC', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON', 50 ),

        # R340-OPE-COMM-2
        oEdtk::Field->new( 'R340-PROCCODOPC-2', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-2', 50 ),

        # R340-OPE-COMM-3
        oEdtk::Field->new( 'R340-PROCCODOPC-3', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-3', 50 ),

        # R340-OPE-COMM-4
        oEdtk::Field->new( 'R340-PROCCODOPC-4', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-4', 50 ),

        # R340-OPE-COMM-5
        oEdtk::Field->new( 'R340-PROCCODOPC-5', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-5', 50 ),

        # R340-OPE-COMM-6
        oEdtk::Field->new( 'R340-PROCCODOPC-6', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-6', 50 ),

        # R340-OPE-COMM-7
        oEdtk::Field->new( 'R340-PROCCODOPC-7', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-7', 50 ),

        # R340-OPE-COMM-8
        oEdtk::Field->new( 'R340-PROCCODOPC-8', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-8', 50 ),

        # R340-OPE-COMM-9
        oEdtk::Field->new( 'R340-PROCCODOPC-9', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-9', 50 ),

        # R340-OPE-COMM-10
        oEdtk::Field->new( 'R340-PROCCODOPC-10', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-10', 50 ),

        # R340-OPE-COMM-11
        oEdtk::Field->new( 'R340-PROCCODOPC-11', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-11', 50 ),

        # R340-OPE-COMM-12
        oEdtk::Field->new( 'R340-PROCCODOPC-12', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-12', 50 ),

        # R340-OPE-COMM-13
        oEdtk::Field->new( 'R340-PROCCODOPC-13', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-13', 50 ),

        # R340-OPE-COMM-14
        oEdtk::Field->new( 'R340-PROCCODOPC-14', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-14', 50 ),

        # R340-OPE-COMM-15
        oEdtk::Field->new( 'R340-PROCCODOPC-15', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-15', 50 ),

        # R340-OPE-COMM-16
        oEdtk::Field->new( 'R340-PROCCODOPC-16', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-16', 50 ),

        # R340-OPE-COMM-17
        oEdtk::Field->new( 'R340-PROCCODOPC-17', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-17', 50 ),

        # R340-OPE-COMM-18
        oEdtk::Field->new( 'R340-PROCCODOPC-18', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-18', 50 ),

        # R340-OPE-COMM-19
        oEdtk::Field->new( 'R340-PROCCODOPC-19', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-19', 50 ),

        # R340-OPE-COMM-20
        oEdtk::Field->new( 'R340-PROCCODOPC-20', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-20', 50 ),

        # R340-OPE-COMM-21
        oEdtk::Field->new( 'R340-PROCCODOPC-21', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-21', 50 ),

        # R340-OPE-COMM-22
        oEdtk::Field->new( 'R340-PROCCODOPC-22', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-22', 50 ),

        # R340-OPE-COMM-23
        oEdtk::Field->new( 'R340-PROCCODOPC-23', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-23', 50 ),

        # R340-OPE-COMM-24
        oEdtk::Field->new( 'R340-PROCCODOPC-24', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-24', 50 ),

        # R340-OPE-COMM-25
        oEdtk::Field->new( 'R340-PROCCODOPC-25', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-25', 50 ),

        # R340-OPE-COMM-26
        oEdtk::Field->new( 'R340-PROCCODOPC-26', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-26', 50 ),

        # R340-OPE-COMM-27
        oEdtk::Field->new( 'R340-PROCCODOPC-27', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-27', 50 ),

        # R340-OPE-COMM-28
        oEdtk::Field->new( 'R340-PROCCODOPC-28', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-28', 50 ),

        # R340-OPE-COMM-29
        oEdtk::Field->new( 'R340-PROCCODOPC-29', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-29', 50 ),

        # R340-OPE-COMM-30
        oEdtk::Field->new( 'R340-PROCCODOPC-30', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-30', 50 ),

        # R340-OPE-COMM-31
        oEdtk::Field->new( 'R340-PROCCODOPC-31', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-31', 50 ),

        # R340-OPE-COMM-32
        oEdtk::Field->new( 'R340-PROCCODOPC-32', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-32', 50 ),

        # R340-OPE-COMM-33
        oEdtk::Field->new( 'R340-PROCCODOPC-33', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-33', 50 ),

        # R340-OPE-COMM-34
        oEdtk::Field->new( 'R340-PROCCODOPC-34', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-34', 50 ),

        # R340-OPE-COMM-35
        oEdtk::Field->new( 'R340-PROCCODOPC-35', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-35', 50 ),

        # R340-OPE-COMM-36
        oEdtk::Field->new( 'R340-PROCCODOPC-36', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-36', 50 ),

        # R340-OPE-COMM-37
        oEdtk::Field->new( 'R340-PROCCODOPC-37', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-37', 50 ),

        # R340-OPE-COMM-38
        oEdtk::Field->new( 'R340-PROCCODOPC-38', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-38', 50 ),

        # R340-OPE-COMM-39
        oEdtk::Field->new( 'R340-PROCCODOPC-39', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-39', 50 ),

        # R340-OPE-COMM-40
        oEdtk::Field->new( 'R340-PROCCODOPC-40', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-40', 50 ),

        # R340-OPE-COMM-41
        oEdtk::Field->new( 'R340-PROCCODOPC-41', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-41', 50 ),

        # R340-OPE-COMM-42
        oEdtk::Field->new( 'R340-PROCCODOPC-42', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-42', 50 ),

        # R340-OPE-COMM-43
        oEdtk::Field->new( 'R340-PROCCODOPC-43', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-43', 50 ),

        # R340-OPE-COMM-44
        oEdtk::Field->new( 'R340-PROCCODOPC-44', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-44', 50 ),

        # R340-OPE-COMM-45
        oEdtk::Field->new( 'R340-PROCCODOPC-45', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-45', 50 ),

        # R340-OPE-COMM-46
        oEdtk::Field->new( 'R340-PROCCODOPC-46', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-46', 50 ),

        # R340-OPE-COMM-47
        oEdtk::Field->new( 'R340-PROCCODOPC-47', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-47', 50 ),

        # R340-OPE-COMM-48
        oEdtk::Field->new( 'R340-PROCCODOPC-48', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-48', 50 ),

        # R340-OPE-COMM-49
        oEdtk::Field->new( 'R340-PROCCODOPC-49', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-49', 50 ),

        # R340-OPE-COMM-50
        oEdtk::Field->new( 'R340-PROCCODOPC-50', 6 ),
        oEdtk::Field->new( 'R340-PROCLIBLON-50', 50 ),
    );
}

1;
