package oEUser::Descriptor::R620;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R620-LGNDAT
        oEdtk::Field->new( 'R620-PEPENUMPER',  8 ),
        oEdtk::Field->new( 'R620-PEAFCODGRE',  2 ),
        oEdtk::Field->new( 'R620-PEAFCODCPA',  4 ),
        oEdtk::Field->new( 'R620-PEAFCODCRO',  3 ),
        oEdtk::Field->new( 'R620-PEAFNUMSEE',  13 ),
        oEdtk::Field->new( 'R620-PEAFCLESEE',  2 ),
        oEdtk::Field->new( 'R620-PEAFCODRGI',  4 ),
        oEdtk::Field->new( 'R620-PERODDEDRO',  8 ),
        oEdtk::Field->new( 'R620-PERODFEDRO',  8 ),
        oEdtk::Field->new( 'R620-PEROCODTAS',  1 ),
        oEdtk::Field->new( 'R620-PEROMTFDRO',  2 ),
        oEdtk::Field->new( 'R620-PEROTYPDNA',  2 ),
        oEdtk::Field->new( 'R620-PERODATNAI',  8 ),
        oEdtk::Field->new( 'R620-PERORNGRO',   1 ),
        oEdtk::Field->new( 'R620-PERODFEPRS',  8 ),
        oEdtk::Field->new( 'R620-PEROMTFPRS',  3 ),
        oEdtk::Field->new( 'R620-PERODFEVIT',  8 ),
        oEdtk::Field->new( 'R620-PEROMAJVIT',  8 ),
        oEdtk::Field->new( 'R620-PEROCODDPT',  3 ),
        oEdtk::Field->new( 'R620-PEROCODRGP',  3 ),
        oEdtk::Field->new( 'R620-PERODDERGP',  8 ),
        oEdtk::Field->new( 'R620-PEROCODRGS',  3 ),
        oEdtk::Field->new( 'R620-PERODDERGS',  8 ),
        oEdtk::Field->new( 'R620-PERODDEIMA',  8 ),
        oEdtk::Field->new( 'R620-PEROCOGREP',  2 ),
        oEdtk::Field->new( 'R620-PEROCOCROP',  3 ),
        oEdtk::Field->new( 'R620-PEROCOCPAP',  4 ),
        oEdtk::Field->new( 'R620-PEROCODPTP',  3 ),
        oEdtk::Field->new( 'R620-PEROCODRGM',  3 ),
        oEdtk::Field->new( 'R620-PERONUMCIN',  15 ),
        oEdtk::Field->new( 'R620-PERODDE',     8 ),
        oEdtk::Field->new( 'R620-PERODFE',     8 ),
        oEdtk::Field->new( 'R620-PERONUMCIN2', 15 ),
        oEdtk::Field->new( 'R620-PERONUMOPE',  5 ),
    );
}

1;
