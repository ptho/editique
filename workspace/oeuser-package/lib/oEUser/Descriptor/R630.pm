package oEUser::Descriptor::R630;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R630-LGNDAT
        oEdtk::Field->new( 'R630-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R630-PECMDDE',       8 ),
        oEdtk::Field->new( 'R620-PECMDFE',       8 ),
        oEdtk::Field->new( 'R630-PECMNUMREN',    9 ),
        oEdtk::Field->new( 'R630-PECMNUMRET',    5 ),
        oEdtk::Field->new( 'R630-PECMCODAPE',    5 ),
        oEdtk::Field->new( 'R630-PECMCODSJU',    5 ),
        oEdtk::Field->new( 'R630-LL-PECMCODSJU', 50 ),
        oEdtk::Field->new( 'R630-LC-PECMCODSJU', 15 ),
        oEdtk::Field->new( 'R630-PECMCODOPF',    2 ),
        oEdtk::Field->new( 'R630-LL-PECMCODOPF', 50 ),
        oEdtk::Field->new( 'R630-LC-PECMCODOPF', 15 ),
        oEdtk::Field->new( 'R630-PECMMOICTA',    4 ),
        oEdtk::Field->new( 'R630-PECMDDEACT',    8 ),
        oEdtk::Field->new( 'R630-PECMDFEACT',    8 ),
        oEdtk::Field->new( 'R630-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R630-ACOPNUMOPE',    5 ),
    );
}

1;
