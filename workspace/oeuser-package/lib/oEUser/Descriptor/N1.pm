package oEUser::Descriptor::N1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # N1-LGNDAT
        # N1-COTIS-PARTI
        oEdtk::Field->new( 'N1-TYPCNP',        1 ),
        oEdtk::Field->new( 'N1-LIBCOU-TYPCNP', 15 ),
        oEdtk::Field->new( 'N1-LIBLON-TYPCNP', 50 ),
        oEdtk::SignedField->new( 'N1-MONTANT-HT', 12, 2 ),
        oEdtk::Field->new( 'N1-FILLER', 250 ),
    );
}

1;
