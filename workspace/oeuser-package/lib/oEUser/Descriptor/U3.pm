package oEUser::Descriptor::U3;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # SCC-U3-ENRADR
        # SCC-210-IDFADR
        oEdtk::Field->new( 'SCC-210-NUMFLX', 20 ),
        oEdtk::Field->new( 'SCC-210-NUMLOT', 6 ),
        oEdtk::Field->new( 'SCC-210-NUMDOS', 6 ),
        oEdtk::Field->new( 'SCC-210-NUMLCO', 6 ),
        oEdtk::Field->new( 'SCC-210-NUMLGA', 6 ),
        oEdtk::Field->new( 'SCC-210-TYPENR', 3 ),
        oEdtk::Field->new( 'SCC-210-STYENR', 3 ),

        # SCC-210-DONADR
        oEdtk::Field->new( 'SCC-210-DSTRGL', 20 ),
        oEdtk::Field->new( 'SCC-210-CODCIV', 5 ),
        oEdtk::Field->new( 'SCC-210-NOMBEN', 30 ),
        oEdtk::Field->new( 'SCC-210-PRNBEN', 30 ),
        oEdtk::Field->new( 'SCC-210-NUMVOI', 5 ),
        oEdtk::Field->new( 'SCC-210-BTQVOI', 1 ),
        oEdtk::Field->new( 'SCC-210-NATVOI', 5 ),
        oEdtk::Field->new( 'SCC-210-LIBVOI', 30 ),
        oEdtk::Field->new( 'SCC-210-CMPADR', 30 ),
        oEdtk::Field->new( 'SCC-210-COMMUN', 30 ),
        oEdtk::Field->new( 'SCC-210-CODPOS', 5 ),
        oEdtk::Field->new( 'SCC-210-BURDIS', 30 ),
        oEdtk::Field->new( 'SCC-210-MDPDST', 3 ),
        oEdtk::Field->new( 'SCC-210-CIVCPT', 5 ),
        oEdtk::Field->new( 'SCC-210-NOMCPT', 30 ),
        oEdtk::Field->new( 'SCC-210-PRNCPT', 30 ),
        oEdtk::Field->new( 'SCC-210-CODBQE', 5 ),
        oEdtk::Field->new( 'SCC-210-CODGUI', 5 ),
        oEdtk::Field->new( 'SCC-210-NUMCPT', 11 ),
        oEdtk::Field->new( 'SCC-210-CLECPT', 2 ),
    );
}

1;
