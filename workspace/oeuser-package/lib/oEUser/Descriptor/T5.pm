package oEUser::Descriptor::T5;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # T5-LGNDAT
        # T5-IDENT-PART
        oEdtk::Field->new( 'T5-NUMPART', 5 ),
        oEdtk::Field->new( 'T5-FILLER',  15 ),

        # T5-INFO-FRAIS
        oEdtk::SignedField->new( 'T5-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'T5-MONTANT-TTC', 12, 2 ),
        oEdtk::Field->new( 'T5-ECHIPU',       8 ),
        oEdtk::Field->new( 'T5-CODFRS',       2 ),
        oEdtk::Field->new( 'T5-LIBCOU-FRAIS', 15 ),
        oEdtk::Field->new( 'T5-LIBLON-FRAIS', 50 ),

        # T5-HORS-PERIODE-DIFF-C-I
        oEdtk::Field->new( 'T5-FILLER1', 1 ),
        oEdtk::SignedField->new( 'T5-TOT-CONSO-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T5-FILLER2', 1 ),
        oEdtk::SignedField->new( 'T5-TOT-IND-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T5-FILLER3', 1 ),
        oEdtk::SignedField->new( 'T5-DIFF-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T5-FILLER4',              1 ),
        oEdtk::Field->new( 'T5-TEST-YA-DIF-HORS-PER', 2 ),

        # T5-YA-DIFF-HORS-PER
        oEdtk::Field->new( 'T5-DATEXI', 8 ),
    );
}

1;
