package oEUser::Descriptor::L9;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # L9-LGNDAT
        oEdtk::Field->new( 'L9-ACN1CODSIT',  2 ),
        oEdtk::Field->new( 'L9-ACN1CODTYR',  2 ),
        oEdtk::Field->new( 'L9-ACN1LIBCTYR', 15 ),
        oEdtk::Field->new( 'L9-ENCTCODCTI',  9 ),

        # L9-ENCTNOM
        oEdtk::Field->new( 'L9-ENCTCODTIT', 10 ),
        oEdtk::Field->new( 'L9-ENCTLICTIT', 15 ),
        oEdtk::Field->new( 'L9-ENCTLIBLON', 50 ),
        oEdtk::Field->new( 'L9-ENACCODACD', 10 ),
        oEdtk::Field->new( 'L9-ENCRCODGRE', 2 ),
        oEdtk::Field->new( 'L9-ENCRCODCRO', 3 ),

        # L9-ENCRNOM
        oEdtk::Field->new( 'L9-ENCRCODTIT', 10 ),
        oEdtk::Field->new( 'L9-ENCRLICTIT', 15 ),
        oEdtk::Field->new( 'L9-ENCRLIBLON', 50 ),
        oEdtk::Field->new( 'L9-ODERCODERA', 9 ),

        # L9-ODERNOMERA
        oEdtk::Field->new( 'L9-ODERCODTIT',    10 ),
        oEdtk::Field->new( 'L9-ODERLICTIT',    15 ),
        oEdtk::Field->new( 'L9-ODERLIBLON',    50 ),
        oEdtk::Field->new( 'L9-ACTION-RETOUR', 10 ),
        oEdtk::Field->new( 'L9-LIBCART',       15 ),
        oEdtk::Field->new( 'L9-LIBLART',       50 ),
        oEdtk::Field->new( 'L9-ACN1NUMTRS',    8 ),
        oEdtk::Field->new( 'L9-ACN1CODMRO',    13 ),
        oEdtk::Field->new( 'L9-ACN1NATNOM',    1 ),
        oEdtk::Field->new( 'L9-ACN1NOMASU',    50 ),
        oEdtk::Field->new( 'L9-ACN1PRNASU',    15 ),
        oEdtk::Field->new( 'L9-ACN1DATNAI',    8 ),
        oEdtk::Field->new( 'L9-ACN1RNGNAI',    1 ),
        oEdtk::Field->new( 'L9-ACN1NOMAYD',    50 ),
        oEdtk::Field->new( 'L9-ACN1PRNAYD',    15 ),
        oEdtk::Field->new( 'L9-ACN1NUMCIN',    15 ),
        oEdtk::Field->new( 'L9-ACN1ACOPR1',    8 ),
        oEdtk::Field->new( 'L9-ACN1ACASR2',    2 ),
        oEdtk::Field->new( 'L9-ACN1ACOPR4',    5 ),
        oEdtk::Field->new( 'L9-ACN1CODMFR',    10 ),
        oEdtk::Field->new( 'L9-ACN1LICMFR',    15 ),
        oEdtk::Field->new( 'L9-ACN1LILMFR',    50 ),
        oEdtk::Field->new( 'L9-ACZZ-LIBMTF',   50 ),
        oEdtk::Field->new( 'L9-ACN1DEVNMI',    8 ),
        oEdtk::Field->new( 'L9-ACN1CLEMRO',    2 ),
        oEdtk::Field->new( 'L9-MOTIF-LIBANO',  100 ),
        oEdtk::Field->new( 'L9-MOTIF-TRTANO',  300 ),
        oEdtk::Field->new( 'L9-ODASCODASR',    9 ),
    );
}

1;
