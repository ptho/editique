package oEUser::Descriptor::EV;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # EV-ENREG
        oEdtk::SignedField->new( 'EV-MTRGDEV', 12, 2 ),
        oEdtk::Field->new( 'EV-CODDEV',      3 ),
        oEdtk::Field->new( 'EV-LIBDEV',      50 ),
        oEdtk::Field->new( 'EV-DAT-SAIS',    8 ),
        oEdtk::Field->new( 'EV-CODUTIL',     8 ),
        oEdtk::Field->new( 'EV-CODCIV-UTIL', 8 ),
        oEdtk::Field->new( 'EV-LIBCIV-UTIL', 15 ),
        oEdtk::Field->new( 'EV-CODTIT-UTIL', 8 ),
        oEdtk::Field->new( 'EV-LIBTIT-UTIL', 15 ),
        oEdtk::Field->new( 'EV-NOM-UTIL',    30 ),
        oEdtk::Field->new( 'EV-PRENOM-UTIL', 30 ),
        oEdtk::Field->new( 'EV-CODMYENC',    2 ),
        oEdtk::Field->new( 'EV-LIBMYE',      50 ),
        oEdtk::Field->new( 'EV-NUMLOT',      5 ),
        oEdtk::Field->new( 'EV-NUMCHQ',      9 ),
        oEdtk::Field->new( 'EV-NOMBQE',      20 ),
        oEdtk::Field->new( 'EV-CODCHQ',      1 ),
        oEdtk::Field->new( 'EV-LIBCHQ',      50 ),
        oEdtk::Field->new( 'EV-TITCPT',      50 ),
        oEdtk::Field->new( 'EV-NOREG',       9 ),
        oEdtk::Field->new( 'EV-NUMOPE-REGL', 5 ),
        oEdtk::Field->new( 'EV-VIDE',        2133 ),
        oEdtk::Field->new( 'EV-NUMSECT',     4 ),
    );
}

1;
