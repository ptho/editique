package oEUser::Descriptor::G6;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # G6-LGNDAT
        oEdtk::Field->new( 'G6-DATE-EFFET-SUSP', 8 ),
        oEdtk::Field->new( 'G6-RESERVE-G6',      200 ),
    );
}

1;
