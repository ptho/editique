package oEUser::Descriptor::R425;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R425-LGNDAT
        oEdtk::Field->new( 'R425-ACVVACASR2',    2 ),
        oEdtk::Field->new( 'R425-ACVVDDEPRD',    8 ),
        oEdtk::Field->new( 'R425-ACVVDFEPRD',    8 ),
        oEdtk::Field->new( 'R425-ACVVPRPCRI',    1 ),
        oEdtk::Field->new( 'R425-LC-ACVVPRPCRI', 15 ),
        oEdtk::Field->new( 'R425-LL-ACVVPRPCRI', 50 ),
        oEdtk::Field->new( 'R425-ACVVUNICAL',    4 ),
        oEdtk::Field->new( 'R425-LC-ACVVUNICAL', 15 ),
        oEdtk::Field->new( 'R425-LL-ACVVUNICAL', 50 ),
        oEdtk::Field->new( 'R425-ACVVVALVAA',    20 ),
        oEdtk::Field->new( 'R425-ACVVVALVAD',    8 ),
        oEdtk::Field->new( 'R425-ACVVVALVAE',    9 ),
        oEdtk::SignedField->new( 'R425-ACVVVALMNT', 12, 2 ),
        oEdtk::SignedField->new( 'R425-ACVVVALTAU', 4,  5 ),
    );
}

1;
