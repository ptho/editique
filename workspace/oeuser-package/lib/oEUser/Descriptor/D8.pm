package oEUser::Descriptor::D8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # D8-RELANCES
        oEdtk::Field->new( 'D8-DCRHDCINR1',    10 ),
        oEdtk::Field->new( 'D8-DCRHNUMSEQ',    5 ),
        oEdtk::Field->new( 'D8-DCRHDCOCR2',    8 ),
        oEdtk::Field->new( 'D8-DCRHCODETA',    2 ),
        oEdtk::Field->new( 'D8-DCRHREFCRR',    10 ),
        oEdtk::Field->new( 'D8-CODE-COURRIER', 50 ),
        oEdtk::Field->new( 'D8-DCRHDATECH',    19 ),
        oEdtk::Field->new( 'D8-DCRHCODSIT',    2 ),
    );
}

1;
