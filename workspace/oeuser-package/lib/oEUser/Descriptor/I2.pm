package oEUser::Descriptor::I2;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # I2-LGNDAT
        oEdtk::Field->new( 'I2-ECCIACOBI1', 8 ),
        oEdtk::Field->new( 'I2-ECCINUMPAR', 5 ),
        oEdtk::Field->new( 'I2-ECD1DATECH', 8 ),
        oEdtk::Field->new( 'I2-PERICPTTIT', 50 ),
        oEdtk::Field->new( 'I2-PERICODBQE', 10 ),
        oEdtk::Field->new( 'I2-PERICODGUI', 10 ),
        oEdtk::Field->new( 'I2-PERINUMCPT', 20 ),
        oEdtk::Field->new( 'I2-PERICLECPT', 2 ),
        oEdtk::Field->new( 'I2-PERICLEIBA', 4 ),
        oEdtk::Field->new( 'I2-PEPACODPAY', 3 ),
        oEdtk::Field->new( 'I2-PERICODDEV', 3 ),
        oEdtk::SignedField->new( 'I2-MONTANT-DEBIT', 12, 2 ),
    );
}

1;
