package oEUser::Descriptor::GE;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # GE-LGNDAT
        oEdtk::Field->new( 'CO62-GE-NUMPART',      5 ),
        oEdtk::Field->new( 'CO62-GE-CODPART',      3 ),
        oEdtk::Field->new( 'CO62-GE-LIBCOUCODPAR', 15 ),
        oEdtk::Field->new( 'CO62-GE-LIBLONCODPAR', 50 ),
        oEdtk::Field->new( 'CO62-GE-LIB-FRAIS',    50 ),
        oEdtk::SignedField->new( 'CO62-GE-MTFRAISHT',  12, 2 ),
        oEdtk::SignedField->new( 'CO62-GE-MTFRAISTTC', 12, 2 ),
        oEdtk::Field->new( 'CO62-GE-LIB-ARRONDIS', 50 ),
        oEdtk::SignedField->new( 'CO62-GE-MTARRONDISHT',  12, 2 ),
        oEdtk::SignedField->new( 'CO62-GE-MTARRONDISTTC', 12, 2 ),
        oEdtk::Field->new( 'CO62-GE-LIB-COTPART', 50 ),
        oEdtk::SignedField->new( 'CO62-GE-MTCOTPARTHT',  12, 2 ),
        oEdtk::SignedField->new( 'CO62-GE-MTCOTPARTTTC', 12, 2 ),
    );
}

1;
