package oEUser::Descriptor::GC;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CO12-GC-LGNDAT
        # CO12-GC-GROUPE-ASS
        oEdtk::Field->new( 'CO12-GC-CODGRA', 6 ),

        # CO12-GC-NOM-GA
        oEdtk::Field->new( 'CO12-GC-PENOCODTIT-GA', 10 ),
        oEdtk::Field->new( 'CO12-GC-PENOLICTIT-GA', 15 ),
        oEdtk::Field->new( 'CO12-GC-PENOLIBLON-GA', 50 ),
        oEdtk::Field->new( 'CO12-GC-CODSIR-GA',     14 ),
        oEdtk::Field->new( 'CO12-GC-ACH7DDEADH',    8 ),
        oEdtk::Field->new( 'CO12-GC-ACH7DDERAD',    8 ),
        oEdtk::Field->new( 'CO12-GC-ACH7LIBATT',    30 ),
        oEdtk::Field->new( 'CO12-GC-ACH7INDABR',    1 ),
        oEdtk::Field->new( 'CO12-GC-ACH7ACGAR3',    6 ),

        # CO12-GC-ADRESSE-GA
        oEdtk::Field->new( 'CO12-GC-PNTREM-GA',       32 ),
        oEdtk::Field->new( 'CO12-GC-CPLADR-GA',       32 ),
        oEdtk::Field->new( 'CO12-GC-PEADNUMVOI-GA',   9 ),
        oEdtk::Field->new( 'CO12-GC-PEADBTQVOI-GA',   2 ),
        oEdtk::Field->new( 'CO12-GC-PEADLICBTQ-GA',   15 ),
        oEdtk::Field->new( 'CO12-GC-PEVONATVOI-GA',   5 ),
        oEdtk::Field->new( 'CO12-GC-PEADLICNTV-GA',   15 ),
        oEdtk::Field->new( 'CO12-GC-LIBVOI-GA',       32 ),
        oEdtk::Field->new( 'CO12-GC-INDCDX-GA',       1 ),
        oEdtk::Field->new( 'CO12-GC-BOIPOSLIBLDT-GA', 32 ),
        oEdtk::Field->new( 'CO12-GC-LIBLOCLIBLOC-GA', 32 ),
        oEdtk::Field->new( 'CO12-GC-CODCDXCODPOS-GA', 10 ),
        oEdtk::Field->new( 'CO12-GC-NOMCDXLIBACH-GA', 32 ),
        oEdtk::Field->new( 'CO12-GC-PEPACODPAY-GA',   3 ),
        oEdtk::Field->new( 'CO12-GC-PEPALICPAY-GA',   15 ),
        oEdtk::Field->new( 'CO12-GC-TRNFAC-GA',       4 ),

        # CO12-GC-CONVENTIONS
        # CO12-GC-LISTE-PRODUIT
        oEdtk::Field->new( 'CO12-GC-CODPRT1',   10 ),
        oEdtk::Field->new( 'CO12-GC-LICCODPRT', 15 ),
        oEdtk::Field->new( 'CO12-GC-LIBCODPRT', 50 ),

        # CO12-GC-LISTE-PRODUIT-2
        oEdtk::Field->new( 'CO12-GC-CODPRT1-2',   10 ),
        oEdtk::Field->new( 'CO12-GC-LICCODPRT-2', 15 ),
        oEdtk::Field->new( 'CO12-GC-LIBCODPRT-2', 50 ),

        # CO12-GC-LISTE-PRODUIT-3
        oEdtk::Field->new( 'CO12-GC-CODPRT1-3',   10 ),
        oEdtk::Field->new( 'CO12-GC-LICCODPRT-3', 15 ),
        oEdtk::Field->new( 'CO12-GC-LIBCODPRT-3', 50 ),

        # CO12-GC-LISTE-PRODUIT-4
        oEdtk::Field->new( 'CO12-GC-CODPRT1-4',   10 ),
        oEdtk::Field->new( 'CO12-GC-LICCODPRT-4', 15 ),
        oEdtk::Field->new( 'CO12-GC-LIBCODPRT-4', 50 ),

        # CO12-GC-LISTE-PRODUIT-5
        oEdtk::Field->new( 'CO12-GC-CODPRT1-5',   10 ),
        oEdtk::Field->new( 'CO12-GC-LICCODPRT-5', 15 ),
        oEdtk::Field->new( 'CO12-GC-LIBCODPRT-5', 50 ),

        # CO12-GC-LISTE-PRODUIT-6
        oEdtk::Field->new( 'CO12-GC-CODPRT1-6',   10 ),
        oEdtk::Field->new( 'CO12-GC-LICCODPRT-6', 15 ),
        oEdtk::Field->new( 'CO12-GC-LIBCODPRT-6', 50 ),

        # CO12-GC-LISTE-PRODUIT-7
        oEdtk::Field->new( 'CO12-GC-CODPRT1-7',   10 ),
        oEdtk::Field->new( 'CO12-GC-LICCODPRT-7', 15 ),
        oEdtk::Field->new( 'CO12-GC-LIBCODPRT-7', 50 ),
    );
}

1;
