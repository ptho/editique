package oEUser::Descriptor::R315;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R315-LGNDAT
        oEdtk::Field->new( 'R315-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R315-PENOLIBLON',    50 ),
        oEdtk::Field->new( 'R315-PENOPRN01',     20 ),
        oEdtk::Field->new( 'R315-PEADCODPOS',    5 ),
        oEdtk::Field->new( 'R315-ACDPACASR2',    2 ),
        oEdtk::Field->new( 'R315-ACDPACGAR2',    6 ),
        oEdtk::Field->new( 'R315-ACDPDDEPRD',    8 ),
        oEdtk::Field->new( 'R315-ACDPDFEPRD',    8 ),
        oEdtk::Field->new( 'R315-ACDPLIBATT',    30 ),
        oEdtk::Field->new( 'R315-ACDPMODRGL',    2 ),
        oEdtk::Field->new( 'R315-LL-ACDPMODRGL', 50 ),
        oEdtk::Field->new( 'R315-LC-ACDPMODRGL', 15 ),
        oEdtk::Field->new( 'R315-ACDPCODDVP',    5 ),
        oEdtk::Field->new( 'R315-LL-ACDPCODDVP', 50 ),
        oEdtk::Field->new( 'R315-LC-ACDPCODDVP', 15 ),
    );
}

1;
