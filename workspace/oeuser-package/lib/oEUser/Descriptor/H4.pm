package oEUser::Descriptor::H4;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H4-LGNDAT
        # H4-INFO-GARANTIE
        oEdtk::Field->new( 'H4-ACH4ACGSR2',        2 ),
        oEdtk::Field->new( 'H4-ACH4CODGAR',        10 ),
        oEdtk::Field->new( 'H4-PRGALIBCOU',        15 ),
        oEdtk::Field->new( 'H4-PRGALIBLON',        50 ),
        oEdtk::Field->new( 'H4-PRGPTYPGAR',        1 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRGPTYPGAR', 15 ),
        oEdtk::Field->new( 'H4-ACH4CODPRT',        10 ),
        oEdtk::Field->new( 'H4-PRPRLIBCOU',        15 ),
        oEdtk::Field->new( 'H4-PRPRLIBLON',        50 ),
        oEdtk::Field->new( 'H4-ACH4DDEADH',        8 ),
        oEdtk::Field->new( 'H4-ACH4DDERAD',        8 ),
        oEdtk::Field->new( 'H4-ACOPDDEOPE',        8 ),
        oEdtk::Field->new( 'H4-ACH4DDECOT',        8 ),
        oEdtk::Field->new( 'H4-ACH4DFECOT',        8 ),
        oEdtk::Field->new( 'H4-ACH4DATVTE',        8 ),
        oEdtk::Field->new( 'H4-ACH4DURGAR',        4 ),
        oEdtk::Field->new( 'H4-ACH4DFEEFC',        8 ),
        oEdtk::Field->new( 'H4-ACH4DATNAI',        8 ),
        oEdtk::Field->new( 'H4-ACH4CODSEX',        1 ),
        oEdtk::Field->new( 'H4-LIBCOU-ACH4CODSEX', 15 ),
        oEdtk::Field->new( 'H4-ACH4CODCSP',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-ACH4CODCSP', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-ACH4CODCSP', 50 ),
        oEdtk::Field->new( 'H4-ACH4CODCSO',        12 ),
        oEdtk::Field->new( 'H4-LIBCOU-ACH4CODCSO', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-ACH4CODCSO', 50 ),
        oEdtk::Field->new( 'H4-ACH4CODCLR',        2 ),
        oEdtk::Field->new( 'H4-LIBCOU-ACH4CODCLR', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-ACH4CODCLR', 50 ),
        oEdtk::Field->new( 'H4-ACH4CODFRI',        6 ),
        oEdtk::Field->new( 'H4-LIBCOU-ACH4CODFRI', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-ACH4CODFRI', 50 ),
        oEdtk::Field->new( 'H4-CALCUL-OK',         1 ),
        oEdtk::SignedField->new( 'H4-ACCOMAOCOT',         12, 2 ),
        oEdtk::SignedField->new( 'H4-ACCOMPHCOT',         12, 2 ),
        oEdtk::SignedField->new( 'H4-ACCOMAHCOT',         12, 2 ),
        oEdtk::SignedField->new( 'H4-ACCO-REDUCTION-TTC', 12, 2 ),
        oEdtk::SignedField->new( 'H4-ACCO-REDUCTION-HT',  12, 2 ),
        oEdtk::Field->new( 'H4-ACH4ACGSR3',       5 ),
        oEdtk::Field->new( 'H4-ACH4ACGSR4',       5 ),
        oEdtk::Field->new( 'H4-CODE-NAT-PREST',   1 ),
        oEdtk::Field->new( 'H4-LIBCOU-NAT-PREST', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-NAT-PREST', 50 ),

        # H4-CAPITAL-RENTE
        oEdtk::Field->new( 'H4-ACKGUNICAL', 5 ),
        oEdtk::SignedField->new( 'H4-ACKGVALMNT', 12, 2 ),
        oEdtk::Field->new( 'H4-ACKGTYPMNT',        1 ),
        oEdtk::Field->new( 'H4-LIBCOU-ACKGTYPMNT', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-ACKGTYPMNT', 50 ),
        oEdtk::Field->new( 'H4-ACKGDURETA',        2 ),
        oEdtk::Field->new( 'H4-LIBCOU-ACKGDURETA', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-ACKGDURETA', 50 ),
        oEdtk::SignedField->new( 'H4-ACKGVALTAU',    4,  5 ),
        oEdtk::SignedField->new( 'H4-ACEMSLRREF-CR', 12, 2 ),

        # H4-IJ-FRANCHISE
        # H4-IJ
        oEdtk::Field->new( 'H4-ACIJUNICAL', 5 ),
        oEdtk::SignedField->new( 'H4-ACIJVALMNT',    12, 2 ),
        oEdtk::SignedField->new( 'H4-ACIJVALTAU',    4,  5 ),
        oEdtk::SignedField->new( 'H4-ACEMSLRREF-IJ', 12, 2 ),
        oEdtk::Field->new( 'H4-PRFRCODDVE',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRCODDVE', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-PRFRCODDVE', 50 ),

        # H4-FRANCHISE
        oEdtk::Field->new( 'H4-PRFRCODFRD', 6 ),
        oEdtk::Field->new( 'H4-PRFRLIBLON', 50 ),

        # H4-FRANCHISE-RISQUE1
        oEdtk::Field->new( 'H4-PRFRRIS01',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA01',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA01', 15 ),

        # H4-FRANCHISE-RISQUE2
        oEdtk::Field->new( 'H4-PRFRRIS02',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA02',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA02', 15 ),

        # H4-FRANCHISE-RISQUE3
        oEdtk::Field->new( 'H4-PRFRRIS03',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA03',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA03', 15 ),

        # H4-IJ-FRANCHISE-2
        # H4-IJ-2
        oEdtk::Field->new( 'H4-ACIJUNICAL-2', 5 ),
        oEdtk::SignedField->new( 'H4-ACIJVALMNT-2',    12, 2 ),
        oEdtk::SignedField->new( 'H4-ACIJVALTAU-2',    4,  5 ),
        oEdtk::SignedField->new( 'H4-ACEMSLRREF-IJ-2', 12, 2 ),
        oEdtk::Field->new( 'H4-PRFRCODDVE-2',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRCODDVE-2', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-PRFRCODDVE-2', 50 ),

        # H4-FRANCHISE-2
        oEdtk::Field->new( 'H4-PRFRCODFRD-2', 6 ),
        oEdtk::Field->new( 'H4-PRFRLIBLON-2', 50 ),

        # H4-FRANCHISE-RISQUE1-2
        oEdtk::Field->new( 'H4-PRFRRIS01-2',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA01-2',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA01-2', 15 ),

        # H4-FRANCHISE-RISQUE2-2
        oEdtk::Field->new( 'H4-PRFRRIS02-2',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA02-2',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA02-2', 15 ),

        # H4-FRANCHISE-RISQUE3-2
        oEdtk::Field->new( 'H4-PRFRRIS03-2',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA03-2',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA03-2', 15 ),

        # H4-IJ-FRANCHISE-3
        # H4-IJ-3
        oEdtk::Field->new( 'H4-ACIJUNICAL-3', 5 ),
        oEdtk::SignedField->new( 'H4-ACIJVALMNT-3',    12, 2 ),
        oEdtk::SignedField->new( 'H4-ACIJVALTAU-3',    4,  5 ),
        oEdtk::SignedField->new( 'H4-ACEMSLRREF-IJ-3', 12, 2 ),
        oEdtk::Field->new( 'H4-PRFRCODDVE-3',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRCODDVE-3', 15 ),
        oEdtk::Field->new( 'H4-LIBLON-PRFRCODDVE-3', 50 ),

        # H4-FRANCHISE-3
        oEdtk::Field->new( 'H4-PRFRCODFRD-3', 6 ),
        oEdtk::Field->new( 'H4-PRFRLIBLON-3', 50 ),

        # H4-FRANCHISE-RISQUE1-3
        oEdtk::Field->new( 'H4-PRFRRIS01-3',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA01-3',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA01-3', 15 ),

        # H4-FRANCHISE-RISQUE2-3
        oEdtk::Field->new( 'H4-PRFRRIS02-3',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA02-3',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA02-3', 15 ),

        # H4-FRANCHISE-RISQUE3-3
        oEdtk::Field->new( 'H4-PRFRRIS03-3',        6 ),
        oEdtk::Field->new( 'H4-PRFRFRA03-3',        4 ),
        oEdtk::Field->new( 'H4-LIBCOU-PRFRFRA03-3', 15 ),
        oEdtk::Field->new( 'H4-RESERVE-H4',         200 ),
    );
}

1;
