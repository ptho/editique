package oEUser::Descriptor::H5;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H5-LGNDAT
        oEdtk::Field->new( 'H5-ACCLACASR2',        2 ),
        oEdtk::Field->new( 'H5-ACH4CODGAR',        10 ),
        oEdtk::Field->new( 'H5-ACH4CODPRT',        10 ),
        oEdtk::Field->new( 'H5-ACCLACGSR3',        5 ),
        oEdtk::Field->new( 'H5-ACCLACGSR4',        5 ),
        oEdtk::Field->new( 'H5-ACCLNUMCAU',        5 ),
        oEdtk::Field->new( 'H5-ACCLDDEPRD',        8 ),
        oEdtk::Field->new( 'H5-ACCLDFEPRD',        8 ),
        oEdtk::Field->new( 'H5-ACCLTYPTXT',        1 ),
        oEdtk::Field->new( 'H5-LIBCOU-ACCLTYPTXT', 15 ),
        oEdtk::Field->new( 'H5-ACCLPRCLI1',        8 ),

        # H5-TEXTE-CLAUSE
        oEdtk::Field->new( 'H5-TEXCL1',     250 ),
        oEdtk::Field->new( 'H5-TEXCL2',     250 ),
        oEdtk::Field->new( 'H5-TEXCL3',     250 ),
        oEdtk::Field->new( 'H5-TEXCL4',     250 ),
        oEdtk::Field->new( 'H5-RESERVE-H5', 200 ),
    );
}

1;
