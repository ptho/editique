package oEUser::Descriptor::R320;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R320-LGNDAT
        oEdtk::Field->new( 'R320-PEPENUMPER', 8 ),
        oEdtk::Field->new( 'R320-PENOLIBLON', 50 ),
        oEdtk::Field->new( 'R320-PENOPRN01',  20 ),
        oEdtk::Field->new( 'R320-PEADCODPOS', 5 ),
        oEdtk::Field->new( 'R320-ACDVACASR2', 2 ),
        oEdtk::Field->new( 'R320-ACDVDDEPRD', 8 ),
        oEdtk::Field->new( 'R320-ACDVDFEPRD', 8 ),
        oEdtk::Field->new( 'R320-ACDVLIBATT', 30 ),
    );
}

1;
