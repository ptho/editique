package oEUser::Descriptor::R535;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R535-LGNDAT
        # R535-LST-CRITERES-LIBRES
        oEdtk::Field->new( 'R535-ACVSACASR2',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI', 50 ),

        # R535-LST-CRITERES-LIBRES-2
        oEdtk::Field->new( 'R535-ACVSACASR2-2',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-2',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-2',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-2',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-2',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-2', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-2', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-2',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-2', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-2', 50 ),

        # R535-LST-CRITERES-LIBRES-3
        oEdtk::Field->new( 'R535-ACVSACASR2-3',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-3',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-3',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-3',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-3',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-3', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-3', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-3',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-3', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-3', 50 ),

        # R535-LST-CRITERES-LIBRES-4
        oEdtk::Field->new( 'R535-ACVSACASR2-4',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-4',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-4',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-4',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-4',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-4', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-4', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-4',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-4', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-4', 50 ),

        # R535-LST-CRITERES-LIBRES-5
        oEdtk::Field->new( 'R535-ACVSACASR2-5',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-5',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-5',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-5',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-5',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-5', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-5', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-5',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-5', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-5', 50 ),

        # R535-LST-CRITERES-LIBRES-6
        oEdtk::Field->new( 'R535-ACVSACASR2-6',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-6',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-6',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-6',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-6',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-6', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-6', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-6',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-6', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-6', 50 ),

        # R535-LST-CRITERES-LIBRES-7
        oEdtk::Field->new( 'R535-ACVSACASR2-7',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-7',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-7',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-7',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-7',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-7', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-7', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-7',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-7', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-7', 50 ),

        # R535-LST-CRITERES-LIBRES-8
        oEdtk::Field->new( 'R535-ACVSACASR2-8',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-8',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-8',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-8',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-8',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-8', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-8', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-8',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-8', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-8', 50 ),

        # R535-LST-CRITERES-LIBRES-9
        oEdtk::Field->new( 'R535-ACVSACASR2-9',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-9',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-9',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-9',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-9',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-9', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-9', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-9',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-9', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-9', 50 ),

        # R535-LST-CRITERES-LIBRES-10
        oEdtk::Field->new( 'R535-ACVSACASR2-10',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-10',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-10',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-10',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-10',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-10', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-10', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-10',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-10', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-10', 50 ),

        # R535-LST-CRITERES-LIBRES-11
        oEdtk::Field->new( 'R535-ACVSACASR2-11',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-11',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-11',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-11',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-11',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-11', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-11', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-11',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-11', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-11', 50 ),

        # R535-LST-CRITERES-LIBRES-12
        oEdtk::Field->new( 'R535-ACVSACASR2-12',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-12',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-12',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-12',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-12',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-12', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-12', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-12',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-12', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-12', 50 ),

        # R535-LST-CRITERES-LIBRES-13
        oEdtk::Field->new( 'R535-ACVSACASR2-13',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-13',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-13',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-13',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-13',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-13', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-13', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-13',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-13', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-13', 50 ),

        # R535-LST-CRITERES-LIBRES-14
        oEdtk::Field->new( 'R535-ACVSACASR2-14',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-14',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-14',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-14',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-14',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-14', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-14', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-14',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-14', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-14', 50 ),

        # R535-LST-CRITERES-LIBRES-15
        oEdtk::Field->new( 'R535-ACVSACASR2-15',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-15',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-15',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-15',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-15',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-15', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-15', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-15',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-15', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-15', 50 ),

        # R535-LST-CRITERES-LIBRES-16
        oEdtk::Field->new( 'R535-ACVSACASR2-16',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-16',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-16',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-16',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-16',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-16', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-16', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-16',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-16', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-16', 50 ),

        # R535-LST-CRITERES-LIBRES-17
        oEdtk::Field->new( 'R535-ACVSACASR2-17',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-17',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-17',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-17',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-17',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-17', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-17', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-17',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-17', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-17', 50 ),

        # R535-LST-CRITERES-LIBRES-18
        oEdtk::Field->new( 'R535-ACVSACASR2-18',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-18',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-18',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-18',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-18',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-18', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-18', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-18',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-18', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-18', 50 ),

        # R535-LST-CRITERES-LIBRES-19
        oEdtk::Field->new( 'R535-ACVSACASR2-19',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-19',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-19',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-19',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-19',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-19', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-19', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-19',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-19', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-19', 50 ),

        # R535-LST-CRITERES-LIBRES-20
        oEdtk::Field->new( 'R535-ACVSACASR2-20',    2 ),
        oEdtk::Field->new( 'R535-ACVSCODGAR-20',    10 ),
        oEdtk::Field->new( 'R535-ACVSCODPRT-20',    10 ),
        oEdtk::Field->new( 'R535-ACVSDDEPRD-20',    8 ),
        oEdtk::Field->new( 'R535-ACVSDFEPRD-20',    8 ),
        oEdtk::Field->new( 'R535-LC-ACVCARATAR-20', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVCARATAR-20', 50 ),
        oEdtk::Field->new( 'R535-ACVSVALCRI-20',    6 ),
        oEdtk::Field->new( 'R535-LC-ACVSVALCRI-20', 15 ),
        oEdtk::Field->new( 'R535-LL-ACVSVALCRI-20', 50 ),
    );
}

1;
