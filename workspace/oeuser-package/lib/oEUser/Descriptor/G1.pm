package oEUser::Descriptor::G1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # G1-LGNDAT
        oEdtk::Field->new( 'G1-ACH7ACGAR2',        6 ),
        oEdtk::Field->new( 'G1-ACH7PEREI1',        8 ),
        oEdtk::Field->new( 'G1-ACH7ACGAR3',        6 ),
        oEdtk::Field->new( 'G1-PENOLIBLON',        50 ),
        oEdtk::Field->new( 'G1-PECMNUMRET',        5 ),
        oEdtk::Field->new( 'G1-PECMNUMREN',        9 ),
        oEdtk::Field->new( 'G1-ODGGCODGGS',        9 ),
        oEdtk::Field->new( 'G1-ODCGCODCGS',        9 ),
        oEdtk::Field->new( 'G1-PEADPNTREM',        50 ),
        oEdtk::Field->new( 'G1-PEADCPLADR',        50 ),
        oEdtk::Field->new( 'G1-PEADNUMVOI',        9 ),
        oEdtk::Field->new( 'G1-PEADBTQVOI',        2 ),
        oEdtk::Field->new( 'G1-LIBCOU-PEADNATVOI', 15 ),
        oEdtk::Field->new( 'G1-LIBLON-PEADNATVOI', 50 ),
        oEdtk::Field->new( 'G1-PEADLIBVOI',        50 ),
        oEdtk::Field->new( 'G1-INADR',             1 ),
        oEdtk::Field->new( 'G1-PEADBOIPOS-LIBLIE', 50 ),
        oEdtk::Field->new( 'G1-PEADLIBLOC',        50 ),
        oEdtk::Field->new( 'G1-PEADCODCDX-CODPOS', 10 ),
        oEdtk::Field->new( 'G1-PEADNOMCDX-LIBACH', 50 ),
        oEdtk::Field->new( 'G1-PEADCODPAY',        3 ),
        oEdtk::Field->new( 'G1-LIBCOU-PEADCODPAY', 15 ),
        oEdtk::Field->new( 'G1-LIBLON-PEADCODPAY', 50 ),
        oEdtk::Field->new( 'G1-PEADNATVOI',        5 ),
        oEdtk::Field->new( 'G1-ACH7DDEADH',        8 ),
        oEdtk::Field->new( 'G1-ACH7DDERAD',        8 ),
        oEdtk::Field->new( 'G1-RESERVE-G1',        200 ),
    );
}

1;
