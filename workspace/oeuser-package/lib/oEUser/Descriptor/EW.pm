package oEUser::Descriptor::EW;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # EW-ENREG
        oEdtk::Field->new( 'EW-CODENT-RATT',     9 ),
        oEdtk::Field->new( 'EW-LIB-CODENT-RATT', 50 ),
        oEdtk::Field->new( 'EW-NUMCONT',         15 ),
        oEdtk::Field->new( 'EW-CIVIL-PAY',       10 ),
        oEdtk::Field->new( 'EW-LIBCIV-PAY',      15 ),
        oEdtk::Field->new( 'EW-CODTIT-PAY',      10 ),
        oEdtk::Field->new( 'EW-LIBTIT-PAY',      10 ),
        oEdtk::Field->new( 'EW-NOMPAY',          50 ),
        oEdtk::Field->new( 'EW-PRENPAY',         20 ),
        oEdtk::Field->new( 'EW-CODCGS',          9 ),
        oEdtk::Field->new( 'EW-LIBLON-CG',       50 ),

        # EW-ADRESSE-CG
        oEdtk::Field->new( 'EW-PNTREM',       32 ),
        oEdtk::Field->new( 'EW-CPLADR',       32 ),
        oEdtk::Field->new( 'EW-NUMVOI',       9 ),
        oEdtk::Field->new( 'EW-BTQVOI',       2 ),
        oEdtk::Field->new( 'EW-NATVOI',       5 ),
        oEdtk::Field->new( 'EW-LIBVOI',       32 ),
        oEdtk::Field->new( 'EW-INDCDX',       1 ),
        oEdtk::Field->new( 'EW-BOIPOSLIBLDT', 32 ),
        oEdtk::Field->new( 'EW-LIBLOCLIBLOC', 32 ),
        oEdtk::Field->new( 'EW-CODCDXCODPOS', 10 ),
        oEdtk::Field->new( 'EW-NOMCDXLIBACH', 32 ),
        oEdtk::Field->new( 'EW-CODPAY',       3 ),
        oEdtk::Field->new( 'EW-TRNFAC',       4 ),
        oEdtk::Field->new( 'EW-NUECCO',       15 ),

        # EW-RIB-EMETTEUR
        oEdtk::Field->new( 'EW-CPTTIT',     50 ),
        oEdtk::Field->new( 'EW-CODBQE',     10 ),
        oEdtk::Field->new( 'EW-CODGUI',     10 ),
        oEdtk::Field->new( 'EW-NUMCPT',     20 ),
        oEdtk::Field->new( 'EW-CLECPT',     2 ),
        oEdtk::Field->new( 'EW-CLEIBA',     4 ),
        oEdtk::Field->new( 'EW-CODDEV',     3 ),
        oEdtk::Field->new( 'EW-CODPAY-RIB', 3 ),

        # EW-CPL-ADR-CG				Complément adresse du centre de gestion
        oEdtk::Field->new( 'EW-PNTREM-CG',       50 ),
        oEdtk::Field->new( 'EW-CPLADR-CG',       50 ),
        oEdtk::Field->new( 'EW-LIBVOI-CG',       50 ),
        oEdtk::Field->new( 'EW-BOIPOSLIBLDT-CG', 50 ),
        oEdtk::Field->new( 'EW-LIBLOCLIBLOC-CG', 50 ),
        oEdtk::Field->new( 'EW-NOMCDXLIBACH-CG', 50 ),
    );
}

1;
