package oEUser::Descriptor::E2;

use strict;
use warnings;

use oEdtk::Record;

sub get {
    return oEdtk::Record->new(

        # E2-ZZEDIT-E2-LGNDAT
        # E2-ZZEDIT-ACSO
        oEdtk::Field->new( 'E2-ZZZZ-EDIT-TYPBLC', 2 ),
        oEdtk::Field->new( 'E2-ZZZZ-IDFREL',      8 ),

        # E2-ZZEDIT-NOM-P-PH
        oEdtk::Field->new( 'E2-PENOCODCIV',     10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODCIV', 15 ),
        oEdtk::Field->new( 'E2-PENOCODTIT',     10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODTIT', 15 ),
        oEdtk::Field->new( 'E2-PENOLIBLON',     50 ),
        oEdtk::Field->new( 'E2-PENOPRN01',      20 ),

        # E2-ZZEDIT-ADRESSE
        oEdtk::Field->new( 'E2-ZZZZ-PNTREM',       32 ),
        oEdtk::Field->new( 'E2-ZZZZ-CPLADR',       32 ),
        oEdtk::Field->new( 'E2-PEADNUMVOI',        9 ),
        oEdtk::Field->new( 'E2-PEADBTQVOI',        2 ),
        oEdtk::Field->new( 'E2-PEZZ-LICBTQVOI',    15 ),
        oEdtk::Field->new( 'E2-PEVONATVOI',        5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATVOI',    15 ),
        oEdtk::Field->new( 'E2-ZZZZ-LIBVOI',       32 ),
        oEdtk::Field->new( 'E2-ZZZZ-INDCDX',       1 ),
        oEdtk::Field->new( 'E2-ZZZZ-BOIPOSLIBLDT', 32 ),
        oEdtk::Field->new( 'E2-ZZZZ-LIBLOCLIBLOC', 32 ),
        oEdtk::Field->new( 'E2-ZZZZ-CODCDXCODPOS', 10 ),
        oEdtk::Field->new( 'E2-ZZZZ-NOMCDXLIBACH', 32 ),
        oEdtk::Field->new( 'E2-PEPACODPAY',        3 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODPAY',    15 ),
        oEdtk::Field->new( 'E2-ZZZZ-TRNFAC',       4 ),

        # E2-ZZEDIT-ADM-CT
        oEdtk::Field->new( 'E2-ACOBIDFSYS',     8 ),
        oEdtk::Field->new( 'E2-ACZZ-LICTYPCCI', 15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBTYPCCI', 50 ),
        oEdtk::Field->new( 'E2-ACCCNUMCCO',     15 ),
        oEdtk::Field->new( 'E2-ACGACODGRA',     6 ),

        # E2-ZZ-EDIT-NOM-GRA
        oEdtk::Field->new( 'E2-PENOCODCIV-2',        10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODCIV-2',    15 ),
        oEdtk::Field->new( 'E2-PENOCODTIT-2',        10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODTIT-2',    15 ),
        oEdtk::Field->new( 'E2-PENOLIBLON-2',        50 ),
        oEdtk::Field->new( 'E2-PENOPRN01-2',         20 ),
        oEdtk::Field->new( 'E2-ZZZZ-CODSIR',         14 ),
        oEdtk::Field->new( 'E2-ACH1NUMCIN',          15 ),
        oEdtk::Field->new( 'E2-ACH1NUECIN',          15 ),
        oEdtk::Field->new( 'E2-ACZZ-LICORICTT',      15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBORICTT',      50 ),
        oEdtk::Field->new( 'E2-ZZZZ-DATEDT',         8 ),
        oEdtk::Field->new( 'E2-ACH1CODGGS',          9 ),
        oEdtk::Field->new( 'E2-ACH1DDE1SO',          8 ),
        oEdtk::Field->new( 'E2-ACH1DDEZRE',          8 ),
        oEdtk::Field->new( 'E2-ZZZZ-ECHPAL',         4 ),
        oEdtk::Field->new( 'E2-ZZZZ-DURCIN',         4 ),
        oEdtk::Field->new( 'E2-ZZZZ-INDACS',         1 ),
        oEdtk::Field->new( 'E2-ZZZZ-INDSAS',         1 ),
        oEdtk::Field->new( 'E2-ZZZZ-ACH1AGFASC',     6 ),
        oEdtk::Field->new( 'E2-ZZZZ-ZZZZ-INDQCO',    1 ),
        oEdtk::Field->new( 'E2-ZZZZ-ACH1TYPQCO',     1 ),
        oEdtk::Field->new( 'E2-ZZZZ-ACZZ-LICTYPQCO', 15 ),
        oEdtk::Field->new( 'E2-ZZZZ-ZZZZ-INDTMP',    1 ),

        # E2-ZZEDIT-EMPLOYEUR
        # E2-ZZEDIT-NOM-P-MO
        oEdtk::Field->new( 'E2-PENOCODTIT-3',     10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODTIT-3', 15 ),
        oEdtk::Field->new( 'E2-PENOLIBLON-3',     50 ),
        oEdtk::Field->new( 'E2-ENENCODSIR',       14 ),

        # E2-ZZEDIT-PROF
        oEdtk::Field->new( 'E2-PROFCODOFF', 10 ),
        oEdtk::Field->new( 'E2-PROFLIBCOU', 15 ),
        oEdtk::Field->new( 'E2-PROFLIBLON', 50 ),

        # E2-ZZEDIT-PAYEUR-CI
        oEdtk::Field->new( 'E2-ACPAMODRGC',     2 ),
        oEdtk::Field->new( 'E2-ACZZ-LICMODRGC', 15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBMODRGC', 50 ),
        oEdtk::Field->new( 'E2-ACFQFRQRGC',     2 ),
        oEdtk::Field->new( 'E2-ACFQLIBCOU',     15 ),
        oEdtk::Field->new( 'E2-ACFQLIBLON',     50 ),
        oEdtk::Field->new( 'E2-ACPAFRQAVI',     2 ),
        oEdtk::Field->new( 'E2-ACZZ-LICFRQAVI', 15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBFRQAVI', 50 ),
        oEdtk::Field->new( 'E2-ACPAECHPBA',     2 ),
        oEdtk::Field->new( 'E2-ACZZ-LICECHPBA', 15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBECHPBA', 50 ),
        oEdtk::Field->new( 'E2-ACPADDEPBA',     8 ),
        oEdtk::Field->new( 'E2-ACPADFEPBA',     8 ),
        oEdtk::Field->new( 'E2-ACPAMTFPBA',     2 ),
        oEdtk::Field->new( 'E2-ACZZ-LICMTF',    15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBMTF',    50 ),

        # E2-ZZEDIT-ACEN
        oEdtk::Field->new( 'E2-ACENCODEVL', 15 ),

        # E2-ZZEDIT-ENUT
        oEdtk::Field->new( 'E2-ENUTCODUTI', 8 ),

        # E2-ZZEDIT-NOM-P-PH
        oEdtk::Field->new( 'E2-PENOCODCIV-3',     10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODCIV-3', 15 ),
        oEdtk::Field->new( 'E2-PENOCODTIT-4',     10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODTIT-4', 15 ),
        oEdtk::Field->new( 'E2-PENOLIBLON-4',     50 ),
        oEdtk::Field->new( 'E2-PENOPRN01-3',      20 ),

        # E2-ZZEDIT-COMM-1
        oEdtk::Field->new( 'E2-PECONATCOM',     5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'E2-PECOVALCOM',     50 ),

        # E2-ZZEDIT-COMM-2
        oEdtk::Field->new( 'E2-PECONATCOM',     5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'E2-PECOVALCOM',     50 ),

        # E2-ZZEDIT-COMM-3
        oEdtk::Field->new( 'E2-PECONATCOM',     5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'E2-PECOVALCOM',     50 ),

        # E2-ZZEDIT-ODAG
        oEdtk::Field->new( 'E2-ZZ-EDIT-TYPBLC', 2 ),
        oEdtk::Field->new( 'E2-ODAGCODAGC',     9 ),

        # E2-ZZEDIT-NOM-P-MO
        oEdtk::Field->new( 'E2-PENOCODTIT-5',     10 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODTIT-5', 15 ),
        oEdtk::Field->new( 'E2-PENOLIBLON-5',     50 ),

        # E2-ZZEDIT-ADRESSE
        oEdtk::Field->new( 'E2-ZZZZ-PNTREM-2',       32 ),
        oEdtk::Field->new( 'E2-ZZZZ-CPLADR-2',       32 ),
        oEdtk::Field->new( 'E2-PEADNUMVOI-2',        9 ),
        oEdtk::Field->new( 'E2-PEADBTQVOI-2',        2 ),
        oEdtk::Field->new( 'E2-PEZZ-LICBTQVOI-2',    15 ),
        oEdtk::Field->new( 'E2-PEVONATVOI-2',        5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATVOI-2',    15 ),
        oEdtk::Field->new( 'E2-ZZZZ-LIBVOI-2',       32 ),
        oEdtk::Field->new( 'E2-ZZZZ-INDCDX-2',       1 ),
        oEdtk::Field->new( 'E2-ZZZZ-BOIPOSLIBLDT-2', 32 ),
        oEdtk::Field->new( 'E2-ZZZZ-LIBLOCLIBLOC-2', 32 ),
        oEdtk::Field->new( 'E2-ZZZZ-CODCDXCODPOS-2', 10 ),
        oEdtk::Field->new( 'E2-ZZZZ-NOMCDXLIBACH-2', 32 ),
        oEdtk::Field->new( 'E2-PEPACODPAY-2',        3 ),
        oEdtk::Field->new( 'E2-PEZZ-LICCODPAY-2',    15 ),
        oEdtk::Field->new( 'E2-ZZZZ-TRNFAC-2',       4 ),

        # E2-ZZEDIT-COMM-1
        oEdtk::Field->new( 'E2-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'E2-PECOVALCOM-2',     50 ),

        # E2-ZZEDIT-COMM-2
        oEdtk::Field->new( 'E2-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'E2-PECOVALCOM-2',     50 ),

        # E2-ZZEDIT-COMM-3
        oEdtk::Field->new( 'E2-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'E2-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'E2-PECOVALCOM-2',     50 ),

        # E2-ZZEDIT-ODRS
        oEdtk::Field->new( 'E2-ZZ-EDIT-TYPBLC-2', 2 ),
        oEdtk::Field->new( 'E2-ODRSCODRDI',       9 ),
        oEdtk::Field->new( 'E2-ODRSLIBLON',       50 ),

        # E2-ZZEDIT-CTX-EDITION
        oEdtk::Field->new( 'E2-ACOPCODAGC',     4 ),
        oEdtk::Field->new( 'E2-ACZZ-LICCODAGC', 15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBCODAGC', 50 ),
        oEdtk::Field->new( 'E2-ACOPCODCTX',     4 ),
        oEdtk::Field->new( 'E2-ACZZ-LICCODCTX', 15 ),
        oEdtk::Field->new( 'E2-ACZZ-LIBCODCTX', 50 ),
        oEdtk::Field->new( 'E2-ACOPDDEOPE',     8 ),
        oEdtk::Field->new( 'E2-ACOPDDECOT',     8 ),
        oEdtk::Field->new( 'E2-ACOPDATDEM',     8 ),
        oEdtk::Field->new( 'E2-ACOPDATVTE',     8 ),
        oEdtk::Field->new( 'E2-ZZZZ-NUMAVT',    3 ),
        oEdtk::Field->new( 'E2-ZZZZ-ZONEDH',    40 ),
        oEdtk::Field->new( 'E2-ZZZZ-CDCOUR',    7 )
    );
}

1;
