package oEUser::Descriptor::P9;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;
use oEdtk::SignedField;

sub get {
    return oEdtk::Record->new(

        # P9-LDEC
        oEdtk::Field->new( 'P9-PSRLIDFREL', 10 ),
        oEdtk::Field->new( 'P9-PSREDATEDT', 8 ),

        # P9-PSZZ-LST-CRIT-DRC
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-2
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-2', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-3
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-3', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-4
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-4', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-5
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-5', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-6
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-6', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-7
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-7', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-8
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-8', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-9
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-9', 50 ),

        # P9-PSZZ-LST-CRIT-DRC-10
        oEdtk::Field->new( 'P9-PSZZ-CONST-CRIT-DRC-10', 50 ),

        # P9-INF-LIGDRC
        oEdtk::Field->new( 'P9-PSLDNUMLGP', 5 ),
        oEdtk::Field->new( 'P9-PSLDCODARC', 5 ),
        oEdtk::Field->new( 'P9-PSZZLIBARC', 50 ),
        oEdtk::Field->new( 'P9-PSZZLIBCOU', 15 ),
        oEdtk::SignedField->new( 'P9-PSLDQTERTU', 4 ),
        oEdtk::SignedField->new( 'P9-PSLDCOERTU', 4,  5 ),
        oEdtk::SignedField->new( 'P9-PSLDMNTPRU', 12, 2 ),
        oEdtk::SignedField->new( 'P9-PSLDMNTDEP', 12, 2 ),
        oEdtk::SignedField->new( 'P9-PSLOVALTXO', 4,  5 ),
        oEdtk::SignedField->new( 'P9-PSLOVALTXC', 4,  5 ),
        oEdtk::SignedField->new( 'P9-PSLDMNTROR', 12, 2 ),
        oEdtk::SignedField->new( 'P9-PSLDMNTRCR', 12, 2 ),
        oEdtk::DateField->new( 'P9-PSLPDATPCP', 8 ),
        oEdtk::DateField->new( 'P9-PSLPDDESOI', 8 ),
        oEdtk::DateField->new( 'P9-PSLPDFESOI', 8 ),
        oEdtk::Field->new( 'P9-PSZZCODGAR', 10 ),
        oEdtk::Field->new( 'P9-PSZZLIBGAR', 50 ),
        oEdtk::Field->new( 'P9-PSLDCODGEL', 5 ),
        oEdtk::Field->new( 'P9-PSZZLIBGEL', 50 ),
        oEdtk::Field->new( 'P9-PSLDCODGAE', 2 ),
        oEdtk::SignedField->new( 'P9-PSLORRUMGE', 12, 2 ),
        oEdtk::SignedField->new( 'P9-PSLORCXMGE', 12, 2 ),
        oEdtk::SignedField->new( 'P9-PSLDMNTRCD', 12, 2 ),
        oEdtk::Field->new( 'P9-PSZZMSG-LD', 250 ),
        oEdtk::Field->new( 'P9-PSLNNUMEXA', 9 ),
        oEdtk::Field->new( 'P9-PSLNNOMEXA', 50 ),
        oEdtk::SignedField->new( 'P9-PSLNBASRBT', 12, 2 ),
        oEdtk::Field->new( 'P9-PSZZTYPACE', 3 ),

        # P9-PSLNLCAD-TAB
        oEdtk::Field->new( 'P9-PSLNLCAD', 2 ),

        # P9-PSLNLCAD-TAB-2
        oEdtk::Field->new( 'P9-PSLNLCAD-2', 2 ),

        # P9-PSLNLCAD-TAB-3
        oEdtk::Field->new( 'P9-PSLNLCAD-3', 2 ),

        # P9-PSLNLCAD-TAB-4
        oEdtk::Field->new( 'P9-PSLNLCAD-4', 2 ),

        # P9-PSLNLCAD-TAB-5
        oEdtk::Field->new( 'P9-PSLNLCAD-5', 2 ),

        # P9-PSLNLCAD-TAB-6
        oEdtk::Field->new( 'P9-PSLNLCAD-6', 2 ),

        # P9-PSLNLCAD-TAB-7
        oEdtk::Field->new( 'P9-PSLNLCAD-7', 2 ),

        # P9-PSLNLCAD-TAB-8
        oEdtk::Field->new( 'P9-PSLNLCAD-8', 2 ),

        # P9-PSLNLCAD-TAB-9
        oEdtk::Field->new( 'P9-PSLNLCAD-9', 2 ),

        # P9-PSLNLCAD-TAB-10
        oEdtk::Field->new( 'P9-PSLNLCAD-10', 2 ),

        # P9-PSLNLCAD-TAB-11
        oEdtk::Field->new( 'P9-PSLNLCAD-11', 2 ),

        # P9-PSLNLCAD-TAB-12
        oEdtk::Field->new( 'P9-PSLNLCAD-12', 2 ),

        # P9-PSLNLCAD-TAB-13
        oEdtk::Field->new( 'P9-PSLNLCAD-13', 2 ),

        # P9-PSLNLCAD-TAB-14
        oEdtk::Field->new( 'P9-PSLNLCAD-14', 2 ),

        # P9-PSLNLCAD-TAB-15
        oEdtk::Field->new( 'P9-PSLNLCAD-15', 2 ),

        # P9-PSLNLCAD-TAB-16
        oEdtk::Field->new( 'P9-PSLNLCAD-16',      2 ),
        oEdtk::Field->new( 'P9-PSLNQIFDEP',       2 ),
        oEdtk::Field->new( 'P9-PSZZLIB-QIFDEP-S', 50 ),
        oEdtk::Field->new( 'P9-PSZZQIFDEP-E',     2 ),
        oEdtk::Field->new( 'P9-PSZZLIB-QIFDEP-E', 50 ),
        oEdtk::Field->new( 'P9-PSZZOPTCOO',       1 ),
        oEdtk::Field->new( 'P9-PSZZLIB-OPTCOO',   50 ),
        oEdtk::Field->new( 'P9-PSZZQIFPS-E',      1 ),
        oEdtk::Field->new( 'P9-PSZZLIB-QIFPS-E',  50 ),
        oEdtk::Field->new( 'P9-PSZZQIFPS-S',      1 ),
        oEdtk::Field->new( 'P9-PSZZLIB-QIFPS-S',  50 ),
        oEdtk::Field->new( 'P9-PSZZORIPRES',      1 ),
        oEdtk::Field->new( 'P9-PSZZLIB-ORIPRES',  50 ),
        oEdtk::Field->new( 'P9-PSZZSPCEXA',       2 ),
        oEdtk::Field->new( 'P9-PSZZLIB-SPCEXA',   50 ),
        oEdtk::Field->new( 'P9-PSZZLIB-COO',      50 ),
        oEdtk::Field->new( 'P9-PSLINUMEXT',       8 ),
        oEdtk::SignedField->new( 'P9-PSLNMNTMTM', 12, 2 ),
        oEdtk::Field->new( 'P9-PSLNTOPPRV', 1 ),
        oEdtk::Field->new( 'P9-LIB-TOPPRV', 50 ),
        oEdtk::Field->new( 'P9-PSLNDISPRV', 2 ),
        oEdtk::Field->new( 'P9-LIB-DISPRV', 50 ),
        oEdtk::Field->new( 'P9-PSLNMATERI', 5 ),
        oEdtk::Field->new( 'P9-MATR-COLO1', 15 ),
        oEdtk::Field->new( 'P9-MATR-COLO2', 50 ),
        oEdtk::Field->new( 'P9-PSLNTYPEPS', 5 ),
        oEdtk::Field->new( 'P9-TYPP-COLO1', 15 ),
        oEdtk::Field->new( 'P9-TYPP-COLO2', 50 ),
        oEdtk::Field->new( 'P9-PSLNCODEPS', 2 ),
        oEdtk::Field->new( 'P9-CODP-COLO1', 15 ),
        oEdtk::Field->new( 'P9-CODP-COLO2', 50 ),
        oEdtk::Field->new( 'P9-PSLNCODLPP', 13 ),
        oEdtk::Field->new( 'P9-CODL-COLO1', 15 ),
        oEdtk::Field->new( 'P9-CODL-COLO2', 50 ),

        # P9-STR-OPTIQUE
        # P9-STR-SPHERE
        oEdtk::SignedField->new( 'P9-PSLNSPHERE', 2, 2 ),
        oEdtk::Field->new( 'P9-PSZZ-IND-SPHERE', 1 ),

        # P9-STR-CYLIND
        oEdtk::SignedField->new( 'P9-PSLNCYLIND', 2, 2 ),
        oEdtk::Field->new( 'P9-PSZZ-IND-CYLIND', 1 ),
        oEdtk::Field->new( 'P9-FILLER-P9',       435 ),
    );
}

1;
