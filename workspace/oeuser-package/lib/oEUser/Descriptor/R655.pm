package oEUser::Descriptor::R655;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R655-LGNDAT
        oEdtk::Field->new( 'R655-PESICODSPA', 5 ),
        oEdtk::Field->new( 'R655-PESIDDE',    8 ),
        oEdtk::Field->new( 'R655-PESIDFE',    8 ),
        oEdtk::Field->new( 'R655-ACOPNUMCIN', 15 ),
        oEdtk::Field->new( 'R655-ACOPNUMOPE', 5 ),
    );
}

1;
