package oEUser::Descriptor::M1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # M1-LGNDAT
        oEdtk::Field->new( 'M1-ACE9CODCEC',        9 ),
        oEdtk::Field->new( 'M1-LIBCOU-ACE9CODCEC', 15 ),
        oEdtk::Field->new( 'M1-LIBLON-ACE9CODCEC', 50 ),
        oEdtk::Field->new( 'M1-ACE9TYPCEC',        6 ),
        oEdtk::Field->new( 'M1-LIBCOU-ACE9TYPCEC', 15 ),
        oEdtk::Field->new( 'M1-ACE9TYPCNP',        1 ),
        oEdtk::Field->new( 'M1-LIBCOU-ACE9TYPCNP', 15 ),
        oEdtk::Field->new( 'M1-ACE9MTFCNP',        5 ),
        oEdtk::Field->new( 'M1-LIBCOU-ACE9MTFCNP', 15 ),
        oEdtk::Field->new( 'M1-ACE9CODNFI',        8 ),
        oEdtk::Field->new( 'M1-LIBCOU-ACE9CODNFI', 15 ),
        oEdtk::Field->new( 'M1-LIBLON-ACE9CODNFI', 50 ),
        oEdtk::Field->new( 'M1-ACE9ECHIPU',        8 ),
        oEdtk::SignedField->new( 'M1-ACM5MPHECH', 12, 2 ),

        # M1-TAB-TAXES
        # M1-ELT-TAXES
        oEdtk::Field->new( 'M1-PRTXCODTAX', 6 ),
        oEdtk::SignedField->new( 'M1-PRVLVALTAU', 4,  5 ),
        oEdtk::SignedField->new( 'M1-ACEAMNTECH', 12, 2 ),
        oEdtk::Field->new( 'M1-PRTXIDFSYS', 8 ),
        oEdtk::Field->new( 'M1-PRTXLIBLON', 50 ),

        # M1-TAXE-1
        oEdtk::Field->new( 'M1-PRTXCODTAX-1', 6 ),
        oEdtk::SignedField->new( 'M1-PRVLVALTAU-1', 4,  5 ),
        oEdtk::SignedField->new( 'M1-ACEAMNTECH-1', 12, 2 ),
        oEdtk::Field->new( 'M1-PRTXIDFSYS-1', 8 ),
        oEdtk::Field->new( 'M1-PRTXLIBLON-1', 50 ),

        # M1-TAXE-2
        oEdtk::Field->new( 'M1-PRTXCODTAX-2', 6 ),
        oEdtk::SignedField->new( 'M1-PRVLVALTAU-2', 4,  5 ),
        oEdtk::SignedField->new( 'M1-ACEAMNTECH-2', 12, 2 ),
        oEdtk::Field->new( 'M1-PRTXIDFSYS-2', 8 ),
        oEdtk::Field->new( 'M1-PRTXLIBLON-2', 50 ),

        # M1-TAXE-3
        oEdtk::Field->new( 'M1-PRTXCODTAX-3', 6 ),
        oEdtk::SignedField->new( 'M1-PRVLVALTAU-3', 4,  5 ),
        oEdtk::SignedField->new( 'M1-ACEAMNTECH-3', 12, 2 ),
        oEdtk::Field->new( 'M1-PRTXIDFSYS-3', 8 ),
        oEdtk::Field->new( 'M1-PRTXLIBLON-3', 50 ),

        # M1-TAXE-4
        oEdtk::Field->new( 'M1-PRTXCODTAX-4', 6 ),
        oEdtk::SignedField->new( 'M1-PRVLVALTAU-4', 4,  5 ),
        oEdtk::SignedField->new( 'M1-ACEAMNTECH-4', 12, 2 ),
        oEdtk::Field->new( 'M1-PRTXIDFSYS-4', 8 ),
        oEdtk::Field->new( 'M1-PRTXLIBLON-4', 50 ),

        # M1-TAXE-5
        oEdtk::Field->new( 'M1-PRTXCODTAX-5', 6 ),
        oEdtk::SignedField->new( 'M1-PRVLVALTAU-5', 4,  5 ),
        oEdtk::SignedField->new( 'M1-ACEAMNTECH-5', 12, 2 ),
        oEdtk::Field->new( 'M1-PRTXIDFSYS-5', 8 ),
        oEdtk::Field->new( 'M1-PRTXLIBLON-5', 50 ),
        oEdtk::Field->new( 'M1-ACFSACFCR3',   8 ),
        oEdtk::Field->new( 'M1-RESERVE-M1',   200 ),
    );
}

1;
