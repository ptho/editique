package oEUser::Descriptor::T4;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # T4-LGNDAT
        # T4-IDENT-PART
        oEdtk::Field->new( 'T4-NUMPART', 5 ),
        oEdtk::Field->new( 'T4-FILLER',  15 ),

        # T4-INFO-MONTANT
        oEdtk::SignedField->new( 'T4-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'T4-MONTANT-TTC', 12, 2 ),
        oEdtk::Field->new( 'T4-ECHIPU', 8 ),

        # T4-INFO-GS
        oEdtk::Field->new( 'T4-PRGACODGAR', 10 ),
        oEdtk::Field->new( 'T4-PRGALIBCOU', 15 ),
        oEdtk::Field->new( 'T4-PRGALIBLON', 50 ),

        # T4-HORS-PERIODE-DIFF-C-I
        oEdtk::Field->new( 'T4-FILLER1', 1 ),
        oEdtk::SignedField->new( 'T4-TOT-CONSO-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T4-FILLER2', 1 ),
        oEdtk::SignedField->new( 'T4-TOT-IND-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T4-FILLER3', 1 ),
        oEdtk::SignedField->new( 'T4-DIFF-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T4-FILLER4',              1 ),
        oEdtk::Field->new( 'T4-TEST-YA-DIF-HORS-PER', 2 ),

        # T4-YA-DIFf-HORS-PER
        oEdtk::Field->new( 'T4-DATEXI', 8 ),
    );
}

1;
