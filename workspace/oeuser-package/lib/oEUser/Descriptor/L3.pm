package oEUser::Descriptor::L3;
use utf8;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # L3-LGNDAT
        # L3-IDENT-CI
        oEdtk::Field->new( 'L3-NUMPART',    5 ),
        oEdtk::Field->new( 'L3-ACH1NUMCIN', 15 ),
        oEdtk::Field->new( 'L3-ACH1NUECIN', 15 ),
        oEdtk::Field->new( 'L3-ACOBIDFSYS', 8 ),
        oEdtk::Field->new( 'L3-PEPPLIBMAT', 15 ),

        # L3-SOLDE-CI
        oEdtk::SignedField->new( 'L3-DEJA-PERCU',         12, 2 ),
        oEdtk::SignedField->new( 'L3-RESTE-DU-ANTERIEUR', 12, 2 ),

        # L3-IDENT-SO
        oEdtk::Field->new( 'L3-IDFREL-SO', 8 ),

        # L3-NOM-SO
        oEdtk::Field->new( 'L3-PENOCODCIV-SO', 10 ),
        oEdtk::Field->new( 'L3-PENOLICCIV-SO', 15 ),
        oEdtk::Field->new( 'L3-PENOCODTIT-SO', 10 ),
        oEdtk::Field->new( 'L3-PENOLICTIT-SO', 15 ),
        oEdtk::Field->new( 'L3-PENOLIBLON-SO', 50 ),
        oEdtk::Field->new( 'L3-PENOPRN01-SO',  20 ),

        # L3-ADRESSE-SO
        oEdtk::Field->new( 'L3-PNTREM-SO',       32 ),
        oEdtk::Field->new( 'L3-CPLADR-SO',       32 ),
        oEdtk::Field->new( 'L3-PEADNUMVOI-SO',   9 ),
        oEdtk::Field->new( 'L3-PEADBTQVOI-SO',   2 ),
        oEdtk::Field->new( 'L3-PEADLICBTQ-SO',   15 ),
        oEdtk::Field->new( 'L3-PEVONATVOI-SO',   5 ),
        oEdtk::Field->new( 'L3-PEADLICNTV-SO',   15 ),
        oEdtk::Field->new( 'L3-LIBVOI-SO',       32 ),
        oEdtk::Field->new( 'L3-INDCDX-SO',       1 ),
        oEdtk::Field->new( 'L3-BOIPOSLIBLDT-SO', 32 ),
        oEdtk::Field->new( 'L3-LIBLOCLIBLOC-SO', 32 ),
        oEdtk::Field->new( 'L3-CODCDXCODPOS-SO', 10 ),
        oEdtk::Field->new( 'L3-NOMCDXLIBACH-SO', 32 ),
        oEdtk::Field->new( 'L3-PEPACODPAY-SO',   3 ),
        oEdtk::Field->new( 'L3-PEPALICPAY-SO',   15 ),
        oEdtk::Field->new( 'L3-TRNFAC-SO',       4 ),

        # L3-MT-SOLDE-EC
        oEdtk::Field->new( 'L3-FILLER1', 1 ),
        oEdtk::SignedField->new( 'L3-MTTOT-DETTE', 12, 2 ),
        oEdtk::SignedField->new( 'L3-MTTOT-ENC',   12, 2 ),

        # L3-MT-HORS-PERIODE-AC019
        oEdtk::Field->new( 'L3-FILLER2', 1 ),
        oEdtk::SignedField->new( 'L3-TTC-HORS-PERIODE', 12, 2 ),

        # L3-INFOS-ECH-PREC
        oEdtk::Field->new( 'L3-ETAT-ADHESION', 1 ),
        oEdtk::SignedField->new( 'L3-MTTOT-ECH-PREC', 12, 2 ),
        oEdtk::SignedField->new( 'L3-RESTE-DU',       12, 2 ),
        oEdtk::SignedField->new( 'L3-TROP-PERCU',     12, 2 ),
        oEdtk::Field->new( 'L3-DATADHESION-GRA', 8 ),

        # L3-ADRESSE-SOUSCRIPTEUR    Complément adresse du souscripteur
        oEdtk::Field->new( 'L3-PNTREM-CSO',       50 ),
        oEdtk::Field->new( 'L3-CPLADR-CSO',       50 ),
        oEdtk::Field->new( 'L3-LIBVOI-CSO',       50 ),
        oEdtk::Field->new( 'L3-BOIPOSLIBLDT-CSO', 50 ),
        oEdtk::Field->new( 'L3-LIBLOCLIBLOC-CSO', 50 ),
        oEdtk::Field->new( 'L3-NOMCDXLIBACH-CSO', 50 ),

    );
}

1;
