package oEUser::Descriptor::NE;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CE031C-NE-LGNDAT
        oEdtk::Field->new( 'CE031C-NE-ACNDCODSIT', 2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNDCODACD', 10 ),
        oEdtk::Field->new( 'CE031C-NE-ACNETYPECH', 2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEETAECH', 2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNENUMEMI', 8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNESTAINT', 2 ),
        oEdtk::Field->new( 'CE031C-NE-ENCTCODCTI', 9 ),
        oEdtk::Field->new( 'CE031C-NE-ENCTCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NE-ENCTLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NE-ENCTLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEDEVNMI', 8 ),
        oEdtk::Field->new( 'CE031C-NE-ENCRCODGRE', 2 ),
        oEdtk::Field->new( 'CE031C-NE-ENCRCODCRO', 3 ),
        oEdtk::Field->new( 'CE031C-NE-ENCRCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NE-ENCRLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NE-ENCRLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NE-ENACNUMORC', 10 ),
        oEdtk::Field->new( 'CE031C-NE-ENCNCODCPA', 4 ),
        oEdtk::Field->new( 'CE031C-NE-ENCNCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NE-ENCNLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NE-ENCNLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NE-ODERCODERA', 9 ),

        # CE031C-NE-ODERNOMERA
        oEdtk::Field->new( 'CE031C-NE-ODERCODTIT', 10 ),
        oEdtk::Field->new( 'CE031C-NE-ODERLICTIT', 15 ),
        oEdtk::Field->new( 'CE031C-NE-ODERLIBLON', 50 ),
        oEdtk::Field->new( 'CE031C-NE-ODCGCODCGS', 9 ),

        # CE031C-NE-ODCGNOMCGS
        oEdtk::Field->new( 'CE031C-NE-ODCGCODTIT',    10 ),
        oEdtk::Field->new( 'CE031C-NE-ODCGLICTIT',    15 ),
        oEdtk::Field->new( 'CE031C-NE-ODCGLIBLON',    50 ),
        oEdtk::Field->new( 'CE031C-NE-ACNDCODMRO',    13 ),
        oEdtk::Field->new( 'CE031C-NE-ACNDCLEMRO',    2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNENATNOM',    1 ),
        oEdtk::Field->new( 'CE031C-NE-ACNENOMASU',    50 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEPRNASU',    15 ),
        oEdtk::Field->new( 'CE031C-NE-ACNDDATNAI',    8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNDRNGNAI',    1 ),
        oEdtk::Field->new( 'CE031C-NE-ACNENUMTRS',    8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNECODMVT',    1 ),
        oEdtk::Field->new( 'CE031C-NE-ACNETYPCTT1',   2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEDDECTT1',   8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEDFECTT1',   8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNETYPCTT2',   2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEDDECTT2',   8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEDFECTT2',   8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNETYPCTT3',   2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEDDECTT3',   8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEDFECTT3',   8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNENOMAYD',    50 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEPRNAYD',    15 ),
        oEdtk::Field->new( 'CE031C-NE-ACNECODUGR',    4 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEACOPR1',    8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNEACOPR2',    8 ),
        oEdtk::Field->new( 'CE031C-NE-ACCINUMCIN',    15 ),
        oEdtk::Field->new( 'CE031C-NE-ACNECODMFE',    8 ),
        oEdtk::Field->new( 'CE031C-NE-LILMFE-E0142',  8 ),
        oEdtk::Field->new( 'CE031C-NE-ACNECODMFR',    10 ),
        oEdtk::Field->new( 'CE031C-NE-LILDMFR-U0121', 10 ),
        oEdtk::Field->new( 'CE031C-NE-ACNELIBRET',    100 ),
        oEdtk::Field->new( 'CE031C-NE-ACNAIDFSYS',    10 ),
        oEdtk::Field->new( 'CE031C-NE-ACNACODACN',    4 ),
        oEdtk::Field->new( 'CE031C-NE-ACNACMRUTI',    100 ),
        oEdtk::Field->new( 'CE031C-NE-ACNACODCRR',    6 ),
        oEdtk::Field->new( 'CE031C-NE-ACNANOUSTA',    2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNAANCSTA',    2 ),
        oEdtk::Field->new( 'CE031C-NE-ACNADELREE',    3 ),
        oEdtk::Field->new( 'CE031C-NE-ACNACODETA',    2 ),
    );
}

1;
