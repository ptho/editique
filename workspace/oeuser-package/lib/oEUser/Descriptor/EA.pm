package oEUser::Descriptor::EA;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # EA-LGNDAT
        oEdtk::Field->new( 'EA-ECCICODPAR',  3 ),
        oEdtk::Field->new( 'EA-LIB-PART',    50 ),
        oEdtk::Field->new( 'EA-ECDOMODRGC',  2 ),
        oEdtk::Field->new( 'EA-LIB-MODRGC',  50 ),
        oEdtk::Field->new( 'EA-ECCINUMCCO',  15 ),
        oEdtk::Field->new( 'EA-ECCICODGRA',  6 ),
        oEdtk::Field->new( 'EA-ACH7LIBAUT',  50 ),
        oEdtk::Field->new( 'EA-ECDOCODDVG',  3 ),
        oEdtk::Field->new( 'EA-LIB-DEVISE',  50 ),
        oEdtk::Field->new( 'EA-DATE-ARRETE', 8 ),
        oEdtk::Field->new( 'EA-ECDOETACTR',  2 ),
        oEdtk::Field->new( 'EA-ECDODDEZRE',  8 ),
        oEdtk::SignedField->new( 'EA-ECD1MTTECH', 12, 2 ),
        oEdtk::SignedField->new( 'EA-ECD1MTTENC', 12, 2 ),
        oEdtk::SignedField->new( 'EA-ECZZ-SOLDE', 12, 2 ),
        oEdtk::Field->new( 'EA-ECDOCODCGS', 9 ),
        oEdtk::Field->new( 'EA-LIBLON-CGS', 50 ),
        oEdtk::Field->new( 'EA-ECCICODERA', 9 ),
        oEdtk::Field->new( 'EA-ODRSCODRDI', 9 ),
        oEdtk::Field->new( 'EA-ACOFCODOFF', 10 ),
        oEdtk::Field->new( 'EA-ACH6NUECCO', 15 ),
        oEdtk::Field->new( 'EA-ECDOMODGPA', 1 ),
    );
}

1;
