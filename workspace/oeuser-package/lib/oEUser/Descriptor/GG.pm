package oEUser::Descriptor::GG;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # GG-GG-LGNDAT
        # G4-INFO-VAR-DECLARATIVES
        oEdtk::Field->new( 'GG-ACEPACGCR2', 6 ),
        oEdtk::Field->new( 'GG-ACEPCODPAR', 3 ),
        oEdtk::Field->new( 'GG-ACEPCODPRT', 10 ),
        oEdtk::Field->new( 'GG-ACEPCODGAR', 10 ),
        oEdtk::Field->new( 'GG-ACCODDEPRD', 8 ),
        oEdtk::Field->new( 'GG-ACCODFEPRD', 8 ),
        oEdtk::Field->new( 'GG-PRASCODASF', 6 ),
        oEdtk::Field->new( 'GG-PRASLIBCOU', 15 ),
        oEdtk::Field->new( 'GG-PRASLIBLON', 50 ),
        oEdtk::Field->new( 'GG-ACFEUNICAL', 4 ),
        oEdtk::SignedField->new( 'GG-ACFEFACMNT', 14, 2 ),
        oEdtk::SignedField->new( 'GG-ACFEFACTAU', 9,  5 ),
        oEdtk::Field->new( 'GG-RESERVE-GG', 2350 ),
    );
}

1;
