package oEUser::Descriptor::T7;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # T7-LGNDAT
        # T7-IDENT-PART
        oEdtk::Field->new( 'T7-NUMPART', 5 ),
        oEdtk::Field->new( 'T7-FILLER',  15 ),

        # T7-ARRONDI
        oEdtk::SignedField->new( 'T7-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'T7-MONTANT-TTC', 12, 2 ),
        oEdtk::Field->new( 'T7-ECHIPU',     8 ),
        oEdtk::Field->new( 'T7-TYPCNP',     1 ),
        oEdtk::Field->new( 'T7-LIC-TYPCNP', 15 ),
        oEdtk::Field->new( 'T7-LIB-TYPCNP', 50 ),

        # T7-HORS-PERIODE-DIFF-C-I
        oEdtk::Field->new( 'T7-FILLER1', 1 ),
        oEdtk::SignedField->new( 'T7-TOT-CONSO-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T7-FILLER2', 1 ),
        oEdtk::SignedField->new( 'T7-TOT-IND-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T7-FILLER3', 1 ),
        oEdtk::SignedField->new( 'T7-DIFF-TTC-HORS-PER', 12, 2 ),
        oEdtk::Field->new( 'T7-FILLER4',              1 ),
        oEdtk::Field->new( 'T7-TEST-YA-DIF-HORS-PER', 2 ),

        # T7-YA-DIFF-HORS-PER
        oEdtk::Field->new( 'T7-DATEXI', 8 ),
    );
}

1;
