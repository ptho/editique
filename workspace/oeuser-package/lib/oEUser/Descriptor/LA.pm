package oEUser::Descriptor::LA;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CE006C-LA-LGNDAT
        oEdtk::Field->new( 'CE006C-LA-ENACCODACD', 10 ),
        oEdtk::Field->new( 'CE006C-LA-ENCTCODCTI', 9 ),

        # CE006C-LA-ENCTNOM
        oEdtk::Field->new( 'CE006C-LA-ENCTCODTIT', 10 ),
        oEdtk::Field->new( 'CE006C-LA-ENCTLICTIT', 15 ),
        oEdtk::Field->new( 'CE006C-LA-ENCTLIBLON', 50 ),
        oEdtk::Field->new( 'CE006C-LA-ENCRCODGRE', 2 ),
        oEdtk::Field->new( 'CE006C-LA-ENCRCODCRO', 3 ),
        oEdtk::Field->new( 'CE006C-LA-ENCRCODCPA', 4 ),

        # CE006C-LA-ENCRNOM
        oEdtk::Field->new( 'CE006C-LA-ENCRCODTIT', 10 ),
        oEdtk::Field->new( 'CE006C-LA-ENCRLICTIT', 15 ),
        oEdtk::Field->new( 'CE006C-LA-ENCRLIBLON', 50 ),
        oEdtk::Field->new( 'CE006C-LA-ODERCODERA', 9 ),

        # CE006C-LA-ODERNOMERA
        oEdtk::Field->new( 'CE006C-LA-ODERCODTIT', 10 ),
        oEdtk::Field->new( 'CE006C-LA-ODERLICTIT', 15 ),
        oEdtk::Field->new( 'CE006C-LA-ODERLIBLON', 50 ),
    );
}

1;
