package oEUser::Descriptor::OE;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # OE-ENR
        oEdtk::Field->new( 'OE-CODE-SECTION',  50 ),
        oEdtk::Field->new( 'OE-Domiciliation', 50 ),
        oEdtk::Field->new( 'OE-IBAN',          50 ),
        oEdtk::Field->new( 'OE-BIC',           50 ),
        oEdtk::Field->new( 'OE-BANQUE',        50 ),
        oEdtk::Field->new( 'OE-GUICHET',       50 ),
        oEdtk::Field->new( 'OE-COMPTE',        50 ),
        oEdtk::Field->new( 'OE-CLE',           50 ),
        oEdtk::Field->new( 'OE-DEVISE',        50 ),
    );
}

1;
