package oEUser::Descriptor::OK;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # OG-ZZEDIT-OG-LGNDAT
        # OG-ZZEDIT-BANQUE-DOC
        oEdtk::Field->new( 'OK-NUMRO', 13 ),
    );
}

1;
