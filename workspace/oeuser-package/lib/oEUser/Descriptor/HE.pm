package oEUser::Descriptor::HE;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CC71-HE-LGNDAT
        # CC71-HE-INFO-AS
        oEdtk::Field->new( 'CC71-HE-ACASRNGASU', 2 ),

        # CC71-HE-INFO-PS
        oEdtk::Field->new( 'CC71-HE-CODE-PDT', 10 ),

        # CC71-HE-INFO-GS
        oEdtk::Field->new( 'CC71-HE-CODE-GAR-TECH', 10 ),

        # CC71-HE-INFO-TECH
        oEdtk::Field->new( 'CC71-HE-NBR-LGN', 5 ),

        # CC71-HE-INFO-ASSIETTE-01
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-01',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-01', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-01', 50 ),

        # CC71-HE-INFO-MONTANT-01
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-01',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-01', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-02
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-02',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-02', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-02', 50 ),

        # CC71-HE-INFO-MONTANT-02
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-02',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-02', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-03
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-03',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-03', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-03', 50 ),

        # CC71-HE-INFO-MONTANT-03
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-03',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-03', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-04
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-04',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-04', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-04', 50 ),

        # CC71-HE-INFO-MONTANT-04
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-04',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-04', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-05
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-05',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-05', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-05', 50 ),

        # CC71-HE-INFO-MONTANT-05
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-05',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-05', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-06
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-06',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-06', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-06', 50 ),

        # CC71-HE-INFO-MONTANT-06
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-06',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-06', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-07
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-07',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-07', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-07', 50 ),

        # CC71-HE-INFO-MONTANT-07
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-07',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-07', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-08
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-08',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-08', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-08', 50 ),

        # CC71-HE-INFO-MONTANT-08
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-08',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-08', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-09
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-09',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-09', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-09', 50 ),

        # CC71-HE-INFO-MONTANT-09
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-09',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-09', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-10
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-10',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-10', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-10', 50 ),

        # CC71-HE-INFO-MONTANT-10
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-10',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-10', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-11
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-11',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-11', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-11', 50 ),

        # CC71-HE-INFO-MONTANT-11
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-11',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-11', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-12
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-12',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-12', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-12', 50 ),

        # CC71-HE-INFO-MONTANT-12
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-12',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-12', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-13
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-13',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-13', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-13', 50 ),

        # CC71-HE-INFO-MONTANT-13
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-13',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-13', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-14
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-14',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-14', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-14', 50 ),

        # CC71-HE-INFO-MONTANT-14
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-14',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-14', 12, 2 ),

        # CC71-HE-INFO-ASSIETTE-15
        oEdtk::Field->new( 'CC71-HE-CODE-ASS-15',   6 ),
        oEdtk::Field->new( 'CC71-HE-LIBCOU-ASS-15', 15 ),
        oEdtk::Field->new( 'CC71-HE-LIBLON-ASS-15', 50 ),

        # CC71-HE-INFO-MONTANT-15
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-15',     12, 2 ),
        oEdtk::SignedField->new( 'CC71-HE-MONTANT-HT-EXE-15', 12, 2 ),
    );
}

1;
