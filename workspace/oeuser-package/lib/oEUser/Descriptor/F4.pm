package oEUser::Descriptor::F4;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::DateField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CE0011C-F4-LGNDAT
        # CE0011C-F4-ACIP
        oEdtk::Field->new( 'CE0011C-F4-ACIPNUMPRF', 9 ),

        # CE0011C-F4-ACIPNUMPAG-ALPHA
        oEdtk::SignedField->new( 'CE0011C-F4-ACIPNUMPAG-ETENDU', 6, 0 ),

        # CE0011C-F4-ACIPNUMCRT-ALPHA
        oEdtk::SignedField->new( 'CE0011C-F4-ACIPNUMCRT-ETENDU', 6, 0 ),
        oEdtk::DateField->new( 'CE0011C-F4-ACIPDDV', 8 ),
        oEdtk::DateField->new( 'CE0011C-F4-ACIPDFV', 8 ),

        # CE0011C-F4-POSTE
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL', 8 ),

        # CE0011C-F4-POSTE-2
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-2', 8 ),

        # CE0011C-F4-POSTE-3
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-3', 8 ),

        # CE0011C-F4-POSTE-4
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-4', 8 ),

        # CE0011C-F4-POSTE-5
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-5', 8 ),

        # CE0011C-F4-POSTE-6
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-6', 8 ),

        # CE0011C-F4-POSTE-7
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-7', 8 ),

        # CE0011C-F4-POSTE-8
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-8', 8 ),

        # CE0011C-F4-POSTE-9
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-9', 8 ),

        # CE0011C-F4-POSTE-10
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-10', 8 ),

        # CE0011C-F4-POSTE-11
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-11', 8 ),

        # CE0011C-F4-POSTE-12
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-12', 8 ),

        # CE0011C-F4-POSTE-13
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-13', 8 ),

        # CE0011C-F4-POSTE-14
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-14', 8 ),

        # CE0011C-F4-POSTE-15
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-15', 8 ),

        # CE0011C-F4-POSTE-16
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-16', 8 ),

        # CE0011C-F4-POSTE-17
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-17', 8 ),

        # CE0011C-F4-POSTE-18
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-18', 8 ),

        # CE0011C-F4-POSTE-19
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-19', 8 ),

        # CE0011C-F4-POSTE-20
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-20', 8 ),

        # CE0011C-F4-POSTE-21
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-21', 8 ),

        # CE0011C-F4-POSTE-22
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-22', 8 ),

        # CE0011C-F4-POSTE-23
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-23', 8 ),

        # CE0011C-F4-POSTE-24
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-24', 8 ),

        # CE0011C-F4-POSTE-25
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-25', 8 ),

        # CE0011C-F4-POSTE-26
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-26', 8 ),

        # CE0011C-F4-POSTE-27
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-27', 8 ),

        # CE0011C-F4-POSTE-28
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-28', 8 ),

        # CE0011C-F4-POSTE-29
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-29', 8 ),

        # CE0011C-F4-POSTE-30
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-30', 8 ),

        # CE0011C-F4-POSTE-31
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-31', 8 ),

        # CE0011C-F4-POSTE-32
        oEdtk::Field->new( 'CE0011C-F4-ACIPENTCOL-32', 8 ),

        # CE0011C-F4-ACRV
        # CE0011C-F4-ACRVNUMREN-ALPHA
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI',        50 ),

        # CE0011C-F4-ACRV-2
        # CE0011C-F4-ACRVNUMREN-ALPHA-2
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-2', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-2',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-2',        50 ),

        # CE0011C-F4-ACRV-3
        # CE0011C-F4-ACRVNUMREN-ALPHA-3
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-3', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-3',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-3',        50 ),

        # CE0011C-F4-ACRV-4
        # CE0011C-F4-ACRVNUMREN-ALPHA-4
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-4', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-4',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-4',        50 ),

        # CE0011C-F4-ACRV-5
        # CE0011C-F4-ACRVNUMREN-ALPHA-5
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-5', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-5',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-5',        50 ),

        # CE0011C-F4-ACRV-6
        # CE0011C-F4-ACRVNUMREN-ALPHA-6
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-6', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-6',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-6',        50 ),

        # CE0011C-F4-ACRV-7
        # CE0011C-F4-ACRVNUMREN-ALPHA-7
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-7', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-7',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-7',        50 ),

        # CE0011C-F4-ACRV-8
        # CE0011C-F4-ACRVNUMREN-ALPHA-8
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-8', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-8',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-8',        50 ),

        # CE0011C-F4-ACRV-9
        # CE0011C-F4-ACRVNUMREN-ALPHA-9
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-9', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-9',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-9',        50 ),

        # CE0011C-F4-ACRV-10
        # CE0011C-F4-ACRVNUMREN-ALPHA-10
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-10', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-10',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-10',        50 ),

        # CE0011C-F4-ACRV-11
        # CE0011C-F4-ACRVNUMREN-ALPHA-11
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-11', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-11',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-11',        50 ),

        # CE0011C-F4-ACRV-12
        # CE0011C-F4-ACRVNUMREN-ALPHA-12
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-12', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-12',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-12',        50 ),

        # CE0011C-F4-ACRV-13
        # CE0011C-F4-ACRVNUMREN-ALPHA-13
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-13', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-13',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-13',        50 ),

        # CE0011C-F4-ACRV-14
        # CE0011C-F4-ACRVNUMREN-ALPHA-14
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-14', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-14',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-14',        50 ),

        # CE0011C-F4-ACRV-15
        # CE0011C-F4-ACRVNUMREN-ALPHA-15
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-15', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-15',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-15',        50 ),

        # CE0011C-F4-ACRV-16
        # CE0011C-F4-ACRVNUMREN-ALPHA-16
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-16', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-16',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-16',        50 ),

        # CE0011C-F4-ACRV-17
        # CE0011C-F4-ACRVNUMREN-ALPHA-17
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-17', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-17',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-17',        50 ),

        # CE0011C-F4-ACRV-18
        # CE0011C-F4-ACRVNUMREN-ALPHA-18
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-18', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-18',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-18',        50 ),

        # CE0011C-F4-ACRV-19
        # CE0011C-F4-ACRVNUMREN-ALPHA-19
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-19', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-19',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-19',        50 ),

        # CE0011C-F4-ACRV-20
        # CE0011C-F4-ACRVNUMREN-ALPHA-20
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-20', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-20',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-20',        50 ),

        # CE0011C-F4-ACRV-21
        # CE0011C-F4-ACRVNUMREN-ALPHA-21
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-21', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-21',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-21',        50 ),

        # CE0011C-F4-ACRV-22
        # CE0011C-F4-ACRVNUMREN-ALPHA-22
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-22', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-22',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-22',        50 ),

        # CE0011C-F4-ACRV-23
        # CE0011C-F4-ACRVNUMREN-ALPHA-23
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-23', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-23',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-23',        50 ),

        # CE0011C-F4-ACRV-24
        # CE0011C-F4-ACRVNUMREN-ALPHA-24
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-24', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-24',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-24',        50 ),

        # CE0011C-F4-ACRV-25
        # CE0011C-F4-ACRVNUMREN-ALPHA-25
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-25', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-25',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-25',        50 ),

        # CE0011C-F4-ACRV-26
        # CE0011C-F4-ACRVNUMREN-ALPHA-26
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-26', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-26',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-26',        50 ),

        # CE0011C-F4-ACRV-27
        # CE0011C-F4-ACRVNUMREN-ALPHA-27
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-27', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-27',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-27',        50 ),

        # CE0011C-F4-ACRV-28
        # CE0011C-F4-ACRVNUMREN-ALPHA-28
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-28', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-28',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-28',        50 ),

        # CE0011C-F4-ACRV-29
        # CE0011C-F4-ACRVNUMREN-ALPHA-29
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-29', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-29',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-29',        50 ),

        # CE0011C-F4-ACRV-30
        # CE0011C-F4-ACRVNUMREN-ALPHA-30
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-30', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-30',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-30',        50 ),

        # CE0011C-F4-ACRV-31
        # CE0011C-F4-ACRVNUMREN-ALPHA-31
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-31', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-31',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-31',        50 ),

        # CE0011C-F4-ACRV-32
        # CE0011C-F4-ACRVNUMREN-ALPHA-32
        oEdtk::Field->new( 'CE0011C-F4-ACRVNUMREN-ETENDU-32', 2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVCODREN-32',        2 ),
        oEdtk::Field->new( 'CE0011C-F4-ACRVRENVOI-32',        50 ),

        # CE0011C-F4-CARACTERISTIQUES
        oEdtk::Field->new( 'CE0011C-F4-CODE-CARACT', 10 ),
        oEdtk::Field->new( 'CE0011C-F4-VAL-CARACT',  30 ),

        # CE0011C-F4-CARACTERISTIQUES-2
        oEdtk::Field->new( 'CE0011C-F4-CODE-CARACT-2', 10 ),
        oEdtk::Field->new( 'CE0011C-F4-VAL-CARACT-2',  30 ),

        # CE0011C-F4-CARACTERISTIQUES-3
        oEdtk::Field->new( 'CE0011C-F4-CODE-CARACT-3',   10 ),
        oEdtk::Field->new( 'CE0011C-F4-VAL-CARACT-3',    30 ),
        oEdtk::Field->new( 'CE0011C-F4-TYPE-CARTE',      3 ),
        oEdtk::Field->new( 'CE0011C-F4-NIVEAU-CARTE-TP', 1 ),
    );
}

1;
