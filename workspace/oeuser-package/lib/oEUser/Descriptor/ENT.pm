package oEUser::Descriptor::ENT;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    my $record = oEdtk::Record->new(

        # ENT-ENTETE
        #            oEdtk::Field->new('ENT-E-TYP-REC', 6),
        oEdtk::SignedField->new( 'ENT-E-ZZJOR1', 10 ),
        oEdtk::Field->new( 'ENT-E-IDFSYS', 10 ),
        oEdtk::Field->new( 'ENT-E-CODSIT', 2 ),
        oEdtk::Field->new( 'ENT-E-IDFDES', 30 ),
        oEdtk::Field->new( 'ENT-E-IDFBL2', 30 ),
        oEdtk::Field->new( 'ENT-E-IDFBL3', 30 ),
        oEdtk::Field->new( 'ENT-E-IDFDLI', 30 ),
        oEdtk::Field->new( 'ENT-E-CODDOC', 5 ),
        oEdtk::Field->new( 'ENT-E-TYPIMP', 2 ),
        oEdtk::Field->new( 'ENT-E-CODMDX', 2 ),
        oEdtk::Field->new( 'ENT-E-DATTRT', 8 ),
        oEdtk::Field->new( 'ENT-E-DATDMD', 8 ),
        oEdtk::Field->new( 'ENT-E-CODTRT', 5 ),
        oEdtk::Field->new( 'ENT-E-LIBPCD', 50 ),
        oEdtk::Field->new( 'ENT-E-CODPRM', 30 ),
        oEdtk::Field->new( 'ENT-E-CODUTL', 8 ),
        oEdtk::Field->new( 'ENT-E-CRTSEL', 30 ),
        oEdtk::Field->new( 'ENT-E-EDIMSG', 150 ),
        oEdtk::Field->new( 'ENT-E-EDICOP', 150 ),
        oEdtk::Field->new( 'ENT-E-CRTARC', 150 ),
        oEdtk::SignedField->new( 'ENT-E-IDFDST', 10 ),
        oEdtk::SignedField->new( 'ENT-E-RELDST', 8 ),
        oEdtk::SignedField->new( 'ENT-E-IDFB22', 15 ),
        oEdtk::SignedField->new( 'ENT-E-RELBL2', 15 ),
        oEdtk::SignedField->new( 'ENT-E-IDFB32', 15 ),
        oEdtk::SignedField->new( 'ENT-E-RELBL3', 15 ),
        oEdtk::SignedField->new( 'ENT-E-IDFGRP', 15 ),
        oEdtk::Field->new( 'ENT-E-INFDST', 700 ),
        oEdtk::Field->new( 'ENT-E-INFBL2', 700 ),
        oEdtk::Field->new( 'ENT-E-INFBL3', 700 ),
        oEdtk::Field->new( 'ENT-E-INFDLI', 700 ),
    );

    #	$record->set_seek_key("(ENTETE)");
    return $record;
}

1;
