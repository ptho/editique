package oEUser::Descriptor::R500;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R500-LGNDAT
        oEdtk::Field->new( 'R500-ACSTACASR2',    2 ),
        oEdtk::Field->new( 'R500-ACH4CODPRT',    10 ),
        oEdtk::Field->new( 'R500-ACH4CODGAR',    10 ),
        oEdtk::Field->new( 'R500-ACH4CODETA',    2 ),
        oEdtk::Field->new( 'R500-LL-ACH4CODETA', 50 ),
        oEdtk::Field->new( 'R500-LC-ACH4CODETA', 15 ),
        oEdtk::Field->new( 'R500-ACH4CODET1',    2 ),
        oEdtk::Field->new( 'R500-LL-ACH4CODET1', 50 ),
        oEdtk::Field->new( 'R500-LC-ACH4CODET1', 15 ),
        oEdtk::Field->new( 'R500-ACH4DDEPRD',    8 ),
        oEdtk::Field->new( 'R500-ACH4DFEPRD',    8 ),
        oEdtk::Field->new( 'R500-ACH4DDEADH',    8 ),
        oEdtk::Field->new( 'R500-ACH4DDERAD',    8 ),
        oEdtk::Field->new( 'R500-ACH4DDECOT',    8 ),
        oEdtk::Field->new( 'R500-ACH4TYPGBE',    1 ),
        oEdtk::Field->new( 'R500-LL-ACH4TYPGBE', 50 ),
        oEdtk::Field->new( 'R500-LC-ACH4TYPGBE', 15 ),
        oEdtk::Field->new( 'R500-ACH4DURGAR',    4 ),
        oEdtk::Field->new( 'R500-ACH4DFEEFC',    8 ),
        oEdtk::Field->new( 'R500-ACH4INDRSM',    1 ),
        oEdtk::Field->new( 'R500-ACH4INDRSA',    1 ),
        oEdtk::Field->new( 'R500-ACH4NUEGAR',    15 ),
        oEdtk::Field->new( 'R500-ACH4DATVTE',    8 ),
        oEdtk::Field->new( 'R500-ACH4DATANT',    8 ),
        oEdtk::SignedField->new( 'R500-ACH4COEMIG', 4, 5 ),
        oEdtk::Field->new( 'R500-ACH4ACGCR2',    6 ),
        oEdtk::Field->new( 'R500-ACH4DATNAI',    8 ),
        oEdtk::Field->new( 'R500-ACH4CODSEX',    1 ),
        oEdtk::Field->new( 'R500-LL-ACH4CODSEX', 50 ),
        oEdtk::Field->new( 'R500-LC-ACH4CODSEX', 15 ),
        oEdtk::Field->new( 'R500-PRGACODGAR',    10 ),
        oEdtk::Field->new( 'R500-LL-PRGACODGAR', 50 ),
        oEdtk::Field->new( 'R500-LC-PRGACODGAR', 15 ),
    );
}

1;
