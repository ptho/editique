package oEUser::Descriptor::C1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # C1-LGNDAT
        # C1-IDENT-DD
        oEdtk::Field->new( 'C1-GESTION-DST-DOC', 2 ),
        oEdtk::Field->new( 'C1-CODGRA-DD',       6 ),

        # C1-NOM-DD
        oEdtk::AddrField->new( 'C1-PENOCODTIT-DD', 10 ),
        oEdtk::Field->new( 'C1-PENOLICTIT-DD', 15 ),
        oEdtk::AddrField->new( 'C1-PENOLIBLON-DD', 50 ),

        # C1-ADRESSE-DD
        oEdtk::AddrField->new( 'C1-PNTREM-DD',     32 ),
        oEdtk::AddrField->new( 'C1-CPLADR-DD',     32 ),
        oEdtk::AddrField->new( 'C1-PEADNUMVOI-DD', 9 ),
        oEdtk::AddrField->new( 'C1-PEADBTQVOI-DD', 2 ),
        oEdtk::Field->new( 'C1-PEADLICBTQ-DD', 15 ),
        oEdtk::AddrField->new( 'C1-PEVONATVOI-DD', 5 ),
        oEdtk::Field->new( 'C1-PEADLICNTV-DD', 15 ),
        oEdtk::AddrField->new( 'C1-LIBVOI-DD', 32 ),
        oEdtk::Field->new( 'C1-INDCDX-DD', 1 ),
        oEdtk::AddrField->new( 'C1-BOIPOSLIBLDT-DD', 32 ),
        oEdtk::AddrField->new( 'C1-LIBLOCLIBLOC-DD', 32 ),
        oEdtk::Field->new( 'C1-CODCDXCODPOS-DD', 10 ),
        oEdtk::AddrField->new( 'C1-NOMCDXLIBACH-DD', 32 ),
        oEdtk::Field->new( 'C1-PEPACODPAY-DD', 3 ),
        oEdtk::AddrField->new( 'C1-PEPALICPAY-DD', 15 ),
        oEdtk::Field->new( 'C1-TRNFAC-DD', 4 ),

        # C1-IDENT-ERA
        oEdtk::Field->new( 'C1-TYPBLC-ER',  2 ),
        oEdtk::Field->new( 'C1-ODERCODERA', 9 ),

        # C1-NOM-ER
        oEdtk::AddrField->new( 'C1-PENOCODTIT-ER', 10 ),
        oEdtk::Field->new( 'C1-PENOLICTIT-ER', 15 ),
        oEdtk::AddrField->new( 'C1-PENOLIBLON-ER', 50 ),

        # C1-ADRESSE-ER
        oEdtk::AddrField->new( 'C1-PNTREM-ER',     32 ),
        oEdtk::AddrField->new( 'C1-CPLADR-ER',     32 ),
        oEdtk::AddrField->new( 'C1-PEADNUMVOI-ER', 9 ),
        oEdtk::AddrField->new( 'C1-PEADBTQVOI-ER', 2 ),
        oEdtk::Field->new( 'C1-PEADLICBTQ-ER', 15 ),
        oEdtk::AddrField->new( 'C1-PEVONATVOI-ER', 5 ),
        oEdtk::Field->new( 'C1-PEADLICNTV-ER', 15 ),
        oEdtk::AddrField->new( 'C1-LIBVOI-ER', 32 ),
        oEdtk::Field->new( 'C1-INDCDX-ER', 1 ),
        oEdtk::AddrField->new( 'C1-BOIPOSLIBLDT-ER', 32 ),
        oEdtk::AddrField->new( 'C1-LIBLOCLIBLOC-ER', 32 ),
        oEdtk::AddrField->new( 'C1-CODCDXCODPOS-ER', 10 ),
        oEdtk::AddrField->new( 'C1-NOMCDXLIBACH-ER', 32 ),
        oEdtk::Field->new( 'C1-PEPACODPAY-ER', 3 ),
        oEdtk::AddrField->new( 'C1-PEPALICPAY-ER', 15 ),
        oEdtk::Field->new( 'C1-TRNFAC-ER', 4 ),

        # C1-COMM-ER
        oEdtk::Field->new( 'C1-PECONATCOM-ER', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-ER', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-ER', 50 ),

        # C1-COMM-ER-2
        oEdtk::Field->new( 'C1-PECONATCOM-ER-2', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-ER-2', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-ER-2', 50 ),

        # C1-COMM-ER-3
        oEdtk::Field->new( 'C1-PECONATCOM-ER-3', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-ER-3', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-ER-3', 50 ),

        # C1-IDENT-CG
        oEdtk::Field->new( 'C1-TYPBLC-CG',  2 ),
        oEdtk::Field->new( 'C1-ODCGCODCGS', 9 ),

        # C1-NOM-CG
        oEdtk::Field->new( 'C1-PENOCODTIT-CG', 10 ),
        oEdtk::Field->new( 'C1-PENOLICTIT-CG', 15 ),
        oEdtk::Field->new( 'C1-PENOLIBLON-CG', 50 ),

        # C1-ADRESSE-CG
        oEdtk::Field->new( 'C1-PNTREM-CG',       32 ),
        oEdtk::Field->new( 'C1-CPLADR-CG',       32 ),
        oEdtk::Field->new( 'C1-PEADNUMVOI-CG',   9 ),
        oEdtk::Field->new( 'C1-PEADBTQVOI-CG',   2 ),
        oEdtk::Field->new( 'C1-PEADLICBTQ-CG',   15 ),
        oEdtk::Field->new( 'C1-PEVONATVOI-CG',   5 ),
        oEdtk::Field->new( 'C1-PEADLICNTV-CG',   15 ),
        oEdtk::Field->new( 'C1-LIBVOI-CG',       32 ),
        oEdtk::Field->new( 'C1-INDCDX-CG',       1 ),
        oEdtk::Field->new( 'C1-BOIPOSLIBLDT-CG', 32 ),
        oEdtk::Field->new( 'C1-LIBLOCLIBLOC-CG', 32 ),
        oEdtk::Field->new( 'C1-CODCDXCODPOS-CG', 10 ),
        oEdtk::Field->new( 'C1-NOMCDXLIBACH-CG', 32 ),
        oEdtk::Field->new( 'C1-PEPACODPAY-CG',   3 ),
        oEdtk::Field->new( 'C1-PEPALICPAY-CG',   15 ),
        oEdtk::Field->new( 'C1-TRNFAC-CG',       4 ),

        # C1-COMM-CG
        oEdtk::Field->new( 'C1-PECONATCOM-CG', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-CG', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-CG', 50 ),

        # C1-COMM-CG-2
        oEdtk::Field->new( 'C1-PECONATCOM-CG-2', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-CG-2', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-CG-2', 50 ),

        # C1-COMM-CG-3
        oEdtk::Field->new( 'C1-PECONATCOM-CG-3', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-CG-3', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-CG-3', 50 ),

        # C1-IDENT-PU
        oEdtk::Field->new( 'C1-TYPBLC-PU',  2 ),
        oEdtk::Field->new( 'C1-ODPUCODPUC', 9 ),

        # C1-NOM-PU
        oEdtk::Field->new( 'C1-PENOCODCIV-PU', 10 ),
        oEdtk::Field->new( 'C1-PENOLICCIV-PU', 15 ),
        oEdtk::Field->new( 'C1-PENOCODTIT-PU', 10 ),
        oEdtk::Field->new( 'C1-PENOLICTIT-PU', 15 ),
        oEdtk::Field->new( 'C1-PENOLIBLON-PU', 50 ),
        oEdtk::Field->new( 'C1-PENOPRN01-PU',  20 ),

        # C1-ADRESSE-PU
        oEdtk::Field->new( 'C1-PNTREM-PU',       32 ),
        oEdtk::Field->new( 'C1-CPLADR-PU',       32 ),
        oEdtk::Field->new( 'C1-PEADNUMVOI-PU',   9 ),
        oEdtk::Field->new( 'C1-PEADBTQVOI-PU',   2 ),
        oEdtk::Field->new( 'C1-PEADLICBTQ-PU',   15 ),
        oEdtk::Field->new( 'C1-PEVONATVOI-PU',   5 ),
        oEdtk::Field->new( 'C1-PEADLICNTV-PU',   15 ),
        oEdtk::Field->new( 'C1-LIBVOI-PU',       32 ),
        oEdtk::Field->new( 'C1-INDCDX-PU',       1 ),
        oEdtk::Field->new( 'C1-BOIPOSLIBLDT-PU', 32 ),
        oEdtk::Field->new( 'C1-LIBLOCLIBLOC-PU', 32 ),
        oEdtk::Field->new( 'C1-CODCDXCODPOS-PU', 10 ),
        oEdtk::Field->new( 'C1-NOMCDXLIBACH-PU', 32 ),
        oEdtk::Field->new( 'C1-PEPACODPAY-PU',   3 ),
        oEdtk::Field->new( 'C1-PEPALICPAY-PU',   15 ),
        oEdtk::Field->new( 'C1-TRNFAC-PU',       4 ),

        # C1-COMM-PU
        oEdtk::Field->new( 'C1-PECONATCOM-PU', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-PU', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-PU', 50 ),

        # C1-COMM-PU-2
        oEdtk::Field->new( 'C1-PECONATCOM-PU-2', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-PU-2', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-PU-2', 50 ),

        # C1-COMM-PU-3
        oEdtk::Field->new( 'C1-PECONATCOM-PU-3', 5 ),
        oEdtk::Field->new( 'C1-PENOLICCOM-PU-3', 15 ),
        oEdtk::Field->new( 'C1-PECOVALCOM-PU-3', 50 ),

        # C1-NUMERO-PERSONNES
        oEdtk::Field->new( 'C1-PENONUMPER-DD', 8 ),
        oEdtk::Field->new( 'C1-PENONUMPER-ER', 8 ),
        oEdtk::Field->new( 'C1-PENONUMPER-CG', 8 ),
        oEdtk::Field->new( 'C1-PENONUMPER-PU', 8 ),

        # C1-DST-ZZEDIT-COMM
        oEdtk::Field->new( 'C1-DST-PECONATCOM',     5 ),
        oEdtk::Field->new( 'C1-DST-PEZZ-LICNATCOM', 15 ),
        oEdtk::Field->new( 'C1-DST-PECOVALCOMM',    50 ),

        # C1-DST-ZZEDIT-COMM-2
        oEdtk::Field->new( 'C1-DST-PECONATCOM-2',     5 ),
        oEdtk::Field->new( 'C1-DST-PEZZ-LICNATCOM-2', 15 ),
        oEdtk::Field->new( 'C1-DST-PECOVALCOMM-2',    50 ),

        # C1-DST-ZZEDIT-COMM-3
        oEdtk::Field->new( 'C1-DST-PECONATCOM-3',     5 ),
        oEdtk::Field->new( 'C1-DST-PEZZ-LICNATCOM-3', 15 ),
        oEdtk::Field->new( 'C1-DST-PECOVALCOMM-3',    50 ),

        # C1-DST-ZZEDIT-COMM-4
        oEdtk::Field->new( 'C1-DST-PECONATCOM-4',     5 ),
        oEdtk::Field->new( 'C1-DST-PEZZ-LICNATCOM-4', 15 ),
        oEdtk::Field->new( 'C1-DST-PECOVALCOMM-4',    50 ),

        # C1-DST-ZZEDIT-COMM-5
        oEdtk::Field->new( 'C1-DST-PECONATCOM-5',     5 ),
        oEdtk::Field->new( 'C1-DST-PEZZ-LICNATCOM-5', 15 ),
        oEdtk::Field->new( 'C1-DST-PECOVALCOMM-5',    50 ),
    );
}

1;
