package oEUser::Descriptor::F3;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::DateField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # F3-LGNDAT
        oEdtk::DateField->new( 'F3-DDEPRD', 8 ),
        oEdtk::DateField->new( 'F3-DFEPRD', 8 ),

        # F3-RIB-PY
        oEdtk::Field->new( 'F3-PERICPTTIT',       50 ),
        oEdtk::Field->new( 'F3-PERICODBQE',       10 ),
        oEdtk::Field->new( 'F3-PERICODGUI',       10 ),
        oEdtk::Field->new( 'F3-PERINUMCPT',       20 ),
        oEdtk::Field->new( 'F3-PERICLECPT',       2 ),
        oEdtk::Field->new( 'F3-PERICLEIBA',       4 ),
        oEdtk::Field->new( 'F3-PEPACODPAY',       3 ),
        oEdtk::Field->new( 'F3-PEPALICCODPAY',    15 ),
        oEdtk::Field->new( 'F3-PERICODDEV',       3 ),
        oEdtk::Field->new( 'F3-PEPALICCODDEV',    15 ),
        oEdtk::Field->new( 'F3-NUMPART',          5 ),
        oEdtk::Field->new( 'F3-NUMOPE-AVIS',      5 ),
        oEdtk::Field->new( 'F3-NUMCPT-TIP',       24 ),
        oEdtk::Field->new( 'F3-ORIGINE-DEM-AVIS', 15 ),

        # F3-SEPA
        oEdtk::Field->new( 'F3-PERICODPA2', 2 ),
        oEdtk::Field->new( 'F3-PERICODBIC', 14 ),
        oEdtk::Field->new( 'F3-PERIIDTNAT', 30 ),
        oEdtk::Field->new( 'F3-ICS',        13 ),
        oEdtk::Field->new( 'F3-RUI',        35 ),
        oEdtk::Field->new( 'F3-RUM',        35 ),
        oEdtk::DateField->new( 'F3-DAT-SIG', 8 ),

        # F3-FILLER
        oEdtk::Field->new( 'F3-FILLER', 89 ),
    );
}

1;
