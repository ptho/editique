package oEUser::Descriptor::L1;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::DateField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # CE0011C-L1-LGNDAT
        # CE0011C-L1-ACH2
        # CE0011C-L1-ADMIN-ASU-RO1
        # CE0011C-L1-ACH2-PEREI1-ALPHA
        oEdtk::Field->new( 'CE0011C-L1-ACH2-PEREI1-ETENDU', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-CODCIV',      10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-LICCODCIV',   15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-CODTIT',      10 ),
        oEdtk::Field->new( 'CE011C-L1-ACH2-P-LICCODTIT',    15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-LIBLON',      50 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-PRN01',       20 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-ACZZTYPTAS',    10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-LIBCODTAS',     15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-LIBLONTAS',     50 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-CODAXT',        10 ),

        # CE0011C-L1-ACH2-RNGASU-ALPHA
        oEdtk::Field->new( 'CE0011C-L1-ACH2-RNGASU-ETENDU', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-CODCSO',        12 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-LIBCODCSO',   15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-LIBLONCSO',   50 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-CODCSP',        4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-LIBCODCSP',   15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-P-LIBLONCSP',   50 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH2-DATNAI', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-DDE1AD', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH2-DDEZRA', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-TYPDNA',  2 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-NUMRO',   13 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-CLERO',   2 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-DDEDRO',  8 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-DFEDRO',  8 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-CODGRE',  2 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-CODCRO',  3 ),
        oEdtk::Field->new( 'CE0011C-L1-RO1-CODCPA',  4 ),

        # CE0011C-L1-ACH4
        oEdtk::Field->new( 'CE0011C-L1-ACH4OPTMUT',      10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4-LIBCCODMUT', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4-LIBLONMUT',  50 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DFEOPT',      8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4JUSQUAU',     8 ),

        # CE0011C-L1-INFOS-PRODUITS
        oEdtk::Field->new( 'CE0011C-L1-ACH4ACGSR3', 5 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODPRT', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-PRPRLIBCOU', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-PRGPTYPGAR', 1 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4ACGSR4', 5 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODGAR', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-PRGALIBCOU', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODFRI', 6 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4LICFRI', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODCLR', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4LICCLR', 15 ),

        # CE0011C-L1-ACH4-GAR
        # CE0011C-L1-ACH4NUMCOL-ALPHA
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN',        2 ),

        # CE0011C-L1-ACH4-GAR-2
        # CE0011C-L1-ACH4NUMCOL-ALPHA-2
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-2', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-2',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-2',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-2', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-2', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-2', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-2',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-2', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-2
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-2', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-2',        2 ),

        # CE0011C-L1-ACH4-GAR-3
        # CE0011C-L1-ACH4NUMCOL-ALPHA-3
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-3', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-3',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-3',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-3', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-3', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-3', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-3',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-3', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-3
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-3', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-3',        2 ),

        # CE0011C-L1-ACH4-GAR-4
        # CE0011C-L1-ACH4NUMCOL-ALPHA-4
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-4', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-4',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-4',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-4', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-4', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-4', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-4',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-4', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-4
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-4', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-4',        2 ),

        # CE0011C-L1-ACH4-GAR-5
        # CE0011C-L1-ACH4NUMCOL-ALPHA-5
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-5', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-5',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-5',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-5', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-5', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-5', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-5',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-5', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-5
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-5', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-5',        2 ),

        # CE0011C-L1-ACH4-GAR-6
        # CE0011C-L1-ACH4NUMCOL-ALPHA-6
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-6', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-6',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-6',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-6', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-6', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-6', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-6',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-6', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-6
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-6', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-6',        2 ),

        # CE0011C-L1-ACH4-GAR-7
        # CE0011C-L1-ACH4NUMCOL-ALPHA-7
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-7', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-7',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-7',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-7', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-7', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-7', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-7',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-7', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-7
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-7', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-7',        2 ),

        # CE0011C-L1-ACH4-GAR-8
        # CE0011C-L1-ACH4NUMCOL-ALPHA-8
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-8', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-8',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-8',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-8', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-8', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-8', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-8',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-8', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-8
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-8', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-8',        2 ),

        # CE0011C-L1-ACH4-GAR-9
        # CE0011C-L1-ACH4NUMCOL-ALPHA-9
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-9', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-9',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-9',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-9', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-9', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-9', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-9',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-9', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-9
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-9', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-9',        2 ),

        # CE0011C-L1-ACH4-GAR-10
        # CE0011C-L1-ACH4NUMCOL-ALPHA-10
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-10', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-10',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-10',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-10', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-10', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-10', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-10',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-10', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-10
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-10', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-10',        2 ),

        # CE0011C-L1-ACH4-GAR-11
        # CE0011C-L1-ACH4NUMCOL-ALPHA-11
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-11', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-11',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-11',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-11', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-11', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-11', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-11',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-11', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-11
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-11', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-11',        2 ),

        # CE0011C-L1-ACH4-GAR-12
        # CE0011C-L1-ACH4NUMCOL-ALPHA-12
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-12', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-12',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-12',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-12', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-12', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-12', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-12',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-12', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-12
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-12', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-12',        2 ),

        # CE0011C-L1-ACH4-GAR-13
        # CE0011C-L1-ACH4NUMCOL-ALPHA-13
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-13', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-13',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-13',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-13', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-13', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-13', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-13',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-13', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-13
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-13', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-13',        2 ),

        # CE0011C-L1-ACH4-GAR-14
        # CE0011C-L1-ACH4NUMCOL-ALPHA-14
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-14', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-14',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-14',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-14', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-14', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-14', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-14',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-14', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-14
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-14', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-14',        2 ),

        # CE0011C-L1-ACH4-GAR-15
        # CE0011C-L1-ACH4NUMCOL-ALPHA-15
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-15', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-15',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-15',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-15', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-15', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-15', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-15',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-15', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-15
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-15', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-15',        2 ),

        # CE0011C-L1-ACH4-GAR-16
        # CE0011C-L1-ACH4NUMCOL-ALPHA-16
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-16', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-16',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-16',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-16', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-16', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-16', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-16',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-16', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-16
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-16', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-16',        2 ),

        # CE0011C-L1-ACH4-GAR-17
        # CE0011C-L1-ACH4NUMCOL-ALPHA-17
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-17', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-17',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-17',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-17', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-17', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-17', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-17',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-17', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-17
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-17', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-17',        2 ),

        # CE0011C-L1-ACH4-GAR-18
        # CE0011C-L1-ACH4NUMCOL-ALPHA-18
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-18', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-18',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-18',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-18', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-18', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-18', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-18',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-18', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-18
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-18', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-18',        2 ),

        # CE0011C-L1-ACH4-GAR-19
        # CE0011C-L1-ACH4NUMCOL-ALPHA-19
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-19', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-19',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-19',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-19', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-19', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-19', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-19',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-19', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-19
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-19', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-19',        2 ),

        # CE0011C-L1-ACH4-GAR-20
        # CE0011C-L1-ACH4NUMCOL-ALPHA-20
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-20', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-20',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-20',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-20', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-20', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-20', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-20',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-20', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-20
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-20', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-20',        2 ),

        # CE0011C-L1-ACH4-GAR-21
        # CE0011C-L1-ACH4NUMCOL-ALPHA-21
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-21', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-21',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-21',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-21', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-21', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-21', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-21',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-21', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-21
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-21', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-21',        2 ),

        # CE0011C-L1-ACH4-GAR-22
        # CE0011C-L1-ACH4NUMCOL-ALPHA-22
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-22', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-22',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-22',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-22', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-22', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-22', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-22',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-22', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-22
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-22', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-22',        2 ),

        # CE0011C-L1-ACH4-GAR-23
        # CE0011C-L1-ACH4NUMCOL-ALPHA-23
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-23', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-23',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-23',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-23', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-23', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-23', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-23',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-23', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-23
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-23', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-23',        2 ),

        # CE0011C-L1-ACH4-GAR-24
        # CE0011C-L1-ACH4NUMCOL-ALPHA-24
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-24', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-24',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-24',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-24', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-24', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-24', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-24',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-24', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-24
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-24', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-24',        2 ),

        # CE0011C-L1-ACH4-GAR-25
        # CE0011C-L1-ACH4NUMCOL-ALPHA-25
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-25', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-25',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-25',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-25', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-25', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-25', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-25',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-25', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-25
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-25', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-25',        2 ),

        # CE0011C-L1-ACH4-GAR-26
        # CE0011C-L1-ACH4NUMCOL-ALPHA-26
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-26', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-26',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-26',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-26', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-26', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-26', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-26',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-26', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-26
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-26', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-26',        2 ),

        # CE0011C-L1-ACH4-GAR-27
        # CE0011C-L1-ACH4NUMCOL-ALPHA-27
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-27', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-27',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-27',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-27', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-27', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-27', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-27',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-27', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-27
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-27', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-27',        2 ),

        # CE0011C-L1-ACH4-GAR-28
        # CE0011C-L1-ACH4NUMCOL-ALPHA-28
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-28', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-28',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-28',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-28', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-28', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-28', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-28',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-28', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-28
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-28', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-28',        2 ),

        # CE0011C-L1-ACH4-GAR-29
        # CE0011C-L1-ACH4NUMCOL-ALPHA-29
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-29', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-29',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-29',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-29', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-29', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-29', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-29',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-29', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-29
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-29', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-29',        2 ),

        # CE0011C-L1-ACH4-GAR-30
        # CE0011C-L1-ACH4NUMCOL-ALPHA-30
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-30', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-30',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-30',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-30', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-30', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-30', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-30',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-30', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-30
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-30', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-30',        2 ),

        # CE0011C-L1-ACH4-GAR-31
        # CE0011C-L1-ACH4NUMCOL-ALPHA-31
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-31', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-31',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-31',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-31', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-31', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-31', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-31',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-31', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-31
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-31', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-31',        2 ),

        # CE0011C-L1-ACH4-GAR-32
        # CE0011C-L1-ACH4NUMCOL-ALPHA-32
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMCOL-ETENDU-32', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDEADH-32',        8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4DDERAD-32',        8 ),
        oEdtk::DateField->new( 'CE0011C-L1-ACH4DDEPRD-32', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALALP-32', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4VALNUM-32', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4UNITE-32',  4 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4TYPCON-32', 1 ),

        # CE0011C-L1-ACH4NUMREN-ALPHA-32
        oEdtk::Field->new( 'CE0011C-L1-ACH4NUMREN-ETENDU-32', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-ACH4CODREN-32',        2 ),

        # CE0011C-L1-INFOS-PROFIL-PROF
        oEdtk::Field->new( 'CE0011C-L1-PEPPLIBMAT', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPCODUSI', 10 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPCODATE', 10 ),

        # CE0011C-L1-ACH2-IND
        oEdtk::Field->new( 'CE0011C-L1-ACH2-INDTLT', 1 ),

        # CE0011C-L1-INFOS-PROFIL-PROF-P
        oEdtk::Field->new( 'CE0011C-L1-PEPPCODEXP', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-PEZZLICEXP', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPETBPCM', 20 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPCOEGRD', 5 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPCODFCT', 5 ),
        oEdtk::Field->new( 'CE0011C-L1-PEZZLICFCT', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPTYPCNG', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-PEZZLICCNG', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPTYPCTT', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-PEZZLICCTT', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPCODPSU', 2 ),
        oEdtk::Field->new( 'CE0011C-L1-PEZZLICPSU', 15 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPDDEACT', 8 ),
        oEdtk::Field->new( 'CE0011C-L1-PEPPDFEACT', 8 ),

        # CE0011C-L1-INFOS-RO2
        oEdtk::Field->new( 'CE0011C-L1-RO2-NUMRO', 13 ),
        oEdtk::Field->new( 'CE0011C-L1-RO2-CLERO', 2 ),
    );
}

1;
