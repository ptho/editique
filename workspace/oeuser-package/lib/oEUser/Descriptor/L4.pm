package oEUser::Descriptor::L4;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # L4-LGNDAT
        # L4-IDENT-CI
        oEdtk::Field->new( 'L4-NUMPART',    5 ),
        oEdtk::Field->new( 'L4-ACH1NUMCIN', 15 ),

        # L4-INFO-MONTANT
        oEdtk::SignedField->new( 'L4-MONTANT-HT',  12, 2 ),
        oEdtk::SignedField->new( 'L4-MONTANT-TTC', 12, 2 ),

        # L4-INFO-PERIODE
        oEdtk::Field->new( 'L4-ACCODDEPRD', 8 ),
        oEdtk::Field->new( 'L4-ACCODFEPRD', 8 ),

        # L4-INFO-ADMIN-AS
        oEdtk::Field->new( 'L4-IDFSYS-ASSUR', 8 ),

        # L4-NOM-AS
        oEdtk::Field->new( 'L4-PENOCODCIV-AS',       10 ),
        oEdtk::Field->new( 'L4-PENOLICCIV-AS',       15 ),
        oEdtk::Field->new( 'L4-PENOCODTIT-AS',       10 ),
        oEdtk::Field->new( 'L4-PENOLICTIT-AS',       15 ),
        oEdtk::Field->new( 'L4-PENOLIBLON-AS',       50 ),
        oEdtk::Field->new( 'L4-PENOPRN01-AS',        20 ),
        oEdtk::Field->new( 'L4-CODTYP-ASSUR',        6 ),
        oEdtk::Field->new( 'L4-LIBCOU-TYP-ASSUR',    15 ),
        oEdtk::Field->new( 'L4-LIBLON-TYP-ASSUR',    50 ),
        oEdtk::Field->new( 'L4-RANG-ASSUR',          2 ),
        oEdtk::Field->new( 'L4-CODEXTERNE-ASSUR',    10 ),
        oEdtk::Field->new( 'L4-CATEG-SOCIALE-ASSUR', 12 ),
        oEdtk::Field->new( 'L4-LIBCOU-CSO-ASSUR',    15 ),
        oEdtk::Field->new( 'L4-LIBLON-CSO-ASSUR',    50 ),
        oEdtk::Field->new( 'L4-CODCSP-ASSUR',        4 ),
        oEdtk::Field->new( 'L4-LIBCOU-CSP-ASSUR',    15 ),
        oEdtk::Field->new( 'L4-LIBLON-CSP-ASSUR',    50 ),
        oEdtk::Field->new( 'L4-CODPROF-ASSURE',      5 ),
        oEdtk::Field->new( 'L4-LIBCOU-PRF-ASSUR',    15 ),
        oEdtk::Field->new( 'L4-LIBLON-PRF-ASSUR',    50 ),
        oEdtk::Field->new( 'L4-DATNAISSANCE-ASSUR',  8 ),
        oEdtk::Field->new( 'L4-DATADHESION-ASSUR',   8 ),
        oEdtk::Field->new( 'L4-DATRADIATION-ASSUR',  8 ),
        oEdtk::Field->new( 'L4-TYPE-DATNAISSANCE',   2 ),

        # L4-INFO-RO
        oEdtk::Field->new( 'L4-NUMRO-ASSUR',        13 ),
        oEdtk::Field->new( 'L4-CLE-RO-ASSUR',       2 ),
        oEdtk::Field->new( 'L4-DDE-DROITS-RO',      8 ),
        oEdtk::Field->new( 'L4-DFE-DROITS-RO',      8 ),
        oEdtk::Field->new( 'L4-CODREGIME-RO-ASSUR', 2 ),
        oEdtk::Field->new( 'L4-CODCAISSE-RO-ASSUR', 3 ),
        oEdtk::Field->new( 'L4-CODCENTREPAI-ASSUR', 4 ),
        oEdtk::Field->new( 'L4-IND-CETIP',          1 ),
        oEdtk::Field->new( 'L4-DDE-CETIP',          8 ),
        oEdtk::Field->new( 'L4-DFE-CETIP',          8 ),
        oEdtk::Field->new( 'L4-IND-CMU',            1 ),
        oEdtk::Field->new( 'L4-IND-TLT',            1 ),
        oEdtk::Field->new( 'L4-DDE-TLT',            8 ),
        oEdtk::Field->new( 'L4-DFE-TLT',            8 ),

        # L4-INFO-PS
        oEdtk::Field->new( 'L4-PRPRCODPDT', 10 ),
        oEdtk::Field->new( 'L4-PRPRLIBCOU', 15 ),
        oEdtk::Field->new( 'L4-PRPRLIBLON', 50 ),

        # L4-INFO-GS
        oEdtk::Field->new( 'L4-PRGACODGAR', 10 ),
        oEdtk::Field->new( 'L4-PRGALIBCOU', 15 ),
        oEdtk::Field->new( 'L4-PRGALIBLON', 50 ),

        # L4-INFO-OPE
        oEdtk::Field->new( 'L4-ACOPNUMOPE', 5 ),
        oEdtk::Field->new( 'L4-ACOPDDEOPE', 8 ),
        oEdtk::Field->new( 'L4-ACOPDDECOT', 8 ),
        oEdtk::Field->new( 'L4-ACOPNUMAVT', 3 ),
        oEdtk::Field->new( 'L4-ACOPCODAGC', 4 ),
        oEdtk::Field->new( 'L4-ACOPLICAGC', 15 ),
        oEdtk::Field->new( 'L4-ACOPLIBAGC', 50 ),
        oEdtk::Field->new( 'L4-ACOPCODCTX', 4 ),
        oEdtk::Field->new( 'L4-ACOPLICCTX', 15 ),
        oEdtk::Field->new( 'L4-ACOPLIBCTX', 50 ),
        oEdtk::Field->new( 'L4-ACOPCODTRT', 5 ),

        # L4-INFO-ASS
        oEdtk::Field->new( 'L4-PEPPLIBMAT',    15 ),
        oEdtk::Field->new( 'L4-PEPPCODUSI',    10 ),
        oEdtk::Field->new( 'L4-PEPPCODATE',    10 ),
        oEdtk::Field->new( 'L4-PEPPCODEXP',    2 ),
        oEdtk::Field->new( 'L4-PEZZLICEXP',    15 ),
        oEdtk::Field->new( 'L4-PEPPETBPCM',    20 ),
        oEdtk::Field->new( 'L4-PEPPCOEGRD',    5 ),
        oEdtk::Field->new( 'L4-PEPPCODFCT',    5 ),
        oEdtk::Field->new( 'L4-PEZZLICFCT',    15 ),
        oEdtk::Field->new( 'L4-PEPPTYPCNG',    2 ),
        oEdtk::Field->new( 'L4-PEZZLICCNG',    15 ),
        oEdtk::Field->new( 'L4-PEPPTYPCTT',    2 ),
        oEdtk::Field->new( 'L4-PEZZLICCTT',    15 ),
        oEdtk::Field->new( 'L4-PEPPCODPSU',    2 ),
        oEdtk::Field->new( 'L4-PEZZLICPSU',    15 ),
        oEdtk::Field->new( 'L4-PEPPDDEACT',    8 ),
        oEdtk::Field->new( 'L4-PEPPDFEACT',    8 ),
        oEdtk::Field->new( 'L4-DDE-CSO-ASSUR', 8 ),
    );
}

1;
