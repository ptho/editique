package oEUser::Descriptor::H7;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # H7-LGNDAT
        oEdtk::Field->new( 'H7-ACCBACASR2',        2 ),
        oEdtk::Field->new( 'H7-ACH4CODGAR',        10 ),
        oEdtk::Field->new( 'H7-ACH4CODPRT',        10 ),
        oEdtk::Field->new( 'H7-ACCBACGSR3',        5 ),
        oEdtk::Field->new( 'H7-ACCBACGSR4',        5 ),
        oEdtk::Field->new( 'H7-ACCLNUMCAU',        5 ),
        oEdtk::Field->new( 'H7-ACCBDDEPRD',        8 ),
        oEdtk::Field->new( 'H7-ACCBDFEPRD',        8 ),
        oEdtk::Field->new( 'H7-ACCBTYPTXT',        1 ),
        oEdtk::Field->new( 'H7-LIBCOU-ACCBTYPTXT', 15 ),
        oEdtk::Field->new( 'H7-ACCBPRCLI1',        8 ),

        # H7-TEXTE-CLAUSE
        oEdtk::Field->new( 'H7-TEXCL1',     250 ),
        oEdtk::Field->new( 'H7-TEXCL2',     250 ),
        oEdtk::Field->new( 'H7-TEXCL3',     250 ),
        oEdtk::Field->new( 'H7-TEXCL4',     250 ),
        oEdtk::Field->new( 'H7-RESERVE-H7', 200 ),
    );
}

1;
