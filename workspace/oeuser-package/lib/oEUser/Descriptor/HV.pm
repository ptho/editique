package oEUser::Descriptor::HV;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # HV-LGNDAT
        oEdtk::Field->new( 'HV-ACH4ACASR2', 2 ),
        oEdtk::Field->new( 'HV-ACH4CODPRT', 10 ),
        oEdtk::Field->new( 'HV-ACH4CODGAR', 10 ),
        oEdtk::Field->new( 'HV-ACVHACKGR3', 5 ),
        oEdtk::Field->new( 'HV-ACVHACKGR4', 5 ),
        oEdtk::Field->new( 'HV-ACVHACKGR5', 5 ),
        oEdtk::Field->new( 'HV-ACVHDDEPRD', 8 ),
        oEdtk::SignedField->new( 'HV-ACVHVALEFC', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE1', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL1', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE2', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL2', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE3', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL3', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE4', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL4', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE5', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL5', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE6', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL6', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE7', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL7', 14, 2 ),
        oEdtk::Field->new( 'HV-ANNEE8', 4 ),
        oEdtk::SignedField->new( 'HV-ACVHVAL8', 14, 2 ),
    );
}

1;
