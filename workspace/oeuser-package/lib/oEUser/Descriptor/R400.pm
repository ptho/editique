package oEUser::Descriptor::R400;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R400-LGNDAT
        oEdtk::Field->new( 'R400-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R400-PENOLIBLON',    50 ),
        oEdtk::Field->new( 'R400-PENOPRN01',     20 ),
        oEdtk::Field->new( 'R400-PEADCODPOS',    5 ),
        oEdtk::Field->new( 'R400-ACH2DDEPRD',    8 ),
        oEdtk::Field->new( 'R400-ACH2DFEPRD',    8 ),
        oEdtk::Field->new( 'R400-ACH2CODETA',    2 ),
        oEdtk::Field->new( 'R400-LL-ACH2CODETA', 50 ),
        oEdtk::Field->new( 'R400-LC-ACH2CODETA', 15 ),
        oEdtk::Field->new( 'R400-ACH2CODAXT',    10 ),
        oEdtk::Field->new( 'R400-ACH2DDE1AD',    8 ),
        oEdtk::Field->new( 'R400-ACH2DDEZRA',    8 ),
        oEdtk::Field->new( 'R400-ACH2DATANT',    8 ),
        oEdtk::Field->new( 'R400-ACH2CODTAS',    6 ),
        oEdtk::Field->new( 'R400-LL-ACH2CODTAS', 50 ),
        oEdtk::Field->new( 'R400-LC-ACH2CODTAS', 15 ),
        oEdtk::Field->new( 'R400-ACH2INDCMU',    1 ),
        oEdtk::Field->new( 'R400-ACH2INDRSM',    1 ),
        oEdtk::Field->new( 'R400-ACH2INDRSA',    1 ),
        oEdtk::Field->new( 'R400-ACH2INDTLT',    1 ),
        oEdtk::Field->new( 'R400-ACH2DDETLT',    8 ),
        oEdtk::Field->new( 'R400-ACH2DFETLT',    8 ),
        oEdtk::Field->new( 'R400-ACH2INDSVI',    1 ),
        oEdtk::Field->new( 'R400-ACH2INDTIP',    1 ),
        oEdtk::Field->new( 'R400-ACH2DDETIP',    8 ),
        oEdtk::Field->new( 'R400-ACH2DFETIP',    8 ),
        oEdtk::Field->new( 'R400-ACH2CODPAY',    3 ),
        oEdtk::Field->new( 'R400-LL-ACH2CODPAY', 50 ),
        oEdtk::Field->new( 'R400-LC-ACH2CODPAY', 15 ),
        oEdtk::Field->new( 'R400-ACH2CODDPT',    3 ),
        oEdtk::Field->new( 'R400-LL-ACH2CODDPT', 50 ),
        oEdtk::Field->new( 'R400-LC-ACH2CODDPT', 15 ),
        oEdtk::Field->new( 'R400-ACH2CODCLR',    2 ),
        oEdtk::Field->new( 'R400-LL-ACH2CODCLR', 50 ),
        oEdtk::Field->new( 'R400-LC-ACH2CODCLR', 15 ),
        oEdtk::Field->new( 'R400-ACH2CODGRE',    2 ),
        oEdtk::Field->new( 'R400-LL-ACH2CODGRE', 50 ),
        oEdtk::Field->new( 'R400-LC-ACH2CODGRE', 15 ),
        oEdtk::Field->new( 'R400-ACH2CODCRO',    3 ),
        oEdtk::Field->new( 'R400-ACH2CODCPA',    4 ),
    );
}

1;
