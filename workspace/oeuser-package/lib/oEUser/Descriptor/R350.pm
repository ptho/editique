package oEUser::Descriptor::R350;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R350-LGNDAT
        oEdtk::Field->new( 'R350-PEPENUMPER', 8 ),
        oEdtk::Field->new( 'R350-PENOLIBLON', 50 ),
        oEdtk::Field->new( 'R350-PENOPRN01',  20 ),
        oEdtk::Field->new( 'R350-PEADCODPOS', 5 ),
        oEdtk::Field->new( 'R350-ACSODDEPRD', 8 ),
        oEdtk::Field->new( 'R350-ACSODFEPRD', 8 ),
    );
}

1;
