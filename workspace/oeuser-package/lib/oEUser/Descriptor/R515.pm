package oEUser::Descriptor::R515;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R515-LGNDAT
        # R515-LST-CLAUSES
        oEdtk::Field->new( 'R515-ACCBACASR2',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT', 50 ),

        # R515-TEXTE-CLAUSE
        oEdtk::Field->new( 'R515-TEXCL1', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4', 250 ),

        # R515-LST-CLAUSES-2
        oEdtk::Field->new( 'R515-ACCBACASR2-2',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-2',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-2',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-2',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-2',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-2',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-2',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-2', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-2', 50 ),

        # R515-TEXTE-CLAUSE-2
        oEdtk::Field->new( 'R515-TEXCL1-2', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-2', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-2', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-2', 250 ),

        # R515-LST-CLAUSES-3
        oEdtk::Field->new( 'R515-ACCBACASR2-3',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-3',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-3',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-3',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-3',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-3',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-3',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-3', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-3', 50 ),

        # R515-TEXTE-CLAUSE-3
        oEdtk::Field->new( 'R515-TEXCL1-3', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-3', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-3', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-3', 250 ),

        # R515-LST-CLAUSES-4
        oEdtk::Field->new( 'R515-ACCBACASR2-4',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-4',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-4',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-4',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-4',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-4',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-4',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-4', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-4', 50 ),

        # R515-TEXTE-CLAUSE-4
        oEdtk::Field->new( 'R515-TEXCL1-4', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-4', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-4', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-4', 250 ),

        # R515-LST-CLAUSES-5
        oEdtk::Field->new( 'R515-ACCBACASR2-5',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-5',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-5',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-5',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-5',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-5',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-5',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-5', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-5', 50 ),

        # R515-TEXTE-CLAUSE-5
        oEdtk::Field->new( 'R515-TEXCL1-5', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-5', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-5', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-5', 250 ),

        # R515-LST-CLAUSES-6
        oEdtk::Field->new( 'R515-ACCBACASR2-6',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-6',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-6',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-6',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-6',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-6',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-6',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-6', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-6', 50 ),

        # R515-TEXTE-CLAUSE-6
        oEdtk::Field->new( 'R515-TEXCL1-6', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-6', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-6', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-6', 250 ),

        # R515-LST-CLAUSES-7
        oEdtk::Field->new( 'R515-ACCBACASR2-7',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-7',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-7',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-7',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-7',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-7',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-7',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-7', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-7', 50 ),

        # R515-TEXTE-CLAUSE-7
        oEdtk::Field->new( 'R515-TEXCL1-7', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-7', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-7', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-7', 250 ),

        # R515-LST-CLAUSES-8
        oEdtk::Field->new( 'R515-ACCBACASR2-8',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-8',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-8',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-8',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-8',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-8',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-8',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-8', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-8', 50 ),

        # R515-TEXTE-CLAUSE-8
        oEdtk::Field->new( 'R515-TEXCL1-8', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-8', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-8', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-8', 250 ),

        # R515-LST-CLAUSES-9
        oEdtk::Field->new( 'R515-ACCBACASR2-9',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-9',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-9',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-9',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-9',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-9',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-9',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-9', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-9', 50 ),

        # R515-TEXTE-CLAUSE-9
        oEdtk::Field->new( 'R515-TEXCL1-9', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-9', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-9', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-9', 250 ),

        # R515-LST-CLAUSES-10
        oEdtk::Field->new( 'R515-ACCBACASR2-10',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-10',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-10',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-10',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-10',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-10',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-10',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-10', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-10', 50 ),

        # R515-TEXTE-CLAUSE-10
        oEdtk::Field->new( 'R515-TEXCL1-10', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-10', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-10', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-10', 250 ),

        # R515-LST-CLAUSES-11
        oEdtk::Field->new( 'R515-ACCBACASR2-11',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-11',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-11',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-11',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-11',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-11',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-11',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-11', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-11', 50 ),

        # R515-TEXTE-CLAUSE-11
        oEdtk::Field->new( 'R515-TEXCL1-11', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-11', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-11', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-11', 250 ),

        # R515-LST-CLAUSES-12
        oEdtk::Field->new( 'R515-ACCBACASR2-12',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-12',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-12',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-12',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-12',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-12',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-12',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-12', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-12', 50 ),

        # R515-TEXTE-CLAUSE-12
        oEdtk::Field->new( 'R515-TEXCL1-12', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-12', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-12', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-12', 250 ),

        # R515-LST-CLAUSES-13
        oEdtk::Field->new( 'R515-ACCBACASR2-13',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-13',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-13',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-13',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-13',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-13',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-13',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-13', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-13', 50 ),

        # R515-TEXTE-CLAUSE-13
        oEdtk::Field->new( 'R515-TEXCL1-13', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-13', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-13', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-13', 250 ),

        # R515-LST-CLAUSES-14
        oEdtk::Field->new( 'R515-ACCBACASR2-14',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-14',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-14',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-14',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-14',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-14',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-14',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-14', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-14', 50 ),

        # R515-TEXTE-CLAUSE-14
        oEdtk::Field->new( 'R515-TEXCL1-14', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-14', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-14', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-14', 250 ),

        # R515-LST-CLAUSES-15
        oEdtk::Field->new( 'R515-ACCBACASR2-15',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-15',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-15',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-15',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-15',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-15',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-15',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-15', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-15', 50 ),

        # R515-TEXTE-CLAUSE-15
        oEdtk::Field->new( 'R515-TEXCL1-15', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-15', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-15', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-15', 250 ),

        # R515-LST-CLAUSES-16
        oEdtk::Field->new( 'R515-ACCBACASR2-16',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-16',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-16',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-16',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-16',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-16',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-16',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-16', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-16', 50 ),

        # R515-TEXTE-CLAUSE-16
        oEdtk::Field->new( 'R515-TEXCL1-16', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-16', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-16', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-16', 250 ),

        # R515-LST-CLAUSES-17
        oEdtk::Field->new( 'R515-ACCBACASR2-17',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-17',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-17',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-17',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-17',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-17',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-17',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-17', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-17', 50 ),

        # R515-TEXTE-CLAUSE-17
        oEdtk::Field->new( 'R515-TEXCL1-17', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-17', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-17', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-17', 250 ),

        # R515-LST-CLAUSES-18
        oEdtk::Field->new( 'R515-ACCBACASR2-18',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-18',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-18',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-18',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-18',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-18',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-18',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-18', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-18', 50 ),

        # R515-TEXTE-CLAUSE-18
        oEdtk::Field->new( 'R515-TEXCL1-18', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-18', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-18', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-18', 250 ),

        # R515-LST-CLAUSES-19
        oEdtk::Field->new( 'R515-ACCBACASR2-19',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-19',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-19',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-19',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-19',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-19',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-19',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-19', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-19', 50 ),

        # R515-TEXTE-CLAUSE-19
        oEdtk::Field->new( 'R515-TEXCL1-19', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-19', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-19', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-19', 250 ),

        # R515-LST-CLAUSES-20
        oEdtk::Field->new( 'R515-ACCBACASR2-20',    2 ),
        oEdtk::Field->new( 'R515-ACH4CODGAR-20',    10 ),
        oEdtk::Field->new( 'R515-ACH4CODPRT-20',    10 ),
        oEdtk::Field->new( 'R515-ACCLNUMCAU-20',    5 ),
        oEdtk::Field->new( 'R515-ACCBDDEPRD-20',    8 ),
        oEdtk::Field->new( 'R515-ACCBDFEPRD-20',    8 ),
        oEdtk::Field->new( 'R515-ACCBTYPTXT-20',    1 ),
        oEdtk::Field->new( 'R515-LC-ACCBTYPTXT-20', 15 ),
        oEdtk::Field->new( 'R515-LL-ACCBTYPTXT-20', 50 ),

        # R515-TEXTE-CLAUSE-20
        oEdtk::Field->new( 'R515-TEXCL1-20', 250 ),
        oEdtk::Field->new( 'R515-TEXCL2-20', 250 ),
        oEdtk::Field->new( 'R515-TEXCL3-20', 250 ),
        oEdtk::Field->new( 'R515-TEXCL4-20', 250 ),
    );
}

1;
