package oEUser::Descriptor::R645;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R645-LGNDAT
        oEdtk::Field->new( 'R645-PEPENUMPER',    8 ),
        oEdtk::Field->new( 'R645-PEFPTYPLIE',    1 ),
        oEdtk::Field->new( 'R645-PEZZDDE',       8 ),
        oEdtk::Field->new( 'R645-PEZZDFE',       8 ),
        oEdtk::Field->new( 'R645-PEPPCODCSP',    4 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODCSP', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODCSP', 15 ),
        oEdtk::Field->new( 'R645-PEPPCODPRF',    5 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODPRF', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODPRF', 15 ),
        oEdtk::Field->new( 'R645-PEPPDDEACT',    8 ),
        oEdtk::Field->new( 'R645-PEPPDFEACT',    8 ),
        oEdtk::Field->new( 'R645-PEPPCODSEC',    5 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODSEC', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODSEC', 15 ),
        oEdtk::SignedField->new( 'R645-PEPPREVPER', 12, 2 ),
        oEdtk::Field->new( 'R645-PEPPCODFIL',    2 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODFIL', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODFIL', 15 ),
        oEdtk::Field->new( 'R645-PEPPCODCOL',    2 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODCOL', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODCOL', 15 ),
        oEdtk::Field->new( 'R645-PEPPCODPSU',    2 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODPSU', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODPSU', 15 ),
        oEdtk::SignedField->new( 'R645-PEPPMAPRAN', 12, 2 ),
        oEdtk::Field->new( 'R645-PEPPCODCSO',    12 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODCSO', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODCSO', 15 ),
        oEdtk::Field->new( 'R645-PEPPLIBMAT',    15 ),
        oEdtk::Field->new( 'R645-PEPPCODUSI',    10 ),
        oEdtk::Field->new( 'R645-PEPPCODATE',    10 ),
        oEdtk::Field->new( 'R645-PEPPCODFCT',    5 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODFCT', 50 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODFCT', 15 ),
        oEdtk::Field->new( 'R645-PEPPCODTRA',    2 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODTRA', 15 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODTRA', 50 ),
        oEdtk::Field->new( 'R645-PEPPCODEXP',    2 ),
        oEdtk::Field->new( 'R645-LC-PEPPCODEXP', 15 ),
        oEdtk::Field->new( 'R645-LL-PEPPCODEXP', 50 ),
        oEdtk::Field->new( 'R645-PEPPETBPCM',    20 ),
        oEdtk::Field->new( 'R645-PEPPCOEGRD',    5 ),
        oEdtk::Field->new( 'R645-PEPPTYPCNG',    2 ),
        oEdtk::Field->new( 'R645-LC-PEPPTYPCNG', 15 ),
        oEdtk::Field->new( 'R645-LL-PEPPTYPCNG', 50 ),
        oEdtk::Field->new( 'R645-PEPPTYPCTT',    2 ),
        oEdtk::Field->new( 'R645-LC-PEPPTYPCTT', 15 ),
        oEdtk::Field->new( 'R645-LL-PEPPTYPCTT', 50 ),
        oEdtk::Field->new( 'R645-ACOPNUMCIN',    15 ),
        oEdtk::Field->new( 'R645-ACOPNUMOPE',    5 ),
    );
}

1;
