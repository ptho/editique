package oEUser::Descriptor::OH;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(
        oEdtk::Field->new( 'OH-EMAIL-DOC', 50 ),
        oEdtk::Field->new( 'OH-DEMAT-DOC', 50 ),
    );
}

1;
