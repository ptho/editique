package oEUser::Descriptor::R305;

use strict;
use warnings;

use oEdtk::Record;
use oEdtk::AddrField;
use oEdtk::SignedField;
use oEdtk::Field;

sub get {
    return oEdtk::Record->new(

        # R305-LGNDAT
        oEdtk::Field->new( 'R305-ACOFCODERA', 9 ),
        oEdtk::Field->new( 'R305-ACOFCODOFF', 10 ),
        oEdtk::Field->new( 'R305-ACOFDDEPRD', 8 ),
        oEdtk::Field->new( 'R305-ACOFDFEPRD', 8 ),
        oEdtk::Field->new( 'R305-ODRSCODRDI', 9 ),
        oEdtk::Field->new( 'R305-ACREDDEPRD', 8 ),
        oEdtk::Field->new( 'R305-ACREDFEPRD', 8 ),
        oEdtk::Field->new( 'R305-ODAGCODAGC', 9 ),
        oEdtk::Field->new( 'R305-ACAGDDEPRD', 8 ),
        oEdtk::Field->new( 'R305-ACAGDFEPRD', 8 ),
        oEdtk::Field->new( 'R305-ODPUCODPUC', 9 ),
    );
}

1;
