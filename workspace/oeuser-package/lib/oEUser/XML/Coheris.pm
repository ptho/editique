package oEUser::XML::Coheris;
use utf8;

use strict;
use warnings;

use oEdtk::XPath;

use Exporter;
our $VERSION   = 0.02;
our @ISA       = qw(Exporter);
our @EXPORT_OK = qw(coheris_cod_extract coheris_dod_extract);

# Extract a table of data following the Coheris conventions.
sub table_extract {
    my ( $xml, $node ) = @_;

    my $data = [];

    # Fetch the column headers.
    my @cols = $xml->findnodes( './row[@type="header"]/data[@value]', $node );
    my $row = [];
    foreach my $cnode (@cols) {
        my $val = $xml->findTextValue( './@value', $cnode );
        push( @$row, $val );
    }
    push( @$data, $row );

    # Now fetch the row values.
    my @rows = $xml->findnodes( './row[@type="line"]', $node );
    foreach my $rnode (@rows) {
        $row = [];
        my @vals = $xml->findnodes( './data[@value]', $rnode );
        foreach my $vnode (@vals) {
            my $val = $xml->findTextValue( './@value', $vnode );
            push( @$row, $val );
        }
        push( @$data, $row );
    }
    return $data;
}

# The common parts within the <CLIENTS> tag.
sub clients_extract {
    my ( $xml, $cnode ) = @_;
    my $data = {};
    $data->{'CLIREF'} =
      $xml->findTextValue( './CLIENTS.REFERENCE/@value', $cnode );
    $data->{'CLINOM'} = $xml->findTextValue( './CLIENTS.NOM/@value', $cnode );
    $data->{'CLIPRENOM'} =
      $xml->findTextValue( './CLIENTS.PRENOM/@value', $cnode );
    $data->{'CLICOLLEMP'} =
      $xml->findTextValue( './CLIENTS.CL_RUB2/@value', $cnode );
    $data->{'CLIMAT'} =
      $xml->findTextValue( './CLIENTS.CL_RUB3/@value', $cnode );
    $data->{'NUMSSADH'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP28/@value', $cnode );
    $data->{'DATNAISSADHPOURIE'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP14/@value', $cnode );
    $data->{'ADRESSLIGNEIoI'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP31/@value', $cnode );
    $data->{'ADRESSLIGNEIoII'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP26/@value', $cnode );
    $data->{'ADRESSLIGNEIoIII'} =
      $xml->findTextValue( './TABLEMU.TMU_LIB/@value', $cnode );
    $data->{'ADRESSLIGNEIoIV'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP27/@value', $cnode );
    $data->{'ADRESSLIGNEIIoI'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP48/@value', $cnode );
    $data->{'ADRESSLIGNEIIoII'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP34/@value', $cnode );
    $data->{'ADRESSLIGNEIIoIII'} =
      $xml->findTextValue( './CLIENTS.CL_INFO_COMP33/@value', $cnode );
    $data->{'CPADH'} =
      $xml->findTextValue( './CLIENTS.CODE_POSTAL/@value', $cnode );
    $data->{'VILLEADH'} =
      $xml->findTextValue( './CLIENTS.VILLE/@value', $cnode );
    return $data;
}

# The common parts within the <AFFAIRES> tag.
sub affaires_extract {
    my ( $xml, $anode, $flag ) = @_;   #flag set à distinguer le cas COD et DOD
    my $data = {};
    $data->{'AFFCODE'} =
      $xml->findTextValue( './AFFAIRES.AF_CODE/@value', $anode );
    $data->{'TYPCMD'} =
      $xml->findTextValue( './AFFAIRES.T6_CODE/@value', $anode );
    $data->{'TYPCONTRAT'} =
      $xml->findTextValue( './AFFAIRES.T8_CODE/@value', $anode );
    $data->{'DATEARRET'} =
      $xml->findTextValue( './AFFAIRES.AF_INFO_COMP11/@value', $anode );
    $data->{'DATEREPRISE'} =
      $xml->findTextValue( './AFFAIRES.AF_INFO_COMP12/@value', $anode );
    $data->{'REPRISE'} =
      $xml->findTextValue( './AFFAIRES.T14_CODE/@value', $anode );
    $data->{'TYPCM'} =
      $xml->findTextValue( './AFFAIRES.T21_CODE/@value', $anode );
    $data->{'AFFREF'} =
      $xml->findTextValue( './AFFAIRES.REFERENCE/@value', $anode );

    if ($flag) {    # On ne va chercher ces infos que pour le cas des dod
        $data->{'ACCIDENT'} =
          $xml->findTextValue( './AFFAIRES.T9_CODE/@value', $anode );
        $data->{'DATEDEMI'} =
          $xml->findTextValue( './AFFAIRES.AF_INFO_COMP13/@value', $anode );
    }
    return $data;
}

# The common parts within the <CLIENTS_SOUSCRIPTEUR> tag. Called only if collectif contract ($data->{'TYPCONTRAT'}=="Collectif")
sub clients_souscript_extract {
    my ( $xml, $csnode ) = @_;
    my $data = {};
    $data->{'COLLSOUS'} =
      $xml->findTextValue( './CLIENTS.CLIENT/@value', $csnode );
    return $data;
}

# The common parts within the <TABLEU_CTPP> tag.
sub adr_extract {
    my ( $xml, $adrnode ) = @_;
    my $data = {};
    $data->{'ADRCTPP'} =
      $xml->findTextValue( './TABLEU.TU_INFO_COMP04/@value', $adrnode );
    return $data;
}

# The common parts within the <CLIENTS_COLLECTIVITE_EMETTRICE> tag.
sub emit_extract {
    my ( $xml, $emitnode ) = @_;
    my $data = {};
    $data->{'EMETTRICE'} =
      $xml->findTextValue( './CLIENTS.CL_RUB2/@value', $emitnode );
    return $data;
}

# The common parts within the <CLIENTS> tag.
sub typeVoie_extract {
    my ( $xml, $tvnode, $data ) = @_;
    $data->{'TYPEVOIE'} =
      $xml->findTextValue( './TABLEMU.TMU_LIB/@value', $tvnode );
    return $data;
}

# Extract the key/value pairs from the Coheris XML file and return them in a hash
# Extraction communes à COD et DOD
sub coheris_common_extract {
    my ( $xml, $root, $flag ) = @_;

    #extract data for informations in CLIENTS tag
    my ($cnode) = $xml->findnodes( './CLIENTS[@type="Detail"]', $root );
    my $cdata = clients_extract( $xml, $cnode );

    #extract data for the CTPP
    my ($adrnode) = $xml->findnodes( './TABLEU_CTPP[@type="Detail"]', $root );
    my $adrdata = adr_extract( $xml, $adrnode );

    #extract data for the Collectivité émettrice
    my ($emitnode) =
      $xml->findnodes( './CLIENTS_COLLECTIVITE_EMETTRICE[@type="Detail"]',
        $root );
    my $emitdata = emit_extract( $xml, $emitnode );

    #extract data for the "Détails des périodes à indemniser"
    my ($tnode) = $xml->findnodes( './AF_LINK_LIST[@type="List"]', $root );
    my $ind = table_extract( $xml, $tnode );

    #extract data for the Observations block and the date of creation
    my ($obsnode) =
      $xml->findnodes( './HISTORIQUE_DATA_DIVERSE[@type="List"]', $root );
    my $obs_et_date = table_extract( $xml, $obsnode );

    #extract data for informations in AFFAIRES tag
    my ($anode) = $xml->findnodes( './AFFAIRES[@type="Detail"]', $root );
    my $adata = affaires_extract( $xml, $anode, $flag );

    my $data = { %$cdata, %$adrdata, %$emitdata, %$adata };
    $data->{'PERINDEMN'} = $ind;
    $data->{'OBSETDATE'} = $obs_et_date;

#extract data for the "collectivité souscripteur" only in the case of collectif contract
    if ( $adata->{'TYPCONTRAT'} =~ /Collectif/i ) {
        my ($csnode) =
          $xml->findnodes( './CLIENTS_SOUSCRIPTEUR[@type="Detail"]', $root );
        my $csdata = clients_souscript_extract( $xml, $csnode );
        $data = { %$data, %$csdata };
    }

    return $data;
}

# COD application (complément de dossier).
sub coheris_cod_extract {
    my ($fh) = @_;
    my $xml = oEdtk::XPath->new( ioref => $fh );
    my ($root) = $xml->findnodes('/ApplView');

    my $data = coheris_common_extract( $xml, $root, 0 );
    return $data;
}

# DOD application (demande d'ouverture de droits).
sub coheris_dod_extract {
    my ($fh) = @_;
    my $xml = oEdtk::XPath->new( ioref => $fh );
    my ($root) = $xml->findnodes('/ApplView');

    my $data = coheris_common_extract( $xml, $root, 1 );

    my ($node) = $xml->findnodes( './AF_COST[@type="List"]', $root );
    my $arr = table_extract( $xml, $node );
    $data->{'PERARRETS'} = $arr;

    my ($tvnode) = $xml->findnodes( './TypeVoie[@type="Detail"]', $root );
    my $data = typeVoie_extract( $xml, $tvnode, $data );

    return $data;
}

1;
