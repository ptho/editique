package oEUser::XML::Generic;

use strict;
use warnings;

use XML::Simple;

use Exporter;
our $VERSION = 0.01;
our @ISA     = qw(Exporter);
our @EXPORT  = qw(xml_open);

# Extract the key/value pairs from the Cameleon XML file and
# return them in a hash.
sub xml_open {
    my ( $fh, $options ) = @_;

    binmode($fh);    # Make sure to drop any PerlIO layer.
    my $xml  = new XML::Simple();
    my $data = $xml->XMLin(
        $fh,
        suppressempty => +'',
        ParserOpts    => [ ProtocolEncoding => 'ISO-8859-1' ]
    );

    return $data;
}

1;
