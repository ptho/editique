package oEUser::XML::Cameleon;
use utf8;

use strict;
use warnings;

use Encode qw(decode encode);
use Fcntl qw(:seek);
use XML::LibXML;

# This is so we can refer to Unicode characters by their name.
use charnames ':full';

use Exporter;
our $VERSION   = 0.02;
our @ISA       = qw(Exporter);
our @EXPORT_OK = qw(cameleon_open cameleon_extract);

# Wrapper for Encode::encode that warns about characters that cannot be
# converted, but doesn't insert any replacement text.
sub my_encode {
    my ( $enc, $data ) = @_;
    my $check = sub {
        warn sprintf( "%s <U+%04X> does not map to $enc", $data, shift );
        return '';
    };
    decode( $enc, encode( $enc, $data, $check ), $check );
}

# Extract the key/value pairs from the Cameleon XML file and
# return them in a hash.
sub cameleon_open {
    my ( $fh, $path ) = @_;

    binmode($fh);    # Make sure to drop any PerlIO layer.
    my $xml = XML::LibXML->load_xml( IO => $fh );
    my $xpc = XML::LibXML::XPathContext->new($xml);

    return $xpc;
}

sub cameleon_extract {
    my ( $xpc, $path ) = @_;
    my $cond = "[\@cpe=\"$path\"]";
    my @nodes =
      $xpc->findnodes( '//conf:GenerativeProcess//'
          . 'conf:ItemSalesBreakdownLine'
          . $cond
          . '/conf:Sig//conf:BusinessProperties' );

    my $data = {};
    foreach my $line (@nodes) {
        my ($knode) = $xpc->findnodes(
'./conf:SingleBusinessProperty[@name="bpTypeDonnee"]/conf:Value/conf:textValue',
            $line
        );
        my ($vnode) = $xpc->findnodes(
'./conf:SingleBusinessProperty[@name="bpValeur"]/conf:Value/conf:longTextValue',
            $line
        );
        if (   !defined($vnode)
            || !defined($knode)
            || $vnode->nodeType != XML_ELEMENT_NODE
            || $knode->nodeType != XML_ELEMENT_NODE )
        {
#			return undef;		## Depuis Caméléon V9, les valeurs vides ne sont pas mises dans le flux, il peut donc y avoir des clefs sans valeurs
            next;
        }
        my $key = $knode->textContent();
        my $val = $vnode->textContent();

        # We substitute the unicode character "RIGHT SINGLE
        # QUOTATION MARK" by the common apostrophe character,
        # because there is no other corresponding character
        # when encoding in ISO-8859-15.
        # http://ascii-table.com/unicode-index-e.php
        $val =~ s/\N{RIGHT SINGLE QUOTATION MARK}/'/g;
        $val =~ s/\N{HORIZONTAL ELLIPSIS}/.../g;

        # $val = my_encode('latin9', $val);
        # $data->{$key} = $val;
        $data->{$key} = my_encode( 'iso-8859-15', $val );    # alias = latin9
    }
    return $data;
}

1;
