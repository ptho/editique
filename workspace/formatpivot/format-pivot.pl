#!/opt/editique/perl/bin/perl

use strict;
use warnings;
use Readonly;
use Data::Dumper;
use Config::Simple;
use File::Basename qw( basename dirname );
use File::Copy qw(move);

use MIME::QuotedPrint qw( decode_qp );
use Encode qw( decode encode );

Readonly my $INIFILE => 'fp.ini';

our ( $fpconfig, $sep, $typedoc, $encoding ); # Variables read by called modules

sub main {
    my ( $opt, $filename, $nopdftest ) = @_;

    return usage() if !defined $filename;
    die "file $filename does not exist" unless -e $filename;
    die "file $filename not readable"   unless -r $filename;
    die "file $filename is a directory" if -d $filename;

    my ($path) = $filename =~ /(^.+\/)[^\/]+\.\w{3}$/;

    $encoding = `file -bi $filename`;
    ($encoding) = $encoding =~ /charset=(.*)$/;
    ( $fpconfig, $sep, $typedoc ) = getApiConf( $opt, $INIFILE );

    my $firstline;
    my $headersize;
    open my $in, '< :encoding(' . $encoding . ')', $filename;
    my $out_file = dirname($filename) . "/treated_" . basename($filename);
    open my $out, '>', $out_file or die "Can't write new file '$out_file': $!";
    my $cpt = 0;
    my @heads;
    while ( my $row = <$in> ) {
        chomp $row;
        if ( $cpt == 0 ) {
            @heads = split /$sep->{value}/, $row;
            pop @heads;
            print Dumper(@heads);
            ( $row, $headersize ) = newHeaders( $row, $sep, $opt );
        }
        else {
            my @chunks;
            @chunks = split /$sep->{value}/, $row;
            $chunks[$headersize] = undef;
            if ( $opt == 0 ) {
                my $tdoc = $chunks[1];
                $tdoc =~ s/ /-/g;
                $row =~ s/$chunks[1]/$tdoc/g;
                if ( exists $typedoc->{$tdoc} ) {
                    $tdoc = $typedoc->{$tdoc};
                }
                $row =~ s/$sep->{value}
	/$sep->{value}$tdoc
	/;
                $chunks[$headersize] = $tdoc;
            }

# Dans Docubase les - ont été remplacés par des espaces -> Remplace les espaces dans l'idDoc par des -
            $chunks[3] =~ s/ /-/g;
            my %hashedline;
            my $i = 0;
            @hashedline{@heads} = @chunks;
            my $idkey =
              $opt == 6
              ? "Identifiant unique"
              : "Identifiant unique de document";
            $row =~
s/^\w\:.+\\([^\\|^$sep->{value}]+\.PDF)$sep->{value}/$hashedline{$idkey}-$1$sep->{value}/;
            $chunks[0] = $hashedline{$idkey} . "-" . $1;

            #`touch $1`;
            if ( !defined($nopdftest) ) {
                move( $path . "/" . $1,
                    $path . "/" . $hashedline{$idkey} . "-" . $1 )
                  or die "move failed: $! Unable to find file $path/$1";
            }
            $row = join( ";", @chunks );
        }
        print $out $row . "\n";
        $cpt++;
    }
    close $in;
    close $out;
    print "treatment succeed: output file is $out_file\n";
    printf "Fonctionnement de test, pas de pdf créé" if defined($nopdftest);
    return 0;
}

sub newHeaders {
    my ( $firstline, $sep, $opt ) = @_;
    $firstline = decode( $encoding, $firstline );
    $firstline = encode( 'utf-8', $firstline );
    $firstline =~ s/$sep->{value}$//;
    $firstline =~ s/\r\n\z//;
    $firstline =~ s/\r//g;
    $firstline =~ s/
//;

    #print $firstline."\n\n";
    my @headers = split /$sep->{value}/, $firstline;
    my $newheader;
    foreach my $header (@headers) {

        #print "[".$header."] : ".$fpconfig->{$header}."\n";
        next if length($header) == 0;
        die "unexpected header [$header] in the index"
          unless $fpconfig->{$header};
        $newheader .= $fpconfig->{$header} . $sep->{value};
    }
    $newheader .= "typeDoc" if $opt == 0;
    my $headersize = scalar(@headers);
    return ( $newheader, $headersize );

    #$newheader=~s/$sep->{value}$//;
}

sub usage {
    die << "EOUSAGE"
----------------------------- Format Pivot error -------------------------------
parameter are expected
usage: $0 <app> <csv data file> 
opt in [0,6]
$0 6 <csv data file> # 6 for PREVAIR
$0 0 <csv data file> # 0 for IHM
--------------------------------------------------------------------------------
EOUSAGE
}

sub usage_opt {
    die << "EOUSAGE"
----------------------------- Option error -------------------------------
option 0 or 6 is expected
opt in [0,6]
--------------------------------------------------------------------------------
EOUSAGE
}

sub getApiConf {
    my ( $opt, $inifile ) = @_;
    my $configPath = './conf/' . $inifile;
    my $config     = new Config::Simple($configPath);
    die "Impossible d'ouvrir le fichier $configPath" unless defined($config);
    my $databases = $config->param( -block => 'DATABASES' );

    return usage_opt($opt) if !defined $databases->{$opt};
    my $fpconfig = $config->param( -block => $opt );
    my $sep      = $config->param( -block => "SEPARATOR" );
    my $typedoc  = $config->param( -block => "TYPEDOC" );
    return ( $fpconfig, $sep, $typedoc );
}

exit main(@ARGV);
