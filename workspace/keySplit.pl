#!/usr/bin/perl
use locale;
use oEdtk::Main;
use File::Basename;
$PROG = basename( $0, "" );
$NOK = -1;

sub usage() {
    print STDOUT << "EOF";

	Usage : $PROG -fi=fichier [-lignes=n] [-cle="clef de recherche"] [-head=nb_lignes_entete]

	version v0.14.1 22/11/2005 15:40:28

EOF
    exit 0;
}

sub main() {
    my %hParm = ();
    oEdtk::Main::myGetOptions( \%hParm, "h", "fi=s", "lignes:i", "cle:s",
        "head:s" );
    if ( $hParm{h} ) {
        &usage();
        exit 1;
    }
    unless ( $hParm{fi} ) {
        &usage();
        exit $NOK;
    }

    #if ($#ARGV !=0 && $#ARGV >2) { &usage(); }

    my $cle      = "";
    my $flagCle  = 0;
    my $header   = 10;
    my $compteur = 0;
    my $nbLignes = 100;
    my @tLignes;    # tableau  de ligne empile avec push purge avec shift

    $nbLignes = $hParm{lignes}   if ( $hParm{lignes} );
    $cle      = $hParm{cle}      if ( $hParm{cle} );
    $header   = $hParm{head} + 1 if ( $hParm{head} );
    $flagCle  = 1                if ( $cle eq "" );
    my $fo = "$hParm{fi}.split.$cle";

    if ( $cle eq "" ) {
        print
"\n Split des $nbLignes premieres lignes\n du fichier $hParm{fi}\n vers $fo\n";
    }
    else {
        print
"\n Split autour de la cle '$cle' de $nbLignes lignes\n du fichier $hParm{fi}\n vers $fo\n";
        print "\n ", $header - 1,
          " lignes conservees avant la ligne contenant la cle '$cle' et ",
          $nbLignes - $header, " lignes apres cette cle.\n"
          if $cle;
    }

    open( IN, "<$hParm{fi}" )
      or die "echec a l'ouverture de $hParm{fi}, code retour : $!\n";
    while ( $ligne = <IN> ) {
        push @tLigne, $ligne;
        $compteur++;
        if ( $flagCle eq 0 ) {
            while ( $#tLigne + 1 > $header ) {
                shift @tLigne;
            }  # on purge ce qui est hors header pour consommer moins de memoire
            if ( index( $ligne, $cle ) ne -1 ) {    # des que l'on trouve la cle

#			if ($ligne =~/\${cle}/s){                                           # des que l'on trouve la cle
                $flagCle = 1;
                print
"\n cle '$cle' trouvee ligne $compteur\n- extrait -> $ligne\n";
            }
        }
        elsif ( $#tLigne + 1 >= $nbLignes && $flagCle eq 1 ) {
            last;
        }
    }

    if ( $flagCle eq 0 ) {
        print
"\n La cle '$cle' n'a pas ete trouvee.\n Fin de traitement sans sortie.\n";
        exit $NOK;
    }
    else {
        open( OUT, ">$fo" )
          or die "echec a l'ouverture de $fo, code retour : $!\n";
        while (@tLigne) {
            print OUT shift(@tLigne);
        }
        close OUT or die "echec a la fermeture de $fo, code retour $!\n";
    }

    close IN or die "echec a la fermeture de $hParm{fi}, code retour $!\n";
    print "\n - FIN - \n";
    1;
}

&main;
1;
