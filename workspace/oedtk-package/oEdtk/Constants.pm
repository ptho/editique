#! /usr/bin/perl
package oEdtk::Constants;
use base qw( Exporter );
use warnings;
use strict;
use Carp qw( carp croak );
use Readonly;
use Text::CSV_XS;
use Data::Dumper qw( Dumper );

our @EXPORT_OK = qw( check_typdoc );

# Source:
# \\srvetudes\Sources\P%%C3%%B4leRestitutions\Editique\admin_MNT\Specs\Liste des types documents pour ECM v1.1.xlsx
# (mind the various tabs; use Documents tab)
# Copied on the Redmine Editique wiki (not necessarily up-to-date): [[Typologie des documents]]
my $TYPDOC = {};

sub trim_scalar {
    my ($s) = @_;
    $s =~ s{^\s+}{}sox;
    $s =~ s{\s+$}{}sox;
    $s =~ s/\s+/\N{SPACE}/sox;
    return $s;
}

sub trim_array {
    my (@l) = @_;
    return map { trim_scalar($_) } @l;
}

sub populate {
    my ( $h, $nH, $l ) = @_;
    my $n = scalar @{$l};
    croak "Bad number of cells $n (expected $nH): " . Dumper($l) if $n != $nH;
    my ( $short, $code, $long ) = trim_array( @{$l} );
    return
      unless $code =~
      m{\S}sx;    # there are empty lines in the reference Excel stylesheet...
    if ( exists $TYPDOC->{$code} ) {
        carp "Code '$code' multiply defined";    # XXX should be croak
    }
    else {
        $TYPDOC->{$code} = [ $short, $long ];
    }
    return;
}

sub init {
    my $csv = Text::CSV_XS->new( { sep_char => ';', binary => 1 } );
    my @headers;
    my $nHeaders = 0;
    while (<DATA>) {
        my $line = $_;
        chomp $line;
        if ( $csv->parse($line) ) {
            my @l = $csv->fields();
            if ($nHeaders) {
                populate( \@headers, $nHeaders, \@l );
            }
            else {
                @headers  = @l;
                $nHeaders = scalar @headers;

                # check they are as expected
            }
        }
        else {
            croak join( "\n",
                "Cannot parse '$line':",
                $csv->error_diag(), $csv->error_input() );
        }
    }
    binmode STDOUT, ':encoding(UTF-8)';
    carp 'KNOWN TYPDOCs: ' . Dumper($TYPDOC);
}

sub check_typdoc {
    my ($code) = @_;
    init() unless scalar keys %{$TYPDOC};
    croak "Unknown code '$code'" unless exists $TYPDOC->{$code};
    my ( $short, $long ) = @{ $TYPDOC->{$code} };
    carp "Recognized code '$code' for document type '$short' ($long)";
    return $code;
}

1;

__DATA__
Libellé court;Code;Libellé long;ECM Destination;Durée de rétention (jours);;
Type de document non reconnu;0000;Type de document non reconnu;;;;
Autorisation remb de presta;AARP;Autorisation écrite de l’agent pour remboursement de prestations;Santé - Prévoyance;;;
Arrêté de radiation des cadres;ACAD;Arrêté de radiation des cadres;Santé - Prévoyance;;;
Arrêté changement statut;ACHS;Arrêté municipal notifiant le changement statutaire;Santé - Prévoyance;;;Courriers entrants
Attest méd cause du décès;ACNA;Attestation médicale précisant la cause naturelle ou non du décès;Service Médical;;;Courriers sortants
Avis commission de réforme;ACOR;Avis de la commission de réforme;Santé - Prévoyance;;;
Avis d'appel de cot;ACOT;Avis d'appel de cotisations;Santé - Prévoyance;;ACI-AC014;EPS-AC016
Justif attribution AEEH;AEEH;Justificatif de l'attribution de l'AEEH;Santé - Prévoyance;;;
Attest hébergement méd ;AHEB;Attestation d'hébergement en unité de long séjour ou dans une section de cure médicale;Santé - Prévoyance;;;
Avis d’imposition;AIMP;Avis d'imposition de l'assuré;Santé - Prévoyance;;;
Attest méd incap définitive;AINC;Attestation médicale précisant l'incapacité définitive de se livrer à la moindre activité pouvant procurer gain ou profit;Santé - Prévoyance;;;
Attest indice majoré ;AIND;Attestation de la collectivité indiquant l'indice majoré;Santé - Prévoyance;;;
Attestation de labellisation;ALAB;Attestation de labellisation;Santé - Prévoyance;;;
Arrêté de licenciement pour inaptitude;ALII;Arrêté de licenciement pour inaptitude;Santé - Prévoyance;;;
Attest mairie service social;AMAI;Attestation de la mairie ou d'un service social ou d'une association;Santé - Prévoyance;;;
Attest méd perte d’autonomie;APAU;Attestation médicale de perte d'autonomie;Service Médical;;;
Attest de présence;APRE;Attestation de présence;Santé - Prévoyance;;;
Annexe aux primes;APRI;Annexe aux primes;Santé - Prévoyance;;;
Avis recouvrement URSSAF;AREC;Avis de recouvrement URSSAF;Comptabilité;;;
A R outil chgt ;AROC;A R outil chgt ;Santé - Prévoyance;;;
Arrêté;ARRE;Arrêté;Santé - Prévoyance;;;
Détail salaires annuels;ASAL;Attestation de la collectivité indiquant le détail des salaires annuels bruts;Santé - Prévoyance;;;
Attestation CPAM;ATCP;Attestation CPAM;Santé - Prévoyance;;;
Attest employeur;ATEM;Attestation employeur;Santé - Prévoyance;;;
Arrêté ATIACL;ATIA;Arrêté d'attribution de l'ATIACL;Santé - Prévoyance;;;
Attest tierce personne;ATIE;Attestation de recours obligatoire à une tierce personne pour effectuer les actes ordinaires de la vie;Santé - Prévoyance;;;
Attest versements à tort;ATOR;Attestation de la collectivité précisant si des règlements ont été versés à tort;Santé - Prévoyance;;;
Courrier dem attestation;ATTS;Courrier de demande d'attestation;Santé - Prévoyance;;;
Autorisation précompte CNRACL;AUPS;Autorisation précompte CNRACL;Santé - Prévoyance;;;
Avis arrêt municipal;AVAM;Avis de l'arrêt municipal;Santé - Prévoyance;;;
Avis d'AT;AVAT;Avis d'arrêt de travail;Santé - Prévoyance;;;
Avis commission de réforme;AVCR;Avis  de la Commission de réforme;Santé - Prévoyance;;;
Avenant;AVEN;Avenant;Santé - Prévoyance;;;
Avis comité méd;AVIM;Avis du comité médical;Santé - Prévoyance;;;
Avis Tiers détenteur;AVTD;Avis Tiers détenteur;Santé - Prévoyance;;BULADH;
Bulletin Adhésion ;BADH;Bulletin d'adhésion;Santé - Prévoyance;;;
Bulletin Décés;BDEC;Bulletin de décès;Santé - Prévoyance;;;
Bulletin d'hospitalisation;BHOS;Bulletin d'hospitalisation;Santé - Prévoyance;;;
Bull paiement CNRACL;BPAI;Bulletin de paiement CNRACL;Santé - Prévoyance;;;
Borderau de paiement aux tiers;BPAT;Borderau de paiement aux tiers;Santé - Prévoyance;;;
Bull de salaire;BSAL;Bulletin de salaire;Santé - Prévoyance;;;
Bull de situation;BSIT;Bulletin de situation;Santé - Prévoyance;;;
Courrier avenant contrat;CACO;Courrier avenant contrat;Bureautique;;;
Courrier Adhérent;CADH;Courrier d’adhérent;Santé - Prévoyance;;;
Courrier adhésion au contrat;CADH;Courrier adhésion au contrat;Bureautique;;;
Certif d’admission;CADM;Certificat d'admission;Santé - Prévoyance;;;
Courrier de désignation des bénéficiaires décé;CBDC;Courrier de désignation des bénéficiaires décé;Bureautique;;;
Coupon Modif Base Salaire ;CBSA;Coupon Modif Base Salaire ;Santé - Prévoyance;;;
Courrier de déconnection Noemi;CDNO;Courrier de déconnection Noemi;Bureautique;;;
Courrier échelon dette;CECH;Courrier d'échelonnement de dette;Comptabilité;;;
Convention MNT équilibre;CEQU;Convention MNT équilibre;Santé - Prévoyance;;;
Chèque emploi-service;CESU;Chèques emplois-services (Copie);Santé - Prévoyance;;;
Convention de gestion des prestations indues;CGPI;Convention de gestion des prestations indues;Santé - Prévoyance;;;
Contrat d’hébergement;CHEB;Contrat d'hébergement;Santé - Prévoyance;;;
Courriers indus;CIND;Courriers indus;Santé - Prévoyance;;;
Certif inscription;CINS;Certificat d'inscription;Santé - Prévoyance;;;
Calcul des jours d'arrêt;CJAE;Calcul des jours d'arrêt par l'employeur;Santé - Prévoyance;;;
CNI;CNID;Carte Nationale d'Identité;Santé - Prévoyance;;;
Convention participation;CPAP;Convention de participation;Santé - Prévoyance;;;
Convention de partenariat;CPAT;Convention de partenariat;Santé - Prévoyance;;;
Formulaire Dépendance;CPAU;« Contrat 1489T de Perte d'Autonomie - Dépendance »;Santé - Prévoyance;;;
CP contrat collectif;CPCC;Conditions particulières au contrat collectif;Santé - Prévoyance;;;
Courrier de prise en charge hospitalière;CPEC;Courrier de prise en charge hospitalière;Santé - Prévoyance;;;
Courrier de précontentieux;CPRE;Courrier de précontentieux;Juridique;;;
Cert. Radia. pr OA MP >35 ans;CRAD;Cert. Radia. pr OA MP >35 ans;Santé - Prévoyance;;;
Confirm résil contrat coll;CRCC;Confirmation de la résiliation contrat collectif;Santé - Prévoyance;;;
Confirmation de la résiliation contrat individuel;CRCI;Confirmation de la résiliation contrat individuel;Santé - Prévoyance;;;
Courrier résiliation décé;CRDC;Courrier résiliation décé;Santé - Prévoyance;;;
Courrier de rejet de prélèvement bancaire;CRPR;Courrier de rejet de prélèvement bancaire;Santé - Prévoyance;;;
Courrier adhérent Résil;CRSI;Courrier adhérent Résil;Santé - Prévoyance;;;
Contrat Rente de survie;CRSU;Contrat Rente de survie;Santé - Prévoyance;;;
Courrier de rejet de virement bancaire;CRVI;Courrier de rejet de virement bancaire;Santé - Prévoyance;;;
Certificat de séjour;CSEJ;Certificat de séjour;Santé - Prévoyance;;;
Carte tiers payant;CTIP;Carte tiers payant;Santé - Prévoyance;;;
Contrat de travail;CTRA;Contrat de travail;Santé - Prévoyance;;;
Certificat de vie;CVIE;Certificat de vie de l'assuré;Santé - Prévoyance;;;
Désignation bénéficiaire;DDEC;Désignation bénéficiaire Décès;Santé - Prévoyance;;;
Devis;DEVI;Devis;Santé - Prévoyance;;;
Décomptes IJ SS;DJSS;Décomptes des indemnités journalières Sécurité Sociale;Santé - Prévoyance;;;
Décompte liquid NBI CNRACL;DNBI;Décompte de liquidation de la Nouvelle Bonification Indiciaire (NBI) de la CNRACL;Santé - Prévoyance;;;
Complt indemnisation IJ;DODC;« Demande de prestations Indemnités journalières - Complément de dossier »;Santé - Prévoyance;;;
Ouverture des droits IJ;DODD;« Demande de prestations Indemnités journalières – Ouverture des droits »;Santé - Prévoyance;;;
Décompte pension CNRACL;DPEN;Décompte définitif de pension CNRACL;Santé - Prévoyance;;;
Décompte de pension d'invalidité;DPIN;Décompte de pension d'invalidité;Santé - Prévoyance;;;
Courrier résil contrat coll;DRCC;Courrier de demande de résiliation contrat collectif;Santé - Prévoyance;;;
Courrier de demande de résiliation contrat individuel;DRCI;Courrier de demande de résiliation contrat individuel;Santé - Prévoyance;;;
Formulaire rente inval;DRIN;« Demande de rente d'invalidité »;Santé - Prévoyance;;;
Éval autonomie AVQ;EAVQ;Évaluation d'autonomie - Actes de la Vie Quotidienne (AVQ);Santé - Prévoyance;;;
Édition remises en banque;EDRB;Édition remises en banque;Comptabilité;;;
factures de médicaments;FACM;factures de médicaments;Santé - Prévoyance;;;
Feuilles de calculs prévoyance Phoenix;FCPR;Feuilles de calculs prévoyance Phoenix;Santé - Prévoyance;;;
Factures décomptes des heures;FDOM;Factures avec décomptes des heures effectuées à domicile;Santé - Prévoyance;;;
Facture frais d’obsèques;FOBS;Facture des frais d'obsèques;Santé - Prévoyance;;;
Factures frais de séjour;FSEJ;Factures de frais de séjour;Santé - Prévoyance;;;
Formulaire + Territorial;GATS;« + TERRITORIAL GARANTIE ACCIDENTS DU TRAVAIL OU DE SERVICE - MALADIES PROFESSIONNELLES »;Santé - Prévoyance;;;
Formulaire capital décès;GDCD;« Garantie décès invalidité totale et permanente – demande de capital décès »;Santé - Prévoyance;;;
Formulaire capital inval;GDCI;« Garantie décès invalidité totale et permanente – demande de capital invalidité »;Santé - Prévoyance;;;
Demande frais d’obsèques;GOBS;« garantie frais d'obsèques – demande de capital »;Santé - Prévoyance;;;
Justif paiement alloc;JALL;Justificatif du paiement de l'allocation;Santé - Prévoyance;;;
Justif cause accidentelle;JCAC;Justificatif de la cause accidentelle;Santé - Prévoyance;;;
Justif arrêt activité pro;JCES;Justificatif de la cessation totale de l'activité professionnelle;Santé - Prévoyance;;;
Justif. adhésion mut. Oblig.;JMUT;Justif. adhésion mut. Oblig.;Santé - Prévoyance;;;
Justif nature accident;JNAC;Justificatif de la nature de l'accident (coupure de presse, rapport de police, témoignages);Santé - Prévoyance;;;
Justif paiement CPAM;JPAM;Justificatif du paiement par l'Assurance Maladie;Santé - Prévoyance;;;
Justif attribution PCH;JPCH;Justificatif de l'attribution de la PCH;Santé - Prévoyance;;;
Jugement tutelle;JTUT;Jugement de tutelle;Juridique;;;
Liste des adhérents non soldés;LANS;Liste des adhérents non soldés;Santé - Prévoyance;;;
Lettres chèques;LCHD;Lettres chèques;Santé - Prévoyance;;;
Liste des appels de cotisations;LDAC;Liste des adhérents non soldés;Santé - Prévoyance;;COURREC;
Acte Etat Civil;LFAM;Livret de famille;Santé - Prévoyance;;;
Liste du personnel;LIPE;Liste du personnel;Santé - Prévoyance;;;
Notif attrib rente incap;NAIP;Notification d'attribution de rente d'incapacité permanente;Santé - Prévoyance;;;
Notif montant pension inval;NAMI;Notification d'attribution de montant de pension d'invalidité;Santé - Prévoyance;;;
Notif attribution APA;NAPA;Notification de l'attribution de l'APA;Santé - Prévoyance;;;
NICP;NICP;Notice d'Information des Conditions Particulières;Santé - Prévoyance;;Mandat SEPA;
Notif CPAM incap perma;NINC;Notification par l'Assurance Maladie de l'attribution au titre de l'incapacité permanente;Santé - Prévoyance;;;
NI;NINF;Notice d'informations;Santé - Prévoyance;;;
Notif de majoration;NMAJ;Notification d'une majoration;Santé - Prévoyance;;;
Note d'honoraires de carence;NOCA;Note d'honoraires de carence;Santé - Prévoyance;;;
Note d'honoraires;NOTH;Note d'honoraires;Santé - Prévoyance;;;
Notif de pension CPAM;NPSS;Notification de pension CPAM;Santé - Prévoyance;;;
Notif attrib retraite;NRET;Notification d'attribution d'une retraite;Santé - Prévoyance;;;
PV requalifiant le congé;PCOM;Procès verbal du comité médical requalifiant le congé maladie;Service Médical;;;
Page de garde;PGPR;Page de garde;;;;
Questionnaire accident ;QACC;Questionnaire accident;Santé - Prévoyance;;;
QM;QMED;Questionnaire Médical;Service Médical;;;
Relevé de carrière;RCAR;Relevé de carrière;Santé - Prévoyance;;;
Courrier de réclamation;RECL;Courrier de réclamation;Santé - Prévoyance;;;
Relevé de prestations;RELP;Relevé de prestations;Santé - Prévoyance;;;
Courrier modif renseignement;RENS;Courrier de demande de modification ou de renseignement;Santé - Prévoyance;;;
Rapport de police;RGEN;Rapport de gendarmerie ou de police;Juridique;;;
RIB;RIBS;RIB;Comptabilité;;;
Rejet de prélèvement bancaire;RJPB;Liste des appels de cotisations;Santé - Prévoyance;;;
Rapport médical;RMED;Rapport médical;Service Médical;;;
Com de surend BDF;SBDF;Documents de Commission de surendettement BDF;Juridique;;;
SEPA;SEPA;SEPA;Comptabilité;;;
Titre pension;TPEN;Titre de pension;;;;
;;;;;;
Courrier bureautique analyse de devis;CBAD;Courrier bureautique analyse de devis;Bureautique;;;
Courrier de refus de résiliation;CRRE;Courrier de refus de résiliation;Bureautique;;;
Courrier d'information de résiliation;CIRE;Courrier d'information de résiliation;Bureautique;;;
;;;;;;
Courrier de refus de remboursement;CRER;Courrier de refus de remboursement;Bureautique;;;
Courrier de remboursement;CREM;Courrier de remboursement;Bureautique;;;
Courrier de relation tier;CRET;Courrier de relation tier;Bureautique;;;
Liste compta;LCOI;Liste compta;Comptabilité;;;
Courrier relation adhérent;CREA;Courrier relation adhérent;Bureautique;;;
