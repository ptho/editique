package oEdtk::Utils;
use base qw( Exporter );
use utf8;
use strict;
use warnings;
use Readonly;
use Cwd qw ( abs_path );
use oEdtk::Config qw(config_read);
use oEdtk::Main qw( oe_corporation_set );
use oEUser::Lib qw( user_corp_file_prefixe user_get_aneto_user );
use oEdtk::Tracking;
use Carp qw( carp croak );
use Data::Dumper qw( Dumper );
use Text::CSV_XS qw( csv );
use Term::ANSIColor qw( colored );
use Date::Calc qw( Days_in_Month );

our @EXPORT_OK =
  qw( parse_jjmmaaaa parse_price parse_yyyymmdd track_begin track track_end IDX_FORMAT alert flatten_index_variables max_value scalar_dump trim_list );

=pod
from edtkdoc.cls:
  \def\line{\xAppRef;\xIdLdoc;\thepage;\thedoc;\xCPDEST;"\city";\xIDDEST;"\nomdest";\xIDEMET;\dtedition;\xTYPPROD;\xPORTADR;%
  "\daddr";"\xCLEGEDi";"\daddri";"\xCLEGEDii";"\daddrii";"\xCLEGEDiii";"\daddriii";"\xCLEGEDiv";"\daddriv";\edCorp;\xDOCLIB;%
  \xREFIMP;"\daddrv";\xSOURCE;\xOWNER;\xHOST;\xIDIDX;\xCATDOC;\xCODRUPT;\xTYPDOC}%
=cut

Readonly my $LATEX_INDEX_FORMAT => [
    [ xAppRef => 'applicationReference' ],
    [ xIdLdoc => 'idLotDocuments' ],
    [ thepage => 'pageNumber' ],
    [ thedoc  => 'docNumber' ],
];

# Skip first four (only LaTeX can know about \thedoc and, above all, about \thepage)
Readonly my $INDEX_FORMAT => [
    [ xCPDEST    => 'indexDestCodePostal' ],
    [ xVILDEST   => 'indexDestVille' ],
    [ xIDDEST    => 'indexIdDest' ],
    [ xNOMDEST   => 'indexDestNomPrenom' ],
    [ xIDEMET    => 'indexIdEmetteur' ],
    [ dtedition  => 'date_edition' ],
    [ xTYPPROD   => 'type_production' ],
    [ xPORTADR   => '' ],                      # unused(!)
    [ daddr      => 'adresse', 0 ],
    [ xCLEGEDi   => 'indexClef', 0 ],
    [ daddri     => 'adresse', 1 ],
    [ xCLEGEDii  => 'indexClef', 1 ],
    [ daddrii    => 'adresse', 2 ],
    [ xCLEGEDiii => 'indexClef', 2 ],
    [ daddriii   => 'adresse', 3 ],
    [ xCLEGEDiv  => 'indexClef', 3 ],
    [ daddriv    => 'adresse', 4 ],
    [ edCorp     => 'corporation' ],
    [ xDOCLIB    => 'pdf_filename' ],
    [ xREFIMP    => 'ref_preimprime' ],
    [ daddrv     => 'adresse', 5 ],
    [ xSOURCE    => 'domaine_metier' ],
    [ xOWNER     => 'demandeur_edition' ],
    [ xHOST      => 'serveur_edition' ],
    [ xIDIDX     => 'version_idx' ],
    [ xCATDOC    => 'categorie_doc' ],         # unused(?)
    [ xCODRUPT   => 'code_rupture' ],
    [ xTYPEDOC   => 'type_document' ],
];

Readonly my $FULL_INDEX_FORMAT => [ @{$LATEX_INDEX_FORMAT}, @{$INDEX_FORMAT} ];

# XXX to remove
Readonly my $INDEX_CLEF_GED      => 'xCLEGED';
Readonly my $INDEX_ADRESSE_LISTE => 'xADRLN';
Readonly my $INDEX_ID_MNT_NUMABA => 'xIDDEST';
Readonly my $INDEX_ID_EMETTEUR   => 'xIDEMET';

# French strings constants
Readonly my @FRENCH_MONTHS => (
    "janvier",   "f\N{LATIN SMALL LETTER E WITH ACUTE}vrier",
    "mars",      "avril",
    "mai",       "juin",
    "juillet",   "ao\N{LATIN SMALL LETTER U WITH CIRCUMFLEX}t",
    "septembre", "octobre",
    "novembre",  "d\N{LATIN SMALL LETTER E WITH ACUTE}cembre"
);

Readonly my $TRACK_MODE_BATCH => 'Batch';
Readonly my $TRACK_DOCUMENT   => 'D';
Readonly my $TRACK_WARNING    => 'W';

my ( $CFG, $RUN_PARAMS );    # cached variables

## Function: kill_non_latin_1_9_chars_in_place( STRING_REF )
#  Keeps C0 control chars; replaces illegal latin-1/9 bytes with spaces.
#  Circumvents garbage sometimes received in flows.
#  Amor confirms (e-mail dated 20170530T14:03+02) LANG=fr_FR.8859-15
sub kill_non_latin_1_9_chars_in_place {
    my ($strRef) = @_;
    $$strRef =~ s/[\x7f\x80-\x9f]/\N{SPACE}/gsox;
    return;
}

## Function: trim_list( LIST )
#  Normalize blank spaces in a string.
sub trim_list {
    my (@l) = @_;
    my $str = join( "\N{SPACE}", @l );
    $str =~ s{^\s+}{}sx;
    $str =~ s{\s+@}{}sox;
    $str =~ s/\s+/\N{SPACE}/gsox;
    return $str;
}

## Function: IDX_FORMAT( VOID )
#  Returns the current index format.
sub IDX_FORMAT {
    return $INDEX_FORMAT;
}

## Function: parse_jjmmaaaa( YYYY, MM, DD )
#  Returns a non-empty error message if parameters are not a valid date.
sub check_date {
    my ( $year, $mon, $dd, $str ) = @_;
    return "bad month number for date '$str'" if $mon < 1 || $mon > 12;
    return "bad day number for date '$str'"
      if $dd < 1 || $dd > Days_in_Month( $year, $mon );
    return '';
}

## Function: parse_jjmmaaaa( STRING )
#  Returns (DD, "MONTH_NAME YEAR") if STRING is DD/MM/YYYY
sub parse_jjmmaaaa {
    my ( $str, $flowFile, $lineNumber ) = @_;
    if ( $str =~ m{^ 0* (\d+) / 0*(\d+) / (\d+) $}sx ) {
        my ( $dd, $mon, $year ) = ( $1, $2, $3 );
        my $error_msg = check_date( $year, $mon, $dd, $str );
        return alert( $flowFile, $lineNumber, $error_msg, [$str] )
          if length $error_msg;
        return [ $dd, "$FRENCH_MONTHS[$mon-1] $year" ];
    }
    return alert( $flowFile, $lineNumber, "cannot parse date '$str'", [$str] );
}

## Function: parse_yyyymmdd( STRING )
#  Returns (DD, "MONTH_NAME YEAR") if STRING is YYYYMMDDD
sub parse_yyyymmdd {
    my ( $str, $flowFile, $lineNumber ) = @_;
    if ( $str =~ m{^ (\d\d\d\d) (\d\d) (\d\d) $}sx ) {
        my ( $year, $mon, $dd ) =
          map { sprintf( '%d', $_ ) } ( $1, $2, $3 );    # leading 0's
        my $error_msg = check_date( $year, $mon, $dd, $str );
        return alert( $flowFile, $lineNumber, $error_msg, [$str] )
          if length $error_msg;
        return [ $dd, "$FRENCH_MONTHS[$mon-1] $year" ];
    }
    return alert( $flowFile, $lineNumber, "cannot parse date '$str'", [$str] );
}

## Function: parse_price( STRING )
#  Checks a price is valid.
sub parse_price {
    my ( $n, $flowFile, $lineNumber ) = @_;
    return $n if $n =~ m{^ [0-9]+(?:\.[0-9]+)? }sox || $n =~ m{^ \.[0-9]+ $}sox;
    return alert( $flowFile, $lineNumber, "cannot parse price '$n'", [$n] );
}

## Function: alert( FILE, LINE, MSG, OBJS)
#  Adds an alert to the list of current alerts.
sub alert {
    my ( $flowFile, $lineNumber, $msg, @objs ) = @_;
    carp "flowFile='$flowFile' lineNumber='$lineNumber' msg='$msg' objs: "
      . Dumper( \@objs );
}

# use as:
# perl -e 'use oEdtk::Utils; oEdtk::Utils::comment_index_file();'
# this will show the produced index in a pretty way to check it is ok
sub latest_idx1_in_directory {
    my ($dir) = @_;
    opendir( my $dh, $dir ) || croak $!;
    my ( $latest_file, $latest_time ) = ( '', 0 );
    while ( readdir $dh ) {
        my $file = $_;
        if ( $file =~ m{\.idx1$}sx ) {
            my $path  = "$dir/$file";
            my $ctime = ( stat($path) )[9];
            ( $latest_file, $latest_time ) = ( $path, $ctime )
              if $ctime > $latest_time;
        }
    }
    closedir $dh;
    croak "No file found in '$dir'" unless length $latest_file;
    return $latest_file;
}

sub comment_index_file {
    my ($file) = @_;
    $file = '.' unless defined $file;
    $file = latest_idx1_in_directory($file) if -d $file;
    my $aoa = csv( in => $file, sep_char => ';' );
    croak "Problem parsing CSV from '$file': $!" unless defined $aoa;
    my $nCols = scalar @{$FULL_INDEX_FORMAT};
    my $nRows = scalar @{$aoa};
    for ( my $i = 0 ; $i < $nRows ; $i++ ) {
        my $ii    = $i + 1;
        my $row   = $$aoa[$i];
        my $ncols = scalar @{$row};
        carp "Found $ncols columns on row $ii; expected $nCols"
          if $ncols != $nCols;
        print "\nROW $ii/$nRows\n";
        for ( my $j = 0 ; $j < $nCols ; $j++ ) {
            my $jj    = $j + 1;
            my $value = $$row[$j];
            $value = '(undef)' unless defined $value;
            my $s = $$FULL_INDEX_FORMAT[$j];
            my ( $old_field, $new_field, $index ) =
              @{ $$FULL_INDEX_FORMAT[$j] };
            $new_field .= "[$index]" if defined $index;
            print "COL $jj/$nCols: $old_field / $new_field: ",
              colored( $value, 'bold red' ), "\n";
        }
    }
    return;
}

## Function: TRACK_MODE_BATCH
#  Returns a constant.
sub TRACK_MODE_BATCH {
    return $TRACK_MODE_BATCH;
}

## Function: track_begin( TRACKING_JOB, USER, TRACKING_FIELDS )
#  Starts tracking. Returns a tracking object.
sub track_begin {
    my ( $trackingJob, $user, $tracking_fields, $mode ) = @_;
    $mode = TRACK_MODE_BATCH() unless defined $mode;
    return oEdtk::Tracking->new(
        $trackingJob,
        user   => $user,
        entity => oEdtk::Main::oe_corporation_set(),
        keys   => $tracking_fields,
        edmode => $mode,
    );
}

## Function: track( TRACK_OBJ, VALUES, DOCUMENT_TYPE )
#  Tracks a document.
sub track {
    my ( $trk, $l, $type ) = @_;
    $type = $TRACK_DOCUMENT unless defined $type;
    return $trk->track( $type, 1, @{$l} );
}

## Function: track_end( TRACK_OBJ )
#  End of tracking.
sub track_end {
    my ($trk) = @_;
    return $trk->track( $TRACK_WARNING, 1, '', '', '', '',
        'Fin de traitement' );
}

# XXX group accessors
## Function: INDEX_CLEF_GED( VOID )
#  Returns a constant.
sub INDEX_CLEF_GED {
    return $INDEX_CLEF_GED;
}

## Function: INDEX_ADRESSE_LISTE( VOID )
#  Returns a constant.
sub INDEX_ADRESSE_LISTE {
    return $INDEX_ADRESSE_LISTE;
}

## Function: INDEX_ID_MNT_NUMABA( VOID )
#  Returns a constant.
sub INDEX_ID_MNT_NUMABA {
    return $INDEX_ID_MNT_NUMABA;
}

## Function: INDEX_ID_EMETTEUR( VOID )
#  Returns a constant.
sub INDEX_ID_EMETTEUR {
    return $INDEX_ID_EMETTEUR;
}

## Function: init( FLOW_FILE, TRACKING_FIELDS )
#  Initializes the process and returns a tracking object.
sub init {
    my ( $flowFile, $tracking_fields ) = @_;
    return alert( $flowFile, -1, 'file does not exist' )  unless -e $flowFile;
    return alert( $flowFile, -1, 'file is not readable' ) unless -r $flowFile;
    my $trackingJob = abs_path($flowFile);
    carp "Using trackingJob='$trackingJob'";
    user_corp_file_prefixe( $flowFile, '_' );
    my $user = user_get_aneto_user($flowFile);
    return track_begin( $trackingJob, $user, $tracking_fields );
}

## Function: flatten_index_variables( HASH )
#  Returns a hash ref of the index_variables sub-hash with key names
#  prepended with "index".
sub flatten_index_variables {
    my ($h)   = @_;
    my $iv    = $h->{'index_variables'};
    my $index = {};
    foreach my $key ( keys %{$iv} ) {
        $index->{"index$key"} = $iv->{$key};
    }
    return $index;
}

## Function: flatten_index_variables( ARRAY_REF )
#  Returns the 'max' value (string-wise) in the list.
sub max_value {
    my ($l) = @_;
    my $res = $$l[0];
    foreach my $str ( @{$l} ) {
        $res = $str if $str gt $res;
    }
    return $res;
}

## Function: scalar_dump( SCALAR )
#  Returns the special bytes in an easier to read form.
sub scalar_dump {
    my ($str) = @_;
    my @res   = ();
    my $n     = length($str);
    for ( my $i = 0 ; $i < $n ; $i++ ) {
        my $char = substr( $str, $i, 1 );
        my $ord = ord($char);
        if (   ( $ord >= 32 )
            && ( $ord <= 126 )
            && ( $ord != 28 )
            && ( $ord != 29 ) )
        {
            push @res, $char;
        }
        else {
            push @res, sprintf( "(x%02x)", $ord );
        }
    }
    return join( '', @res );
}

1;
