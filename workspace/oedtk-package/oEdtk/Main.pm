package oEdtk::Main;

# This module is a attempt to pool code shared by all applications.
# However, this module should be entirely rewritten since:
#
#   1. some functionalities are uselessly coupled.
#   2. The module API is wrong, as it induces useless coupling between the present module and
#      the applications that use it.
#
# Please note that the presence of useless coupling is symptomatic of a chaotic software evolution.
#
# Note:
#
#     Please note that this module may be configured in order to record debug data.
#     This configuration may be done through the code, or through the environment.
#     Configuration through the code:
#        See the following private variables listed below:
#        - $_DEFAULT_OUTPUT_DUMP_MODE
#        - $_DEFAULT_OUTPUT_DUMP_FILE.
#     Configuration through the environment:
#        The environment variable MNT_OUTPUT_DUMP_MODE is used to enable/disable the debug mode.
#        The environment variable MNT_OUTPUT_DUMP_PATH is used to overwrite the default path to the DEBUG file.
#     See the function oe_print_to_output_file().
#     In order to activate the debug mode: export MNT_OUTPUT_DUMP_MODE=1

use utf8;

# XXX should be TEMPORARY, because the implications are reaching many files outside this one!
## no critic InputOutput::ProhibitBarewordFileHandles

use strict;
use warnings;


use Exporter;
our $VERSION = 0.8022;    # release number : Y.YMMS -> Year, Month, Sequence

our @ISA    = qw(Exporter);
our @EXPORT = qw(
  oe_app_usage
  oe_char_xlate
  oe_clean_addr_line
  oe_clean_addr
  oe_compo_link
  oe_corporation_get
  oe_corporation_set
  oe_corporation_tag
  oe_data_build
  oe_date_biggest
  oe_date_smallest
  oe_define_TeX_output
  oe_ID_LDOC
  oe_new_ID_LDOC
  oe_iso_country
  oe_list_encodings
  oe_new_job
  oe_now_time
  oe_num_sign_x
  oe_num2txt_us
  oe_open_fi_IN
  oe_open_fo_OUT
  oe_process_ref_rec
  oe_rec_motif
  oe_rec_pre_process
  oe_rec_process
  oe_set_sys_date
  oe_to_date
  oe_trimp_space
  oe_trt_ref_rec
  oe_uc_sans_accents
  oe_print_to_output_file
  oe_get_handle_to_input_file
  *OUT *IN @DATATAB $LAST_ENR
  %motifs %ouTags %evalSsTrt
);

use POSIX qw(mkfifo);
use Date::Calc qw(Today Gmtime);
use Encode;
use File::Basename;
use Getopt::Long qw( GetOptionsFromArray );
use List::MoreUtils qw(uniq);
use List::Util qw(reduce);
use Math::Round qw(nearest);
use Sys::Hostname;
use Scalar::Util qw(openhandle);
use oEdtk;
use Readonly;

require oEdtk::Outmngr;
require oEdtk::TexDoc;
use oEdtk::Dict;
use oEdtk::Config qw(config_read);
use oEdtk::Run qw(oe_status_to_msg oe_compo_run oe_after_compo oe_outmngr_output_run_tex);
use Data::UUID;

# [PRIVATE]
# This private flag has been introduced in order to ease the reverse engineering of the present software.
# It represents the default dump mode. The value may be:
# - 0: the dump mode is not activated.
# - 1: the dump mode is activated.
# Please note that you should leave the value of this configuration parameter to 0.
# @see oe_print_to_output_file()
Readonly::Scalar my $_DEFAULT_OUTPUT_DUMP_MODE => 0;

# [PRIVATE]
# This private flag has been introduced in order to ease the reverse engineering of the present software.
# It represents the default path to the file used to record all the text written in the output file.
# @see oe_print_to_output_file()
Readonly::Scalar my $_DEFAULT_OUTPUT_DUMP_FILE => 'output.rec';

our @DATATAB;              # le tableau dans lequel les enregistrements sont ventilés changer en OE_DATATAB
our $LAST_ENR     = "";    # QUID LAST_ENR ????
our $CURRENT_REC  = "";    # enrgistrement courant
our $PREVIOUS_REC = "";    # enregistrement précédent
our %motifs;               # rendre privée
our %ouTags;               # rendre privée
our %evalSsTrt;            # rendre privée

my $_ID_LDOC   = '';    # initialisation de l'identifiant unique de document (un par run)
my $PUSH_VALUE = "";

Readonly::Scalar my $TAG_OPEN       => "\\"; # une ouverture de balise (open)
Readonly::Scalar my $TAG_CLOSE      => "";   # une fermeture de balise (close)
Readonly::Scalar my $TAG_COMMENT    => "%";  # un commentaire (rem)

################################################################################
## 		SPECIFIQUE COMPUSET UTILISE PAR AILLEURS (XLS)
################################################################################

##  Function : oe_rec_motif ( CLEF , MOTIF )
#   FONCTION POUR DÉCRIRE LE MOTIF UNPACK DE L'ENREGISTREMENT
#
#   appel :
#   oe_rec_motif ($keyRec, "A2 A10 A15 A10 A15 A*");
sub oe_rec_motif {    # migrer oe_rec_motif
    my ($keyRec, $motif) = @_;
    $motifs{$keyRec} = $motif;
    1;
}

# Open the input file that contains the data exported from Infinite.

sub oe_open_fi_IN {    # GESTION DE BASE INPUT FILE
    my $fi = shift;
    open( IN, '<', $fi ) or die sprintf('Cannot open the input file "%s": %s', $fi, $!);
    1
}

# Open the output file that contains the data used to store LaTeX definitions.

sub oe_open_fo_OUT {    # GESTION DE BASE OUTPUT FILE
    my $fo = shift;
    open( OUT, '>', $fo ) or die sprintf('Cannot open the output file "%s": %s', $fo, $!);
    1;
}

# [PRIVATE]
# Returns the handle to the output file.
# @note The file handle OUT should not be accessed directly from the
#       outside of this module (all the more so, as a global variable!).

sub _oe_get_handle_to_output_file {
    my $fh = openhandle(*OUT);
    if (! defined $fh) {
        die "You tried to used the handle to the output file. However, the output file has not been opened yet!"
    }
    return $fh
}

# Returns the handle to the input file (that is, the file that contains the Infinite flux).
# @note The file handle IN should not be accessed directly from the
#       outside of this module (all the more so, as a global variable!).
# @todo Write a function oe_read_from_input_file().

sub oe_get_handle_to_input_file {
    my $fh = openhandle(*IN);
    if (! defined $fh) {
        die "You tried to used the handle to the input file. However, the output file has not been opened yet!"
    }
    return $fh
}

# Print a text into the output file.
# @param [string] $in_text Text to print.
# @note All actions to the output file should be done through an "output adaptor",
#       implemented as a plugin (that is: a class that implements an interface).
#       This function tries to make things better... However, the entire software
#       should be rewritten.

sub oe_print_to_output_file {
    my $in_text = shift;
    my $fh = _oe_get_handle_to_output_file();
    return print $fh $in_text;
}

# Print a text into the "output debug file".
# @param [string] $in_text Text to print.
# @note Please note the behaviour of this function can be configured through the
#       use of the environment variables listed below:
#       - MNT_OUTPUT_DUMP_MODE: activate or desactivate the recording (0: desactivated, 1: activated).
#       - MNT_OUTPUT_DUMP_PATH: the path to the file used to store the recorded data.
#       Both environment variables have default values (see $_DEFAULT_OUTPUT_DUMP_MODE and $_DEFAULT_OUTPUT_DUMP_FILE).

sub _dump {
    my ($in_text) = @_;

    # Is the dump mode activated ?
    my $dump_mode = exists $ENV{MNT_OUTPUT_DUMP_MODE} && defined $ENV{MNT_OUTPUT_DUMP_MODE} ? $ENV{MNT_OUTPUT_DUMP_MODE} : $_DEFAULT_OUTPUT_DUMP_MODE;
    if (! $dump_mode) {
        return;
    }

    my $dump_path = exists $ENV{MNT_OUTPUT_DUMP_PATH} && defined $ENV{MNT_OUTPUT_DUMP_PATH} ? $ENV{MNT_OUTPUT_DUMP_PATH} : $_DEFAULT_OUTPUT_DUMP_FILE;
    use open ':encoding(iso-8859-15)';
    open(my $fd, '>>', $dump_path) or die sprintf('The module "%s::Main" is configured to record all text written to the output file. Can not open the dump file "%s": %s.',  __PACKAGE__, $dump_path, $!);
    printf $fd "%s\n", $in_text;
    close($fd) or die sprintf('The module "%s::Main" is configured to record all text written to the output file. Can not close the dump file "%s": %s.',  __PACKAGE__, $dump_path, $!);
}


sub oe_rec_pre_process {
    my ($in_key_rec, $in_ref_fonction) = @_;
    # FONCTION POUR ASSOCIER UN PRÉ TRAITEMENT À UN ENREGISTREMENT
     #  ce traitement est effectué avant le chargement de l'enregistrement dans DATATAB
     #  le contenu de l'enregistrement précédent est toujours disponible dans DATATAB
     #  le type de l'enregistrement courant est connu dans le contexte d'execution
     #
     #  appel :
     # 	oe_rec_pre_process ($keyRec, \&fonction);
    $evalSsTrt{$in_key_rec}[0] = $in_ref_fonction;
    1;
}

sub oe_rec_process {
    # FONCTION POUR ASSOCIER UN TRAITEMENT À UN ENREGISTREMENT
    #  ce traitement est effectué juste après le chargement de l'enregistrement dans DATATAB
    #
    #  appel :
    # 	oe_rec_process ($keyRec, \&fonction);

    my ($in_key_rec, $in_ref_fonction) = @_;
    $evalSsTrt{$in_key_rec}[1] = $in_ref_fonction;
    1;
}

sub oe_process_ref_rec($$\$;$$) {    # migrer oe_process_ref_rec
    # ANALYSE ET TRAITEMENT COMBINES DES ENREGISTREMENTS
    #  il encapsule l'analyse et le traitement complet de l'enregistrement (oe_trt_ref_rec)
    #  il faut un appel par longueur de cle, dans l'ordre décroissant (de la cle la plus stricte à la moins contraingnante)
    #  APPEL :
    #	oe_process_ref_rec ($offsetKey, $lenKey, $ligne [,$offsetRec, $lenRec]);
    #  RETOURNE : statut
    #
    #	exemple 			if 		(oe_process_ref_rec (0, 3, $ligne)){
    #					} elsif 	(oe_process_ref_rec (0, 2, $ligne)){
    #						etc.
    my $offsetKey = shift;
    my $lenKey    = shift;
    my $refLigne  = shift;
    my $offsetRec = shift;    # optionnel
    $offsetRec ||= 0;
    my $lenRec = shift;       # optionnel
    $lenRec ||= "";

    if ( ${$refLigne} =~ m/^.{$offsetKey}(\w{$lenKey})/s
        && oe_trt_ref_rec( $1, $refLigne, $offsetRec, $lenRec ) )
    {
        # l'enregistrement a été identifié et traité
        # on édite l'enregistrement
        oe_print_to_output_file(${$refLigne});
        return 1;
    }

    # SINON ON A PAS RECONNU L'ENREGISTREMENT, C'EST UN ECHEC
    return 0;
}

sub oe_trt_ref_rec($\$;$$) {    # migrer oe_trt_ref_rec
                                # TRAITEMENT PRINCIPAL DES ENREGISTREMENTS
     # MÉTHODE GÉNÉRIQUE V0.2.1 27/04/2009 10:05:03 (le passage de référence devient implicite)
     # LA FONCTION A BESOIN DU TYPE DE L'ENREGISTEMENT ET DE LA RÉFÉRENCE À UNE LIGNE DE DONNÉES
     #  appel :
     #	oe_trt_ref_rec($Rec_ID, $ligne [,$offsetRec,$lenRec]);
     #  retourne : statut, $Rec_ID
    my $Rec_ID   = shift;
    my $refLigne = shift;
    my $offsetRec =
      shift;    # OFFSET OPTIONNEL DE DONNÉES À SUPPRIMER EN TÊTE DE LIGNE
    my $lenRec = shift;    # LONGUEUR ÉVENTUELLE DE DONNÉEES À TRAITER
                           # VALEURS PAR DÉFAUT
    $ouTags{$Rec_ID} ||= "-1";
    $motifs{$Rec_ID} ||= "";
    $offsetRec       ||= 0;
    $lenRec          ||= "";

# SI MOTIF D'EXTRACTION DU TYPE D'ENREGISTREMENT N'EST PAS CONNU,
#  ET SI IL N'Y A AUCUN PRE TRAITEMENT ASSOCIÉ AU TYPE D'ENREGISTREMENT,
#  ALORS LE TYPE D'ENREGISTREMENT N'EST PAS CONNU
#
# CE CONTRÔLE PERMET DE DÉFINIR DYNAMIQUEMENT UN TYPE D'ENREGISTREMENT EN FOCNTION DU CONTEXTE
#  C'EST A DIRE QU'UN ENREGISTREMENT TYPÉ "1" POURRA AVOIR DES CARACTÉRISITQUES DIFFÉRENTES
#  EN FONCTION DU TYPE D'ENREGISTREMENT TRAITÉ PRÉCÉDEMMENT.
#  CES CARACTÉRISITIQUES PEUVENT ÊTRE DÉFINIES AU MOMENT DU PRÉ TRAITEMENT.
#
    if ( $motifs{$Rec_ID} eq "" && !( $evalSsTrt{$Rec_ID}[0] ) ) {
        warn "INFO : oe_trt_ref_rec() > LIGNE $. REC. '$Rec_ID' (offset $offsetRec) UNKNOWN\n";
        return 0;
    }

    $PREVIOUS_REC = $CURRENT_REC;
    $CURRENT_REC  = $Rec_ID;

    # STEP 0 : EVAL PRE TRAITEMENT de $refLigne
    &{ $evalSsTrt{$Rec_ID}[0] }($refLigne) if $evalSsTrt{$Rec_ID}[0];

    # ON S'ASSURE DE BIEN VIDER LE TABLEAU DE LECTURE DE L'ENREGISTREMENT PRECEDENT
    undef @DATATAB;

    # EVENTUELLEMENT SUPPRESSION DES DONNEES NON UTILES (OFFSET ET HORS DATA UTILES (lenData))
    ${$refLigne} =~ s/^.{$offsetRec}(.{1,$lenRec}).*/$1/ if ( $offsetRec > 0 );

    # ECLATEMENT DE L'ENREGISTREMENT EN CHAMPS
    @DATATAB = unpack( $motifs{$Rec_ID}, ${$refLigne} )
      or die "ERROR: oe_trt_ref_rec() > LIGNE $. typEnr '$Rec_ID' motif >$motifs{$Rec_ID}< UNKNOWN\n";

    # STEP 1 : EVAL TRAITEMENT CHAMPS
    &{ $evalSsTrt{$Rec_ID}[1] } if $evalSsTrt{$Rec_ID}[1];

    # STRUCTURATION DE L'ENREGISTREMENT POUR SORTIE
    if ( $ouTags{$Rec_ID} ne "-1" ) {
        ${$refLigne} = "${TAG_OPEN}a${Rec_ID}${TAG_CLOSE}";
        ${$refLigne} .= sprintf( $ouTags{$Rec_ID}, @DATATAB )
          or die "ERROR: oe_trt_ref_rec() > LIGNE $. typEnr '$Rec_ID' ouTags >$ouTags{$Rec_ID}<\n";
        ${$refLigne} .= "${TAG_OPEN}e${Rec_ID}${TAG_CLOSE}\n";
    }
    else {
        ${$refLigne} = "";
    }
    $LAST_ENR = $Rec_ID;

    # STEP 2 : EVAL POST TRAITEMENT
    &{ $evalSsTrt{$Rec_ID}[2] } if $evalSsTrt{$Rec_ID}[2];

    # ÉVENTUELLEMENT AJOUT DE DONNÉES COMPLÉMENTAIRES
    ${$refLigne} .= $PUSH_VALUE;
    $PUSH_VALUE = "";
    ${$refLigne} =~ s/\s{2,}/ /g;    #	CONCATÉNATION DES BLANCS
                                     #$LAST_ENR=$Rec_ID;

    return 1, $Rec_ID;
}



################################################################################


sub oe_num_sign_x(\$;$) {    # migrer oe_num_sign_x
    # traitement des montants signés alphanumeriques
    # recoit : une reference a une variable alphanumerique
    #          un nombre de décimal après la virgule (optionnel, 0 par défaut)

    my ( $refMontant, $decimal ) = @_;
    ${$refMontant} ||= "";
    $decimal ||= 0;

    # controle de la validite de la valeur transmise
    ${$refMontant} =~ s/\s+//g;
    if ( ${$refMontant} eq "" || ${$refMontant} eq 0 ) {
        ${$refMontant} = 0;
        return 1;
    }
    elsif ( ${$refMontant} =~ /\D{2,}/ ) {
        warn "INFO : value (${$refMontant}) not numeric.\n";
        return -1;
    }

    my %hXVal;
    $hXVal{'p'} = 0;
    $hXVal{'q'} = 1;
    $hXVal{'r'} = 2;
    $hXVal{'s'} = 3;
    $hXVal{'t'} = 4;
    $hXVal{'u'} = 5;
    $hXVal{'v'} = 6;
    $hXVal{'w'} = 7;
    $hXVal{'x'} = 8;
    $hXVal{'y'} = 9;

    if ( ${$refMontant} =~ s/(\D{1})$/$hXVal{$1}/ ) {

        # une valeur avec signe negatif alphanumerique 213y => -2139
        ${$refMontant} = ( ${$refMontant} * (-1) );

        # warn "INFO : MONTANT SIGNE";
    }
    elsif ( ${$refMontant} =~ /^-{1}/ ) {

        # une valeur avec un signe negatif -123456
    }

    ${$refMontant} = ${$refMontant} / ( 10**$decimal );

    return ${$refMontant};
}

# Return the current timestamp (YYYYDDMMHHMMSS).
# @return [string] The function returns the current timestamp.

sub oe_now_time {
    my $time = time;
    my ($sec, $min, $hour, $mday, $mon, $year) = (gmtime($time))[0,1,2,3,4,5];
    $time = sprintf(
        "%4.0f%02.0f%02.0f%02.0f%02.0f%02.0f",
        $year + 1900,
        $mon + 1, $mday, $hour, $min, $sec
    );

    return $time;
}


my $_DICO_COUNTRY; # DICTIONNAIRE ISO DES PAYS
my $_DICO_POST;    # DICTIONNAIRE DES ABBRÉVIATIONS.
my $_DICO_CHAR;    # DICTIONNAIRE de translation des caractères (déconseillés, mais parfois nécessaires).

{
    my $_LAST_ISO = "";

    sub oe_iso_country {

        # retourne le code pays dans la codification country_dico.ini ou la dernière valeur connue
        my ($country) = @_;
        if ( !defined($_DICO_COUNTRY) ) {
            my $cfg = config_read();
            $_DICO_COUNTRY = oEdtk::Dict->new( $cfg->{'EDTK_DICO_COUNTRY'},
                , { section => $cfg->{'EDTK_LANG'} } );
        }
        $_LAST_ISO = $_DICO_COUNTRY->translate($country) if defined($country);
        return $_LAST_ISO;
    }
}

sub oe_char_xlate {    # à migrer dans le dictionnaire ?
    my $scalar = shift;

    if ( !defined($_DICO_CHAR) ) {
        my $section = shift || 'DEFAULT';
        my $cfg = config_read();
        $_DICO_CHAR = oEdtk::Dict->new( $cfg->{'EDTK_DICO_XLAT'},,
            { section => $section } );
    }
    $scalar = $_DICO_CHAR->substitue($scalar);

    return $scalar;
}

sub oe_clean_addr_line(\$) {    # migrer oe_clean_addr_line
     # CETTE FONCTION PERMET UN NETTOYAGE DES LIGNES D'ADRESSE POUR CONSTRUIRE LES BLOCS D'ADRESSE DESTINTATIRE
     # elle travaille sur la référence de la variable directement mais retourne aussi la chaine resultante
    my $rLine = shift;

    # valeur par défaut dans le cas où le champs serait undef
    if ( !defined($$rLine) || length($$rLine) == 0 ) {
        $$rLine = '';
        return $$rLine;
    }

    chomp($$rLine)
      ;  # pour être sûr de ne pas avoir de retour à la ligne en fin de champ
    oe_trimp_space($rLine);

    $$rLine = oe_char_xlate( $$rLine, 'ADDRESS' );

    # à faire : une expression régulière qui traite tout ce qui n'est pas 0-9\-\°\w par des blancs...

    # LA LIGNE SUIVANTE N'EST À ACTIVER QU'APRÈS TEST, POUR LE MOMENT PRIS EN CHARGE PAR LE oe_char_xlate
    #$$rLine =~ s/\\+/\//g;	# on supprime on remplace les backslash qui sont des caractères d'échappement et qui semble-t-il ne sont pas comptés lors du controle de taille (sprintf("%.38s",...)
    $$rLine =~ s/^\s+//
      ; # on supprime les blancs consécutifs en début de chaîne (on a fait un oe_trimp_space en premier...) TRIM gauche
    $$rLine =~ s/\s+$//
      ; # on supprime les blancs consécutifs en fin de chaîne (...) TRIM droite
    $$rLine =~ s/^0\s+//
      ; # on supprime les zéros tout seul en début de chaine (on le passe en dernier, après les TRIM gauche)
    $$rLine =~ s/\s+/ /;    # concentration des blancs consécutifs

    # Use the given dictionary to translate words.
    if ( length($$rLine) > 38 ) {
        if ( !defined($_DICO_POST) ) {
            my $cfg = config_read();
            $_DICO_POST = oEdtk::Dict->new( $cfg->{'EDTK_DICO_POST'} );
        }
        my @words = split( / /, $$rLine );
        $$rLine = join( ' ', map { $_DICO_POST->translate($_) } @words );
    }
    $$rLine = sprintf( "%.38s", $$rLine )
      ;    # on s'assure de ne pas dépasser 38 caractères par lignes

    return $$rLine;
}

sub oe_clean_addr(\$) {    # migrer oe_clean_addr_line
    # CETTE FONCTION PERMET UN NETTOYAGE DES LIGNES D'ADRESSE POUR CONSTRUIRE LES BLOCS D'ADRESSE DESTINTATIRE
    # elle travaille sur la référence de la variable directement mais retourne aussi la chaine resultante
    my $rLine = shift;

    # valeur par défaut dans le cas où le champs serait undef
    if ( !defined($$rLine) || length($$rLine) == 0 ) {
        $$rLine = '';
        return $$rLine;
    }

    chomp($$rLine)
      ;  # pour être sûr de ne pas avoir de retour à la ligne en fin de champ
    oe_trimp_space($rLine);

    $$rLine = oe_char_xlate( $$rLine, 'ADDRESS' );

    # à faire : une expression régulière qui traite tout ce qui n'est pas 0-9\-\°\w par des blancs...

# LA LIGNE SUIVANTE N'EST À ACTIVER QU'APRÈS TEST, POUR LE MOMENT PRIS EN CHARGE PAR LE oe_char_xlate
#$$rLine =~ s/\\+/\//g;	# on supprime on remplace les backslash qui sont des caractères d'échappement et qui semble-t-il ne sont pas comptés lors du controle de taille (sprintf("%.38s",...)
    $$rLine =~ s/^\s+//
      ; # on supprime les blancs consécutifs en début de chaîne (on a fait un oe_trimp_space en premier...) TRIM gauche
    $$rLine =~ s/\s+$//
      ; # on supprime les blancs consécutifs en fin de chaîne (...) TRIM droite
    $$rLine =~ s/^0\s+//
      ; # on supprime les zéros tout seul en début de chaine (on le passe en dernier, après les TRIM gauche)
    $$rLine =~ s/\s+/ /;    # concentration des blancs consécutifs
    $$rLine =~ s/(\s{1}\W{1}){2}/$1/
      ;    # on supprime les espaces suivis de non alpha-numérique consécutifs
    $$rLine =~ s/\W+$//
      ;    # on supprime les non alpha-numérique consécutifs en fin de chaîne

    return $$rLine;
}

# FIXME this is buggy and horrible
sub oe_uc_sans_accents(\$) {    # migrer oe_uc_sans_accents
     # CETTE FONCTION CONVERTIT LES CARACTÈRES ACCENTUÉS EN CARACTÈRES NON ACCENTUÉS
     # l'utilisation de la localisation provoque un bug dans la commande "sort".
     # On ne s'appuie pas sur la possibilité de rétablir le comportement par défaut par échappement
     # (la directive no locale ou lorsqu'on sort du bloc englobant la directive use locale)
     # de façon à adopter un mode de fonctionnement standard et simplifié.
     # NB : la localisation ralentit considérablement les tris.
     # (cf. doc Perl concernant la localisation : perllocale)
     #
     # l'appel de la fonction se fait par passage de référence implicite
     #	oe_uc_sans_accents($chaine);

    my $refChaine = shift;
    ${$refChaine} ||= "";
    ${$refChaine} =~ s/[\x{e0}-\x{e5}]/a/ig;
    ${$refChaine} =~ s/\x{e6}/ae/ig;
    ${$refChaine} =~ s/\x{e7}/c/ig;
    ${$refChaine} =~ s/[\x{e8}-\x{eb}]/e/ig;
    ${$refChaine} =~ s/[\x{ec}-\x{ef}]/i/ig;
    ${$refChaine} =~ s/[\x{f2}-\x{f6}]/o/ig;
    ${$refChaine} =~ s/[\x{f9}-\x{fc}]/u/ig;
    ${$refChaine} = uc ${$refChaine};

    return ${$refChaine};
}

sub oe_trimp_space(\$) {    # migrer oe_trimp_space
       # SUPPRESSION DES ESPACES CONSECUTIFS (TRAILING BLANK) PAR GROUPAGE
       # le parametre doit etre une reference, exemple : oe_trimp_space($chaine)
       # retourne le nombre de caracteres retires
    my $rChaine = shift;
    ${$rChaine} ||= "";
    ${$rChaine} =~ s/\s{2,}/ /go;

    return ${$rChaine};
}

sub oe_list_encodings {
    my @list = Encode->encodings();
    warn "INFO : available encodings on this machine are : @list\n";
}


sub oe_to_date(\$) {    # migrer oe_to_date
                        # RECOIT UNE REFERENCE SUR UNE DATE AU FORMAT AAAAMMJJ
                        # FORMATE AU FORMAT JJ/MM/AAAA
    my $refVar = shift;
    ${$refVar} ||= "";
    ${$refVar} =~ s/(\d{4})(\d{2})(\d{2})(.*)/$3\/$2\/$1/o;

    return ${$refVar};
}

# Convert a date in DD/MM/YYYY format to YYYYMMDD format.

sub oe_date_convert {    # oe_date_format
    my $date = shift;

    if ( $date !~ /^\s*(\d{1,2})\/(\d{1,2})\/(\d{4})\s*$/ ) {
        return;
    }
    return sprintf( "%d%02d%02d", $3, $2, $1 );
}

sub oe_date_compare {
    my ( $date1, $date2 ) = @_;

    if ( $date1 eq '' ) {
        $date1 = $date2;
    }

    my $wdate1 = oe_date_convert($date1);
    my $wdate2 = oe_date_convert($date1);

    if ( !defined($wdate1) ) {
        warn "INFO : Unexpected date format: \"$date1\" should be dd/mm/yyyy. Date ignored\n";
        return 1;
    }
    if ( !defined($wdate2) ) {
        warn "INFO : Unexpected date format: \"$date2\" should be dd/mm/yyyy. Date ignored\n";
        return -1;
    }
    return $wdate1 <=> $wdate2;
}

sub oe_date_smallest {
    my ( $date1, $date2 ) = @_;

    if ( oe_date_compare( $date1, $date2 ) <= 0 ) {
        return $date1;
    }
    else {
        return $date2;
    }
}

sub oe_date_biggest {
    my ( $date1, $date2 ) = @_;

    if ( oe_date_compare( $date1, $date2 ) <= 0 ) {
        return $date2;
    }
    else {
        return $date1;
    }
}

sub oe_num2txt_us(\$) {

    # traitement des montants au format Texte
    # le séparateur de décimal "," est transformé en "." pour les commandes de chargement US / C7
    # le séparateur de millier "." ou " " est supprimé
    # recoit : une variable alphanumerique formattée pour l'affichage
    # 		$value = oe_num2txt_us($value);
    # ou par référence
    # 		oe_num2txt_us($value);

    my $refValue = shift;
    ${$refValue} ||= "";

    if ( ${$refValue} ) {
        ${$refValue} =~ s/\s+//g;     # suppression des blancs
        ${$refValue} =~ s/\.//g;      # suppression des séparateurs de milliers
        ${$refValue} =~ s/\,/\./g;    # remplacement du séparateur de décimal
        ${$refValue} =~
          s/(.*)(\-)$/$2$1/;   # éventuellement on met le signe négatif devant

    }
    else {
        ${$refValue} = 0;
    }

    return ${$refValue};
}

# Set the path to the included (.txt) file that contains the definitions of the tags.
# The given path to a file will be injected to the LaTex code below:
#     \input{<path to a file>}
# @param [string] $in_base_name Base name of the file.
# @param [undef | string] $in_opt_path_or_config_key Optional parameter that may represents;
#        - the name of a configuration parameter (EDTK_DIR_...), that represents the path to a directory.
#        - a path to a directory.
#        If this parameter is not specified, then the default value is "." (the current directory).

sub oe_include_build
{
    my ( $in_base_name, $in_opt_path_or_config_key ) = @_;
    my $doc = oEdtk::TexDoc->new();
    $doc->include( $in_base_name, $in_opt_path_or_config_key );
    return $doc;
}

# Create the text that represents the definition of a tag.
# @param [string] $in_name Name of the tag.
# @param [undef | string | array ref | hash ref] $in_value Optional value.
# @return [string] The function returns that text that represents the definition of the tag.

sub oe_data_build {
    my ( $in_name, $in_value ) = @_;
    my $tag = oEdtk::TexTag->new( $in_name, $in_value );
    return $tag->emit();
}

sub oe_app_usage {    # migrer oe_app_usage
    my $app = "";
    $0 =~ /([\w-]+[\.plmex]*$)/;
    $1 ? $app = "application.pl" : $app = $1;
    print STDOUT << "EOF";

 Usage :	$app </source/input/file.dat> [job] [options]
 Usage :	$app --noinputfiles [job] [options]
 options :
		--massmail 	to confirm mass treatment
		--edms		to confirm edms treatment
		--cgi
				these values depend on ED_REFIDDOC config table
				(example : omgr treatment confirmation)

		--input_code	input caracters encoding
				(ie : --input_code=iso-8859-1)
		--noinputfiles	no data file needed for treatment
		--help		this message

EOF

    # 		--notracking	halt tracking, do not track
    oe_list_encodings();
    exit 1;
}

# XXX Global variable used to remember stuff from oe_new_job() when we
# are in oe_compo_link().  It would be *much* better to keep state in an
# object instance instead.
my $_RUN_PARAMS;

# Circumvent long-standing core Perl bug. See
# https://rt.cpan.org/Public/Bug/Display.html?id=120562
sub myGetOptions {
    my (@args) = @_;
    my @argv = @ARGV;
    GetOptionsFromArray( \@argv, @args );
    @ARGV = @argv;
    return;
}

# Create a new "job definition", and, eventually run the process that creates the output (PDF) file asynchronously.
# Please note that if a process is run asynchronously, then the "job definition" is a "fifo file".
# A "job definition" is a text file (.txt).
# Please note that:
#   - the name of the job's file contains the name of the application (ex: "PRACI-AC014.txt").
#   - this file is accessible through the global file handle OUT.
# @param [array] A list of command line options. Options can be:
#        --help
#        --index
#        --edms
#        --cgi
#        --noinputfiles
#        --input_code=s (ex: utf8)
# @note Please note that this function opens the data file (generated by Infinite).
#       The opened input file is accessible through the global file handle IN.

sub oe_new_job {

    # surcharge éventuelle des options avec les paramamètre de oe_new_job pour myGetOptions
    @ARGV = ( @ARGV, @_ );

    # Load all the configuration parameters the describes the publication environment:
    # EDTK_BIN_COMPO, EDTK_COMPO, EDTK_COMPO_CMD_PDF...
    my $cfg    = config_read('COMPO');
    my $params = {};

    # DEFAULT OPTION VALUES.
    my %defaults = (

        #		xls	 	=> 0,
        #		tex		=> 0,
        index      => 0,
        massmail   => 0,
        edms       => 0,
        cgi        => 0,
        input_code => 0
    );

    # exemples d'ajout de paramètres au lancement
    # oe_new_job('--index');
    # oe_new_job("--input_code=utf8");

    myGetOptions( \%defaults, 'help', 'index', 'massmail', 'edms', 'cgi', 'noinputfiles', 'input_code=s' );
    while ( my ( $key, $val ) = each(%defaults) ) {
        if ( !defined( $params->{$key} ) ) {
            $params->{$key} = $val;
        }
    }

    # Set the default FIFO mode.

    if ( $^O ne 'MSWin32' ) {
        $defaults{'fifo'} = 1;
    }
    else {
        $defaults{'fifo'} = 0;
    }

    $params->{'doclib'} = _omngr_doclib();
    $params->{'idldoc'} = oe_ID_LDOC();

    # Please note that the value of the parameter EDTK_PRGNAME is initialised within "Config::config_read()".
    $params->{'outfile'}     = $cfg->{'EDTK_PRGNAME'} . ".txt";    # devrait être lié à TexMode
    $params->{'output_code'} = $cfg->{'EDTK_OUT_ENCODING'} || 'utf8';
    $params->{'output_code'} = ">:encoding(" . $params->{'output_code'} . ")";
    if ( $params->{'input_code'} ) {
        $params->{'input_code'} = "<:encoding(" . $params->{'input_code'} . ")";
    }
    else {
        $params->{'input_code'} = "<";
    }

    my $fi;
    if ( $defaults{help} ) {
        &oe_app_usage();
        exit 0;
    }
    elsif ( $defaults{noinputfiles} ) {
        $fi = 0;
        warn "INFO : no input file for this treatment\n";
    }
    elsif ( $#ARGV == -1 ) {    # TO KEEP COMPATIBILITY
        &oe_app_usage();
        exit 0;
    }
    else {
        $fi = $ARGV[0];         # TO KEEP COMPATIBILITY
        open(IN, $params->{input_code}, $fi) or die sprintf('Cannot open the input fie "%s": %s', $fi, $!);
        warn "INFO : input perl data is $fi (encode \'"
          . $params->{'input_code'}
          . "\' $ARGV[-1])\n";
    }

    # Override default setting if EDTK_COMPO_ASYNC is set in edtk.ini.

    if (exists $cfg->{'EDTK_COMPO_ASYNC'} &&
        defined $cfg->{'EDTK_COMPO_ASYNC'} &&
        $cfg->{'EDTK_COMPO_ASYNC'}) {
        my $async = $cfg->{'EDTK_COMPO_ASYNC'};
        if ($async =~ /^yes$/i ) {
            $params->{'fifo'} = 1;
        } elsif ($async =~ /^no$/i) {
            $params->{'fifo'} = 0;
        } else {
            die sprintf 'Unexpected value for configuration tag EDTK_COMPO_ASYNC: "%s"', $async;
        }
    }

    if ($params->{'fifo'}) {
        if (   ( exists $cfg->{EDTK_FIFO_MODE} )
            && ( $cfg->{EDTK_FIFO_MODE} eq 'NONE' ) )
        {
            warn "INFO: Deactivating FIFO MODE, as requested in the configuration";
            $params->{'fifo'} = 0;
        }
        elsif ( $^O eq 'MSWin32' ) {
            warn "INFO : FIFO mode is not possible under Windows, ignoring.\n";
            $params->{'fifo'} = 0;
        }
    }

    # If we are in FIFO mode and there is a left-over text file, the mkfifo()
    # call would fail. If we are not in FIFO mode and there's a left-over FIFO,
    # we would hang indefinitely, so make sure to remove this file first.
    if (-e $params->{'outfile'}) {
        unlink($params->{'outfile'}) or die(sprintf 'Cannot remove the file "%s": %s', $params->{'outfile'}, $!);
    }

    # Handle options passed in the EDTK_OPTIONS environment variable.
    if ( exists( $ENV{'EDTK_OPTIONS'} ) ) {
        my @opts = split( ',', $ENV{'EDTK_OPTIONS'} );
        foreach my $opt (@opts) {
            $params->{$opt} = 1;
        }
    }

    # Start "pdflatex" if the fifo mode is activated.

    if ( $params->{'fifo'} ) {
        warn "INFO : Creating FIFO for output data file (" . $params->{'outfile'} . ")\n";
        mkfifo( $params->{'outfile'}, oct(700) ) or die sprintf('ERROR: Could not create fifo in file "%s": %s', $params->{'outfile'}, $!);

        # Executed command (oEdtk::Run::oe_cmd_run_bg()):
        #
        # pdflatex -halt-on-error \
        #          -shell-escape \
        #          -interaction=nonstopmode \
        #          "/path/to/latex/file.tex" >&2
        #
        # Please note that "/path/to/latex/file.tex" is a "fifo file".

        my $pid = oe_compo_run( $cfg->{'EDTK_PRGNAME'}, $params );
        $params->{'pid'} = $pid;
    }

    # Create the output file "/path/to/latex/file.tex".
    _dump(sprintf "\n%\n% ==> %s <==\n%\n%", $params->{outfile});
    open( OUT, $params->{output_code}, $params->{outfile} ) or die sprintf 'Can not open the output file "%s": %s', $params->{outfile}, $!;
    warn sprintf('INFO : input compo data is "%s" (encode is "%s".', $params->{'outfile'}, $params->{'output_code'});

    # Remember for later use in oe_compo_link() & oEdtk::Main.

    $_RUN_PARAMS = $params;

    if ( defined $cfg->{'EDTK_COMPO_INCLUDE'}
        && $cfg->{'EDTK_COMPO_INCLUDE'} =~ /yes/i )
    {
        oe_print_to_output_file(
            oe_include_build($cfg->{'EDTK_PRGNAME'} . "." . $cfg->{'EDTK_EXT_COMPO'}, 'EDTK_DIR_SCRIPT' )
        );
    }

    oe_print_to_output_file(oe_data_build(oe_corporation_tag()));
    oe_print_to_output_file(oe_data_build( 'xIdLdoc', $params->{'idldoc'} ));
    oe_print_to_output_file(oe_data_build('xDebFlux'));
    oe_print_to_output_file(oe_data_build( 'xAppRef', $cfg->{'EDTK_PRGNAME'} ));
    oe_print_to_output_file(oe_data_build( 'xDOCLIB', $params->{'doclib'} ));

    my $env = $cfg->{'EDTK_TYPE_ENV'};
    if ( $env ne 'Production' ) {
        # On génère le filigrane de 'TEST EDITION'.
        oe_print_to_output_file(oe_data_build( 'xWaterM', $cfg->{'EDTK_WATERMARKTEXT'} || ' ' ));
        oe_print_to_output_file(oe_data_build('xTstApp'));
    }
    else {
        # Pas de filigrane.
        oe_print_to_output_file(oe_data_build('xProdApp'));
    }
    oe_print_to_output_file(oe_data_build( 'xTYPPROD', substr( $env, 0, 1 ) ));
    oe_print_to_output_file(oe_data_build( 'xHOST', hostname() ));

    # Do we want to generate an index file?
    if ( $params->{'index'} ) {
        oe_print_to_output_file(oe_data_build('xStOmgr'));
        oe_print_to_output_file(oe_data_build( 'xHost', hostname() ));
    }
    oe_print_to_output_file($TAG_COMMENT);
    oe_print_to_output_file("\n");
}

# Generates the output (PDF) file.
# Please note that this function may run the process that generates the output (PDF) file
# asynchronously, or it may wait that a previously launched process terminates (see function
# "oe_new_job()").

sub oe_compo_link {    # migrer oe_close_files oe_compo_link
    # SI LE FLUX D'ENTREE FAIT MOINS DE 1 LIGNE (variable $.), SORTIES EN ERREUR
     # if ($. == 0) {
     #	# FLUX INVALIDE ARRET
     #	die 	"ERROR: uncomplete datastream\n $message \n\n";
     #}

    my @opt    = @_;
    my $cfg    = config_read('COMPO');
    my $params = $_RUN_PARAMS;
    $params->{'corp'} = oe_corporation_set();
    if (@opt) {
        foreach (@opt) {
            $_ =~ s/\-+//g;
            $params->{$_} = 1;
        }
    }

    oe_print_to_output_file(oe_data_build('xFinFlux'));

    close(OUT) or die sprintf('An error occurred while closing the output file "%s": %s', $params->{outfile}, $!);
    if ( ! $params->{'noinputfiles'} ) {
        close(IN) or die "An error occurred while closing the input file: $!";
    }

    # If the "fifo mode" is activated: wait for the "pdflatex" process to terminate.
    # Otherwise, start the "pdflatex" process.

    if ( $params->{'fifo'} ) {

        # Disable signal handler.
        $SIG{'CHLD'} = 'DEFAULT';
        if ( !defined( $params->{'cldstatus'} ) ) {

            # Wait for the LaTeX process to terminate.
            my $pid = $params->{'pid'};
            warn "INFO : Waiting for the LaTeX process to terminate ($pid)...\n";
            my $kid = waitpid( $pid, 0 );
            if ( $kid <= 0 ) {
                die "ERROR: Could not collect child process status: $!\n";
            }
            $params->{'cldstatus'} = $?;
        }
        my $status = $params->{'cldstatus'};
        if ( $status != 0 ) {
            my $msg = oe_status_to_msg($status);
            die "ERROR: LaTeX process failed: $msg\n";
        }
        warn "INFO : The LaTeX process terminated successfully.\n";
    }
    else {
        # Run the LaTeX process now.
        oe_compo_run( $cfg->{'EDTK_PRGNAME'}, $params );
    }

    oe_after_compo( $cfg->{'EDTK_PRGNAME'}, $params );

    return 1;
}

sub oe_new_ID_LDOC {
    $_ID_LDOC = Data::UUID->new->create_str();
    return $_ID_LDOC;
}

sub oe_ID_LDOC {

# on ne le génère qu'une fois par run : plusieurs appels dans la même instance retournent le même id
    if ( $_ID_LDOC eq '' ) {
        oe_new_ID_LDOC();
    }
    return $_ID_LDOC;
}

{
    my $_app_typ_trt;    # type de traitement de lotissement,
        # valeur accessible uniquement par la méthode _app_typ_trt

    sub _app_typ_trt {

# désignation du type de traitement de lotissement (Output Management)
# si la fonction est appelée avec un paramètre on l'attribue à $_app_typ_trt
# si la fonction est appelée seule, on renvoie juste la valeur normée
# par defaut la valeur est 'U' pour 'undef'
# ON NE PEUT PAS CHANGER DE VALEUR EN COURS DE TRAITEMENT, SAUF pour passer en Test ou Debug
# valeurs possibles :
# - 'M' -> traitement de Masse avec lotissement
# - 'G' -> traitement de reGroupement, lotissement en attente
# - 'L' -> traitement édition Locale sans lotissement
# - 'H' -> traitement homologation sans lotissement
# - 'T' -> traitement test/homologation, lotissement en test possible
# - 'D' -> mode Debug, conservation des fichiers intermédiaires
# - 'U' -> 'undef' traitement sans lotissement

# Gestion des types d'éxécution (Mass/Grouped/Local/Homol/Test/Debug/Undef) en 3 groupes :
# - MTD -> font du Lotissement
# - G	-> lotissement en attente
# - LHU -> ne font pas de lotissement
# - D   -> ne supprime pas les fichiers intermédiaires
# - U   -> mode par défaut
# - H   -> mode associé à l'extension 'Homologation' (-V2)

# Nouvelle gestion d'exécution à partir de EDTK_TYPE_ENV :
# EDTK_TYPE_ENV = Production	-> cleanup, si mode indexé : détermination des trt à partir de EDTK_REFIDDOC
# EDTK_TYPE_ENV = Integration	-> cleanup, bandeau, si mode indexé : détermination des trt à partir de EDTK_REFIDDOC
# EDTK_TYPE_ENV = Test		-> bandeau, si mode indexé : détermination des trt à partir de EDTK_REFIDDOC
# EDTK_TYPE_ENV = Development	-> cleanup, bandeau, traitement 'court'

        my $xTypTrt = shift || '';
        if ( defined $_app_typ_trt && $xTypTrt !~ /^[TD]/i ) {
            return $_app_typ_trt;
        }

# seules les types Test et Debug permettent de changer $_app_typ_trt s'il est déjà défini

        if ( $xTypTrt !~ /^([MGLHTDU])/i ) {
            $_app_typ_trt = 'U';

        }
        elsif ( $xTypTrt =~ /^([MGL])/i ) {
            $_app_typ_trt = $1;

        }
        elsif ( $xTypTrt =~ /^([HTD])/i ) {
            $_app_typ_trt = $1;
        }

        warn
"INFO : type de traitement OM = $_app_typ_trt (Mass/Grouped/Local/Homol/Test/Debug/Undef)\n";
        return $_app_typ_trt;
    }
}

{
    my $_DOCLIB;    # DESIGNATION DE LA DCLIB pour le lotissement
                    # valeur accessible uniquement par la méthode _omngr_doclib

    sub _omngr_doclib {
        if ( defined $_DOCLIB ) { return $_DOCLIB; }

        my $doclib = shift;
        if ( !defined $doclib ) {
            my $cfg = config_read('ENVDESC');
            my $ext = shift || $cfg->{'EDTK_EXT_DEFAULT'};
            $_DOCLIB = "DCLIB_" . oe_ID_LDOC() . "." . $ext;

            #$_DOCLIB =~ s/\./_/;
        }
        else {
            $_DOCLIB = $doclib;
        }

        return $_DOCLIB;
    }
}

sub oe_corporation_tag {
    return ( sprintf( "x%.7s", oe_corporation_set() ) );
}

my $_xCORPOR;
my $_DICT;

sub oe_corporation_get {
    return $_xCORPOR;
}

sub oe_corporation_set {

# UTILISATION DICTIONNAIRE :
#	- si paramètre connu dans le dictionnaire => valeur du dictionnaire
#	- si paramètre inconnu dans le paramètre => valeur par défaut (edtk.ini / EDTK_CORP)
#	- si aucun paramètre => dernière valeur connue
    my $parametre = shift;

    if ( !defined($_DICT) ) {
        my $cfg = config_read();
        $_xCORPOR = $cfg->{'EDTK_CORP'};    # Valeur par défaut
        $_DICT = oEdtk::Dict->new( $cfg->{'EDTK_DICO'}, { invert => 1 } );
    }

    my $entity;
    if ( defined($parametre) ) {
        $entity = $parametre;
    }
    else {
        $entity = $_xCORPOR;
    }

    $entity = $_DICT->translate( $entity, 1 );

    if ( defined($entity) ) {

        # si la valeur a été trouvée dans le dictionnaire
        $_xCORPOR = $entity;
    }

    return $_xCORPOR;
}

{
    my $_backup_date;

    sub oe_set_sys_date {
        my $requested_date = shift;

        my $time = time;
        my ( $year, $month, $day, $hour, $min, $sec, $doy, $dow, $dst ) =
          Gmtime($time);

        my $commande = sprintf( "date %s", $requested_date );
        warn "INFO : $commande\n";

        eval { system($commande); };

        #		if ($?){
        if ($@) {
            warn "ERROR: echec commande $commande : $@\n";
            return -1;
        }

        if ( !defined $_backup_date ) {
            $_backup_date = sprintf( "%02s-%02s-%02s", $day, $month, $year );
        }
        return $_backup_date;
    }

    sub _restore_sys_date {
        oe_set_sys_date($_backup_date) if ( defined $_backup_date );
        1;
    }
}

END {
    _restore_sys_date;
    # return "(c) 2005-2012 daunay\@cpan.org - edtk\@free.fr - oEdtk v$VERSION\n";
}

1;
