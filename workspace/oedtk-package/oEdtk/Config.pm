package oEdtk::Config;

# This module implements the loading of the configuration file "edtk.ini".
# Please note that the path to this configuration file depends on the location of the present module:
# this configuration file should be in the subdirectory "iniEdtk", relatively to the present module location.
# In practice: workspace/oedtk-package/oEdtk/iniEdtk/

use utf8;

use strict;
use warnings;

use Config::IniFiles;
use Sys::Hostname;

use Exporter;
our $VERSION   = 0.8031;
our @ISA       = qw(Exporter);
our @EXPORT_OK = qw(config_read);



# Load the configurations from a given list of sections.
# There are two ways of calling this function:
#   * First usage: config_read(\@, app)
#     The first argument is a reference to an array that contains a list of sections's names.
#     The second argument is the name of the application.
#     config_read([<list of sections names>], <application name>)
#     ex: config_read(['section1', 'section2'], 'AC14')
#   * Second usage: config_read(@)
#     Each argument represents the name of a section.
#     config_read(<list of sections names>)
#     ex: config_read('section1', 'section2')
# @return [hash ref] The function returns a reference to a hash that contains the loaded configuration.
# @note The parameter EDTK_PRGNAME is set here.

sub config_read(@) {
    my ( $in_sections, $in_opt_app );

    if ( ref( $_[0] ) eq 'ARRAY' ) {
        $in_sections = $_[0];
        $in_opt_app  = $_[1];
    }
    else {
        $in_sections = \@_;
    }

    my $mod = __PACKAGE__;
    $mod =~ s/::/\//;
    $mod .= '.pm';
    my $dir = $INC{$mod};
    $dir =~ s/\/[^\/]+$//;

    my $ini;
    my %cfg = ();
    if ( -e "$dir/iniEdtk/edtk.ini" ) {
        $ini = "$dir/iniEdtk/edtk.ini";
    }
    else {
        $ini = "$dir/iniEdtk/tplate.edtk.ini";
        warn "INFO : accessing $ini\n";
    }

    my %allcfg = ();
    for ( ; ; ) {
        die "ERROR: config file not found or unreadable: $ini\n" unless -r $ini;
        tie %allcfg, 'Config::IniFiles',
          ( -file => $ini, -default => 'DEFAULT', -fallback => 'DEFAULT' );

        $cfg{'EDTK_HOST'} = uc( hostname() );
        foreach ( 'DEFAULT', 'ENVDESC', @$in_sections ) {
            if ( exists $allcfg{$_} ) {
                %cfg = ( %cfg, %{ $allcfg{$_} } );
            }
        }

        my $ini2 = glob( ( tied %allcfg )->val( "DEFAULT", 'iniEdtk' ) );
        last if not defined $ini2 or $ini2 eq $ini or $ini2 eq 'local';
        $ini    = $ini2;
        %allcfg = ();
    }

    # Get current application name
    if ( defined($in_opt_app) ) {
        $cfg{'EDTK_PRGNAME'} = $in_opt_app;
    }
    else {
        if ( $0 =~ /([\w\-\.]+)\.p[lm]$/i ) {
            $cfg{'EDTK_PRGNAME'} = $1;
        }
        else {
            $cfg{'EDTK_PRGNAME'} = $0;
        }
    }

    # Expand variables inside other variables.
    foreach my $key ( keys %cfg ) {
        1 while $cfg{$key} =~ s/\$(\w+)/read_hash(\%cfg, $1)/gsex;
    }

    return \%cfg;
}

# [PRIVATE]
# Return the value associated to a given key, within a given hash.
# @param [hash ref] $in_hash_ref Reference to the hash.
# @param [string] $in_key The key.
# @return If the given key exists within the given hash, and if the associated value is defined, then the function returns the value.
#         Otherwise, the function returns an empty string ("").
# @TODO What if the value is defined, and its value is an empty string ? How to make the difference between an undefined value, a non-existant key, and an empty string as value ?

sub read_hash {
    my ( $in_hash_ref, $in_key ) = @_;
    if ( exists $in_hash_ref->{$in_key} ) {
        if ( defined $in_hash_ref->{$in_key} ) {
            return $in_hash_ref->{$in_key};
        }
        else {
            warn "Key '$in_key' has an undef value in hash";
        }
    }
    else {
        warn "Key '$in_key' does not exists in hash";
    }
    return '';
}

1;
