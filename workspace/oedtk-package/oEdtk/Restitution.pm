#! /usr/bin/perl
package oEdtk::Restitution;
use base qw( Exporter );
use warnings;
use strict;
use feature qw( switch );
use Carp qw( carp croak );
use Readonly;
use Data::Dumper qw( Dumper );

our @EXPORT_OK = qw( follow_path restitution_value );

# follows a 'path' in a hash:
# if $h = { a => b => { "value" } } then follow_path($h, [ "a", "b" ]) returns "value"
# returns '' if list undef
# returns execution on current value if code ref
sub follow_path {
    my ( $h, $l, $flag_quiet ) = @_;
    $flag_quiet = 0 unless defined $flag_quiet;
    my $res = $h;
    foreach my $key ( @{$l} ) {
        my $ref = ref $key;
        given ($ref) {
            when ( $ref eq '' ) {
                if ( exists $res->{$key} ) {
                    $res = $res->{$key};
                }
                else {
                    carp "No key '$key' found" unless $flag_quiet;
                    return '';
                }
            }
            when ( $ref eq 'CODE' ) {
                return &{$key}($res);
            }
            default { croak "Unexpected key (ref='$ref'): " . Dumper($key); }
        }
    }
    my $ref = ref $res;
    return ( $ref eq 'ARRAY' )
      ? join( "\n", @{$res} )    # adresse_payeur
      : ($ref) ? croak "Unexpected result: " . Dumper($res)
      :          $res;
}

sub restitution_value {
    my ( $h, $obj, $flag_quiet ) = @_;
    return '' if !defined $obj;
    my $ref = ref $obj;
    given ($ref) {
        when ( $ref eq 'ARRAY' ) { return follow_path( $h, $obj, $flag_quiet ) }
        default { croak "Unexpected restitution rule: " . Dumper($obj); }
    }
}

1;
