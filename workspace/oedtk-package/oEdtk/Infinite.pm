#! /usr/bin/perl
package oEdtk::Infinite;
use base qw( Exporter );
use warnings;
use strict;
use feature qw( switch );
use Carp qw( carp croak );
use Readonly;
use Data::Dumper qw( Dumper );
use oEUser::Lib qw( user_cleanup_addr norme_38 );

our @EXPORT_OK = qw( import_cobol_X import_cobol_S9V9
  is_entete is_flux is_ligne is_option
  id_ligne record_values
  extract_options );

Readonly my $INDEX_CODE_POSTAL_DEST => 'xCPDEST';
Readonly my $INDEX_VILLE_DEST       => 'xVILDEST';
Readonly my $INDEX_NOM_DEST         => 'xNOMDEST';

# DFD_ZZ083S01.pdf
Readonly my $DATA_BEGIN_END    => qr{\x00\x02}sox;
Readonly my $OPTIONS_SEPARATOR => qr{\x00\x01}sox;
Readonly my $OPTION_SKIP_BEGIN => 15;              # OPTIONU5063\x00\x02\x00\x01
Readonly my $OPTION_SKIP_END   => 4;               # \x00\x01\x00\x02

sub import_cobol_X {
    my ( $strRef, $pos, $len ) = @_;
    my $res = substr( $$strRef, $pos - 1, $len );
    $res =~ s/\N{SPACE}+$//sox;
    return $res;
}

=pod
# Unused sub?
sub import_cobol_9 {
    my ( $strRef, $pos, $len ) = @_;
    my $res = substr( $$strRef, $pos - 1, $len );
    return $res;
}
=cut

sub import_cobol_S9V9 {
    my ( $strRef, $pos, $int_len, $frac_len ) = @_;
    my $res = substr( $$strRef, $pos - 1, $int_len + $frac_len );
    my $sign = ( $res =~ s/([p-y])$/ord($1) - ord('p')/sex ) ? -1 : 1;
    if ( $res =~ m{^0* \d+$}sx ) {
        substr $res, -$frac_len, 0, ".";    # 4-arg substr (PBP p. 165)
        $res =~ s{^0+}{}sx;
        return $sign * $res;
    }
    else {
        print STDERR "pos=$pos int_len=$int_len frac_len=$frac_len\n";
        croak "Cannot import_cobol_S9V9: " . $res;
    }
}

sub is_entete {
    my ($lineRef) = @_;
    return $$lineRef =~ m{^ENTETE \d}sox;
}

sub is_flux {
    my ($lineRef) = @_;
    return $$lineRef =~ m{^FLUX \N{SPACE}}sox;
}

sub is_ligne {
    my ($lineRef) = @_;
    return $$lineRef =~ m{^LIGNE \N{SPACE}}sox;
}

# DFD_ZZ083S01.pdf
sub is_option {
    my ($lineRef) = @_;
    return $$lineRef =~ m/^OPTIONU506[34]/sox;
}

sub id_ligne {
    my ($lineRef) = @_;
    return substr( $$lineRef, 158, 2 );
}

sub build_address {
    my ($a) = @_;
    return (
        join( "\N{SPACE}", map { $a->{$_} } qw( civilite titre nom prenom ) ),
        join( "\N{SPACE}", map { $a->{$_} } qw( point_remise ) ),
        join( "\N{SPACE}", map { $a->{$_} } qw( complement_adresse ) ),
        join( "\N{SPACE}",
            map { $a->{$_} } qw( numero_voie code_bis_ter libelle_voie ) ),
        join( "\N{SPACE}", map { $a->{$_} } qw( boite_postale ) ),
        join( "\N{SPACE}", map { $a->{$_} } qw( code_cedex nom_cedex ) ),
    );
}

# Inspired by CRP-CTRSANTE and Record E1: http://doc_infinite/mdg/V5.3.0/LIV/DET_AC014.pdf
sub ED_adresse_payeur {    # refacto with adresse_souscripteur if/when needed
    my ($lineRef) = @_;
    my $a = {
        civilite           => import_cobol_X( $lineRef, 429, 10 ),
        titre              => import_cobol_X( $lineRef, 454, 10 ),
        nom                => import_cobol_X( $lineRef, 479, 50 ),
        prenom             => import_cobol_X( $lineRef, 529, 20 ),
        point_remise       => import_cobol_X( $lineRef, 549, 32 ),
        complement_adresse => import_cobol_X( $lineRef, 581, 32 ),
        numero_voie        => import_cobol_X( $lineRef, 613, 9 ),
        code_bis_ter       => import_cobol_X( $lineRef, 622, 2 ),
        libelle_voie       => import_cobol_X( $lineRef, 629, 32 ),
        boite_postale      => import_cobol_X( $lineRef, 662, 32 ),
        code_cedex         => import_cobol_X( $lineRef, 726, 10 ),
        nom_cedex          => import_cobol_X( $lineRef, 736, 32 ),
    };
    return [ user_cleanup_addr( build_address($a) ) ];
}

sub EL_adresse_souscripteur
{    # Attention! do not refacto with ED_adresse_payeur -- may diverge
    my ($lineRef) = @_;
    my $a = {
        civilite           => import_cobol_X( $lineRef, 98 + 429, 10 ),
        titre              => import_cobol_X( $lineRef, 98 + 454, 10 ),
        nom                => import_cobol_X( $lineRef, 98 + 479, 50 ),
        prenom             => import_cobol_X( $lineRef, 98 + 529, 20 ),
        point_remise       => import_cobol_X( $lineRef, 98 + 549, 32 ),
        complement_adresse => import_cobol_X( $lineRef, 98 + 581, 32 ),
        numero_voie        => import_cobol_X( $lineRef, 98 + 613, 9 ),
        code_bis_ter       => import_cobol_X( $lineRef, 98 + 622, 2 ),
        libelle_voie       => import_cobol_X( $lineRef, 98 + 629, 32 ),
        boite_postale      => import_cobol_X( $lineRef, 98 + 662, 32 ),
        code_cedex         => import_cobol_X( $lineRef, 98 + 726, 10 ),
        nom_cedex          => import_cobol_X( $lineRef, 98 + 736, 32 ),
    };
    return [ user_cleanup_addr( build_address($a) ) ];
}

sub ED_index_variables {
    my ($lineRef) = @_;
    return {
        $INDEX_CODE_POSTAL_DEST => import_cobol_X( $lineRef, 726, 10 ),
        $INDEX_VILLE_DEST       => import_cobol_X( $lineRef, 736, 32 ),
        $INDEX_NOM_DEST         => join( "\N{SPACE}",
            import_cobol_X( $lineRef, 479, 50 ),
            import_cobol_X( $lineRef, 529, 20 ) ),
    };
}

sub ED_restitution {
    my ($lineRef) = @_;
    return {
        civilite => import_cobol_X( $lineRef, 429,  10 ),
        nom      => import_cobol_X( $lineRef, 479,  50 ),
        prenom   => import_cobol_X( $lineRef, 529,  20 ),
        ER       => import_cobol_X( $lineRef, 1158, 9 ),
        centre   => import_cobol_X( $lineRef, 1548, 50 )
        ,    # or centre_de_gestion at 1808 X(9)
    };
}

sub EL_restitution {    # TODO refacto with ED_restitution
    my ($lineRef) = @_;
    my $a = {
        civilite => import_cobol_X( $lineRef, 98 + 429, 10 ),
        nom      => import_cobol_X( $lineRef, 98 + 479, 50 ),
        prenom   => import_cobol_X( $lineRef, 98 + 529, 20 ),
        ER       => import_cobol_X( $lineRef, 1270,     9 ),
    };
    $a->{"civilite_nom_prenom"} =
      join( "\N{SPACE}", map { $a->{$_} } qw( civilite nom prenom ) ),
      return $a;
}

sub EI_restitution {
    my ($lineRef) = @_;
    return { nouveau_mode_de_reglement => import_cobol_X( $lineRef, 221, 2 ), };
}

sub E7_adresse {    # refacto with other XX_adresse*
    my ($lineRef) = @_;
    my $a = {
        point_remise       => import_cobol_X( $lineRef, 169, 50 ),
        complement_adresse => import_cobol_X( $lineRef, 219, 50 ),
        numero_voie        => import_cobol_X( $lineRef, 269, 9 ),
        code_bis_ter       => import_cobol_X( $lineRef, 278, 2 ),
        libelle_voie       => import_cobol_X( $lineRef, 295, 50 ),
        boite_postale      => import_cobol_X( $lineRef, 346, 50 ),
        code_cedex         => import_cobol_X( $lineRef, 446, 10 ),
        nom_cedex          => import_cobol_X( $lineRef, 456, 50 ),
    };
    return norme_38( build_address($a) );
}

sub E7_adresse_agence {    # refacto with other XX_adresse*
    my ($lineRef) = @_;
    my $a = {
        point_remise       => import_cobol_X( $lineRef, 528, 50 ),
        complement_adresse => import_cobol_X( $lineRef, 578, 50 ),
        numero_voie        => import_cobol_X( $lineRef, 628, 9 ),
        code_bis_ter       => import_cobol_X( $lineRef, 637, 2 ),
        libelle_voie       => import_cobol_X( $lineRef, 654, 50 ),
        boite_postale      => import_cobol_X( $lineRef, 705, 50 ),
        code_cedex         => import_cobol_X( $lineRef, 805, 10 ),
        nom_cedex          => import_cobol_X( $lineRef, 815, 50 ),
    };
    return norme_38( build_address($a) );
}

sub E7_index_variables {
    my ($lineRef) = @_;
    return {
        DestCodePostal => import_cobol_X( $lineRef, 446, 10 ),
        DestVille      => import_cobol_X( $lineRef, 456, 50 ),
    };
}

sub compute_pic {
    my ( $obj, $lineRef ) = @_;
    my ( $pic, $pos, $len, @args ) = @{$obj};
    given ($pic) {
        when ( $pic eq 'X' ) { return import_cobol_X( $lineRef, $pos, $len ); }
        when ( $pic eq 'S9V9' ) {
            return import_cobol_S9V9( $lineRef, $pos, $len, @args );
        }
        when ( $pic eq 'S9(12)V99' ) {
            return import_cobol_S9V9( $lineRef, $pos, $len, @args );
        }
        default { croak "Unknown pic '$pic' for obj: " . Dumper($obj); }
    }
    return;
}

sub compute_value {
    my ( $obj, $lineRef ) = @_;
    my $ref = ref $obj;
    given ($ref) {
        when ( $ref eq "ARRAY" ) { return compute_pic( $obj, $lineRef ); }
        when ( $ref eq "CODE" ) { return &{$obj}($lineRef); }
        default { croak "Unknown ref '$ref' for obj: " . Dumper($obj); }
    }
    return;
}

sub map_sub {
    my ($map_rule) = @_;
    return ( ( scalar @{$map_rule} ) && ( ( ref $$map_rule[-1] ) eq 'CODE' ) )
      ? $$map_rule[-1]
      : undef;
}

# we retain all met values, as a list
sub sub_list {
    my ( $old_value, $new_value ) = @_;
    return ref $old_value
      ? [ @{$old_value}, $new_value ]
      : [ $old_value, $new_value ];
}

sub record_values {
    my ( $h, $map, $lineRef ) = @_;
    my @errors = ();
    foreach my $field ( keys %{$map} ) {
        if ( exists $h->{$field} ) {
            my $sub = map_sub( $map->{$field} );
            if ( defined $sub ) {
                $h->{$field} = &{$sub}
                  ( $h->{$field}, compute_value( $map->{$field}, $lineRef ) );
            }
            else {
                push @errors,
                  "Field '$field' already present in hash: " . Dumper($h);
            }
        }
        else {
            $h->{$field} = compute_value( $map->{$field}, $lineRef );
        }
    }
    return @errors;
}

sub extract_options {
    my ($lineRef) = @_;
    $$lineRef =~ s/\x80//sox
      ; ### Dégage les x80 qui polluent les flux de recette (mais ne sont pas sur ceux de prod...)
    return [
        split m/$OPTIONS_SEPARATOR/,
        substr( $$lineRef, $OPTION_SKIP_BEGIN, -$OPTION_SKIP_END - 1 )
    ];
}

1;
