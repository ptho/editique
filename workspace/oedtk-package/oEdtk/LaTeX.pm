#! /usr/bin/perl
package oEdtk::LaTeX;
use base qw( Exporter );
use warnings;
use strict;
use feature qw( switch );
use Carp qw( carp croak );
use Readonly;
use Data::Dumper qw( Dumper );
use Encode qw( encode );
use Roman qw( roman );
use oEdtk::Utils qw( IDX_FORMAT );
use charnames ':full';

our @EXPORT_OK =
  qw( tag tag_args ged_tag make_index demo demo_write demo_expected_write latex_escape_string translate_unit concat_string latex_boolean unfold_list
);

Readonly my $LATIN9_ENCODING => 'iso-8859-15';
Readonly my $OUTPUT_ENCODING => $LATIN9_ENCODING;
Readonly my $LIST_SEPARATOR  => "\N{LATIN CAPITAL LETTER S WITH CARON}"
  ;    # 0xa6 in latin-9 (U+00A6 is BROKEN BAR)
Readonly my $LINE_BREAK        => '\\\\';
Readonly my $ADDRESS_SEPARATOR => '~\edGreenBullet~';

Readonly my $UNKNOWN_CHARACTER => '\textcolor{red}{(\#)}';
Readonly my $DEMO_FILE         => '/tmp/demo_chars.tex';

Readonly my $INDEX_FORMAT => oEdtk::Utils::IDX_FORMAT();

Readonly my $WRITE_UNKNOWN_CHAR  => '?';
Readonly my $DEMO_WRITE_FILE     => '/tmp/demo_write.tex';
Readonly my $EXPECTED_WRITE_FILE => '/tmp/expected_write.txt';

Readonly my $VERBOSE => exists $ENV{'VERBOSE'} && $ENV{'VERBOSE'};

Readonly my $GED_TAG_XSOURCE          => 'xSOURCE';
Readonly my $GED_VALUE_PRECONTENTIEUX => 'PREC';

Readonly my @ORDS => sort { $a <=> $b }
  ( 0 .. 255, 0x20ac, 0x160, 0x161, 0x17d, 0x17e, 0x152, 0x153, 0x178 )
  ;    # character #'s to know about
my $CHARACTER_CACHE = { map { $_ => compute_character_cache($_) } @ORDS };

sub ged_tag_xSOURCE          { return $GED_TAG_XSOURCE; }
sub ged_value_precontentieux { return $GED_VALUE_PRECONTENTIEUX; }

sub encode_error {
    my ($ord) = @_;
    carp sprintf(
        "Character <U+%04x> ('%s') does not map to %s",
        $ord, charnames::viacode($ord),
        $OUTPUT_ENCODING
    ) if $VERBOSE;
    return $UNKNOWN_CHARACTER;
}

sub unknown_character {
    my ($str) = @_;
    die "Bad str '$str'" if $str =~ m{[^a-z0-9]}six;    # avoid deep recursion
    return '\textcolor{red}{(' . $str . ')}';
}

=pod
# Unused sub?
sub encode_write {
    my ($ord) = @_;
    carp sprintf(
        "Character <U+%04x> ('%s') does not map to %s",
        $ord, charnames::viacode($ord),
        $OUTPUT_ENCODING
    ) if $VERBOSE;
    return $WRITE_UNKNOWN_CHAR;
}
=cut

=pod
# Unused sub?
sub compute_expected_write_cache {
    my ($ord) = @_;
    croak "Unimplemented output encoding '$OUTPUT_ENCODING'"
      if $OUTPUT_ENCODING ne 'iso-8859-15';    # latin-9
    given ($ord) {
        when ( $ord < 0x20 ) {                 # control character
            carp sprintf "Found character 0x%02x ('%s')", $ord,
              charnames::viacode($ord)
              if $VERBOSE;
            return $WRITE_UNKNOWN_CHAR;
        }
        when ( $ord < 0x7f ) {                 # ASCII (DEL excluded)
            return chr($ord);
        }
        when ( $ord <= 0x9f ) {                # windows-1252? bugware this?
            carp sprintf "Found character 0x%02x ('%s')", $ord,
              charnames::viacode($ord)
              if $VERBOSE;
            return $WRITE_UNKNOWN_CHAR;
        }
        when ( $ord <= 0xff ) {                #
            return "\x20" if $ord == 0xa0;     # U+00A0 NO-BREAK SPACE
            return encode( $OUTPUT_ENCODING, chr($ord), \&encode_write )
              ;                                # not latin => 8 exceptions
        }
        default {
            return chr($ord)
              if $ord == 0x152;    # U+0152 LATIN CAPITAL LIGATURE OE
            return chr($ord) if $ord == 0x153;  # U+0153 LATIN SMALL LIGATURE OE
            return chr($ord)
              if $ord == 0x160;    # U+0160 LATIN CAPITAL LETTER S WITH CARON
            return chr($ord)
              if $ord == 0x161;    # U+0161 LATIN SMALL LETTER S WITH CARON
            return chr($ord)
              if $ord == 0x178;   # U+0178 LATIN CAPITAL LETTER Y WITH DIAERESIS
            return chr($ord)
              if $ord == 0x17d;    # U+017D LATIN CAPITAL LETTER Z WITH CARON
            return chr($ord)
              if $ord == 0x17e;    # U+017E LATIN SMALL LETTER Z WITH CARON
            return chr($ord)
              if $ord == 0x20ac
              ; # U+20AC EURO SIGN -- NB violates the standard, unlike eurosym's \EUR{}
            carp Dumper(
                sprintf( "Unknown character 0x%02x ('%s')", $ord, chr($ord) ) )
              if $VERBOSE;
            return $WRITE_UNKNOWN_CHAR;
        }
    }
}
=cut

sub compute_character_cache_ascii {
    my ($ord) = @_;
    return '\#'                 if $ord == 0x23;
    return '\$'                 if $ord == 0x24;
    return '\%'                 if $ord == 0x25;
    return '\&'                 if $ord == 0x26;
    return '\_'                 if $ord == 0x5f;
    return '\textbackslash{}'   if $ord == 0x5c;    # U+005C REVERSE SOLIDUS
    return '\textasciicircum{}' if $ord == 0x5e;    # U+005E CIRCUMFLEX ACCENT
    return '\{'                 if $ord == 0x7b;    # '\\textbraceleft{}'
    return '\}'                 if $ord == 0x7d;    # '\\textbraceright{}'
    return '\textasciitilde{}'  if $ord == 0x7e;    # U+007E TILDE
    return chr($ord);
}

sub compute_character_cache_8bit {
    my ($ord) = @_;
    return '{$\neg$}'   if $ord == 0xac;            # U+00AC NOT SIGN
    return '{$\pm$}'    if $ord == 0xb1;            # U+00B1 PLUS-MINUS SIGN
    return '{$^2$}'     if $ord == 0xb2;            # U+00B2 SUPERSCRIPT TWO
    return '{$^3$}'     if $ord == 0xb3;            # U+00B3 SUPERSCRIPT THREE
    return '{$\mu$}'    if $ord == 0xb5;            # U+00B5 MICRO SIGN
    return '{$^1$}'     if $ord == 0xb9;            # U+00B9 SUPERSCRIPT ONE
    return '{$\times$}' if $ord == 0xd7;            # U+00D7 MULTIPLICATION SIGN
    return '\textdiv{}' if $ord == 0xf7;            # U+00F7 DIVISION SIGN
    return encode( $OUTPUT_ENCODING, chr($ord), \&encode_error )
      ;                                             # not latin => 8 exceptions
}

sub compute_character_cache_extra {
    my ($ord) = @_;
    return chr($ord) if $ord == 0x152;    # U+0152 LATIN CAPITAL LIGATURE OE
    return chr($ord) if $ord == 0x153;    # U+0153 LATIN SMALL LIGATURE OE
    return chr($ord)
      if $ord == 0x160;    # U+0160 LATIN CAPITAL LETTER S WITH CARON
    return chr($ord) if $ord == 0x161;  # U+0161 LATIN SMALL LETTER S WITH CARON
    return chr($ord)
      if $ord == 0x178;    # U+0178 LATIN CAPITAL LETTER Y WITH DIAERESIS
    return chr($ord)
      if $ord == 0x17d;    # U+017D LATIN CAPITAL LETTER Z WITH CARON
    return chr($ord) if $ord == 0x17e;  # U+017E LATIN SMALL LETTER Z WITH CARON
    return chr($ord)
      if $ord == 0x20ac
      ;  # U+20AC EURO SIGN -- NB violates the standard, unlike eurosym's \EUR{}
    carp Dumper( sprintf( "Unknown character 0x%02x ('%s')", $ord, chr($ord) ) )
      if $VERBOSE;
    return $UNKNOWN_CHARACTER;
}

sub compute_character_cache {
    my ($ord) = @_;
    croak "Unimplemented output encoding '$OUTPUT_ENCODING'"
      if $OUTPUT_ENCODING ne $LATIN9_ENCODING;
    given ($ord) {
        when ( $ord < 0x20 ) {    # control character
            return "$LINE_BREAK\n"
              if $ord == 0x0a
              ; # \n --; \n avoids '\\"{}' which makes indexation fail # might mail if line is empty
            return '' if $ord == 0x0d;    # \r
            carp sprintf "Found character 0x%02x ('%s')", $ord,
              charnames::viacode($ord)
              if $VERBOSE;
            return unknown_character( sprintf( "%02x", $ord ) );

            # return $UNKNOWN_CHARACTER;
        }
        when ( $ord < 0x7f ) { return compute_character_cache_ascii($ord); }
        when ( $ord <= 0x9f ) {           # windows-1252? bugware this?
            carp sprintf "Found character 0x%02x ('%s')", $ord,
              charnames::viacode($ord)
              if $VERBOSE;
            return unknown_character( sprintf( "%02x", $ord ) );

            # return $UNKNOWN_CHARACTER;
        }
        when ( $ord <= 0xff ) { return compute_character_cache_8bit($ord); }
        default               { return compute_character_cache_extra($ord); }
    }
}

sub latex_escape_character {
    my ($ord) = @_;
    return exists $CHARACTER_CACHE->{$ord}
      ? $CHARACTER_CACHE->{$ord}
      : compute_character_cache($ord);
}

sub latex_boolean {
    my ($bool) = @_;
    return $bool ? "true" : "false";
}

sub translate_unit {
    my ($str) = @_;
    die "str undef" unless defined $str;
    given ($str) {
        when ( $str eq '' ) { return ''; }
        when ( $str eq 'EUR' ) {
            return latex_narrow_space() . tag_args('EUR');
        }    # eurosym EURO symbol
        when ( $str eq '%' ) {
            return latex_narrow_space() . latex_escape_string('%');
        }
        default { croak "Unknown unit '$str'"; }
    }
}

sub latex_escape_string {
    my ($str) = @_;
    return
      join( "", map { latex_escape_character( ord($_) ) } split( m//, $str ) );
}

sub latex_escape_string_no_byte_a6 {
    my ($str) = @_;
    croak "OUTPUT_ENCODING unexpected: $OUTPUT_ENCODING"
      if $OUTPUT_ENCODING ne $LATIN9_ENCODING;
    if ( $str =~ m/\N{LATIN CAPITAL LETTER S WITH CARON}/sox ) {
        carp
          "Replace capital letter S's with caron in '$str' with capital S's...";
        $str =~ s/\N{LATIN CAPITAL LETTER S WITH CARON}/S/gsox;
    }
    return latex_escape_string($str);
}

sub latex_escape_list_no_byte_a6 {
    my (@l) = @_;
    return
      map { ( $_ =~ m{\S}sx ) ? latex_escape_string_no_byte_a6($_) : () } @l;
}

sub latex_list {
    my ($l) = @_;
    my @res = latex_escape_list_no_byte_a6( @{$l} );
    return join( $LIST_SEPARATOR, @res );
}

# cannot use colors with \StrSubstitute replacements... (TeX capacity exceeded)
# Used for addresses in the bottom of pages (green separators)
sub special_colored_address {
    my ($val) = @_;
    if ( ref $val eq 'HASH' ) {
        my $nKeys = scalar keys %{$val};
        if (   ( $nKeys == 1 )
            && ( exists $val->{'address'} )
            && ( ( ref( $val->{'address'} ) eq 'ARRAY' ) ) )
        {
            my @res   = ();
            my @val   = @{ $val->{'address'} };
            my $first = shift @val;
            push @res, '\textbf{', latex_escape_string_no_byte_a6($first),
              '}' . $LINE_BREAK
              if $first =~ m{\S}sox;
            push @res,
              join( $ADDRESS_SEPARATOR, latex_escape_list_no_byte_a6(@val) );
            push @res, $LINE_BREAK if scalar @val;
            push @res, '{\TelIndigoWebsite}';
            return join( '', @res );
        }
    }
    croak 'Not implemented: ' . Dumper($val);
}

# kludges when we need Perl to compute complicated LaTeX
sub special_treatment {
    my ($h)   = @_;
    my @keys  = keys %{$h};
    my $nKeys = scalar @keys;
    if ( $nKeys == 1 ) {
        my $key = $keys[0];
        given ($key) {
            when ( $key eq 'colored_address' ) {
                return special_colored_address( $h->{$key} );
            }
        }
    }
    croak 'Not implemented: ' . Dumper($h);
}

sub latex_xkeyval_value {
    my ($obj) = @_;
    my $ref = ref $obj;
    given ($ref) {
        when ( $ref eq "" ) {    # should we care about embedded newlines?
            $obj = latex_escape_string($obj);
        }
        when ( $ref eq "ARRAY" ) {
            $obj = latex_list($obj);
        }
        when ( $ref eq "HASH" ) {
            $obj = special_treatment($obj);
        }
        default {
            croak "Should not happen/not implemented. Bad object ref: "
              . Dumper($obj);
        }
    }
    return ( $obj =~ m{[=,\{\}]}sx ) ? "{$obj}" : $obj;
}

# define a list the oEdtk way
sub latex_define_list {
    my ( $tag, $l ) = @_;
    return ( "\\edListNew{$tag}", map { "\\edListAdd{$tag}{$_}" } @{$l} );
}

sub remove_roman_numbering {
    my ($h) = @_;
    foreach my $key ( sort keys %{$h} ) {
        my $ref = ref $h->{$key};
        if ( substr( $key, -6 ) eq "#roman" ) {
            croak "Unexpected value for key '$key': " . Dumper($h)
              unless $ref eq "ARRAY";
            my $counter = 0;
            my $baseKey = substr( $key, 0, -6 );
            foreach my $val ( @{ $h->{$key} } ) {
                $counter++;
                my $romanKey = $baseKey . roman($counter);
                croak "Bad tag '$romanKey'" unless is_valid_tag($romanKey);
                $h->{$romanKey} = $val;
            }
            delete $h->{$key};
        }
        else {
            croak "Bad tag '$key'" unless is_valid_tag($key);
        }
    }
    return $h;
}

sub index_value {
    my ( $h, $tag ) = @_;
    my $ref = ref $h->{$tag};
    return ( $ref eq '' )
      ? "\\long\\gdef\\$tag" . "{" . latex_escape_string( $h->{$tag} ) . "}%"
      : ( $ref eq 'ARRAY' ) ? latex_define_list( $tag, $h->{$tag} )
      :   croak "Unexpected value for key '$tag': " . Dumper($h);
}

sub make_index {
    my ( $h, %extra ) = @_;

    # check_index_is_complete( $h );

    # uncomment to see which ones are undef (in dev)
    # print STDERR 'make_index: h: ' . Dumper($h);
    # print STDERR 'make_index: extra: ' . Dumper(\%extra);
    my $rrn = remove_roman_numbering($h);    # avoid side effects
    my @res   = map { index_value( $rrn, $_ ) } sort keys %{$rrn};
    my $extra = remove_roman_numbering( \%extra );
    push @res, map { index_value( $extra, $_ ) } sort keys %{$extra};
    return join( "\n", @res, '' );
}

sub check_index_is_complete {
    my ($h) = @_;
    my @errors = ();
    foreach my $s ( @{$INDEX_FORMAT} ) {
        my ( $orig, $new, $offset ) = @{$s};
        next unless length $new;
        if ( !exists $h->{$new} ) {
            push @errors,
"Missing field '$new' (was '\\$orig' in the previous index system)";
        }
        else {
            if ( defined $offset ) {
                push @errors,
"Field '$new' not a list (was '\\$orig' in the previous index system)"
                  unless ref( $h->{$new} ) eq 'ARRAY';
            }
        }
    }
    croak join( "\n", @errors, '' ) if scalar @errors;
    return;
}

sub max {
    my (@l) = @_;
    my $res = 0;
    foreach my $val (@l) {
        $res = $val if $val > $res;
    }
    return $res;
}

sub pad_left {
    my ( $str, $len ) = @_;
    return ( " " x ( $len - length($str) ) ) . $str;
}

sub is_valid_tag {
    my ($tag) = @_;
    return $tag =~ m{^[a-z]+$}six;
}

sub ged_tag {
    my ( $tag, $value ) = @_;
    return ["Bad tag '$tag'"] unless is_valid_tag($tag);
    return "\\gdef\\$tag" . '{' . latex_escape_string($value) . '}%';
}

# returns a list of one error, or the string wanted
sub tag {
    my ( $tag, $h ) = @_;
    return ["Bad tag '$tag'"] unless is_valid_tag($tag);
    return "\\$tag" if !defined $h;
    return ["Defined but empty hash for tag '$tag'"] unless scalar keys %{$h};
    my @l = ();
    my $pad = 1 + max( map { length $_ } keys %{$h} );
    foreach my $key ( sort keys %{$h} ) {
        return [ "key '$key' undef: " . Dumper($h) ] unless defined $h->{$key};
        push @l,
          join( " = ",
            pad_left( $key, $pad ),
            latex_xkeyval_value( $h->{$key} ) );
    }
    return "\\$tag\[\n\x20" . join( "\n,", @l ) . "\n]%";
}

sub latex_narrow_space {
    return "\\,";
}

sub concat_string {
    my (@l) = @_;
    return join( '', map { interpret_argument($_) } @l );
}

sub interpret_argument {
    my ($arg) = @_;
    my $ref = ref $arg;
    given ($ref) {
        when ('')      { return latex_escape_string($arg); }
        when ('ARRAY') { my ( $sub, @l ) = @{$arg}; return &{$sub}(@l); }
        default        { croak "Unexpected ref $ref for arg: " . Dumper($arg); }
    }
}

sub tag_args {
    my ( $tag, @args ) = @_;
    die "Bad tag '$tag'" unless is_valid_tag($tag);
    @args = ('') unless scalar @args;    # at least {} after the tag name
    return
      join( '', "\\$tag", map { '{' . interpret_argument($_) . '}' } @args );
}

sub demo {
    my @l =
      map { [ $_, $_, $CHARACTER_CACHE->{$_}, charnames::viacode($_) ] } @ORDS;
    my $preamble = << 'EOPREAMBLE';
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{color,textcomp,longtable}
\begin{document}
EOPREAMBLE
    my $postamble = << 'EOPOSTAMBLE';
\end{document}
EOPOSTAMBLE
    my $body = join(
        "\n",
        '\begin{longtable}{llcl}',
        '\hline',
        "\\textbf{Dec} & \\textbf{Hex} & \\textbf{Char} & \\textbf{Name}"
          . $LINE_BREAK,
        '\hline', '\hline',
        '\endhead',
        '\endfoot',
        (
            map {
                sprintf "%d & %02x & %s & %s$LINE_BREAK%s", @{$_},
                  ( $$_[0] % 2 )
                  ? "\n\\hline"
                  : ''
            } @l
        ),
        '\end{longtable}',

        #  '',
        #  '\begin{itemize}',
        #  ( map { sprintf "\\item %d %02x %s ('%s')", @{$_} }  @l ),
        #  '\end{itemize}',
        ''
    );
    open my $out, ">encoding($OUTPUT_ENCODING)", $DEMO_FILE || croak $!;
    print $out join( '', $preamble, $body, $postamble );
    close $out || croak $!;
    carp "demo saved to '$DEMO_FILE'";
    return 0;
}

1;
