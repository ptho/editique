#
# eOcr
#
# This package contain functions to OCRise documents
#
# Documentation at the __END__
#
package eOcr;

use Data::Dumper;
use strict;
use warnings;
use Exporter;
use Readonly;
use oEdtk::Main qw(oe_new_ID_LDOC);
use File::Copy qw( copy );
use Sys::Syslog qw(:standard :macros setlogsock);

our @ISA    = qw(Exporter);
our @EXPORT = qw( ocrise );

Readonly my $TESSERACT => '/usr/local/bin/tesseract';
Readonly my $BASEDIR   => '/home/akelwood';
Readonly my $WORKDIR   => $BASEDIR . '/tmp';
Readonly my $FACILITY  => 'local7';
Readonly my $LOG_INFO  => 'INFO';
Readonly my $LOG_ERROR => 'ERROR';

sub ocrise {
    my ( $source_path, $source_file ) = @_;

    Sys::Syslog::setlogsock 'unix';
    Sys::Syslog::openlog( 'webservice editique', 'pid', $FACILITY );    # check
    ocrSyslog( $LOG_INFO, "pdf2pdftxt initialisation" );

    return error( "working directory : " . $WORKDIR . " is missing" )
      unless -d $WORKDIR;
    print "WORKDIR:" . $WORKDIR . "\n";
    chdir($WORKDIR);

    return error( $source_path . "/" . $source_file . " is missing" )
      unless -e $source_path . "/" . $source_file;

    my @stat = stat $source_path . "/" . $source_file;
    return error( $source_path . "/" . $source_file . " is empty" )
      if $stat[7] == 0;

    my $uid = oe_new_ID_LDOC();
    mkdir( $WORKDIR . "/" . $uid, 0700 ) unless ( -d $WORKDIR . "/" . $uid );
    chdir( $WORKDIR . "/" . $uid ) or die "can't chdir $WORKDIR/$uid\n";

    my $tiff_file = pdf2tiff( $source_path, $source_file, $uid );
    my $text_file = tiff2text($tiff_file);

    my $target_file = merge2pdf( $source_file, $text_file );

    return ( $WORKDIR . "/" . $uid, $target_file );
}

sub pdf2tiff {
    my ( $source_path, $source_file, $uid ) = @_;
    my $tiff_file = $source_file;
    $tiff_file =~ s/\.pdf$/\.tif/;
    copy( $source_path . "/" . $source_file, $WORKDIR . "/" . $uid )
      or die "Copy failed: $!";
    print "convert -density 300 "
      . $source_file
      . " -type Grayscale -compress lzw -background white +matte -depth 1 "
      . $tiff_file . "\n";
    my $res =
      system( "convert -density 300 "
          . $source_file
          . " -type Grayscale -compress lzw -background white +matte -depth 1 "
          . $tiff_file );
    die("error $res") unless $res == 0;
    return $tiff_file;
}

sub tiff2text {
    my ($tiff_file) = @_;
    my $text_file = $tiff_file;
    $text_file =~ s/\.[.]+$/\.text/;
    print $TESSERACT
      . " -c textonly_pdf=1 "
      . $tiff_file . " "
      . $text_file
      . " pdf\n";
    my $res =
      system( $TESSERACT
          . " -c textonly_pdf=1 "
          . $tiff_file . " "
          . $text_file
          . " pdf" );
    die("error $res") unless $res == 0;
    return $text_file . ".pdf";
}

sub merge2pdf {
    my ( $source_file, $text_file ) = @_;

    my $target_file = $source_file;
    $target_file =~ s/\.(.+)$/\.ocrised\.$1/;
    print "pdftk "
      . $text_file
      . " background "
      . $source_file
      . " output "
      . $target_file . "\n";
    my $res =
      system( "pdftk "
          . $text_file
          . " background "
          . $source_file
          . " output "
          . $target_file );
    die("error $res") unless $res == 0;
    return $target_file;
}

sub error {
    my ($error_string) = @_;
    ocrSyslog( $LOG_ERROR, $error_string );
    die $error_string;
}

sub ocrSyslog {
    my ( $severity, $message ) = @_;
    print $severity. " " . uc($severity) . " - " . $message . "\n";
    syslog( "NOTICE", " " . uc($severity) . " - " . $message );
}

1;
