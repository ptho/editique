#!/opt/editique/perl/bin/perl

BEGIN {
    unshift( @INC,
        '/home/akelwood/sources/edtk_MNT/workspace/oedtk-package/ocr/' );
    unshift( @INC,
        '/home/akelwood/sources/edtk_MNT/workspace/webservice/lib/' );
}

use strict;
use warnings;
use eOcr qw ( ocrise );
use File::Basename;

my ($file) = @ARGV;

print $file. "\n";

my ( $filename, $directories, $suffix ) = fileparse($file);

print "[$filename, $directories, $suffix]\n";
my $realpath = `realpath $directories`;
print "$realpath\n";
$realpath =~ s/\n//g;

ocrise( $realpath, $filename );
